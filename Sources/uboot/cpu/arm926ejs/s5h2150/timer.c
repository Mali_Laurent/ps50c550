/**
 * u-boot-1.1.6 port for S5H2150
 *
 * (C)Copyright 2003-2007 Samsung Electronics, Inc.
 */
 /*
 * (C) Copyright 2002
 * Gary Jennejohn, DENX Software Engineering, <gj@denx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

 /**
 * timer.c : 16-bit timer
 * 
 * @author   : ij.jang
 * @version  : $Revision$
 * @see      : S5H2150 User's manual
 *
 * modification history
 * --------------------
 * 7/July/2007 : ij.jang port to u-boot-1.1.6 (From u-boot-1.1.3)
 * 17/Apr/2007 : ij.jang fix initial values. (timer was too fast)
 * 14/June/2006 : ij.jang modified from saturn.
 */

#include <common.h>
#include <asm/arch/s5h2150.h>

#define TIMER_LOAD_VAL	0xFFFF

static ulong timestamp;
static ulong lastdec;

ulong READ_TIMER(void)
{
	return (0xFFFF & REG_GET(R_TMCNT0));
}

/**
 * timer_init()
 * 	initialize the 16 bit timer
 *	prescaler=10, MUX=4, timer freq = 1250000Hz
 *	0.8 micro seconds per 1 decrement
 * @author		ij.jang
 * @return		0 when success
 * @version		$Revison$
 */
int timer_init(void)
{
	/* Before init, make interrupt disabled and stop the timer */
	REG_PUT(R_TMCON0, 0x00000000);
	/* no DMA request channel */
	REG_PUT(R_TMDMASEL, 0x00);
	/* prescaler 10, period 0xFFFF */	
	REG_PUT(R_TMDATA0, (10<<16) | TIMER_LOAD_VAL);	
	
	timestamp = 0;
	lastdec = TIMER_LOAD_VAL;

	/* 1/4 Mux input, interrupt or DMA enable and start timer*/
	REG_PUT(R_TMCON0, 0x3);

	return (0);
}

void reset_timer (void)
{
	reset_timer_masked ();
}

ulong get_timer (ulong base)
{
	return get_timer_masked () - base;
}

void set_timer (ulong t)
{
	timestamp = t;
}

static ulong get_delay_time(const unsigned long usec)
{
#if 0
	return (usec / (1000000 / CFG_HZ));
#else
	/* CFG_HZ is 1250000 */
	return (usec + (usec >> 2));
#endif
}

void udelay (unsigned long usec)
{
	ulong tmo;
	
	tmo = get_delay_time(usec);
	tmo += get_timer (0);			/* from current time to +tmo */

	while (get_timer_masked () < tmo)
		/*NOP*/;
}

void udelay_masked (unsigned long usec)
{
	ulong tmo;

	tmo = get_delay_time(usec);	
	reset_timer_masked ();			/* from 0 to tmo */

	while (get_timer_masked () < tmo)
		/*NOP*/;
}

void reset_timer_masked (void)
{
	lastdec = READ_TIMER();
	timestamp = 0;
}

ulong get_timer_masked (void)
{
	volatile ulong now = READ_TIMER();

	/* add elapsed time to timestamp */
	timestamp += ( (lastdec - now) & 0xFFFF );
	lastdec = now;

	return timestamp; 
}

unsigned long long get_ticks(void)
{
	return get_timer(0);
}
