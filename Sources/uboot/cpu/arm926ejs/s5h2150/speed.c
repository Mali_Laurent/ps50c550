/**
 * u-boot-1.1.6 port for S5H2150
 *
 * (C)Copyright 2003-2007 Samsung Electronics, Inc.
 */
 
/**
 * speed.c : implements functions that returns the clock value
 * 
 * @author   : ijj.ang
 * @see      : S5H2150 User's manual
 * @version  : $Revision$
 *
 * modification history
 * --------------------
 * 9, July, 2007 : ij.jang port to u-boot-1.1.6
 * 14, Jun, 2006 : jh.ryu modified for S5h2112.
 * 14, Nov, 2005 : ij.jang created.
 */

#include <common.h>
#include <asm/arch/s5h2150.h>

static ulong g_fclk = 0;
static ulong g_ddrclk = 0;
static ulong g_bclk = 0;

/* there are 3 source input clock 27Mh */
ulong get_FCLK(void)
{
	ulong r;
	ulong m, p, s;

	/* calculate only 1 time, and reuse */
	if( g_fclk )
		return g_fclk;

	r = REG_GET(R_PLLCON) & 0xFFFF0000;

	r=(r>>16);
	s=(r&0x3);
	p = (r & 0xFC) >> 2;
	m = (r & 0xFF00) >> 8;

	g_fclk = r = (m * CONFIG_SYS_CLK_FREQ)/(p);

	return r;
}

ulong get_BCLK(void)
{
	ulong r;
	ulong m, p;

	if (g_bclk != 0)
		return g_bclk;

	r = REG_GET(R_BUSPLLCON) & 0xFFFF;
	p = (r>>2) & 0x3F;
	m = (r>>8) & 0xFF;

	g_bclk = (m * CONFIG_SYS_CLK_FREQ) / (p);
	return g_bclk;
}

ulong get_DDRCLK(void)
{
	ulong r;
	ulong m, p;

	if (g_bclk != 0)
		return g_bclk;

	r = REG_GET(R_PLLCON) & 0xFFFF;
	p = (r>>2) & 0x3F;
	m = (r>>8) & 0xFF;

	g_ddrclk = ((m * CONFIG_SYS_CLK_FREQ) / p);
	return g_ddrclk;
}

ulong get_HCLK(void)
{
	uchar mod = (uchar)((REG_GET(R_CLKMOD) >>4) & 0x0000000F);
	return get_FCLK() >> mod;
}

ulong get_PCLK(void)
{
	uchar mod = (uchar)((REG_GET(R_CLKMOD) & 0x0000000F));
	return get_FCLK() >> mod;
}

int print_cpuinfo(void)
{
	ulong arm_clock = get_FCLK();
	ulong ddr2_clock = get_DDRCLK();
	ulong bus_clock = get_BCLK();
	ulong ahb_clock = get_HCLK();

	printf("S5H2150 Operation clock\n");
	printf("ARM clock : %03d.%03d Mhz\n", arm_clock/1000000, (arm_clock%1000000)/10000);
	printf("DDR clock : %03d.%03d Mhz\n", ddr2_clock/1000000, (ddr2_clock%1000000)/10000);
	printf("BUS clock : %03d.%03d Mhz\n", bus_clock/1000000, (bus_clock%1000000)/10000);
	printf("AHB clock : %03d.%03d Mhz\n", ahb_clock/1000000, (ahb_clock%1000000)/10000);
	
	return 0;
}
