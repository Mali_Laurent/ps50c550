/**
 * u-boot-1.1.6 port for S5H2150
 *
 * (C)Copyright 2003-2007 Samsung Electronics, Inc.
 */
 
/*
 * (C) Copyright 2002
 * Gary Jennejohn, DENX Software Engineering, <gj@denx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
/**
 * serial.S : basic UART driver for S5H2150
 * 			original code from u-boot smdk2410 port
 * @author   : Ikjoon Jang (ij.jang@samsung.com)
 * @version  : $Revision$
 * @see      : S5H21500 User's manual
 *
 * modification history
 * --------------------
 * 09.July.2006 - ij.jang, port to u-boot-1.1.6
 * 04.May.2006 - ij.jang, remove the modem control support
 * 16.Feb.2006 - ij.jang, modify the UFCON initial value
 * 16.Nov.2005 - ij.jang, support FIFO buffer for Rx/Tx
 * 14.Nov.2005 - ij.jang, created.
 */

#include <config.h>
#include <common.h>
#include <asm/arch/s5h2150.h>

/* registers */
#if defined(CONFIG_SERIAL0)
#define UART_PORT	0
#elif defined(CONFIG_SERIAL1)
#define UART_PORT	1
#elif defined(CONFIG_SERIAL2)
#define UART_PORT	2
#endif

#define	R_ULCON			V_UART(unsigned, UART_PORT, 0x00)
#define	R_UCON			V_UART(unsigned, UART_PORT, 0x04)
#define	R_UFCON			V_UART(unsigned, UART_PORT, 0x08)
#define R_UMCON			V_UART(unsigned, UART_PORT, 0x0C)
#define	R_UTRSTAT		V_UART(unsigned, UART_PORT, 0x10)
#define	R_UERSTAT		V_UART(unsigned, UART_PORT, 0x14)
#define	R_UFSTAT		V_UART(unsigned, UART_PORT, 0x18)
#define R_UMSTAT		V_UART(unsigned, UART_PORT, 0x1C)
#define	R_UBRDIV		V_UART(unsigned, UART_PORT, 0x28)

#define	R_UTXH			V_UART(char, UART_PORT, 0x20)
#define	R_URXH			V_UART(char, UART_PORT, 0x24)

/* UART register initial values */
#define ULCON_INIT_VAL		(0x03)
#define UMCON_INIT_VAL		(0)
#define UCON_INIT_VAL		(0x05)

#ifdef CONFIG_USE_UART_FIFO
#define UFCON_INIT_VAL		(0x07)
#define POLLING_RX()		((REG_GET(R_UFSTAT) & 0x10F) > 0)
#define POLLING_TX()		((REG_GET(R_UFSTAT) & (1<<9)) == 0 )
#else
#define UFCON_INIT_VAL		(0x0)
#define POLLING_RX()		((REG_GET(R_UTRSTAT) & 0x1))
#define POLLING_TX()		((REG_GET(R_UTRSTAT) & 0x4))
#endif

/* see speed.c */
extern ulong get_PCLK(void);

/**
 * serial_tstc()
 * 		: Test whether a character is in the RX buffer
 * @return		0 on no inputs, otherwise returns the count of inputs
 * @version    $Revison$
 */
int serial_tstc (void)
{
	return POLLING_RX();
}

/**
 * serial_setbrg()
 * 		: Initialize the UART controller
 * @author		ij.jang
 * @version		$Revison$
 */
void serial_setbrg(void)
{
	volatile int i;
	unsigned int reg = 0;

	reg = ((unsigned int)((get_PCLK() / (CONFIG_BAUDRATE * 16))+0.5)) -1;	
	
	REG_PUT(R_ULCON, ULCON_INIT_VAL);
	REG_PUT(R_UCON, UCON_INIT_VAL);
	REG_PUT(R_UFCON, UFCON_INIT_VAL);
	REG_PUT(R_UMCON0, UMCON_INIT_VAL);

	REG_PUT(R_UBRDIV, reg);

	/* delay */
	for (i = 0; i < 100; i++)
	{
	}
}

/**
 * serial_init()
 * 		: Initialize the UART controller
 * @author		ij.jang
 * @return		Always return 0
 * @version		$Revison$
 */
int serial_init (void)
{
	serial_setbrg();
	return (0);
}

/**
 * serial_getc()
 * 		: Read a single byte from the serial port.
 * @author		ij.jang
 * @return		Returns 1 on success, 0 otherwise.
 * @version		$Revison$
 */
int serial_getc (void)
{
	while( !POLLING_RX() )
	{
	}
	return (REG_GET(R_URXH) & 0xff);
}

/**
 * serial_putc()
 * 		: Output a single byte to the serial port.
 * @version    $Revison$
 */
void serial_putc (const char c)
{
	while( POLLING_TX() == 0 )
	{
	}
	REG_PUT(R_UTXH, c);

	if( c == '\n' )
	{
		serial_putc('\r');
	}
}

/**
 * serial_puts()
 * 		: Output a string
 * @version    $Revison$
 * @param	s : [in]output string
 */
void serial_puts (const char *s)
{
	while (*s)
	{
		serial_putc(*s);
		s++;
	}
}

/**
 * debug_putc()
 *              : Output a single byte to the serial port.
 * @version    $Revison$
 */
void debug_putc (const char c)
{
        while( POLLING_TX() == 0 )
        {
        }
        REG_PUT(R_UTXH, c);
}

