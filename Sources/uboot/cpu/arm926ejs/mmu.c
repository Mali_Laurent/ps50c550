/**
 * u-boot-1.1.3 port for s5h2xxx
 *
 * D-TV Development team, SYS.LSI
 * (C)Copyright 2007 Samsung Electronics, Inc.
 */
/**
 * lowlevel_init.S : Initialize board-specific functions.
 * 
 * @author   : ij.jang@samsing.com
 * @see      : ARM reference manual
 * @version  : $Revision$
 *
 * modification history
 * --------------------
 * 08.Aug.2007 : only support 1MB mapping, remove 2nd level table support.
 		move table storage to global variable
 		from raw memory address.
 * 28.March.2007 : make common code for arm926ejs
 * ??.??.2007 : created.
 *
 */

#include <common.h>

#ifdef USE_926EJS_MMU_TRANS

#include <command.h>
#include <arm926ejs.h>

#include <arm926ejs_mmu.h>

#ifdef MMU_DEBUG
#define MMU_VERBOSE(...)	printf(...)
#else
#define MMU_VERBOSE(...)
#endif


/* MMU TABLE size :
   	16KB for 1st level table,
	grow up when 2nd level table is created.
*/
struct mmu_info {
	unsigned int saddr;
	unsigned int size;
};


/* read co-processor 15, register #1 (control register) */
static unsigned long read_p15_c1 (void)
{
	unsigned long value;

	__asm__ __volatile__(
		"mrc	p15, 0, %0, c1, c0, 0   @ read control reg\n"
		: "=r" (value)
		:
		: "memory");

#ifdef MMU_DEBUG
	printf ("p15/c1 is = %08lx\n", value);
#endif
	return value;
}

/* write to co-processor 15, register #1 (control register) */
static void write_p15_c1 (unsigned long value)
{
#ifdef MMU_DEBUG
	printf ("write %08lx to p15/c1\n", value);
#endif
	__asm__ __volatile__(
		"mcr	p15, 0, %0, c1, c0, 0   @ write it back\n"
		:
		: "r" (value)
		: "memory");

	read_p15_c1 ();
}

static void cp_delay (void)
{
	volatile int i;

	for (i = 0; i < 100; i++);
}


static struct mmu_info mmu_info;

/* support only SECTION mapping */
#if 0
/* create second-level table - small page descriptors
   parameters:
 	cptd : coarse page table descriptor
 	tbl : address of second-level table to make
 	map : mapping info for second-level table
 	mapcnt : count of mapping info
 */
int create_mmu_sl(const unsigned int pabase, const unsigned int *tbl,
		const struct mmu_mapinfo *map, const int mapcnt)
{
	unsigned int desc = 0;
	unsigned int *p;
	int i, j;
	/* AP0~3 = 0b11, Small page */
	desc = (0x3<<10) | (0x3<<8) | (0x3<<6) | (0x3<<4) | SLD_SMALL;

	/* make 256 entries */
	p = (unsigned int*)tbl;
	memset (p, 0, sizeof(desc)*256);

	for (i=0; i<mapcnt; i++) {
		if ((map[i].flags & SLD_TMASK) != SLD_SMALL)
			return -1;
		if (map[i].saddr & 0xFFF || map[i].size & 0xFFF)
			return -1;

		desc &= ~(SLD_CBMASK);
		desc |= map[i].flags & SLD_CBMASK;

		desc &= ~(SLD_ADDRMASK);
		desc |= pabase & 0xFFF00000;
		desc |= map[i].saddr & 0x000FF000;

		for (j=0; j<(map[i].size)>>12; j++) {
			*p = desc;
			desc += 0x1000;
			p++;
		}
	}

#ifdef MMU_DEBUG
	if (tbl+256 != p) {
		printf ("secondary mapping info is wrong\n");
		return -1;
	}
#endif

	return 0;
}
#endif

int create_mmu_fl(struct mmu_info *info, struct mmu_mapinfo *map)
{
	unsigned int desc = 0;
	unsigned int *taddr;
	//unsigned int *taddr2;
	int i;

	/* check 1MB alignment */
	if (map->size & 0xFFFFF || map->saddr & 0xFFFFF) {
		printf ("mmu: 1st level table, not 1MB aligned.\n");
		return -1;
	}

	/* address of first entry */
	taddr = ((unsigned int *)(info->saddr)) + (map->saddr >>20);
	
	switch (map->flags & FLD_TMASK) {
	case FLD_SECTION:
		/* AP = 0b11, Domain = 0b0001 */
		desc = (3<<10) | (1<<5) | map->flags;
		desc |= (FLD_ADDRMASK & map->saddr);
		for (i=0; i<(map->size>>20); i++,taddr++) {
			*taddr = desc;
			desc += 0x00100000;
		}
		break;

	case FLD_FAULT:
		if (map->size & 0xFFFFF)
			return -1;
		/* create fault descriptors */
		memset ((void*)taddr, 0, (map->size)>>20);
		break;

		/* ij.jang : only support 1MB mapping */
#if 0
	case FLD_COARSE:
		/* Domain = 0b0001 */
		desc = (1<<5) | FLD_COARSE;

		for (i=0; i<(map->size>>20); i++,taddr++) {
			/* taddr2 = address of 2nd page table */
			taddr2 = (unsigned int*)(info->saddr + info->size);
			/* grow up the mmu table area */
			info->size += (sizeof(desc)*256);

			desc |= ((unsigned int)taddr2) & 0xFFFFFC00;
			*taddr = desc;

			create_mmu_sl (map->saddr + (i<<20), taddr2,
					map->tbl, map->tbl_cnt);
		}

		break;
#endif
	default:
		puts ("MMU ERROR : mapping table bad!\n");
		return -2;
	}

	return 0;
}

int mmu_status (void)
{
	return (read_p15_c1 () & C1_MMU) != 0;
}

static void arm926_dcache_disable(void);
static void arm926_dcache_enable(void);

//__asm__ __volatile__("mrc p15, 0, %0, c2, c0, 0" :"=r" (reg));
void mmu_enable(void)
{
	volatile unsigned int reg;

	icache_disable();
	arm926_dcache_disable();

	if (mmu_status()) {
		printf ("FATAL : MMU is enabled already, do nothing.\n");
		return;
	}

	/* TTB */
	reg = 0xFFFFC000 & mmu_info.saddr;
	__asm__ __volatile__("mcr p15, 0, %0, c2, c0, 0" ::"r" (reg));

	/* No permission check on all domains */
	reg = 0xFFFFFFFF;
	__asm__ __volatile__("mcr p15, 0, %0, c3, c0, 0" ::"r" (reg));

	/* Invalidate I/D Cache */
	reg = 0;
	__asm__ __volatile__("mcr p15, 0, %0, c7, c7, 0" ::"r" (reg));

	/* Turn on the MMU translation */
	reg = read_p15_c1 ();
	cp_delay ();
	write_p15_c1 (reg | C1_MMU);

	arm926_dcache_enable();
	icache_enable();
}

void mmu_disable(void)
{
	/* ARM926EJS = 4way, 8 word line length,
	   S5H2150 = 16KB DC */

	if (!mmu_status()) {
		printf ("MMU is disabled already..\n");
		return;
	}

	__asm__ __volatile__ (
		"mov	r0, #0"				"\n"

		/* Clean all dcache line without dirty bit test */
		"clean_loop1:"				"\n"
		"mrc	p15, 0, r15, c7, c14, 3"	"\n"
		"bne	clean_loop1"			"\n"
		
		/* Invalidate icache */
		"mcrne	p15, 0, r0, c7, c5, 0"		"\n"
		
		/* drain write buffer */
		"mcrne	p15, 0, r0, c7, c10, 4"		"\n"

		/* turn off mmu/dcache */
		"mrc	p15, 0, r0, c1, c0, 0"		"\n"
		"and	r0, r0, #0xFFFFFFFA"		"\n"
		"mcr	p15, 0, r0, c1, c0, 0"		"\n"
	
		/* 2 nops are nedded if VA != PA */
		"nop"					"\n"
		"nop"					"\n"
		"nop"					"\n"

		/* init to zero TAR, Domain conrol bits */
		"mov	r0, #0"				"\n"
		"mcr	p15, 0, r0, c2, c0, 0"		"\n"
		"mcr	p15, 0, r0, c3, c0, 0"		"\n"
	:::"r0","r1");

	arm926_dcache_disable();
}

static void arm926_dcache_enable(void)
/* dcache enable */
{
	ulong reg;
	reg = read_p15_c1();
	cp_delay();
	write_p15_c1(reg | C1_DC);
}

/* dcache disable */
static void arm926_dcache_disable(void)
{
	ulong reg;
	reg = read_p15_c1();
	cp_delay();
	write_p15_c1(reg & ~(C1_DC));
}

#if 0
void dcache_invalidate(unsigned int *saddr)
{
	volatile unsigned int reg = (volatile unsigned int)saddr;
	asm ("mcr p15, 0, %0, c7, c6, 1"::"r"(reg));
}

void dcache_flush(unsigned int saddr, unsigned int size)
{
	volatile unsigned int reg = 0;
	asm ("mcr p15, 0, %0, c7, c6, 0"::"r"(reg));
}
#endif

/* 070808 ij.jang */
static unsigned char mmu_table[16 *1024] __attribute__((aligned(16384)));

/* returns ttb address */
unsigned int create_mmu_mapping(struct mmu_mapinfo* mmu_fmap, int n)
{
	int i;

	/* first-level table */
#if 0
	mmu_info.saddr = (unsigned int)CFG_MMU_PTADDR;
	mmu_info.size = 16 * 1024;
#else
	mmu_info.saddr = (unsigned int)mmu_table;
	mmu_info.size = sizeof(mmu_table);
#endif

	/* initialize the first-level table */
	memset ((void*)mmu_info.saddr, 0, mmu_info.size);

	for (i=0; i<n; i++ ) {
		 if (create_mmu_fl (&mmu_info, &mmu_fmap[i]))
			 return -1;
	}
	return mmu_info.saddr;
}

static int on_off (const char *s)
{
	if (strcmp(s, "on") == 0) {
		return (1);
	} else if (strcmp(s, "off") == 0) {
		return (0);
	}
	return (-1);
}

int do_mmu (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if (argc>1) {
		switch (on_off (argv[1])) {
		case 1:
			if (!mmu_status()) 
				mmu_enable ();
			break;
		case 0:
			if (mmu_status()) 
				mmu_disable();
			break;
		default:
			return -1;
		}
	}

	printf ("MMU is %s\n",
		mmu_status() ? "ON" : "OFF");

	return 0;
}

U_BOOT_CMD(
	mmu,   2,   1,     do_mmu,
	"mmu  - enable or disable mmu\n",
	"[on, off]\n"
	"    - enable or disable mmu memory translation\n"
);

#endif

