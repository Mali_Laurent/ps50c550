#include <common.h>
#include <asm/hardware.h>
#if defined(CONFIG_SDP76)

DECLARE_GLOBAL_DATA_PTR;

#ifdef CONFIG_PCI

#include <pci.h>

#define PCI_MEM_ADDR				0x20000000
#define PCI_IO_ADDR 				0x00000000
#define PCI_MEM_BAR0				0x20000000
#define PCI_MEM_BAR1				0
#define PCI_MEM_BAR2				0
#define PCI_MEM_SIZE				0x10000000
#define PCI_IO_SIZE					0x02000000
#define AHB_PCI_MEM_BASE			0x40000000
#define AHB_PCI_CONFIG_TYPE0_BASE	0x50000000
#define AHB_PCI_CONFIG_TYPE1_BASE	0x54000000
#define AHB_PCI_IO_BASE				0x5c000000

#define  AHB_ADDR_PCI_CFG0( device, functn, offset )			\
			(AHB_PCI_CONFIG_TYPE0_BASE | ((device)<<11) | \
			((	  (functn)	  &0x07)<< 8) | \
			((	  (offset)	  &0xFF)	)	)
#define  AHB_ADDR_PCI_CFG1( bus, device, functn, offset )		\
			(AHB_PCI_CONFIG_TYPE1_BASE | (((bus)&0xFF)<<16) | \
			((	  (device)	  &0x1F)<<11) |\
			((	  (functn)	  &0x07)<< 8) | \
			((	  (offset)	  &0xFF)	)	)

static struct pci_config_table s4lx_pci_config_table[] = {
	{ PCI_ANY_ID, PCI_ANY_ID, PCI_ANY_ID,
		PCI_ANY_ID, PCI_ANY_ID, PCI_ANY_ID,
	  	pci_cfgfunc_config_device,
	  	{AHB_PCI_IO_BASE, AHB_PCI_MEM_BASE,
		PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER}
	},
	{},
};

extern void pciauto_config_init(struct pci_controller *hose);

#define mb() __asm__ __volatile__ ("" : : : "memory")

static unsigned long s4lx_open_config_window(int bus, pci_dev_t dev, int offset)
{
	if(bus == 0)
		return AHB_ADDR_PCI_CFG0(PCI_DEV(dev), PCI_FUNC(dev), offset);
	return AHB_ADDR_PCI_CFG1(PCI_BUS(dev), PCI_DEV(dev), PCI_FUNC(dev), offset);
}

static void s4lx_close_config_window(void)
{
	mb();
}

static int s4lx_read_byte (struct pci_controller *hose, pci_dev_t dev,int offset, u8 *val)
{
	*val=*(volatile u8 *)s4lx_open_config_window(hose->current_busno,dev,offset);
	s4lx_close_config_window();
	return 0;
}

static int s4lx_read_word (struct pci_controller *hose, pci_dev_t dev,int offset, u16 *val)
{
	*val=*(volatile u16 *)s4lx_open_config_window(hose->current_busno,dev,offset);
	s4lx_close_config_window();
	return 0;
}

static int s4lx_read_dword (struct pci_controller *hose, pci_dev_t dev,int offset, u32 *val)
{
	*val=*(volatile u32 *)s4lx_open_config_window(hose->current_busno,dev,offset);
	s4lx_close_config_window();
	return 0;
}

static int s4lx_write_byte (struct pci_controller *hose,pci_dev_t dev, int offset,u8 val)
{
	*(volatile u8 *)s4lx_open_config_window(hose->current_busno,dev,offset)=val;
	s4lx_close_config_window();
	return 0;
}

static int s4lx_write_word (struct pci_controller *hose,pci_dev_t dev, int offset,u16 val)
{
	*(volatile u16 *)s4lx_open_config_window(hose->current_busno,dev,offset)=val;
	s4lx_close_config_window();
	return 0;
}

static int s4lx_write_dword (struct pci_controller *hose,pci_dev_t dev, int offset,u32 val)
{
	*(volatile u32 *)s4lx_open_config_window(hose->current_busno,dev,offset)=val;
	s4lx_close_config_window();
	return 0;
}


/******************************
 * PCI initialisation
 ******************************/

struct pci_controller s4lx_hose = {
#if 0
#ifndef CONFIG_PCI_PNP
	config_table: s4lx_pci_config_table,
#endif
#endif
};

#define barrier()

void pci_init_board (void)
{
	struct pci_controller *hose = &s4lx_hose;
	S4LX_PCI *pci;
	S4LX_CLOCK_POWER *clk;
	unsigned int reg_val;

	pci=S4LX_GetBase_PCI();
	clk=S4LX_GetBase_CLOCK_POWER();

//	R_USB_PCI_SET	|= 1;
	clk->USB_PCI_SET|=1;
	udelay(20 * 1000);

	pci->PCIINTEN=0x00000000;
	pci->PCICON=0x00000013;
	
	udelay (1000 * 10);
	pci->PCIDIAG0&= 0xfffffff0;

#ifdef CFG_PCI_EXT_CLOCK
	pci->PCIDIAG0|=0x1<<2;
#endif
		
#ifdef CFG_PCI_EXT_RESET
	pci->PCIDIAG0|= (0x1<<1);
#endif
	pci->PCIDIAG0&=0xfffffff0;

	reg_val=pci->PCIRCC;
	reg_val|=3;
#ifdef CFG_PCI_CLK_66
	reg_val&=0xfffffff7;
#else
	reg_val|=1<<3;
#endif
	pci->PCIRCC = reg_val;

	pci->PCIBATAPM	= 0x0;
	pci->PCIBATAPI 	= 0x00000000;
	pci->PCISET 	= 0x00000400;
	pci->PCIBAM0   	= 0xF0000000;
	pci->PCIBATPA0	= PCI_MEM_BAR0;
	pci->PCIHBAR0	= PCI_MEM_BAR0;

//	R_PCIBATPA0	= PCI_MEM_BAR0;
//	R_PCIBAM0	= ~((unsigned int)PCI_MEM_BAR0)+1;

//	R_PCIHBAR0	= 0x4000;

	pci->PCIHBAR1	= PCI_MEM_BAR1;
//	barrier();
	/* Base Address Translation */
	pci->PCIBATPA1	= PCI_MEM_BAR1;
	/* Base Address Mask */
	pci->PCIBAM1	= 0x0;
	/* I/O Base Address 2 from PCI to AHB */
	/* disable */
	/* location & size */
	pci->PCIHBAR2	=0x30030100;
	/* Base Address Translation */
	pci->PCIBATPA2	= 0x30030100;
	/* Base Address Mask */
	pci->PCIBAM2	= 0x0;
	pci->PCIHLINE	= 0x00001008;
	pci->PCIHLTIT	&= 0xffff0000;
	/* Int Line:26(0x1a), Int Pin:INTA# */
	pci->PCIHLTIT	|= 0x0000011a;
	pci->PCIHTIMER	= 0x00008080;
	/* I/O, Memory, Bus Master, ..., SERR Enable and Clear Status */
	pci->PCIHSC 	= 0xffff0357;
	pci->PCICON 	= 0x00000313;

	udelay (10*1000);	// delay 10ms
	/*
	 * Register the hose
	 */
	hose->first_busno = 0;
	hose->region_count = 2;	

	pci_set_region (hose->regions + 0, 0x4000, AHB_PCI_MEM_BASE+0x4000,
			0x10000000, PCI_REGION_MEM);
	pci_set_region (hose->regions + 1, 0x600, AHB_PCI_IO_BASE+0x600,
			0x00080000, PCI_REGION_IO);
	pci_set_ops (hose,s4lx_read_byte,s4lx_read_word,
			s4lx_read_dword, s4lx_write_byte,
			s4lx_write_word,s4lx_write_dword);
	pci_register_hose (hose);

	hose->last_busno = pci_hose_scan (hose);


}
#endif /* CONFIG_PCI */

#endif /* CONFIG_S4LJ003X */
