/**
 */
/**
 */
/**
 * Don't use ip (=r13)
 */
#if 0
#include <config.h>

.globl cpu_misc_init

#ifdef CONFIG_DEBUG_UART
.globl uart_debug_init
.globl uart_debug_putc
#endif

#define P_WDT_BASE	0x30090600
#define O_WTPSV		0x0
#define O_WTCON		0x4
#define O_WTCNT		0x8

#define P_UART_BASE	0x30090a80
#define O_ULCON		0x0
#define O_UCON		0x4
#define O_UFCON		0x8
#define O_UMCON		0xC
#define O_UTRSTAT	0x10
#define O_UERSTAT	0x14
#define O_UFSTAT	0x18
#define O_UMSTAT	0x1c
#define O_UTXH		0x20
#define O_URXH		0x24
#define O_UBRDIV	0x28
	.macro nop4
	nop
	nop
	nop
	nop
	.endm

/***********************************************************************
 * cpu_misc_init
 ***********************************************************************/
cpu_misc_init:
	// watch dog disable
	ldr     r0, =P_WDT_BASE
	mov     r1, #0xA5
	str     r1, [r0, #O_WTCON]

	// mask all IRQs
	ldr	r0, =P_INT_BASE
	mvn	r1, #0x0	@ 0xFFFFFFFF
	str	r1, [r0, #O_INTMSK]

	mov	pc, lr

#ifdef CONFIG_DEBUG_UART
/***********************************************************************
 * init_uart_debug : init uart cont for debugging
 ***********************************************************************/
uart_debug_init:
	ldr		r0, =P_UART_BASE	/* uart 2 base addr */
	
	mov		r2, #0x03
	str		r2, [r0, #O_ULCON]

	mov		r2, #0x05
	str		r2, [r0, #O_UCON]

	mov		r2, #0x90
	str		r2, [r0, #O_UFCON]

	mov		r2, #0x5
	str		r2, [r0, #O_UMCON]

	mov		r2, #0x11
	str		r2, [r0, #O_UBRDIV]

	mov		pc, lr

uart_debug_putc:
	/* param r0 : ascii value to send */
	ldr		r1, =P_UART_BASE
	str		r0, [r1, #O_UTXH]

	mov		pc, lr

#endif

#endif
