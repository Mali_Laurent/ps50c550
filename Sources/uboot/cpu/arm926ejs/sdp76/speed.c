/*
 * (C) Copyright 2001-2004
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * (C) Copyright 2002
 * David Mueller, ELSOFT AG, d.mueller@elsoft.ch
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/* This code should work for both the S3C2400 and the S3C2410
 * as they seem to have the same PLL and clock machinery inside.
 * The different address mapping is handled by the s3c24xx.h files below.
 */

#include <common.h>
#include <command.h>

#if defined(CONFIG_SDP76)

#if defined(CONFIG_SDP76)
#include <asm/arch/sdp76.h>
#endif

#define GET_ARM_PLL_P(x)			((x>>3)&0x3F)
#define GET_ARM_PLL_M(x)			((x>>9)&0x1FF)
#define GET_ARM_PLL_S(x)			((x>>0)&0x7)

#define GET_DDR_PLL_P(x)			((x>>3)& 0x3F)
#define GET_DDR_PLL_M(x)			((x>>9) & 0x1FF)
#define GET_DDR_PLL_S(x)			((x>>0) & 0x7)


/* ------------------------------------------------------------------------- */
/* NOTE: This describes the proper use of this file.
 *
 * CONFIG_SYS_CLK_FREQ should be defined as the input frequency of the PLL.
 *
 */
/* ------------------------------------------------------------------------- */

static ulong get_ARM_PLLCLK(void)
{
	S4LX_CLOCK_POWER * const clk_power = S4LX_GetBase_CLOCK_POWER();
	ulong reg, m, p, s;
	reg=clk_power->ARMPLLCON;
	m=GET_ARM_PLL_M(reg);
	p=GET_ARM_PLL_P(reg);
	s=GET_ARM_PLL_S(reg);
#if 0
	return((CONFIG_SYS_CLK_FREQ * m) / (p << s));
#else
	reg = CONFIG_SYS_CLK_FREQ / (p<<s);
	reg *= m;
	return reg;
#endif
}

static ulong get_DDR_PLLCLK(void)
{
	S4LX_CLOCK_POWER * const clk_power = S4LX_GetBase_CLOCK_POWER();
	ulong reg, m, p, s;
	reg=clk_power->DDRPLLCON;
	m=GET_DDR_PLL_M(reg);
	p=GET_DDR_PLL_P(reg);
	s=GET_DDR_PLL_S(reg);
	return((CONFIG_SYS_CLK_FREQ * m) / (p << s));
}

/* return FCLK frequency */
ulong get_FCLK(void)
{
	S4LX_CLOCK_POWER * const clk_power = S4LX_GetBase_CLOCK_POWER();
	ulong pll,mod;
	pll=clk_power->FCLKCON&1?get_DDR_PLLCLK():get_ARM_PLLCLK();
	mod=(clk_power->CLKMOD&0xf)>>12;
	return pll>>mod;
}

ulong get_DDRCLK(void)
{
	S4LX_CLOCK_POWER * const clk_power = S4LX_GetBase_CLOCK_POWER();
	ulong mod;
	mod=(clk_power->CLKMOD&0xf)>>24;
	return get_DDR_PLLCLK()>>mod;
}

/* return HCLK frequency */
ulong get_HCLK(void)
{
	S4LX_CLOCK_POWER * const clk_power = S4LX_GetBase_CLOCK_POWER();
	ulong mod;
	mod=(clk_power->CLKMOD&0xf)>>4;
	return get_ARM_PLLCLK()>>mod;
}

/* return PCLK frequency */
ulong get_PCLK(void)
{
	S4LX_CLOCK_POWER * const clk_power = S4LX_GetBase_CLOCK_POWER();
	ulong mod;
	mod=(clk_power->CLKMOD&0xf)>>0;
	return get_ARM_PLLCLK()>>mod;
}

ulong get_BUSCLK(void)
{
	return get_DDR_PLLCLK()>>1;
}

ulong get_DISPLAYBUSCLK(void)
{
	S4LX_CLOCK_POWER * const clk_power = S4LX_GetBase_CLOCK_POWER();
	ulong mod;
	mod=(clk_power->CLKMOD&0xf)>>20;
	return get_DDR_PLLCLK()>>mod;
}

ulong get_IPCLK(void)
{
	S4LX_CLOCK_POWER * const clk_power = S4LX_GetBase_CLOCK_POWER();
	ulong mod;
	mod=(clk_power->CLKMOD&0xf)>>16;
	return get_DDR_PLLCLK()>>mod;
}

#endif /* CONFIG_S4LJ003X_FPGA */
