/**
 * u-boot-1.1.3 port for S5H2XXXX
 *
 * D-TV Development team, SYS.LSI
 * (C)Copyright 2007 Samsung Electronics, Inc.
 */
/**
 * cmd_ddrtest.c : DDR controller test
 * 
 * @author   : seongkoo.cheong
 * @see      : User's manual of each chip
 * @version  : $Revision$
 *
 * modification history
 * --------------------
 * 30, Apr, 2007 : seongkoo.cheong create.
 */

#include <common.h>
#include <command.h>

#if defined(CONFIG_S5H2150)
extern void ddrc_main(void);

extern unsigned int _ddrtest_start;
extern unsigned int _ddrtest_end;

int do_test_ddr(cmd_tbl_t *cmdtp, int flag, int argc, char* argv[])
{
	unsigned short *src,*dst;
	volatile unsigned int func = 
		(volatile unsigned int)_ddrtest_start;

#if defined(CFG_DDRTEST_DIRECT)
	func -= TEXT_BASE;

#elif defined(CFG_DDRTEST_COPY)
//	memcpy ((void*)CFG_DDRTEST_ADDR, _ddrtest_start, 
//			(_ddrtest_end - _ddrtest_start));
	/* set async read mode */
	*((volatile unsigned int *)0x30020040) = 0x0;
	*((volatile unsigned int *)0x3002004C) = 0x41;
	*((volatile unsigned short*)0x0001E442) = 0x40E0;

	for(src=CFG_DDRTEST_ADDR,dst=_ddrtest_start;src<CFG_DDRTEST_ADDR+(_ddrtest_end-_ddrtest_start);src++,dst++)
		*src=*dst;
		
	func = CFG_DDRTEST_ADDR;
#endif
	((void(*)(void))func)();
	for(;;);

	return 0;
}

U_BOOT_CMD(
	test_ddr,    1,    1,    do_test_ddr,
	"test_ddr	- run ddrc DQS value\n",
	"test_ddr	- run ddrc DQS value\n"
);

#endif	/* CONFIG_S5H2150 */

