/**
 * u-boot-1.1.3 port for S5H2110
 *
 * D-TV Development team, SYS.LSI
 * (C)Copyright 2005 Samsung Electronics, Inc.
 */
 /*
 * (C) Copyright 2002
 * Gary Jennejohn, DENX Software Engineering, <gj@denx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

 /**
 * interrupts.c : 16-bit timer
 * 
 * @author   : Ikjoon Jang (ij.jang@samsung.com)
 * @version  : $Revision$
 * @see      : S5H2110 User's manual
 *
 * modification history
 * --------------------
 * 30.July.2007 ij.jang : port to 1.1.6
 * 14/11/2005 : ij.jang modified from mercury.
 * 22/11/2005 : ij.jang fixed READ_TIMER function(0xFFFF problem)
 *
 */

#include <common.h>
#include <asm/arch/s5h2111.h>

/* speed.c */
extern ulong get_PCLK(void);

static ulong timestamp;
static ulong lastdec;

#define TIMER_LOAD_VAL	0x8000

/**
 * READ_TIMER()
 * 		: get timer data value
 * @author		ij.jang
 * @return		timer data value
 * @version		$Revison$
 */
ulong READ_TIMER(void)
{
#if 0
	volatile ulong val;
	val = 0x000FFFF & REG_GET(rTMCNT0);
	if( val > 0x2932 )
		val = 0;
	return val;
#endif
	return (0xFFFF & REG_GET(rTMCNT0));
}

/**
 * interrupt_init()
 * 		initialize the 16 bit timer
 *		PCLK = 33750000Hz -> timer freq = 1054687.5Hz
 * @author		ij.jang
 * @return		0 when success
 * @version		$Revison$
 */
int timer_init(void)
{
	/* Before init, make interrupt disabled and stop the timer */
	REG_PUT(rTMCON0, 0x00000000);
	/* no DMA request channel */
	REG_PUT(rTMDMASEL, 0x00);
	/* prescaler 7, period TIMER_LOAD_VAL */	
	REG_PUT(rTMDATA0, (0x7<<16) | (TIMER_LOAD_VAL & 0xFFFF));	
	
	timestamp = 0;
	lastdec = TIMER_LOAD_VAL;

	/* 1/4 Mux input, interrupt or DMA enable and start timer*/
	REG_PUT(rTMCON0, 0x3);

	return (0);
}

/**
 * reset_cpu()
 * 		: reset the cpu by setting up the watchdog timer 
 * @author		ij.jang
 * @version		$Revison$
 */
/* ij.jang 1.26.2006 : reset_cpu is in arm926ejs/start.S */
void reset_cpu(ulong ignored)
{
	// disable watchdog timer
	REG_PUT(rWTCON, 0x0000);

	// init the watchdog timer count
	REG_PUT(rWTCNT, 0x0001);

	// enable watchdog
	REG_PUT(rWTCON, 0x0021);

	// wait for the reset
	while(1);
	reset_cpu(0);
}

/**
 * reset_timer()
 * 		: reset the timer
 * @author		ij.jang
 * @version		$Revison$
 */
void reset_timer (void)
{
	reset_timer_masked ();
}

/**
 * get_timer()
 * 		: reset the timer
 * @author		ij.jang
 * @return		get elapsed time from 'base'
 * @param		base basetime
 * @version		$Revison$
 */
ulong get_timer (ulong base)
{
	return get_timer_masked () - base;
}

/**
 * set_timer()
 * 		: set the timestamp
 * @author		ij.jang
 * @param		t timestamp value
 * @version		$Revison$
 */
void set_timer (ulong t)
{
	timestamp = t;
}

/**
 * get_delay_time() : convert usec to timer value
 * @author		ij.jang
 * @return		converted timer value
 * @param		usec delay time(micro second)
 * @version		$Revison$
 */
inline ulong get_delay_time(const unsigned long usec)
{
	/* 1/1054687.5 nearly equals 1  */
#if 0
	ulong tmo;
	tmo = (TIMER_LOAD_VAL * usec) / 10000;
#endif
	return usec;
}

/**
 * udelay() : keep idle time with given usec
 * @author		ij.jang
 * @param		usec delay time(micro second)
 * @version		$Revison$
 */
void udelay (unsigned long usec)
{
	ulong tmo;
	
	tmo = get_delay_time(usec);
	tmo += get_timer (0);			/* from current time to +tmo */

	while (get_timer_masked () < tmo)
		/*NOP*/;
}

/**
 * udelay_masked() : reset the timer and keep idle time with given usec
 * @author		ij.jang
 * @param		usec delay time(micro second)
 * @version		$Revison$
 */
void udelay_masked (unsigned long usec)
{
	ulong tmo;

	tmo = get_delay_time(usec);	
	reset_timer_masked ();			/* from 0 to tmo */

	while (get_timer_masked () < tmo)
		/*NOP*/;
}

/**
 * reset_timer_masked()
 * 		: update lastdec, reset timestamp value
 * @author		ij.jang
 * @return		0 when success
 * @version		$Revison$
 */
void reset_timer_masked (void)
{
	lastdec = READ_TIMER();
	timestamp = 0;
}

/**
 * get_timer_masked()
 * 		: retrieve the timestamp value
 * @author		ij.jang
 * @return		timestamp value
 * @version		$Revison$
 */
ulong get_timer_masked (void)
{
	volatile ulong now = READ_TIMER();

	/* add elapsed time to timestamp */
	if( lastdec >= now )
		timestamp += lastdec - now;
	else
		// ij.jang 26.Feb.2006 : fixed the critical bug
		timestamp += (0xFFFF) & ((TIMER_LOAD_VAL - now) + lastdec);

	lastdec = now;

	return timestamp; 
}

/*
 * This function is derived from PowerPC code (read timebase as long long).
 * On ARM it just returns the timer value.
 */
unsigned long long get_ticks(void)
{
	return get_timer(0);
}

/*
 * This function is derived from PowerPC code (timebase clock frequency).
 * On ARM it returns the number of timer ticks per second.
 */
ulong get_tbclk (void)
{
	return 0;
}

