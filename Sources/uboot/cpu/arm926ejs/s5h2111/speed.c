/**
 * u-boot-1.1.3 port for S5H2111
 *
 * D-TV Development team, SYS.LSI
 * (C)Copyright 2005 Samsung Electronics, Inc.
 */
 
/**
 * speed.c : implements functions that returns the clock value
 * 
 * @author   : Ikjoon Jang (ij.jang@samsung.com)
 * @see      : S5H2111 User's manual
 * @version  : $Revision$
 *
 * modification history
 * --------------------
 * 30.July.2007 : ij.jang port to 1.1.6
 * 14, Nov, 2005 : ij.jang created.
 */

#include <common.h>
#include <asm/arch/s5h2111.h>

static ulong g_fclk = 0;

/**
 * get_FCLK()
 * 		: returns FCLK value
 * @author		ij.jang
 * @return		FCLK value(Mhz)
 * @version		$Revison$
 */
ulong get_FCLK(void)
{
	ulong r;
	ulong m, p;

	/* calculate only 1 time, and reuse */
	if( g_fclk )
		return g_fclk;

	/* get P/M value of FCLK */
	r = (REG_VAL(rPLLCON) & 0xFFFC);
	p = (r & 0xFC) >> 2;
	m = (r & 0xFF00) >> 8;

	/* calculate FCLK value */
	g_fclk = r = ( (m+8) * CONFIG_SYS_CLK_FREQ ) / (p+2); 

	return r;
}

/**
 * get_clkModValue()
 * 		: returns modulized clock value
 * @author		ij.jang
 * @return		clock value
 * @param		mod, [in]modulation value
 * @param		src, [in]source clock
 * @version		$Revison$
 */
inline ulong get_clkModValue(uchar mod)
{
	return get_FCLK() >> mod;
}

/**
 * get_BCLK()
 * 		: returns BCLK value (Bus)
 * @author		ij.jang
 * @return		BCLK value(Mhz)
 * @version		$Revison$
 */
ulong get_BCLK(void)
{
	uchar mod = (uchar)( (REG_VAL(rCLKMOD) >>20) & 0x0000000F );
	return get_clkModValue(mod); 
}

/**
 * get_HCLK()
 * 		: returns HCLK value (AHB)
 * @author		ij.jang
 * @return		HCLK value(Mhz)
 * @version		$Revison$
 */
ulong get_HCLK(void)
{
	uchar mod = (uchar)( (REG_VAL(rCLKMOD) >>4) & 0x0000000F );
	return get_clkModValue(mod);
}

/**
 * get_PCLK()
 * 		: returns PCLK value (APB)
 * @author		ij.jang
 * @return		HCLK value(Mhz)
 * @version		$Revison$
 */
ulong get_PCLK(void)
{
	uchar mod = (uchar)( REG_VAL(rCLKMOD) & 0x0000000F );
	return get_clkModValue(mod);
}

