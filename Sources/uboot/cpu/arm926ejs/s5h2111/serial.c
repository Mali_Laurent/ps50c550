/**
 * u-boot-1.1.3 port for S5H2110
 *
 * D-TV Development team, SYS.LSI
 * (C)Copyright 2005 Samsung Electronics, Inc.
 */
 
/*
 * (C) Copyright 2002
 * Gary Jennejohn, DENX Software Engineering, <gj@denx.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
 
/**
 * serial.S : basic UART driver for S5H2110, S5H2111
 * 			modified from u-boot smdk2410 port
 * @author   : Ikjoon Jang (ij.jang@samsung.com)
 * @version  : $Revision$
 * @see      : S5H2110 User's manual
 *
 * modification history
 * --------------------
 * 30.July.2007 ij.jang : port to 1.1.6
 * 04.May.2006 - ij.jang, remove the modem control support
 * 16.Feb.2006 - ij.jang, modify the UFCON initial value
 * 16.Nov.2005 - ij.jang, support FIFO buffer for Rx/Tx
 * 14.Nov.2005 - ij.jang, created.
 */

#include <config.h>
#include <common.h>
#include <asm/arch/s5h2111.h>

/* registers */
#if defined(CONFIG_SERIAL0)
#define	rULCON				rULCON0	
#define	rUCON				rUCON0
#define	rUFCON				rUFCON0
#define	rUTRSTAT			rUTRSTAT0
#define	rUERSTAT			rUERSTAT0
#define	rUFSTAT				rUFSTAT0
#define	rUTXH				rUTXH0
#define	rURXH				rURXH0
#define	rUBRDIV				rUBRDIV0

#elif defined(CONFIG_SERIAL1)
#define	rULCON				rULCON1
#define	rUCON				rUCON1
#define	rUFCON				rUFCON1
#define	rUTRSTAT			rUTRSTAT1
#define	rUERSTAT			rUERSTAT1
#define	rUFSTAT				rUFSTAT1
#define	rUTXH				rUTXH1
#define	rURXH				rURXH1
#define	rUBRDIV				rUBRDIV1

#elif defined(CONFIG_SERIAL2)
#define	rULCON				rULCON2
#define	rUCON				rUCON2
#define	rUFCON				rUFCON2
#define	rUTRSTAT			rUTRSTAT2
#define	rUERSTAT			rUERSTAT2
#define	rUFSTAT				rUFSTAT2
#define	rUTXH				rUTXH2
#define	rURXH				rURXH2
#define	rUBRDIV				rUBRDIV2
#endif

/* UART register initial values */
#define ULCON_INIT_VAL		(0x03)
#define UMCON_INIT_VAL		(0)
#define UCON_INIT_VAL		(0x05)

#ifdef CONFIG_USE_UART_FIFO
#define UFCON_INIT_VAL		(0x07)
#define POLLING_RX()		((REG_GET(rUFSTAT) & 0xF) > 0)
#define POLLING_TX()		((REG_GET(rUFSTAT) & (1<<9)) == 0 )
#else
#define UFCON_INIT_VAL		(0x0)
#define POLLING_RX()		((REG_GET(rUTRSTAT) & 0x1))
#define POLLING_TX()		((REG_GET(rUTRSTAT) & 0x4))
#endif

/* see speed.c */
extern unsigned long get_PCLK(void);

/**
 * serial_tstc()
 * 		: Test whether a character is in the RX buffer
 * @return		0 on no inputs, otherwise returns the count of inputs
 * @version    $Revison$
 */
int serial_tstc (void)
{
	return POLLING_RX();
}

/**
 * serial_setbrg()
 * 		: Initialize the UART controller
 * @author		ij.jang
 * @version		$Revison$
 */
void serial_setbrg(void)
{
	volatile int i;
	unsigned int reg = 0;

	reg = ((unsigned int)(get_PCLK() / (CONFIG_BAUDRATE * 16))) -1; 

	REG_PUT(rULCON, ULCON_INIT_VAL);
	REG_PUT(rUCON, UCON_INIT_VAL);
	REG_PUT(rUFCON, UFCON_INIT_VAL);
	REG_PUT(rUMCON0, UMCON_INIT_VAL);

	REG_PUT(rUBRDIV, reg);

	/* delay */
	for (i = 0; i < 100; i++)
	{
	}
}

/**
 * serial_init()
 * 		: Initialize the UART controller
 * @author		ij.jang
 * @return		Always return 0
 * @version		$Revison$
 */
int serial_init (void)
{
	serial_setbrg();
	return (0);
}

/**
 * serial_getc()
 * 		: Read a single byte from the serial port.
 * @author		ij.jang
 * @return		Returns 1 on success, 0 otherwise.
 * @version		$Revison$
 */
int serial_getc (void)
{
	while( !POLLING_RX() )
	{
	}
	return (REG_GET(rURXH) & 0xff);
}

/**
 * serial_putc()
 * 		: Output a single byte to the serial port.
 * @version    $Revison$
 */
void serial_putc (const char c)
{
	while( POLLING_TX() == 0 )
	{
	}
	REG_PUT(rUTXH, c);

	if( c == '\n' )
	{
		serial_putc('\r');
	}
}

/**
 * serial_puts()
 * 		: Output a string
 * @version    $Revison$
 * @param	s : [in]output string
 */
void serial_puts (const char *s)
{
	while (*s)
	{
		serial_putc(*s);
		s++;
	}
}

