/**
 * u-boot-1.1.3 port for s5h2xxx
 *
 * D-TV Development team, SYS.LSI
 * (C)Copyright 2007 Samsung Electronics, Inc.
 */
/**
 * lowlevel_init.S : Initialize board-specific functions.
 * 
 * @author   : ij.jang@samsing.com
 * @see      : ARM reference manual
 * @version  : $Revision$
 *
 * modification history
 * --------------------
 * 28.March.2007 : make common code for arm926ejs
 * ??.??.2007 : created.
 *
 */
/* all address translation: VA = PA,
   only 1MB/4KB paging methods are supported. */


#ifndef _ARM_MMU_H_
#define _ARM_MMU_H_

/* See also ARM Ref. Man. */
#define C1_MMU		(1<<0)		/* mmu off/on */
#define C1_ALIGN	(1<<1)		/* alignment faults off/on */
#define C1_DC		(1<<2)		/* dcache off/on */
#define C1_WB		(1<<3)		/* merging write buffer on/off */
#define C1_BIG_ENDIAN	(1<<7)	/* big endian off/on */
#define C1_SYS_PROT	(1<<8)		/* system protection */
#define C1_ROM_PROT	(1<<9)		/* ROM protection */
#define C1_IC		(1<<12)		/* icache off/on */
#define C1_HIGH_VECTORS	(1<<13)	/* location of vectors: low/high addresses */
#define RESERVED_1	(0xf << 3)	/* must be 111b for R/W */

/* ARM First-level descriptor flags */
#define FLD_C		(1<<3)
#define FLD_B		(1<<2)
#define FLD_FAULT	(0)
#define FLD_COARSE	(1)	/* supports only 4KB page size */
#define FLD_SECTION	(2)	/* 1MB page */
#define FLD_FINE	(3)	/* not support */
#define FLD_TMASK	(3)
#define FLD_ADDRMASK	(0xFFF00000)

/* ARM second-level descriptor flags */
#define SLD_C		(1<<3)
#define SLD_B		(1<<2)
#define SLD_FAULT	(0)
#define SLD_LARGE	(1)	/* not support */
#define SLD_SMALL	(2)
#define SLD_TINY	(3)	/* not support */
#define SLD_TMASK	(3)
#define SLD_CBMASK	(0xC)
#define SLD_ADDRMASK	(0xFFFFF000)

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(X) (sizeof(X)/sizeof(X[0]))
#endif

struct mmu_mapinfo {
	unsigned int saddr;		/* start address */
	unsigned int size;		/* end address */
	unsigned int flags;		/* descriptor flags */
	struct mmu_mapinfo *tbl;	/* next-level mapping */
	int tbl_cnt;			/* next-level mapinfo count */
};

#endif

