/*
 * linux/include/asm-arm/arch-ssdtv/ssa1_fpga.h
 *
 *  Copyright (C) 2003-2006 Samsung Electronics
 *  Author: tukho.kim@samsung.com
 *
 */

#ifndef __SSA1_FPGA_H
#define __SSA1_FPGA_H

#if 0
#include "bitfield.h"
#else
/* macros for C */
#define	REG_PTR(ADDR)			((volatile unsigned	int*)ADDR)
#define	REG_VAL(ADDR) 			(*((volatile unsigned int*)ADDR))
#define REG_PUT(ADDR, DATA)		ADDR = DATA
#define REG_GET(ADDR)			ADDR
#endif

/*
 * SSA1_FPGA internal registers
 *
 * Be careful !!!!
 * PAGE_OFFSET + VMALLOC_END = 0xD0000000
 *
 */
#define VA_IO_BASE0           (0x30010000)
#define PA_IO_BASE0           (0x30010000)

#define PA_SMC_BASE           (0x30020000)
#define VA_SMC_BASE           (VA_IO_BASE0 - PA_IO_BASE0 + PA_SMC_BASE)

#define PA_TIMER_BASE         (0x30090400)
#define VA_TIMER_BASE         (VA_IO_BASE0 - PA_IO_BASE0 + PA_TIMER_BASE)
/*
#define PA_RTC_BASE           (0x30090500)
#define VA_RTC_BASE           (VA_IO_BASE0 - PA_IO_BASE0 + PA_RTC_BASE)
*/
#define PA_WDT_BASE           (0x30090600)
#define VA_WDT_BASE           (VA_IO_BASE0 - PA_IO_BASE0 + PA_WDT_BASE)

#define PA_PLL_BASE           (0x30090800)
#define VA_PLL_BASE           (VA_IO_BASE0 - PA_IO_BASE0 + PA_PLL_BASE)

#define PA_UART_BASE          (0x30090A00)
#define VA_UART_BASE          (VA_IO_BASE0 - PA_IO_BASE0 + PA_UART_BASE)

#define PA_GPIO_BASE          (0x30090C00)
#define VA_GPIO_BASE          (VA_IO_BASE0 - PA_IO_BASE0 + PA_GPIO_BASE)

#define PA_INT_BASE           (0x30090F00)
#define VA_INT_BASE           (VA_IO_BASE0 - PA_IO_BASE0 + PA_INT_BASE)

/* EMC Register */
#define VA_SMC(offset)  (*(volatile unsigned *)(VA_SMC_BASE+(offset)))


/* interrupt */
#define VA_INT(offset)  (*(volatile unsigned *)(VA_INT_BASE+(offset)))

#define rINTCON		VA_INT(0x00)
#define rINTPND		VA_INT(0x04)
#define rINTMOD		VA_INT(0x08)
#define rINTMSK		VA_INT(0x0C)
#define rLEVEL		VA_INT(0x10)
#define rI_PSLV0	VA_INT(0x14)
#define rI_PSLV1 	VA_INT(0x18)
#define rI_PSLV2	VA_INT(0x1C)
#define rI_PSLV3 	VA_INT(0x20)
#define rI_PMST 	VA_INT(0x24)
#define rI_CSLV0 	VA_INT(0x28)
#define rI_CSLV1	VA_INT(0x2C)
#define rI_CSLV2 	VA_INT(0x30)
#define rI_CSLV3	VA_INT(0x34)
#define rI_CMST 	VA_INT(0x38)
#define rI_ISPR 	VA_INT(0x3C)
#define rI_ISPC		VA_INT(0x40)
#define rF_PSLV0 	VA_INT(0x44)
#define rF_PSLV1 	VA_INT(0x48)
#define rF_PSLV2  	VA_INT(0x4C)
#define rF_PSLV3 	VA_INT(0x50)
#define rF_PMST 	VA_INT(0x54)
#define rF_CSLV0 	VA_INT(0x58)
#define rF_CSLV1 	VA_INT(0x5C)
#define rF_CSLV2 	VA_INT(0x60)
#define rF_CSLV3 	VA_INT(0x64)
#define rF_CMST  	VA_INT(0x68)
#define rF_ISPR 	VA_INT(0x6C)
#define rF_ISPC 	VA_INT(0x70)
#define rPOLARITY 	VA_INT(0x74)
#define rI_VECADDR 	VA_INT(0x78)
#define rF_VECADDR 	VA_INT(0x7C)
#define rTIC_QSRC	VA_INT(0x80)
#define rTIC_FIRQ	VA_INT(0x84)
/*                  0x88 reserved */
#define rTIC_TESTEN	VA_INT(0x8C)
#define rSRCPND		VA_INT(0x90)
#define rSUBINT		VA_INT(0x94)
#define rINTSRCSEL0	VA_INT(0x98)
#define rINTSRCSEL1	VA_INT(0x9C)
#define rINTSRCSEL2	VA_INT(0xA0)
#define rINTSRCSEL3	VA_INT(0xA4)
#define rINTSRCSEL4	VA_INT(0xA8)
#define rINTSRCSEL5	VA_INT(0xAC)
#define rINTSRCSEL6	VA_INT(0xB0)
#define rINTSRCSEL7	VA_INT(0xB4)

/* clock & power management */
#define VA_PLL(offset)    (*(volatile unsigned *)(VA_PLL_BASE+(offset)))

#define rPLLCON		VA_PLL(0x00) /* PLL Control */
#define rMODCON		VA_PLL(0x04) 
#define rFCLKCON	VA_PLL(0x08)
#define rHCLKCON	VA_PLL(0x0c) 
#define rPCLKCON	VA_PLL(0x10) 
#define rCLKMOD		VA_PLL(0x14)
#define rLDTCNT		VA_PLL(0x18)
#define rRSETCON	VA_PLL(0x1c)
/* reserved */
#define rRESPTIM	VA_PLL(0x24)
/* reserved */
#define rSOFTRES	VA_PLL(0x2c)
#define rECLKCON	VA_PLL(0x30)

/* UART */
#define VA_UART(dt,  n, offset)	\
    (*(volatile dt *)(VA_UART_BASE+(0x40*n)+(offset)))

#define VA_UART_0(offset)	VA_UART(unsigned, 0, offset)
#define VA_UART_1(offset)	VA_UART(unsigned, 1, offset)
#define VA_UART_2(offset)	VA_UART(unsigned, 2, offset)

#define VA_UART_0_B(offset)	VA_UART(char, 0, offset)
#define VA_UART_1_B(offset)	VA_UART(char, 1, offset)
#define VA_UART_2_B(offset)	VA_UART(char, 2, offset)

#define rULCON0     VA_UART_0(0x00) /* UART 0 Line control */
#define rUCON0      VA_UART_0(0x04) /* UART 0 Control */
#define rUFCON0     VA_UART_0(0x08) /* UART 0 FIFO control */
#define rUMCON0     VA_UART_0(0x0c) /* UART 0 Modem control */
#define rUTRSTAT0   VA_UART_0(0x10) /* UART 0 Tx/Rx status */
#define rUERSTAT0   VA_UART_0(0x14) /* UART 0 Rx error status */
#define rUFSTAT0    VA_UART_0(0x18) /* UART 0 FIFO status */
#define rUMSTAT0    VA_UART_0(0x1c) /* UART 0 Modem status */
#define rUBRDIV0    VA_UART_0(0x28) /* UART 0 Baud rate divisor */

#define rULCON1     VA_UART_1(0x00) /* UART 1 Line control */
#define rUCON1      VA_UART_1(0x04) /* UART 1 Control */
#define rUFCON1     VA_UART_1(0x08) /* UART 1 FIFO control */
#define rUMCON1     VA_UART_1(0x0c) /* UART 1 Modem control */
#define rUTRSTAT1   VA_UART_1(0x10) /* UART 1 Tx/Rx status */
#define rUERSTAT1   VA_UART_1(0x14) /* UART 1 Rx error status */
#define rUFSTAT1    VA_UART_1(0x18) /* UART 1 FIFO status */
#define rUMSTAT1    VA_UART_1(0x1c) /* UART 1 Modem status */
#define rUBRDIV1    VA_UART_1(0x28) /* UART 1 Baud rate divisor */

#define rULCON2     VA_UART_2(0x00) /* UART 2 Line control */
#define rUCON2      VA_UART_2(0x04) /* UART 2 Control */
#define rUFCON2     VA_UART_2(0x08) /* UART 2 FIFO control */
#define rUMCON2     VA_UART_2(0x0c) /* UART 2 Modem control */
#define rUTRSTAT2   VA_UART_2(0x10) /* UART 2 Tx/Rx status */
#define rUERSTAT2   VA_UART_2(0x14) /* UART 2 Rx error status */
#define rUFSTAT2    VA_UART_2(0x18) /* UART 2 FIFO status */
#define rUMSTAT2    VA_UART_2(0x1c) /* UART 2 Modem status */
#define rUBRDIV2    VA_UART_2(0x28) /* UART 2 Baud rate divisor */

#ifdef __BIG_ENDIAN
#define rUTXH0  VA_UART_0_B(0x23) /* UART 0 Transmission Hold */
#define rURXH0  VA_UART_0_B(0x27) /* UART 0 Receive buffer */
#else /* Little Endian */
#define rUTXH0	VA_UART_0_B(0x20) /* UART 0 Transmission Hold */
#define rURXH0	VA_UART_0_B(0x24) /* UART 0 Receive buffer */
#endif

#ifdef __BIG_ENDIAN
#define rUTXH1  VA_UART_1_B(0x23) /* UART 1 Transmission Hold */
#define rURXH1  VA_UART_1_B(0x27) /* UART 1 Receive buffer */
#else /* Little Endian */
#define rUTXH1	VA_UART_1_B(0x20) /* UART 1 Transmission Hold */
#define rURXH1	VA_UART_1_B(0x24) /* UART 1 Receive buffer */
#endif

#ifdef __BIG_ENDIAN
#define rUTXH2  VA_UART_2_B(0x23) /* UART 2 Transmission Hold */
#define rURXH2  VA_UART_2_B(0x27) /* UART 2 Receive buffer */
#else /* Little Endian */
#define rUTXH2	VA_UART_2_B(0x20) /* UART 2 Transmission Hold */
#define rURXH2	VA_UART_2_B(0x24) /* UART 2 Receive buffer */
#endif

/* timer */
#define VA_TIMER(t, offset)	\
    (*(volatile unsigned *)(VA_TIMER_BASE+(0x10*t)+(offset)))

#define rTMDMASEL	VA_TIMER(0, 0x0c) /* dma or interrupt mode selection */

#define VA_TIMER_0(offset)	VA_TIMER(0, offset)
#define VA_TIMER_1(offset)	VA_TIMER(1, offset)
#define VA_TIMER_2(offset)	VA_TIMER(2, offset)

#define rTMCON0		VA_TIMER_0(0x00)
#define rTMDATA0	VA_TIMER_0(0x04)
#define rTMCNT0		VA_TIMER_0(0x08)

#define rTMCON1		VA_TIMER_1(0x00)
#define rTMDATA1	VA_TIMER_1(0x04)
#define rTMCNT1		VA_TIMER_1(0x08)

#define rTMCON2		VA_TIMER_2(0x00)
#define rTMDATA2	VA_TIMER_2(0x04)
#define rTMCNT2		VA_TIMER_2(0x08)

#define rTMSTATUS	VA_TIMER_0(0x30)

#define TMCON_MUX04	(0x0 << 2)
#define TMCON_MUX08	(0x1 << 2)
#define TMCON_MUX16	(0x2 << 2)
#define TMCON_MUX32	(0x3 << 2)

#define TMCON_INT_DMA_EN	(0x1 << 1)
#define TMCON_RUN	(0x1)

#define TMDATA_PRES(x)	(x > 0) ? ((x & 0xFF) << 16) : 1

/* watch dog timer */
#define VA_WDT(offset)    (*(volatile unsigned *)(VA_WDT_BASE+(offset)))

#define WTPSV		VA_WDT(0x00) /* Watch-dog timer prescaler value */
#define WTCON		VA_WDT(0x04) /* Watch-dog timer mode */
#define WTCNT		VA_WDT(0x08) /* Eatch-dog timer count */

/*-----------------------------------------------------*/
/* GPIO ports */
#define VA_IO_PORT(offset)  (*(volatile unsigned *)(VA_GPIO_BASE+(offset)))

#define	rGPIOSEL0		VA_IO_PORT(0x00)
#define	rEXTINTMSK		VA_IO_PORT(0x04) 
#define	rGPIOA_DAT		VA_IO_PORT(0x08)
#define	rGPIOA_CON		VA_IO_PORT(0x0C) 
#define	rGPIOB_DAT		VA_IO_PORT(0x10) 
#define	rGPIOB_CON		VA_IO_PORT(0x14) 
#define	rGPIOC_DAT		VA_IO_PORT(0x18) 
#define	rGPIOC_CON		VA_IO_PORT(0x1C) 
#define	rGPIOD_DAT		VA_IO_PORT(0x20) 
#define	rGPIOD_CON		VA_IO_PORT(0x24)
#define	rGPIOE_DAT		VA_IO_PORT(0x28) 
#define	rGPIOE_CON		VA_IO_PORT(0x2C) 
#define	rGPIOF_DAT		VA_IO_PORT(0x30) 
#define	rGPIOF_CON		VA_IO_PORT(0x34)
#define	rGPIOG_DAT		VA_IO_PORT(0x38) 
#define	rGPIOG_CON		VA_IO_PORT(0x3C) 
#define	rEXTINTRCON		VA_IO_PORT(0x40) /*	External interrupt control register	*/
#define	rPINSELCON		VA_IO_PORT(0x44) 

/* RTC */
#ifdef __BIG_ENDIAN
#define VA_RTC(offset)  (*(volatile char *)(VA_RTC_BASE+(offset)+3))
#else
#define VA_RTC(offset)  (*(volatile char *)(VA_RTC_BASE+(offset)))
#endif

#define RTCCON    VA_RTC(0x00) /* RTC control */
#define RTCALM    VA_RTC(0x04) /* RTC alarm control */
#define ALMSEC    VA_RTC(0x08) /* Alarm second */
#define ALMMIN    VA_RTC(0x0c) /* Alarm minute */
#define ALMHOUR   VA_RTC(0x10) /* Alarm Hour */
#define ALMDATE   VA_RTC(0x14) /* Alarm day */
#define ALMMON    VA_RTC(0x18) /* Alarm month */
#define ALMYEAR   VA_RTC(0x1c) /* Alarm year */
#define BCDSEC    VA_RTC(0x20) /* BCD second */
#define BCDMIN    VA_RTC(0x24) /* BCD minute */
#define BCDHOUR   VA_RTC(0x28) /* BCD hour */
#define BCDDATE   VA_RTC(0x2c) /* BCD day */
#define BCDDDAY   VA_RTC(0x30) /* BCD date */
#define BCDMON    VA_RTC(0x34) /* BCD month */
#define BCDYEAR   VA_RTC(0x38) /* BCD year */
#define TICNT     VA_RTC(0x40) /* Tick time count */
#define RTCRST	  VA_RTC(0x44) /* RTC round reset */
#define RTCSTA    VA_RTC(0x48) /* RTC status */

/* Field */
#define fRTC_SEC                Fld(7,0)
#define fRTC_MIN                Fld(7,0)
#define fRTC_HOUR               Fld(6,0)
#define fRTC_DATE               Fld(6,0)
#define fRTC_DAY                Fld(3,0)
#define fRTC_MON                Fld(5,0)
#define fRTC_YEAR               Fld(8,0)
/* Mask */
#define Msk_RTCSEC              FMsk(fRTC_SEC)
#define Msk_RTCMIN              FMsk(fRTC_MIN)
#define Msk_RTCHOUR             FMsk(fRTC_HOUR)
#define Msk_RTCDAY              FMsk(fRTC_DAY)
#define Msk_RTCDATE             FMsk(fRTC_DATE)
#define Msk_RTCMON              FMsk(fRTC_MON)
#define Msk_RTCYEAR             FMsk(fRTC_YEAR)
/* bits */
#define RTCCON_EN               (1 << 0) /* RTC Control Enable */
#define RTCCON_CLKSEL           (1 << 1) /* BCD clock as XTAL 1/2^25 clock */
#define RTCCON_CNTSEL           (1 << 2) /* 0: Merge BCD counters */
#define RTCCON_CLKRST           (1 << 3) /* RTC clock count reset */

/* Round Reset Register */
#define RTCRST_SRSTEN           (1<<3)          /* Enable Round Reset   -one shot */
#define RTCRST_SECCR_30         (3)             /* Round Boundary       - 30 sec */
#define RTCRST_SECCR_40         (4)             /* Round Boundary       - 40 sec */
#define RTCRST_SECCR_50         (5)             /* Round Boundary       - 50 sec */
#define RTCRST_SECCR_ROUNDOFF   (0)             /* Just resets the seconds. */

/* Tick Time count register */
#define RTCALM_GLOBAL           (1 << 6) /* Global alarm enable */
#define RTCALM_YEAR                     (1 << 5) /* Year alarm enable */
#define RTCALM_MON                      (1 << 4) /* Month alarm enable */
#define RTCALM_DAY                      (1 << 3) /* Day alarm enable */
#define RTCALM_HOUR                     (1 << 2) /* Hour alarm enable */
#define RTCALM_MIN                      (1 << 1) /* Minute alarm enable */
#define RTCALM_SEC                      (1 << 0) /* Second alarm enable */
#define RTCALM_EN                       (RTCALM_GLOBAL | RTCALM_YEAR | RTCALM_MON |\
										RTCALM_DAY | RTCALM_HOUR | RTCALM_MIN |\
										RTCALM_SEC)
#define RTCALM_DIS                      (~RTCALM_EN)

#if 0
/* Linux clock api definition */ 
// PLLCON define
#define GET_MDIV(val)	((val >> 8) & 0xFF)
#define GET_PDIV(val)	((val >> 2) & 0x3F)

// HCLK Clock define
#define CLK_SMC		2
#define CLK_USB		3

// PCKL Clock define 
#define CLK_I2C		0
#define CLK_SSP		1
#define	CLK_IRR		2
#define CLK_TIMER	3
#define CLK_RTC		4
#define CLK_WDT		5
#define CLK_IDLE	6
#define CLK_UART	7
#define CLK_INTC	8
#define CLK_GPIO	9
#define CLK_AIOCKG	10

// CLKMOD Define
#define GET_FCLK_MOD(val)	((val >> 8) & 0x7)
#define GET_HCLK_MOD(val)	((val >> 4) & 0xF)
#define GET_PCLK_MOD(val)	(val & 0xF)

#endif

#define REQ_FCLK	1
#define REQ_DCLK	2
#define REQ_HCLK	3
#define REQ_PCLK	4

#define MHZ		1000000

#endif /* __SSA1_FPGA_H */
