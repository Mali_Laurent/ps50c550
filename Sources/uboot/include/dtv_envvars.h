#define CONFIG_ZERO_BOOTDELAY_CHECK

#if defined(CONFIG_SERIAL1)
#	define CONFIG_CONSOLE "ttyS1"
#elif defined(CONFIG_SERIAL2)
#	define CONFIG_CONSOLE "ttyS2"
#else
#	define CONFIG_CONSOLE "ttyS0"
#endif

#define CONFIG_BOOTDELAY		10
#define CONFIG_ETHADDR			00:00:00:00:00:00
#define CONFIG_IPADDR			0.0.0.0
#define CONFIG_SERVERIP			0.0.0.0
#define CONFIG_GATEWAYIP		0.0.0.0
#define CONFIG_NETMASK			255.255.255.0
#define CONFIG_BOOTFILE			uImage
#define CONFIG_LOADADDR			CFG_LOAD_ADDR
#define CONFIG_BOOTCOMMAND		"tftp;bootm"
#define CONFIG_NFSBOOTCOMMAND		"run nfsargs;bootd"
#define CONFIG_EXTRA_ENV_SETTINGS \
"rootpath=/home\0" \
"console=" CONFIG_CONSOLE "\0" \
"nfsargs=setenv bootargs $(bootargs) root=/dev/nfs rw nfsroot=$(serverip):$(rootpath) ethaddr=$(ethaddr) ip=$(ipaddr):$(serverip):$(gatewayip):$(netmask)::: console=$(console)\0" 
