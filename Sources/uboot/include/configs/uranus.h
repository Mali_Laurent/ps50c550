#ifndef _URANUS_H_
#define _URANUS_H_

#include <asm/arch/sizes.h>

/*
 * High Level Configuration Options
 */
#define CONFIG_ARM926EJ
#define CONFIG_URANUS

/* ij.jang : add MMU/DCACHE support */
#define USE_926EJS_MMU_TRANS
/* see board/sdp/crux/crux.c */
#define CFG_MMU_NCNB_SIZE	0x400000	/* NCNB area : 4MB, at last area of DRAM */
/* see drivers/rtl8139.c */
#define CFG_RTL8139_BUFF	0x27F80000	/* last 512KB */

#define CONFIG_DISPLAY_CPUINFO
#define INPUT_CLOCK			27000000	//27MHZ
#define CONFIG_SYS_CLK_FREQ		INPUT_CLOCK

#undef CONFIG_USE_IRQ
#define CONFIG_SKIP_LOWLEVEL_INIT
#define cONFIG_SKIP_RELOCATE_UBOOT

// need to modify 
#define CFG_HZ				(1250000)	/* see cpu/arm926ejs/s5h2150/timer.c */

/* uart */
#define CFG_BAUDRATE_TABLE		{9600, 19200, 38400, 57600, 115200}
#define CONFIG_SERIAL1          	1
#define CONFIG_BAUDRATE			115200
#define CONFIG_USE_UART_FIFO		1

#define CONFIG_NR_DRAM_BANKS		1

#define PHYS_SDRAM_1			0x20000000
#define PHYS_SDRAM_1_SIZE		(128 << 20)

#define CFG_MEMTEST_START		0x20000000	/* memtest works on	*/
#define CFG_MEMTEST_END			0x22000000	/* 126 MB in DRAM	*/
#define	CFG_LOAD_ADDR			0x20000000	/* default load address	*/

/* PCI */
#undef CONFIG_PCI

#if defined(CONFIG_PCI)
#define CFG_PCI_EXT_CLOCK
#undef CFG_PCI_EXT_RESET
#undef CFG_PCI_CLK_66
#define CONFIG_MISC_INIT_R
#define CONFIG_PCI_PNP
#endif

/* USB */
#define LITTLEENDIAN
#define CONFIG_USB_OHCI
#define CONFIG_USB_STORAGE
#define CONFIG_DOS_PARTITION

/* Ethernet adaptors */
#define CONFIG_NET_MULTI
#undef CONFIG_DRIVER_CS8900
#define CONFIG_RTL8150USB
#undef CONFIG_DRIVER_RTL8150
#undef CONFIG_RTL8139

/* CS8900A */
#if defined(CONFIG_DRIVER_CS8900)
#define CS8900_BASE			0x08000300
#define CS8900_BUS16			1
#endif

/* RTL8139 on PCI */
#ifdef CONFIG_RTL8139
#define	CFG_ETH_NO_EEPROM
#define KSEG1ADDR(x)            (x)

/* ij.jang : see board/s5h2x/uranus/uranus.c, drivers/rtl8139.c */
#if defined(USE_926EJS_MMU_TRANS)
#define CFG_RTL8139_BUFF	0x27000000
#endif

#endif

/* ONENAND */
#define CONFIG_BBML
#undef CONFIG_TBML

#undef CFG_ENV_IS_IN_ONENAND
#undef CFG_ENV_IS_NOWHERE
#define CFG_ONENAND_BASE		0x04000000
/* see tbml/u-boot_interface.h */
#define CFG_ONENAND_ONWADDR		0x20000000
#define CFG_ONENAND_BUFADDR		0x21000000

/* Flash define */
#define PHYS_FLASH_1			0x00000000
#define PHYS_FLASH_SECT_SIZE		0x00020000	/* 128 KB per 1 sector */

/* intel NOR flash */
#define CFG_FLASH_INTEL			1
#define CFG_FLASH_BASE			PHYS_FLASH_1
#define CFG_MAX_FLASH_BANKS		1		/* only use 1 bank */
#define CFG_MAX_FLASH_SECT		128
#define FLASH_PORT_WIDTH16		1
#define CFG_FLASH_PROTECTION	1	

/* timeout values are in ticks */
#define CFG_FLASH_ERASE_TOUT		(2 * CFG_HZ)
#define CFG_FLASH_WRITE_TOUT		(5 * CFG_HZ)
#define CFG_FLASH_PROTECT_TOUT		(2 * CFG_HZ)

/* u-boot image location */
#define CFG_U_BOOT_SIZE			(3 * PHYS_FLASH_SECT_SIZE)

/* environment info location */
#define CFG_ENV_IS_IN_FLASH		1
#define CFG_ENV_ADDR			(PHYS_FLASH_1 + CFG_U_BOOT_SIZE)
#define CFG_ENV_SIZE			0x00020000	/* 128KB */
#define CFG_ENV_SECT_SIZE		0x00020000
#define CONFIG_ENV_OVERWRITE		/* allow to overwrite serial and ethaddr */
#undef	CFG_ENV_IS_IN_DATAFLASH

/* FULL memory test */
#define CFG_ALT_MEMTEST				1

/*
 * Size of malloc() pool
 */
#define CFG_MALLOC_LEN           (CFG_ENV_SIZE + SZ_128K)
#define CFG_GBL_DATA_SIZE        128  /* size in bytes reserved for initial data */

/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE         SZ_128K /* regular stack */
#ifdef CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ     SZ_4K   /* IRQ stack */
#define CONFIG_STACKSIZE_FIQ     SZ_4K   /* FIQ stack */
#endif


/***********************************************************
 * Command & Environment data definition
 ***********************************************************/
#define CONFIG_CMDLINE_TAG		1

#define CONFIG_COMMANDS ( \
			CONFIG_CMD_DFL		| \
			CFG_CMD_REGINFO		| \
			CFG_CMD_MEMORY		| \
			CFG_CMD_SAVES		| \
			CFG_CMD_ENV 		| \
			CFG_CMD_PING		| \
			CFG_CMD_LOADS		| \
			CFG_CMD_FLASH		| \
			CFG_CMD_NET		| \
			CFG_CMD_RUN		| \
			CFG_CMD_PCI		| \
			CFG_CMD_ONENAND		| \
			CFG_CMD_USB		| \
			CFG_CMD_FAT		| \
			0)
			/*
			CFG_CMD_CACHE		| \
			CFG_CMD_ELF		| \
			CFG_CMD_LOADB		| \
			CFG_CMD_NAND		| \
			CFG_CMD_EEPROM		| \
			CFG_CMD_I2C		| \
			CFG_CMD_DATE		| \
			*/

#define CFG_CONSOLE_IS_IN_ENV	1
/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
#include <cmd_confdefs.h>
#include <dtv_envvars.h>

/************************************************************
 * Debugging options
 ************************************************************/
#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
#define CONFIG_KGDB_BAUDRATE	115200		/* speed to run kgdb serial port */
#define CONFIG_KGDB_SER_INDEX	2			/* which serial port to use */
#endif

/***********************************************************
 * Buffer options
 ***********************************************************/
/* Console I/O Buffer Size	*/
#define	CFG_CBSIZE			256

/* Print Buffer Size */
#define	CFG_PBSIZE 			(CFG_CBSIZE+sizeof(CFG_PROMPT)+16)

/* max number of command args	*/
#define	CFG_MAXARGS			16

/* Boot Argument Buffer Size	*/
#define CFG_BARGSIZE			CFG_CBSIZE

/***********************************************************
 * Miscellaneous configurable options
 ***********************************************************/
#define	CFG_LONGHELP		/* undef to save memory		*/
#define	CFG_PROMPT		"URANUS # "

#endif							/* __CONFIG_H */
