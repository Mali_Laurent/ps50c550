/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 * Gary Jennejohn <gj@denx.de>
 * David Mueller <d.mueller@elsoft.ch>
 *
 * Modified for the friendly-arm SBC-2410X by
 * (C) Copyright 2005
 * JinHua Luo, GuangDong Linux Center, <luo.jinhua@gd-linux.com>
 *
 * Configuation settings for the friendly-arm SBC-2410X board.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#ifndef __CONFIG_H
#define __CONFIG_H

/*
 * High Level Configuration Options
 * (easy to change)
 */
#define CONFIG_ARM926EJS	1	/* This is an ARM926EJ Core	*/

#define CONFIG_SDP76		1	/* on a friendly-arm SBC-2410X Board  */
#define CONFIG_BHPLCD

/*
 * If we are developing, we might want to start armboot from ram
 * so we MUST NOT initialize critical regs like mem-timing ...
 */
#define CONFIG_SKIP_LOWLEVEL_INIT	/* undef for developing */
#undef CONFIG_SKIP_RELOCATE_UBOOT

/* see cpu/arm926ejs/mmu.c : add ARM926EJS mmu support */
#define USE_926EJS_MMU_TRANS
/* see board/sdp/crux/crux.c */
#define CFG_MMU_NCNB_SIZE	0x400000	/* NCNB area : 4MB, at last area of DRAM */

/* input clock of PLL */
#define CONFIG_SYS_CLK_FREQ	27000000
#undef CFG_SDP76_HCLK80			/* HCLK 80Mhz for OneNAND Sync burst */
#undef CONFIG_USE_IRQ			/* we don't need IRQ/FIQ stuff */

/*-----------------------------------------------------------------------
 * Physical Memory Map
 */
#define CONFIG_NR_DRAM_BANKS	1	   /* we have 1 bank of DRAM */
#define PHYS_SDRAM_1		0x21c00000 /* SDRAM Bank #1 */
#define PHYS_SDRAM_1_SIZE	0x02000000 /* 32 MB */

/* SMC banks */
#define PHYS_SMC_BANK0		0x0
#define PHYS_SMC_BANK1		0x04000000
#define PHYS_SMC_BANK2		0x08000000

#define BAYHILL_PLUS_LINEAR_128

/*
 * Hardware drivers
 */
/* USB */
#define LITTLEENDIAN
#define CONFIG_USB_OHCI
#define CONFIG_USB_STORAGE
#define CONFIG_DOS_PARTITION

#define CONFIG_PCI
#undef CFG_PCI_EXT_CLOCK
#undef CFG_PCI_EXT_RESET
#undef CFG_PCI_CLK_66
#define CONFIG_MISC_INIT_R
#define CONFIG_PCI_PNP

#define CONFIG_NET_MULTI
#define CONFIG_DRIVER_CS8900
#define CONFIG_RTL8139
/* NCNB area, last 512KB */
#define CFG_RTL8139_BUFF	(PHYS_SDRAM_1 + PHYS_SDRAM_1_SIZE - 0x80000)
#undef CONFIG_RTL8150USB

#ifdef CONFIG_RTL8139
#define CFG_ETH_NO_EEPROM
#define KSEG1ADDR(x)	(x)
#endif

#ifdef CONFIG_DRIVER_CS8900
#define CS8900_BASE             0x08000300
#define CS8900_BUS16
#endif

/*
 * select serial console configuration
 */
 /* valid baudrates */
#define CFG_BAUDRATE_TABLE	{ 9600, 19200, 38400, 57600, 115200 }

#define CONFIG_SERIAL0          	1
#define CONFIG_BAUDRATE			115200
#define CONFIG_USE_UART_FIFO		1

/* allow to overwrite serial and ethaddr */
#define CONFIG_ENV_OVERWRITE

/***********************************************************
 * Command definition
 ***********************************************************/
#define CONFIG_COMMANDS ( \
			CONFIG_CMD_DFL		| \
			CFG_CMD_REGINFO		| \
			CFG_CMD_MEMORY		| \
			CFG_CMD_SAVES		| \
			CFG_CMD_ENV 		| \
			CFG_CMD_PING		| \
			CFG_CMD_LOADS		| \
			CFG_CMD_NET		| \
			CFG_CMD_RUN		| \
			CFG_CMD_PCI		| \
			CFG_CMD_ONENAND		| \
			CFG_CMD_USB		| \
			CFG_CMD_FAT		| \
			0) & ~(0		| \
			CFG_CMD_IMLS		| \
			CFG_CMD_FLASH		| \
			0)
			/*
			CFG_CMD_ELF			| \
			CFG_CMD_LOADB		| \
			CFG_CMD_NAND		| \
			CFG_CMD_EEPROM		| \
			CFG_CMD_I2C			| \
			CFG_CMD_USB			| \
			CFG_CMD_DATE		| \
			*/

/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
#include <cmd_confdefs.h>
#include <dtv_envvars.h>

#if 0
#define CONFIG_BOOTDELAY	20
#define CONFIG_BOOTARGS    	"console=ttySAC0 root=/dev/nfs nfsroot=192.168.0.1:/friendly-arm/rootfs_netserv ip=192.168.0.69:192.168.0.1:192.168.0.1:255.255.255.0:debian:eth0:off"
#define CONFIG_ETHADDR	        08:00:3e:26:0a:5b
#define CONFIG_NETMASK          255.255.255.0
#define CONFIG_IPADDR		192.168.0.69
#define CONFIG_SERVERIP		192.168.0.1
/*#define CONFIG_BOOTFILE	"elinos-lart" */
#define CONFIG_BOOTCOMMAND	""
#endif

/*
 * Miscellaneous configurable options
 */
#define	CFG_LONGHELP				/* undef to save memory		*/
#define	CFG_PROMPT		"[BHPLCD ]# "	/* Monitor Command Prompt	*/
#define	CFG_CBSIZE		256		/* Console I/O Buffer Size	*/
#define	CFG_PBSIZE (CFG_CBSIZE+sizeof(CFG_PROMPT)+16) /* Print Buffer Size */
#define	CFG_MAXARGS		16		/* max number of command args	*/
#define CFG_BARGSIZE		CFG_CBSIZE	/* Boot Argument Buffer Size	*/

#define CFG_MEMTEST_START	0x22E00000	/* memtest works on	*/
#define CFG_MEMTEST_END		0x23F00000

#undef  CFG_CLKS_IN_HZ		/* everything, incl board info, in Hz */

#define	CFG_LOAD_ADDR		CFG_MEMTEST_START	/* default load address	*/

/* the PWM TImer 4 uses a counter of 15625 for 10 ms, so we need */
/* it to wrap 100 times (total 1562500) to get 1 sec. */
#define	CFG_HZ			1562500

/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE	(128*1024)	/* regular stack */
#ifdef CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ	(4*1024)	/* IRQ stack */
#define CONFIG_STACKSIZE_FIQ	(4*1024)	/* FIQ stack */
#endif

/*-----------------------------------------------------------------------
 * FLASH and environment organization
 */

/* ONENAND XSR */
#undef CONFIG_BBML
/* ONENAND TBML */
#define CONFIG_TBML

#if defined(CONFIG_BBML) || defined(CONFIG_TBML)
#define CFG_ONENAND_BASE	PHYS_SMC_BANK0
/* sh.kim - start */
#define CFG_ONENAND_BASE1	PHYS_SMC_BANK1
/* sh.kim - end */

#define CFG_ONENAND_ONWADDR	0x20000000	/* update-util(ONW) */
#define CFG_ONENAND_BUFADDR	0x21000000	/* kernel */
#endif

#define CFG_NO_FLASH

/* u-boot image location */
#define CFG_U_BOOT_SIZE                 0x60000

/* environment info location */
#undef CFG_ENV_IS_IN_FLASH
#define CFG_ENV_IS_IN_ONENAND
#undef CFG_ENV_IS_IN_BBML

#if defined(CFG_ENV_IS_IN_FLASH)
#define CFG_ENV_ADDR			(PHYS_FLASH_1 + CFG_U_BOOT_SIZE)
#define CFG_ENV_SIZE			0x20000
//#define CFG_ENV_SECT_SIZE		(0x00020000)

#elif defined(CFG_ENV_IS_IN_ONENAND)
#define CFG_ENV_ADDR			0x80000
#define CFG_ENV_SIZE			0x00020000
#define CFG_ENV_SECT_SIZE		0x00020000

#elif defined(CFG_ENV_IS_IN_BBML)
#define CFG_ENV_SIZE			0x00020000
#endif

#define CONFIG_ENV_OVERWRITE            /* allow to overwrite serial and ethaddr */
#undef  CFG_ENV_IS_IN_DATAFLASH

#if 0
#define CFG_HUSH_PARSER
#define CFG_PROMPT_HUSH_PS2   "> "
#endif

#define CONFIG_CMDLINE_EDITING
#define CONFIG_CMDLINE_TAG
#undef CONFIG_SETUP_MEMORY_TAGS
#undef CONFIG_INITRD_TAG

#ifdef CONFIG_CMDLINE_EDITING
#undef CONFIG_AUTO_COMPLETE
#else
#define CONFIG_AUTO_COMPLETE
#endif

/*
 * Size of malloc() pool
 */
#define CFG_MALLOC_LEN		(CFG_ENV_SIZE + 128*1024)
#define CFG_GBL_DATA_SIZE	128	/* size in bytes reserved for initial data */

#endif	/* __CONFIG_H */
