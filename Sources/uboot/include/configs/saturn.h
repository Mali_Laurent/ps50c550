/*
 * u-boot-1.1.3 port for S5H2111
 *
 * D-TV Dev Team, System LSI
 * Copyright(c) 2005 Samsung Electronics, Inc.
 * All rights reserved.
 */
/**
 * jupiter.h : U-Boot Configuration file for jupiter target platform.
 *
 * @author   : Ikjoon Jang (ij.jang@samsung.com)
 * @see      : S5H2111 User's manual, README file in U-Boot directory
 * @version  : $Revision$
 */

/*
modification history
-----------------------------
02, July,2007 : ij.jang port to u-boot-1.1.6
25, Jan, 2005 : ij.jang copy from jupiter
14, Nov, 2005 : ij.jang created. (skel. code from mercury)
*/

#ifndef __CONFIG_H_
#define __CONFIG_H_

/************************************************************
 * Board information
 ************************************************************/
#define CONFIG_ARM926EJS	1	/* This is an ARM926EJS Core	*/
#define CONFIG_S5H2111		1	/* S5H2111 LAKE SoC */
#define CONFIG_SATURN		1	/* SEC SATURN board*/
#define CONFIG_SATURN3			/* SEC SATURN3 board (LAKE2) */

#define CONFIG_SKIP_LOWLEVEL_INIT
#define CONFIG_SKIP_RELOCATE_UBOOT

/************************************************************
 * System Information
 ************************************************************/
/* input clock of PLL */
#define CONFIG_SYS_CLK_FREQ		27000000	/* 27Mhz PLL input */
#define USE_926EJS_MMU
#undef CONFIG_USE_IRQ			/* not use interrupt */

#define USE_926EJS_MMU_TRANS
/* see board/sdp/crux/crux.c */
#define CFG_MMU_NCNB_SIZE	0x400000	/* NCNB area : 4MB, at last area of DRAM */
/* see drivers/rtl8139.c */
#define CFG_RTL8139_BUFF	0x23F80000	/* last 512KB */

/* Timer0 : 1054687.5Hz input. see interrupts.c */
#define CFG_HZ			 		1054678

/* valid baudrates */
#define CFG_BAUDRATE_TABLE		{ 9600, 19200, 38400, 57600, 115200 }

/************************************************************
 *  Hardware drivers
 ************************************************************/
/* ethernet */
#define CONFIG_DRIVER_CS8900		1
#define CS8900_BASE			0x08000300		/* SCS Bank2 */
#define CS8900_BUS16			1

/* uart */
#define CONFIG_SERIAL0          	1
#define CONFIG_BAUDRATE			115200
#define CONFIG_USE_UART_FIFO		

/************************************************************
 * Memory information
 ************************************************************/
/* SDRAM */
#define CONFIG_NR_DRAM_BANKS		1
#define PHYS_SDRAM_1			(0x20000000)
#define PHYS_SDRAM_1_SIZE		(64 * 1024 * 1024)	/* 64 MB */

/* memory test area */
#define CFG_MEMTEST_START		0x20000000
#define CFG_MEMTEST_END			0x23000000
#define	CFG_LOAD_ADDR			0x21000000

/************************************************************
 * Static memory information
 ************************************************************/
/* SEC OneNAND flash for bbml */
#undef CONFIG_TBML
#define CONFIG_BBML

#if defined(CONFIG_TBML) || defined(CONFIG_BBML)
#define CFG_ONENAND_BASE		0x0	/* Bank1 */
#define CFG_ONENAND_BUFADDR		0x21000000
#endif

#if defined(CONFIG_TBML)
#define CFG_ONENAND_ONWADDR		0x20000000
#endif

/* flash settings */
#undef CFG_FLASH_INTEL
#if defined(CFG_FLASH_INTEL)
#define CFG_FLASH_BASE			PHYS_FLASH_1
#define CFG_MAX_FLASH_BANKS		1
#define CFG_MAX_FLASH_SECT		(64)	/* intel 28F640 */
#define FLASH_PORT_WIDTH16		1
#define PHYS_FLASH_1			0x04000000
#define PHYS_FLASH_SECT_SIZE		(128 * 1024)	/* 128kb per 1 sector */
#define CFG_FLASH_PROTECTION		1

/* timeout values are in ticks */
#define CFG_FLASH_ERASE_TOUT		(2 * CFG_HZ)
#define CFG_FLASH_WRITE_TOUT		(5 * CFG_HZ)
#define CFG_FLASH_PROTECT_TOUT		(2 * CFG_HZ)

#else
#define CFG_NO_FLASH
#endif

#define CFG_ENV_IS_IN_BBML
#undef CFG_ENV_IS_IN_ONENAND
#undef CFG_ENV_IS_IN_FLASH

/* u-boot image location */
#define CFG_U_BOOT_SIZE			(0x60000)

/* environment info location */

#define CONFIG_ENV_OVERWRITE		1

#if defined(CFG_ENV_IS_IN_FLASH)
#define CFG_ENV_ADDR			(PHYS_FLASH_1 + CFG_U_BOOT_SIZE)
#define CFG_ENV_SIZE			(0x00004000)
#define CFG_ENV_SECT_SIZE		(0x20000)
#else
#define CFG_ENV_SECT_SIZE		(0x10000)
#define CFG_ENV_SIZE			(0x10000)
#endif

#undef CFG_ALT_MEMTEST

/************************************************************
 * Linkage information
 ************************************************************/
#define CFG_MALLOC_LEN				(CFG_ENV_SIZE + 128*1024)
#define CFG_GBL_DATA_SIZE			128
#define CONFIG_STACKSIZE			0x20000		/* 128KBytes */

#ifdef CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ		0x1000	/* IRQ stack : 4KB */
#define CONFIG_STACKSIZE_FIQ		0x1000	/* FIQ stack : 4KB */
#endif

/***********************************************************
 * Command & Environment data definition
 ***********************************************************/
#define CONFIG_COMMANDS ( \
			CONFIG_CMD_DFL		| \
			CFG_CMD_REGINFO		| \
			CFG_CMD_MEMORY		| \
			CFG_CMD_SAVES		| \
			CFG_CMD_LOADS		| \
			CFG_CMD_ENV		| \
			CFG_CMD_PING		| \
			CFG_CMD_NET		| \
			CFG_CMD_RUN		| \
			0) & ~(			\
			CFG_CMD_IMLS		| \
			CFG_CMD_FLASH		| \
			0)
			/*
			CFG_CMD_ONENAND		| \
			CFG_CMD_CACHE		| \
			CFG_CMD_ELF		| \
			CFG_CMD_NAND		| \
			CFG_CMD_EEPROM		| \
			CFG_CMD_I2C			| \
			CFG_CMD_USB			| \
			CFG_CMD_DATE		| \
			*/

#define CFG_CONSOLE_IS_IN_ENV	1
/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
#include <cmd_confdefs.h>

/* dtv_envvars.h : defines u-boot envionment variables for
   	D-TV team general development environment */
#include "dtv_envvars.h"

/************************************************************
 * linux kernel booting options 
 ************************************************************/
#define CONFIG_CMDLINE_TAG		1

/************************************************************
 * Debugging options
 ************************************************************/
#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
#define CONFIG_KGDB_BAUDRATE	115200		/* speed to run kgdb serial port */
#define CONFIG_KGDB_SER_INDEX	1			/* which serial port to use */
#endif

#undef CONFIG_DEBUG_BOOT		/* not init pll, smc, ddr */
#define CONFIG_DEBUG_UART		/* enable UART ASAP */

/***********************************************************
 * Buffer options
 ***********************************************************/
/* Console I/O Buffer Size	*/
#define	CFG_CBSIZE			256

/* Print Buffer Size */
#define	CFG_PBSIZE 			(CFG_CBSIZE+sizeof(CFG_PROMPT)+16)

/* max number of command args	*/
#define	CFG_MAXARGS			16	

/* Boot Argument Buffer Size	*/
#define CFG_BARGSIZE		CFG_CBSIZE

/***********************************************************
 * Miscellaneous configurable options
 ***********************************************************/
#define	CFG_LONGHELP		/* undef to save memory		*/
#define	CFG_PROMPT			"SATURN # "

#endif	/* __CONFIG_H_ */

