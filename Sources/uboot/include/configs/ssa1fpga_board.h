/*
 */

#ifndef __CONFIG_H
#define __CONFIG_H

#include <asm/arch/sizes.h>

/*
 * High Level Configuration Options
 */
#define CONFIG_ARM1136           1    /* This is an arm1136 CPU core */
#define CONFIG_ARM1176		 1
#define CONFIG_SSA1FPGA_BOARD	 1

#define FPGA_BOARD_CLOCK		6750000

#define CONFIG_SYS_CLK_FREQ	FPGA_BOARD_CLOCK

#undef CONFIG_USE_IRQ
#define CONFIG_SKIP_LOWLEVEL_INIT
#define CONFIG_SKIP_RELOCATE_UBOOT


#define CFG_HZ		FPGA_BOARD_CLOCK



/* uart */
#define CFG_BAUDRATE_TABLE		{9600, 19200, 38400, 57600, 115200}

#define CONFIG_SERIAL2          	1
#define CONFIG_BAUDRATE			19200
#define CONFIG_USE_UART_FIFO		1

#define CONFIG_NR_DRAM_BANKS		1
#define PHYS_SDRAM_1			0x60000000 	/* DDR SDRAM Bank #1 (West) */
#define PHYS_SDRAM_1_SIZE		0x02000000 	/* 32 MB *//* memory test area */

#define CFG_MEMTEST_START		0x60000000	/* memtest works on	*/
#define CFG_MEMTEST_END			0x61800000	/* 32 MB in DRAM	*/
#define	CFG_LOAD_ADDR			0x60000000	/* default load address	*/

#define CFG_NO_FLASH			1
#define CFG_ENV_IS_NOWHERE		1


/*
 * Size of malloc() pool
 */
#define CFG_ENV_SIZE             SZ_128K     /* Total Size of Environment Sector */
#define CFG_MALLOC_LEN           (CFG_ENV_SIZE + SZ_128K)
#define CFG_GBL_DATA_SIZE        128  /* size in bytes reserved for initial data */

/*-----------------------------------------------------------------------
 * Stack sizes
 *
 * The stack sizes are set up in start.S using the settings below
 */
#define CONFIG_STACKSIZE         SZ_128K /* regular stack */

#ifdef CONFIG_USE_IRQ
#define CONFIG_STACKSIZE_IRQ     SZ_4K   /* IRQ stack */
#define CONFIG_STACKSIZE_FIQ     SZ_4K   /* FIQ stack */
#endif


/***********************************************************
 * Command & Environment data definition
 ***********************************************************/
#define CONFIG_CMDLINE_TAG		1

#define CONFIG_COMMANDS ( \
			CFG_CMD_REGINFO		| \
			CFG_CMD_MEMORY		| \
			0)
			/*
			CFG_CMD_CACHE		| \
			CFG_CMD_ELF			| \
			CFG_CMD_FLASH		| \
			CONFIG_CMD_DFL		| \
			CFG_CMD_SAVES		| \
			CFG_CMD_LOADS		| \
			CFG_CMD_LOADB		| \
			CFG_CMD_ENV 		| \
			CFG_CMD_NAND		| \
			CFG_CMD_EEPROM		| \
			CFG_CMD_I2C			| \
			CFG_CMD_USB			| \
			CFG_CMD_DATE		| \
			CFG_CMD_ONENAND		| \
			CFG_CMD_PING		| \
			*/

#define CFG_CONSOLE_IS_IN_ENV	1
/* this must be included AFTER the definition of CONFIG_COMMANDS (if any) */
#include <cmd_confdefs.h>

#include <dtv_envvars.h>


/************************************************************
 * Debugging options
 ************************************************************/
#if (CONFIG_COMMANDS & CFG_CMD_KGDB)
#define CONFIG_KGDB_BAUDRATE	19200		/* speed to run kgdb serial port */
#define CONFIG_KGDB_SER_INDEX	2			/* which serial port to use */
#endif

#undef CONFIG_DEBUG_BOOT		/* not init pll, smc, ddr */
#undef CONFIG_DEBUG_UART		/* enable UART ASAP */

/***********************************************************
 * Buffer options
 ***********************************************************/
/* Console I/O Buffer Size	*/
#define	CFG_CBSIZE			256

/* Print Buffer Size */
#define	CFG_PBSIZE 			(CFG_CBSIZE+sizeof(CFG_PROMPT)+16)

/* max number of command args	*/
#define	CFG_MAXARGS			16	

/* Boot Argument Buffer Size	*/
#define CFG_BARGSIZE			CFG_CBSIZE

/***********************************************************
 * Miscellaneous configurable options
 ***********************************************************/
#define	CFG_LONGHELP		/* undef to save memory		*/
#define	CFG_PROMPT			"SSA1FPGA # "

#endif							/* __CONFIG_H */
