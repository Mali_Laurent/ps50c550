#ifndef RTL8150_H
#define RTL8150_H

#include <common.h>
#include <devices.h>
#include <usb.h>
#include <net.h>
#include <asm-arm/errno.h>
#include <linux/byteorder/generic.h>
#include <linux/byteorder/little_endian.h>


//#define NULL                  0
//#define EIO                   1
#define IDR                     0x0120
#define MAR                     0x0126
#define CR                      0x012e
#define TCR                     0x012f
#define RCR                     0x0130
#define TSR                     0x0132
#define RSR                     0x0133
#define CON0                    0x0135
#define CON1                    0x0136
#define MSR                     0x0137
#define PHYADD                  0x0138
#define PHYDAT                  0x0139
#define PHYCNT                  0x013b
#define GPPC                    0x013d
#define BMCR                    0x0140
#define BMSR                    0x0142
#define ANAR                    0x0144
#define ANLP                    0x0146
#define AER                     0x0148
#define CSCR                    0x014C  /* This one has the link status */
#define CSCR_LINK_STATUS        (1 << 3)

#define IDR_EEPROM              0x1202

#define PHY_READ                0
#define PHY_WRITE               0x20
#define PHY_GO                  0x40

#define MII_TIMEOUT             10
#define INTBUFSIZE              8

#define RTL8150_REQT_READ       0xc0
#define RTL8150_REQT_WRITE      0x40
#define RTL8150_REQ_GET_REGS    0x05
#define RTL8150_REQ_SET_REGS    0x05

/* Transmit status register errors */
#define TSR_ECOL                (1<<5)
#define TSR_LCOL                (1<<4)
#define TSR_LOSS_CRS            (1<<3)
#define TSR_JBR                 (1<<2)
#define TSR_ERRORS              (TSR_ECOL | TSR_LCOL | TSR_LOSS_CRS | TSR_JBR)
/* Receive status register errors */
#define RSR_CRC                 (1<<2)
#define RSR_FAE                 (1<<1)
#define RSR_ERRORS              (RSR_CRC | RSR_FAE)

/* Media status register definitions */
#define MSR_DUPLEX              (1<<4)
#define MSR_SPEED               (1<<3)
#define MSR_LINK                (1<<2)

/* Interrupt pipe data */
#define INT_TSR                 0x00
#define INT_RSR                 0x01
#define INT_MSR                 0x02
#define INT_WAKSR               0x03
#define INT_TXOK_CNT            0x04
#define INT_RXLOST_CNT          0x05
#define INT_CRERR_CNT           0x06
#define INT_COL_CNT             0x07

/* Transmit status register errors */
#define TSR_ECOL                (1<<5)
#define TSR_LCOL                (1<<4)
#define TSR_LOSS_CRS            (1<<3)
#define TSR_JBR                 (1<<2)
#define TSR_ERRORS              (TSR_ECOL | TSR_LCOL | TSR_LOSS_CRS | TSR_JBR)
/* Receive status register errors */
#define RSR_CRC                 (1<<2)
#define RSR_FAE                 (1<<1)
#define RSR_ERRORS              (RSR_CRC | RSR_FAE)

/* Media status register definitions */
#define MSR_DUPLEX              (1<<4)
#define MSR_SPEED               (1<<3)
#define MSR_LINK                (1<<2)

/* Interrupt pipe data */
#define INT_TSR                 0x00
#define INT_RSR                 0x01
#define INT_MSR                 0x02
#define INT_WAKSR               0x03
#define INT_TXOK_CNT            0x04
#define INT_RXLOST_CNT          0x05
#define INT_CRERR_CNT           0x06
#define INT_COL_CNT             0x07

#define HZ 100

#define RTL8150_MTU             3536
//#define RTL8150_MTU             1536
//#define       RTL8150_MTU             600
#define RTL8150_TX_TIMEOUT      (HZ)
#define RX_SKB_POOL_SIZE        4

/* rtl8150 flags */
#define RTL8150_HW_CRC          0
#define RX_REG_SET              1
#define RTL8150_UNPLUG          2
#define RX_URB_FAIL             3

/* Define these values to match your device */
#define VENDOR_ID_REALTEK               0x0bda
#define VENDOR_ID_MELCO                 0x0411
#define VENDOR_ID_MICRONET              0x3980
#define VENDOR_ID_LONGSHINE             0x07b8

#define PRODUCT_ID_RTL8150              0x8150
#define PRODUCT_ID_LUAKTX               0x0012
#define PRODUCT_ID_LCS8138TX            0x401a
#define PRODUCT_ID_SP128AR              0x0003

#define BMCR_ANENABLE 0x1000
#define LPA_SLCT                0x001f  /* Same as advertise selector  */
#define LPA_10HALF              0x0020  /* Can do 10mbps half-duplex   */
#define LPA_1000XFULL           0x0020  /* Can do 1000BASE-X full-duplex */
#define LPA_10FULL              0x0040  /* Can do 10mbps full-duplex   */
#define LPA_1000XHALF           0x0040  /* Can do 1000BASE-X half-duplex */
#define LPA_100HALF             0x0080  /* Can do 100mbps half-duplex  */
#define LPA_1000XPAUSE          0x0080  /* Can do 1000BASE-X pause     */
#define LPA_100FULL             0x0100  /* Can do 100mbps full-duplex  */
#define LPA_1000XPAUSE_ASYM     0x0100  /* Can do 1000BASE-X pause asym*/
#define LPA_100BASE4            0x0200  /* Can do 100mbps 4k packets   */
#define LPA_PAUSE_CAP           0x0400  /* Can pause                   */
#define LPA_PAUSE_ASYM          0x0800  /* Can pause asymetrically     */
#define LPA_RESV                0x1000  /* Unused...                   */
#define LPA_RFAULT              0x2000  /* Link partner faulted        */
#define LPA_LPACK               0x4000  /* Link partner acked us       */
#define LPA_NPAGE               0x8000  /* Next page bit               */

#define LPA_DUPLEX              (LPA_10FULL | LPA_100FULL)
#define LPA_100                 (LPA_100FULL | LPA_100HALF | LPA_100BASE4)

#define CAP_NET_ADMIN        12


/* Basic mode control register. */
#define BMCR_RESV               0x003f  /* Unused...                   */
#define BMCR_SPEED1000          0x0040  /* MSB of Speed (1000)         */
#define BMCR_CTST               0x0080  /* Collision test              */
#define BMCR_FULLDPLX           0x0100  /* Full duplex                 */
#define BMCR_ANRESTART          0x0200  /* Auto negotiation restart    */
#define BMCR_ISOLATE            0x0400  /* Disconnect DP83840 from MII */
#define BMCR_PDOWN              0x0800  /* Powerdown the DP83840       */
#define BMCR_ANENABLE           0x1000  /* Enable auto negotiation     */
#define BMCR_SPEED100           0x2000  /* Select 100Mbps              */
#define BMCR_LOOPBACK           0x4000  /* TXD loopback bits           */
#define BMCR_RESET              0x8000  /* Reset the DP83840           */

# define GFP_KERNEL     0

typedef __u16 __le16;

#undef  EEPROM_WRITE

typedef unsigned long kernel_ulong_t;


struct usb_device_id {
        /* which fields to match against? */
        __u16           match_flags;

        /* Used for product specific matches; range is inclusive */
        __u16           idVendor;
        __u16           idProduct;
        __u16           bcdDevice_lo;
        __u16           bcdDevice_hi;

        /* Used for device class matches */
        __u8            bDeviceClass;
        __u8            bDeviceSubClass;
        __u8            bDeviceProtocol;

        /* Used for interface class matches */
        __u8            bInterfaceClass;
        __u8            bInterfaceSubClass;
        __u8            bInterfaceProtocol;

        /* not matched against */
        kernel_ulong_t  driver_info;
};


#endif /* RTL8150_H */
