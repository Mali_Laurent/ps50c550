/*
 * RTL8150 Ethernet for u-boot
 *
 * (C) 2006 Sangjin Yun, sj98.yun@samsung.com
 * 	RTL8150 for U-boot	
 *	
 * (C) 2003 Wolfgang Denk, wd@denx.de
 *     Extension to synchronize ethaddr environment variable
 *     against value in EEPROM
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * Copyright (C) 1999 Ben Williamson <benw@pobox.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is loaded into SRAM in bootstrap mode, where it waits
 * for commands on UART1 to read and write memory, jump to code etc.
 * A design goal for this program is to be entirely independent of the
 * target board.  Anything with a CL-PS7111 or EP7211 should be able to run
 * this code in bootstrap mode.  All the board specifics can be handled on
 * the host.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <common.h>
#include <net.h>

#if defined(CONFIG_RTL8150USB) && (CONFIG_COMMANDS & CFG_CMD_NET) && \
	defined(CFG_CMD_USB) && defined(CONFIG_NET_MULTI)

#include <command.h>
#include <malloc.h>
#include "rtl8150.h"

#undef USB_NET_DEBUG
#undef USB_NET_DEBUG_DUMP

#ifdef USB_NET_DEBUG
#define USB_NET_PRINT(fmt, args...) printf("RTL8150USB DEBUG: " fmt, ##args)
#else
#define USB_NET_PRINT(fmt, args...)
#endif

#undef PRINT
#define PRINT(fmt, args...)	printf("RTL8150USB: " fmt, ##args)

#undef RTL8150_INIT_EARLY

#define RTL8150_USB_VENDOR_ID	0x0BDA
#define RTL8150_USB_PROD_ID	0x8150

extern char usb_started;	/* common/usb.c */

volatile unsigned char RTL8150_RX[RTL8150_MTU] __attribute__((aligned(8)));

static int get_registers(struct usb_device* dev, u16 indx, u16 size, void *data)
{
        return usb_control_msg(dev, usb_rcvctrlpipe(dev, 0),
                               RTL8150_REQ_GET_REGS, RTL8150_REQT_READ,
                               indx, 0, data, size, HZ/2);
}

static int set_registers(struct usb_device* dev, u16 indx, u16 size, void *data)
{
        return usb_control_msg(dev, usb_sndctrlpipe(dev, 0),
                               RTL8150_REQ_SET_REGS, RTL8150_REQT_WRITE,
                               indx, 0, data, size, HZ/2);
}

static int rtl8150_reset (struct usb_device* dev)
{
        volatile u8 data = 0x10;
        int i = HZ;

        set_registers(dev, CR, 1, (void*)&data);

        do {
                get_registers(dev, CR, 1, (void*)&data);
        } while ((data & 0x10) && --i);

	USB_NET_PRINT("USB NET: rtl8150_reset\n");
#if 0
	/* ssangjin modified for speed */
        wait_ms(5);
#endif

        return (i > 0) ? 1 : 0;
}

static int enable_net_traffic(struct usb_device* dev)
{
        u8 cr, tcr, rcr, msr;

        /* RCR bit7=1 attach Rx info at the end;  =0 HW CRC (which is broken) */
        //rcr = 0x9e;
        rcr = 0x84;
        tcr = 0xd8;
        cr = 0x0c;
        // if (!(rcr & 0x80))
                // __set_bit(RTL8150_HW_CRC, &dev->flags);
        set_registers(dev, RCR, 1, &rcr);
        set_registers(dev, TCR, 1, &tcr);
        set_registers(dev, CR, 1, &cr);
        get_registers(dev, MSR, 1, &msr);

        if (msr == 0) {
                int loop;

                loop = 60;
                while (loop) {
	/* ssangjin modified for speed */
                        wait_ms(1);
                        get_registers(dev, MSR, 1, &msr);
                        if (msr & MSR_LINK) break;
                        loop--;
                }
        }

        if (msr & MSR_LINK)
                return 0;
        else
		return -1;

}

static void disable_net_traffic(struct usb_device* dev)
{
        u8 cr;

        get_registers(dev, CR, 1, &cr);
        cr &= 0xf3;
        set_registers(dev, CR, 1, &cr);
}

static inline void set_ethernet_addr(struct usb_device* dev, bd_t* bd)
{
        u8 node_id[6];
        char buffer[20];

        get_registers(dev, IDR, sizeof(node_id), node_id);

        sprintf(buffer, "%02x:%02x:%02x:%02x:%02x:%02x", node_id[0], node_id[1], node_id[2], node_id[3], node_id[4], node_id[5]);
        USB_NET_PRINT("Ethernet Address : %s\n", buffer);

        setenv ("ethaddr", buffer);

	memcpy (bd->bi_enetaddr, node_id, 6);
}

static struct usb_device* rtl8150_find_dev(void)
{
	int i;
	struct usb_device *udev, *udev_found=NULL;
	
	if (usb_started != 1) {
		if (usb_init() != 0) {
			PRINT ("USB init failed.\n");
			return NULL;
		}
	}
	
	/* scan all USB Devices */
	for (i=0; i<USB_MAX_DEVICE; i++) {
		udev=usb_get_dev_index(i); /* get device */
		if (udev->devnum == (i+1)) {
			if (udev->descriptor.idVendor == RTL8150_USB_VENDOR_ID
				&& udev->descriptor.idProduct == RTL8150_USB_PROD_ID)  {
				udev_found = udev;
			}
		}
	}

	return udev_found;
}

static int rtl8150_init_usb(struct eth_device *dev, bd_t *bd)
{
	struct usb_device *udev;
	
	dev->priv = NULL;
	udev = rtl8150_find_dev();

	if (udev == NULL) {
		PRINT ("No scanned devices\n");
		return -1;
	}

	dev->priv = (void*)udev;

	if (!rtl8150_reset(udev)) {
		PRINT ("couldn't reset the device\n");
		return -1;
	}

	set_ethernet_addr(udev, bd);
	if (enable_net_traffic(udev) != 0) {
		PRINT ("Link failed\n");
		return -1;
	}
	wait_ms(10);

	return 0;
}

/* exported functions */
static void rtl8150_halt (struct eth_device *dev)
{
	struct usb_device *udev = (struct usb_device *)dev->priv;
	
	if (udev) {
		USB_NET_PRINT ("rtl8150_halt\n");
		disable_net_traffic(udev);
		wait_ms(10);
	}
}

static int rtl8150_init (struct eth_device *dev, bd_t * bd)
{
	USB_NET_PRINT("RTL8150: init\n");
	return ((rtl8150_init_usb (dev, bd) == 0) ? 1 : 0);
}

static int rtl8150_rx (struct eth_device *dev)
{
        int result;
        int rxlen;
        struct usb_device *udev = (struct usb_device *)dev->priv;
	
	if (!udev) {
		USB_NET_PRINT ("fatal\n");
		return 0;
	}

        rxlen = 0;

	USB_NET_PRINT("RTL8150: usb_bulk_msg RECEIVE\n");
        result = usb_bulk_msg(udev, usb_rcvbulkpipe(udev, 1), (void*)RTL8150_RX, RTL8150_MTU, &rxlen, 10);
	USB_NET_PRINT("RTL8150: usb_bulk_msg RECEIVE2\n");

        /* special handling of STALL in DATA phase */
        if (result < 0) {
		USB_NET_PRINT("eth_rx: usb_bulk_msg error status %ld\n", result);
		enable_net_traffic(udev);
		rxlen=0;
        }
#ifdef USB_NET_DEBUG_DUMP
        else {
		int i;

		USB_NET_PRINT("RTL8150: RECV %d byte\n", rxlen);

		for(i=0; i<rxlen && i < 100; i++)
		{
			if( i % 8 == 0)	USB_NET_PRINT("\t");
			if( i % 16 == 0)	USB_NET_PRINT("\n");
			USB_NET_PRINT("%02x ", RTL8150_RX[i]);
		}
		USB_NET_PRINT("\n");
	}
#endif

	/* Pass the packet up to the protocol layers. */
	NetReceive (RTL8150_RX, rxlen);
	
	return rxlen;
}

static int rtl8150_send (struct eth_device *dev, volatile void *packet, int length)
{
        int actlen;
        int count, res;
        int retry = 5;
	struct usb_device *udev = (struct usb_device *)dev->priv;
	
	if (!udev) {
		USB_NET_PRINT ("fatal\n");
		return 0;
	}

        count = (length < 60) ? 60 : length;
        count = (count & 0x3f) ? count : count + 1;

#ifdef USB_NET_DEBUG_DUMP
	{
		int i;
		char* temp = (char*)packet;

		USB_NET_PRINT("RTL8150: SEND %d byte\n", count);

		for(i=0; i<count; i++)
		{
			if( i % 8 == 0)	USB_NET_PRINT("\t");
			if( i % 16 == 0)	USB_NET_PRINT("\n");
			USB_NET_PRINT("%02x ", temp[i]);
		}
		USB_NET_PRINT("\n");
	}
#endif
retry:
        res = usb_bulk_msg(udev, usb_sndbulkpipe(udev, 2), (unsigned char*)packet, count, &actlen, 10);

	USB_NET_PRINT("RTL8150: usb_bulk_msg SEND\n");
        
	if (res < 0)
        {
                USB_NET_PRINT("RTL8150: usb_bulk_msg SEND error\n");
	 	if( retry > 0)
		{
			enable_net_traffic(udev);
			retry--;
			goto retry;	
		}
		else
			return 0;
        }

        return 0;
}

/* ij.jang */
int rtl8150_initialize (bd_t *bis)
{
	struct eth_device *dev = NULL;

	dev = (struct eth_device*)malloc(sizeof(*dev));
	
	if (!dev) {
		PRINT("memory error\n");
		return -1;
	}

	memset ((void *)dev, 0, sizeof(*dev));
	
	strcpy (dev->name, "RTL8150USB");
	dev->priv = NULL;
	dev->init = rtl8150_init;
	dev->halt = rtl8150_halt;
	dev->send = rtl8150_send;
	dev->recv = rtl8150_rx;

#if defined(RTL8150_INIT_EARLY)
	rtl8150_init_usb (dev, bis);
#endif
	eth_register (dev);

	return 1;
}

#endif
