/*
 * (C) Copyright 2005 Samsung Electronics
 * Kyungmin Park <kyungmin.park@samsung.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>

#if defined(CFG_ENV_IS_IN_ONENAND)	/* Environment is in OneNAND */

#include <command.h>
#include <environment.h>
#include <linux/stddef.h>
#include <malloc.h>

#include <linux/mtd/onenand.h>

/* ij.jang */
DECLARE_GLOBAL_DATA_PTR;

extern struct mtd_info onenand_mtd;
extern struct onenand_chip onenand_chip;

/* sh.kim - start */
extern struct mtd_info onenand_mtd_sub;
extern int bbm_load_env(u8* addr);
extern int bbm_save_env(u8* addr);
/* sh.kim - end */

/* ij.jang : no such function like this and no called */
#if 0
extern tbbm_open_device();
#endif

/* ij.jang : add prototypes */
int onenand_read(struct mtd_info *mtd, loff_t from, size_t len,
	size_t *retlen, u_char *buf);

/* References to names in env_common.c */
extern uchar default_environment[];

#define ONENAND_ENV_SIZE(mtd)	(mtd.oobblock - ENV_HEADER_SIZE)

char * env_name_spec = "OneNAND";

#ifdef ENV_IS_EMBEDDED
extern uchar environment[];
env_t *env_ptr = (env_t *) (&environment[0]);
#else	/* ! ENV_IS_EMBEDDED */
static unsigned char onenand_env[MAX_ONENAND_PAGESIZE];
env_t *env_ptr = (env_t *) onenand_env;
#endif	/* ENV_IS_EMBEDDED */

uchar env_get_char_spec (int index)
{
	/* ij.jang */
	//DECLARE_GLOBAL_DATA_PTR;

	return (*((uchar *)(gd->env_addr + index)));
}

/* ij.jang : add ONENAND ENV DEBUG */
#undef ONENAND_ENV_DEBUG

#if defined(ONENAND_ENV_DEBUG)
void dump2KB(void *ptr)
{
	volatile unsigned int *p = (unsigned int*)ptr;
	for (;p<ptr+0x800;p++) {
		printf ("%08X %08X %08X %08X\n",
				*p, *(p+1), *(p+2), *(p+3));
		p += 4;
	}
}
#endif

/* sh.kim -start */
#if 0
void env_relocate_spec(void)
{
	/* ij.jang */
	//DECLARE_GLOBAL_DATA_PTR;
	unsigned long env_addr;
	int use_default = 0;
	int retlen;

	/* ij.jang : set default state of SYS_CONF1 */
/* sh.kim - start */
	onenand_sync_set(0);
	/* Check Sub OneNAND exist */
	if(onenand_mtd_sub.oobblock)
	{
		onenand_sync_set(1);
	}
/* sh.kim - end */

	env_addr = CFG_ENV_ADDR;
	env_addr -= (unsigned long) onenand_chip.base;


	/* Check OneNAND exist */
	if (onenand_mtd.oobblock)
		/* Ignore read fail */
		onenand_read(&onenand_mtd, env_addr, onenand_mtd.oobblock,
			 &retlen, (u_char *) env_ptr);
	else
		onenand_mtd.oobblock = MAX_ONENAND_PAGESIZE;
		

	if (crc32(0, env_ptr->data, ONENAND_ENV_SIZE(onenand_mtd)) != env_ptr->crc)
		use_default = 1;

	/* ij.jang */
#if defined(ONENAND_ENV_DEBUG)
	printf ("crc32 saved = %08X, calculated=%08X env_size=%08X, addr=%08X\n",
		env_ptr->crc, 
		crc32(0, env_ptr->data, ONENAND_ENV_SIZE(onenand_mtd)),
		onenand_mtd.oobblock,
		env_ptr);
	dump2KB(env_ptr);
#endif
	if (use_default) {
		/* ij.jang */
		puts ("ONENAND - bad CRC, using default environment\n");
		memcpy(env_ptr->data, default_environment, ONENAND_ENV_SIZE(onenand_mtd));
		env_ptr->crc = crc32(0, env_ptr->data, ONENAND_ENV_SIZE(onenand_mtd));
	}

	gd->env_addr = (ulong) &env_ptr->data;
	gd->env_valid = 1;
	
	if(bbm_open_device(0) != 0) {
		printf("TinyBML CS0 open error\n");
		return -1;
	}

/* sh.kim - start */
	if(onenand_mtd_sub.oobblock)
	{
		if(bbm_open_device(1) != 0) {
			printf("TinyBML CS1 open error\n");
			return -1;
		}
	}
/* sh.kim - end */
}
#else 
void env_relocate_spec(void)
{
	int 			use_default = 0;
	int 			retlen;
	int 			part_no;
	u8*			buf = (u8 *)env_ptr;

	/* ij.jang : set default state of SYS_CONF1 */
	//onenand_sync_set(0);
	/* Check Sub OneNAND exist */
	if(onenand_mtd_sub.oobblock)
	{
		//onenand_sync_set(1);
	}

	if(bbm_open_device(0) != 0) {
		printf("TinyBML CS0 open error\n");
		return -1;
	}

	if(onenand_mtd_sub.oobblock)
	{
		if(bbm_open_device(1) != 0) {
			printf("TinyBML CS1 open error\n");
			return -1;
		}
	}

	bbm_load_env(buf);
	
	if (crc32(0, env_ptr->data, ONENAND_ENV_SIZE(onenand_mtd)) != env_ptr->crc)
	{
		use_default = 1;
	}

	/* ij.jang */
#if defined(ONENAND_ENV_DEBUG)
	printf ("crc32 saved = %08X, calculated=%08X env_size=%08X, addr=%08X\n",
		env_ptr->crc, 
		crc32(0, env_ptr->data, ONENAND_ENV_SIZE(onenand_mtd)),
		onenand_mtd.oobblock,
		env_ptr);
	dump2KB(env_ptr);
#endif

	if (use_default) {
		/* ij.jang */
		puts ("ONENAND - bad CRC, using default environment\n");
		memcpy(env_ptr->data, default_environment, ONENAND_ENV_SIZE(onenand_mtd));
		env_ptr->crc = crc32(0, env_ptr->data, ONENAND_ENV_SIZE(onenand_mtd));
	}

	gd->env_addr = (ulong) &env_ptr->data;
	gd->env_valid = 1;
}
#endif
/* sh.kim - end */


/* sh.kim - start */
#if 0
int saveenv(void)
{
	unsigned long 		env_addr = CFG_ENV_ADDR;
	struct erase_info 	instr;
	int 				retlen;
	/* ij.jang : set default state to SYS_CONF1, and restore after finished */
	volatile unsigned short*	sysconf1 = (unsigned short*)(CFG_ONENAND_BASE + 0x1E442);
	unsigned short 		val = *sysconf1;
	int 					vol = 0;

/* sh.kim - start */
	onenand_sync_set(vol);
/* sh.kim - end */

	instr.len = CFG_ENV_SIZE;	/* only used with erasing */
	instr.addr = env_addr;
	instr.addr -= (unsigned long) onenand_chip.base;

	if (onenand_erase(&onenand_mtd, &instr)) {
		printf("OneNAND: erase failed at 0x%08x\n", env_addr);
		/* ij.jang : restore */
		*sysconf1 = val;
		return 1;
	}

	/* update crc */
	env_ptr->crc = crc32(0, env_ptr->data, onenand_mtd.oobblock - ENV_HEADER_SIZE);

	/* ij.jang */
#if defined(ONENAND_ENV_DEBUG)
	printf ("created crc = %08X, env_size=%08X, addr=%08X\n", env_ptr->crc, onenand_mtd.oobblock, env_ptr);
#endif

	env_addr -= (unsigned long) onenand_chip.base;
	if (onenand_write(&onenand_mtd, env_addr, onenand_mtd.oobblock, &retlen, (u_char *) env_ptr)) {
		printf("OneNAND: write failed at 0x%08x\n", instr.addr);
		/* ij.jang : restore */
		*sysconf1 = val;
		return 2;
	}

	/* ij.jang : restore */
	*sysconf1 = val;

	return 0;
}
#else
int saveenv(void)
{
	unsigned long 		env_addr = CFG_ENV_ADDR;
	struct erase_info 	instr;
	int 				retlen;
	/* ij.jang : set default state to SYS_CONF1, and restore after finished */
	volatile unsigned short*	sysconf1 = (unsigned short*)(CFG_ONENAND_BASE + 0x1E442);
	unsigned short 		val = *sysconf1;
	int 					vol = 0;
	int					id;

/* sh.kim - start */
	//onenand_sync_set(vol);
/* sh.kim - end */

	instr.len = CFG_ENV_SIZE;	/* only used with erasing */
	instr.addr = env_addr;
	instr.addr -= (unsigned long) onenand_chip.base;

	if (onenand_erase(&onenand_mtd, &instr)) {
		printf("OneNAND: erase failed at 0x%08x\n", env_addr);
		/* ij.jang : restore */
		*sysconf1 = val;
		return 1;
	}

	/* update crc */
	env_ptr->crc = crc32(0, env_ptr->data, onenand_mtd.oobblock - ENV_HEADER_SIZE);

	/* ij.jang */
#if defined(ONENAND_ENV_DEBUG)
	printf ("created crc = %08X, env_size=%08X, addr=%08X\n", env_ptr->crc, onenand_mtd.oobblock, env_ptr);
#endif

	bbm_save_env(env_ptr);

	/* ij.jang : restore */
	*sysconf1 = val;

	return 0;
}
#endif
/* sh.kim - end */


int env_init(void)
{
	/* ij.jang */
	//DECLARE_GLOBAL_DATA_PTR;

	/* use default */
	gd->env_addr = (ulong) &default_environment[0];
	gd->env_valid = 1;

	return 0;
}

#endif	/* CFG_ENV_IS_IN_ONENAND */
