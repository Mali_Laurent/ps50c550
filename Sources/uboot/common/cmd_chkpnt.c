#include <common.h>
#include <command.h>

void chkpoint_start(void)
{
	__asm ("");
}

void chkpoint_stop(void)
{
	__asm ("");
}

int do_chkpoint (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if (argc < 2) {
		printf ("Usage:\n%s\n", cmdtp->usage);
		return -1;
	}

	if (strcmp (argv[1], "start") == 0) {
		chkpoint_start();
	} else if (strcmp (argv[1], "stop") == 0) {
		chkpoint_stop();
	}

	return 0;
}

U_BOOT_CMD(
	chkpoint, 2, 2, do_chkpoint,
	"chkpoint <start|stop>\n",
	"chkpoint	- dummy command to estimate time duration.\n"
);

