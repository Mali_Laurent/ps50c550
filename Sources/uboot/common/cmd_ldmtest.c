#include <common.h>
#include <command.h>
#include <asm/hardware.h>
#include "../xsr/include/BML.h"
#include "../xsr/include/XsrTypes.h"

#define COPYSIZE	512
#define COPYSIZE_MASK	511

#define COPYSIZE16	16
#define COPYSIZE16_MASK	15

/*
   ij.jang : GCC 4.2.x (not checked with other versions)
 no inline this function!!!,
 clobber list will not effect if this codes are embedded,
 and will brake the stack */
static void memcpy128_32(void *dst, void* src, unsigned int len)
	__attribute__((noinline));

static void memcpy128_32(void *dst, void* src, unsigned int len)
{
	asm volatile (
		"1:"	"\n\t"
		"ldmia	%1!, {r0-r7}"	"\n\t"
		"stmia	%0!, {r0-r7}"	"\n\t"
		"ldmia	%1!, {r0-r7}"	"\n\t"
		"stmia	%0!, {r0-r7}"	"\n\t"
		"ldmia	%1!, {r0-r7}"	"\n\t"
		"stmia	%0!, {r0-r7}"	"\n\t"
		"ldmia	%1!, {r0-r7}"	"\n\t"
		"stmia	%0!, {r0-r7}"	"\n\t"
		
		"subs	%2, %2, #128"	"\n\t"
		"bne	1b"		"\n\t"
		:"=r"(dst), "=r"(src), "=r"(len)
		:"0"(dst), "1"(src), "2"(len)
		: "cc", "memory",
		  "r0", "r1", "r2", "r3",
		  "r4", "r5", "r6", "r7"
	);
}

/* copy by 32 bytes, ldm/stm by 16 bytes */
static void memcpy32_16(void *dst, void* src, unsigned int len)
{
	asm volatile (
		"1:"	"\n\t"
		"ldmia	%1!, {r0-r3}"	"\n\t"
		"stmia	%0!, {r0-r3}"	"\n\t"
		"ldmia	%1!, {r0-r3}"	"\n\t"
		"stmia	%0!, {r0-r3}"	"\n\t"
		
		"subs	%2, %2, #32"	"\n\t"
		"bne	1b"		"\n\t"
		:"=r"(dst), "=r"(src), "=r"(len)
		:"0"(dst), "1"(src), "2"(len)
		: "cc", "memory",
		  "r0", "r1", "r2", "r3"
	);
}

/* copy by 16 bytes, ldm/stm by 16 bytes */
static void memcpy16_16(void *dst, void* src, unsigned int len)
{
	asm volatile (
		"1:"	"\n\t"
		"ldmia	%1!, {r0-r3}"	"\n\t"
		"stmia	%0!, {r0-r3}"	"\n\t"
		
		"subs	%2, %2, #16"	"\n\t"
		"bne	1b"		"\n\t"
		:"=r"(dst), "=r"(src), "=r"(len)
		:"0"(dst), "1"(src), "2"(len)
		: "cc", "memory",
		  "r0", "r1", "r2", "r3"
	);
}

/* copy by 16 bytes, ldm/stm by 8 bytes */
static void memcpy16_8(void *dst, void* src, unsigned int len)
{
	asm volatile (
		"1:"	"\n\t"
		"ldmia	%1!, {r0-r1}"	"\n\t"
		"stmia	%0!, {r0-r1}"	"\n\t"
		"ldmia	%1!, {r0-r1}"	"\n\t"
		"stmia	%0!, {r0-r1}"	"\n\t"
		
		"subs	%2, %2, #16"	"\n\t"
		"bne	1b"		"\n\t"
		:"=r"(dst), "=r"(src), "=r"(len)
		:"0"(dst), "1"(src), "2"(len)
		: "cc", "memory",
		  "r0", "r1"
	);
}

/* copy by 8 bytes, ldm/stm by 8 bytes */
static void memcpy8_8(void *dst, void* src, unsigned int len)
{
	asm volatile (
		"1:"	"\n\t"
		"ldmia	%1!, {r0-r1}"	"\n\t"
		"stmia	%0!, {r0-r1}"	"\n\t"
		
		"subs	%2, %2, #8"	"\n\t"
		"bne	1b"		"\n\t"
		:"=r"(dst), "=r"(src), "=r"(len)
		:"0"(dst), "1"(src), "2"(len)
		: "cc", "memory",
		  "r0", "r1"
	);
}

/* copy by 4 bytes, using ldr/str */
static void memcpy4_4(void *dst, void* src, unsigned int len)
{
	asm volatile (
		"mov	r1, #0"		"\n\t"
		"1:"	"\n\t"
		"ldr	r0, [%0, r1]"	"\n\t"
		"str	r0, [%1, r1]"	"\n\t"
		"add	r1, r1, #4"	"\n\t"
		"subs	%2, %2, #4"	"\n\t"
		"bne	1b"		"\n\t"
		:"=r"(dst), "=r"(src), "=r"(len)
		:"0"(dst), "1"(src), "2"(len)
		: "cc", "memory",
		  "r0", "r1"
	);
}

typedef void(*MEM_COPY_FUNC)(void *, void*, unsigned int);

struct mem_copy_tbl
{
	unsigned int copy_unit;
	unsigned int access_unit;
	MEM_COPY_FUNC copy_func;
};

#define MEM_COPY_TBL_ENTRY(copy, access)	{ copy, access, memcpy##copy##_##access }

#if !defined(ARRAY_SIZE)
#define ARRAY_SIZE(array)	(sizeof(array)/sizeof(array[0]))
#endif

struct mem_copy_tbl mem_copy_opinfo[] = {
	MEM_COPY_TBL_ENTRY (128,32),
	MEM_COPY_TBL_ENTRY (32, 16),
	MEM_COPY_TBL_ENTRY (16,16),
	MEM_COPY_TBL_ENTRY (16,8),
	MEM_COPY_TBL_ENTRY (8,8),
	MEM_COPY_TBL_ENTRY (4,4),
};

static int cpm(void *src, void *dst, unsigned int len, struct mem_copy_tbl* op)
{
	unsigned int t1, t2;
	unsigned int smask = op->copy_unit - 1;
	
	/* check argument */
	if( (((unsigned int)src) & smask) ||
		(((unsigned int)dst) & smask) ||
		(len & smask))
	{
		printf ("Misaligned!\n");
		return -1;
	}

	printf ("Copy 0x%08X-->0x%08X, copy 0x%08X, copy by %d bytes, load/save by %d bytes\n",
			(unsigned int)src, (unsigned int)dst, len,
			op->copy_unit, op->access_unit);

	/* start */
	t1 = *(volatile unsigned int*)rTSD_STC_BASE;
	op->copy_func(src, dst, len);
	t2 = *(volatile unsigned int*)rTSD_STC_BASE;

	printf ("Completed, STC 45Khz value diff(%d ~ %d) = %d\n", t1, t2, t2-t1);
	puts ("Compare...");
	if (memcmp (src, dst, len)) {
		puts ("Different!\n");
	} else {
		puts ("Same!\n");
	}

	return 0;
}

static int do_cpm (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int i;
	unsigned int cunit, aunit, src, dst, len;

	if (argc < 6) {
		printf ("USAGE: %s\n", cmdtp->usage);
		return 0;
	}

	cunit = simple_strtoul(argv[1], NULL, 10);
	aunit = simple_strtoul(argv[2], NULL, 10);
	src = simple_strtoul(argv[3], NULL, 16);
	dst = simple_strtoul(argv[4], NULL, 16);
	len = simple_strtoul(argv[5], NULL, 16);

	for (i=0; i<ARRAY_SIZE(mem_copy_opinfo); i++) {
		if (cunit == mem_copy_opinfo[i].copy_unit &&
			cunit == mem_copy_opinfo[i].access_unit) {
			cpm ((void*)src, (void*)dst, len, &mem_copy_opinfo[i]);
			return 0;
		}
	}

	/* no such a function */
	puts ("Supported memcpy functions:\n");
	for (i=0; i<ARRAY_SIZE(mem_copy_opinfo); i++) {
		printf ("cpm %d %d ...\n", mem_copy_opinfo[i].copy_unit,
				mem_copy_opinfo[i].access_unit);
	}

	return 1;
}

U_BOOT_CMD (
	cpm, 6, 6, do_cpm,
	"cpm	<copy size> <access size> <source> <dest> <len>	- copy memory by ldm/stm\n",
	"memory copy test\n"
	"	<copy size> - copy size in bytes(decimal) within 1 loop\n"
	"	<access size> - ARM ldr/str size(1,2,4,8,16,32), decimal\n"
	"	<source>, <dest>, <len> copy <source> to <dest> address <len> bytes, hexa\n"
);

