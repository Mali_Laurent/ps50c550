#ifndef _DTV_BML_BAYHILL_H_
#define _DTV_BML_BAYHILL_H_

#include "dtv_bml.h"

/* Include "BML.H" before. */
#define DTV_DEF_PARTS \
        { PARTITION_ID_NBL2, 1, BML_PI_ATTR_RO, "boot loader", 0x20100000 }, \
        { PARTITION_ID_NBL3, 6, BML_PI_ATTR_RO, "u-boot", 0x20200000}, \
        { PARTITION_USER_DEF_BASE+1, 1, BML_PI_ATTR_RW, "u-boot env", 0x20300000}, \
        { PARTITION_USER_DEF_BASE, 1, BML_PI_ATTR_RW, "update util", 0x20300000}, \
        { PARTITION_ID_COPIEDOS, 48, BML_PI_ATTR_RO, "kernel image", 0x21000000}, \
        { PARTITION_ID_FILESYSTEM, 80, BML_PI_ATTR_RO, "root file system", 0x22000000}, \
        { PARTITION_ID_FILESYSTEM1, 32, BML_PI_ATTR_RO, "S5H2150 drivers", 0x23000000}, \
        { PARTITION_ID_FILESYSTEM2, 80, BML_PI_ATTR_RO, "application", 0x24000000}, \
        { PARTITION_ID_FILESYSTEM3, 160, BML_PI_ATTR_RW, "application data", 0x25000000}, \
        { PARTITION_ID_FILESYSTEM4, 80, BML_PI_ATTR_RW, "channel map", 0x26000000}, \
	{ PARTITION_ID_END, 0, 0, 0, 0}
#endif
