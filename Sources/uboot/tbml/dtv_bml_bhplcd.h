#ifndef _DTV_BML_BHPLCD_H_
#define _DTV_BML_BHPLCD_H_

#include "dtv_bml.h"

#if 0
/* Include "BML.H" before. */
#define DTV_DEF_PARTS \
        { PARTITION_ID_NBL2, 1, BML_PI_ATTR_RW, "boot loader", 0x20100000 }, \
        { PARTITION_ID_NBL3, 3, BML_PI_ATTR_RW, "u-boot", 0x20200000}, \
        { PARTITION_USER_DEF_BASE+1, 1, BML_PI_ATTR_RW, "u-boot env", 0x20300000}, \
        { PARTITION_USER_DEF_BASE, 1, BML_PI_ATTR_RW, "update util", 0x20400000}, \
        { PARTITION_ID_COPIEDOS, 24, BML_PI_ATTR_RW, "kernel image", 0x21000000}, \
        { PARTITION_ID_FILESYSTEM, 64, BML_PI_ATTR_RW, "root file system", 0x22000000}, \
        { PARTITION_ID_FILESYSTEM1, 32, BML_PI_ATTR_RW, "S4LX003X drivers", 0x24000000}, \
        { PARTITION_ID_FILESYSTEM2, 80, BML_PI_ATTR_RW, "application", 0x25000000}, \
        { PARTITION_ID_FILESYSTEM3, 160, BML_PI_ATTR_RW, "application data", 0x26000000}, \
        { PARTITION_ID_FILESYSTEM4, 104, BML_PI_ATTR_RW, "channel map", 0x27000000}, \
	{ PARTITION_ID_END, 0, 0, 0, 0}
#else
#define DTV_DEF_PARTS \
        { PARTITION_ID_NBL2, 1, BML_PI_ATTR_RW, "boot loader", 0x20100000, "onboot.bin" }, \
        { PARTITION_ID_NBL3, 3, BML_PI_ATTR_RW, "u-boot", 0x20200000, "uboot.bin"}, \
        { PARTITION_USER_DEF_BASE+1, 1, BML_PI_ATTR_RW, "u-boot env", 0x20300000, "env.bin"}, \
        { PARTITION_USER_DEF_BASE, 1, BML_PI_ATTR_RW, "update util", 0x20400000, "onw.bin"}, \
        { PARTITION_ID_COPIEDOS, 24, BML_PI_ATTR_RW, "kernel image", 0x21000000, "uImage"}, \
        { PARTITION_ID_FILESYSTEM, 32, BML_PI_ATTR_RW, "root file system", 0x21300000, "rootfs.img"}, \
	{ PARTITION_ID_FILESYSTEM1, 11, BML_PI_ATTR_RW, "boot", 0x21700000, "boot.img"}, \
        { PARTITION_ID_FILESYSTEM2, 14, BML_PI_ATTR_RW, "S4LX003X drivers", 0x24000000, "chip.img"}, \
        { PARTITION_ID_FILESYSTEM3, 50, BML_PI_ATTR_RW, "application", 0x24500000, "exe.img"}, \
	{ PARTITION_ID_FILESYSTEM4, 100, BML_PI_ATTR_RW, "application data", 0x24c00000, "appdata.img"}, \
	{ PARTITION_ID_FILESYSTEM5, 14, BML_PI_ATTR_RW, "S4LX003X drivers 2", 0x26000000, "chip2.img"}, \
        { PARTITION_ID_FILESYSTEM6, 50, BML_PI_ATTR_RW, "application 2", 0x26500000, "exe2.img"}, \
        { PARTITION_ID_FILESYSTEM7, 100, BML_PI_ATTR_RW, "application data 2", 0x26c00000, "appdata2.img"}, \
        { PARTITION_ID_FILESYSTEM8, 40, BML_PI_ATTR_RW, "rwarea", 0x22500000, "rwarea.img"}, \
	{ PARTITION_ID_FILESYSTEM9, 40, BML_PI_ATTR_RW, "dcm", 0x22a00000, "dcm.img"}, \
	{ PARTITION_ID_END, 0, 0, 0, 0}
#endif

#endif
