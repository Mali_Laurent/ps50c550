#ifndef _DTV_BML_POLARIS_H_
#define _DTV_BML_POLARIS_H_

#include "dtv_bml.h"

/* Include "BML.H" before. */
#define DTV_DEF_PARTS \
        { PARTITION_ID_NBL2, 1, BML_PI_ATTR_RO, "boot loader", 0x20100000 }, \
        { PARTITION_ID_NBL3, 3, BML_PI_ATTR_RO, "u-boot", 0x20200000}, \
        { PARTITION_USER_DEF_BASE, 1, BML_PI_ATTR_RW, "u-boot environment data", 0x20300000}, \
        { PARTITION_ID_COPIEDOS, 320, BML_PI_ATTR_RW, "kernel image", 0x21000000}, \
        { PARTITION_ID_FILESYSTEM, 160, BML_PI_ATTR_RW, "root file system", 0x22000000}, \
	{ PARTITION_ID_END, 0, 0, 0, 0}

#endif
