/**
 * @file    tbml_type.h
 * @brief   This file is header file of TinyBML type
 *
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 *
 */

#ifndef _TBML_TYPES_H_
#define _TBML_TYPES_H_

/*****************************************************************************/
/* Constant definition                                                       */
/*****************************************************************************/
#define     DEVS_PER_VOL            (XSR_MAX_DEV / XSR_MAX_VOL)
#define     DEVS_PER_VOL_SHIFT      (2)

/*****************************************************************************/
/* maximum number of blocks in Erase Refresh List							 */
/*****************************************************************************/
#define		MAX_ERL_ITEM			(126)		

/*****************************************************************************/
/* maximum string length of ERL signature									 */
/*****************************************************************************/
#define		MAX_ERL_SIG				(6)

/*****************************************************************************/
/* maximum string length of PCH signature                                    */
/*****************************************************************************/
#define     MAX_PCH_SIG             (8)

/*****************************************************************************/
/* maximum number of Pool Control Block                                      */
/*****************************************************************************/
#define     MAX_PCB                 (2)

/*****************************************************************************/
/* maximum number of Block Map Field per BMP                                 */
/*****************************************************************************/
#define     BMFS_PER_BMS            (127)

/*
 * data structure for BMF(Block Map Field)
 */
typedef struct {
    unsigned short	sbn;	/* Semi physical Block Number */
    unsigned short	rbn;	/* Replaced Block Number in BMI structure
                               Replaced Block offset in BMS structure */
} bmf;

/*
 * data structure for BMI(Block Map Info)
 */
typedef struct {
    unsigned short	num_of_bmfs;	/* the number of BMFs */
    unsigned short	lbmiage;		/* the age of LBMI */
    unsigned short	ubmiage;		/* the age of UBMI */
    bmf	*pst_bmf;           /* Block Map Field Array */
} bmi;

/*
 * data structure for BMS(Block Map Sector)
 */
typedef struct {
    unsigned short     nInf;	/* nInf field shows a cause why BMS is created*/
    unsigned short     nAge;	/* the age of BMS */
    bmf	st_bmf[BMFS_PER_BMS];	/* Block Map Field Array */
} bms;

typedef struct {
    unsigned short     n1stBMFIdx;
    unsigned short     num_of_bmfs;
} badunit;

/*
 * reservoir data structure for storing the info
 */
typedef struct {
    unsigned short	alloc_lockarea;		/* allocated block pointer for Locked Area */
    unsigned short	alloc_unlockarea;	/* allocated block pointer for Unlocked Area */

    unsigned short	cur_upcb_idx;		/* current UPCB index */
    unsigned short	cur_lpcb_idx;		/* current LPCB index */

    unsigned short	upcb[MAX_PCB];		/* Unlocked Pool Control Block number */
    unsigned short	lpcb[MAX_PCB];		/* Locked Pool Control Block number */

    unsigned short	upcb_age;			/* Age of latest UPCB */
    unsigned short	lpcb_age;			/* Age of latest LPCB */

    unsigned short	next_ubms_off;		/* next LBMP offset in BMSG of current PCB */
    unsigned short	next_lbms_off;		/* next LBMP offset in BMSG of current PCB */

    /* Sorts by ascending power of stBMI.pstBMF[x]->nSbn */
    bmi	st_bmi;							/* Block Map Page */

    unsigned char	*pbad_bit_map;		/* Block Allocation Bit Map in Reservoir */

    badunit   *pbad_unit_map;	/* Bad Unit Map in User Area                 
                                   This field is used 
                                   to decrease a loop-up time of BMF         */
} reservoir;


/* 
 * typedefs for Pool Control Header
 * This size of PCH should be 512 Bytes
 */
typedef struct {    
    unsigned char	signature[MAX_PCH_SIG];			/* "LOCKPCHD" / "ULOCKPCH" */
    unsigned short	age;							/* Age of PCH */
    unsigned short	alterpcb;						/* Pbn of alternative PCB */
    unsigned int	erase_sig1;						/* 1st Erase Signature */
    unsigned int	erase_sig2;						/* 2nd Erase Signature */
    unsigned char	aPad[XSR_SECTOR_SIZE - 4 * 3 - MAX_PCH_SIG]; /* reserved */
} pch;

/*
 * data type structure for device information
 */
typedef struct {
    unsigned short	dev_no;			/* physical device number */
    unsigned short	n1st_usbn;		/* 1stSbn of Unlocked Area */
    unsigned short	n1st_upbn;		/* 1stPbn of Unlocked Area */
    unsigned short	blks_in_unlock;	/* number of blocks in Unlocked Area */

    reservoir		st_meta;		/* reservoir structure */

    unsigned char	*ptmp_mbuf;		/* temporary main buffer */
    unsigned char	*ptmp_sbuf;		/* temporary spare buffer */
} flash_dev;

/*
 * data type structure for volume information 
 */
typedef struct {
	unsigned int	vol_open;			/* volume open flag*/
	unsigned int	sm;					/* handle # for semaphore */
	unsigned int	open_cnt;			/* volume open count*/
	unsigned int	num_of_device;		/* number of devices in volume */
	unsigned int	num_of_useblk;		/* number of usable blocks in volume */
	unsigned int 	pre_program;		/* pre-programming en-dis/able */

	unsigned short	blks_per_dev;		/* blocks per device */
	unsigned short	resrv_per_dev;		/* number of blocks in reservoir per device */

	unsigned short	scts_per_blk;		/* sectors per block */
	unsigned short	pgs_per_blk;		/* pages per block */

	unsigned short	n1stsbn_resrv;		/* 1stSbn of  Reservoir */
	unsigned char	lsn_pos;			/*  offset of Lsn in spara area */
	unsigned char	bad_pos;			/*  offset of bad position in spare area */
	unsigned char	ecc_pos;            /*  ECC position */

	unsigned char	scts_per_pg;		/*  sectors per page */
	unsigned char	mask_sctsperpg;		/*  mod value for sectors per page */
	unsigned char	shift_sctsperpg;	/*  mod value for sectors per page */

	unsigned char	planes_per_dev;		/*  plane per device */
	unsigned char	shift_sctsperblk;	/*  shift value for sectors per block */
	unsigned char	shift_numofdev;		/*  shift value for number of devices   
										in volume */
	unsigned char	reserved;
	unsigned short	ecc_flag;			/* ECC policy(Only H/W ECC) */

	int (*lld_init)(void *pParm);
	int (*lld_open)(unsigned int dev);
	int (*lld_close)(unsigned int dev);   
	int (*lld_read)(unsigned int dev, unsigned int nPsn, unsigned int nScts, 
					unsigned char *pMBuf, unsigned char *pSBuf, unsigned int nFlag);
	int (*lld_write)(unsigned int dev, unsigned int nPsn, unsigned int nScts, 
					unsigned char *pMBuf, unsigned char *pSBuf, unsigned int nFlag);
	int (*lld_erase)(unsigned int dev, unsigned int Pbn, unsigned int nFlag);
	int (*lld_dev_info)(unsigned int dev, lldspec *pstDevInfo);
	int (*lld_check_bad)(unsigned int dev, unsigned int Pbn);
	int (*lld_flush)(unsigned int dev); 
	int (*lld_set_rw)(unsigned int dev, unsigned int nSUbn, unsigned int nUBlks);
	int (*lld_ioctl_cmd)(unsigned int vol, unsigned int flag, unsigned char *pbuf_in, unsigned int in_len,
					unsigned char *pbuf_out, unsigned int out_len, unsigned int *byte_ret);
    int (*lld_mread)(unsigned int dev, unsigned int nPsn, unsigned int nScts,
						SGL  *pstSGL, unsigned char *pSBuf, unsigned int nFlag);
	
	part_info	*pi;				/* partition information                 */
	part_exinfo	*pie;			/* partition information extension       */

} flash_vol;

#endif /* _TBML_TYPES_H_ */
