/**
 * @file    tbml_interface.h
 * @brief   This file is header file of TinyBML intefave
 *
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 *
 */

#ifndef _TBML_INTERFACE_H_
#define _TBML_INTERFACE_H_

#include "onenand_lld.h"
/*****************************************************************************/
/* Common Constant definitions                                               */
/*****************************************************************************/
#define     BML_MAX_REQSEC                  (4)
#define     BML_MAX_PARTENTRY               ((512 - 16) / 16)
#define     BML_MAX_PART_SIG                (8)
#define		BML_MAX_ERL_PROCESS				(16)

/*****************************************************************************/
/* flag value of BML_ReadSector, BML_WriteSector,                           */
/*                BML_CopyBack, BML_EraseBlk                                 */
/*****************************************************************************/
#define     BML_FLAG_SYNC_MASK              (1 << 0)
#define     BML_FLAG_ASYNC_OP               (1 << 0)    /* Write/Erase/Copy  */
#define     BML_FLAG_SYNC_OP                (0 << 0)    /* Write/Erase/Copy  */
#define     BML_FLAG_SYNC_OP_INV            (~BML_FLAG_ASYNC_OP) 

#define     BML_FLAG_ECC_MASK               (1 << 1)
#define     BML_FLAG_ECC_ON                 (1 << 1)    /* Read/Write/Copy   */
#define     BML_FLAG_ECC_OFF                (0 << 1)    /* Read/Write/Copy   */
#define     BML_FLAG_ECC_OFF_INV            (~BML_FLAG_ECC_ON)

#define     BML_FLAG_BBM_MASK               (1 << 2)
#define     BML_FLAG_BBM_OFF                (1 << 2)    /* Read              */
#define     BML_FLAG_BBM_ON                 (0 << 2)    /* Read              */
#define     BML_FLAG_BBM_ON_INV             (~BML_FLAG_BBM_OFF)

/*****************************************************************************/
/* flag value of BML_FlushOp                                                */
/*****************************************************************************/
#define     BML_NORMAL_MODE                 (0 << 3)
#define     BML_EMERGENCY_MODE              (1 << 3)

/*****************************************************************************/
/* Return value of BML_XXX()                                                 */
/*****************************************************************************/
#define     BML_SUCCESS                     XSR_RETURN_VALUE(0, 0x0000, 0x0000)
#define     BML_CRITICAL_ERROR              XSR_RETURN_VALUE(1, 0x0001, 0x0000)
#define     BML_READ_ERROR                  XSR_RETURN_VALUE(1, 0x0002, 0x0000)
#define     BML_INVALID_DATA_ERROR          XSR_RETURN_VALUE(1, 0x0003, 0x0000)
#define     BML_ALREADY_OPENED              XSR_RETURN_VALUE(1, 0x0004, 0x0000)
#define     BML_WR_PROTECT_ERROR            XSR_RETURN_VALUE(1, 0x0005, 0x0000)
#define     BML_INVALID_PARAM               XSR_RETURN_VALUE(1, 0x0006, 0x0000)
#define     BML_NO_PIENTRY                  XSR_RETURN_VALUE(1, 0x0007, 0x0000)
#define     BML_STORE_PIEXT_ERROR           XSR_RETURN_VALUE(1, 0x0008, 0x0000)
#define     BML_MAKE_RSVR_ERROR             XSR_RETURN_VALUE(1, 0x0009, 0x0000)
#define     BML_DEVICE_ACCESS_ERROR         XSR_RETURN_VALUE(1, 0x000A, 0x0000)
#define     BML_UNFORMATTED                 XSR_RETURN_VALUE(1, 0x000B, 0x0000)
#define     BML_UNSUPPORTED_IOCTL           XSR_RETURN_VALUE(1, 0x000C, 0x0000)
#define     BML_CANT_LOCK_FOREVER           XSR_RETURN_VALUE(1, 0x000D, 0x0000)
#define     BML_CANT_UNLOCK_WHOLEAREA       XSR_RETURN_VALUE(1, 0x000E, 0x0000)
#define     BML_VOLUME_ALREADY_LOCKTIGHT    XSR_RETURN_VALUE(1, 0x000F, 0x0000)
#define     BML_CANT_CHANGE_PART_ATTR       XSR_RETURN_VALUE(1, 0x0010, 0x0000)
#define     BML_VOLUME_NOT_OPENED           XSR_RETURN_VALUE(1, 0x0011, 0x0000)
#define     BML_ALREADY_INITIALIZED         XSR_RETURN_VALUE(1, 0x0012, 0x0000)
#define     BML_ACQUIRE_SM_ERROR            XSR_RETURN_VALUE(1, 0x0013, 0x0000)
#define     BML_RELEASE_SM_ERROR            XSR_RETURN_VALUE(1, 0x0014, 0x0000)
#define     BML_OAM_ACCESS_ERROR            XSR_RETURN_VALUE(1, 0x0015, 0x0000)
#define     BML_PAM_ACCESS_ERROR            XSR_RETURN_VALUE(1, 0x0016, 0x0000)
#define     BML_CANT_LOCK_BLOCK             XSR_RETURN_VALUE(1, 0x0017, 0x0000)
#define		BML_ERASE_REFRESH_FAIL			XSR_RETURN_VALUE(1, 0x0018, 0x0000)
#define		BML_CANT_UNLOCK_BLOCK			XSR_RETURN_VALUE(1, 0x0019, 0x0000)
#define		BML_SKIP_ERASE_REFRESH			XSR_RETURN_VALUE(1, 0x001A, 0x0000)

/*****************************************************************************/
/* Minor Return value of BML_READ_ERROR and BML_INVALID_DATA_ERROR           */
/*****************************************************************************/
#define     BML_READ_ERROR_S0               XSR_RETURN_VALUE(0, 0x0000, 0x0002)
#define     BML_READ_ERROR_M0               XSR_RETURN_VALUE(0, 0x0000, 0x0008)
#define     BML_READ_ERROR_S1               XSR_RETURN_VALUE(0, 0x0000, 0x0020)
#define     BML_READ_ERROR_M1               XSR_RETURN_VALUE(0, 0x0000, 0x0080)
#define     BML_READ_ERROR_S2               XSR_RETURN_VALUE(0, 0x0000, 0x0200)
#define     BML_READ_ERROR_M2               XSR_RETURN_VALUE(0, 0x0000, 0x0800)
#define     BML_READ_ERROR_S3               XSR_RETURN_VALUE(0, 0x0000, 0x2000)
#define     BML_READ_ERROR_M3               XSR_RETURN_VALUE(0, 0x0000, 0x8000)

/* Previous Operation Flag */
/* BML_PREV_OP_RESULT is combined with BML_WR_PROTECT_ERROR, and means 
   Previous Operation Error of BML_WR_PROTECT_ERROR */
#define     BML_PREV_OP_RESULT              XSR_RETURN_VALUE(0, 0x0000, 0xF0F0)

/*****************************************************************************/
/* Partition Entry ID of BML_LoadPIEntry()                                   */
/* Partition Entry ID from 0 to 0x0FFFFFFF is reserved in BML                */
/* Following ID is the pre-defined value and User can use Partition Entry ID */
/* from PARTITION_USER_DEF_BASE                                              */
/*****************************************************************************/
#define     PARTITION_ID_NBL1               0  /* NAND bootloader stage 1    */
#define     PARTITION_ID_NBL2               1  /* NAND bootloader stage 2    */
#define     PARTITION_ID_NBL3               2  /* NAND bootloader stage 3    */
#define     PARTITION_ID_COPIEDOS           3  /* OS image copied from NAND 
                                                  flash memory to RAM        */
#define     PARTITION_ID_DEMANDONOS         4  /* OS image loaded on demand  */

#define     PARTITION_ID_FILESYSTEM         8  /* file system 0              */
#define     PARTITION_ID_FILESYSTEM1        9  /* file system 1              */
#define     PARTITION_ID_FILESYSTEM2        10 /* file system 2              */
#define     PARTITION_ID_FILESYSTEM3        11 /* file system 3              */
#define     PARTITION_ID_FILESYSTEM4        12 /* file system 4              */
#define     PARTITION_ID_FILESYSTEM5        13 /* file system 5              */
#define     PARTITION_ID_FILESYSTEM6        14 /* file system 6              */
#define     PARTITION_ID_FILESYSTEM7        15 /* file system 7              */
#define     PARTITION_ID_FILESYSTEM8        16 /* file system 8              */
#define     PARTITION_ID_FILESYSTEM9        17 /* file system 9              */


#define     PARTITION_USER_DEF_BASE         0x10000000 /* partition id base for
                                                  user definition            */

/*****************************************************************************/
/* flag of BML_Format()                                                     */
/*****************************************************************************/
#define     BML_INIT_FORMAT                 0
#define     BML_REPARTITION                 1

/*****************************************************************************/
/* value of nAttr of XSRPartEntry structure                                  */
/* nAttr can be 'BML_PI_ATTR_FROZEN + BML_PI_ATTR_RO' or                     */
/*              'BML_PI_ATTR_RO'                      or                     */
/*              'BML_PI_ATTR_RW'.                                            */
/* other value is invalid attribute.                                         */
/*****************************************************************************/
#define     BML_PI_ATTR_FROZEN              0x00000020
#define     BML_PI_ATTR_RO                  0x00000002
#define     BML_PI_ATTR_RW                  0x00000001

/*****************************************************************************/
/* maximum nSizeOfData of XSRPIExt structure                                 */
/*****************************************************************************/
#define     BML_MAX_PIEXT_DATA              (504)

/*****************************************************************************/
/* BML IO Control Code                                                       */
/*****************************************************************************/
#define     BML_READ_RETRY_CNT          1
#define     BML_ASSERT(exp)

//#if defined(XSR_NW)
/*****************************************************************************/
/*  BML_IOCtl(vol, BML_IOCTL_UNLOCK_WHOLEAREA, NULL, 0, NULL, 0, &nRet)     */
/*****************************************************************************/
#define     BML_IOCTL_UNLOCK_WHOLEAREA      XSR_IOCTL_CODE(XSR_MODULE_BML,  \
                                                    0,                      \
                                                    XSR_METHOD_IN_DIRECT,   \
                                                    XSR_WRITE_ACCESS)

/*****************************************************************************/
/*  unsigned int  nSbn;                                                      */
/*  unsigned int  nBlks;                                                     */
/*  unsigned int  nRet;                                                      */
/*  unsigned int  vol;                                                       */
/*  unsigned char  *pBufIn                                                   */
/*                                                                           */
/*  vol      = 0;                                                            */
/*  nDevIdx   = 0;                                                           */
/*                                                                           */
/*  *(unsigned int*)pBufIn = nSbn;                                           */
/*  *((unsigned int*)(pBufIn + 4)) = nBlks;                                  */
/*                                                                           */
/*  BML_IOCtl(vol, BML_IOCTL_SET_BLOCK_UNLOCK, pBufIn, 0,                    */
/*                                              NULL, 0, &nRet)              */
/*****************************************************************************/
#define     BML_IOCTL_SET_BLOCK_UNLOCK        XSR_IOCTL_CODE(XSR_MODULE_BML,\
                                                    1,                      \
                                                    XSR_METHOD_BUFFERED,    \
                                                    XSR_WRITE_ACCESS)

                                                    
//#endif /* XSR_NW */

/*****************************************************************************/
/*  BML_IOCtl(vol, BML_IOCTL_LOCK_FOREVER, NULL, 0, NULL, 0, &nRet)          */
/*****************************************************************************/
#define     BML_IOCTL_LOCK_FOREVER          XSR_IOCTL_CODE(XSR_MODULE_BML,  \
                                                    2,                      \
                                                    XSR_METHOD_IN_DIRECT,   \
                                                    XSR_WRITE_ACCESS)

/*****************************************************************************/
/*  unsigned int  nPartI[2];                                                 */
/*  unsigned int  nRet;                                                      */
/*  unsigned int  vol;                                                       */
/*                                                                           */
/*  vol      = 0;                                                            */
/*  nPartI[0] = PARTITION_ID_COPIEDOS;                                       */
/*  nPartI[1] = BML_PI_ATTR_RO;                                              */
/*                                                                           */
/*  BML_IOCtl(vol, BML_IOCTL_CHANGE_PART_ATTR, (unsigned char *) &nPartI,    */
/*            sizeof(unsigned int) * 2, NULL, 0, &nRet)                      */
/*****************************************************************************/
#define     BML_IOCTL_CHANGE_PART_ATTR      XSR_IOCTL_CODE(XSR_MODULE_BML,  \
                                                    3,                      \
                                                    XSR_METHOD_IN_DIRECT,   \
                                                    XSR_WRITE_ACCESS)

/*****************************************************************************/
/*  unsigned int  nDevIdx;                                                   */
/*  unsigned short  nBMF[512];                                               */
/*  unsigned int  nRet;                                                      */
/*  unsigned int  vol;                                                       */
/*                                                                           */
/*  vol      = 0;                                                            */
/*  nDevIdx   = 0;                                                           */
/*                                                                           */
/*  BML_IOCtl(vol, BML_IOCTL_GET_BMI, (unsigned char *) &nDevIdx, sizeof(unsigned int),   */
/*            (unsigned char *) nBMI, sizeof(unsigned int) * 256, &nRet)                   */
/*****************************************************************************/
#define     BML_IOCTL_GET_BMI               XSR_IOCTL_CODE(XSR_MODULE_BML,  \
                                                    4,                      \
                                                    XSR_METHOD_BUFFERED,    \
                                                    XSR_READ_ACCESS)

/*****************************************************************************/
/*  unsigned int  nWaitTime;                                                 */
/*  unsigned int  vol;                                                       */
/*  unsigned int  nRet;                                                      */
/*                                                                           */
/*  vol      = 0;                                                            */
/*  nWaitTime = 50;                                                          */
/*                                                                           */
/*  BML_IOCtl(vol, BML_IOCTL_SET_WTIME_FOR_ERR, (unsigned char *) &nWaitTime,*/
/*            sizeof(unsigned int), NULL, 0, &nRet)                          */
/*****************************************************************************/
#define     BML_IOCTL_SET_WTIME_FOR_ERR     XSR_IOCTL_CODE(XSR_MODULE_BML,  \
                                                    5,                      \
                                                    XSR_METHOD_IN_DIRECT,   \
                                                    XSR_WRITE_ACCESS)

/*****************************************************************************/
/*  unsigned int   vol;                                                      */
/*  unsigned int   nRet;                                                     */
/*  part_info stPI;                                                          */
/*                                                                           */
/*  vol      = 0;                                                            */
/*                                                                           */
/*  BML_IOCtl(vol, BML_IOCTL_GET_FULL_PI, NULL, 0,                           */
/*            stPI, sizeof(part_info), &nRet)                                */
/*****************************************************************************/
#define     BML_IOCTL_GET_FULL_PI           XSR_IOCTL_CODE(XSR_MODULE_BML,  \
                                                    6,                      \
                                                    XSR_METHOD_BUFFERED,    \
                                                    XSR_READ_ACCESS)

/*****************************************************************************/
/*  unsigned int  nDevIdx;                                                   */
/*  unsigned int  nRet;                                                      */
/*  unsigned int  vol;                                                       */
/*  unsigned int  nCnt;                                                      */
/*                                                                           */
/*  vol      = 0;                                                            */
/*  nDevIdx   = 0;                                                           */
/*                                                                           */
/*  BML_IOCtl(vol, BML_IOCTL_INVALID_BLOCK_CNT, (unsigned char *) &nDevIdx,  */
/*            sizeof(unsigned int), (unsigned char *) &nCnt, sizeof(unsigned int), &nRet)*/
/*****************************************************************************/
#define     BML_IOCTL_INVALID_BLOCK_CNT     XSR_IOCTL_CODE(XSR_MODULE_BML,  \
                                                    7,                      \
                                                    XSR_METHOD_IN_DIRECT,   \
                                                    XSR_READ_ACCESS)

/*****************************************************************************/
/*  unsigned int  nSbn;                                                      */
/*  unsigned int  nBlks;                                                     */
/*  unsigned int  nRet;                                                      */
/*  unsigned int  vol;                                                       */
/*  unsigned char  *pBufIn;                                                  */
/*                                                                           */
/*  vol      = 0;                                                            */
/*  nDevIdx   = 0;                                                           */
/*                                                                           */
/*  *(unsigned int*)pBufIn = nSbn;                                           */
/*  *((unsigned int*)(pBufIn + 4)) = nBlks;                                  */
/*                                                                           */
/*  BML_IOCtl(vol, BML_IOCTL_SET_BLOCK_LOCK, pBufIn, 0,                      */
/*                                              NULL, 0, &nRet)              */
/*****************************************************************************/
#define     BML_IOCTL_SET_BLOCK_LOCK        XSR_IOCTL_CODE(XSR_MODULE_BML,  \
                                                    8,                      \
                                                    XSR_METHOD_BUFFERED,    \
                                                    XSR_WRITE_ACCESS)
/*
 * Tiny BML volume spec
 */
typedef struct {
    unsigned short		pgs_per_blk;	/* the number of pages per block */
    unsigned char		scts_per_pg;	/* the number of sectors per page */
    unsigned char		lsn_pos;		/* offset for storing LSN in spare area */
    unsigned char		ecc_pos;		/* offset for storing ECC value in spare area*/
    unsigned int		num_of_useblk;	/* the number of usable blocks in the volume */
    unsigned int		multi_erase;	/* MErase Policy */
    unsigned int		ecc_flag;		/* ECC policy */
    unsigned char		uniq_id[XSR_UID_SIZE]; /* Uniqure ID in OTP area of NAND device*/
} tbml_vol_spec;

/* 
 * partition entry structure
 */
typedef struct {
    unsigned int	id;				/* partition entry ID */
    unsigned int	attr;			/* Attribute */
    unsigned int	n1st_vbn;		/* 1st virtual block number */
    unsigned int	num_of_blks;	/* # of blocks */
} part_entry;

/*
 * Partition Information structure
 */
typedef struct {
    unsigned char		signature[BML_MAX_PART_SIG];	/* signature of partition information */
    unsigned int		version;		/* version of partition information */
    unsigned int        num_of_partentry;	/* # of partition entry */
    part_entry		pentry[BML_MAX_PARTENTRY];
} part_info;

/*
 * Partition Extension Information structure
 */
typedef struct {
    unsigned int	id;			/* nID of partition information extension */
    unsigned int	data_size;	/* size of pData */
    void			*pdata;		/* pointer of user-defined data structure */
} part_exinfo;


#ifndef CONFIG_RFS_TINYBML
/* This data structure is CopyBack operation */
typedef struct {
    unsigned int		nSrcVsn;       /* source virtual sector number */
    unsigned short		nRndInOffset;  /* spare area offset (0~15) for random-in */
                                /* Random-In of ScrArg is only used */
                                /* for spare area */
    unsigned short		nRndInBytes;   /* number of bytes for random-in */
    unsigned char		*pRndInBuf;     /* buffer pointer for random-in */
} SrcArg;

typedef struct {
    SrcArg			aSrcArg[XSR_MAX_REQSEC];
    unsigned int	nDstVsn;       /* destination virtual sector number */
    unsigned int	nCopyCnt;      /* number of CopyArg */
} CopyArg;
#endif 


/*
 * Tiny BML API fuction 
 */
/* sh.kim -start */
int tbml_init(unsigned int vol);
/* sh.kim - end */
int	tbml_open(unsigned int vol);
int	tbml_close(unsigned int vol);
int	tbml_format(unsigned int vol, part_info *pst_part_info, unsigned int flag);
int tbml_read(unsigned int  vol, unsigned int vsn, unsigned int scts_per_pg,
				unsigned char *pm_buf, unsigned char *ps_buf, unsigned int flag);
int tbml_write(unsigned int vol, unsigned int vsn, unsigned int scts_per_pg,
				unsigned char *pm_buf, unsigned char *ps_buf, unsigned int flag);
int tbml_eraseblk(unsigned int vol, unsigned int vbn, unsigned int flag);
int	tbml_get_vol_spec(unsigned int vol, tbml_vol_spec *pst_vol_spec);
int	tbml_load_partition(unsigned int  vol,  unsigned int id, part_entry *pst_part_entry);
int tbml_ioctl(unsigned int vol, unsigned int flag, unsigned char *pbuf_in, 
				unsigned int in_len, unsigned char *pbuf_out, unsigned int out_len,
				unsigned int *byte_ret);
int tbml_mread(unsigned int vol, unsigned int vsn, unsigned int num_of_scts,
				unsigned char *pm_buf, unsigned char *ps_buf, unsigned int nFlag);
#endif  /* _TBML_INTERFACE_H_ */
