#ifndef _DTV_BML_BAYHILL_H_
#define _DTV_BML_BAYHILL_H_

#include "dtv_bml.h"

/* Include "BML.H" before. */
#if 0
#define DTV_DEF_PARTS \
        { PARTITION_ID_NBL2, 1, BML_PI_ATTR_RW, "boot loader", 0x20100000 }, \
        { PARTITION_ID_NBL3, 3, BML_PI_ATTR_RW, "u-boot", 0x20200000}, \
        { PARTITION_USER_DEF_BASE+1, 1, BML_PI_ATTR_RW, "u-boot env", 0x20300000}, \
        { PARTITION_USER_DEF_BASE, 1, BML_PI_ATTR_RW, "update util", 0x20400000}, \
        { PARTITION_ID_COPIEDOS, 24, BML_PI_ATTR_RW, "kernel image", 0x21000000}, \
        { PARTITION_ID_FILESYSTEM, 64, BML_PI_ATTR_RW, "root file system", 0x22000000}, \
        { PARTITION_ID_FILESYSTEM1, 32, BML_PI_ATTR_RW, "S4LX003X drivers", 0x23000000}, \
        { PARTITION_ID_FILESYSTEM2, 80, BML_PI_ATTR_RW, "application", 0x28000000}, \
        { PARTITION_ID_FILESYSTEM3, 160, BML_PI_ATTR_RW, "application data", 0x29000000}, \
        { PARTITION_ID_FILESYSTEM4, 104, BML_PI_ATTR_RW, "channel map", 0x2a000000}, \
	{ PARTITION_ID_END, 0, 0, 0, 0}
#else
asdasd
#define DTV_DEF_PARTS \
        { PARTITION_ID_NBL2, 1, BML_PI_ATTR_RW, "boot loader", 0x20100000 }, \
        { PARTITION_ID_NBL3, 3, BML_PI_ATTR_RW, "u-boot", 0x20200000}, \
        { PARTITION_USER_DEF_BASE+1, 1, BML_PI_ATTR_RW, "u-boot env", 0x20300000}, \
        { PARTITION_USER_DEF_BASE, 1, BML_PI_ATTR_RW, "update util", 0x20400000}, \
        { PARTITION_ID_COPIEDOS, 24, BML_PI_ATTR_RW, "kernel image", 0x21000000}, \
        { PARTITION_ID_FILESYSTEM, 24, BML_PI_ATTR_RW, "root file system", 0x22000000}, \
		{ PARTITION_ID_FILESYSTEM1, 11, BML_PI_ATTR_RW, "boot", 0x22000000}, \
        { PARTITION_ID_FILESYSTEM2, 18, BML_PI_ATTR_RW, "S4LX003X drivers", 0x24000000}, \
        { PARTITION_ID_FILESYSTEM3, 50, BML_PI_ATTR_RW, "application", 0x25000000}, \
		{ PARTITION_ID_FILESYSTEM4, 100, BML_PI_ATTR_RW, "application data", 0x26000000}, \
		{ PARTITION_ID_FILESYSTEM5, 18, BML_PI_ATTR_RW, "S4LX003X drivers 2", 0x24000000}, \
        { PARTITION_ID_FILESYSTEM6, 50, BML_PI_ATTR_RW, "application 2", 0x25000000}, \
        { PARTITION_ID_FILESYSTEM7, 100, BML_PI_ATTR_RW, "application data 2", 0x26000000}, \
        { PARTITION_ID_FILESYSTEM8, 30, BML_PI_ATTR_RW, "rwarea", 0x27000000}, \
		{ PARTITION_ID_FILESYSTEM9, 40, BML_PI_ATTR_RW, "dcm", 0x27000000}, \
	{ PARTITION_ID_END, 0, 0, 0, 0}
#endif

#endif
