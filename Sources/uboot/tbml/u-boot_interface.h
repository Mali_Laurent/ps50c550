 /*
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * Technical support:
 *   Samsung Flash software Group
 *   daeho kim <daehosky.kim@samsung.com>
 */

#ifndef _BBML_H_
#define _BBML_H_

#undef CONFIG_DEBUG_INTERFACE

/*This is for u-boot*/
#define PRINT 	printf
#define U-BOOT_DEBUG
//#define BBML_DEBUG

#ifdef BBML_DEBUG
#define DPRINT(fmt, args...)\
do {									\
	PRINT("[%s+%d] " fmt "\n", __func__, __LINE__, ##args);	\
} while(0)		
#else
#define DPRINT(...)
#endif


#define MAX_PARTITION_NUM 	16  // darth
#define XSR_MAX_DEVICES		10 
#define SECTOR_BITS   		 9
/* ij.jang */
#if 0
#define KERNEL_BUFFER_START	0x81000000 
#define UPDATE_IMAGE_START	0x80008000 
#else
#define KERNEL_BUFFER_START	CFG_ONENAND_BUFADDR
#define UPDATE_IMAGE_START	CFG_ONENAND_ONWADDR
#endif

/* sh.kim - start */
#define CFG_ALL_PARTITION			100
#define CFG_ONENAND_INIT_FORMAT	99
#define CFG_ONENAND_REPART_FORMAT	98
#define CFG_ONENAND_ERASE			97
#define CFG_BBM_UPDATE				96
#define FKERNEL_IMAGE_LOAD_ADDR 	0x21c08000
#define ECC_ON	1
#define ECC_OFF 	0
/* sh.kim - end */

/* ij.jang */
#if 0
/* used for debug */
#define KERNEL_BUFFER		0x86200000
#define ORI_DATA		0x86000000
#endif

struct partition_entry {
    unsigned int id;         /* partition entry ID */
    unsigned int attr;       /* Attribute          */
    unsigned int first_blk;  /* 1st block number   */
    unsigned int no_blks;    /* # of blocks        */
} ;

struct partition_info{
    unsigned char signature[8];   /* signature of partition information*/
    unsigned int version;          /* version of partition information*/
    unsigned int no_part_entries;   /* # of partition entry*/
    struct partition_entry  entry[MAX_PARTITION_NUM];
};

/**
 * struct ufd_info - unified flash device information
 */
struct BML_Info{
	unsigned int	pgs_per_blk;
	unsigned int	scts_per_pg;
	unsigned int	num_of_useblk;
};


int bbml_open(int volume, struct BML_Info *device_type);
int bbml_close(int volume);
int bbml_read_block(int volume, unsigned int block, unsigned char *buf, unsigned int nFlag);
int bbml_load_partition(int volume, struct partition_info *pinfo);
#endif /*_BBML_H_*/
