/**
 * @file    tbml_interface.c
 * @brief   This file is interface file of TinyBML
 *
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 *
 */

#include <tbml_common.h>
#include <os_adapt.h>
#include <onenand_interface.h>
#include <onenand_lld.h>	
#include <tbml_interface.h>	
#include <tbml_type.h>			
#include <gbbm.h>

#ifdef CONFIG_RFS_TINYBML
#include <linux/module.h>
#include <linux/kernel.h>
#endif

/*****************************************************************************/
/* Macros                                                                    */
/*****************************************************************************/
/* ij.jang : override DEBUG */
#if 0
#undef DEBUG
#define DEBUG(level,fmt,...)	printf(fmt,##__VA_ARGS__)
#endif

#define 	VOLUME_CHECK(n)	(n > XSR_MAX_VOL ? 1 : 0)
#define		CHK_VOL_OPEN(VolOpen)                                           \
    if (VolOpen != TRUE32)                                                  \
    {                                                                       \
        DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR] volume(vol:%d) isn't opened\r\n", vol);  \
        return BML_VOLUME_NOT_OPENED;                                       \
    }
/*****************************************************************************/
/* Static variables definitions                                              */
/*****************************************************************************/
static flash_vol		st_volcxt[XSR_MAX_VOL];
static flash_dev		*pst_devcxt[XSR_MAX_DEV];
static lld_ftable		st_ftable[XSR_MAX_VOL];

/* sh.kim - start */
static unsigned int tbml_init_flag[2] = {FALSE32, FALSE32};
/* sh.kim - end */


/*****************************************************************************/
/* Definitions                                                               */
/*****************************************************************************/
#define GET_VOLCXT(v)       (&st_volcxt[v])
#define GET_DEVCXT(d)       (pst_devcxt[d])
#define	GET_DEV_INDEX(v, i)	(v * DEVS_PER_VOL + i)

void _set_phy_unlock_area(unsigned int vol, part_info *pi, unsigned short first_sbn_unlock_area[DEVS_PER_VOL]);
void _protect_lock_area(unsigned int vol, unsigned int flag);
int _low_level_func_register(unsigned int vol, flash_vol *pvol_cxt);
int _open(unsigned int vol);
int _close(unsigned int vol);
int _check_opendev(unsigned int dev);
int _vir_to_phy_addr(unsigned int vol, unsigned int vsn, flash_vol *pvol_cxt);
void _trim_mainbuf(unsigned int nWriteScts, SGL *pstSGL);
int _sgl_read(unsigned int vol, unsigned int vsn, unsigned int num_of_scts,
			SGL *pstSGL, unsigned char *ps_buf, unsigned int nFlag);
/*
 * tbml_init - [Tiny BML Interface] 
 * This function initializes the data structure of TinyBML 
 * and call onenand lld init function.
 *
 */
/* sh.kim - start */
int tbml_init(unsigned int vol)
{
	int cnt;
	int ret = BML_SUCCESS;

	DEBUG(TBML_DEBUG_LEVEL1,"++ tbml_init\n");
   
	 if ( tbml_init_flag[vol] ) {
        	DEBUG(TBML_DEBUG_LEVEL0,"Already initialized\n");
	        return BML_ALREADY_INITIALIZED;
	}

	if(vol == 0)
	{
		AD_Memset(st_volcxt, 0x00, sizeof(flash_vol) * XSR_MAX_VOL);
	}
	/* Volume context initialization */
	st_volcxt[vol].vol_open = FALSE32;
	st_volcxt[vol].open_cnt = 0;

	/* Device context initialization */
	pst_devcxt[vol] = NULL;
	
	if(vol == 0)
	{
		AD_Memset(st_ftable, 0x00, sizeof(lld_ftable) * XSR_MAX_VOL);
	}
	/* registering the LLD function table */
	inter_low_ftable(vol, st_ftable);

	if(st_ftable[vol].init != NULL)	
	{
		ret = st_ftable[vol].init((void *) inter_getparam());
		if (ret != LLD_SUCCESS)	{
			DEBUG(TBML_DEBUG_LEVEL0,"LLD init fail\n");
			return BML_DEVICE_ACCESS_ERROR;
		}
	}
	
	tbml_init_flag[vol] = TRUE32;
	DEBUG(TBML_DEBUG_LEVEL1,"-- tbml_init\n");

	return ret;
}
/* sh.kim - end */
 

/*
 * tbml_open - [Tiny BML Interface] 
 * This function open volume.
 * @vol : volume number
 */
int tbml_open(unsigned int vol)
{
	int cnt;
	int ret = BML_SUCCESS;
	int device_index;
	short first_sbn_unlock_area[DEVS_PER_VOL];
	flash_vol *pvol_cxt;
	flash_dev *pdev_cxt;
	part_info *pst_part_info = NULL;
	part_exinfo *pst_part_info_ext = NULL;
	
	DEBUG(TBML_DEBUG_LEVEL1,"++ tbml_open\n");

	/* if valid volume, get volume context */
	if (VOLUME_CHECK(vol)) {
		DEBUG(TBML_DEBUG_LEVEL0,"Invalid volume number=%d\n",vol);
		return BML_INVALID_PARAM;
	}
	pvol_cxt = GET_VOLCXT(vol);

	/* check a volume open */
	if (pvol_cxt->vol_open == TRUE32) {
		pvol_cxt->open_cnt++;
		DEBUG(TBML_DEBUG_LEVEL0,"tbml open already open\r\n");
		return BML_SUCCESS;
	}

	/* Create the semaphore handle for nDevIdx */
	if (AD_CreateSM (&(pvol_cxt->sm)) == FALSE32) {
		DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  AD_CreateSM Error\r\n");
		return BML_OAM_ACCESS_ERROR;
	}
	
	do {
		/* register low level function*/
		ret = _low_level_func_register(vol, pvol_cxt);	
		if ( ret != BML_SUCCESS ) {
			DEBUG(TBML_DEBUG_LEVEL0,"low level function register failed\n");
			ret = BML_PAM_ACCESS_ERROR;
			break;
		}

		/* lld level open and get lld spec */
		ret = _open(vol);	
		if ( ret != BML_SUCCESS ) {
			DEBUG(TBML_DEBUG_LEVEL0,"_open error \n");
			break;
		}
		
		for ( cnt = 0; cnt < DEVS_PER_VOL; cnt++ ) {
			device_index = GET_DEV_INDEX(vol, cnt);	
			DEBUG(TBML_DEBUG_LEVEL1,"device_index = %d\n", device_index);	
			
			/* check device open */
			if( _check_opendev(device_index) == FALSE32)
				 continue;

			pdev_cxt = GET_DEVCXT(device_index);
			DEBUG(TBML_DEBUG_LEVEL1,"flash_dev->blks_in_unlock = %d\n", pdev_cxt->blks_in_unlock);	
			DEBUG(TBML_DEBUG_LEVEL1,"flash_dev->n1st_usbn = %d\n", pdev_cxt->n1st_usbn);	
			DEBUG(TBML_DEBUG_LEVEL1,"flash_dev->n1st_upbn = %d\n", pdev_cxt->n1st_upbn);	

			if ( (device_index == 0) || (device_index == 4)) {
				pst_part_info 		= pvol_cxt->pi;
				pst_part_info_ext	= pvol_cxt->pie; 
				DEBUG(TBML_DEBUG_LEVEL1,"part_info.num_of_partentry = %d\n", pst_part_info->num_of_partentry);
			}
		
			DEBUG(TBML_DEBUG_LEVEL1,"pvol_cxt->num_of_useblk = %d\n", pvol_cxt->num_of_useblk);
			/* mount reservoir area */
			ret = gbbm_mount(pvol_cxt, pdev_cxt, pst_part_info, pst_part_info_ext);
			if ( ret != BBM_SUCCESS ) {
				DEBUG(TBML_DEBUG_LEVEL0,"gbbm mount failure\r\n");
				ret = BML_UNFORMATTED;
				break;
			}
			DEBUG(TBML_DEBUG_LEVEL1,"pst_part_info.num_of_partentry = %d\n", pst_part_info->num_of_partentry);	
		}

/* ij.jang : debug pi */
#if 0
		{
			part_info *pi = pvol_cxt->pi;
			int i;
			DEBUG(TBML_DEBUG_LEVEL1, "total entries : %d\n", pi->num_of_partentry);
			for (i=0; i<pi->num_of_partentry; i++) {
				DEBUG(TBML_DEBUG_LEVEL1, "id=%08x, start=%d, num=%d\n",
						pi->pentry[i].id, pi->pentry[i].n1st_vbn, pi->pentry[i].num_of_blks);
			}
		}
#endif
		_set_phy_unlock_area(vol, pvol_cxt->pi, first_sbn_unlock_area);

		for(cnt = 0; cnt < DEVS_PER_VOL; cnt++) {
			device_index = GET_DEV_INDEX(vol, cnt);
			/* check open device */
			if (_check_opendev(device_index) == FALSE32 ) 
				continue;

			pdev_cxt = GET_DEVCXT(device_index);
			/* update meta data */
			gbbm_metadata_update(pdev_cxt);
		}

		_protect_lock_area(vol, TRUE32);

	} while(0);

	pvol_cxt->pre_program = FALSE32;
	pvol_cxt->vol_open = TRUE32;
	pvol_cxt->open_cnt = 1;

	DEBUG(TBML_DEBUG_LEVEL1,"-- tbml_open\n");

	return ret; 	
}

/*
 * tbml_read - [Tiny BML Interface] 
 * This function read operation.
 * @vol : volume number
 */

int tbml_read(unsigned int vol, unsigned int vsn, unsigned int scts_per_pg,
				unsigned char *pm_buf, unsigned char *ps_buf, unsigned int flag)
{
	unsigned int ret = BML_SUCCESS;	
	unsigned int phy_index_block;
	unsigned int phy_block;
	unsigned int dev_index;
	unsigned int sector_offset;
	flash_vol *pvol_cxt;

	/* check volume range, parmeters etc.. */
	if( VOLUME_CHECK(vol) ) {
		DEBUG(TBML_DEBUG_LEVEL0,"volume is out of boundary[vol=%d]\n", vol);
		return BML_INVALID_PARAM;
	}
	
	/* Get volume context */
	pvol_cxt = GET_VOLCXT(vol);
	
    /* lock the device */
    if (AD_AcquireSM(pvol_cxt->sm) == FALSE32)
    {
		DEBUG(TBML_DEBUG_LEVEL0,"Semaphore error\n");
        return BML_ACQUIRE_SM_ERROR;
    }
 
	do {
		/* check parmeter (vsn, scts_per_pg, flag) */
		if( pvol_cxt->scts_per_pg < scts_per_pg ) {
			DEBUG(TBML_DEBUG_LEVEL0,"invalid parameter\n");
			ret = BML_INVALID_PARAM;
			break;
		}

		/* translate virtual address to physical address */
		phy_index_block = _vir_to_phy_addr(vol, vsn, pvol_cxt);
		dev_index = ((phy_index_block & 0xC0000000) >> 30);
		phy_block = phy_index_block & 0x3FFFFFFF;
		sector_offset = vsn & (pvol_cxt->scts_per_blk - 1);
		
		ret = pvol_cxt->lld_read( dev_index, (phy_block << pvol_cxt->shift_sctsperblk) + sector_offset,
							scts_per_pg, pm_buf, ps_buf, flag);

		if ( ret != LLD_SUCCESS ) {
			DEBUG(TBML_DEBUG_LEVEL0,"LLD Read error \n");
			break;
		}
	}while(0);

	if (AD_ReleaseSM(pvol_cxt->sm) == FALSE32)
	{
		DEBUG(TBML_DEBUG_LEVEL0,"Error semaphore \n");
		return BML_RELEASE_SM_ERROR;
	}

	return ret;
}


/*
 * tbml_get_vol_spec - [Tiny BML Interface] 
 * This function get tbml volume context information.
 * @vol : volume number
 */
int tbml_get_vol_spec(unsigned int vol, tbml_vol_spec *pvol_spec)
{
	int cnt = 0;
	int ret = BML_SUCCESS;
	int dev_index;
	flash_vol *pvol_cxt;
	lldspec	dev_info;

	dev_index = GET_DEV_INDEX(vol, cnt);	
	pvol_cxt = GET_VOLCXT(vol);

    /* lock the device */
    if (AD_AcquireSM(pvol_cxt->sm) == FALSE32)
    {
		DEBUG(TBML_DEBUG_LEVEL0,"Semaphore error\n");
        return BML_ACQUIRE_SM_ERROR;
    }

	do {	 
		ret = pvol_cxt->lld_dev_info(dev_index, &dev_info);
		if ( ret != LLD_SUCCESS) {
			DEBUG(TBML_DEBUG_LEVEL0,"LLD_GetDevInfo return errror\n");
			break;
		}
	} while(0);

	if (AD_ReleaseSM(pvol_cxt->sm) == FALSE32)
    {
		DEBUG(TBML_DEBUG_LEVEL0,"Error semaphore \n");
        return BML_RELEASE_SM_ERROR;
    }


	pvol_spec->pgs_per_blk	= (unsigned short) pvol_cxt->pgs_per_blk;
	pvol_spec->scts_per_pg	= (unsigned char) pvol_cxt->scts_per_pg;
	pvol_spec->lsn_pos		= (unsigned char) pvol_cxt->lsn_pos;
	pvol_spec->ecc_pos		= (unsigned char) pvol_cxt->ecc_pos;
	pvol_spec->num_of_useblk	= pvol_cxt->num_of_useblk;
	pvol_spec->ecc_flag		= pvol_cxt->ecc_flag;

	AD_Memcpy(pvol_spec->uniq_id, dev_info.uniq_id, XSR_UID_SIZE); 
	return ret;
}

/*
 * tbml_load_partition - [Tiny BML Interface] 
 * This function get partition information.
 * @vol : volume number
 * @id  : partition id
 * @part_entry : partition entry structure.
 */
int tbml_load_partition(unsigned int vol, unsigned int id, part_entry *pe)
{
	int cnt;
	int ret = BML_SUCCESS;
	unsigned int find = FALSE32;
	flash_vol *pvol_cxt;
	part_info *pst_part_info;

	pvol_cxt = GET_VOLCXT(vol);
	pst_part_info = pvol_cxt->pi;
	
	if (pe == NULL) 
		return BML_INVALID_PARAM;
	
	/* searching a id partition */
	for (cnt = 0; cnt < pst_part_info->num_of_partentry; cnt++) {
		if ( id == pst_part_info->pentry[cnt].id ) {
			DEBUG(TBML_DEBUG_LEVEL2,"find partition entry \n");
			AD_Memcpy(pe, &(pst_part_info->pentry[cnt]), sizeof(part_entry));
			find = TRUE32;
			break;
		}
	}

	if ( find == FALSE32 )	{
		DEBUG(TBML_DEBUG_LEVEL0,"Not found Partition entry = %x\n", id);
		ret = BML_NO_PIENTRY;
	}
	return ret;
}

/*
 * tbml_close - [Tiny BML Interface] 
 * This function close operation.
 * @vol : volume number
 */
int tbml_close(unsigned int vol)
{
	unsigned int cnt;
	unsigned int ret = BML_SUCCESS;
	unsigned int device_index;
	flash_vol *pvol_cxt;
	
	if ( VOLUME_CHECK(vol) ) {
		DEBUG(TBML_DEBUG_LEVEL0," Invalid volume parmeter\n");
		return BML_INVALID_PARAM;
	}

	pvol_cxt = GET_VOLCXT(vol);

	if( --pvol_cxt->open_cnt != 0 ) {
		DEBUG(TBML_DEBUG_LEVEL0,"volume already closed\n");
		return BML_SUCCESS;
	}
		
	for( cnt = 0; cnt < DEVS_PER_VOL; cnt++ ) {
		device_index = GET_DEV_INDEX(vol, cnt);
		if ( _check_opendev(device_index) == FALSE32 ) continue;
	
		ret = pvol_cxt->lld_flush(device_index);
		if ( ret != LLD_SUCCESS ) {
			DEBUG(TBML_DEBUG_LEVEL0,"low level flushop operation fail\n");
			return ret;
		}
	}

	ret = _close(vol);	
	if( ret != BML_SUCCESS ) {
		DEBUG(TBML_DEBUG_LEVEL0,"_close error\n");
		return ret;
	}
	
	DEBUG(TBML_DEBUG_LEVEL2,"tbml_close success \n");	
	return ret;	
}

/*
 * tbml_ioctl - [Tiny BML Interface]
 * This function ioctl operation.
 * @vol : volume number
 */
int tbml_ioctl(unsigned int vol, unsigned int flag, unsigned char *pbuf_in, unsigned int in_len,
                unsigned char *pbuf_out, unsigned int out_len, unsigned int *byte_ret)
{
    unsigned int cnt;
    unsigned int device_index;
    unsigned int ret = BML_SUCCESS;

    flash_vol *pvol_cxt;
    flash_dev *pdev_cxt;

    if( VOLUME_CHECK(vol) ) {
        DEBUG(TBML_DEBUG_LEVEL0," Invalid volume parmeter\n");
        return BML_INVALID_PARAM;
    }

    pvol_cxt = GET_VOLCXT(vol);

	/* lock the device */
    if (AD_AcquireSM(pvol_cxt->sm) == FALSE32) {
		DEBUG(TBML_DEBUG_LEVEL0,"Semaphore error\n");
        return BML_ACQUIRE_SM_ERROR;
    }

    switch( flag ) {
        case BML_IOCTL_UNLOCK_WHOLEAREA:
        {
            unsigned short lock_state = 0;

            for( cnt = 0; cnt < DEVS_PER_VOL; cnt++) {
                device_index = GET_DEV_INDEX(vol, cnt);
                if( _check_opendev(device_index) == FALSE32 ) continue;

                pdev_cxt = GET_DEVCXT(device_index);

                ret = pvol_cxt->lld_ioctl_cmd(device_index, LLD_IOC_GET_SECURE_STAT,
                                        NULL, 0,
                                        (unsigned char *)&lock_state,
                                        sizeof(unsigned short), byte_ret);
                if ( ret != LLD_SUCCESS ) {
                    DEBUG(TBML_DEBUG_LEVEL0,"low level IOCtl function error \n");
                    return ret;
                }


                if( lock_state == LLD_IOC_SECURE_LT ) {
                    DEBUG(TBML_DEBUG_LEVEL0,"Already lock tighted \n");
                    return BML_VOLUME_ALREADY_LOCKTIGHT;
                }


                /* set whole block r/w area */
                ret = pvol_cxt->lld_set_rw(device_index, 0, pvol_cxt->blks_per_dev);
                if( ret != LLD_SUCCESS ) {
                    DEBUG(TBML_DEBUG_LEVEL0,"low level SetRWArea function error\n");
                    return ret;
                }
                pdev_cxt->n1st_upbn = 0;
                pdev_cxt->blks_in_unlock = pvol_cxt->blks_per_dev;
                pvol_cxt->pre_program = TRUE32;
            }
            break;
        }
		break;
        case BML_IOCTL_CHANGE_PART_ATTR:
        case BML_IOCTL_GET_BMI:
        case BML_IOCTL_INVALID_BLOCK_CNT:
        case BML_IOCTL_GET_FULL_PI:
        {
            if( pbuf_out == NULL ) {
                DEBUG(TBML_DEBUG_LEVEL0,"invalid parameter \n");
                break;
            }

            if( out_len != sizeof(part_info) ) {
                DEBUG(TBML_DEBUG_LEVEL0,"invalid parameter []\n");
                break;
            }

            pvol_cxt = GET_VOLCXT(vol);
            AD_Memcpy(pbuf_out, pvol_cxt->pi, sizeof(part_info));
            if( byte_ret != NULL ) {
                *byte_ret = sizeof(part_info);
            }
            break;
        }
		break;
        default:
		{
			DEBUG(TBML_DEBUG_LEVEL0, "No parameter ioctl\n");
		}
    }

	/* release the device */
    if (AD_ReleaseSM(pvol_cxt->sm) == FALSE32) {
		DEBUG(TBML_DEBUG_LEVEL0,"Error semaphore \n");
        return BML_RELEASE_SM_ERROR;
    }

    return ret;
}

/*
 * tbml_flush - [Tiny BML Interface]
 * This function ioctl operation.
 * @vol : volume number
 * @flag : volume number
 */
int tbml_flush(unsigned int vol, unsigned int flag)
{
	unsigned int device_index = 0;
	int cnt, ret = BML_SUCCESS;
	register flash_vol *pvol_cxt;

	/* get volume context from given volume */
	pvol_cxt = GET_VOLCXT(vol);

	/* lock the device */
    if (AD_AcquireSM(pvol_cxt->sm) == FALSE32) {
		DEBUG(TBML_DEBUG_LEVEL0,"Semaphore error\n");
        return BML_ACQUIRE_SM_ERROR;
    }

	for( cnt = 0; cnt < DEVS_PER_VOL; cnt++ ) {
		device_index = GET_DEV_INDEX(vol, cnt);
		if ( _check_opendev(device_index) == FALSE32 ) continue;

		ret = pvol_cxt->lld_flush(device_index);
		if ( ret != LLD_SUCCESS ) {
			DEBUG(TBML_DEBUG_LEVEL0,"low level flushop operation fail\n");
			return ret;
		}
	}

	/* release the device */
    if (AD_ReleaseSM(pvol_cxt->sm) == FALSE32) {
		DEBUG(TBML_DEBUG_LEVEL0,"Error semaphore \n");
        return BML_RELEASE_SM_ERROR;
    }


	return ret;
}

/* 
	register low function table and LFT link completion check 
	if register complete return value : 0
	else				 return value : 1
*/
int _low_level_func_register(unsigned int vol, flash_vol *pvol_cxt)
{
	int cnt;
	int ret = BML_SUCCESS;
	unsigned int *pfun_tmp;
	flash_vol *pvol_cxt_tmp;

	DEBUG(TBML_DEBUG_LEVEL1,"_low_level_func_register enter\n");	
	pvol_cxt_tmp = pvol_cxt;	
	
	AD_Memset(st_ftable, 0x00, sizeof(lld_ftable) * XSR_MAX_VOL);

	/* sh.kim - start */
	inter_low_ftable(vol, st_ftable);
	/* sh.kim - end */

	pfun_tmp = (unsigned int *)&st_ftable[vol];
	for ( cnt = 0; cnt < (sizeof(lld_ftable) / sizeof(unsigned int)); cnt++) { 	
		if (pfun_tmp[cnt] == 0x00000000) {
			DEBUG(TBML_DEBUG_LEVEL0,"LLD not registered\n");
			return BML_PAM_ACCESS_ERROR;
		}
	}
	/* regigtering low level function table */	
	pvol_cxt_tmp->lld_init			= st_ftable[vol].init;
	pvol_cxt_tmp->lld_open			= st_ftable[vol].open;
	pvol_cxt_tmp->lld_close			= st_ftable[vol].close;
	pvol_cxt_tmp->lld_read			= st_ftable[vol].read;
	pvol_cxt_tmp->lld_write			= st_ftable[vol].write;
	pvol_cxt_tmp->lld_erase			= st_ftable[vol].erase;
	pvol_cxt_tmp->lld_dev_info		= st_ftable[vol].dev_info;
	pvol_cxt_tmp->lld_check_bad		= st_ftable[vol].check_bad;
	pvol_cxt_tmp->lld_flush			= st_ftable[vol].flush;
	pvol_cxt_tmp->lld_set_rw		= st_ftable[vol].set_rw;
	pvol_cxt_tmp->lld_ioctl_cmd		= st_ftable[vol].ioctl;
	pvol_cxt_tmp->lld_mread			= st_ftable[vol].mread;
	

	DEBUG(TBML_DEBUG_LEVEL1,"_low_level_func_register end\n");	
	return ret;
}

/* LLD open and Get a lldspec info */
int _open(unsigned int vol)
{
	int ret = BML_SUCCESS;
	int cnt;
	int dev_index;
	unsigned int malloc_size;

	flash_vol	*pvol_cxt;
	flash_dev	*pdev_cxt;	
	vol_param	*pvol_parm;
	lldspec		lld_spec;		

	/* get volume context from given volume */
	pvol_cxt = GET_VOLCXT(vol);	
	
	pvol_parm = (vol_param *)inter_getparam();

	/* Using H/W ECC and number of device */
	pvol_cxt->ecc_flag	= pvol_parm[vol].ecc_flag;
	pvol_cxt->num_of_device	= pvol_parm[vol].nDevsInVol;

	/* LLD level function open */
	for (cnt = 0; cnt < pvol_cxt->num_of_device; cnt++) {
		/* get device index */
		dev_index = GET_DEV_INDEX(vol, cnt);
		/* low level device open */
		ret = pvol_cxt->lld_open(dev_index);
		if ( ret != LLD_SUCCESS ) {
			DEBUG(TBML_DEBUG_LEVEL0,"LLD Open failed \n");
			return BML_DEVICE_ACCESS_ERROR;
		}
		
		/* Get lldspec info */
		ret = pvol_cxt->lld_dev_info(dev_index, &lld_spec);
		if ( ret != LLD_SUCCESS ) {
			DEBUG(TBML_DEBUG_LEVEL0," LLD_GetDevInfo read failed \n");
			return BML_DEVICE_ACCESS_ERROR;
		}	

		/* create device context */
		pst_devcxt[dev_index] = (flash_dev *)AD_Malloc(sizeof(flash_dev));
		if (pst_devcxt[dev_index] == NULL ) {
			DEBUG(TBML_DEBUG_LEVEL0,"device context malloc error\n");
			return BML_OAM_ACCESS_ERROR;
		}
		pdev_cxt = pst_devcxt[dev_index];
		pdev_cxt->dev_no = (unsigned int)dev_index;		

		/* fill volume context parmeter */
		pvol_cxt->lsn_pos			= lld_spec.lsn_pos;	
		pvol_cxt->ecc_pos			= lld_spec.ecc_pos;	
		pvol_cxt->bad_pos			= lld_spec.bad_pos;
		pvol_cxt->scts_per_pg		= lld_spec.scts_per_pg;		
		pvol_cxt->planes_per_dev	= lld_spec.num_of_plane;
		pvol_cxt->blks_per_dev		= lld_spec.num_of_blks;
		pvol_cxt->resrv_per_dev		= (unsigned short)(lld_spec.blks_in_rsv + RSV_META_BLKS);
		pvol_cxt->scts_per_blk		= (unsigned short)(lld_spec.scts_per_pg * lld_spec.pgs_per_blk);
		pvol_cxt->pgs_per_blk		= lld_spec.pgs_per_blk;
		pvol_cxt->n1stsbn_resrv		= (unsigned short)(lld_spec.num_of_blks - pvol_cxt->resrv_per_dev);


        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->blks_per_dev   : %d\r\n", pvol_cxt->blks_per_dev);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->resrv_per_dev  : %d\r\n", pvol_cxt->resrv_per_dev);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->scts_per_blk   : %d\r\n", pvol_cxt->scts_per_blk);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->pgs_per_blk    : %d\r\n", pvol_cxt->pgs_per_blk);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->n1stsbn_resrv  : %d\r\n", pvol_cxt->n1stsbn_resrv);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->lsn_pos        : %d\r\n", pvol_cxt->lsn_pos);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->bad_pos        : %d\r\n", pvol_cxt->bad_pos);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->ecc_pos        : %d\r\n", pvol_cxt->ecc_pos);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->ecc_flag       : %d  0:NO_ECC 1:SW_ECC 2:HW_ECC\r\n", pvol_cxt->ecc_flag);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->scts_per_pg    : %d\r\n", pvol_cxt->scts_per_pg);
        DEBUG(TBML_DEBUG_LEVEL2,"pvol_cxt->planes_per_dev : %d\r\n", pvol_cxt->planes_per_dev);


		pdev_cxt->st_meta.st_bmi.pst_bmf = (bmf *) AD_Malloc(lld_spec.blks_in_rsv * sizeof(bmf));
		if (pdev_cxt->st_meta.st_bmi.pst_bmf == NULL) {
			DEBUG(TBML_DEBUG_LEVEL0,"AD_Malloc error : Block Map Field\n");
			return BML_OAM_ACCESS_ERROR;
		}
		
		pdev_cxt->st_meta.pbad_bit_map = (unsigned char *) AD_Malloc(BABITMAP_SIZE);
		if (pdev_cxt->st_meta.pbad_bit_map == NULL) {
			DEBUG(TBML_DEBUG_LEVEL0,"AD_Malloc error : Bad Block Bit Map\n");
			return BML_OAM_ACCESS_ERROR;
		}

		if (lld_spec.num_of_blks > BLKS_PER_BADUNIT) 
			malloc_size = lld_spec.num_of_blks / BLKS_PER_BADUNIT;
		else
			malloc_size = 1;
		
		pdev_cxt->st_meta.pbad_unit_map = (badunit *) AD_Malloc(malloc_size * sizeof(badunit));
		if (pdev_cxt->st_meta.pbad_unit_map == NULL) {
			DEBUG(TBML_DEBUG_LEVEL0,"AD_Malloc error : Bad UnitMap\n");
			return BML_OAM_ACCESS_ERROR;
		}
		
		pdev_cxt->ptmp_mbuf = (unsigned char *) AD_Malloc(XSR_SECTOR_SIZE * 4);
		pdev_cxt->ptmp_sbuf = (unsigned char *) AD_Malloc(XSR_SPARE_SIZE * 4);
			
		pvol_cxt->num_of_useblk += pvol_cxt->n1stsbn_resrv;
	}

    pvol_cxt->shift_sctsperblk  = (unsigned char)((pvol_cxt->scts_per_blk  == 256) ? 8 :
                                      (pvol_cxt->scts_per_blk  ==  32) ? 5 :
                                      (pvol_cxt->scts_per_blk  ==  16) ? 4 : 7);
                                      /* 7 means that scts_per_blk is 128. */

/* Not Using code */
    pvol_cxt->shift_sctsperpg   = (unsigned char)((pvol_cxt->scts_per_pg == 1) ? 0 :
                                      (pvol_cxt->scts_per_pg   ==   2) ? 1 : 2);
                                      /* 2 means that scts_per_pg is 4 */

/* Not using code */
    pvol_cxt->mask_sctsperpg   = (unsigned char)((pvol_cxt->scts_per_pg == 1) ? 0 :
                                      (pvol_cxt->scts_per_pg   ==   2) ? 1 : 3);
                                      /* 3 means that scts_per_pg is 4 */

    pvol_cxt->shift_numofdev   = (unsigned char)((pvol_cxt->num_of_device == 1) ? 0 :
                                      (pvol_cxt->num_of_device    ==   2) ? 1 :
                                      (pvol_cxt->num_of_device    ==   3) ? 3 : 2);
                                      /* 2 means that num_of_device is 4 */


    pvol_cxt->pi        = (part_info *)AD_Malloc(sizeof(part_info));
    pvol_cxt->pie          = (part_exinfo *)AD_Malloc(sizeof(part_exinfo));
    pvol_cxt->pie->pdata   = (void     *)AD_Malloc(BML_MAX_PIEXT_DATA);

    if ((pvol_cxt->pi      == NULL) ||
        (pvol_cxt->pie        == NULL) ||
        (pvol_cxt->pie->pdata == NULL))
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  AD_Malloc Error for PI/PIExt\r\n");
        return BML_OAM_ACCESS_ERROR;
    }

    pvol_cxt->pie->id         = 0xFFFFFFFF;
    pvol_cxt->pie->data_size = 0xFFFFFFFF;

    AD_Memset(pvol_cxt->pie->pdata, 0xFF, BML_MAX_PIEXT_DATA);

	return ret;
}


/*
 * This function set lock/unlock area from partition attrubute.
 * FROZEN partition will find last vbn.
 * Otherwise another partition will find last vbn 
 * 
 */
void _set_phy_unlock_area(unsigned int vol, part_info *pi, unsigned short first_sbn_unlock_area[DEVS_PER_VOL])
{
	unsigned int cnt;
	unsigned int min_first_vbn;
	unsigned int max_last_vbn;
	unsigned int first_vbn;
	unsigned int last_vbn;
	unsigned int first_vbn_unlock_frozen;
	unsigned int device_index;

	register flash_vol *pvol_cxt;
	register flash_dev *pdev_cxt;
	register part_entry *ppart_entry;

	pvol_cxt = GET_VOLCXT(vol);
	ppart_entry = &(pi->pentry[0]);

	min_first_vbn = 0xFFFFFFFF;
	max_last_vbn = 0;
	
	for( cnt = 0; cnt < pi->num_of_partentry; cnt++ ) {
		/* each partition start vbn and number of blocks */
		first_vbn = ppart_entry[cnt].n1st_vbn;
		last_vbn = first_vbn + ppart_entry[cnt].num_of_blks - 1;
		DEBUG(TBML_DEBUG_LEVEL2,"first_vbn = %d, last_vbn = %d\n", first_vbn, last_vbn);
	
		/* partition entry has frozen attribute */	
		if( ( ppart_entry[cnt].attr & BML_PI_ATTR_FROZEN ) != 0 ) {
			if( max_last_vbn < last_vbn ) max_last_vbn = last_vbn;	
		}
		else {
			/* partition entry has not frozen attribute */
			/* set mininum vbn */
			if( min_first_vbn > first_vbn ) min_first_vbn = first_vbn;
		}
	}

	/* has not unfrozen attribute */
	if( min_first_vbn == 0xFFFFFFFF ) {
		first_vbn_unlock_frozen = max_last_vbn + 1;
	}
	else {
		first_vbn_unlock_frozen = min_first_vbn;
	}
	
	for( cnt = 0; cnt < DEVS_PER_VOL; cnt ++) {
		device_index = GET_DEV_INDEX(vol, cnt);
		pdev_cxt = GET_DEVCXT(device_index);
		if ( pdev_cxt == NULL ) continue;

		if( (first_vbn_unlock_frozen % pvol_cxt->num_of_device) <= cnt )	{
			first_sbn_unlock_area[cnt] = (unsigned short)(first_vbn_unlock_frozen 
										 / pvol_cxt->num_of_device);
			DEBUG(TBML_DEBUG_LEVEL2,"first_sbn = %d, first_vbn = %d \n", 
				first_sbn_unlock_area[cnt], first_vbn_unlock_frozen);
		}
		else {
			first_sbn_unlock_area[cnt] = (unsigned short)(first_vbn_unlock_frozen 
										 / pvol_cxt->num_of_device) + 1;
			if (pvol_cxt->n1stsbn_resrv < first_sbn_unlock_area[cnt]) {
				first_sbn_unlock_area[cnt] = pvol_cxt->n1stsbn_resrv;
			}
		}
		pdev_cxt->n1st_usbn = first_sbn_unlock_area[cnt];
	}
	
}

/*
 *	flag TRUE32 : partial lock
 * 		 FALSE32 : total lock
 */
void _protect_lock_area(unsigned int vol, unsigned int flag)
{
	unsigned int cnt;
	unsigned int device_index;

	flash_vol *pvol_cxt;
	flash_dev *pdev_cxt;

	pvol_cxt = GET_VOLCXT(vol);

	for ( cnt = 0; cnt < DEVS_PER_VOL; cnt++) {
		device_index = GET_DEV_INDEX(vol, cnt);
		if ( _check_opendev(device_index) == FALSE32 ) 
			continue;

		pdev_cxt = GET_DEVCXT(device_index);
		
		if ( flag == TRUE32 ) {
			pdev_cxt->n1st_upbn = pdev_cxt->n1st_usbn;
			pdev_cxt->blks_in_unlock	 = (unsigned short) (pdev_cxt->st_meta.alloc_lockarea - pdev_cxt->n1st_usbn);
		}
		else {
			pdev_cxt->n1st_upbn = 0;
			pdev_cxt->blks_in_unlock = pvol_cxt->blks_per_dev;
		}
		
		pvol_cxt->lld_set_rw(device_index, pdev_cxt->n1st_upbn, pdev_cxt->blks_in_unlock);
	}
}

/*
 *	If return value TRUE32 : device open
 * 					FALSE32 : device not open 
 * 	
 */
int _check_opendev(unsigned int dev)
{
    flash_dev *pdev_cxt;
    
    pdev_cxt = GET_DEVCXT(dev);
    if (pdev_cxt == NULL)
        return FALSE32;

    return TRUE32;
}

/*
 * virtual to physical address mapping
 */
int _vir_to_phy_addr(unsigned int vol, unsigned int vsn, flash_vol *pvol_cxt)
{
	unsigned int vir_block;
	unsigned int semi_block;
	unsigned int phy_block;
	unsigned int dev_index;
	unsigned int phy_index_block;
	unsigned int cnt;
	
	flash_dev *pdev_cxt;
	bmf	*pbad_map_field;
	badunit *pbad_unit;
	bmi	*pbad_map_info;

	vir_block = vsn >> pvol_cxt->shift_sctsperblk;

	if( pvol_cxt->num_of_device == 3 ) {
		semi_block = vir_block / pvol_cxt->shift_numofdev;
		dev_index = ((vol << DEVS_PER_VOL_SHIFT) + (vir_block % pvol_cxt->num_of_device)); 
	}
	else {
		semi_block = vir_block >> pvol_cxt->shift_numofdev; 
		dev_index = ((vol << DEVS_PER_VOL_SHIFT) + (vir_block & (pvol_cxt->num_of_device - 1))); 
	}

	/* get device context */	
	pdev_cxt = GET_DEVCXT(dev_index);	

	pbad_map_field = pdev_cxt->st_meta.st_bmi.pst_bmf;
	pbad_unit = pdev_cxt->st_meta.pbad_unit_map;
	pbad_map_info = &(pdev_cxt->st_meta.st_bmi);
	pbad_unit += (semi_block >> SFT_BLKS_PER_BADUNIT);

	phy_block = semi_block;
	/* check bad map field and replacement bad mapping */
	if ( pbad_unit->num_of_bmfs > 0 ) {
		pbad_map_field = (bmf *)pbad_map_info->pst_bmf + pbad_unit->n1stBMFIdx;	
		for ( cnt = 0; cnt < pbad_map_info->num_of_bmfs; cnt++) {
			if ( pbad_map_field->sbn == semi_block ) {
				DEBUG(TBML_DEBUG_LEVEL0,"Sbn = %d --> Pbn = %d\n", semi_block, pbad_map_field->rbn);
				phy_block = pbad_map_field->rbn;
			}
			pbad_map_field++;	
		}
	}	
	
	phy_index_block = (phy_block | (dev_index << 30));
	DEBUG(TBML_DEBUG_LEVEL1,"physical index_block = %d\n", phy_index_block );
	return phy_index_block;
}

int _close(unsigned int vol)
{
	unsigned int cnt;
	unsigned int ret;
	unsigned int device_index;

	flash_vol *pvol_cxt;
	flash_dev *pdev_cxt;

	pvol_cxt = GET_VOLCXT(vol);

	for( cnt = 0; cnt < DEVS_PER_VOL; cnt++ ) {
		device_index = GET_DEV_INDEX(vol, cnt);
		if( _check_opendev(device_index) == FALSE32 ) continue;
	
		pdev_cxt = GET_DEVCXT(device_index);	
		ret = pvol_cxt->lld_close(device_index);
		if( ret != LLD_SUCCESS ) {
			DEBUG(TBML_DEBUG_LEVEL0,"low level close function error\n");
			return ret;
		}
	
		if( pdev_cxt->st_meta.pbad_bit_map != NULL ) {
			AD_Free(pdev_cxt->st_meta.pbad_bit_map);
			pdev_cxt->st_meta.pbad_bit_map = NULL; 	
		}

		if( pdev_cxt->st_meta.pbad_unit_map != NULL ) {
			AD_Free(pdev_cxt->st_meta.pbad_unit_map);
			pdev_cxt->st_meta.pbad_unit_map = NULL;
		}

		if( pdev_cxt->st_meta.st_bmi.pst_bmf != NULL ) {
			AD_Free(pdev_cxt->st_meta.st_bmi.pst_bmf);
			pdev_cxt->st_meta.st_bmi.pst_bmf = NULL;
		}

		if( pdev_cxt->ptmp_mbuf != NULL ) {
			AD_Free(pdev_cxt->ptmp_mbuf);
			pdev_cxt->ptmp_mbuf = NULL;
		}
		
		if( pdev_cxt->ptmp_sbuf != NULL ) {
			AD_Free(pdev_cxt->ptmp_sbuf);
			pdev_cxt->ptmp_sbuf = NULL;
		}

		AD_Free(pdev_cxt);
		pdev_cxt = NULL;
	}

/* This is not using code, TinyBML by cramfs booting is not close */
#ifndef CONFIG_RFS_XSR
    /* Check whether the device is busy or not */
    if (AD_AcquireSM(pvol_cxt->sm) == FALSE32)
    {
		DEBUG(TBML_DEBUG_LEVEL0,"]ERROR semaphore\n");
		return BML_ACQUIRE_SM_ERROR;
    }

    /* Destroy the semaphore handle for nVDevNo */
    AD_DestroySM(pvol_cxt->sm);
#endif

	if( pvol_cxt->pi != NULL ) {
		AD_Free(pvol_cxt->pi);
		pvol_cxt->pi = NULL;
	}
	
	if( pvol_cxt->pie != NULL ) {
		if( pvol_cxt->pie->pdata != NULL ) {
			AD_Free(pvol_cxt->pie->pdata);
			pvol_cxt->pie->pdata = NULL;
		}
		AD_Free(pvol_cxt->pie);
		pvol_cxt->pie = NULL;
	}

	AD_Memset(pvol_cxt, 0x00, sizeof(flash_vol));
	pvol_cxt->vol_open = FALSE32;

	return BML_SUCCESS;
}

int tbml_mread(unsigned int vol, unsigned int vsn, unsigned int num_of_scts,
         unsigned char *pm_buf, unsigned char *ps_buf, unsigned int nFlag)
{
    int			ret = BML_SUCCESS;
    SGL			stSGL;

    DEBUG(TBML_DEBUG_LEVEL2,"[BIF: IN] ++BML_MRead(vol:%d,Vsn:%d,num_of_scts:%d,Flag:0x%x)\r\n",
               vol, vsn, num_of_scts, nFlag);

    /* fills SGL argument for write operation */
    stSGL.stSGLEntry[0].nFlag		= SGL_ENTRY_USER_DATA;
    stSGL.stSGLEntry[0].nSectors	= num_of_scts;
    stSGL.stSGLEntry[0].pBuf		= pm_buf;
    stSGL.nElements					= 1;

    ret = _sgl_read(vol, vsn, num_of_scts, &stSGL, ps_buf, nFlag);

    DEBUG(TBML_DEBUG_LEVEL2,"[BIF:OUT] --BML_MRead() nRe=0x%x\r\n", ret);

    return ret;
}

int _sgl_read(unsigned int vol, unsigned int vsn, unsigned int num_of_scts,
            SGL *pstSGL, unsigned char *ps_buf, unsigned int nFlag)
{
    int			ret = BML_SUCCESS;
    int			nMajorErr;
    int			nMinorErr;
    unsigned char*	pTmpMBuf = NULL;
    unsigned int		read_vsn;
    unsigned int		read_scts;
    unsigned int		nPDevT;
    unsigned int		cnt;
    unsigned int		phy_index_block;
    unsigned int		dev_index;
    unsigned int		phy_block;
    unsigned int		sector_offset;

    register		flash_vol *pvol_cxt;


    DEBUG(TBML_DEBUG_LEVEL2,"[BIF: IN] ++BML_SGLRead(vol:%d,Vsn:%d,num_of_scts:%d,Flag:0x%x)\r\n",
                   vol, vsn, num_of_scts, nFlag);

	if (VOLUME_CHECK(vol))	{
		DEBUG(TBML_DEBUG_LEVEL0,"volume is out of boundary[vol=%d]\n", vol);
		return BML_INVALID_PARAM;
	}

    pvol_cxt = GET_VOLCXT(vol);

    CHK_VOL_OPEN(pvol_cxt->vol_open);

    /* lock the device */
    if (AD_AcquireSM(pvol_cxt->sm) == FALSE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  Acquiring semaphore is failed\r\n");
        DEBUG(TBML_DEBUG_LEVEL0,"[BIF:OUT] --BML_SGLRead() nRe=0x%x\r\n", BML_ACQUIRE_SM_ERROR);

        return BML_ACQUIRE_SM_ERROR;
    }

    if (pstSGL != NULL)
    {
        pTmpMBuf = pstSGL->stSGLEntry[0].pBuf;
    }

	/* start virtual block number */
    read_vsn = vsn;

	/* Max number of read sectors is 256 sectors */
	read_scts = 1 << pvol_cxt->shift_sctsperblk;

	/* If read sector is 256 sector */
    if (read_scts > num_of_scts)
        read_scts = num_of_scts;


    /* All devices of volume should be flushed operations */
    for (cnt = 0; cnt < pvol_cxt->num_of_device; cnt++)
    {
        nPDevT = (vol << DEVS_PER_VOL_SHIFT) + cnt;

        ret = pvol_cxt->lld_flush(nPDevT);
        if (ret != LLD_SUCCESS)
        {
			DEBUG(TBML_DEBUG_LEVEL0,"low level flush operation error\r\n");
			/* DO NOT enter while (num_of_scts > 0) loop */
			num_of_scts = 0;
			break;
        }
    }

    while (num_of_scts > 0)
    {

	/* sh.kim - start */
	DEBUG(TBML_DEBUG_LEVEL1,"Remain sector=%d\n",num_of_scts);
	/* translate virtual address to physical address */
	phy_index_block = _vir_to_phy_addr(vol, vsn, pvol_cxt);

	#if 0
	dev_index = ((phy_index_block & 0xC0000000) >> 30);
	#else
	if(vol == 0)
		dev_index = 0;
	else
		dev_index = 4;
	#endif
	
	phy_block = phy_index_block & 0x3FFFFFFF;
	sector_offset = vsn & (pvol_cxt->scts_per_blk - 1);

        if (pvol_cxt->num_of_useblk <= (read_vsn >> pvol_cxt->shift_sctsperblk))
        {
            DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  Out of bound(vol:%d,Vsn:%d)\r\n", vol, vsn);
            ret = BML_INVALID_PARAM;
            break;
        }

        ret = pvol_cxt->lld_mread(dev_index,
                               (phy_block << pvol_cxt->shift_sctsperblk) + sector_offset,
                                read_scts,
                                pstSGL,
                                ps_buf,
                                nFlag);
	/* sh.kim - end */

        if (ret != LLD_SUCCESS)
        {
            nMajorErr = XSR_RETURN_MAJOR(ret);
            nMinorErr = XSR_RETURN_MINOR(ret);

            /* ------------------------------------------------ */
            /* NOTE                                             */
            /*  LLD only returns the uncorrectable read error.  */
            /*  LLD doesn't return the correctable read error.  */
            /*                                                  */
            /*  If LLD returns the uncorrectable read error,    */
            /* ------------------------------------------------ */
            if (nMajorErr == LLD_READ_ERROR)
            {
                DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  Uncorrectable Read Error Occurs during MRead (LLDErr=0x%x)\r\n",
                               ret);
                DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  at Vsn = %d, Psn = %d, NumberOfScts = %d \r\n",
                               read_vsn, (phy_block << pvol_cxt->shift_sctsperblk) + sector_offset, read_scts);

                ret = BML_READ_ERROR;

            }
            else if (nMajorErr == LLD_READ_DISTURBANCE)
            {
                DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  Read disturbance Occurs during Read (LLDErr=0x%x)\r\n",
                               ret);
                DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  at Vsn = %d, Psn = %d, NumberOfScts = %d \r\n",
                               read_vsn, (phy_block << pvol_cxt->shift_sctsperblk) + sector_offset, read_scts);
				return LLD_READ_DISTURBANCE;
            }
            else
            {
                ret = BML_CRITICAL_ERROR;
                BML_ASSERT(0);
                DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  Unexpected ret=0x%X\r\n", ret);

                break;
            }
        }

        if (pstSGL != NULL)
        {
			_trim_mainbuf(read_scts, pstSGL);
        }

        if (ps_buf != NULL)
        {
            ps_buf += (read_scts * XSR_SPARE_SIZE);
        }

        num_of_scts -= read_scts;
        read_vsn += read_scts;

		read_scts = (1 << pvol_cxt->shift_sctsperblk);
        if (read_scts > num_of_scts)
            read_scts = num_of_scts;
    }

    if (pstSGL != NULL)
    {
        pstSGL->stSGLEntry[0].pBuf = pTmpMBuf;
    }

    if (AD_ReleaseSM(pvol_cxt->sm) == FALSE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BIF:ERR]  Releasing semaphore is failed\r\n");
        DEBUG(TBML_DEBUG_LEVEL0,"[BIF:OUT] --BML_Read() nRe=0x%x\r\n", BML_RELEASE_SM_ERROR);

        return BML_RELEASE_SM_ERROR;
    }

    DEBUG(TBML_DEBUG_LEVEL2,"[BIF:OUT] --BML_SGLRead() nRe=0x%x\r\n", ret);

    return ret;
}


void _trim_mainbuf(UINT32 nWriteScts, SGL *pstSGL)
{
    UINT32      nSGLWriteScts = 0;
    UINT32      nTmpSGLWriteScts;
    UINT8       nSGLIdx1 = 0;
    UINT8       nSGLIdx2 = 0;
    SGL         stTmpSGL;
    stTmpSGL = *pstSGL;
    
    /* Pointer to Main buffer increasement */
    do
    {
        nSGLWriteScts += (UINT32)stTmpSGL.stSGLEntry[nSGLIdx1].nSectors;
        if (nSGLWriteScts > nWriteScts || stTmpSGL.nElements == nSGLIdx1 + 1)
        {
            break;
        }    
        nSGLIdx1++;
    } while(1);

	/* If MWrite() is completely terminated, SGL is not changed */
	if (nWriteScts == nSGLWriteScts && stTmpSGL.nElements == nSGLIdx1 + 1)
	{
		return;
	}

    do
    {

        if (nSGLIdx2 == 0)
        {
            pstSGL->nElements = 0;
            nTmpSGLWriteScts = nSGLWriteScts - stTmpSGL.stSGLEntry[nSGLIdx1].nSectors;
            pstSGL->stSGLEntry[nSGLIdx2].pBuf = stTmpSGL.stSGLEntry[nSGLIdx1].pBuf + ((nWriteScts - nTmpSGLWriteScts) * XSR_SECTOR_SIZE);
            pstSGL->stSGLEntry[nSGLIdx2].nSectors = nSGLWriteScts - nWriteScts;
            pstSGL->stSGLEntry[nSGLIdx2++].nFlag = stTmpSGL.stSGLEntry[nSGLIdx1].nFlag;  
         
        }
        else
        {
            pstSGL->stSGLEntry[nSGLIdx2].pBuf = stTmpSGL.stSGLEntry[nSGLIdx1].pBuf ;
            pstSGL->stSGLEntry[nSGLIdx2].nSectors = stTmpSGL.stSGLEntry[nSGLIdx1].nSectors;
            pstSGL->stSGLEntry[nSGLIdx2++].nFlag = stTmpSGL.stSGLEntry[nSGLIdx1].nFlag;             
                    
        }
        pstSGL->nElements++;

		if (nSGLIdx1 + 1 == stTmpSGL.nElements)
		{
			break;
		}
		
        nSGLIdx1++;

		
    } while(1);

    return;
}


