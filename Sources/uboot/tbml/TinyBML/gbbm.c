/**
 * @file    gbbm.c
 * @brief   This file manage bad blocks
 *
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 *
 */

#include <tbml_common.h>
#include <os_adapt.h>
#include <onenand_interface.h>
#include <tbml_interface.h>
#include <tbml_type.h>
#include <gbbm.h>
#include "../TinyBML/onenand.h"

/*****************************************************************************/
/* nScanDir parameter of _GetFreeRB()                                        */
/*****************************************************************************/
#define     BBM_SCAN2HIGH               (0x00000001)
#define     BBM_SCAN2LOW                (0x00000002)

/*****************************************************************************/
/* Type of PCB                                                               */
/*****************************************************************************/
#define     TYPE_LPCA                   (0x00000001)
#define     TYPE_UPCA                   (0x00000002)

/*****************************************************************************/
/* Type of PI                                                                */
/*****************************************************************************/
#define     TYPE_PI                     (0x00000003)
#define     TYPE_PIEXT                  (0x00000004)

/*****************************************************************************/
/* nUpdateType                                                               */
/*****************************************************************************/
#define     TYPE_INIT_CREATE            (unsigned short) (0xFCFE)
#define     TYPE_UPDATE_PIEXT           (unsigned short) (0xFAFE)
#define     TYPE_READ_ERR               (unsigned short) (0xFBFE)
#define     TYPE_WRITE_ERR              (unsigned short) (0xFDFE)
#define     TYPE_ERASE_ERR              (unsigned short) (0xFEFE)

/*****************************************************************************/
/* BAD mark value                                                            */
/*****************************************************************************/
#define     BADMARKV_READERR            (unsigned char) (0x44)
#define     BADMARKV_WRITEERR           (unsigned char) (0x22)
#define     BADMARKV_ERASEERR           (unsigned char) (0x11)

/*****************************************************************************/
/* Refresh by erase                                                          */
/*****************************************************************************/
#define		REF_SPARE_PBN_POS			(0)
#define		REF_SPARE_COUNT_POS			(6)
#define		REF_SPARE_VALIDMARK_POS		(4)
#define		REF_SPARE_VALIDMARK_1TIME	(0xF0)
#define		REF_SPARE_VALIDMARK_2TIME	(0x00)

#define		ERL_CNT_POS					(6)
#define		ERL_AGE_POS					(7)
#define		ERL_DATA_POS				(8)
#define		ERL_SPARE_VALIDMARK_POS		(5)
#define		ERL_SPARE_VALIDMARK			(0x00)
#define		MASK_ERL_VALIDMARK			(0xFF)

/*****************************************************************************/
/* Local typedefs                                                            */
/*****************************************************************************/
typedef struct {
    unsigned short   n1stPbn;
    unsigned short   nNumOfBlks;
} Area;

                           
/*****************************************************************************/
/* Static function prototypes                                                */
/*****************************************************************************/
static void _init(flash_dev *pdev_cxt, flash_vol *pvol_cxt);
static int _scan_reservoir(flash_dev *pdev_cxt, flash_vol *pvol_cxt);
static void _LoadPIExt(flash_dev *pdev_cxt, flash_vol *pvol_cxt, part_exinfo *pstPExt);
static unsigned int _LoadBMS(flash_dev *pdev_cxt, flash_vol *pvol_cxt, unsigned int nPCAType);
static unsigned int _LoadPI(flash_dev *pdev_cxt, flash_vol *pvol_cxt, part_info *pstPI);
static void _SortBMI(bmi *pst_bmi);
static void _SetAllocRB(flash_dev *pdev_cxt, flash_vol *pvol_cxt, unsigned int pbn);
static void _PrintPI(part_info *pstPI);
static unsigned int _ChkBMIValidity (flash_vol *pvol_cxt, reservoir *meta);
static void _ReconstructBUMap(flash_vol *pvol_cxt, reservoir *meta);
static int _ReadMetaData(unsigned int psn, unsigned char *pMBuf, flash_dev *pdev_cxt, 
				flash_vol *pvol_cxt);

/* ij.jang */
#if 0
#undef DEBUG
#define DEBUG(level,fmt,...)	printf(fmt,##__VA_ARGS__)
#endif

/*
 * This function read two sector of meta data.
 */
static int _ReadMetaData(unsigned int psn, unsigned char *pMBuf, flash_dev *pdev_cxt, flash_vol *pvol_cxt)
{
    unsigned int       dev;
    int        nLLDRe;
    int        nMajorErr;
    int        nMinorErr;
    unsigned short       nIdx;
    unsigned char       *pSBuf;
    int        bValid = FALSE32;

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM: IN] ++_ReadMetaData(PDev:%d,pbn:%d,SecOff:%d)\r\n",
                   pdev_cxt->dev_no,
                   psn / pvol_cxt->scts_per_blk,
                   psn % pvol_cxt->scts_per_blk);

    dev  = pdev_cxt->dev_no;
    pSBuf  = (unsigned char *) pdev_cxt->ptmp_sbuf;

	do
	{
		nLLDRe = pvol_cxt->lld_read(dev, psn, 2, (unsigned char *) pMBuf, (unsigned char *) pSBuf, LLD_FLAG_ECC_ON); 
		if( nLLDRe != LLD_SUCCESS ) 
		{
			return LLD_READ_ERROR;         
		}
                        
        /*
           First, check whether the mirror sector has confirmation mark or not
           If the mirror sector has no confirm-mark, sector has invalid information 
        
           The original sector has always confirm-mark
           if the mirror sector has confirm-mark.
        */
        if (((unsigned char) ~pSBuf[pvol_cxt->lsn_pos + CFM_OFFSET + XSR_SPARE_SIZE] & CFM_INV_DATA) != CFM_INV_DATA)
        {
            DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  _ReadMetaData(PDev:%d,pbn:%3d,SecOff:%3d) / Invalid Confirm(0x%x)\r\n",
                           pdev_cxt->dev_no,
                           psn / pvol_cxt->scts_per_blk,
                           psn  % pvol_cxt->scts_per_blk,
                           pSBuf[pvol_cxt->lsn_pos + CFM_OFFSET]);
            break;
        }
#if defined(BBM_INF_MSG_ON)
        else
        {
            DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  _ReadMetaData(PDev:%d,pbn:%3d,SecOff:%3d) /   Valid Confirm(0x%x)\r\n",
                           pdev_cxt->dev_no,
                           psn / pvol_cxt->scts_per_blk,
                           psn % pvol_cxt->scts_per_blk,
                           pSBuf[pvol_cxt->lsn_pos + CFM_OFFSET]);
        }
#endif /* (BBM_INF_MSG_ON) */

        nMajorErr = XSR_RETURN_MAJOR(nLLDRe);
        nMinorErr = XSR_RETURN_MINOR(nLLDRe);
        
        BML_ASSERT( (nMajorErr == LLD_SUCCESS)      ||
                    (nMajorErr == LLD_READ_ERROR));

        /* if unrecoverable error occurs, it is error */
        if ((nMajorErr == LLD_READ_ERROR) &&
            (nMinorErr == LLD_READ_UERROR_M0))
        {
            DEBUG(TBML_DEBUG_LEVEL1,"[BBM:ERR]  U_ReadErr at Blk:%d,Sec:%d\r\n",
                           psn / pvol_cxt->scts_per_blk,
                           psn % pvol_cxt->scts_per_blk);

            break;
        }
        
        for(nIdx = 0; nIdx < XSR_SECTOR_SIZE; nIdx++)
        {
            if (pMBuf[nIdx] != pMBuf[nIdx + XSR_SECTOR_SIZE])
                break;
        }
        
        if(nIdx != XSR_SECTOR_SIZE)
        {
            break;
        }
        if (((unsigned char) ~pSBuf[pvol_cxt->lsn_pos + CFM_OFFSET] & CFM_INV_DATA) != CFM_INV_DATA)
        {
            bValid = -1;
            break;  
        }               
        bValid = TRUE32;
        break;

    } while(0);    

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_ReadMetaData(PDev:%d,pbn:%d,SecOff:%d,bRet=%d)\r\n",
                   pdev_cxt->dev_no,
                   psn / pvol_cxt->scts_per_blk,
                   psn % pvol_cxt->scts_per_blk,
                   bValid);

    return bValid;
}


/*
 *
 */
static void _init(flash_dev *pdev_cxt, flash_vol *pvol_cxt)
{
    reservoir *meta;
    meta = &(pdev_cxt->st_meta);

    meta->alloc_lockarea = (unsigned short) pvol_cxt->blks_per_dev;
    meta->alloc_unlockarea = (unsigned short) pvol_cxt->n1stsbn_resrv;

    meta->cur_upcb_idx = 0;
    meta->cur_lpcb_idx = 0;    /* it is not used */

    meta->upcb[0] = 0;
    meta->upcb[1] = 0;
    meta->lpcb[0] = 0;
    meta->lpcb[1] = 0;    /* it is not used */

    meta->lpcb_age = 0;
    meta->upcb_age = 0;

    meta->next_ubms_off = 0;
    meta->next_lbms_off = 0;    /* it is not used */

    meta->st_bmi.lbmiage = 0;
    meta->st_bmi.ubmiage = 0;
    meta->st_bmi.num_of_bmfs = 0;

    /* Clear all Block Map Field. 0xFFFF means non-mapping information */
    AD_Memset(meta->st_bmi.pst_bmf, 0xFF, (pvol_cxt->resrv_per_dev - RSV_META_BLKS) * sizeof(bmf));

    /* initialize meta->pBABitMap by 0x00 */
    AD_Memset(meta->pbad_unit_map, 0x00, BABITMAP_SIZE);
}

/*
 *	This function find UPCB/LPCB
 */
static int _scan_reservoir(flash_dev *pdev_cxt, flash_vol *pvol_cxt)
{
    unsigned int dev;
    unsigned int pbn;
    unsigned int bUPCA = FALSE32;
    unsigned int bLPCA = FALSE32;
    reservoir *meta;
    pch *pstPCH;

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM: IN] ++_scan_reservoir(dev:%d)\r\n", 
                   pdev_cxt->dev_no);

    dev  = pdev_cxt->dev_no;
    meta = &(pdev_cxt->st_meta);
    /* allocated temporary buffer for main area */
    pstPCH = (pch *) pdev_cxt->ptmp_mbuf;

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  Scan Blk:%d ==> Blk:%d for UPCA\r\n",
                   pvol_cxt->n1stsbn_resrv + RSV_PCB_OFFSET, pvol_cxt->blks_per_dev - 1);

    meta->upcb_age = 0;
    meta->lpcb_age = 0;
    
    /* Scan reservoir by bottom-up fashion 
       to search for the latest two copies of UPCB 
                 and the latest one copy   of LPCB */
    for (pbn = pvol_cxt->n1stsbn_resrv + RSV_PCB_OFFSET; pbn < pvol_cxt->blks_per_dev; pbn++) {
        /* if Blk is bad block, skip */
        if (pvol_cxt->lld_check_bad(dev, pbn) == LLD_INIT_BADBLOCK)
            continue;
        
        /*

           +----------------+
           |      PCH       | <== second, checks validity of header information
           +----------------+
           |      PCH'      |
           +----------------+
           |      PIA       |
           +----------------+
           |      PIA'      |
           +----------------+
           |    1st BMS     |
           +----------------+
           |    1st BMS'    |
           +----------------+
           |    2nd BMS     |
           +----------------+
           |    2nd BMS'    | <== first, checks confirm mark of 2nd BMS'
           +----------------+

        */

        /*  read 2nd BMS or 2nd BMS' data
            if data has 2bit ECC error or data has no confirmatin mark, skip */
        if (_ReadMetaData(pbn * pvol_cxt->scts_per_blk + SCTS_PER_PCH + SCTS_PER_PIA + SCTS_PER_BMS + SCTS_PER_BMI * 2,
                          (unsigned char *) pstPCH,
                          pdev_cxt,
                          pvol_cxt) == FALSE32)
        {
             if (_ReadMetaData(pbn * pvol_cxt->scts_per_blk + SCTS_PER_PCH + SCTS_PER_PIA + SCTS_PER_BMS + SCTS_PER_BMI,
                              (unsigned char *) pstPCH,
                              pdev_cxt,
                              pvol_cxt) == FALSE32)
            {
                if (_ReadMetaData(pbn * pvol_cxt->scts_per_blk + SCTS_PER_PCH + SCTS_PER_PIA + SCTS_PER_BMS,
                                  (unsigned char *) pstPCH,
                                  pdev_cxt,
                                  pvol_cxt) == FALSE32)
                {
                    continue;
                }
            }
        }

        /*  read header data
            if data has 2bit ECC error or data has no confirmatin mark, skip */
        if (_ReadMetaData(pbn * pvol_cxt->scts_per_blk,
                        (unsigned char *) pstPCH,
                        pdev_cxt,
                        pvol_cxt) == FALSE32)
        {
            continue;
        }

        /* check erase signature of UPCB */
        if ((pstPCH->erase_sig1 != 0x00000000) ||
            (pstPCH->erase_sig2 != 0x00000000))
        {
            DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   Invalid Erase Signature(0x%x/0x%x)\r\n",
                           pstPCH->erase_sig1, pstPCH->erase_sig2);
            continue;
        }

        /* check signature of UPCB and LPCB */
        if (AD_Memcmp(pstPCH->signature, UPCH_SIG, MAX_PCH_SIG) == 0)
        {
            /* check age of UPCB */
            if (pstPCH->age > meta->upcb_age)
            {
                meta->upcb[0]		= (unsigned short) pbn;
                meta->upcb[1]		= pstPCH->alterpcb;
                meta->upcb_age		= pstPCH->age;
                meta->cur_upcb_idx	= 0;

                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPCB(   pbn:%d)\r\n", pbn);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPCB(alterpcb:%d)\r\n", pstPCH->alterpcb);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPCB(   age:%d)\r\n", pstPCH->age);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPCB is updated\r\n");
            }
            else
            {
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPCB(   pbn:%d)\r\n", pbn);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPCB(alterpcb:%d)\r\n", pstPCH->alterpcb);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPCB(   age:%d)\r\n", pstPCH->age);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPCB isn't updated\r\n");
            }

            bUPCA = TRUE32;
        }
        else if (AD_Memcmp(pstPCH->signature, LPCH_SIG, MAX_PCH_SIG) == 0)
        {
            /* check age of LPCB */
            if (pstPCH->age > meta->lpcb_age)
            {
                meta->lpcb[0]		= (unsigned short) pbn;
                meta->lpcb[1]		= pstPCH->alterpcb;
                meta->lpcb_age		= pstPCH->age;
                meta->cur_lpcb_idx	= 0;

                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPCB(   pbn:%d)\r\n", pbn);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPCB(alterpcb:%d)\r\n", pstPCH->alterpcb);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPCB(   age:%d)\r\n", pstPCH->age);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPCB is updated\r\n");
            }
            else
            {
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPCB(   pbn:%d)\r\n", pbn);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPCB(alterpcb:%d)\r\n", pstPCH->alterpcb);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPCB(   age:%d)\r\n", pstPCH->age);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPCB isn't updated\r\n");
            }

            bLPCA = TRUE32;

        }
        else
        {
            DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   Invalid PCH Signature(%02x %02x %02x %02x %02x %02x %02x %02x)\r\n",
				   pstPCH->signature[0],
				   pstPCH->signature[1],
				   pstPCH->signature[2],
				   pstPCH->signature[3],
				   pstPCH->signature[4],
				   pstPCH->signature[5],
				   pstPCH->signature[6],
				   pstPCH->signature[7]);
        }
    }

    if (bUPCA == FALSE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   NO UPCA\r\n");
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_scan_reservoir(dev:%d)\r\n", 
                       pdev_cxt->dev_no);
        return BBM_NO_UPCA;
    }

    if (bLPCA == FALSE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   NO LPCA\r\n");
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_scan_reservoir(dev:%d)\r\n", 
                       pdev_cxt->dev_no);
        return BBM_NO_LPCA;
    }
#if defined(BBM_INF_MSG_ON)
    else
    {
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPcb[0] : %d\r\n", meta->upcb[0]);
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPcb[1] : %d\r\n", meta->upcb[1]);
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   UPcbAge : %d\r\n", meta->upcb_age);
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPcb[0] : %d\r\n", meta->lpcb[0]);
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPcb[0] : %d\r\n", meta->lpcb[1]);
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   LPcbAge : %d\r\n", meta->lpcb_age);
    }
#endif /* (BBM_INF_MSG_ON) */

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_scan_reservoir(dev:%d)\r\n", 
                   pdev_cxt->dev_no);

    return BBM_SUCCESS;
}

/*
 *
 */
static void _LoadPIExt(flash_dev *pdev_cxt, flash_vol *pvol_cxt, part_exinfo *pstPExt)
{
    reservoir *meta;
    unsigned int     n1stPsnOfPExt;
    unsigned char     *pMBuf;


    DEBUG(TBML_DEBUG_LEVEL1,"[BBM: IN] ++_LoadPIExt(dev:%d,pstPExt=0x%p)\r\n", 
                   pdev_cxt->dev_no, pstPExt);
    
    meta         = &(pdev_cxt->st_meta);
    n1stPsnOfPExt  = meta->upcb[0] * pvol_cxt->scts_per_blk + SCTS_PER_PCH;
    pMBuf          = pdev_cxt->ptmp_mbuf;

    if (pstPExt != NULL)
    {
        if (_ReadMetaData(n1stPsnOfPExt,
                          (unsigned char *) pMBuf,
                          pdev_cxt,
                          pvol_cxt) == TRUE32)
        {
            AD_Memcpy(&pstPExt->id,        &pMBuf[0], 4);
            AD_Memcpy(&pstPExt->data_size, &pMBuf[4], 4);
            if (pstPExt->data_size > BML_MAX_PIEXT_DATA)
                pstPExt->data_size = BML_MAX_PIEXT_DATA;

            AD_Memcpy(pstPExt->pdata,       &pMBuf[8], pstPExt->data_size);
        }
        else
        {
            pstPExt->id         = 0;
            pstPExt->data_size = 0;
            AD_Memset(pstPExt->pdata, 0xFF, BML_MAX_PIEXT_DATA);
        }
    }

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_LoadPIExt(dev:%d,pstPExt=0x%p)\r\n", 
                   pdev_cxt->dev_no, pstPExt);
}

/*
 *
 */
static unsigned int _LoadBMS(flash_dev *pdev_cxt, flash_vol *pvol_cxt, unsigned int nPCAType)
{
    bms       *pstBms;
    bmi       *pst_bmi;
    bmf       *pSrcBMF;
    bmf       *pDstBMF;
    reservoir *meta;
    unsigned int     dev;
    unsigned int     nIdx1;
    unsigned int     nIdx2;
    unsigned int     nSctsPerBMSG;
    unsigned int     n1stPsnOfBMSG;
    unsigned int     bValidBMS;
	unsigned int	 bLastBMS;
    unsigned int     bFound;
    unsigned int     bIsAgeOverflow;
    unsigned short   nMaxAge;
    unsigned short   nTmpMaxAge;
    unsigned short   nCurAge;
    unsigned int     nCurBMSOffset;
    unsigned int     nTmpBMSOffset;
    unsigned short	 nRBIdx = 0;
    unsigned char    nNumOfLoadBMI = 0;
    unsigned char    nNumOfBMS = 0;
    int bRet = FALSE32;    
    
    DEBUG(TBML_DEBUG_LEVEL1,"[BBM: IN] ++_LoadBMS(dev:%d)\r\n", pdev_cxt->dev_no);
    
    dev 			= pdev_cxt->dev_no;
    meta 			= &(pdev_cxt->st_meta);
    pst_bmi 		= &(meta->st_bmi);
    pstBms 			= (bms *) (pdev_cxt->ptmp_mbuf);
    nSctsPerBMSG 	= pvol_cxt->scts_per_blk - SCTS_PER_PCH - SCTS_PER_PIA;
    
    if (nPCAType == TYPE_UPCA)
    {
        n1stPsnOfBMSG = meta->upcb[meta->cur_upcb_idx] * pvol_cxt->scts_per_blk +
                        SCTS_PER_PCH + SCTS_PER_PIA;
    }
    else /* TYPE_LPCA */
    {
        n1stPsnOfBMSG = meta->lpcb[meta->cur_lpcb_idx] * pvol_cxt->scts_per_blk +
                        SCTS_PER_PCH + SCTS_PER_PIA;
    }
    nMaxAge            = 0;
    nCurBMSOffset      = 0;
    nTmpMaxAge         = 0;
    nTmpBMSOffset      = 0;
    bFound             = FALSE32;
    bIsAgeOverflow     = FALSE32;
	bLastBMS		   = FALSE32;

    /**************************************************************
      [NOTE]
      In order to reduce the usage of STACK,
      first, search for a sector, which contains the latest BMS.
      second, read the sector, which contains the latest BMS.
    **************************************************************/
    nIdx1 = 0;
    while (nIdx1 < nSctsPerBMSG)
    {
        bValidBMS = TRUE32;
        for (nIdx2 = 0; nIdx2 < SCTS_PER_BMI; nIdx2 += NUM_OF_MIRRORS)
        {
            bRet = _ReadMetaData(n1stPsnOfBMSG + nIdx1 + nIdx2,
                              (unsigned char *) pstBms,
                              pdev_cxt,
                              pvol_cxt);
            if (bRet == FALSE32)
            {
                bValidBMS = FALSE32;
                break;
            }

            if ((pstBms->nInf != TYPE_INIT_CREATE)  &&
                (pstBms->nInf != TYPE_WRITE_ERR)    &&
                (pstBms->nInf != TYPE_ERASE_ERR)    &&
                (pstBms->nInf != TYPE_UPDATE_PIEXT))
            {
                bValidBMS = FALSE32;
                break;
            }
        }

        if (bValidBMS == FALSE32)
        {
            nIdx1 += SCTS_PER_BMI;
            continue;
        }

        nCurAge = pstBms->nAge;
        
        if (bIsAgeOverflow == FALSE32)
        {
            if (nCurAge == (unsigned short) 0xFFFF)
            {
                /* if the vaue of nCurAge is overflowed,
                   retry to compare nCurAge with nMaxAge from beginning. */
                bIsAgeOverflow = TRUE32;
                nIdx1          = 0;
                continue;
            }
        }
    
        if (bIsAgeOverflow == FALSE32)
        {
            if ((unsigned short) nTmpMaxAge < (unsigned short) nCurAge)
            {
                nTmpBMSOffset = (unsigned short) nIdx1;
                nTmpMaxAge    = nCurAge;
            }
        }
        else
        {
            /**********************************************************
            if the vaue of nCurAge is overflowed,
            retry to compare (INT16) nCurAge with
            (INT16) nMaxAge using signed short casting. 

            Ex)
            unsigned short casting   0xFFFE 0xFFFF 0x0000 0x0001 0x0002
              signed short casting       -2     -1      0      1      2

            In case of using unsigned short casting, the largest value
            is 0xFFFF. But in case of using signed short casting, the
            largest value is 2.
            **********************************************************/
            if ((INT16) nTmpMaxAge < (INT16) nCurAge)
            {
                nTmpBMSOffset = (unsigned short) nIdx1;
                nTmpMaxAge    = nCurAge;
            }
        }

        nIdx1 += SCTS_PER_BMI;

        if (bRet == TRUE32)
        {
            nNumOfBMS = nIdx1 - nTmpBMSOffset;
            bFound = TRUE32;
            nCurBMSOffset = nTmpBMSOffset;
            nMaxAge =nTmpMaxAge;
        }
       
    }

    if (bFound == FALSE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   NO VALID BMS\r\n");
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   <<CRITICAL ERROR>>\r\n");

        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_LoadBMS(dev:%d)\r\n", pdev_cxt->dev_no);
        return FALSE32;
    }

    pSrcBMF          = (bmf *) (pstBms->st_bmf);
    pDstBMF          = (bmf *) (meta->st_bmi.pst_bmf);

    for (nIdx1 = 0; nIdx1 < nNumOfBMS; nIdx1 += NUM_OF_MIRRORS)
    {
        /* There is no need to check the return-value of _ReadMetaData,
           because the return-value is already checked
           during search for latest BMS */
        _ReadMetaData(n1stPsnOfBMSG + nCurBMSOffset + nIdx1,
                      (unsigned char *) pstBms,
                      pdev_cxt,
                      pvol_cxt);    

        /* construct Block Map Information using BMS */
        for (nIdx2 = 0; nIdx2 < BMFS_PER_BMS; nIdx2++)
        {
            nRBIdx = pSrcBMF[nIdx2].rbn;

            /* if nRBIdx is 0xFFFF, slot (nIdx2) is empty */
			/* Last bmf field */
            if (nRBIdx == (unsigned short) 0xFFFF && pSrcBMF[nIdx2].sbn == (unsigned short) 0xFFFF)
			{
				bLastBMS = TRUE32;
                break;
			}
            if (pst_bmi->num_of_bmfs >= (unsigned short) (pvol_cxt->resrv_per_dev - RSV_META_BLKS))
            {
                DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR] pst_bmi->num_of_bmfs(%d) >= (unsigned short) (pvol_cxt->resrv_per_dev(%d) - RSV_META_BLKS)\r\n", 
                               pst_bmi->num_of_bmfs, pvol_cxt->resrv_per_dev);
                DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_LoadBMS(dev:%d)\r\n", pdev_cxt->dev_no);
                return FALSE32;
            }

            /* save Sbn and Rbn */
            pDstBMF[pst_bmi->num_of_bmfs].sbn = pSrcBMF[nIdx2].sbn;
            pDstBMF[pst_bmi->num_of_bmfs].rbn = (unsigned short) (nRBIdx + pvol_cxt->n1stsbn_resrv);

            /* increase the number of BMFs */
            pst_bmi->num_of_bmfs++;
            nNumOfLoadBMI = (nIdx1 >> 2) + 1;

        }
        /* if nRBIdx is 0xFFFF, slot (nIdx2) is empty */
        if (bLastBMS == TRUE32)
        {
            break;
        }
    }

    if (nPCAType == TYPE_UPCA)
    {      
        if ((nCurBMSOffset + nNumOfLoadBMI * SCTS_PER_BMI  * 2) > nSctsPerBMSG)
        {
            meta->next_ubms_off = 0;
        }
        else if ((nNumOfLoadBMI * BMS_PER_BMI  * BMFS_PER_BMS == pst_bmi->num_of_bmfs) &&
                (nCurBMSOffset + nNumOfLoadBMI * SCTS_PER_BMI * 2 + SCTS_PER_BMI ) > nSctsPerBMSG)
        {
            meta->next_ubms_off = 0;
        }
        else
        {
            meta->next_ubms_off = (unsigned short) (nCurBMSOffset + nNumOfLoadBMI * SCTS_PER_BMI );
        }
    
        /* store the age of UBMS */
        meta->st_bmi.ubmiage = nMaxAge;

    }
    else /* TYPE_LPCA */
    {
        if ((nCurBMSOffset + nNumOfLoadBMI * SCTS_PER_BMI  * 2) > nSctsPerBMSG)
        {
            meta->next_lbms_off = 0;
        }
        else if ((nNumOfLoadBMI * BMS_PER_BMI  * BMFS_PER_BMS == pst_bmi->num_of_bmfs) &&
                (nCurBMSOffset + nNumOfLoadBMI * SCTS_PER_BMI * 2 + SCTS_PER_BMI ) > nSctsPerBMSG)
        {
            meta->next_lbms_off = 0;
        }
        else
        {
            meta->next_lbms_off = (unsigned short) (nCurBMSOffset + nNumOfLoadBMI * SCTS_PER_BMI );
        }
    
        /* store the age of UBMS */
        meta->st_bmi.lbmiage = nMaxAge;
      
    }
    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_LoadBMS(dev:%d)\r\n", pdev_cxt->dev_no);

    return TRUE32;
}


/*
 *
 */
static unsigned int _LoadPI(flash_dev *pdev_cxt, flash_vol *pvol_cxt, part_info *pstPI)
{
    unsigned int     bRe = TRUE32;
    unsigned char     *pMBuf;

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM: IN] ++_LoadPI(dev:%d)\r\n", pdev_cxt->dev_no);

    pMBuf = pdev_cxt->ptmp_mbuf;
    
    if (pstPI != NULL)
    {
        /* load PI */
        if (_ReadMetaData(pdev_cxt->st_meta.lpcb[0] * pvol_cxt->scts_per_blk + SCTS_PER_PCH,
                        (unsigned char *) pMBuf,
                        pdev_cxt,
                        pvol_cxt) == FALSE32)
        {
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   NO VALID Partition Information\r\n");
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   <<CRITICAL ERROR>>\r\n");
            bRe = FALSE32;
        }

        AD_Memcpy(pstPI, pMBuf, sizeof(part_info));

    }

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --_LoadPI(dev:%d,bRe=%d)\r\n", pdev_cxt->dev_no, bRe);

    return bRe;
}


/*
 *
 */
void gbbm_metadata_update(flash_dev *pdev_cxt)
{
    unsigned int index;
    reservoir *meta;
    bmf *pst_bmf;

    DEBUG(TBML_DEBUG_LEVEL1,"tbc_metadata_update(dev=%d)\r\n", pdev_cxt->dev_no);
	
    meta = &(pdev_cxt->st_meta);
    pst_bmf = meta->st_bmi.pst_bmf;

	/* set a lpcb last block boundary */
    if (meta->alloc_lockarea > meta->lpcb[1]) {
        meta->alloc_lockarea = meta->lpcb[1];
    }
    
    if (meta->upcb[0] < meta->upcb[1]) {
        if (meta->alloc_unlockarea < meta->upcb[1]) {
            meta->alloc_unlockarea = meta->upcb[1];
        }        
    }
    else {
        if (meta->alloc_unlockarea < meta->upcb[0]) {
            meta->alloc_unlockarea = meta->upcb[0];
        }        
    }
    
    for (index = 0; index < meta->st_bmi.num_of_bmfs; index++) {
        if (pst_bmf[index].sbn == (unsigned short) 0xFFFF) {
            break;
        }
        if ((pdev_cxt->n1st_usbn <= pst_bmf[index].sbn) &&
            (meta->alloc_unlockarea   < pst_bmf[index].rbn)) {
            meta->alloc_unlockarea = pst_bmf[index].rbn;
        }
        
        if ((pdev_cxt->n1st_usbn > pst_bmf[index].sbn) &&
            (meta->alloc_lockarea   > pst_bmf[index].rbn)) {
            meta->alloc_lockarea = pst_bmf[index].rbn;
        }        
    }

    DEBUG(TBML_DEBUG_LEVEL1,"meta->alloc_lockarea:%d\r\n", meta->alloc_lockarea);
    DEBUG(TBML_DEBUG_LEVEL1,"meta->alloc_unlockarea:%d\r\n", meta->alloc_unlockarea);
    DEBUG(TBML_DEBUG_LEVEL1,"tbc_metadata_update(dev:%d)\r\n", pdev_cxt->dev_no);
}

/*
 *
 */
static void _SortBMI(bmi *pst_bmi)
{
    unsigned int  nIdx1;
    unsigned int  nIdx2;
    unsigned short  nMinSbn;
    bmf     stBmf;

    /* Sorts by ascending power of pst_bmi->pst_bmf[x].sbn */
    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:MSG] ** Sorts by ascending power of pst_bmi->pst_bmf[].sbn\r\n");

    for (nIdx1 = 0; nIdx1 < pst_bmi->num_of_bmfs; nIdx1++)
    {
        nMinSbn = pst_bmi->pst_bmf[nIdx1].sbn;

        for (nIdx2 = nIdx1 + 1; nIdx2 < pst_bmi->num_of_bmfs; nIdx2++)
        {
            if (nMinSbn > pst_bmi->pst_bmf[nIdx2].sbn)
            {
                nMinSbn               = pst_bmi->pst_bmf[nIdx2].sbn;

                stBmf                 = pst_bmi->pst_bmf[nIdx1];
                pst_bmi->pst_bmf[nIdx1] = pst_bmi->pst_bmf[nIdx2];
                pst_bmi->pst_bmf[nIdx2] = stBmf;
            }
        }
    }
}

/*
 *
 */
static void _SetAllocRB(flash_dev *pdev_cxt, flash_vol *pvol_cxt, unsigned int pbn)
{
    unsigned int  nRBIdx;

    /* get reserved block index */
    nRBIdx = pbn - pvol_cxt->n1stsbn_resrv;

    /* set the bit of Block Allocation BitMap as 1 */
    pdev_cxt->st_meta.pbad_bit_map[nRBIdx / 8] |= (unsigned char) (0x80 >> (nRBIdx % 8));
}


/*
 *
 */
static void _PrintPI(part_info *pstPI)
{   
    unsigned int  nIdx1;

    if (pstPI == NULL)
    {
        return;
    }
      
    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:MSG]  << PARTITION INFORMATION >>\r\n");    
    
    for (nIdx1 = 0; nIdx1 < pstPI->num_of_partentry; nIdx1++)
    {
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  id        : 0x%x\r\n", 
                       pstPI->pentry[nIdx1].id);

        switch (pstPI->pentry[nIdx1].attr)
        {
        case (BML_PI_ATTR_FROZEN | BML_PI_ATTR_RO):
            DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  nAttr      : RO + FROZEN (0x%x)\r\n",
                           pstPI->pentry[nIdx1].attr);
            break;
        case BML_PI_ATTR_RO:
            DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  nAttr      : RO (0x%x)\r\n",
                           pstPI->pentry[nIdx1].attr);
            break;
        case BML_PI_ATTR_RW:
            DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  nAttr      : RW (0x%x)\r\n",
                           pstPI->pentry[nIdx1].attr);
            break;
        }

        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  n1stVbn    : %d\r\n",
                       pstPI->pentry[nIdx1].n1st_vbn);
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  nNumOfBlks : %d\r\n",
                       pstPI->pentry[nIdx1].num_of_blks);
        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]  ---------------------  \r\n");
    }
}

/*
 *
 */
int bad_mapping_info(flash_vol *pvol_cxt, flash_dev *pdev_cxt)
{
    bmi       *pst_bmi;
    unsigned int     nIdx1;
    unsigned int     nPgIdx;
    unsigned char      aSBuf[XSR_SPARE_SIZE];
    unsigned char      nBadMark = 0x00;
	unsigned int 	nLLDRe;

	BML_ASSERT((pvol_cxt != NULL)&&(pdev_cxt != NULL));
	
    pst_bmi = &(pdev_cxt->st_meta.st_bmi);

    DEBUG(TBML_DEBUG_LEVEL0,"[BBM:   ]  << DevNO:%d MAPPING INFORMATION >>\r\n", pdev_cxt->dev_no);

    DEBUG(TBML_DEBUG_LEVEL0,"[BBM:   ]   Bad Mark Information\r\n");
    DEBUG(TBML_DEBUG_LEVEL0,"[BBM:   ]      - Bad Mark (0x%02x) by write error\r\n", BADMARKV_WRITEERR);
    DEBUG(TBML_DEBUG_LEVEL0,"[BBM:   ]      - Bad Mark (0x%02x) by erase error\r\n", BADMARKV_ERASEERR);

    DEBUG(TBML_DEBUG_LEVEL0,"[BBM:   ]   pdev_cxt->n1st_usbn = %d\r\n", pdev_cxt->n1st_usbn);

	AD_Memset(aSBuf, 0xFF, XSR_SPARE_SIZE);
	
    for (nIdx1 = 0; nIdx1 < pst_bmi->num_of_bmfs; nIdx1++)
    {
        for (nPgIdx = 0; nPgIdx < 2; nPgIdx++)
        {
            /* read bad mark */
			nLLDRe = pvol_cxt->lld_read(pdev_cxt->dev_no, pst_bmi->pst_bmf[nIdx1].sbn * pvol_cxt->scts_per_blk + nPgIdx * pvol_cxt->scts_per_pg, 1, NULL, (unsigned char *) aSBuf, LLD_FLAG_ECC_OFF); 
			if ( nLLDRe != LLD_SUCCESS ) {
				return LLD_READ_ERROR;
			}

            nBadMark = aSBuf[pvol_cxt->bad_pos];

            if ((nBadMark == 0x00)              ||
                (nBadMark == BADMARKV_WRITEERR) ||
                (nBadMark == BADMARKV_ERASEERR))
            {
                break;
            }
        }

        if (pdev_cxt->n1st_usbn <= pst_bmi->pst_bmf[nIdx1].sbn)
        {
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:   ]   %03d: Sbn[%4d] ==> Rbn[%4d] / UnLocked / BadMark:0x%02x\r\n",
                           nIdx1,
                           pst_bmi->pst_bmf[nIdx1].sbn,
                           pst_bmi->pst_bmf[nIdx1].rbn,
                           nBadMark);
        }
        else
        {
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:   ]   %03d: Sbn[%4d] ==> Rbn[%4d] / Locked   / BadMark:0x%02x\r\n",
                           nIdx1,
                           pst_bmi->pst_bmf[nIdx1].sbn,
                           pst_bmi->pst_bmf[nIdx1].rbn,
                           nBadMark);
        }
    }   

    DEBUG(TBML_DEBUG_LEVEL0,"[BBM:   ]   << Total : %d BAD-MAPPING INFORMATION >>\r\n", pst_bmi->num_of_bmfs);

	return BML_SUCCESS;
}


/*
 *
 */
static unsigned int _ChkBMIValidity(flash_vol *pvol_cxt, reservoir *meta)
{
    unsigned int nIdx;
    bmf   *pBmf;
    bmi   *pBMI;

    pBMI = &(meta->st_bmi);
    pBmf = pBMI->pst_bmf;

    for (nIdx = 0; nIdx < pBMI->num_of_bmfs; nIdx++)
    {
        if (pBmf[nIdx].sbn >= pvol_cxt->blks_per_dev)
        {
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   sbn(%d) >= pdev_cxt->nNumOfBlks(%d)\r\n",
                           pBmf[nIdx].sbn, pvol_cxt->blks_per_dev);
            return FALSE32;
        }

        if ((pBmf[nIdx].rbn >= pvol_cxt->blks_per_dev) ||
            (pBmf[nIdx].rbn <  pvol_cxt->n1stsbn_resrv + RSV_PCB_OFFSET))
        {
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   rbn(%d) >= pdev_cxt->blks_per_dev(%d)\r\n",
                           pBmf[nIdx].rbn, pvol_cxt->blks_per_dev);
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   rbn(%d) <  pdev_cxt->n1stsbn_resrv + RSV_PCB_OFFSET(%d)\r\n",
                           pBmf[nIdx].rbn, pvol_cxt->n1stsbn_resrv + RSV_PCB_OFFSET);
            return FALSE32;
        }

        if ((pBmf[nIdx].rbn == meta->upcb[0]) ||
            (pBmf[nIdx].rbn == meta->upcb[1]))
        {
            if (pBmf[nIdx].sbn < pvol_cxt->n1stsbn_resrv)
            {
                DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   rbn(%d) == meta->upcb[0]=%d\r\n",
                               pBmf[nIdx].rbn, meta->upcb[0]);
                DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   sbn(%d) <  pdev_cxt->n1stsbn_resrv(%d)\r\n",
                               pBmf[nIdx].sbn, pvol_cxt->n1stsbn_resrv);

                return FALSE32;
            }
        }

        if ((pBmf[nIdx].sbn == meta->upcb[0]) ||
            (pBmf[nIdx].sbn == meta->upcb[1]))
        {
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   sbn(%d) == meta->upcb[0]=%d\r\n",
                           pBmf[nIdx].sbn, meta->upcb[0]);
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   sbn(%d) == meta->upcb[1]=%d\r\n",
                           pBmf[nIdx].sbn, meta->upcb[1]);

            return FALSE32;
        }

        if ((pBmf[nIdx].rbn == meta->lpcb[0]))
        {
            if (pBmf[nIdx].sbn < pvol_cxt->n1stsbn_resrv)
            {
                DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   rbn(%d) == meta->lpcb[0]=%d\r\n",
                               pBmf[nIdx].rbn, meta->lpcb[0]);
                DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   sbn(%d) <  pdev_cxt->n1stsbn_resrv(%d)\r\n",
                               pBmf[nIdx].sbn, pvol_cxt->n1stsbn_resrv);

                return FALSE32;
            }
        }

        if (pBmf[nIdx].sbn == meta->lpcb[0])
        {
            DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   sbn(%d) == meta->lpcb[0]=%d\r\n",
                           pBmf[nIdx].sbn, meta->lpcb[0]);
            return FALSE32;
        }
    }

    return TRUE32;
}

static void _ReconstructBUMap(flash_vol *pvol_cxt, reservoir *meta)
{
    unsigned int index;
    unsigned int nBUIdx;
    unsigned int nMallocSize;


    nMallocSize = (pvol_cxt->blks_per_dev > BLKS_PER_BADUNIT) ? pvol_cxt->blks_per_dev / BLKS_PER_BADUNIT : 1;
    AD_Memset(meta->pbad_unit_map, 0x00, sizeof(badunit) * nMallocSize);

    /* pst_bmi->pst_bmf[].sbn should be sorted by ascending power */
    for (index = 0; index < meta->st_bmi.num_of_bmfs; index++)
    {
        nBUIdx = meta->st_bmi.pst_bmf[index].sbn / BLKS_PER_BADUNIT;

        if (meta->pbad_unit_map[nBUIdx].num_of_bmfs == 0)
            meta->pbad_unit_map[nBUIdx].n1stBMFIdx = (unsigned short) index;

        meta->pbad_unit_map[nBUIdx].num_of_bmfs++;
    }
}


/*
 * gbbm_mount function
 */
int gbbm_mount(flash_vol *pvol_cxt, flash_dev *pdev_cxt, part_info *pstPI, part_exinfo *pstPExt)
{
    unsigned int nIdx;
    reservoir *meta;
    int ret;

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM: IN] ++BBM_Mount(dev:%d,pstPI=0x%p,pstPExt=0x%p)\r\n",
        pdev_cxt->dev_no, pstPI, pstPExt);

    BML_ASSERT(sizeof(pch) == XSR_SECTOR_SIZE);
    BML_ASSERT((pvol_cxt != NULL) && (pdev_cxt != NULL));

    meta = &(pdev_cxt->st_meta);

    /* initializes the variables of reservoir structure */
    _init(pdev_cxt, pvol_cxt);

    /* In order to search latest UPCA and latest LPCA, scan reservoir */
    ret = _scan_reservoir(pdev_cxt, pvol_cxt);
    /* If there is no LPCA or no UPCA,
       it is unformated status or critical error */
    if (ret != BBM_SUCCESS) {
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:OUT] --BBM_Mount(dev:%d,pstPI=0x%p,pstPExt=0x%p)\r\n",
            pdev_cxt->dev_no, pstPI, pstPExt);
        return ret;
    }
    
	/* load PI from LPCA */
    if (_LoadPI(pdev_cxt, pvol_cxt, pstPI) != TRUE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   _LoadPI is failed\r\n");

        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --BBM_Mount(dev:%d,pstPI=0x%p,pstPExt=0x%p)\r\n",
            pdev_cxt->dev_no, pstPI, pstPExt);

        return BBM_LOAD_PI_FAILURE;
    }

    /* load LBMS from LPCA */
    if (_LoadBMS(pdev_cxt, pvol_cxt, TYPE_LPCA) != TRUE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   _LoadLBMS is failed\r\n");

        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --BBM_Mount(dev:%d,pstPI=0x%p,pstPExt=0x%p)\r\n",
            pdev_cxt->dev_no, pstPI, pstPExt);

        return BBM_LOAD_LBMS_FAILURE;
    }

    /* load PIExt from UPCA */
    _LoadPIExt(pdev_cxt, pvol_cxt, pstPExt);

    /* load UBMS from UPCA */
    if (_LoadBMS(pdev_cxt, pvol_cxt, TYPE_UPCA) != TRUE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   _LoadUBMS is failed\r\n");

        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --BBM_Mount(dev:%d,pstPI=0x%p,pstPExt=0x%p)\r\n",
            pdev_cxt->dev_no, pstPI, pstPExt);

        return BBM_LOAD_UBMS_FAILURE;
    }

    if (_ChkBMIValidity(pvol_cxt, meta) == FALSE32)
    {
        DEBUG(TBML_DEBUG_LEVEL0,"[BBM:ERR]   _ChkBMIValidity is failed\r\n");

        DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --BBM_Mount(dev:%d)\r\n",
            pdev_cxt->dev_no);

        return BBM_LOAD_UBMS_FAILURE;
    }

    /* sorts by ascending power of pst_bmi->pst_bmf[].sbn */
    _SortBMI(&(meta->st_bmi));


    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   constructing reserved Block Allocation BitMAP\r\n");

    /* construct pBABitMap using pBmi->pst_bmf[nIdx].rbn */
    for (nIdx = 0; nIdx < meta->st_bmi.num_of_bmfs; nIdx++)
    {
        _SetAllocRB(pdev_cxt, pvol_cxt, meta->st_bmi.pst_bmf[nIdx].rbn);
    }
    
    /* construct pBABitMap using initial bad blocks */
    for (nIdx = pvol_cxt->n1stsbn_resrv; nIdx < pvol_cxt->blks_per_dev; nIdx++)
    {
        if (pvol_cxt->lld_check_bad(pdev_cxt->dev_no, nIdx) == LLD_INIT_GOODBLOCK)
            continue;

        _SetAllocRB(pdev_cxt, pvol_cxt, nIdx);
    }

    /* construct pBABitMap using PCB */
    _SetAllocRB(pdev_cxt, pvol_cxt, pvol_cxt->n1stsbn_resrv);  /* for ERL block */
    _SetAllocRB(pdev_cxt, pvol_cxt, pvol_cxt->n1stsbn_resrv + 1); /* for REF block */
    _SetAllocRB(pdev_cxt, pvol_cxt, meta->lpcb[0]);
    _SetAllocRB(pdev_cxt, pvol_cxt, meta->lpcb[1]);
    _SetAllocRB(pdev_cxt, pvol_cxt, meta->upcb[0]);
    _SetAllocRB(pdev_cxt, pvol_cxt, meta->upcb[1]);

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   constructing is completed\r\n");

    _ReconstructBUMap(pvol_cxt, meta);

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:   ]   constructing bad unit map is completed\r\n");

    /* print partition information */
    _PrintPI(pstPI);

    DEBUG(TBML_DEBUG_LEVEL1,"[BBM:OUT] --BBM_Mount(dev:%d,pstPI=0x%p,pstPExt=0x%p)\r\n",
        pdev_cxt->dev_no, pstPI, pstPExt);

    return BBM_SUCCESS;
}

