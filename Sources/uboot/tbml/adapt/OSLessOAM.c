/*****************************************************************************/
/*                                                                           */
/* PROJECT : AnyStore II                                                     */
/* MODULE  : XSR AD                                                         */
/* NAME    : OSLess AD                                                      */
/* FILE    : OSLessAD.cpp                                                   */
/* PURPOSE : This file contain the OS Adaptation Modules for OSless platform */
/*           such as BootLoader                                              */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*          COPYRIGHT 2003-2005 SAMSUNG ELECTRONICS CO., LTD.                */
/*                          ALL RIGHTS RESERVED                              */
/*                                                                           */
/*   Permission is hereby granted to licenses of Samsung Electronics         */
/*   Co., Ltd. products to use or abstract this computer program only in     */
/*   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     */
/*   AGREEMENT for the sole purpose of implementing a product based on       */
/*   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   */
/*   use, or disseminate this computer program, whether in part or in whole, */
/*   are granted.                                                            */
/*                                                                           */
/*   Samsung Electronics Co., Ltd. makes no representation or warranties     */
/*   with respect to the performance of this computer program, and           */
/*   specifically disclaims any responsibility for any damages,              */
/*   special or consequential, connected with the use of this program.       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* REVISION HISTORY                                                          */
/*                                                                           */
/*   05-AUG-2003 [SongHo Yoon]  : first writing                              */
/*   07-AUG-2003 [Janghwan Kim] : added and modified source code to compile  */
/*   09-AUG-2003 [Janghwan Kim] : added AD_Memcpy, AD_Memset, AD_Memcmp   */
/*   19-JAN-2003 [SongHo Yoon]  : added AD_USE_STDLIB definition            */
/*   19-JAN-2003 [SongHo Yoon]  : fixed a bug in AD_AcquireSM()             */
/*                                change the return value (FALSE32 -> TRUE32)*/
/*                                                                           */
/*****************************************************************************/
#include <tbml_common.h>
#include <os_adapt.h>

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

/*****************************************************************************/
/* Global variables definitions                                              */
/*****************************************************************************/

/*****************************************************************************/
/* Local #defines                                                            */
/*****************************************************************************/
#define		LOCAL_MEM_SIZE		((200 * 1024) / sizeof(UINT32))

//#define		AD_USE_STDLIB

/*****************************************************************************/
/* Local typedefs                                                            */
/*****************************************************************************/

/*****************************************************************************/
/* Local constant definitions                                                */
/*****************************************************************************/

/*****************************************************************************/
/* Static variables definitions                                              */
/*****************************************************************************/
static UINT32  aMemBuf[LOCAL_MEM_SIZE];
static UINT32  nMallocPtr = 0;

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*      AD_Malloc                                                           */
/* DESCRIPTION                                                               */
/*      This function allocates memory for XSR                               */
/*                                                                           */
/* PARAMETERS                                                                */
/*      nSize       [IN]                                                     */
/*            Size to be allocated                                           */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      Pointer of allocated memory                                          */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called by function that wants to use memory         */
/*                                                                           */
/*****************************************************************************/
VOID *
AD_Malloc(UINT32 nSize)
{
	UINT32  nAlignSize;

	nAlignSize = nSize / sizeof(UINT32);
	if (nSize % sizeof(UINT32))
		nAlignSize++;

	nMallocPtr += nAlignSize;
 	if (nMallocPtr > LOCAL_MEM_SIZE)
 		return NULL;

	return (VOID *) &(aMemBuf[nMallocPtr - nAlignSize]);
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_Free                                                                 */
/* DESCRIPTION                                                               */
/*      This function free memory that XSR allocated                         */
/*                                                                           */
/* PARAMETERS                                                                */
/*  pMem        Pointer to be free                                           */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called by function that wants to free memory        */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
VOID
AD_Free(VOID  *pMem)
{
//	_AD_ASSERT(0);
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_Memcpy                                                               */
/* DESCRIPTION                                                               */
/*      This function copies data from source to destination.                */
/*                                                                           */
/* PARAMETERS                                                                */
/*  pDst        Destination array Pointer to be copied                       */
/*  pSrc        Source data allocated Pointer                                */
/*  nLen        length to be copied                                          */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called by function that wants to copy source buffer */
/*  to destination buffer.                                                   */
/*                                                                           */
/*****************************************************************************/
VOID
AD_Memcpy(VOID *pDst, VOID *pSrc, UINT32 nLen)
{
#if defined(AD_USE_STDLIB)
	memcpy(pDst, pSrc, nLen);
#else /* AD_USE_STDLIB */
	register INT32 	nCnt;
	register UINT8	*pD8, *pS8;
	register INT32	nL = nLen;
	register UINT32	*pD32, *pS32;

	pD8 = (UINT8*)pDst;
	pS8 = (UINT8*)pSrc;
	
	if ( ((INT32)pD8 % sizeof(UINT32)) == ((INT32)pS8 % sizeof(UINT32)) )
	{
		while ( (INT32)pD8 % sizeof(UINT32) )
		{
			*pD8++ = *pS8++;
			nL--;

			if( nL <= 0 )
			    return;
		}
	
		pD32 = (UINT32*)pD8;
		pS32 = (UINT32*)pS8;
		
		for (nCnt = 0; nCnt <(INT32)(nL / sizeof(UINT32)); nCnt++)
			*pD32++ = *pS32++;
	
		pD8 = (UINT8*)pD32;
		pS8 = (UINT8*)pS32;
			
		while( nL % sizeof(UINT32) )
		{
			*pD8++ = *pS8++;
			nL--;
		}
	}
	else
	{
		for( nCnt = 0; nCnt < nL; nCnt++)
			*pD8++ = *pS8++;
	}
#endif /* AD_USE_STDLIB */		
}

void memcpy(void *pDst, void *pSrc, int nLen)
{
	AD_Memcpy(pDst, pSrc, nLen);
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_Memset                                                               */
/* DESCRIPTION                                                               */
/*      This function set data of specific buffer.                           */
/*                                                                           */
/* PARAMETERS                                                                */
/*  pSrc        Source data allocated Pointer                                */
/*  nV          Value to be setted                                           */
/*  nLen        length to be setted                                          */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called by function that wants to set source buffer  */
/*  own data.                                                                */
/*                                                                           */
/*****************************************************************************/
VOID
AD_Memset(VOID *pDst, UINT8 nV, UINT32 nLen)
{
#if defined(AD_USE_STDLIB)
	memset(pDst, nV, nLen);
#else /* AD_USE_STDLIB */
	register UINT8	*pD8;
	register UINT32	*pD32;
	register UINT8	nV8 = nV;
	register UINT32	nV32 = (UINT32)( nV << 24 | nV << 16 | nV << 8 | nV ); 
	register INT32	nL = (INT32)nLen;
	register UINT32 nCnt;

	pD8 = (UINT8*)pDst;
	
	while ( (INT32)pDst % sizeof(UINT32) )
	{
		*pD8++ = nV8;
		nL--;

		if( nL <= 0 )
		    return;
	}
	
	pD32 = (UINT32*)pD8;
	for (nCnt = 0; nCnt <(INT32)(nL / sizeof(UINT32)); nCnt++)
		*pD32++ = nV32;

	pD8 = (UINT8*)pD32;	
	while( nL % sizeof(UINT32) )
	{
		*pD8++ = nV8;
		nL--;
	}
#endif /* AD_USE_STDLIB */
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_Memcmp                                                               */
/* DESCRIPTION                                                               */
/*      This function compare data of two buffer.                            */
/*                                                                           */
/* PARAMETERS                                                                */
/*  pSrc        Source data allocated Pointer                                */
/*  pDst        Destination array Pointer to be compared                     */
/*  nLen        length to be compared                                        */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called by function that wants to compare source     */
/*  buffer and destination buffer.                                           */
/*                                                                           */
/*****************************************************************************/
INT32
AD_Memcmp(VOID  *pSrc, VOID  *pDst, UINT32 nLen)
{
#if defined(AD_USE_STDLIB)
	return memcmp(pSrc, pDst, nLen);
#else /* AD_USE_STDLIB */
	UINT8 *pS1 = (UINT8 *)pSrc;
	UINT8 *pD1 = (UINT8 *)pDst;

	while (nLen--)
	{
		if (*pS1 != *pD1)
			return (*pS1 - *pD1);
        pS1++;
        pD1++;
	}

	return 0;
#endif /* AD_USE_STDLIB */
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_CreateSM                                                             */
/* DESCRIPTION                                                               */
/*      This function creates semaphore.                                     */
/*                                                                           */
/* PARAMETERS                                                                */
/*  pHandle        Handle of semaphore                                       */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      If this function creates semaphore successfully, it returns TRUE32.  */
/*      else it returns FALSE32.                                             */
/* NOTES                                                                     */
/*      This function is called by function that wants to create semaphore   */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
BOOL32
AD_CreateSM(SM32 *pHandle)
{
	return TRUE32;
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_GetROLockFlag                                                        */
/* DESCRIPTION                                                               */
/*      This function returns TRUE32 in _IsROPartition()                     */
/*                                                                           */
/* PARAMETERS                                                                */
/*  NONE                                                                     */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      TREU32                                                               */
/*                                                                           */
/* NOTES                                                                     */
/*                                                                           */
/*****************************************************************************/
BOOL32
AD_GetROLockFlag(VOID)
{
    return TRUE32;
}
/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_DestroySM                                                            */
/* DESCRIPTION                                                               */
/*      This function destroys semaphore.                                    */
/*                                                                           */
/* PARAMETERS                                                                */
/*  nHandle        Handle of semaphore to be destroyed                       */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      If this function destroys semaphore successfully, it returns TRUE32. */
/*      else it returns FALSE32.                                             */
/* NOTES                                                                     */
/*      This function is called by function that wants to destroy semaphore  */
/*                                                                           */
/*****************************************************************************/
BOOL32
AD_DestroySM(SM32 nHandle)
{
	return TRUE32;
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_AcquireSM                                                            */
/* DESCRIPTION                                                               */
/*      This function acquires semaphore.                                    */
/*                                                                           */
/* PARAMETERS                                                                */
/*  nHandle        Handle of semaphore to be acquired                        */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      If this function acquires semaphore successfully, it returns TRUE32. */
/*      else it returns FALSE32.                                             */
/* NOTES                                                                     */
/*      This function is called by function that wants to acquire semaphore  */
/*                                                                           */
/*****************************************************************************/
BOOL32
AD_AcquireSM(SM32 nHandle)
{
	return TRUE32;
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_ReleaseSM                                                            */
/* DESCRIPTION                                                               */
/*      This function releases semaphore.                                    */
/*                                                                           */
/* PARAMETERS                                                                */
/*      nHandle    [IN]                                                      */
/*            Handle of semaphore to be released                             */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      If this function releases semaphore successfully, it returns TRUE32. */
/*      else it returns FALSE32.                                             */
/* NOTES                                                                     */
/*      This function is called by function that wants to release semaphore  */
/*                                                                           */
/*****************************************************************************/
BOOL32
AD_ReleaseSM(SM32 nHandle)
{
	return TRUE32;
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_Pa2Va                                                                */
/* DESCRIPTION                                                               */
/*      This function gets virtual address for NAND device physical address  */
/*                                                                           */
/* PARAMETERS                                                                */
/*  nPAddr        physical address of NAND device                            */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      Virtual address of NAND device that Symbian OS using                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function makes virtual address for NAND device when system      */
/*  initialized                                                              */
/*                                                                           */
/*****************************************************************************/
UINT32
AD_Pa2Va(UINT32 nPAddr)
{
    return (nPAddr);
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_InitInt                                                              */
/* DESCRIPTION                                                               */
/*      This function initialize interrupt for NAND device                   */
/*                                                                           */
/* PARAMETERS                                                                */
/*  Caller        Class that this function called                            */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called in Media driver interface layer              */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

VOID
AD_InitInt(VOID *Caller)
{
}


/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_BindInt                                                              */
/* DESCRIPTION                                                               */
/*      This function binds interrupt for NAND device                        */
/*                                                                           */
/* PARAMETERS                                                                */
/*  none                                                                     */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called in Media driver interface layer              */
/*                                                                           */
/*****************************************************************************/
VOID
AD_BindInt(VOID)
{
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_EnableInt                                                            */
/* DESCRIPTION                                                               */
/*      This function enables interrupt for NAND device                      */
/*                                                                           */
/* PARAMETERS                                                                */
/*  none                                                                     */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called in Media driver interface layer              */
/*                                                                           */
/*****************************************************************************/
VOID
AD_EnableInt(VOID)
{    
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_DisableInt                                                           */
/* DESCRIPTION                                                               */
/*      This function disables interrupt for NAND device                     */
/*                                                                           */
/* PARAMETERS                                                                */
/*  none                                                                     */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called in Media driver interface layer              */
/*                                                                           */
/*****************************************************************************/
VOID
AD_DisableInt(VOID)
{    
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_ClearInt                                                             */
/* DESCRIPTION                                                               */
/*      This function clear interrupt for NAND device                        */
/*                                                                           */
/* PARAMETERS                                                                */
/*  none                                                                     */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called in Media driver interface layer              */
/*                                                                           */
/*****************************************************************************/
VOID
AD_ClearInt(VOID)
{    
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_ResetTimer                                                           */
/* DESCRIPTION                                                               */
/*      This function reset timer for using asynchronous write operation     */
/*                                                                           */
/* PARAMETERS                                                                */
/*  none                                                                     */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called in STL layer                                 */
/*                                                                           */
/*****************************************************************************/

static UINT32 nTimerCounter = 0;
static UINT16 nTimerPrevCnt = 0;

VOID 
AD_ResetTimer(VOID)
{    
    nTimerPrevCnt = 0;
    nTimerCounter = 0;
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_GetTime                                                              */
/* DESCRIPTION                                                               */
/*      This function returns cureent timer value                            */
/*                                                                           */
/* PARAMETERS                                                                */
/*  none                                                                     */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      Current timer value                                                  */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is called in STL layer                                 */
/*                                                                           */
/*****************************************************************************/
UINT32 
AD_GetTime(VOID)
{
    return nTimerCounter++;
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_Debug                                                                */
/* DESCRIPTION                                                               */
/*      This function prints debug message                                   */
/*                                                                           */
/* PARAMETERS                                                                */
/*  pFmt    data to be printed                                               */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*      This function is used when system can not support debug              */
/*  print function                                                           */
/*                                                                           */
/*****************************************************************************/
//#if !defined(XSR_NBL2)
static char gDbgStr[1024];
//extern "C" void UART_PutString(char *pt);
//#endif /* XSR_ONBL2 */
//extern "C" void Uart_SendString(char *pt);

VOID
AD_Debug(VOID  *pFmt, ...)
{
//#if !defined(XSR_NBL2)	
    va_list ap;

    va_start(ap, pFmt);
    vsprintf(gDbgStr, (const char *) pFmt, ap);
    Uart_SendString((char *) gDbgStr);
    va_end(ap);
//#endif /* XSR_ONBL2 */
}

/*****************************************************************************/
/*                                                                           */
/* NAME                                                                      */
/*  AD_WaitNMSec                                                            */
/* DESCRIPTION                                                               */
/*      This function waits N msec                                           */
/*                                                                           */
/* PARAMETERS                                                                */
/*  nNMSec        msec time for waiting                                      */
/*                                                                           */
/* RETURN VALUES                                                             */
/*      none                                                                 */
/*                                                                           */
/* NOTES                                                                     */
/*                                                                           */
/*****************************************************************************/
VOID
AD_WaitNMSec(UINT32 nNMSec)
{
	static volatile UINT32 nCnt1;
	static volatile UINT32 nCnt2;


	for (nCnt1 = 0; nCnt1 < nNMSec; nCnt1++)
	{
		for (nCnt2 = 0; nCnt2 < 1000; nCnt2++)
		{
		}
	}
}

/**
 * vsprintf - Format a string and place it in a buffer
 * @buf: The buffer to place the result into
 * @fmt: The format string to use
 * @args: Arguments for the format string
 *
 * Call this function if you are already dealing with a va_list.
 * You probably want sprintf instead.
 */

#define ZEROPAD 1               /* pad with zero */
#define SIGN    2               /* unsigned/signed long */
#define PLUS    4               /* show plus */
#define SPACE   8               /* space if plus */
#define LEFT    16              /* left justified */
#define SPECIAL 32              /* 0x */
#define LARGE   64              /* use 'ABCDEF' instead of 'abcdef' */

#define is_digit(c)     ((c) >= '0' && (c) <= '9')

#define do_div(n,base) ({ \
int __res; \
__res = ((unsigned long) n) % (unsigned) base; \
n = ((unsigned long) n) / (unsigned) base; \
__res; })

size_t strnlen(const char * s, size_t count)
{
        const char *sc;

        for (sc = s; count-- && *sc != '\0'; ++sc)
                /* nothing */;
        return sc - s;
}

static int skip_atoi(const char **s)
{
        int i=0;

        while (is_digit(**s))
                i = i*10 + *((*s)++) - '0';
        return i;
}

static char * number(char * str, long num, int base, int size, int precision
        ,int type)
{
        char c,sign,tmp[66];
        const char *digits="0123456789abcdefghijklmnopqrstuvwxyz";
        int i;

        if (type & LARGE)
                digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        if (type & LEFT)
                type &= ~ZEROPAD;
        if (base < 2 || base > 36)
                return 0;
        c = (type & ZEROPAD) ? '0' : ' ';
        sign = 0;
        if (type & SIGN) {
                if (num < 0) {
                        sign = '-';
                        num = -num;
                        size--;
                } else if (type & PLUS) {
                        sign = '+';
                        size--;
                } else if (type & SPACE) {
                        sign = ' ';
                        size--;
                }
        }
        if (type & SPECIAL) {
                if (base == 16)
                        size -= 2;
                else if (base == 8)
                        size--;
        }
        i = 0;
        if (num == 0)
                tmp[i++]='0';
        else while (num != 0)
                tmp[i++] = digits[do_div(num,base)];
        if (i > precision)
                precision = i;
        size -= precision;
        if (!(type&(ZEROPAD+LEFT)))
                while(size-->0)
                        *str++ = ' ';
        if (sign)
                *str++ = sign;
        if (type & SPECIAL) {
                if (base==8)
                        *str++ = '0';
                else if (base==16) {
                        *str++ = '0';
                        *str++ = digits[33];
                }
        }
        if (!(type & LEFT))
                while (size-- > 0)
                        *str++ = c;
        while (i < precision--)
                *str++ = '0';
        while (i-- > 0)
                *str++ = tmp[i];
        while (size-- > 0)
                *str++ = ' ';
        return str;
}

int vsprintf(char *buf, const char *fmt, va_list args)
{
        int len;
        unsigned long num;
        int i, base;
        char * str;
        const char *s;

        int flags;              /* flags to number() */

        int field_width;        /* width of output field */
        int precision;          /* min. # of digits for integers; max
                                   number of chars for from string */
        int qualifier;          /* 'h', 'l', or 'L' for integer fields */

        for (str=buf ; *fmt ; ++fmt) {
                if (*fmt != '%') {
                        *str++ = *fmt;
                        continue;
                }

                /* process flags */
                flags = 0;
                repeat:
                        ++fmt;          /* this also skips first '%' */
                        switch (*fmt) {
                                case '-': flags |= LEFT; goto repeat;
                                case '+': flags |= PLUS; goto repeat;
                                case ' ': flags |= SPACE; goto repeat;
                                case '#': flags |= SPECIAL; goto repeat;
                                case '0': flags |= ZEROPAD; goto repeat;
                                }

                /* get field width */
                field_width = -1;
                if (is_digit(*fmt))
                        field_width = skip_atoi(&fmt);
                else if (*fmt == '*') {
                        ++fmt;
                        /* it's the next argument */
                        field_width = va_arg(args, int);
                        if (field_width < 0) {
                                field_width = -field_width;
                                flags |= LEFT;
                        }
                }

                /* get the precision */
                precision = -1;
                if (*fmt == '.') {
                        ++fmt;
                        if (is_digit(*fmt))
                                precision = skip_atoi(&fmt);
                        else if (*fmt == '*') {
                                ++fmt;
                                /* it's the next argument */
                                precision = va_arg(args, int);
                        }
                        if (precision < 0)
                                precision = 0;
                }

                /* get the conversion qualifier */
                qualifier = -1;
                if (*fmt == 'h' || *fmt == 'l' || *fmt == 'L') {
                        qualifier = *fmt;
                        ++fmt;
                }

                /* default base */
                base = 10;

                switch (*fmt) {
                case 'c':
                        if (!(flags & LEFT))
                                while (--field_width > 0)
                                        *str++ = ' ';
                        *str++ = (unsigned char) va_arg(args, int);
                        while (--field_width > 0)
                                *str++ = ' ';
                        continue;

                case 's':
                        s = va_arg(args, char *);
                        if (!s)
                                s = "<NULL>";

                        len = strnlen(s, precision);

                        if (!(flags & LEFT))
                                while (len < field_width--)
                                        *str++ = ' ';
                        for (i = 0; i < len; ++i)
                                *str++ = *s++;
                        while (len < field_width--)
                                *str++ = ' ';
                        continue;

                case 'p':
                        if (field_width == -1) {
                                field_width = 2*sizeof(void *);
                                flags |= ZEROPAD;
                        }
                        str = number(str,
                                (unsigned long) va_arg(args, void *), 16,
                                field_width, precision, flags);
                        continue;


                case 'n':
                        if (qualifier == 'l') {
                                long * ip = va_arg(args, long *);
                                *ip = (str - buf);
                        } else {
                                int * ip = va_arg(args, int *);
                                *ip = (str - buf);
                        }
                        continue;

                case '%':
                        *str++ = '%';
                        continue;

                /* integer number formats - set up the flags and "break" */
                case 'o':
                        base = 8;
                        break;

                case 'X':
                        flags |= LARGE;
                case 'x':
                        base = 16;
                        break;

                case 'd':
                case 'i':
                        flags |= SIGN;
                case 'u':
                        break;

                default:
                        *str++ = '%';
                        if (*fmt)
                                *str++ = *fmt;
                        else
                                --fmt;
                        continue;
                }
                if (qualifier == 'l')
                        num = va_arg(args, unsigned long);
                else if (qualifier == 'h') {
                        num = (unsigned short) va_arg(args, int);
                        if (flags & SIGN)
                                num = (short) num;
                } else if (flags & SIGN)
                        num = va_arg(args, int);
                else
                        num = va_arg(args, unsigned int);
                str = number(str, num, base, field_width, precision, flags);
        }
        *str = '\0';
        return str-buf;
}

