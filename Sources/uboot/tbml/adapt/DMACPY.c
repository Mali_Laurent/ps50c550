/**
 * @file    DMACPY.c
 * 
 *
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 *
 */

#include <tbml_common.h>
#include "../../board/omap2420.h"
#include "../../board/2420board.h"


void DMACPY(unsigned char *dst, unsigned int *src, unsigned int byte_len)
{

   DMA4_CSR0 = 0xff;

  DMA4_CSSA0 = (unsigned int)src;
  DMA4_CDSA0 = (unsigned int)dst;

  DMA4_CEN0 = byte_len >> 1;	/* channel element number */

    DMA4_CCR0 = 1<<24 | 0<<23 | 0<<22 | 0<<21 | 0<<19 | 1<<18 | 0<<17 
              | 0<<16 | 1<<14 | 1<<12 | 1<<8 | 1<<7 | 1<<6 | 1<<5 | 0;
           
    
    /* poll if dma finished */
    while (!(DMA4_CSR0 & 0x8))
    {
    	//polling if DMA finished
    }	
	return;
}

void DMA_WRITE(unsigned char *src, unsigned int *dst, unsigned int byte_len)
{

   DMA4_CSR0 = 0xff;

  DMA4_CSSA0 = (unsigned int)src;
  DMA4_CDSA0 = (unsigned int)dst;


  DMA4_CEN0 = byte_len >> 1;	/* channel element number */

    DMA4_CCR0 = 1<<24 | 0<<23 | 0<<22 | 0<<21 | 0<<19 | 1<<18 | 0<<17 
              | 0<<16 | 1<<14 | 1<<12 | 1<<8 | 1<<7 | 1<<6 | 1<<5 | 0;
           
    
    /* poll if dma finished */
    while (!(DMA4_CSR0 & 0x8))
    {
    	//polling if DMA finished
    }	
	return;
}

void DMACPY_init(void)
{

  	/* dma global reg set-up */
//	DMA4_GCR = 1<<16 | 16;

  DMA4_CSDP0 = 0<<21 | 1<<20 | 0<<19 | 1<<18 | 2<<16 | 1<14 | 1<<13
             | 0<<9 | 2<<7 | 1<<6 | 0<<2 | 1<<0;
		// MPU L1 interrupt masked
  DMA4_CICR0 = 0xFF;
	
	/* channel frame number */
  DMA4_CFN0 = 0x1;

	return;
}
