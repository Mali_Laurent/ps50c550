/**
 * @file    gbbm.c
 * @brief   This file manage bad blocks
 *
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 *
 */

#include <tbml_common.h>
#include <os_adapt.h>
#include <onenand_interface.h>

#include <stdarg.h>

#define XSR_MAX_DEVICES	1
/*****************************************************************************/
/* Global variables definitions                                              */
/*****************************************************************************/

/*****************************************************************************/
/* Local #defines                                                            */
/*****************************************************************************/
#define		LOCAL_MEM_SIZE		((100 * 1024) / sizeof(UINT32))

/*****************************************************************************/
/* Local typedefs                                                            */
/*****************************************************************************/

/*****************************************************************************/
/* Local constant definitions                                                */
/*****************************************************************************/

/*****************************************************************************/
/* Static variables definitions                                              */
/*****************************************************************************/

/*****************************************************************************/
/* Static function prototypes                                                */
/*****************************************************************************/

/*****************************************************************************/
/************* MUTUX Initialization for QAM_Create****************************/
/*****************************************************************************/

VOID *
AD_Malloc(UINT32 nSize)
{
	return malloc(nSize);
}

VOID 
AD_Free(VOID  *pMem)
{
		free(pMem);
}

// ij.jang
static void oam_memcpy1(UINT8* dst, UINT8* src, UINT32 len)
{
	while ( len > (sizeof(*dst)-1) ) {
		*dst = *src;
		dst++;
		src++;
		len-=sizeof(*dst);
	}
}

// ij.jang
static void oam_memcpy2(UINT16* dst, UINT16* src, UINT32 len)
{
	while ( len > (sizeof(*dst)-1) ) {
		*dst = *src;
		dst++;
		src++;
		len-=sizeof(*dst);
	}
}

// ij.jang
static void oam_memcpy16(UINT32* dst, UINT32* src, UINT32 len)
{
	asm volatile (
	    "loop:" "\n\t"
	    "ldmia  %1!,{r3-r6}" "\n\t"
	    "stmia  %0!,{r3-r6}" "\n\t"
	    "subs   %2, %2, #16" "\n\t"
	    "bne loop" "\n\t"
	    :
	    :"r"(dst),"r"(src),"r"(len)
	    :"cc","r3","r4","r5","r6"
	);
}

// ij.jang
static void oam_memcpy128(UINT32* dst, UINT32* src, UINT32 len)
{ 
	/* r8 is reserved for global_data */
	asm volatile (
		"stmdb	r13!, {r3-r10}"	"\n\t"
		"1:"	"\n\t"
		"ldmia	%1!, {r3-r10}"	"\n\t"
		"stmia	%0!, {r3-r10}"	"\n\t"

		"ldmia	%1!, {r3-r10}"	"\n\t"
		"stmia	%0!, {r3-r10}"	"\n\t"

		"ldmia	%1!, {r3-r10}"	"\n\t"
		"stmia	%0!, {r3-r10}"	"\n\t"

		"ldmia	%1!, {r3-r10}"	"\n\t"
		"stmia	%0!, {r3-r10}"	"\n\t"

		"subs	%2, %2, #128"	"\n\t"
		"bne	1b"		"\n\t"
		"ldmia	r13!, {r3-r10}"	"\n\t"
		:"=r"(dst), "=r"(src), "=r"(len)
		:"0"(dst), "1"(src), "2"(len)
		:"cc"
	);
}

VOID
AD_Memcpy(VOID *pDst, VOID *pSrc, UINT32 nLen)
{
	/* 02,Aug,2007 ij.jang : use original memcpy,
	   ldm/stm memcpy used only in sector-read */
#if 1
	memcpy(pDst, pSrc, nLen);		
#else
	/* 29.March.2007 ij.jang : fix zero-length copy bug */
	/* 07.July.2007 ij.jang : copy "memcpy code" from xsr/OAM/... */
	if (!nLen)
		return;

	if ( !((UINT32)pDst & 0x3) && !((UINT32)pSrc & 0x3) && !(nLen & 0xF) ) {
		if ( !(nLen & 0x7F) )
			oam_memcpy128 ((UINT32*)pDst, (UINT32*)pSrc, nLen);
		else
			oam_memcpy16 ((UINT32*)pDst, (UINT32*)pSrc, nLen);
	} else if ( !((UINT32)pDst & 1) && !((UINT32)pSrc & 1) && !(nLen &1) ) {
		oam_memcpy2 ((UINT16*)pDst, (UINT16*)pSrc, nLen);
	} else {
		oam_memcpy1 ((UINT8*)pDst, (UINT8*)pSrc, nLen);
	}
#endif
}


VOID
AD_Memset(VOID *pDst, UINT8 nV, UINT32 nLen)
{
	memset(pDst, nV, nLen);
}

INT32
AD_Memcmp(VOID  *pSrc, VOID  *pDst, UINT32 nLen)
{
	return memcmp(pSrc, pDst, nLen);
}

BOOL32
AD_CreateSM(SM32 *pHandle)
{
	return TRUE32;
}

BOOL32
AD_DestroySM(SM32 nHandle)
{
	return TRUE32;

}

BOOL32
AD_AcquireSM(SM32 nHandle)
{
	return TRUE32;
}

BOOL32
AD_ReleaseSM(SM32 nHandle)
{
	return(TRUE32);
}
static char gDbgStr[1024];
VOID
AD_Debug(VOID  *pFmt, ...)
{
    va_list ap;

    va_start(ap, pFmt);
    vsprintf(gDbgStr, (const char *) pFmt, ap);
    puts(gDbgStr);
    va_end(ap);
}

VOID
AD_Idle(UINT32 nMode)
{

}

UINT32
AD_Pa2Va(UINT32 nPAddr)
{
	return (nPAddr);
}

VOID
AD_ResetTimer(VOID)
{

}

UINT32 
AD_GetTime(VOID)
{
	static UINT32 nCounter = 0;

	return nCounter++;
}

VOID
AD_WaitNMSec(UINT32 nNMSec)
{

}

BOOL32
AD_GetROLockFlag(VOID)
{
	return 1;
}

#ifdef ASYNC_MODE
VOID
AD_InitInt(UINT32 nLogIntId)
{   
}

VOID
AD_BindInt(UINT32 nLogIntId, UINT32 nPhyIntId)
{
}

VOID
AD_EnableInt(UINT32 nLogIntId, UINT32 nPhyIntId)
{
}
    
VOID
AD_DisableInt(UINT32 nLogIntId, UINT32 nPhyIntId)
{
}

VOID
AD_ClearInt(UINT32 nLogIntId, UINT32 nPhyIntId)
{
}
#endif
