/**
 * @file    onenand_interface.c
 * @brief   This file is onenand interface file
 *
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 *
 */

/* ij.jang */
#include <config.h>
#include <tbml_common.h>
#include <os_adapt.h>
#include <onenand_interface.h>
#include <onenand_lld.h>
#include "../TinyBML/onenand_reg.h"
#include "../TinyBML/onenand.h"

/* sh.kim - start */
void inter_low_ftable(unsigned int vol, void *pstFunc)
{
	lld_ftable *pstLFT;

	pstLFT = (lld_ftable*)pstFunc;
	
	pstLFT[vol].init			= ONLD_init;
	pstLFT[vol].open			= ONLD_open;
	pstLFT[vol].close			= ONLD_close;
	pstLFT[vol].read			= ONLD_read;
	pstLFT[vol].write			= ONLD_write;
	pstLFT[vol].erase			= ONLD_erase;
	pstLFT[vol].dev_info		= ONLD_getDevInfo;
	pstLFT[vol].check_bad		= ONLD_chkInitBadBlk;
	pstLFT[vol].flush			= ONLD_flushOp;
	pstLFT[vol].set_rw		= ONLD_setRWArea;
	pstLFT[vol].ioctl			= ONLD_iOCtl;
	pstLFT[vol].mread			= ONLD_mRead;

}    
/* sh.kim - end */

static vol_param gstParm[XSR_MAX_VOL];

void* inter_getparam(void)
{
	gstParm[0].nBaseAddr[0]	= CFG_ONENAND_BASE;
	gstParm[0].nBaseAddr[1]	= NOT_MAPPED;
	gstParm[0].nBaseAddr[2]	= NOT_MAPPED;
	gstParm[0].nBaseAddr[3]	= NOT_MAPPED;

	gstParm[0].ecc_flag		= HW_ECC;
	gstParm[0].nLSchemePol	= HW_LOCK_SCHEME;
	gstParm[0].bByteAlign	= TRUE32;
	gstParm[0].nDevsInVol	= 1;
	gstParm[0].pExInfo		= NULL;
/* sh.kim - start */
	gstParm[1].nBaseAddr[0] 	= CFG_ONENAND_BASE1;
	gstParm[1].nBaseAddr[1]	= NOT_MAPPED;
	gstParm[1].nBaseAddr[2]	= NOT_MAPPED;
	gstParm[1].nBaseAddr[3]	= NOT_MAPPED;

	gstParm[1].ecc_flag		= HW_ECC;
	gstParm[1].nLSchemePol	= HW_LOCK_SCHEME;
	gstParm[1].bByteAlign	= TRUE32;
	gstParm[1].nDevsInVol	= 1;
	gstParm[1].pExInfo		= NULL;
/* sh.kim - end */
	return (void *) gstParm;
}

