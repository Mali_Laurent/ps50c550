 /*
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * Technical support:
 *   Samsung Flash software Group 
 * 	 daeho kim <daehosky.kim@samsung.com>
 */ 

#include <tbml_common.h>
#include <os_adapt.h>
#include <onenand_interface.h>
#include <tbml_interface.h>
#include <onenand_lld.h>
#include <tbml_type.h>
#include "u-boot_interface.h"

static	tbml_vol_spec	vol_spec[XSR_MAX_DEV];
static	part_info		part_spec[XSR_MAX_DEV];

/* sh.kim - start */
static	int bml_initialized[2] = {0, 0};
/* sh.kim - end */

#define NR_BLOCKS(pi, i)	(pi->stPEntry[i].n1stVbn + pi->stPEntry[i].nNumOfBlks)
#define SPP(volume)		(vol_spec[volume].scts_per_pg)
#define SPB(volume)		(vol_spec[volume].scts_per_pg * vol_spec[volume].pgs_per_blk)

/*
 * bml_open perform tbml_init, tbml_open.
 * Get a BML_Info informat.
 */

/* sh.kim - start */
int bml_open(int volume, struct BML_Info *device_type)
{
	int ret;

        if (!bml_initialized[volume])
	{
		ret = tbml_init(volume);
		if (ret == BML_ALREADY_INITIALIZED)
		{
       			printf("tbml_init Already opened.. Init success\n");
			return 0;
		}
		else if(ret != BML_SUCCESS)
    		{
       			printf("tbml_init Error\n");
        		return -1;
    		}
		printf("tbml_init success\n");

		bml_initialized[volume] = 1;
      	}
		ret = tbml_open(volume);
    		if (ret != BML_SUCCESS)
    		{
        		printf("tbml_open Error\r\n");
        		return -1;
    		}
		printf("tbml_open success\n");
	
		if (tbml_get_vol_spec(volume, &vol_spec[volume]) != BML_SUCCESS)
    		{
       			printf("[NW:ERR]  tbml_get_vol_spec Fail!\r\n");
        		return -1;
    		}
		device_type->pgs_per_blk = vol_spec[volume].pgs_per_blk;
		device_type->scts_per_pg = vol_spec[volume].scts_per_pg;
		device_type->num_of_useblk = vol_spec[volume].num_of_useblk;
//	}

	return 1;
}
/* sh.kim - end */

int bml_close(int volume)
{
	int ret;

	ret = tbml_close(volume);
	if (ret != BML_SUCCESS)	{
		printf("tbml_close Error code = 0x%x \n", ret);
		return ret;
	}

	return 0;
}

/*
 * This function read block at buf point 
 */
int bml_read_block(int volume, unsigned int block, unsigned char *buf, unsigned char *ps_buf, unsigned int nFlag)
{
        int ret;
 	unsigned int i, first;
	/*
	 * SPB: sectors per block
	 * SPP: sectors per page
	 */
        first = block * SPB(volume);	/* start sector */

	/* ij.jang */
#if 0
	for (i = 0; i < SPB(volume); i += SPP(volume)) {
                ret = tbml_read(volume, first + i , SPP(volume),
                                buf, NULL, BML_FLAG_ECC_ON);
                if (ret != BML_SUCCESS) {
                        printf("tbml_read error: 0x%08x\n", ret);
                        return -1;
                }
		buf += (SPP(volume) << SECTOR_BITS);
        }
#else


/* sh.kim - start */
	if(nFlag == ECC_ON)
	{
	ret = tbml_mread (volume, first, SPB(volume),
				buf, ps_buf, BML_FLAG_ECC_ON);
	}
	else
	{
		ret = tbml_mread (volume, first, SPB(volume),
				buf, ps_buf, BML_FLAG_ECC_OFF);
	}
/* sh.kim - end */
	
	if (ret != BML_SUCCESS) {
		printf ("tbml_mread error: 0x%08X\n", ret);
		return -1;
	}
#endif
	return 0;
}

/**
 * xsr_get_part_spec - get a partition instance
 */

part_info *xsr_get_part_spec(unsigned int volume)
{
        return &part_spec[volume];
}

/**
 * xsr_get_vol_spec - get a volume instance
 */
tbml_vol_spec *xsr_get_vol_spec(unsigned int volume)
{
        return &vol_spec[volume];
}


int bml_load_partition(int volume, struct partition_info *pinfo)
{
        int error;
        unsigned int partno;
        part_info *pi;

        pi = xsr_get_part_spec(volume);

        /*could't find vaild part table*/
        if (!pi->num_of_partentry) {
                error = xsr_update_vol_spec(volume);
                if (error) /*never action, because of low-formatting*/
                        printf("error(%x)", error);
        }

        printf("pi->nNumOfPartEntry: %d\n", pi->num_of_partentry);
        pinfo->no_part_entries =  pi->num_of_partentry;
        for (partno = 0; partno < pi->num_of_partentry; partno++) {
                pinfo->entry[partno].no_blks  = pi->pentry[partno].num_of_blks;
                pinfo->entry[partno].id       = pi->pentry[partno].id;
                pinfo->entry[partno].attr    = pi->pentry[partno].attr;
                pinfo->entry[partno].first_blk = pi->pentry[partno].n1st_vbn;
        }
        return 0;
}

/**
 * xsr_update_vol_spec - update volume & partition instance from the device
 */
int xsr_update_vol_spec(unsigned int volume)
{
        int error, len;
        tbml_vol_spec *vs;
        part_info *pi;

        vs = xsr_get_vol_spec(volume);
        pi = xsr_get_part_spec(volume);

        memset(vs, 0x00, sizeof(tbml_vol_spec));
        memset(pi, 0x00, sizeof(part_info));

        error = tbml_get_vol_spec(volume, vs);
        if (error)
                return -1;

        error = tbml_ioctl(volume, BML_IOCTL_GET_FULL_PI,
                                NULL, 0, (unsigned char *)pi,(sizeof(part_info)), &len);
        if (error)
                return -1;

        return 0;
}


