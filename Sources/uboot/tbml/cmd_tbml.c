 /*
 *---------------------------------------------------------------------------*
 *                                                                           *
 *          COPYRIGHT 2003-2007 SAMSUNG ELECTRONICS CO., LTD.                *
 *                          ALL RIGHTS RESERVED                              *
 *                                                                           *
 *   Permission is hereby granted to licensees of Samsung Electronics        *
 *   Co., Ltd. products to use or abstract this computer program only in     *
 *   accordance with the terms of the NAND FLASH MEMORY SOFTWARE LICENSE     *
 *   AGREEMENT for the sole purpose of implementing a product based on       *
 *   Samsung Electronics Co., Ltd. products. No other rights to reproduce,   *
 *   use, or disseminate this computer program, whether in part or in        *
 *   whole, are granted.                                                     *
 *                                                                           *
 *   Samsung Electronics Co., Ltd. makes no representation or warranties     *
 *   with respect to the performance of this computer program, and           *
 *   specifically disclaims any responsibility for any damages,              *
 *   special or consequential, connected with the use of this program.       *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * Technical support:
 *   Samsung Flash software Group
 *   daeho kim <daehosky.kim@samsung.com>
 */

#include <common.h>
#include <command.h>

/* ij.jang */
#if 0
#include "./../../tbml/include/tbml_common.h"
#include "./../../tbml/include/tbml_interface.h"
#include "./../../tbml/include/tbml_type.h"
#include "../include/asm-arm/arch-arm1136/mux.h"	/* for Get time */
#include "./../../tbml/include/onenand_lld.h"
#include "../tbml/u-boot_interface.h"
#include "./../../tbml/TinyBML/onenand_reg.h"
#else
#include "../tbml/include/tbml_common.h"
#include "../tbml/include/tbml_interface.h"
#include "../tbml/include/tbml_type.h"
#include "../tbml/include/onenand_lld.h"
#include "../tbml/u-boot_interface.h"
#include "../tbml/TinyBML/onenand_reg.h"
#endif

/* dj.kim */
#define TOTAL_BLOCKS		(512)
#define SECTORS_PER_PAGE	(4)
#define PAGES_PER_BLOCK		(64)
#define SECTORS_PER_BLOCK	(SECTORS_PER_PAGE * PAGES_PER_BLOCK)

#define DEFAULT_DUMP_ADDR	0x23D00000
#define SZ_MAIN_AREA			(2048)
#define SZ_SPARE_AREA		(64)
#define SZ_DUMP_MEMORY		(TOTAL_BLOCKS*PAGES_PER_BLOCK*(SZ_MAIN_AREA+SZ_SPARE_AREA))

#define MAIN_BLOCK_SIZE	(SZ_MAIN_AREA * PAGES_PER_BLOCK)

static struct BML_Info	stVol_Info[XSR_MAX_DEVICES];
static int				nVol=0;
int 					open_flag[XSR_MAX_VOL] = {-1, -1};

/* sh.kim - start */
struct partition_info 	partitions[XSR_MAX_VOL];
static u32 			in_memory_partition[XSR_MAX_VOL] = {0, 0};
char					usb_folder[] = "/temp/";
extern 				onenand_mtd_sub;
extern char			console_buffer[CFG_CBSIZE];
/* sh.kim - end */

/* GBBM2 command function */
#define SPB(nVol)	(stVol_Info[nVol].pgs_per_blk * stVol_Info[nVol].scts_per_pg)	

/* ij.jang : add prototypes */
/* sh.kim - start */
static int 	load_partition(int vol);
void 		bbm_device_info(int vol);
int 		find_parition(int vol, unsigned int id);
/* sh.kim - end */


int do_bbm(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	switch (argc) {
	case 2:
		if (strcmp(argv[1], "update") == 0)
		{
			bbm_update_image();

			/* sh.kim -start */
			bbm_run(nVol, CFG_BBM_UPDATE);
			/* sh.kim - end */
			
			serial_setbrg();
			return 0;
		}
		/* sh.kim - start */	
		if (strcmp(argv[1], "copy") == 0)
		{
			return bbm_copy();
		}

		if (strcmp(argv[1], "usb") == 0)
		{
			return bbm_usb();
		}
		/* sh.kim - end */	

		break;

	case 3:
		/* sh.kim - start */
		if (strcmp(argv[1], "open") == 0 &&
			strcmp(argv[2], "cs0") == 0)
			return bbm_open_device(0);
		if (strcmp(argv[1], "open") == 0 &&
			strcmp(argv[2], "cs1") == 0)
			return bbm_open_device(1);
		if (strcmp(argv[1], "close") == 0 &&
			strcmp(argv[2], "cs0") == 0)
			return bbm_close_device(0);
		if (strcmp(argv[1], "close") == 0 &&
			strcmp(argv[2], "cs1") == 0)
			return bbm_close_device(1);
		/* sh.kim - end */

		if (strcmp(argv[1], "show") == 0 &&
		    strcmp(argv[2], "partition") == 0) 
		{
			return (bbm_show_partition(0) || bbm_show_partition(1));
		}
	
		/* dj.kim */
		if (strcmp(argv[1], "dump") == 0 &&
		    strcmp(argv[2], "cs0") == 0)
		{
			return bbm_dump_all(0, (unsigned char *)DEFAULT_DUMP_ADDR);
		}
		if (strcmp(argv[1], "dump") == 0 &&
		    strcmp(argv[2], "cs1") == 0)
		{
			return bbm_dump_all(1, (unsigned char *)DEFAULT_DUMP_ADDR);
		}

		if (strcmp(argv[1], "load") == 0 &&
		    strcmp(argv[2], "kernel") == 0)
		{
			return bbm_load_kernel(nVol, KERNEL_BUFFER_START);
		}

		/* sh.kim - start */
		if (strcmp(argv[1], "load") == 0 &&
		    strcmp(argv[2], "fkernel") == 0)
		{
			return bbm_load_kernel(nVol, FKERNEL_IMAGE_LOAD_ADDR);
		}
		/* sh.kim - end */

		/* dj.kim */
		if (strcmp(argv[1], "load") == 0 &&
		    strcmp(argv[2], "all") == 0)
		{
			return bbm_load_all(nVol);
		}

		/* dj.kim */
		if (strcmp(argv[1], "load") == 0 &&
		    strcmp(argv[2], "onw") == 0)
		{
			return bbm_load_onw(nVol);
		}

		/* dj.kim */
		if(strcmp(argv[1], "save") == 0 &&
			strcmp(argv[2], "all") == 0)
		{
			bbm_run(nVol, CFG_ALL_PARTITION);		//1/* 1 : all image update */
			serial_setbrg();
			return 0;
		}
		break;
	/* ij.jang : support load kernel onto the another address */
	case 4:
		/* dj.kim */
		if (strcmp(argv[1], "dump") == 0 &&
		    strcmp(argv[2], "cs0") == 0)
		{
			unsigned char *addr = simple_strtoul(argv[3], NULL, 16);
			return bbm_dump_all(0, (unsigned char *)addr);
		}
		if (strcmp(argv[1], "dump") == 0 &&
		    strcmp(argv[2], "cs1") == 0)
		{
			unsigned char *addr = simple_strtoul(argv[3], NULL, 16);
			return bbm_dump_all(1, (unsigned char *)addr);
		}

		/* dj.kim */
		if(strcmp(argv[1], "load") == 0 &&
			strcmp(argv[2], "partition") == 0)
		{
			unsigned int id = simple_strtoul(argv[3], NULL, 16);
			return bbm_load_partition(nVol, id);
		}

		if (strcmp(argv[1], "load") == 0 &&
		    strcmp(argv[2], "kernel") == 0) {
			unsigned int *addr = simple_strtoul(argv[3], NULL, 16);
			return bbm_load_kernel(nVol, addr);
		}
		break;

	default:
		break;
	}

	printf("Usage:\n%s\n", cmdtp->usage);
	return 1;
}

	/* ij.jang : to correct address */
#if 0
U_BOOT_CMD(
	bbm, 6, 1, do_bbm,
	"bbm   - BBM sub-system\n",
	"bbm open - open device\n"
	"bbm load kernel - load kernel image\n"
	"	 	- load kernel 0x81000000 from kernel partition\n"
	"bbm update - load update util image\n"
	"	 	- load kernel 0x80008000 from update partition\n"
	"bbm show partition - show partition information\n"
);
#else

#define XMK_STR(x)	#x
#define MK_STR(x)	XMK_STR(x)

U_BOOT_CMD(
	bbm, 6, 1, do_bbm,
	"bbm - BBM sub-system\n",
	"bbm open \t\t- open device\n"\

	/* sh.kim - start */	
	"bbm copy \t\t- (CS0 <-> CS1) onenand partition copy\n"\	
	"bbm usb \t\t- (USb -> CS0) onenand partition copy\n"\
	"bbm load all \t\t- (CS0 -> DRAM) load all images\n"\
	"bbm save all \t\t- (DRAM -> CS0) save all images\n"\
	"bbm update \t\t- (DRAM -> CS0) load update util image to [" MK_STR(CFG_ONENAND_ONWADDR) "]\n" \
	"bbm load onw \t\t- (DRAM -> CS0/CS1) load update util image\n"\
	"bbm show partition \t- (CS0 / CS1) show partition information\n"
	"bbm load partition [id]\t- (CS0 -> DRAM) load [id] image\n" \
	"bbm load kernel [addr] \t- (CS0 -> DRAM) load kernel image to [addr(hex), default=" MK_STR(CFG_ONENAND_BUFADDR) "]\n" \
	"bbm load fkernel [addr]\t- (CS0 -> DRAM) load fkernel image to [addr(hex), default=" MK_STR(FKERNEL_IMAGE_LOAD_ADDR) "]\n" \
	"bbm dump cs0 [addr] \t- (CS0 -> DRAM) dump all images to [addr(hex), default=" MK_STR(DEFAULT_DUMP_ADDR) "]\n" \
	"bbm dump cs1 [addr] \t- (CS1 -> DRAM) dump all images to [addr(hex), default=" MK_STR(DEFAULT_DUMP_ADDR) "]\n" \
	/* sh.kim - end */	
);
#endif

#if 1	/* kdj 0806 */
/* sh.kim - start */
void bbm_run(int vol, int type)
{
	int ret=0;
	unsigned int addr = CFG_ONENAND_ONWADDR;

        if (open_flag[vol] == -1) {
        	printf("CS%d device not opened\n", vol);
/*
                return;
*/
        }

	printf("prev load image address: 0x%x\r\n",addr);
	ret = load_file(addr, vol, type);
	printf("after load image address: 0x%x\r\n", addr);
	printf("return value: 0x%x\r\n", ret);
}
/* sh.kim - end */
#endif


int bbm_open_device(int vol)
{
	int nret;

	/* sh.kim - start */
        if (open_flag[vol] == 1) {
        	printf("CS%d device already opened\n", vol);
                return 0;
        }

	if(vol == 1 && onenand_mtd_sub == 0)
	{
		printf("TinyBML CS%d open error\n", vol);
		return -1;
	}
	/* sh.kim - end */

	/* ij.jang : move onenand_sync_set() call before open */
	//onenand_sync_unset(vol);

	nret =	bml_open(vol, &stVol_Info[vol]);
	if( nret == -1 ) {
		printf("TinyBML CS%d open error\n", vol);
		printf("error return value %x\n", nret);
		return -1;
	}
	bbm_device_info(vol);

#if 0
	onenand_sync_set();
#endif
	open_flag[vol] = 1;

	/* sh.kim - start */
	#if 0
	bbm_device_info();
	#endif
	/* sh.kim - end */
	
	printf("TinyBML CS%d open success\n", vol);
	
	DPRINT("after open=%x\n", *(volatile unsigned short *)(0x1e442));
	return 0;	
}

/* ij.jang : custom description support */
#if defined(CONFIG_BHLCD) || defined(CONFIG_URANUS) || \
	defined(CONFIG_CRUX) || defined(CONFIG_BHPLCD) || \
	defined(CONFIG_POLARIS)
#define SUPPORT_CUSTOM_DESC
#endif

#if defined(SUPPORT_CUSTOM_DESC)

#include "../tbml/dtv_bml.h"

/* these header files is existed in ONW with same content */
#if defined(CONFIG_BHLCD)
#include "../tbml/dtv_bml_bhlcd.h"
#elif defined(CONFIG_URANUS)
#include "../tbml/dtv_bml_uranus.h"
#elif defined(CONFIG_CRUX)
#include "../tbml/dtv_bml_crux.h"
#elif defined(CONFIG_BHPLCD)
#include "../tbml/dtv_bml_bhplcd.h"
#elif defined(CONFIG_POLARIS)
#include "../tbml/dtv_bml_polaris.h"
#endif

/* array should have one item(PARTITON_ID_END) at least. */
static struct xsr_parts dtv_def_parts[] = {
	DTV_DEF_PARTS
};


/* sh.kim - start */
#if 0
static void print_part_info(const unsigned int id)
{
	struct xsr_parts *part = &dtv_def_parts[0];
	int mem_limit = 16;

	while ( (part->id != PARTITION_ID_END) && mem_limit-- > 0) {
		if (part->id == id)
			break;
		part++;
	}

	if (part->id != PARTITION_ID_END) {
		printf(" id        : %s", part->desc);
		printf(" (0x%X)\n", id);
		printf(" load addr : 0x%08X\n", part->buf_addr);
	} else {
		printf(" id : User Defined");
		printf(" (0x%X)\n", id);
		printf(" load addr : unknown\n");
	}
}
#else
static void print_part_info(const unsigned int id)
{
	struct xsr_parts *part = &dtv_def_parts[0];
	int mem_limit = MAX_PARTITION_NUM;

	while ( (part->id != PARTITION_ID_END) && mem_limit-- > 0) {
		if (part->id == id)
			break;
		part++;
	}

	if (part->id != PARTITION_ID_END) {
		printf("%-20s 0x%08x\t (0x%08x)\t ", part->desc, part->buf_addr, id);
	} else {
		printf("User Defined\t\t unknown\t (0x%08x)\t ", id);
	}
}
#endif
/* sh.kim - end */


#endif


/* sh.kim - start */
int bbm_show_partition(int vol)
{
        unsigned int i;
        struct partition_info * pinfo;
	int ret;
	
        printf("\n");
        printf("<< CS%d PARTITION INFORMATION >>\r\n", vol);
        printf("====================================================================================== \r\n");

	if (open_flag[vol] == -1) {
        	printf("CS%d device not opened\n", vol);
                return -1;
        }
        
	ret = load_partition(vol);
	if(ret < 0){
		printf("load partition error\n");
		return -1;
	}

        pinfo = &partitions[vol];

        printf("     description \t  load addr \t     id    \t  attr \t    first_blk  no_blks \r\n");
/*
        printf("\t\t    id \t\t\t load addr \t attr \t    first_blk  no_blks \r\n");
*/        
        printf("====================================================================================== \r\n");

        for (i = 0; i < pinfo->no_part_entries ; i++) {
	        printf("%2d : ", i);
			
		#if defined(SUPPORT_CUSTOM_DESC)
		print_part_info(pinfo->entry[i].id);
		#else
                switch(pinfo->entry[i].id & 0xffffffff) {
	                case PARTITION_ID_NBL2:
	                        printf(" id        : Boot Loader");
	                        break;

	                case PARTITION_ID_NBL3:
	                        printf(" id        : u-boot");
	                        break;

	                case (PARTITION_USER_DEF_BASE | PARTITION_ID_NBL3):
	                        printf(" id        : u-boot parmeter");
	                        break;

	                case (PARTITION_USER_DEF_BASE):
	                        printf(" id        : update partition");
	                        break;

	                case PARTITION_ID_COPIEDOS:
	                        printf(" id        : KERNEL");
	                        break;

	                case PARTITION_ID_FILESYSTEM:
	                        printf(" id        : CRAMFS");
	                        break;

	                case PARTITION_ID_FILESYSTEM1:
	                        printf(" id        : RFS");
	                        break;

	                default:
	                        printf(" id        : User Defined");
	                        break;
                }
		                printf(" (0x%x)\r\n", pinfo->entry[i].id);
		#endif	/* SUPPORT_CUSTOM_DESC */

                switch (pinfo->entry[i].attr) {
                case (BML_PI_ATTR_FROZEN | BML_PI_ATTR_RO):
                        printf("RO + FROZEN (0x%x)\t",
                                pinfo->entry[i].attr);
                        break;

                case BML_PI_ATTR_RO:
                        printf("RO (0x%x)\t",
                                pinfo->entry[i].attr);
                        break;

                case BML_PI_ATTR_RW:
                        printf("RW (0x%x)\t",
                                pinfo->entry[i].attr);
                        break;
                }

                printf("%d \t %d\r\n", pinfo->entry[i].first_blk, pinfo->entry[i].no_blks);
	        printf("--------------------------------------------------------------------------------------\r\n");
        }
        printf("====================================================================================== \r\n");

        return 0;
}

static int load_partition(int vol)
{
        if (open_flag[vol] == -1) {
        	printf("CS%d device not opened\n", vol);
                return -1;
        }

        if (in_memory_partition[vol]){
                return 1;
        }

        if ( bml_load_partition(vol, &partitions[vol]) < 0){
                printf("Error loading partition\n");
                return -1;
        }

        printf("Success loading partition\n");

        in_memory_partition[vol] = 1;

        return 0;
}
	

/* sh.kim - start */
int find_partition(int vol, unsigned int id)
{
	int nRet;
	unsigned int i;

        if (open_flag[vol] == -1) {
        	printf("CS%d device not opened\n", vol);
                return -1;
        }
	nRet =  load_partition(vol);
	
	if(nRet < 0)
	{
		printf("partition load error\n");
		return -1;
	}

	for (i = 0; i < partitions[vol].no_part_entries; i++)
	{
		if (partitions[vol].entry[i].id == id)
		{
			return i;
		}
	}

	if (i == partitions[vol].no_part_entries)
	{
		printf("Coudn't find partition id : 0x%x\n", id);
		return -1;
	}
	return -1;
}


int bbm_close_device(int vol)
{
	int ret;
	
	ret = bml_close(vol);
	if(ret)	{	
		printf("CS%d bbm close fail \n", vol);
		printf("error return value = %x\n", ret);
		return -1;
	}

	open_flag[vol] = -1;	
	printf("CS%d bbm close success\n", vol);

	return 0;
}

/*
 * load kernel from kernel partiton
 */
int bbm_load_kernel(int vol, const unsigned int *addr)
{
	/* ij.jang */
#if 0
	unsigned char   *buf = (unsigned char *) KERNEL_BUFFER_START;
#else
	unsigned char   *buf = (unsigned char *) addr;
#endif
	struct partition_entry * entry;
	int nNumOfScts;
	int nRet;
	unsigned int i;
	unsigned int block, end_block;

	i = find_partition(vol, PARTITION_ID_COPIEDOS);
	if(i == -1)
	{
		return -1;
	}

	entry = &partitions[vol].entry[i];
	DPRINT("entry->first_blk = %d, entry->no_blks= %d"
			,entry->first_blk, entry->no_blks);

	block = entry->first_blk;
	end_block = entry->no_blks + entry->first_blk;
	for (; block < end_block ; block++) {
		nRet = bml_read_block(vol, block, buf, NULL, ECC_ON);
		if(nRet < 0) {
			printf("bml read block error\n");
			return -1;
		}
                buf += (SPB(vol) << SECTOR_BITS);
		DPRINT("block: %dbuf:0x%08x\n", block, buf);
	}

	printf("read success from [kernel] partition\n");
	/* ij.jang : to correct address */
#if 0
	printf("Loaded kernel 0x81000000 from OneNAND kernel partition\n");
#else
	printf("Loaded [kernel] 0x%08X from [kernel] partition\n",
			(unsigned long)addr);
#endif

	DPRINT("\rTimer2:%x Timer3:%x\n", TIMER2_TCRR, TIMER3_TCRR);

	return 0;
}


/* dj.kim */
int bbm_load_partition(int vol, unsigned int id)
{
	unsigned char   *buf;
	struct partition_entry * entry;
	int nNumOfScts;
	int nRet;
	unsigned int part_num;
	unsigned int nFlag;
	unsigned int block, end_block;
	unsigned char* ps_buf;
	struct xsr_parts *part = &dtv_def_parts[0];

	part_num = find_partition(vol, id);
	if(part_num == -1)
	{
		return -1;
	}
	part += part_num;

	buf = (unsigned char *)part->buf_addr;
	
	entry = &partitions[vol].entry[part_num];
	DPRINT("entry->first_blk = %d, entry->no_blks= %d"
			,entry->first_blk, entry->no_blks);

	block = entry->first_blk;
	end_block = entry->no_blks + entry->first_blk;

	if(entry->id == PARTITION_ID_FILESYSTEM8 || entry->id == PARTITION_ID_FILESYSTEM9)
	{
		DPRINT("bml_read_block with ECC_OFF option\n");
/*
		nFlag = ECC_OFF;
		ps_buf = buf + entry->no_blks * MAIN_BLOCK_SIZE;
*/
		nFlag = ECC_ON;
		ps_buf = NULL;
	}
	else
	{
		DPRINT("bml_read_block with ECC_ON option\n");
		nFlag = ECC_ON;
		ps_buf = NULL;
	}

	for (; block < end_block ; block++) 
	{
		nRet = bml_read_block(vol, block, buf, ps_buf, nFlag);
		if(nRet < 0) {
			printf("bml read block error\n");
			/* skip read error
			return -1;
			*/
		}
                buf += (SPB(vol) << SECTOR_BITS);
		DPRINT("block: %dbuf:0x%08x\n", block, buf);
	}

	printf("Read success from [%s] Partition Id 0x%x\n", part->desc, entry->id);
	printf("Loaded 0x%08X from [%s] Partition Id 0x%x\n",
			(unsigned long)part->buf_addr, part->desc, id);
	DPRINT("\rTimer2:%x Timer3:%x\n", TIMER2_TCRR, TIMER3_TCRR);

	return 0;
}



/* dj.kim */
int bbm_load_all(int vol)
{
	unsigned char   *buf;
	struct partition_entry * entry;
	int nNumOfScts;
	int nRet;
	unsigned int i;
	unsigned int nFlag;
	unsigned int block, end_block;
	unsigned char* ps_buf;
	struct xsr_parts *part = &dtv_def_parts[0];

        if (open_flag[vol] == -1) {
        	printf("CS%d device not opened\n", vol);
                return -1;
        }
	
	nRet =  load_partition(vol);
	if(nRet < 0){
		printf("partition load partition error\n");
		return -1;
	}

	for(i = 0 ; i < partitions[vol].no_part_entries ; i++)
	{	
		buf = (unsigned char *)part->buf_addr;
		printf("[%s] Partition load address %#010x\n", part->desc, buf);
		
		entry = &partitions[vol].entry[i];
		DPRINT("entry->first_blk = %d, entry->no_blks= %d"
				,entry->first_blk, entry->no_blks);

		block = entry->first_blk;
		end_block = entry->no_blks + entry->first_blk;

		if(entry->id == PARTITION_ID_FILESYSTEM8 || entry->id == PARTITION_ID_FILESYSTEM9)
		{
			DPRINT("bml_read_block with ECC_OFF option\n");
/*
			nFlag = ECC_OFF;
			ps_buf = buf + entry->no_blks * MAIN_BLOCK_SIZE;
*/
			nFlag = ECC_ON;
			ps_buf = NULL;
		}
		else
		{
			DPRINT("bml_read_block with ECC_ON option\n");
			nFlag = ECC_ON;
			ps_buf = NULL;
		}

		for (; block < end_block ; block++) 
		{
			nRet = bml_read_block(vol, block, buf, ps_buf, nFlag);
			if(nRet < 0) 
			{
				printf("bml read block error\n");
				/* skip read error
				return -1;
				*/
			}
			buf += (SPB(vol) << SECTOR_BITS);
			DPRINT("block: %dbuf:0x%08x\n", block, buf);
		}

		printf("Read success from [%s] partition\n", part->desc);
		printf("Loaded 0x%08X from [%s] Partition\n\n",
				(unsigned long)part->buf_addr, part->desc);
		DPRINT("\rTimer2:%x Timer3:%x\n", TIMER2_TCRR, TIMER3_TCRR);
		part++;
	}

	bbm_update_image();
	return 0;
}


/* dj.kim */
int bbm_load_onw(int vol)
{
	unsigned char*		buf = (unsigned char *)CFG_ONENAND_ONWADDR;
	struct partition_entry*	entry;
	int 					nNumOfScts;
	int 					nRet;
	unsigned int 			part_num;
	unsigned int 			block, end_block;
	int 					nArgs = 2;
	char					select;
	struct xsr_parts*		part = &dtv_def_parts[0];

	part_num = find_partition(vol, PARTITION_USER_DEF_BASE);
	if(part_num == -1)
	{
		return -1;
	}
	part += part_num;
		
	printf("[%s] Partition load address %#010x\n", part->desc, buf);
	
	entry = &partitions[vol].entry[part_num];
	DPRINT("entry->first_blk = %d, entry->no_blks= %d"
			,entry->first_blk, entry->no_blks);

	block = entry->first_blk;
	end_block = entry->no_blks + entry->first_blk;
	for (; block < end_block ; block++) {
		nRet = bml_read_block(vol, block, buf, NULL, ECC_ON);
		if(nRet < 0) {
			printf("bml read block error\n");
			return -1;
		}
                buf += (SPB(nVol) << SECTOR_BITS);
		DPRINT("block: %dbuf:0x%08x\n", block, buf);
	}

	printf("Read success from [%s] Partition Id 0x%x\n", part->desc, entry->id);
	printf("Loaded 0x%08X from [%s] Partition Id 0x%x\n\n",
			CFG_ONENAND_ONWADDR, part->desc, entry->id);
	DPRINT("\rTimer2:%x Timer3:%x\n", TIMER2_TCRR, TIMER3_TCRR);

	while(1)
	{
		printf("[BBM:	] 1 : CS0\n");
		printf("[BBM:	] 2 : CS1\n");
		printf("[BBM:	] x : Exit\n");
		printf("---------------------------------------------------\n");

		readline("[BBM:	] Choose a number : ");
		printf("\n");
		
		if(console_buffer[1] != NULL)
		{
			printf("[BBM:	] Invalid number !!\n\n");
			continue;
		}
		select = console_buffer[0];

		if(select == 'x')
		{
			break;
		}
		else if(select == '1')
		{
			((ulong (*)(int, unsigned int, unsigned int))(CFG_ONENAND_ONWADDR)) (nArgs, 0, CFG_BBM_UPDATE);
			break;
		}
		else if(select == '2')
		{
			((ulong (*)(int, unsigned int, unsigned int))(CFG_ONENAND_ONWADDR)) (nArgs, 1, CFG_BBM_UPDATE);
			break;
		}
		else
		{
			printf("[BBM:	] Invalid number !!\n\n");
		}
	}

	return 0;
}


/* dj.kim */
int bbm_dump_all(int vol, unsigned char *addr)
{
	int 	ret = 0;
	int 	iPsn = 0;
	int 	i = 0;
	int 	iblock = 0;
	int 	nDev = 0;
	
	unsigned char * pMain;
	unsigned char * pSpare;

	if(vol == 1)
	{
		nDev = 4;
	}

	pMain = (unsigned char *)addr;
	memset(pMain, 0xff, SZ_DUMP_MEMORY);
	
	pSpare = pMain + SZ_MAIN_AREA;

	ret = ONLD_init((void *)inter_getparam());
	printf ( "ONLD_Init : %d \n", ret ) ;
	
	ret = ONLD_open(nDev) ;
	if(ret < 0)
	{
		printf ( "ONLD_Open error: %d \n", ret ) ;
		return -1;
	}

	printf ( "ONLD_Open : %d \n", ret ) ;

	for (iblock=0; iblock< TOTAL_BLOCKS; iblock++)
	{
		iPsn = iblock * SECTORS_PER_BLOCK ; 
		
		for (i=0 ; i < SECTORS_PER_BLOCK; i+=SECTORS_PER_PAGE)
		{
		    ret = ONLD_read(nDev,									/* Device Number                 */
		                     iPsn + i ,                 		/* Physical Sector Number        */
		                     (unsigned int)SECTORS_PER_PAGE,    /* Number of Sectors to be read  */
		                     pMain,								/* Buffer pointer for Main area  */
		                     pSpare,							/* Buffer pointer for Spare area */ 
		                     (unsigned int)2);					/* ONLD_FLAG_ECC_ON */

			pMain += (SZ_MAIN_AREA + SZ_SPARE_AREA);
			pSpare = pMain + SZ_MAIN_AREA;
		}
		printf ( "ONLD_Read  : Block %d, ret=%d \n",iblock, ret ) ;
	}

	printf("DUMP END.\n");
	printf("PLEASE SAVE BINARY ADDRESS 0x%X--0x%X\n", addr, (int)addr + (int)SZ_DUMP_MEMORY);
	return 0;
}


void bbm_update_image()
{
	unsigned char   *buf = (unsigned char *) UPDATE_IMAGE_START;
	struct partition_entry * entry;
	int nNumOfScts;
	int nRet;
	unsigned int part_num;
	unsigned int block, end_block;
	int vol = 0;

	part_num = find_partition(vol, PARTITION_USER_DEF_BASE);
	if(part_num == -1)
	{
		return -1;
	}

	entry = &partitions[vol].entry[part_num];
	DPRINT("entry->first_blk = %d, entry->no_blks= %d"
			,entry->first_blk, entry->no_blks);

	block = entry->first_blk;
	end_block = entry->no_blks + entry->first_blk;
	
	/* sh.kim - start */
	for (; block < end_block ; block++) {
		nRet = bml_read_block(vol, block, buf, NULL, ECC_ON);
		if(nRet < 0) {
			printf("bml read block error\n");
			return -1;
		}
                buf += (SPB(nVol) << SECTOR_BITS);
		DPRINT("block: %dbuf:0x%08x\n", block, buf);
	}
	/* sh.kim - end */

	printf("Loading success from [update util] partition\n");
	printf("Loaded image  0x%x from [update util] partition\n", UPDATE_IMAGE_START);
	DPRINT("\rTimer2:%x Timer3:%x\n", TIMER2_TCRR, TIMER3_TCRR);

	return 0;

}

/*
 * Display device information
 */
/* sh.kim - start */
void bbm_device_info(int vol)
{
	printf("**************** device info  *******************\n");
	printf("nPgsPerBlk = %d\n", stVol_Info[vol].pgs_per_blk);
	printf("nSctsPerPg = %d\n", stVol_Info[vol].scts_per_pg);
	printf("nNumOfUsBlks = %d\n", stVol_Info[vol].num_of_useblk);
	printf("*************************************************\n");
}
/* sh.kim - end */



/* ij.jang 02.Aug.2007 : function move to <board.c> */
#if 0
void onenand_sync_set()
{
	int nBAddr = 0;

    /* IO Buffer Enable for NAND interrupt enable/disable and ECC ON */
	ONLD_REG_SYS_CONF1(nBAddr) &= 0xFF;

#if defined(SYNC_16BURST_83M)
	*(volatile unsigned short *)(0x1E442) |= (IOBE_ENABLE | BST_RD_LATENCY_6 | BST_LENGTH_CONT | 0x0004 | 0x0010);
#else
	*(volatile unsigned short *)(0x1E442) |= (IOBE_ENABLE | BST_RD_LATENCY_4 | BST_LENGTH_CONT );
#endif

#if !defined(ASYNC_READ)
	*(volatile unsigned short *)(0x1E442) |= (SYNC_READ_MODE);
#endif

}
#endif


/* sh.kim - start */	
int bbm_usb(void)
{
	char 				select;
	int	 			part_num;
	int				nRet;
	int 				i;
	struct xsr_parts*	part;
	char				filename[CFG_CBSIZE]; 
	int				vol = 0;
	int				copy_flag[MAX_PARTITION_NUM];
/*
	int 				env_part_num;
 */
 
	nRet =  load_partition(vol);
	if(nRet < 0)
	{
		printf("load partition error\n");
		return -1;
	}

	/* USB initialize */
	usb_stop();
	printf("(Re)start USB...\n");
	i = usb_init();
	#ifdef CONFIG_USB_STORAGE
	/* try to recognize storage devices immediately */
	if (i >= 0)
	{
		nRet = usb_stor_scan(1);
		if(nRet < 0)
		{
			printf("\n** Please connect USB storage device first **\n\n");
			return -1;
		}
	}
	#endif

	while(1)
	{
		printf("\n");
		
		part = &dtv_def_parts[0];
		for (i = 0; i < partitions[vol].no_part_entries; i++)
		{		
			printf("[BBM:	] %2d : %-20s(0x%x)\t \"%s\"\n", i, part->desc, part->buf_addr, part->file_name);
			part++;
		}
		printf("---------------------------------------------------------------\n");
		printf("[BBM:	] a : Copy all partitions\n");
		printf("---------------------------------------------------------------\n");
		printf("[BBM:	] f : list \"/temp\" directory\n");
		printf("---------------------------------------------------------------\n");
		printf("[BBM:	] x : Exit\n");
		printf("---------------------------------------------------------------\n");

		readline("[BBM:	] Choose a number : ");
		printf("\n");

		if(strlen(console_buffer) > 2)
		{
			printf("[BBM:	] Invalid number !!\n\n");
			continue;
		}
		else if(console_buffer[1] != NULL)
		{
			select = ((console_buffer[0] - '0') * 10) + console_buffer[1]; 
		}
		else
		{
			select = console_buffer[0];
		}

		if(select == 'x')
		{
			break;
		}
		else if(select == 'a')
		{
			char temp[CFG_CBSIZE];

			readline("[BBM:	] filename(enter to default [/temp/]) : ");
			printf("\n");

			part = &dtv_def_parts[0];
			for (i = 0; i < partitions[vol].no_part_entries; i++)
			{
				copy_flag[i] = 1;
				
				if(console_buffer[0] == NULL)
				{
					strcpy(temp, usb_folder);
				}
				else
				{
					strcpy(temp, console_buffer);
				}

/*
				if(part->id == PARTITION_USER_DEF_BASE + 1)
				{
					env_part_num = i;
					bbm_load_env(part->buf_addr);  // backup uboot env partition
					part++;
					continue;
				}
*/				
				strcat(temp, part->file_name);

				memset(part->buf_addr, 0xff, part->blocks*128*1024);
				
				if(bhp_fatload(temp, part->buf_addr) != 0)  // check images in usb
				{
					copy_flag[i] = 0;
					if(part->id == PARTITION_ID_NBL2
						|| part->id == PARTITION_ID_NBL3 
						||part->id == PARTITION_USER_DEF_BASE)
					{
						return -1;  // error if onboot, uboot, onw images are not exist
					}
				}
				part++;
			}

			bbm_update_image();

			bbm_run(vol, CFG_ONENAND_INIT_FORMAT);  // init format before update whole images
			for(i = 0; i < partitions[vol].no_part_entries; i++)  // update partition which was loaded from usb
			{
				if(copy_flag[i] == 1) 
				{
					bbm_run(vol, i);
				}
			}
/*
			bbm_run(vol, env_part_num);  // return uboot env partition
*/
			serial_setbrg();
			
		}
		else if(select == 'f')
		{
			char temp[CFG_CBSIZE];

			readline("[BBM:	] filename(enter to default [/temp/]) : ");
			printf("\n");

			if(console_buffer[0] == NULL)
			{
				strcpy(temp, usb_folder);
			}
			else
			{
				strcpy(temp, console_buffer);
			}

			printf("---------------------------------------------------------------\n");
			bhp_fatls(temp);
			printf("---------------------------------------------------------------\n");
		}
		else if((select >= '0') && (select < ('0' + partitions[nVol].no_part_entries)))
		{
			part_num = select - '0';

			part = &dtv_def_parts[0];
			part += part_num;
			
			readline("[BBM:	] filename : ");
			printf("\n");

			strcpy(filename, console_buffer);

			memset(part->buf_addr, 0xff, part->blocks*128*1024);

			if(bhp_fatload(filename, part->buf_addr) == 0)
			{
				bbm_update_image();
				bbm_run(vol, part_num);
				serial_setbrg();
			}
		}
		else
		{
			printf("[BBM:	] Invalid number !!\n\n");
		}
	}
	return 0;
}
/* sh.kim - end */


/* sh.kim - start */	
int bbm_copy(void)
{
	char 				select_main, select_sub;
	int 				vol = 0;
	int	 			part_num;
	int				nRet;
	int 				i;
	struct xsr_parts*	part;
 	
	while(1)
	{
		printf("[BBM:	] 1 : CS0 -> CS1\n");
		printf("[BBM:	] 2 : CS1 -> CS0\n");
		printf("[BBM:	] x : Exit\n");
		printf("---------------------------------------------------\n");

		readline("[BBM:	] Choose a number : ");
		printf("\n");
		
		if(console_buffer[1] != NULL)
		{
			printf("[BBM:	] Invalid number !!\n\n");
			continue;
		}
		select_main = console_buffer[0];

		if(select_main == 'x')
		{
			break;
		}
		else if((select_main == '1') ||(select_main == '2'))
		{
			if(select_main == '1')
			{
 				vol = 0;  // CS0 -> CS1
			}
			else if(select_main == '2')
			{
 				vol = 1;  // CS1 -> CS0
			}
			if(onenand_mtd_sub == 0) 
			{
				printf("[BBM:	] Error : CS1 is not available\n\n");
				continue;
			}
			
		        if (open_flag[vol] == -1) 
			{
		        	printf("CS%d device not opened\n", vol);
		                return -1;
		        }
			
			nRet =  load_partition(vol);
			if(nRet < 0)
			{
				printf("partition load error\n");
				return -1;
			}

			while(1)
			{
				printf("\n");
	
				part = &dtv_def_parts[0];
				for (i = 0; i < partitions[vol].no_part_entries; i++)
				{		
					printf("[BBM:	] %2d : %-20s\t(0x%x)\n", i, part->desc, part->buf_addr);
					part++;
				}
				printf("---------------------------------------------------\n");
				printf("[BBM:	] a : Copy all partitions\n");
				printf("---------------------------------------------------\n");
				printf("[BBM:	] x : Go back\n");
				printf("---------------------------------------------------\n");

				readline("[BBM:	] Choose a number : ");

				printf("\n");

				if(strlen(console_buffer) > 2)
				{
					printf("[BBM:	] Invalid number !!\n\n");
					continue;
				}
				else if(console_buffer[1] != NULL)
				{
					select_sub = ((console_buffer[0] - '0') * 10) + console_buffer[1];
				}
				else
				{
					select_sub = console_buffer[0];
				}

				if(select_sub == 'x')
				{
					break;
				}
				else if(select_sub == 'a')
				{
					bbm_copy_all(vol);
					serial_setbrg();
					continue;
				}
				else if((select_sub >= '0') && (select_sub < ('0' + partitions[vol].no_part_entries)))
				{
					part_num = select_sub - '0';
					bbm_copy_partition(vol, part_num);
					serial_setbrg();
				}
				else
				{
					printf("[BBM:	] Invalid number !!\n\n");
				}
			}
		}
		else
		{
			printf("[BBM:	] Invalid number !!\n\n");
		}
	}
	return 0;
}


int bbm_copy_partition(unsigned int vol,  int part_num)
{
	unsigned char*		buf;
	struct partition_entry* 	entry;
	int 					nRet;
	unsigned int 			block, end_block;
	struct xsr_parts*		part = &dtv_def_parts[part_num];
	int 					onwVol;
	unsigned int			nFlag;
	unsigned char*		ps_buf;
	
	if(vol == 0)
	{
		onwVol = 1;
	}
	else 
	{
		onwVol = 0;
	}

	buf = (unsigned char *)part->buf_addr;
	
	entry = &partitions[vol].entry[part_num];
	DPRINT("entry->first_blk = %d, entry->no_blks= %d"
			,entry->first_blk, entry->no_blks);
	block = entry->first_blk;
	end_block = entry->no_blks + entry->first_blk;

	if(entry->id == PARTITION_ID_FILESYSTEM8 || entry->id == PARTITION_ID_FILESYSTEM9)
	{
		DPRINT("bml_read_block with ECC_OFF option\n");
/*
		nFlag = ECC_OFF;
		ps_buf = buf + entry->no_blks * MAIN_BLOCK_SIZE;
*/
		nFlag = ECC_ON;
		ps_buf = NULL;
	}
	else
	{
		DPRINT("bml_read_block with ECC_ON option\n");
		nFlag = ECC_ON;
		ps_buf = NULL;
	}

	for (; block < end_block ; block++) 
	{
		nRet = bml_read_block(vol, block, buf, ps_buf, nFlag);
		if(nRet < 0) 
		{
			printf("bml read block error\n\n");
			/* skip read error
			return -1;
			*/
		}
                buf += (SPB(vol) << SECTOR_BITS);
		DPRINT("block: %dbuf:0x%08x\n", block, buf);
	}

	printf("Read success from volume %d : [%s] partition\n", 
			vol,
			part->desc);

	printf("Loaded 0x%08X from OneNAND volume %d : [%s] partition\n\n",
			(unsigned long)part->buf_addr,
			vol,
			part->desc);

	DPRINT("\rTimer2:%x Timer3:%x\n\n", TIMER2_TCRR, TIMER3_TCRR);
		
	bbm_update_image();
	bbm_run(onwVol, part_num);

	return 0;
}


int bbm_copy_all(unsigned int vol)
{
	unsigned char*		buf;
	struct partition_entry*	entry;
	int 					nRet;
	unsigned int 			i;
	unsigned int 			block, end_block;
	struct xsr_parts *		part = &dtv_def_parts[0];
	int 					onwVol;
	unsigned int 			nFlag;
	unsigned char*		ps_buf;

	if(vol == 0)
	{
		onwVol = 1;
	}
	else 
	{
		onwVol = 0;
	}

	for(i = 0 ; i < partitions[vol].no_part_entries ; i++)
	{	
		if(part->id == PARTITION_USER_DEF_BASE + 1)
		{
			part++;
			continue;
		}

		buf = (unsigned char *)part->buf_addr;
		entry = &partitions[vol].entry[i];
		DPRINT("entry->first_blk = %d, entry->no_blks= %d"
				,entry->first_blk, entry->no_blks);

		block = entry->first_blk;
		end_block = entry->no_blks + entry->first_blk;

		if(entry->id == PARTITION_ID_FILESYSTEM8 || entry->id == PARTITION_ID_FILESYSTEM9)
		{
			DPRINT("bml_read_block with ECC_OFF option\n");
/*
			nFlag = ECC_OFF;
			ps_buf = buf + entry->no_blks * MAIN_BLOCK_SIZE;
*/
			nFlag = ECC_ON;
			ps_buf = NULL;
		}
		else
		{
			DPRINT("bml_read_block with ECC_ON option\n");
			nFlag = ECC_ON;
			ps_buf = NULL;
		}

		for (; block < end_block ; block++) 
		{
			nRet = bml_read_block(vol, block, buf, ps_buf, nFlag);
			if(nRet < 0) 
			{
				printf("bml read block error\n\n");
				/* skip read error
				return -1;
				*/
			}
			buf += (SPB(vol) << SECTOR_BITS);
			DPRINT("block: %dbuf:0x%08x\n", block, buf);
		}

		printf("Read success from volume %d : [%s] partition\n", 
				vol,
				part->desc);

		printf("Loaded 0x%08X from OneNAND volume %d : [%s] partition\n\n",
				(unsigned long)part->buf_addr,
				vol,
				part->desc);

		DPRINT("\rTimer2:%x Timer3:%x\n\n", TIMER2_TCRR, TIMER3_TCRR);
		part++;
	}

	bbm_update_image();
	bbm_run(onwVol, CFG_ALL_PARTITION);

	return 0;
}
/* sh.kim - end */	


/* sh.kim - start */	
int bbm_load_env(u8*addr)
{
	struct partition_entry * 	entry;
	int 					nNumOfScts;
	int					nRet;
	unsigned int 			part_num;
	unsigned int 			block, end_block;
	u8*					buf = addr;
	int 					vol = 0;

	part_num = find_partition(vol, PARTITION_USER_DEF_BASE + 1);
	if(part_num == -1)
	{
		return -1;
	}

	entry = &partitions[vol].entry[part_num];
	DPRINT("entry->first_blk = %d, entry->no_blks= %d entry->id=0x%x\n"
			,entry->first_blk, entry->no_blks, entry->id);

	block = entry->first_blk;
	end_block = entry->no_blks + entry->first_blk;
	for (; block < end_block ; block++) {
		nRet = bml_read_block(vol, block, buf, NULL, ECC_ON);
		if(nRet < 0) {
			printf("bml read block error\n");
			return -1;
		}
                buf += (SPB(vol) << SECTOR_BITS);
		DPRINT("block: %d buf:0x%08x\n", block, buf);
	}

	return 0;
}
/* sh.kim - end */	


/* sh.kim - start */	
int bbm_save_env(u8* addr)
{
	struct xsr_parts*		part;
	int 					vol = 0;
	int 					part_num;

	part_num = find_partition(vol, PARTITION_USER_DEF_BASE + 1);
	part = &dtv_def_parts[part_num];

	memset(part->buf_addr, 0, CFG_ENV_SIZE);
	memcpy(part->buf_addr, addr, CFG_ENV_SIZE);
	
	bbm_update_image();
	bbm_run(vol, part_num);
	serial_setbrg();
	return 0;
}
/* sh.kim - end */	


/* 02,Aug,2007 ij.jang :
   onenand performance test code from S/W platform(V) */
#include <asm/hardware.h>

#if !defined(REG_GET)
#define REG_GET(addr)	(*(volatile unsigned*)addr)
#endif

static unsigned int stc1, stc2;
static void start_tsd_stc_val(void)
{
	stc1 = REG_GET(rTSD_STC_BASE);
}
static void end_tsd_stc_val(void)
{
	stc2 = REG_GET(rTSD_STC_BASE);
}
static int tbml_load_image_test(u32 id)
{
	int i;
	unsigned int cnt, first;
	struct partition_entry *entry;
	unsigned char* mbuf = (unsigned char *)KERNEL_BUFFER_START;

	/* sh.kim - start */
	if (load_partition(nVol) < 0) {
		printf ("partition load error\n");
		return -1;
	}
	/* sh.kim - end */

	for (i = 0; i < partitions[nVol].no_part_entries; i++){
		if (partitions[nVol].entry[i].id == PARTITION_ID_COPIEDOS)
			break;
	}

	if (i == partitions[nVol].no_part_entries){
		printf("Coudn't find kernel partition");
		return -1;
	}

	entry = &partitions[nVol].entry[i];

	first = entry->first_blk * SPB(0);
	cnt = entry->no_blks * SPB(0);
	
	/* ij.jang : tbml_mread is working fine only for 1 block.
	 	don't know why.. */
#if 0

	printf ("read %dth sector, %d sectors\n", first, scts);

	if (tbml_mread (0, first, scts, mbuf, NULL, BML_FLAG_ECC_ON)
			!= BML_SUCCES) {
		printf ("tbml_mread failed\n");
		return -1;
	}
#else
#if 0
	printf ("read from %dth block, %d blocks, (%dth sector, %d sectors)\n",
			entry->first_blk, entry->no_blks,
			first, cnt);
#endif
	for (i=0; i<entry->no_blks; i++) {
		if (tbml_mread(0, first, SPB(0), mbuf, NULL, BML_FLAG_ECC_ON)
				!= BML_SUCCESS) {
			printf ("tbml_mread error!\n");
			return -1;
		}
		first += SPB(0);
		mbuf += (SPB(0) << SECTOR_BITS);
	}
#endif

	/* not have to do this */
#if 0
	tbml_flush (0, BML_FLAG_ECC_ON);
#endif

	return 0;
}

/* load kernel */
static int do_ttbml(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	start_tsd_stc_val();
	tbml_load_image_test(PARTITION_ID_COPIEDOS);
	end_tsd_stc_val();
	printf ("45Khz, %d\n", stc2-stc1);
	return 0;
}

U_BOOT_CMD (
	test_tbml, 1, 1, do_ttbml,
	"test_tbml	- load kernel using BML\n",
	"test_tbml	- load kernel using BML\n"
);
