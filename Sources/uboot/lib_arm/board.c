/*
 * (C) Copyright 2002-2006
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * To match the U-Boot user interface on ARM platforms to the U-Boot
 * standard (as on PPC platforms), some messages with debug character
 * are removed from the default U-Boot build.
 *
 * Define DEBUG here if you want additional info as shown below
 * printed upon startup:
 *
 * U-Boot code: 00F00000 -> 00F3C774  BSS: -> 00FC3274
 * IRQ Stack: 00ebff7c
 * FIQ Stack: 00ebef7c
 */

#include <common.h>
#include <command.h>
#include <malloc.h>
#include <devices.h>
#include <version.h>
#include <net.h>

#ifdef CONFIG_DRIVER_SMC91111
#include "../drivers/smc91111.h"
#endif
#ifdef CONFIG_DRIVER_LAN91C96
#include "../drivers/lan91c96.h"
#endif

/* sh.kim - start */
#include "../tbml/u-boot_interface.h"
/* sh.kim - end */

//#define _FASTBOOT_

DECLARE_GLOBAL_DATA_PTR;

#if (CONFIG_COMMANDS & CFG_CMD_NAND)
void nand_init (void);
#endif

/* ij.jang : from RFS-1.2.2P-R2, TinyBootloader-1.0.0 */
#if (CONFIG_COMMANDS & CFG_CMD_ONENAND)
void onenand_init (void);
#endif

ulong monitor_flash_len;

#ifdef CONFIG_HAS_DATAFLASH
extern int  AT91F_DataflashInit(void);
extern void dataflash_print_info(void);
#endif

#ifndef CONFIG_IDENT_STRING
#define CONFIG_IDENT_STRING ""
#endif

const char version_string[] =
	U_BOOT_VERSION" (" __DATE__ " - " __TIME__ ")"CONFIG_IDENT_STRING;

#ifdef CONFIG_DRIVER_CS8900
extern void cs8900_get_enetaddr (uchar * addr);
#endif

#ifdef CONFIG_DRIVER_RTL8019
extern void rtl8019_get_enetaddr (uchar * addr);
#endif

/* sh.kim - start */
extern char	console_buffer[CFG_CBSIZE];
/* sh.kim - end */

/*
 * Begin and End of memory area for malloc(), and current "brk"
 */
static ulong mem_malloc_start = 0;
static ulong mem_malloc_end = 0;
static ulong mem_malloc_brk = 0;

static
void mem_malloc_init (ulong dest_addr)
{
	mem_malloc_start = dest_addr;
	mem_malloc_end = dest_addr + CFG_MALLOC_LEN;
	mem_malloc_brk = mem_malloc_start;

	memset ((void *) mem_malloc_start, 0,
			mem_malloc_end - mem_malloc_start);
}

void *sbrk (ptrdiff_t increment)
{
	ulong old = mem_malloc_brk;
	ulong new = old + increment;

	if ((new < mem_malloc_start) || (new > mem_malloc_end)) {
		return (NULL);
	}
	mem_malloc_brk = new;

	return ((void *) old);
}

/************************************************************************
 * Init Utilities							*
 ************************************************************************
 * Some of this code should be moved into the core functions,
 * or dropped completely,
 * but let's get it working (again) first...
 */

static int init_baudrate (void)
{
	char tmp[64];	/* long enough for environment variables */
	int i = getenv_r ("baudrate", tmp, sizeof (tmp));
	gd->bd->bi_baudrate = gd->baudrate = (i > 0)
			? (int) simple_strtoul (tmp, NULL, 10)
			: CONFIG_BAUDRATE;

	return (0);
}

static int display_banner (void)
{
	printf ("\n\n%s\n\n", version_string);
	debug ("U-Boot code: %08lX -> %08lX  BSS: -> %08lX\n",
	       _armboot_start, _bss_start, _bss_end);
#ifdef CONFIG_MODEM_SUPPORT
	debug ("Modem Support enabled\n");
#endif
#ifdef CONFIG_USE_IRQ
	debug ("IRQ Stack: %08lx\n", IRQ_STACK_START);
	debug ("FIQ Stack: %08lx\n", FIQ_STACK_START);
#endif

	return (0);
}

/*
 * WARNING: this code looks "cleaner" than the PowerPC version, but
 * has the disadvantage that you either get nothing, or everything.
 * On PowerPC, you might see "DRAM: " before the system hangs - which
 * gives a simple yet clear indication which part of the
 * initialization if failing.
 */
static int display_dram_config (void)
{
	int i;

#ifdef DEBUG
	puts ("RAM Configuration:\n");

	for(i=0; i<CONFIG_NR_DRAM_BANKS; i++) {
		printf ("Bank #%d: %08lx ", i, gd->bd->bi_dram[i].start);
		print_size (gd->bd->bi_dram[i].size, "\n");
	}
#else
	ulong size = 0;

	for (i=0; i<CONFIG_NR_DRAM_BANKS; i++) {
		size += gd->bd->bi_dram[i].size;
	}
	puts("DRAM:  ");
	print_size(size, "\n");
#endif

	return (0);
}

#ifndef CFG_NO_FLASH
static void display_flash_config (ulong size)
{
	puts ("Flash: ");
	print_size (size, "\n");
}
#endif /* CFG_NO_FLASH */


/* hs.lee */

int check_wallmount()
{
	unsigned int bWall;
	unsigned int temp;

	*(volatile unsigned int *)0x22000000 = 0x1000;
	
	temp = (*(volatile unsigned int *)0x22000000);

	if ((temp>>12) & 0x1)
		bWall = 1;
	else
		bWall = 0;

	printf("########## wallmount %x\n", bWall);

	return bWall;
}


#if 1
int env_setup(void)
{
	char *buffer1;

	while(1)
	{
		int i;

		printf("\n");
		printf("------------------------------\n");
		printf("-     ENVIRONMENT SETUP      -\n");
		printf("------------------------------\n");
		printf("1. BOOTING COMMAND	= %s\n", getenv("bootcmd"));
		printf("2. BOOTING DELAY	= %s\n", getenv("bootdelay"));
		printf("3. TARGET IP		= %s\n", getenv("ipaddr"));
		printf("4. SERVER IP		= %s\n", getenv("serverip"));
		printf("5. GATEWAY IP		= %s\n", getenv("gatewayip"));
		printf("6. ETHERNET ADDRESS	= %s\n", getenv("ethaddr"));
		printf("7. BOOTING ARGS		= %s\n", getenv("bootargs"));
		printf("8. UPDATE BOOTING ARGS\n");
		printf("------------------------------\n");
		printf("s. SAVE ENVIRONMENT\n");
		printf("x. EXIT\n");
		printf("------------------------------\n");

		readline("SELECT COMMAND: ");
		if(console_buffer[1] != NULL)
			continue;
		i = (int)console_buffer[0];
		printf("\n");

		switch(i)
		{
			case '1':
				readline("INPUT BOOTING COMMAND : ");
				setenv("bootcmd",console_buffer);
				break;

			case '2':
				readline("INPUT BOOTING DELAY : ");
				setenv("bootdelay",console_buffer);
				break;

			case '3':
				readline("INPUT TARGET IP : ");
				setenv("ipaddr",console_buffer);
				break;

			case '4':
				readline("INPUT SERVER IP : ");
				setenv("serverip",console_buffer);
				break;

			case '5':
				readline("INPUT GATEWAY IP : ");
				setenv("gatewayip",console_buffer);
				break;

			case '6':
				readline("INPUT ETHERNET ADDRESS : ");
				setenv("ethaddr",console_buffer);
				break;

			case '7':
				printf("PLEASE USE 'UPDATING BOOTING ARGS' COMMAND\n");

			case '8':
				printf("UPDATING BOOTING ARGS...\n");

				buffer1 = (char *)malloc(500 * sizeof(char));

				strcpy(buffer1,"root=139:6 rw ip=");
				strcat(buffer1,getenv("ipaddr"));
				strcat(buffer1,":");
				strcat(buffer1,getenv("serverip"));
				strcat(buffer1,":");
				strcat(buffer1,getenv("gatewayip"));
				strcat(buffer1,":255.255.255.0::eth0 console=ttyS0 ethaddr=");
				strcat(buffer1,getenv("ethaddr"));

				setenv("bootargs",buffer1);

				free(buffer1);
				break;

			case 's':
				saveenv();
				printf("saved...\n");
				break;

			case 'x':
				return 1;

			default:
				;
		}
	}
}

int samsung_prompt(void)
{
	int vol = 0;
	int nFlag;
	
	while(1)
	{
	    int i;

		printf("\n");
		printf("------------------------------\n");
		printf("-   BOOTROM DEBUG SESSION    -\n");
		printf("------------------------------\n");
		printf("1. ENVIRONMENT SETUP\n\n");
		printf("2. SHOW PARTITION\n\n");
		printf("3. BBM COPY\n");
		printf("------------------------------\n");
		printf("0. JUMP TO UBOOT PROMPT\n");
		printf("r. REBOOT\n");
		printf("------------------------------\n");
		printf("E. FLASH FORMAT & ERASE\n");
		printf("F. FLASH REPARTITION FORMAT\n");
		printf("------------------------------\n");

		readline("SELECT COMMAND: ");
		if(console_buffer[1] != NULL)
			continue;
		i = (int)console_buffer[0];
		printf("\n");

		switch(i)
		{
			case '1':
				env_setup();
				break;

			case '2':
				bbm_show_partition(0);
				bbm_show_partition(1);
				break;

			case '3':
				bbm_copy();
				break;


/* sh.kim - start */
#if 0
			case 'E':
				printf("\n");
				printf("------------------------------\n");
				printf("1. CS0\n");
				printf("2. CS1\n");
				printf("------------------------------\n");
				printf("x. Go back\n");
				printf("------------------------------\n");

				readline("SELECT COMMAND: ");

				printf("\n");

				if (console_buffer[0] == 'x')
				{
					break;
				}

				if((console_buffer[1] != NULL) 
					|| ((console_buffer[0] != '1') && (console_buffer[0] != '2')))
				{
					printf("Invalid number !!\n\n");
					break;
				}
				vol = console_buffer[0] - '1';
				printf("\n");

				readline("ARE YOU SURE YOU WANT TO ERASE THIS FLASH? (y/n) : ");

				if (console_buffer[0] == 'y')
				{
					bbm_update_image();
					bbm_run(vol, CFG_ONENAND_ERASE);
					serial_setbrg();
					break;
				}
				else
				{
					break;
				}

			case 'F':
				printf("\n");
				printf("------------------------------\n");
				printf("1. INIT FORMAT\n");
				printf("2. REPARTITION FORMAT\n");
				printf("------------------------------\n");
				printf("x. Go back\n");
				printf("------------------------------\n");

				readline("SELECT COMMAND: ");

				printf("\n");

				if((console_buffer[1] != NULL) || 
					((console_buffer[0] != '1') && (console_buffer[0] != '2')))
				{
					printf("Invalid number !!\n\n");
					break;
				}
				nFlag= console_buffer[0] - '0';
				
				printf("\n");
				printf("------------------------------\n");
				printf("1. CS0\n");
				printf("2. CS1\n");
				printf("------------------------------\n");
				printf("x. Go back\n");
				printf("------------------------------\n");

				readline("SELECT COMMAND: ");

				printf("\n");

				if (console_buffer[0] == 'x')
				{
					break;
				}

				if((console_buffer[1] != NULL) 
					|| ((console_buffer[0] != '1') && (console_buffer[0] != '2')))
				{
					printf("Invalid number !!\n\n");
					break;
				}
				vol = console_buffer[0] - '1';
				printf("\n");

				readline("ARE YOU SURE YOU WANT TO FORMAT THIS FLASH? (y/n) : ");

				if (console_buffer[0] == 'y')
				{
					bbm_update_image();

					if(nFlag == 1)
					{
						bbm_run(vol, CFG_ONENAND_INIT_FORMAT);
					}
					else
					{
						bbm_run(vol, CFG_ONENAND_REPART_FORMAT);
					}
					serial_setbrg();
					break;
				}
				else
				{
					break;
				}
#else 
			case 'E':
				printf("\n");
				printf("------------------------------\n");
				printf("1. CS0\n");
				printf("2. CS1\n");
				printf("------------------------------\n");
				printf("x. Go back\n");
				printf("------------------------------\n");

				readline("SELECT COMMAND: ");

				printf("\n");

				if (console_buffer[0] == 'x')
				{
					break;
				}

				if((console_buffer[1] != NULL) 
					|| ((console_buffer[0] != '1') && (console_buffer[0] != '2')))
				{
					printf("Invalid number !!\n\n");
					break;
				}

				vol = console_buffer[0] - '1';
				printf("\n");

				readline("ARE YOU SURE YOU WANT TO ERASE THIS FLASH? (y/n) : ");

				if (console_buffer[0] == 'y')
				{
					bbm_update_image();
					bbm_run(vol, CFG_ONENAND_INIT_FORMAT);
					bbm_run(vol, CFG_ONENAND_ERASE);
					serial_setbrg();
					break;
				}
				else
				{
					break;
				}

			case 'F':
				printf("\n");
				printf("------------------------------\n");
				printf("1. CS0\n");
				printf("2. CS1\n");
				printf("------------------------------\n");
				printf("x. Go back\n");
				printf("------------------------------\n");

				readline("SELECT COMMAND: ");

				printf("\n");

				if (console_buffer[0] == 'x')
				{
					break;
				}

				if((console_buffer[1] != NULL) 
					|| ((console_buffer[0] != '1') && (console_buffer[0] != '2')))
				{
					printf("Invalid number !!\n\n");
					break;
				}

				vol = console_buffer[0] - '1';
				printf("\n");

				readline("ARE YOU SURE YOU WANT TO FORMAT THIS FLASH? (y/n) : ");

				if (console_buffer[0] == 'y')
				{
					bbm_update_image();
					bbm_run(vol, CFG_ONENAND_REPART_FORMAT);
					serial_setbrg();
					break;
				}
				else
				{
					break;
				}

#endif
/* sh.kim - end */
			case '0':
				return 1;

			case 'r':
				do_reset(NULL, 0, 0, NULL);
				break;

			default:
				;
		}
	}
	return 0;
}
#endif



/*
 * Breathe some life into the board...
 *
 * Initialize a serial port as console, and carry out some hardware
 * tests.
 *
 * The first part of initialization is running from Flash memory;
 * its main purpose is to initialize the RAM so that we
 * can relocate the monitor code to RAM.
 */

/*
 * All attempts to come up with a "common" initialization sequence
 * that works for all boards and architectures failed: some of the
 * requirements are just _too_ different. To get rid of the resulting
 * mess of board dependent #ifdef'ed code we now make the whole
 * initialization sequence configurable to the user.
 *
 * The requirements for any new initalization function is simple: it
 * receives a pointer to the "global data" structure as it's only
 * argument, and returns an integer return code, where 0 means
 * "continue" and != 0 means "fatal error, hang the system".
 */
typedef int (init_fnc_t) (void);

int print_cpuinfo (void); /* test-only */

init_fnc_t *init_sequence[] = {
	cpu_init,		/* basic cpu dependent setup */
	board_init,		/* basic board dependent setup */
	interrupt_init,		/* set up exceptions */
	env_init,		/* initialize environment */
	init_baudrate,		/* initialze baudrate settings */
	serial_init,		/* serial communications setup */
	console_init_f,		/* stage 1 init of console */
	display_banner,		/* say that we are here */
#if defined(CONFIG_DISPLAY_CPUINFO)
	print_cpuinfo,		/* display cpu info (and speed) */
#endif
#if defined(CONFIG_DISPLAY_BOARDINFO)
	checkboard,		/* display board info */
#endif
	dram_init,		/* configure available RAM banks */
	display_dram_config,
	NULL,
};

void start_armboot (void)
{
#ifdef _FASTBOOT_
	init_fnc_t **init_fnc_ptr;
	char *s;

	/* Pointer is writable since we allocated a register for it */
	gd = (gd_t*)(_armboot_start - CFG_MALLOC_LEN - sizeof(gd_t));
	/* compiler optimization barrier needed for GCC >= 3.4 */
	__asm__ __volatile__("": : :"memory");

	memset ((void*)gd, 0, sizeof (gd_t));
	gd->bd = (bd_t*)((char*)gd - sizeof(bd_t));
	memset (gd->bd, 0, sizeof (bd_t));

	monitor_flash_len = _bss_start - _armboot_start;

	for (init_fnc_ptr = init_sequence; *init_fnc_ptr; ++init_fnc_ptr) {
		if ((*init_fnc_ptr)() != 0) {
			hang ();
		}
	}

	/* armboot_start is defined in the board-specific linker script */
	mem_malloc_init (_armboot_start - CFG_MALLOC_LEN);


/* ij.jang : from RFS-1.2.2P-R2, TinyBootloader-1.0.0 */
#if (CONFIG_COMMANDS & CFG_CMD_ONENAND)
	onenand_init();
#endif

	/* initialize environment */
	env_relocate ();

	run_command ("bootd", 0);

	
#else  // _FASTBOOT_

	init_fnc_t **init_fnc_ptr;
	char *s;
#ifndef CFG_NO_FLASH
	ulong size;
#endif
#if defined(CONFIG_VFD) || defined(CONFIG_LCD)
	unsigned long addr;
#endif
	/* Pointer is writable since we allocated a register for it */
	gd = (gd_t*)(_armboot_start - CFG_MALLOC_LEN - sizeof(gd_t));
	/* compiler optimization barrier needed for GCC >= 3.4 */
	__asm__ __volatile__("": : :"memory");

	memset ((void*)gd, 0, sizeof (gd_t));
	gd->bd = (bd_t*)((char*)gd - sizeof(bd_t));
	memset (gd->bd, 0, sizeof (bd_t));

	monitor_flash_len = _bss_start - _armboot_start;

	for (init_fnc_ptr = init_sequence; *init_fnc_ptr; ++init_fnc_ptr) {
		if ((*init_fnc_ptr)() != 0) {
			hang ();
		}
	}

#ifndef CFG_NO_FLASH
	/* configure available FLASH banks */
	size = flash_init ();
	display_flash_config (size);
#endif /* CFG_NO_FLASH */

#ifdef CONFIG_VFD
	#ifndef PAGE_SIZE
	#define PAGE_SIZE 4096
	#endif
	/*
	 * reserve memory for VFD display (always full pages)
	 */
	/* bss_end is defined in the board-specific linker script */
	addr = (_bss_end + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1);
	size = vfd_setmem (addr);
	gd->fb_base = addr;
#endif /* CONFIG_VFD */

#ifdef CONFIG_LCD
	#ifndef PAGE_SIZE
	#define PAGE_SIZE 4096
	#endif
	/*
	 * reserve memory for LCD display (always full pages)
	 */
	/* bss_end is defined in the board-specific linker script */
	addr = (_bss_end + (PAGE_SIZE - 1)) & ~(PAGE_SIZE - 1);
	size = lcd_setmem (addr);
	gd->fb_base = addr;
#endif /* CONFIG_LCD */

	/* armboot_start is defined in the board-specific linker script */
	mem_malloc_init (_armboot_start - CFG_MALLOC_LEN);

#if (CONFIG_COMMANDS & CFG_CMD_NAND)
	puts ("NAND:  ");
	nand_init();		/* go init the NAND */
#endif

/* ij.jang : from RFS-1.2.2P-R2, TinyBootloader-1.0.0 */
#if (CONFIG_COMMANDS & CFG_CMD_ONENAND)
	onenand_init();
#endif

#ifdef CONFIG_HAS_DATAFLASH
	AT91F_DataflashInit();
	dataflash_print_info();
#endif

	/* initialize environment */
	env_relocate ();

#ifdef CONFIG_VFD
	/* must do this after the framebuffer is allocated */
	drv_vfd_init();
#endif /* CONFIG_VFD */

	/* IP Address */
	gd->bd->bi_ip_addr = getenv_IPaddr ("ipaddr");

	/* MAC Address */
	{
		int i;
		ulong reg;
		char *s, *e;
		char tmp[64];

		i = getenv_r ("ethaddr", tmp, sizeof (tmp));
		s = (i > 0) ? tmp : NULL;

		for (reg = 0; reg < 6; ++reg) {
			gd->bd->bi_enetaddr[reg] = s ? simple_strtoul (s, &e, 16) : 0;
			if (s)
				s = (*e) ? e + 1 : e;
		}

#ifdef CONFIG_HAS_ETH1
		i = getenv_r ("eth1addr", tmp, sizeof (tmp));
		s = (i > 0) ? tmp : NULL;

		for (reg = 0; reg < 6; ++reg) {
			gd->bd->bi_enet1addr[reg] = s ? simple_strtoul (s, &e, 16) : 0;
			if (s)
				s = (*e) ? e + 1 : e;
		}
#endif
	}

	devices_init ();	/* get the devices list going. */

#ifdef CONFIG_CMC_PU2
	load_sernum_ethaddr ();
#endif /* CONFIG_CMC_PU2 */

	jumptable_init ();

	console_init_r ();	/* fully init console as a device */

#if defined(CONFIG_MISC_INIT_R)
	/* miscellaneous platform dependent initialisations */
	misc_init_r ();
#endif

	/* enable exceptions */
	enable_interrupts ();

	/* Perform network card initialisation if necessary */
#ifdef CONFIG_DRIVER_CS8900
	cs8900_get_enetaddr (gd->bd->bi_enetaddr);
#endif

#if defined(CONFIG_DRIVER_SMC91111) || defined (CONFIG_DRIVER_LAN91C96)
	if (getenv ("ethaddr")) {
		smc_set_mac_addr(gd->bd->bi_enetaddr);
	}
#endif /* CONFIG_DRIVER_SMC91111 || CONFIG_DRIVER_LAN91C96 */

	/* Initialize from environment */
	if ((s = getenv ("loadaddr")) != NULL) {
		load_addr = simple_strtoul (s, NULL, 16);
	}
#if (CONFIG_COMMANDS & CFG_CMD_NET)
	if ((s = getenv ("bootfile")) != NULL) {
		copy_filename (BootFile, s, sizeof (BootFile));
	}
#endif	/* CFG_CMD_NET */

#ifdef BOARD_LATE_INIT
	board_late_init ();
#endif
#if (CONFIG_COMMANDS & CFG_CMD_NET)
#if defined(CONFIG_NET_MULTI)
	puts ("Net:   ");
#endif
	eth_initialize(gd->bd);
#endif

	/* main_loop() can return to retry autoboot, if so just run it again. */
	for (;;) {
		main_loop ();
	}

	/* NOTREACHED - no way out of command loop except booting */

#endif  // _FASTBOOT_
}

void hang (void)
{
	puts ("### ERROR ### Please RESET the board ###\n");
	for (;;);
}

#ifdef CONFIG_MODEM_SUPPORT
static inline void mdm_readline(char *buf, int bufsiz);

/* called from main loop (common/main.c) */
extern void  dbg(const char *fmt, ...);
int mdm_init (void)
{
	char env_str[16];
	char *init_str;
	int i;
	extern char console_buffer[];
	extern void enable_putc(void);
	extern int hwflow_onoff(int);

	enable_putc(); /* enable serial_putc() */

#ifdef CONFIG_HWFLOW
	init_str = getenv("mdm_flow_control");
	if (init_str && (strcmp(init_str, "rts/cts") == 0))
		hwflow_onoff (1);
	else
		hwflow_onoff(-1);
#endif

	for (i = 1;;i++) {
		sprintf(env_str, "mdm_init%d", i);
		if ((init_str = getenv(env_str)) != NULL) {
			serial_puts(init_str);
			serial_puts("\n");
			for(;;) {
				mdm_readline(console_buffer, CFG_CBSIZE);
				dbg("ini%d: [%s]", i, console_buffer);

				if ((strcmp(console_buffer, "OK") == 0) ||
					(strcmp(console_buffer, "ERROR") == 0)) {
					dbg("ini%d: cmd done", i);
					break;
				} else /* in case we are originating call ... */
					if (strncmp(console_buffer, "CONNECT", 7) == 0) {
						dbg("ini%d: connect", i);
						return 0;
					}
			}
		} else
			break; /* no init string - stop modem init */

		udelay(100000);
	}

	udelay(100000);

	/* final stage - wait for connect */
	for(;i > 1;) { /* if 'i' > 1 - wait for connection
				  message from modem */
		mdm_readline(console_buffer, CFG_CBSIZE);
		dbg("ini_f: [%s]", console_buffer);
		if (strncmp(console_buffer, "CONNECT", 7) == 0) {
			dbg("ini_f: connected");
			return 0;
		}
	}

	return 0;
}

/* 'inline' - We have to do it fast */
static inline void mdm_readline(char *buf, int bufsiz)
{
	char c;
	char *p;
	int n;

	n = 0;
	p = buf;
	for(;;) {
		c = serial_getc();

		/*		dbg("(%c)", c); */

		switch(c) {
		case '\r':
			break;
		case '\n':
			*p = '\0';
			return;

		default:
			if(n++ > bufsiz) {
				*p = '\0';
				return; /* sanity check */
			}
			*p = c;
			p++;
			break;
		}
	}
}
#endif	/* CONFIG_MODEM_SUPPORT */
