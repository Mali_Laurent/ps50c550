/**
 */
/**
 *
 */

#include <config.h>
#include <common.h>
#include <ssa1_fpga.h>
#include "asm/mach-types.h"

/**
 * board_init()
 * 		: Initialize jupiter board specific functions
 * @author		ij.jang
 * @return		0 on success, other value
 * @version		$Revison$
 */
int board_init (void)
{
	DECLARE_GLOBAL_DATA_PTR;
	
	/* ij.jang 051025 : this value should defined in linux/arch/arm/tools/mach-types */
	gd->bd->bi_arch_number = MACH_TYPE_SSA1FPGA;

	/* address of boot parameters = RAM Base Address + 0x100 */
	gd->bd->bi_boot_params = PHYS_SDRAM_1 + 0x100;

	icache_enable();
#if 0
	dcache_enable();
#endif

	return 0;
}

/**
 * dram_init() : Initialize the dram information of gd
 * @author		ij.jang
 * @version		$Revison$
 */
int dram_init (void)
{
	DECLARE_GLOBAL_DATA_PTR;

	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size = PHYS_SDRAM_1_SIZE;

	return 0;
}

