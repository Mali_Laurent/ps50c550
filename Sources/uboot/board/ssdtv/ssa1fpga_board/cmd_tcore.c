/*
 */

/*
 * Boot support
 */
#include <common.h>
#include <command.h>
#include <malloc.h>

#include "ssa1_tcore.h"

DECLARE_GLOBAL_DATA_PTR;


int do_tcore (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	char cmd;
	const char* prompt = "tcore select : ";

	printf("---------- two core test Menu ------------\n");
	printf("1. Register Control \n");
	printf("2. Control Slave arm \n");

	readline (prompt);

	return 0;
}

U_BOOT_CMD(
 	tcore,	CFG_MAXARGS,	0,	do_tcore,
 	"tcore   - \n",
 	"Enter menu about testing of two core\n"
);

int regControl(void)
{


}
