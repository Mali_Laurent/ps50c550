/*
 * (C) Copyright 2001
 * Kyle Harris, Nexus Technologies, Inc. kharris@nexus-tech.net
 *
 * (C) Copyright 2001
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/* Intel Strata Flash Support (28F128, 28F640) 
 * 
 * config.h should have definitions below :
 *   CFG_FLASH_INTEL		: For include this code to u-boot build
 *   CFG_FLASH_BASE			: Base address of first flash bank
 *   CFG_MAX_FLASH_BANKS	: number of flash banks
 *   CFG_MAX_FLASH_SECT		: Number of sectors for each bank
 *   PHYS_FLASH_1			: Base address of first bank
 *   PHYS_FLASH_2,PHYS_FLASH_3	: Base address of second and third bank
 *   FLASH_PORT_WIDTH16		: define this if data bus width is 16
 *   CFG_ENV_ADDR, CFG_ENV_SIZE : Base address of env data, and size
 *   CFG_FLASH_ERASE_TOUT, CFG_FLASH_WRITE_TOUT, CFG_PROTECT_TOUT : Timeout values
 *
 *   CFG_FLASH_PROTECTION	: [optional] use s/w protection
 */

#include <common.h>
#include <flash.h>

#ifdef CFG_FLASH_INTEL
#include <linux/byteorder/swab.h>

flash_info_t flash_info[CFG_MAX_FLASH_BANKS];	/* info for FLASH chips    */
/* Board support for 1 or 2 flash devices */
#ifdef FLASH_PORT_WIDTH16
#define FLASH_PORT_WIDTH		ushort
#define FLASH_PORT_WIDTHV		vu_short
#define SWAP(x)               __swab16(x)
#else
#define FLASH_PORT_WIDTH		ulong
#define FLASH_PORT_WIDTHV		vu_long
#define SWAP(x)               __swab32(x)
#endif

#define FPW	   FLASH_PORT_WIDTH
#define FPWV   FLASH_PORT_WIDTHV

#define mb() __asm__ __volatile__ ("" : : : "memory")

#define SR0		((FPW)0x00010001)
#define SR1		((FPW)0x00020002)
#define SR2		((FPW)0x00040004)
#define SR3		((FPW)0x00080008)
#define SR4		((FPW)0x00100010)
#define SR5		((FPW)0x00200020)
#define SR6		((FPW)0x00400040)
#define SR7		((FPW)0x00500080)

#define FLASH_SECT_SIZE_28F128		(128*1024)
#define FLASH_SECT_SIZE_28F640		(128*1024)
#define FLASH_SECT_NUM_28F128		128
#define FLASH_SECT_NUM_28F640		64

static ulong flash_get_size (FPW *addr, flash_info_t *info);
static int write_data (flash_info_t *info, ulong dest, FPW data);
static void flash_get_offsets (ulong base, flash_info_t *info);

unsigned long flash_init (void)
{
	ulong size = 0;
	int i=0;
	ulong flash_addr;

	for( i=0; i<CFG_MAX_FLASH_BANKS; i++ )
	{
		if( i==0 )
		{
			flash_addr = PHYS_FLASH_1;
		}
#ifdef PHYS_FLASH_2
		else if( i==1 )
		{
			flash_addr = PHYS_FLASH_2;
		}
#endif
#ifdef PHYS_FLASH_3
		else if( i==2 )
		{
			flash_addr = PHYS_FLASH_3;
		}
#endif
		else
		{
			puts("bad flash configurations..\n");
			hang();
		}

		flash_get_size ((FPW*)flash_addr, &flash_info[i]);
		flash_get_offsets (flash_addr, &flash_info[i]);		
		size += flash_info[i].size;
	}
	
	/* Protect monitor and environment sectors */
	flash_protect ( FLAG_PROTECT_SET,
			CFG_FLASH_BASE,
			CFG_FLASH_BASE + monitor_flash_len - 1,
			&flash_info[0] );

#if defined(CFG_ENV_IS_IN_FLASH) && defined(CFG_ENV_ADDR)
	flash_protect ( FLAG_PROTECT_SET,
			CFG_ENV_ADDR,
			CFG_ENV_ADDR + CFG_ENV_SIZE - 1, &flash_info[0] );
#endif
	return size;
}

/*-----------------------------------------------------------------------
 */
static void flash_get_offsets (ulong base, flash_info_t *info)
{
	int i;

	if (info->flash_id == FLASH_UNKNOWN)
	{
		return;
	}

	if ((info->flash_id & FLASH_VENDMASK) == FLASH_MAN_INTEL)
	{
		for (i = 0; i < info->sector_count; i++)
		{
			info->start[i] = base + (i * PHYS_FLASH_SECT_SIZE);
			info->protect[i] = 1;
		}
	}
}

/*-----------------------------------------------------------------------
 */
void flash_print_info (flash_info_t *info)
{
	int i;

	if (info->flash_id == FLASH_UNKNOWN) {
		printf ("missing or unknown FLASH type\n");
		return;
	}

	switch (info->flash_id & FLASH_VENDMASK) {
	case FLASH_MAN_INTEL:
		printf ("INTEL ");
		break;
	default:
		printf ("Unknown Vendor ");
		break;
	}

	switch (info->flash_id & FLASH_TYPEMASK) {
	case FLASH_28F128J3A:
		printf ("28F128J3A\n");
		break;
	case FLASH_28F640J3A:
		printf ("28F640J3A\n");
		break;
	default:
		printf ("Unknown Chip Type\n");
		break;
	}

	printf ("  Size: %ld MB in %d Sectors\n",
			info->size >> 20, info->sector_count);

	printf ("  Sector Start Addresses:");
	for (i = 0; i < info->sector_count; ++i) {
		if ((i % 5) == 0)
			printf ("\n   ");
		printf (" %08lX%s",
			info->start[i],
			info->protect[i] ? " (RO)" : "     ");
	}
	printf ("\n");
	return;
}

/*
 * The following code cannot be run from FLASH!
 */
static ulong flash_get_size (FPW *addr, flash_info_t *info)
{
	volatile FPW value;

	/* Write auto select command: read Manufacturer ID */
	addr[0x5555] = (FPW) 0x00AA00AA;
	addr[0x2AAA] = (FPW) 0x00550055;
	addr[0x5555] = (FPW) 0x00900090;

	mb ();
	value = addr[0];

	printf("flash id code = %04X, ", value);
	
	switch (value) {

	case (FPW) INTEL_MANUFACT:
		info->flash_id = FLASH_MAN_INTEL;
		break;

	default:
		info->flash_id = FLASH_UNKNOWN;
		info->sector_count = 0;
		info->size = 0;
		addr[0] = (FPW) 0x00FF00FF;
		printf("Detection failed.\n");
		return (0);	/* no or unknown flash  */
	}

	mb ();
	value = addr[1];			/* device ID        */
	
	switch (value)
	{
	case (FPW) INTEL_ID_28F128J3A:
		info->flash_id += FLASH_28F128J3A;
		info->sector_count = CFG_MAX_FLASH_SECT;
		info->size = FLASH_SECT_SIZE_28F128 * CFG_MAX_FLASH_SECT;
		if( FLASH_SECT_SIZE_28F128 != PHYS_FLASH_SECT_SIZE )
		{
			printf("Configuration bad : Sector size is missmatch.\n");
			info->flash_id = FLASH_UNKNOWN;
		}
		printf("Intel 28F128J3A\n");
		if( CFG_MAX_FLASH_SECT > FLASH_SECT_NUM_28F128 )
		{
			printf("Configuration bad : sector number should be lower than %d\n",
					FLASH_SECT_NUM_28F128);
			info->flash_id = FLASH_UNKNOWN;
		}
		break;
	case (FPW) INTEL_ID_28F640J3A:
		info->flash_id += FLASH_28F640J3A;
		info->sector_count = CFG_MAX_FLASH_SECT;
		info->size = FLASH_SECT_SIZE_28F640 * CFG_MAX_FLASH_SECT;
		if( FLASH_SECT_SIZE_28F640 != PHYS_FLASH_SECT_SIZE )
		{
			printf("Configuration bad : Sector size is missmatch.\n");
			info->flash_id = FLASH_UNKNOWN;
		}
		printf("Intel 28F640J3A\n");
		if( CFG_MAX_FLASH_SECT > FLASH_SECT_NUM_28F640 )
		{
			printf("Configuration bad : sector number should be lower than %d\n",
					FLASH_SECT_NUM_28F640);
			info->flash_id = FLASH_UNKNOWN;
		}
		break;
	default:
		info->flash_id = FLASH_UNKNOWN;
		printf("Not supported device.\n");
		break;
	}

	addr[0] = (FPW) 0x00FF00FF;		/* restore read mode */
	return (info->size);
}

int flash_erase (flash_info_t *info, int s_first, int s_last)
{
	int flag, prot, sect;
	ulong type, start, last;
	int rcode = 0;

	if ((s_first < 0) || (s_first > s_last)) {
		if (info->flash_id == FLASH_UNKNOWN) {
			printf ("- missing\n");
		} else {
			printf ("- no sectors to erase\n");
		}
		return 1;
	}

	type = (info->flash_id & FLASH_VENDMASK);
	if ((type != FLASH_MAN_INTEL)) {
		printf ("Can't erase unknown flash type %08lx - aborted\n",
			info->flash_id);
		return 1;
	}

	prot = 0;
	for (sect = s_first; sect <= s_last; sect++) {
		if (info->protect[sect]) {
			prot++;
		}
	}

	if (prot) {
		printf ("- %d protected sectors will not be erased!\n",
			prot);
	} else {
		printf ("\n");
	}

	start = get_timer (0);
	last = start;

	/* Disable interrupts which might cause a timeout here */
	flag = disable_interrupts ();

	/* Start erase on unprotected sectors */
	for (sect = s_first; sect <= s_last; sect++) {
		if (info->protect[sect] == 0) {	/* not protected */
			FPWV *addr = (FPWV *) (info->start[sect]);
			FPW status;

			printf ("Erasing sector %2d ... ", sect);

			/* arm simple, non interrupt dependent timer */
			reset_timer_masked ();

			*addr = (FPW) 0x00500050;	/* clear status register */
			*addr = (FPW) 0x00200020;	/* erase setup */
			*addr = (FPW) 0x00D000D0;	/* erase confirm */

			while (((status = *addr) & (FPW) 0x00800080) !=(FPW) 0x00800080) {
				if (get_timer_masked () > CFG_FLASH_ERASE_TOUT) {
					printf ("Timeout\n");
					*addr = (FPW) 0x00B000B0;	/* suspend erase     */
					*addr = (FPW) 0x00FF00FF;	/* reset to read mode */
					rcode = 1;
					break;
				}
			}

			/* ij.jang 3.Mar.2006 fixed : check the status */
			/* SR.5 : error,
			   SR.4 : invalid command,
			   SR.3 : VPEN < VPENLK
			   SR.1 : block lock bit is set */
			status = *addr;
			rcode = 1;
			if( status & SR5 )	/* Status Register bit 5th */
			{
				printf("Error detected : ");
				if( status & SR3 )
					printf("VPEN low voltage detected. (<VPENLK)\n");
				else if( status & SR1 )
					printf("Block Lock-Bit detected. operation aborted\n");
				else if( status &  SR4 )
					printf("Programming sequence error\n");
			}
			else
			{
				rcode = 0;	/* ok */
			}

			/* ij.jang 11.22.2005 fixed : apply casting */
			*addr = (FPW)0x00500050;	/* clear status register cmd.   */
			*addr = (FPW)0x00FF00FF;	/* resest to read mode          */

			printf (" done\n");
		}
	}
	return rcode;
}

/*-----------------------------------------------------------------------
 * Copy memory to flash, returns:
 * 0 - OK
 * 1 - write timeout
 * 2 - Flash not erased
 * 4 - Flash not identified
 */
int write_buff (flash_info_t *info, uchar *src, ulong addr, ulong cnt)
{
	ulong cp, wp;
	FPW data;
	int i, l, rc, port_width, cnt_bytes;

	if (info->flash_id == FLASH_UNKNOWN) {
		return 4;
	}
/* get lower word aligned address */
#ifdef FLASH_PORT_WIDTH16
	wp = (addr & ~1);
	port_width = 2;
#else
	wp = (addr & ~3);
	port_width = 4;
#endif

	/*
	 * handle unaligned start bytes
	 */
	if ((l = addr - wp) != 0)
	{
		data = 0;
		for (i = 0, cp = wp; i < l; ++i, ++cp)
		{
			data = (data << 8) | (*(uchar *) cp);
		}
		for (; i < port_width && cnt > 0; ++i) {
			data = (data << 8) | *src++;
			--cnt;
			++cp;
		}
		for (; cnt == 0 && i < port_width; ++i, ++cp) {
			data = (data << 8) | (*(uchar *) cp);
		}

		if ((rc = write_data (info, wp, SWAP (data))) != 0) {
			return (rc);
		}
		wp += port_width;
	}

	/*
	 * handle word aligned part
	 */
	putc('\n');
	cnt_bytes = 0;
	while (cnt >= port_width)
	{
		data = 0;
		for (i = 0; i < port_width; ++i)
		{
			data = (data << 8) | *src++;
		}
		if ((rc = write_data (info, wp, SWAP (data))) != 0)
		{
			return (rc);
		}
		wp += port_width;
		cnt -= port_width;
		
		if ( cnt_bytes > 0 )
		{
			if( (cnt_bytes % 0x1000) == 0 ) /* 4kb */
			{
				putc('#');
			}
			if( (cnt_bytes % 0x20000) == 0 ) /* 100kb */
			{
				printf(" %dKB\n", cnt_bytes >> 10 );
			}
		}
		cnt_bytes += port_width;
	}
	printf("\nTotal %dKB(0x%X bytes)\n", cnt_bytes>>10, cnt_bytes );

	if (cnt == 0) {
		return (0);
	}

	/*
	 * handle unaligned tail bytes
	 */
	data = 0;
	for (i = 0, cp = wp; i < port_width && cnt > 0; ++i, ++cp)
	{
		data = (data << 8) | *src++;
		--cnt;
	}
	for (; i < port_width; ++i, ++cp)
	{
		data = (data << 8) | (*(uchar *) cp);
	}

	return (write_data (info, wp, SWAP (data)));
}

/*-----------------------------------------------------------------------
 * Write a word or halfword to Flash, returns:
 * 0 - OK
 * 1 - write timeout
 * 2 - Flash not erased
 */
static int write_data (flash_info_t *info, ulong dest, FPW data)
{
	FPWV *addr = (FPWV *) dest;
	ulong status;
	int flag;
	unsigned long timestamp = 0;
	int rcode = 0;

	/* Check if Flash is (sufficiently) erased */
	if ((*addr & data) != data) {
		printf ("not erased at %08lx (%lx)\n", (ulong) addr, *addr);
		return (2);
	}
	/* Disable interrupts which might cause a timeout here */
	flag = disable_interrupts ();

	*addr = (FPW) 0x00500050;	/* clear status register */
	*addr = (FPW) 0x00400040;	/* write setup */
	*addr = data;

	/* arm simple, non interrupt dependent timer */
	reset_timer_masked ();
	
	/* wait while polling the status register */
	while (((status = *addr) & (FPW) 0x00800080) != (FPW) 0x00800080)
	{
		timestamp = get_timer_masked();
		if ( timestamp > CFG_FLASH_WRITE_TOUT )
		{
			*addr = (FPW) 0x00FF00FF;	/* restore read mode */
			return (1);
		}
	}
	/* ij.jang 3.Mar.2006 fixed : check the status */
	/* SR.4 : error,
	   SR.3 : Low voltage (VPEN<VPENLK),
	   SR.1 : block lock bit is set */
	/* It works fine, but slow.. */
#if 0
	status = *addr;
	rcode = 1;

	if( status & (FPW)0x00080008 )	/* SR.3 */
		printf("Low voltage detected(VPEN<VPENLK)\n");
	else if( status & (FPW)0x00020002 )	/* SR.1 */
		printf("Block Lock-bit detected. operation aborted\n");
	else if( status & (FPW)0x00100010 ) /* SR.4 */
		printf("Flash programming Error\n");
	else
		rcode = 0;	/* ok */
#else
		rcode = 0;
#endif
	*addr = (FPW)0x00500050;	/* clear status register cmd.   */
	*addr = (FPW)0x00FF00FF;	/* restore read mode */
	return rcode;
}

/**
 * flash_real_protect()
 * 		: implement if CFG_FLASH_PROTECTION was defined.
 * @author		ij.jang
 * @return		0 on success, other value
 * @param		info: flash info, sector: sector number, prot: 0 unprotect, 1 protect
 * @version		$Revison$
 */
/* ij.jang 8.Mar.2006 : implement */
#ifdef CFG_FLASH_PROTECTION
int flash_real_protect(flash_info_t *info, long sector, int prot)
{
	FPWV status;
	int rcode = 0;
	unsigned int timestamp;

	if( sector < 0 || sector >= info->sector_count )
	{
		printf("%d sector : out of the sector range\n");
		return 1;
	}

	FPWV* addr = (FPWV*)info->start[sector];
	*addr = (FPW)0x00500050;	/* clear status register cmd.   */

	if( prot )
	{
		/* Set Block Lock-Bit */
		*addr = (FPW)0x00600060;
		*addr = (FPW)0x00010001;
	}
	else
	{
		/* Clear Lock-Bit */
		*addr = (FPW)0x00600060;
		*addr = (FPW)0x00D000D0;
	}
	
	reset_timer_masked ();

	/* wait until finished */
	while (((status = *addr) & (FPW) 0x00800080) != (FPW) 0x00800080)
	{
		timestamp = get_timer_masked();
		if ( timestamp > CFG_FLASH_PROTECT_TOUT )
		{
			*addr = (FPW) 0x00FF00FF;	/* restore read mode */
			return (1);
		}
	}

	/* Check the result */
	status = *addr;

	if( prot )
	{
		if( status & SR3 )
		{
			printf("Voltage Range Error.\n");
			rcode = 1;
		}
		else if( ( status & (SR4|SR5) ) == (SR4|SR5) )
		{
			printf("Command sequeuce Error\n");
			rcode = 1;
		}
		else if( (status & SR4) == SR4 )
		{
			printf("Unknown Error\n");
			rcode = 1;
		}
		else
		{
			/* ok */
			rcode = 0;
		}
	}

	*addr = (FPW)0x00500050;	/* clear status register cmd.   */
	*addr = (FPW)0x00FF00FF;	/* reset to read mode          */
	return rcode;
}
#endif
#endif

