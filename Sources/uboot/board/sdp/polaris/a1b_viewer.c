#include <common.h>
#include <command.h>
#include <strutils.h>

#undef TEST_MODE
#define barrier() __asm__ __volatile__("": : :"memory")
#undef ERROR_LOG
#define READ_DQS_DEFAULT	0x0
#define WRITE_DQS_DEFAULT	0x0
#define READ_DQS_DEFAULT	0x0
#define WRITE_DQS_DEFAULT	0x0
#ifndef TEST_MODE
extern void a1b_reg_write(unsigned int offset,unsigned int bit_pos,unsigned int bit_size,unsigned int value);
extern void a1b_reg_read(unsigned int offset,unsigned int bit_pos,unsigned int bit_size,unsigned int *value);
extern void a1b_block_dump(unsigned int block_offset,unsigned int *dump_buffer,unsigned int block_size);
extern void a1b_memory_dump(unsigned int offset,unsigned int *dump_buffer,unsigned int dump_size);
extern void a1b_memory_write(unsigned int offset,unsigned int *dump_buffer,unsigned int dump_size);
#endif

struct error_list_t
{
	unsigned int dqs;
	unsigned int original_value;
	unsigned int error_value;
};
struct error_list_t * error_list=(struct error_list_t *)0x21000000;

extern int readhex(const char* prompt, unsigned int *pval);

#define MEM_TEST_MAX	8
#define MEM_TEST_SIZE	4096
//static unsigned int mem_test_pattern[MEM_TEST_MAX]={0x00000000,0xffffffff,0x00000000,0xffffffff,0x00000000,0xffffffff,0x00000000,0xffffffff};
static unsigned int mem_test_pattern[MEM_TEST_MAX]={0x11111111,0x22222222,0x33333333,0x44444444,0x55555555,0x66666666,0x77777777,0x88888888};

static unsigned int *dump_buffer=0x20000000;

static void show_register_dump_data(unsigned int start_offset,unsigned int *dump_data,unsigned int size)
{
	int i;
	for(i=0;i<size;i++)
	{
		if((i&0x3)==0)
			printf("\n\r%04x :",start_offset+i);
		printf("\t%04x",(unsigned short)dump_data[i]);
	}
}

static void show_host_memory(unsigned int *start_address,unsigned int size)
{
	int i;
	for(i=0;i<size;i++)
	{
		if((i&0x3)==0)
			printf("\n\r%08x :",start_address+(i));
		printf("\t%08x",start_address[i]);
	}
}

static void show_pci_memory(unsigned int *start_address,unsigned int size,unsigned int pci_addr)
{
	int i;
	for(i=0;i<size;i++)
	{
		if((i&0x3)==0)
			printf("\n\r%08x :",pci_addr+(i<<2));
		printf("\t%08x",start_address[i]);
	}
}

int do_a1b_test(cmd_tbl_t *cmdtp, int flag, int argc, char* argv[])
{
	unsigned int offset,bit_pos,bit_size,value,test_size,test_count,error_count,error_no;
	int rw_sel,i,j,k,l,m,pattern_index;
	unsigned int dqs_value,dqs_value_2,backup_value1,backup_value2,backup_value3,count,ctrl_offset,delay_amount,loop;
	char progress[]="|/-\\";
#ifndef TEST_MODE
	backup_value1=READ_DQS_DEFAULT;
	backup_value2=READ_DQS_DEFAULT;
	backup_value3=WRITE_DQS_DEFAULT;
	a1b_reg_write(0x96,0,16,backup_value1);
	a1b_reg_write(0x97,0,16,backup_value2);
	a1b_reg_write(0x9d,9,7,backup_value3);
	a1b_reg_write(0xa4,0,1,1);
	a1b_reg_write(0xa4,0,1,0);
#endif
	while(1)
	{
		printf("\n\r1. Register Read\n\r");
		printf("2. Register Write\n\r");
		printf("3. Register Block Dump\n\r");
		printf("4. PCI Memory Dump\n\r");
		printf("5. PCI Memory Write\n\r");
		printf("6. Memory Margine Test\n\r");
		printf("7. Memory Long Run Test\n\r");
		printf("8. Memory Test\n\r");
		printf("9. Host Memory Dump\r\n");
		printf("10. Host Memory Compare\r\n");
		printf("11. Byte Unit Margine Test\r\n");
		printf("12. Memory Margine Test Short\r\n");
		printf("13. Memory Margine Test Short\r\n");
		printf("99. Exit\n\r");

		readhex("Select Item: ", &rw_sel);

		switch(rw_sel)
		{
			case 1:
			case 2:
				readhex("Register Offset : ", &offset);
				readhex("Bit Position : ",&bit_pos);
				readhex("Bit size : ",&bit_size);
				if(rw_sel==2)
				{
					readhex("Write Value : ",&value);
#ifndef TEST_MODE
					a1b_reg_write(offset,bit_pos,bit_size,value);
#endif
				}
				if(rw_sel==1)
				{
#ifndef TEST_MODE
					a1b_reg_read(offset,bit_pos,bit_size,&value);
#endif
					printf("Read Value : %x\n\r",(unsigned short)value);
				}
			break;
			case 3:
				readhex("Block Offset : ", &offset);
				readhex("Block size(dword) : ",&bit_size);
#ifndef TEST_MODE
				a1b_block_dump(offset,dump_buffer,bit_size);
#endif
				show_register_dump_data(offset,dump_buffer,bit_size);
			break;
			case 4:
				readhex("Memory offset(hex) : ",&offset);
				readhex("Dump Size(hex:1==32byte) : ",&bit_size);
#ifndef TEST_MODE
				a1b_memory_dump(offset,dump_buffer,bit_size);
#endif
				show_pci_memory(dump_buffer,bit_size<<3,offset);

			break;
			case 5:
				readhex("Memory offset(hex) : ",&offset);
				readhex("Write Data Address (hex) : ",&bit_pos);
				readhex("Write Size(hex:1==32byte) : ",&bit_size);
#ifndef TEST_MODE
				a1b_memory_write(offset,dump_buffer,bit_size);
#endif
			break;
			case 6:
				pattern_index=0;
				printf("1. Read Test\n\r");
				printf("2. Write Test\n\r");
				readhex("Select Item: ", &rw_sel);
				readhex("Test Size(hex:4byte)",&bit_size);
				readhex("Test Count(hex)",&test_count);
#ifndef TEST_MODE
				a1b_reg_read(0x96,0,16,&backup_value1);
				a1b_reg_read(0x97,0,16,&backup_value2);
				a1b_reg_read(0x9d,9,7,&backup_value3);
				a1b_reg_write(0xa4,0,1,1);
				a1b_reg_write(0xa4,0,1,0);
#endif
				error_no=0;
				for(ctrl_offset=0;ctrl_offset<2;ctrl_offset++)
					for(delay_amount=0;delay_amount<0x20;delay_amount++)
					{
						dqs_value=0;
						dqs_value|=ctrl_offset;
						dqs_value<<=6;
						dqs_value|=delay_amount;
						value=0;
						value|=dqs_value;
						value<<=8;
						value|=dqs_value;
						error_count=0;
						printf("ctrl_offset[6]=%x, delay amount=%02x ",(dqs_value&0x40)>>6,dqs_value&0x3f);
						for(k=0;k<test_count;k++)
						{
							pattern_index++;
							for(i=0;i<bit_size;i++)
								for(j=0;j<MEM_TEST_MAX;j++)
									dump_buffer[i*MEM_TEST_MAX+j]=mem_test_pattern[(j+pattern_index)%MEM_TEST_MAX];
							if(rw_sel==1)
							{

								a1b_memory_write(0x32a0000,dump_buffer,bit_size);
								a1b_reg_write(0x96,0,16,value);
								a1b_reg_write(0x97,0,16,value);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
								a1b_memory_dump(0x32a0000,dump_buffer,bit_size);
								a1b_reg_write(0x96,0,16,backup_value1);
								a1b_reg_write(0x97,0,16,backup_value2);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
							}
							else
							{
								a1b_reg_write(0x9d,9,7,dqs_value);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
								a1b_memory_write(0x32a0000,dump_buffer,bit_size);
								a1b_reg_write(0x9d,9,7,backup_value3);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
								a1b_memory_dump(0x32a0000,dump_buffer,bit_size);
							}

							for(i=0;i<(bit_size-1)*MEM_TEST_MAX;i++)
								if(dump_buffer[i]!=mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX])
								{
									error_list[error_no].dqs=dqs_value;
									error_list[error_no].original_value=mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX];
									error_list[error_no++].error_value=dump_buffer[i];
									error_count++;
									break;
								}
						}
						if(error_count==0)
							printf("Pass\n");
						else
						{
							printf("Fail...error count = %d(%d/%d)\n",error_count,error_count,test_count);
						}
						a1b_reg_write(0x96,0,16,backup_value1);
						a1b_reg_write(0x97,0,16,backup_value2);
						a1b_reg_write(0x9d,9,7,backup_value3);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
					}
#ifdef ERROR_LOG
					for(i=0;i<error_no;i++)
						{
							printf("ctrl[6]=%02x,delay amount = %02x ",(error_list[i].dqs&0x40)>>6,error_list[i].dqs&0x3f); 
							printf(" original value = %08x, error value = %08x\n",error_list[i].original_value,error_list[i].error_value);
						}
#endif
			break;
			case 7:
				pattern_index=0;
				loop=0;
#ifndef TEST_MODE
				a1b_reg_read(0x96,0,16,&backup_value1);
				a1b_reg_read(0x97,0,16,&backup_value2);

				a1b_reg_read(0x9d,9,7,&backup_value3);
#endif
				printf("1. Read Test\n\r");
				printf("2. Write Test\n\r");
				readhex("Select Item: ", &rw_sel);
				readhex("Test Size(hex:4byte) : ",&test_size);
				readhex("Ctrl Offset[6] : ",&offset);
				readhex("Delay_Amount : ",&bit_pos);
				error_count=0;
				value=0;
				dqs_value=0;
				dqs_value|=offset;
				dqs_value<<=6;
				dqs_value|=bit_pos;
				value|=dqs_value;
				value<<=8;
				value|=dqs_value;
				while(1)
				{
#ifndef TEST_MODE
					count++;
					pattern_index++;
					for(i=0;i<test_size;i++)
						for(j=0;j<MEM_TEST_MAX;j++)
							dump_buffer[i*MEM_TEST_MAX+j]=mem_test_pattern[(j+pattern_index)%MEM_TEST_MAX];

					if(rw_sel==1)
					{
						a1b_reg_write(0x96,0,16,backup_value1);
						a1b_reg_write(0x97,0,16,backup_value2);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
						a1b_memory_write(0x32a0000,dump_buffer,test_size);
						a1b_reg_write(0x96,0,16,value);
						a1b_reg_write(0x97,0,16,value);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
					}
					else
					{
						a1b_reg_write(0x9d,9,7,dqs_value);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
						a1b_memory_write(0x32a0000,dump_buffer,test_size);
						a1b_reg_write(0x9d,9,7,backup_value3);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
					}
					a1b_memory_dump(0x32a0000,dump_buffer,test_size);
#endif
					for(i=0;i<(test_size-1)*MEM_TEST_MAX;i++)
						if(dump_buffer[i]!=mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX])
						{
							error_count++;
							printf("\nerror address = %08x, original value = %08x, error value = %08x error count = %d(%d/%d)\n",(0x32a0000+i<<4),mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX],dump_buffer[i],error_count,error_count,loop);
						}
					loop++;
					printf("\b");
					printf("%c",progress[pattern_index%4]);
					continue;
not_match:
				}
			break;
			case 8:
				pattern_index=0;
#ifndef TEST_MODE
				a1b_reg_read(0x96,0,16,&backup_value1);
				a1b_reg_read(0x97,0,16,&backup_value2);
				a1b_reg_read(0x9d,9,7,&backup_value3);
#endif
				printf("1. Read Test\n\r");
				printf("2. Write Test\n\r");
				readhex("Select Item: ", &rw_sel);
				readhex("Test Size(hex:4byte) : ",&test_size);
				readhex("Test Count (hex) : ",&test_count);
				readhex("Ctrl Offset[6] : ",&offset);
				readhex("Delay_Amount : ",&bit_pos);
				value=0;
				dqs_value=0;
				dqs_value|=offset;
				dqs_value<<=6;
				dqs_value|=bit_pos;
				value|=dqs_value;
				value<<=8;
				value|=dqs_value;
				error_count=0;
				for(k=0;k<test_count;k++)
				{
					pattern_index++;
					for(i=0;i<test_size;i++)
						for(j=0;j<MEM_TEST_MAX;j++)
							dump_buffer[i*MEM_TEST_MAX+j]=mem_test_pattern[(j+pattern_index)%MEM_TEST_MAX];

#ifndef TEST_MODE
					if(rw_sel==1)
					{
						a1b_reg_write(0x96,0,16,backup_value1);
						a1b_reg_write(0x97,0,16,backup_value2);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
						a1b_memory_write(0x32a0000,dump_buffer,test_size);
						a1b_reg_write(0x96,0,16,value);
						a1b_reg_write(0x97,0,16,value);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
					}
					else
					{
						a1b_reg_write(0x9d,9,7,dqs_value);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
						a1b_memory_write(0x32a0000,dump_buffer,bit_size);
						a1b_reg_write(0x9d,9,7,backup_value3);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
					}
					a1b_memory_dump(0x32a0000,dump_buffer,test_size);
#endif
					for(i=0;i<(test_size-1)*MEM_TEST_MAX;i++)
						if(dump_buffer[i]!=mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX])
							goto no_match;
					printf("O");
					a1b_reg_write(0x96,0,16,backup_value1);
					a1b_reg_write(0x97,0,16,backup_value2);
					a1b_reg_write(0x9d,9,7,backup_value2);
					a1b_reg_write(0xa4,0,1,1);
					a1b_reg_write(0xa4,0,1,0);
					continue;
no_match:
					printf("X");
					error_count++;
					a1b_reg_write(0x96,0,16,backup_value1);
					a1b_reg_write(0x97,0,16,backup_value2);
					a1b_reg_write(0x9d,9,7,backup_value2);
					a1b_reg_write(0xa4,0,1,1);
					a1b_reg_write(0xa4,0,1,0);
				}
				if(error_count==0)
					printf("\n>>>>>>>>>>Memory Test Pass\n");
				else
					printf("\n>>>>>>>>>>Memory Test Fail...Error Count = %d\n",error_count);
			break;
			case 0x9:
				readhex("Start Address(hex) : ",&bit_pos);
				readhex("SIze (hex:dword): ",&bit_size);

				show_host_memory((unsigned int *)bit_pos,bit_size);
			break;
			case 0x10:
				readhex("SRC Address(hex) : ",&offset);
				readhex("DST Address(hex) : ",&bit_pos);
				readhex("SIze (hex:byte): ",&bit_size);
				if(	!memcmp((void *)offset,(void *)bit_pos,bit_size))
					printf(">>>>>>>>>>match memory area\n\r");
				else
					printf(">>>>>>>>>>not match memory area\r\n");
			break;
			case 0x11:
				pattern_index=0;
				readhex("Test Size(hex:4byte) : ",&bit_size);
				readhex("Test Count(hex) : ",&test_count);
				a1b_reg_read(0x96,0,16,&backup_value1);
				a1b_reg_read(0x97,0,16,&backup_value2);
				a1b_reg_write(0xa4,0,1,1);
				a1b_reg_write(0xa4,0,1,0);
				error_no=0;
				for(k=0;k<4;k++)
					for(ctrl_offset=0;ctrl_offset<2;ctrl_offset++)
						for(delay_amount=0;delay_amount<0x20;delay_amount++)
						{
							switch(k)
							{
								case 0:
									value=0;
									dqs_value=0;
									dqs_value|=ctrl_offset;
									dqs_value<<=6;
									dqs_value|=delay_amount;
								break;
								case 1:
									value=0;
									dqs_value=0;
									dqs_value|=ctrl_offset;
									dqs_value<<=6;
									dqs_value|=delay_amount;
									dqs_value<<=8;
								break;
								case 2:
									value=0;
									dqs_value=0;
									value|=ctrl_offset;
									value<<=6;
									value|=delay_amount;
								break;
								case 3:
									value=0;
									dqs_value=0;
									value|=ctrl_offset;
									value<<=6;
									value|=delay_amount;
									value<<=8;
								break;
							}
							printf("ctrl_offset%d[6]=%x, delay amount=%02x ",k,ctrl_offset,delay_amount);
							error_count=0;
							for(l=0;l<test_count;l++)
							{
								pattern_index++;
								for(i=0;i<bit_size;i++)
									for(j=0;j<MEM_TEST_MAX;j++)
										dump_buffer[i*MEM_TEST_MAX+j]=mem_test_pattern[(j+pattern_index)%MEM_TEST_MAX];
								a1b_memory_write(0x32a0000,dump_buffer,bit_size);
								a1b_reg_write(0x96,0,16,dqs_value);
								a1b_reg_write(0x97,0,16,value);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
								a1b_memory_dump(0x32a0000,dump_buffer,bit_size);
								a1b_reg_write(0x96,0,16,backup_value1);
								a1b_reg_write(0x97,0,16,backup_value2);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
								for(i=0;i<(bit_size-1)*MEM_TEST_MAX;i++)
									if(dump_buffer[i]!=mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX])
									{
										error_list[error_no].dqs=value<<16;
										error_list[error_no].dqs|=dqs_value;
										error_list[error_no].original_value=mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX];
										error_list[error_no++].error_value=dump_buffer[i];
										barrier();
										error_count++;
										barrier();
										break;
									}
							}
							if(error_count==0)
								printf("Pass\n");
							else
								printf("Fail..Error Count=%d(%d/%d)\n",error_count,error_count,test_count);
						}
				a1b_reg_write(0x96,0,16,backup_value1);
				a1b_reg_write(0x97,0,16,backup_value2);
				a1b_reg_write(0xa4,0,1,1);
				a1b_reg_write(0xa4,0,1,0);
#ifdef ERROR_LOG
				for(i=0;i<error_no;i++)
				{
					printf("ctrl0[6]=%02x,delay amount = %02x,ctrl1[6]=%02x,delay amount = %02x,ctrl2[6]=%02x,delay amount = %02x,ctrl3[6]=%02x,delay amount = %02x\n",
						(error_list[i].dqs&1<<6)>>6,(error_list[i].dqs&0x3f<<0)>>0,
						(error_list[i].dqs&1<<14)>>14,(error_list[i].dqs&0x3f<<8)>>8,
						(error_list[i].dqs&1<<22)>>22,(error_list[i].dqs&0x3f<<16)>>16,
						(error_list[i].dqs&1<<30)>>30,(error_list[i].dqs&0x3f<<24)>>24);
					printf(" original value = %08x, error value = %08x\n",error_list[i].original_value,error_list[i].error_value);
				}
#endif					
			break;
			case 0x12:
				pattern_index=0;
				printf("1. Read Test\n\r");
				printf("2. Write Test\n\r");
				readhex("Select Item: ", &rw_sel);
				readhex("Test Size(hex:4byte)",&bit_size);
				readhex("Test Count(hex)",&test_count);
#ifndef TEST_MODE
				a1b_reg_read(0x96,0,16,&backup_value1);
				a1b_reg_read(0x97,0,16,&backup_value2);
				a1b_reg_read(0x9d,9,7,&backup_value3);
				a1b_reg_write(0xa4,0,1,1);
				a1b_reg_write(0xa4,0,1,0);
#endif
				error_no=0;
				for(ctrl_offset=0;ctrl_offset<2;ctrl_offset++)
					for(delay_amount=0;delay_amount<0x20;delay_amount++)
					{
						if(ctrl_offset==0&&delay_amount>0x9)
							break;
						if(ctrl_offset==1&&delay_amount>0x15)
							break;
						dqs_value=0;
						dqs_value|=ctrl_offset;
						dqs_value<<=6;
						dqs_value|=delay_amount;
						value=0;
						value|=dqs_value;
						value<<=8;
						value|=dqs_value;
						error_count=0;
						printf("ctrl_offset[6]=%x, delay amount=%02x ",(dqs_value&0x40)>>6,dqs_value&0x3f);
						for(k=0;k<test_count;k++)
						{
							pattern_index++;
							for(i=0;i<bit_size;i++)
								for(j=0;j<MEM_TEST_MAX;j++)
									dump_buffer[i*MEM_TEST_MAX+j]=mem_test_pattern[(j+pattern_index)%MEM_TEST_MAX];
							if(rw_sel==1)
							{

								a1b_memory_write(0x32a0000,dump_buffer,bit_size);
								a1b_reg_write(0x96,0,16,value);
								a1b_reg_write(0x97,0,16,value);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
								a1b_memory_dump(0x32a0000,dump_buffer,bit_size);
								a1b_reg_write(0x96,0,16,backup_value1);
								a1b_reg_write(0x97,0,16,backup_value2);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
							}
							else
							{
								a1b_reg_write(0x9d,9,7,dqs_value);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
								a1b_memory_write(0x32a0000,dump_buffer,bit_size);
								a1b_reg_write(0x9d,9,7,backup_value3);
								a1b_reg_write(0xa4,0,1,1);
								a1b_reg_write(0xa4,0,1,0);
								a1b_memory_dump(0x32a0000,dump_buffer,bit_size);
							}

							for(i=0;i<(bit_size-1)*MEM_TEST_MAX;i++)
								if(dump_buffer[i]!=mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX])
								{
									error_list[error_no].dqs=dqs_value;
									error_list[error_no].original_value=mem_test_pattern[(i+pattern_index)%MEM_TEST_MAX];
									error_list[error_no++].error_value=dump_buffer[i];
									error_count++;
									break;
								}
						}
						if(error_count==0)
							printf("Pass\n");
						else
						{
							printf("Fail...error count = %d(%d/%d)\n",error_count,error_count,test_count);
						}
						a1b_reg_write(0x96,0,16,backup_value1);
						a1b_reg_write(0x97,0,16,backup_value2);
						a1b_reg_write(0x9d,9,7,backup_value3);
						a1b_reg_write(0xa4,0,1,1);
						a1b_reg_write(0xa4,0,1,0);
					}
#ifdef ERROR_LOG
					for(i=0;i<error_no;i++)
						{
							printf("ctrl[6]=%02x,delay amount = %02x ",(error_list[i].dqs&0x40)>>6,error_list[i].dqs&0x3f); 
							printf(" original value = %08x, error value = %08x\n",error_list[i].original_value,error_list[i].error_value);
						}
#endif
			break;
			case 0x99:
				return 0;
			break;
		}
	}
	return 0;
}

U_BOOT_CMD(
	a1b_test,	2,	2,	do_a1b_test,
	"a1b_test	- run a1b test\n\r",
	"a1b_test	- run a1b test\n\r"
);

