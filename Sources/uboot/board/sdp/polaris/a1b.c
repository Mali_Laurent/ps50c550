#include <common.h>
#include <malloc.h>
#include <net.h>
#include <asm/io.h>
#include <pci.h>

#undef TEST_MODE



#if defined(CONFIG_PCI) && defined(CONFIG_A1B)

#define PDMACON0	0x30030180
#define PDMASRC0	0x30030184
#define PDMADST0	0x30030188
#define PDMACNT0	0x3003018c

#define PCIBATAPM	0x30030114

#define A1B_VENDOR_ID	0x144d
#define A1B_DEVICE_ID	0xa622

#if defined(CONFIG_JUPITER) || defined(CONFIG_URANUS)
#define bus_to_phys(a)	pci_mem_to_phys((pci_dev_t)dev->priv, a)
#define phys_to_bus(a)	(a)
#else
#define bus_to_phys(a)	pci_mem_to_phys((pci_dev_t)dev->priv, a)
#define phys_to_bus(a)	pci_phys_to_mem((pci_dev_t)dev->priv, a)
#endif

static struct pci_device_id supported[] = {
#ifndef TEST_MODE
	{A1B_VENDOR_ID,A1B_DEVICE_ID},
#else
	{0x10ec,0x8139},
#endif
	{}
};

struct a1b_dev_t
{
	void *memory_addr;
	void *register_addr;
	unsigned int pcibase;
}a1b_dev;

int a1b_initialize(bd_t *bis)
{
	int card_number=0;
	pci_dev_t devno;
	u32 iobase,status;
	if((devno=pci_find_devices(supported,0))<0)
		return 0;
	pci_read_config_dword (devno, PCI_BASE_ADDRESS_0, &iobase);
	printf("A1B Base Address @0x%x\n",iobase);
	a1b_dev.pcibase=iobase&~0xf;
#ifdef TEST_MODE
	iobase=0x20000000;
#else
	iobase=pci_mem_to_phys(devno,iobase);
#endif
	//a1b_dev.memory_addr=(void *)iobase;
	
	a1b_dev.memory_addr=a1b_dev.pcibase;
	printf("a1b memory address = %x\n",a1b_dev.memory_addr);

#ifdef TEST_MODE
	a1b_dev.register_addr=(void *)(0x21000000);
#else
	a1b_dev.register_addr=(void *)(a1b_dev.memory_addr+0x6000000);
#endif
	printf("a1b register address = %x\n",a1b_dev.register_addr);
	pci_write_config_dword (devno,PCI_COMMAND,PCI_COMMAND_MEMORY|PCI_COMMAND_MASTER);
	pci_read_config_dword (devno, PCI_COMMAND, &status);
	if (!(status & PCI_COMMAND_MEMORY))
		printf ("Error: Can not enable MEM access.\n");

	if (!(status & PCI_COMMAND_MASTER))
		printf ("Error: Can not enable Bus Mastering.\n");

	pci_write_config_byte (devno, PCI_LATENCY_TIMER, 0x20);
	*(unsigned int *)PCIBATAPM=a1b_dev.pcibase;

	return card_number;
}

void a1b_reg_write(unsigned int offset,unsigned int bit_pos,unsigned int bit_size,unsigned int value)
{
	unsigned int bit_mask;
	unsigned int *reg;
//	*(unsigned int *)PCIBATAPM=a1b_dev.pcibase;
	reg=(unsigned int *)((u32)a1b_dev.register_addr+(offset<<2));
	value=(value&(~(~0UL<<bit_size)))<<bit_pos;
	bit_mask=~(~0UL<<bit_size)<<bit_pos;
	*(unsigned int *)reg&=~bit_mask;
	*(unsigned int *)reg|=value;
//	*(unsigned int *)PCIBATAPM=0;
}

void a1b_reg_read(unsigned int offset,unsigned int bit_pos,unsigned int bit_size,unsigned int *value)
{
	unsigned int bit_mask;
	unsigned int *reg;
//	*(unsigned int *)PCIBATAPM=a1b_dev.pcibase;
	reg=(unsigned int *)((u32)a1b_dev.register_addr+(offset<<2));
	bit_mask=~(~0UL<<bit_size)<<bit_pos;
	*value=(*(unsigned int *)reg&bit_mask)>>bit_pos;
//	*(unsigned int *)PCIBATAPM=0;
}

void a1b_block_dump(unsigned int block_offset,unsigned int *dump_buffer,unsigned int block_size)
{
	unsigned int *reg;
	unsigned int size;
	reg=(unsigned int *)((u32)a1b_dev.register_addr+(block_offset<<2));
	for(size=0;size<block_size;size++,reg++)
		dump_buffer[size]=*reg;
}

static void _test_memcpy(void *pDst,void *pSrc)
{
	    asm  volatile(					/*for sychronous burst read*/
		"ldmia	%1,  {r2,r3,r4,r5,r6,r7,r9,r10}" "\n\t"
		"stmia	%0,  {r2,r3,r4,r5,r6,r7,r9,r10}" "\n\t"
        	:
                :"r"(pDst),"r"(pSrc)
                :"r2","r3","r4","r5","r6","r7","r9","r10"
		 );
}

void a1b_memory_dump(unsigned int offset,unsigned int *dump_buffer,unsigned int dump_size)
{
#if 0
//	*(unsigned int *)PCIBATAPM=a1b_dev.pcibase;
	*(unsigned int *)PDMASRC0=a1b_dev.pcibase+offset;
	*(unsigned int *)PDMADST0=(unsigned int)dump_buffer;
	dump_size<<=6;
	if(dump_size==0)
		dump_size=16;
	*(unsigned int *)PDMACNT0=dump_size;
	*(unsigned int *)PDMACON0=0x613;
	while(*(unsigned int *)PDMACON0&0x1);
//	*(unsigned int *)PCIBATAPM=0;	
#endif
	unsigned int i;
	volatile unsigned int j;
	unsigned int *src=a1b_dev.pcibase+offset;
	for(i=0;i<dump_size;i++)
	{
		_test_memcpy(dump_buffer,src);
//		for(j=0;j<50;j++);
		dump_buffer+=8;
		src+=8;
	}
}

void a1b_memory_write(unsigned int offset,unsigned int *dump_buffer,unsigned int dump_size)
{
#if 0
//	*(unsigned int *)PCIBATAPM=a1b_dev.pcibase;
	*(unsigned int *)PDMASRC0=(unsigned int)dump_buffer;
	*(unsigned int *)PDMADST0=a1b_dev.pcibase+offset;
	dump_size<<=6;
	
	if(dump_size==0)
		dump_size=16;
	*(unsigned int *)PDMACNT0=dump_size;
	*(unsigned int *)PDMACON0=0x7023;
	while(*(unsigned int *)PDMACON0&0x1);
//	*(unsigned int *)PCIBATAPM=0;
#endif
	unsigned int i;
	volatile unsigned int j;
	unsigned int *dst=a1b_dev.pcibase+offset;
	for(i=0;i<dump_size;i++)
	{
		_test_memcpy(dst,dump_buffer);

#if 0
		for(j=0;j<500;j++);
		for(j=0;j<8;j++)
			if(dst[j]!=dump_buffer[j])
				printf(">>Write error %x\n",dst);
#endif
		dump_buffer+=8;
		dst+=8;
	}

}

#endif

