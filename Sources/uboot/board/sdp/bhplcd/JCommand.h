
#ifndef __J_COMMNAD_H__
#define __J_COMMAND_H__

#ifdef __cplusplus
extern "C" {
#endif

#define JCMD_DEBUG_MODE		1
#undef JCMD_DEBUG_MODE

// define Jade Host Command
#define JCMD_READ		0x00	// read command
#define JCMD_READ_SIZE	   	5	// read command size

#define JCMD_BREAD		0x01	// burst read command	
#define JCMD_BREAD_SIZE	   	6	// burst read command size

#define JCMD_READF		0x02	// read field command
#define JCMD_READF_SIZE		9	// read field command size

#define JCMD_WRITE		0x20	// write command
#define JCMD_WRITE_SIZE	   	9	// write command size

#define JCMD_BWRITE		0x21	// burst write command	
#define JCMD_BWRITE_MAX_SIZE   	126	// burst write command size

#define JCMD_WRITEF		0x22	// write field command
#define JCMD_WRITEF_SIZE	13	// write field command size

#define JCMD_ECHO		0x50	// echo
#define JCMD_ECHO_MAX_SIZE	128	// echo size

#define JCMD_END		0x7F	// end
#define JCMD_END_DATA		"END"	// end string  or 'end'
#define JCMD_END_SIZE		4	// end size

// define Jade Target Ack Data
#define JACK_READ		0x80	// read data
#define JACK_READ_SIZE	   	9	// read data size

#define JACK_BREAD		0x81	// burst read data	
#define JACK_BREAD_MAX_SIZE   	126	// burst read data max size

#define JACK_READF		0x82	// read field data
#define JACK_READF_SIZE		9	// read field data size

#define JACK_ECHO		0xD0	// echo 
#define JACK_ECHO_SIZE		JCMD_ECHO_MAX_SIZE

#define JACK_WRONG_ADDR		0xF0	// wrong address
#define JACK_WRONG_ADDR_SIZE	5	// wrong address size


#ifdef __cplusplus
}
#endif

#endif //__J_COMMAND_H__

