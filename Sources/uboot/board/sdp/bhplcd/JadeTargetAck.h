#ifndef __J_TARGET_ACK_H__
#define __J_TARGET_ACK_H__

#ifdef __cplusplus
extern "C" {
#endif

#ifndef __KERNEL__
#include <sys/mman.h>

#define N_MEM_ZONE 10

struct jade_mem_t {
	off_t base;
	size_t size;
};

extern int jadeInit(int nr_zone, struct jade_mem_t* pJadeMem);
#else
#  ifndef NULL
#  define NULL	0
#  endif
#endif

extern int jadeWrite(unsigned char *rcvBuf, unsigned char size);
extern int jadeBurstWrite(unsigned char *rcvBuf, unsigned char size);
extern int jadeWriteField(unsigned char *rcvBuf, unsigned char size);

extern int jadeReadAck(unsigned char *rcvBuf, unsigned char size);
extern int jadeBurstReadAck(unsigned char *rcvBuf, unsigned char size);
extern int jadeReadFieldAck(unsigned char *rcvBuf, unsigned char size);

extern int jadeEcho(unsigned char *rcvBuf, unsigned char size);
extern int jadeEnd(unsigned char *rcvBuf);

#ifdef __cplusplus
}
#endif

#endif //__J_COMMAND_H__

