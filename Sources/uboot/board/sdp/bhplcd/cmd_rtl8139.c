#include <common.h> // included <stdio.h> <stdlib.h> <string.h> "S5H2200.h" "S5H2200lib.h" "def.h" "board.h" 
#include <command.h>
#include <asm/io.h>

#ifdef CONFIG_RTL8139

enum RTL8129_registers {
	MAC0=0,						/* Ethernet hardware address. */
	MAR0=8,						/* Multicast filter. */
	TxStat0=0x10,				/* Transmit status (Four 32bit registers). */
	TxAddr0=0x20,				/* Tx descriptors (also four 32bit). */
	RxBuf=0x30, RxEarlyCnt=0x34, RxEarlyStatus=0x36,
	ChipCmd=0x37, RxBufPtr=0x38, RxBufAddr=0x3A,
	IntrMask=0x3C, IntrStatus=0x3E,
	TxConfig=0x40, RxConfig=0x44, /* Must enable Tx/Rx before writing. */
	Timer=0x48,					/* A general-purpose counter. */
	RxMissed=0x4C,				/* 24 bits valid, write clears. */
	Cfg9346=0x50, Config0=0x51, Config1=0x52,
	FlashReg=0x54, GPPinData=0x58, GPPinDir=0x59, MII_SMI=0x5A, HltClk=0x5B,
	MultiIntr=0x5C, TxSummary=0x60,
	MII_BMCR=0x62, MII_BMSR=0x64, NWayAdvert=0x66, NWayLPAR=0x68,
	NWayExpansion=0x6A, FlashAccess=0xD4,
};

enum EEPROM_Cmds { EE_WriteCmd=5, EE_ReadCmd=6, EE_EraseCmd=7, };

enum EEPROM_Ctrl_Bits {
	EE_ShiftClk=0x04, EE_ChipSelect=0x88, EE_DataOut=0x02, EE_DataIn=0x01,
	EE_Write0=0x88, EE_Write1=0x8A,
};

/*  EEPROM_Ctrl bits. */
#define EE_SHIFT_CLK	0x04	/* EEPROM shift clock. */
#define EE_CS			0x08	/* EEPROM chip select. */
#define EE_DATA_WRITE	0x02	/* EEPROM chip data in. */
#define EE_WRITE_0		(0x00 | EE_ENB)
#define EE_WRITE_1		(0x02 | EE_ENB)
#define EE_DATA_READ	0x01	/* EEPROM chip data out. */
#define EE_ENB			(0x80 | EE_CS)

/* Delay between EEPROM clock transitions.
   This forces out buffered PCI writes.
*/
#define eeprom_delay(ee_addr)	inb(ee_addr);inb(ee_addr);inb(ee_addr);inb(ee_addr);

#define EE_WRITE_CMD	(5)
#define EE_READ_CMD		(6)
#define EE_ERASE_CMD	(7)

#define MAX_EEPROM_SIZE 256		/* In 16 bit words. */
#define EEPROM_SA_OFFSET	7

static int do_eeprom_cmd(long ioaddr, int cmd, int cmd_len)
{
	unsigned retval = 0;
	long ee_addr = ioaddr + Cfg9346;

	outb(EE_ChipSelect | EE_ShiftClk, ee_addr);

	/* Shift the command bits out. */
	do {
		short dataval = (cmd & (1 << cmd_len)) ? EE_Write1 : EE_Write0;
		outb(dataval, ee_addr);
		eeprom_delay(ee_addr);
		outb(dataval | EE_ShiftClk, ee_addr);
		eeprom_delay(ee_addr);
		retval = (retval << 1) | ((inb(ee_addr) & EE_DataIn) ? 1 : 0);
	} while (--cmd_len >= 0);
	outb(EE_ChipSelect, ee_addr);

	/* Terminate the EEPROM access. */
	outb(0x80, ee_addr);
	outb(0, ee_addr);
	return retval;
}

static int eeprom_busy_poll(long ee_ioaddr)
{
	int i;
	outb(EE_ChipSelect, ee_ioaddr);
	for (i = 0; i < 10000; i++)			/* Typical 2000 ticks */
		if (inb(ee_ioaddr) & EE_DataIn)
			break;
	return i;
}


static int read_eeprom(long ioaddr, int location, int addr_len)
{
	return do_eeprom_cmd(ioaddr, ((EE_ReadCmd << addr_len) | location) << 16,
						 3 + addr_len + 16) & 0xffff;
}

static int eeprom_addr_len(long ioaddr)
{
	int testval = do_eeprom_cmd(ioaddr,
								(((EE_READ_CMD<<8)|0) << 16) | 0xffff,
								3 + 8 + 16);
	int addr_size = (read_eeprom(ioaddr, 0, 6) & 0xffff) == 0x8129 ? 6 : 8;

	return addr_size;
}


static void write_eeprom(long ioaddr, int index, int value, int addr_len)
{
	long ee_ioaddr = ioaddr + Cfg9346;
	int i;

	/* Poll for previous op finished. */
	eeprom_busy_poll(ee_ioaddr);

	/* Enable programming modes. */
	do_eeprom_cmd(ioaddr, (0x4f << (addr_len-4)), 3 + addr_len);
	/* Do the actual write. */ 
	do_eeprom_cmd(ioaddr,
				  (((EE_WriteCmd<<addr_len) | index) << 16) | (value & 0xffff),
				  3 + addr_len + 16);
	i = eeprom_busy_poll(ee_ioaddr);

	/* Disable programming.  Note: this command is not instantaneous, so
	   we check for busy before the next op. */
	do_eeprom_cmd(ioaddr, (0x40 << (addr_len-4)), 3 + addr_len);
	eeprom_busy_poll(ee_ioaddr);
}

static int do_update(long ioaddr, unsigned short *ee_values, int index,
					 char *field_name, int new_value, int ee_addr_len)
{
	if (ee_values[index] == new_value)
		return 0;
		printf("Writing new %s entry 0x%4.4x to offset %d.\n",
			   field_name, new_value, index);
		write_eeprom(ioaddr, index, new_value, ee_addr_len);
		printf("  New %s value at offset %d is %4.4x.\n",
			   field_name, index, read_eeprom(ioaddr, index, ee_addr_len));
	ee_values[index] = new_value;
	return 1;
}

#define ioaddr	0x600

#define MAX_EEPROM_SIZE 256		/* In 16 bit words. */
#define EEPROM_SA_OFFSET	7
static unsigned short eeprom_contents[MAX_EEPROM_SIZE];
static unsigned short new_ee_contents[MAX_EEPROM_SIZE];


unsigned short write_eeprom_contents[]={0x8129,0x10ec,0x8139,0x10ec,0x8139,0x4020,0xe512,0x0e00,0x3d2e,0x325f,0x4d10,0xf7c2,0x8801,0x03b9,0x60f4,0x071a,0xdfa3,0x9836,0xdfa3,0x9836,0x03b9,0x60f4,0x1a1a,0x1a1a,0x0000,0xa2d1,0x0000,0x0000,0x0000,0x0000,0x0000,0x2000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000,0x0000};
#if 0
unsigned short write_eeprom_contents[MAX_EEPROM_SIZE]=
			{0x8129, 0x10ec, 0x8139, 0x10ec, 0x8139, 0x4020, 0xe110, 0x2301,
			0x6745, 0xab89, 0x4d10, 0xf7c2, 0x8001, 0xb388, 0x58fa, 0x0708,
			0xd843, 0xa438, 0xd843, 0xa438, 0xd843, 0xa438, 0xd843, 0xa438};
#endif
static void hwaddr_media_modify(int *hwaddr)
{
	unsigned char new_hwaddr[6];
	int i,new_media;
	for (i = 0; i < 6; i++)
		new_hwaddr[i] = hwaddr[i];
	for (i = 0; i < 3; i++)
		write_eeprom_contents[i + EEPROM_SA_OFFSET] =(new_hwaddr[i*2+1]<<8) + new_hwaddr[i*2];
	new_media = write_eeprom_contents[6] & ~0x0031;
	new_media |= 0x0020;
//	write_eeprom_contents[6] = new_media;
}

int do_rtl8139_eeprom(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ee_addr_len = 6, eeprom_size = 64;
	int i;
	eeprom_size = 1 << ee_addr_len;
#if 1
	for (i = 0; i < eeprom_size; i++)
		eeprom_contents[i] = read_eeprom(ioaddr, i, ee_addr_len);
	memcpy(new_ee_contents, eeprom_contents, eeprom_size << 1);
	printf("printf content\n");
	for (i = 0; i < eeprom_size; i++)
		printf("%04x\t",eeprom_contents[i]);
	printf("\n");
#else
	for (i = 0; i < eeprom_size; i++)
		write_eeprom(ioaddr, i, write_eeprom_contents[i],ee_addr_len);
//write_eeprom(ioaddr, i, 0xffff,ee_addr_len);
#endif
	return 0;
}

int do_bayhill_eth_addr(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int hwaddr[6];
	int i,j,index;
	char buffer[3];
	int ee_addr_len = 6, eeprom_size = 64;
	eeprom_size = 1 << ee_addr_len;

	if(argv[1][2]!=':'||argv[1][2]!=':'||argv[1][5]!=':'||argv[1][8]!=':'||argv[1][11]!=':'||argv[1][14]!=':')
	{
		printf("retry..\n");
		return -1;
	}

	buffer[2]='\0';
	for(i=0,index=0,j=0;i<strlen(argv[1]);i++)
	{
		if(argv[1][i]!=':')
		{
			buffer[j++]=argv[1][i];
		}
		else
		{
			j=0;
			hwaddr[index++]=simple_strtoul(buffer,NULL,16);
		}
	}
	hwaddr[index]=simple_strtoul(buffer,NULL,16);
	hwaddr_media_modify(hwaddr);
	for (i = 0; i < eeprom_size; i++)
	{
		write_eeprom(ioaddr, i, write_eeprom_contents[i],ee_addr_len);
		printf("Writing new value entry 0x%4.4x to offset %x.  ", write_eeprom_contents[i], i);
		printf("  New value at offset %x is %4.4x.\n", i, read_eeprom(ioaddr, i, ee_addr_len));
	}
	return 1;
}

int do_bayhill_eth_init(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int ee_addr_len = 6, eeprom_size = 64;
	int i;
	eeprom_size = 1 << ee_addr_len;
	for (i = 0; i < eeprom_size; i++)
	{
		write_eeprom(ioaddr, i,0xffff,ee_addr_len);
		printf("Writing new value entry 0x%4.4x to offset %x.  ", 0xffff, i);
		printf("  New value at offset %x is %4.4x.\n", i, read_eeprom(ioaddr, i, ee_addr_len));
	}
	return 0;
}

int do_bayhill_eth_setup(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	int hwaddr[6];
	int i,j,index;
	char buffer[3];
	int ee_addr_len = 6, eeprom_size = 64;
	eeprom_size = 1 << ee_addr_len;

	for (i = 0; i < eeprom_size; i++)
	{
		write_eeprom(ioaddr, i, write_eeprom_contents[i],ee_addr_len);
		printf("Writing new value entry 0x%4.4x to offset %x.  ", write_eeprom_contents[i], i);
		printf("  New value at offset %x is %4.4x.\n", i, read_eeprom(ioaddr, i, ee_addr_len));
	}
	return 1;
}

U_BOOT_CMD(
	rtl8139_eeprom,2,1,do_rtl8139_eeprom,
	"osd_test	- run osd test\n\r",
	"osd_test	- run osd test\n\r"
);

U_BOOT_CMD(
	bayhill_eth_addr,2,1,do_bayhill_eth_addr,
	"osd_test	- run osd test\n\r",
	"osd_test	- run osd test\n\r"
);

U_BOOT_CMD(
	bayhill_eth_setup,2,1,do_bayhill_eth_setup,
	"osd_test	- run osd test\n\r",
	"osd_test	- run osd test\n\r"
);

U_BOOT_CMD(
	bayhill_eth_init,2,1,do_bayhill_eth_init,
	"osd_test	- run osd test\n\r",
	"osd_test	- run osd test\n\r"
);

#endif
