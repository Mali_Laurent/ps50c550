
#ifndef __DPROTOCOL_H__
#define __DPROTOCOL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <linux/types.h>

#define DPRO_DEBUG_MODE		1
#undef DPRO_DEBUG_MODE

// define about jade debug protocol
// packet
#define DPRO_DATA_MAX	128	// size = PSize + 1 -> PSize : 0 ~ 127
#define DPRO_PACK_MAX	166	//packet max size 128(Data)+32(MSB)+info(6) = 166 byte

#define DPRO_PACK_S	0xF7  	//packet Start
#define DPRO_PACK_E	0xFD	//packet stop
#define DPRO_PACK_MSB	0xB0	//packet MSB Byte 0xB0 ~ 0xBF
#define DPRO_PACK_EOD	0xDE	//packet End of data
// Ack
#define DPRO_ACK_ACK	0xF5	//Ack
#define DPRO_ACK_RETRY	0xF6	//Retry
#define DPRO_ACK_TIMEOUT	0xF9 // Timeout

#define DPRO_MAXBUF_SIZE	255

#define DPRO_OK			0
#define DPRO_ERROR 		-1

#define WAIT_ACK_TIME		2  // second
#define MAX_RETRY		3

// Jade Message Queue Name
#define DE_ACKQ_NAME		"/dproAckQ"
#define DE_RCVQ_NAME		"/dproRcvQ"

int jadeDebugSend(unsigned char* data, unsigned char size);
int jadeDebugRcv(unsigned char* data, unsigned char* size, unsigned int waitSec);
int jadeDebugInit(char* dproDevice,  int baudrate);
void jadeDebugExit(void);


#ifdef __cplusplus
}
#endif

#endif //__DPROTOCOL_H__

