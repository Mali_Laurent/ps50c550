/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2002
 * David Mueller, ELSOFT AG, <d.mueller@elsoft.ch>
 *
 * (C) Copyright 2005
 * JinHua Luo, GuangDong Linux Center, <luo.jinhua@gd-linux.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/sdp76.h>

DECLARE_GLOBAL_DATA_PTR;

/* ij.jang : add arm926 MMU support */
#if defined(USE_926EJS_MMU_TRANS)
#include <arm926ejs_mmu.h>

extern unsigned int create_mmu_mapping (struct mmu_mapinfo*, int);

static struct mmu_mapinfo mmu_fmap[] = {
	/* SMC Bank #0~2 */
	{ 0x00000000, PHYS_SDRAM_1, FLD_SECTION, 0, 0},
	/* DRAM bank*/
	{ PHYS_SDRAM_1, PHYS_SDRAM_1_SIZE-CFG_MMU_NCNB_SIZE, FLD_SECTION | FLD_C | FLD_B , 0, 0 },
	/* DRAM NCNB Area */
	{ PHYS_SDRAM_1+PHYS_SDRAM_1_SIZE-CFG_MMU_NCNB_SIZE, CFG_MMU_NCNB_SIZE, FLD_SECTION, 0, 0 },
	/* REST */
	{ PHYS_SDRAM_1+PHYS_SDRAM_1_SIZE, 0xE0000000 - PHYS_SDRAM_1_SIZE, FLD_SECTION, 0, 0 },
};

#endif	/* USE_926EJS_MMU_TRANS */

static inline void delay (unsigned long loops)
{
	__asm__ volatile ("1:\n"
			  "subs %0, %1, #1\n"
			  "bne 1b":"=r" (loops):"0" (loops));
}

#if defined(CONFIG_USB_OHCI)
void bhplcd_usb_init(void)
{
	/* using crystal, TX_VREF_TUNE 3.75% */
	S4LX_CLOCK_POWER *pwm = S4LX_GetBase_CLOCK_POWER();
	S4LX_REG32 val = pwm->USB_PCI_SET;
	val &= ~(USB_PCI_TXPREEMP | USB_PCI_TXVREF | USB_PCI_REFCLKSEL);
	val |= (0x1 << 17) | (0xB << 19) | (0x1 << 25);
	pwm->USB_PCI_SET = val;
}
#endif

/*
 * Miscellaneous platform dependent initialisations
 */

int board_init (void)
{
	
	gd->bd->bi_arch_number = MACH_TYPE_BHPLCD;

	gd->bd->bi_boot_params = PHYS_SDRAM_1+0x100;

	icache_enable();
#if defined(USE_926EJS_MMU_TRANS)
	create_mmu_mapping (mmu_fmap, ARRAY_SIZE(mmu_fmap));
#endif
#if defined(CONFIG_USB_OHCI)
	bhplcd_usb_init();
#endif
	return 0;
}

int dram_init (void)
{
	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size = PHYS_SDRAM_1_SIZE;

	return 0;
}

int misc_init_r(void)
{
#if defined(CONFIG_PCI)
	        pci_init();
#endif
        return 0;
}

#if defined(CONFIG_BBML) || defined(CONFIG_TBML) || defined(CFG_ENV_IS_IS_ONENAND)

#if !defined(REG_PUT)
#define REG_PUT(addr, data)	((*(volatile unsigned int*)(addr)) = (data))
#endif

/* 02,Aug,2007 ij.jang : added for sync burst read */
/* sh.kim - start */
void onenand_sync_set(int vol)
{
#if defined(CFG_SDP76_HCLK80)
	/* SMC register bank address */
	volatile unsigned int *smc = (volatile unsigned int*)0x30020000;
	ulong	onenand_base;
	
	if(vol == 0)
	{
		onenand_base = CFG_ONENAND_BASE;
		smc += 14;	/* 0x38 */
	}
	else 
	{
		onenand_base = CFG_ONENAND_BASE1;
		smc += 7;	/* 0x1C */
	}

	unsigned int bank = (onenand_base >> 26);
	if(!(bank == 0 || bank == 1))
	{
		puts ("CFG_ONENAND_BASE wrong!!\n");
		return;
	}

	/* Onenand SYSCONF1 register */
	writew (0xC6E0, onenand_base + 0x1E442);

	/* Sync burst read, 16 word burst len */
	writel (0x04, smc);	/* CYR */
	writel (0x0A, smc+1);	/* WST1 */
	writel (0x01, smc+2);	/* WST2 */
	writel (0x00, smc+3);	/* WSTOEN */
	writel (0x00, smc+4);	/* WSTWEN */
	writel (0x61, smc+5);	/* CR */

#else
	if(vol == 0)
	{
		writew (0x40E0, CFG_ONENAND_BASE + 0x1E442);
	}
	else 
	{
		writew (0x40E0, CFG_ONENAND_BASE1 + 0x1E442);
	}
#endif
}
/* sh.kim - end */

#endif
