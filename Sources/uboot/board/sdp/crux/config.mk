#
# u-boot-1.1.3 port for S5H2110
#
# D-TV Development team, SYS.LSI
# (C)Copyright 2005 Samsung Electronics, Inc.
#
# modification history
# --------------------
# 16, Nov, 2005 : ij.jang created.
#
#

TEXT_BASE = 0x23A00000
USE_TBML = y
#USE_BBML = y
