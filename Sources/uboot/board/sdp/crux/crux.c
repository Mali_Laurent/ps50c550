/*
 * (C) Copyright 2002
 * Sysgo Real-Time Solutions, GmbH <www.elinos.com>
 * Marius Groeger <mgroeger@sysgo.de>
 *
 * (C) Copyright 2002
 * David Mueller, ELSOFT AG, <d.mueller@elsoft.ch>
 *
 * (C) Copyright 2005
 * JinHua Luo, GuangDong Linux Center, <luo.jinhua@gd-linux.com>
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

#include <common.h>
#include <asm/io.h>
#include <asm/arch/sdp76.h>

DECLARE_GLOBAL_DATA_PTR;

/* ij.jang : add arm926 MMU support */
#if defined(USE_926EJS_MMU_TRANS)
#include <arm926ejs_mmu.h>

extern unsigned int create_mmu_mapping (struct mmu_mapinfo*, int);

static struct mmu_mapinfo mmu_fmap[] = {
	/* SMC Bank #0~2 */
	{ 0x00000000, 0x20000000, FLD_SECTION, 0, 0},
	
	/* DRAM bank hole head*/
	{ 0x20000000, PHYS_SDRAM_1 - 0x20000000, FLD_SECTION, 0, 0},
	
	/* DRAM bank CB */
	{ PHYS_SDRAM_1, PHYS_SDRAM_1_SIZE-CFG_MMU_NCNB_SIZE, FLD_SECTION | FLD_C | FLD_B , 0, 0 },
	
	/* DRAM NCNB Area */
	{ PHYS_SDRAM_1+PHYS_SDRAM_1_SIZE-CFG_MMU_NCNB_SIZE, CFG_MMU_NCNB_SIZE, FLD_SECTION, 0, 0 },
	
	/* DRAM bank hole tail */
	{ PHYS_SDRAM_1+PHYS_SDRAM_1_SIZE, 0x30000000-(PHYS_SDRAM_1+PHYS_SDRAM_1_SIZE), FLD_SECTION, 0, 0},
	
	/* REST */
	{ 0x30000000, 0xD0000000, FLD_SECTION, 0, 0},
};

#endif	/* USE_926EJS_MMU_TRANS */

static inline void delay (unsigned long loops)
{
	__asm__ volatile ("1:\n"
			  "subs %0, %1, #1\n"
			  "bne 1b":"=r" (loops):"0" (loops));
}

#if defined(CONFIG_USB_OHCI)
void crux_usb_init(void)
{
	/* using crystal, TX_VREF_TUNE 3.75% */
	S4LX_CLOCK_POWER *pwm = S4LX_GetBase_CLOCK_POWER();
	S4LX_REG32 val = pwm->USB_PCI_SET;
	val &= ~(USB_PCI_TXPREEMP | USB_PCI_TXVREF | USB_PCI_REFCLKSEL);
	val |= (0x1 << 17) | (0xB << 19);
	pwm->USB_PCI_SET = val;
}
#endif

/*
 * Miscellaneous platform dependent initialisations
 */

int board_init (void)
{
	
	gd->bd->bi_arch_number = MACH_TYPE_CRUX;

	gd->bd->bi_boot_params = PHYS_SDRAM_1+0x100;

	icache_enable();
#if defined(USE_926EJS_MMU_TRANS)
	if (create_mmu_mapping (mmu_fmap, ARRAY_SIZE(mmu_fmap))) {
		printf ("MMU mapping table wrong!\n");
	}
#endif
#if defined(CONFIG_USB_OHCI)
	crux_usb_init();
#endif
	return 0;
}

int dram_init (void)
{
	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size = PHYS_SDRAM_1_SIZE;

	return 0;
}

int misc_init_r(void)
{
#if defined(CONFIG_PCI)
	        pci_init();
#endif
        return 0;
}

#if defined(CONFIG_BBML) || defined(CONFIG_TBML) || defined(CFG_ENV_IS_IN_ONENAND)

#if !defined(REG_PUT)
#define REG_PUT(addr, data)	((*(volatile unsigned int*)(addr)) = (data))
#endif

/* 02,Aug,2007 ij.jang : added for sync burst read */
void onenand_sync_set(void)
{
#if defined(CFG_SDP76_HCLK80)
	/* SMC register bank address */
	volatile unsigned int *smc = (volatile unsigned int*)0x30020000;
	unsigned int bank = (CFG_ONENAND_BASE >> 26);

	switch (bank)
	{
		case 0:
			smc += 14;	/* 0x38 */
			break;
		case 1:
			smc += 7;	/* 0x1C */
			break;
		default:
			puts ("CFG_ONENAND_BASE wrong!!\n");
			return;
	}

	/* Onenand SYSCONF1 register */
	writew (0xC6E0, CFG_ONENAND_BASE + 0x1E442);

	/* Sync burst read, 16 word burst len */
	writel (0x04, smc);	/* CYR */
	writel (0x0A, smc+1);	/* WST1 */
	writel (0x01, smc+2);	/* WST2 */
	writel (0x00, smc+3);	/* WSTOEN */
	writel (0x00, smc+4);	/* WSTWEN */
	writel (0x61, smc+5);	/* CR */
	
	writel (0x02, rSMCCLKSTOP);
	writel (0x01, rSMCCLKSTOP);
	writel (0x00, rSMCCLKSTOP);
	writel (1<<bank, rSMCSYNCEN);
#else
	/* Onenand SYSCONF1 register */
	writew (0x40E0, CFG_ONENAND_BASE + 0x1E442);
#endif
}

#endif
