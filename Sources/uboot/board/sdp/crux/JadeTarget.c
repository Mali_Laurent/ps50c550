#ifndef __KERNEL__
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

#include <Dprotocol.h>
#include <JCommand.h>
#else //__KERNEL__

#include <common.h>
#include <command.h>
#include "Dprotocol.h"
#include "JCommand.h"

#endif //__KERNEL__

#include "JadeTargetAck.h"

#define ENV_FILE    "jadeTarget.env"
#define DPRO_DEVICE "/dev/ttySD1"

#ifndef __KERNEL__
int main(int argc, char* argv[])
#else //__KERNEL__
int do_jade(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
#endif //__KERNEL__
{

	int retVal;

	int baudrate;	
	char dPortName[] = DPRO_DEVICE;

        unsigned char rcvBuf[255];
        unsigned char size;
#ifndef __KERNEL__
	struct jade_mem_t memZone[N_MEM_ZONE] = {
			{0x30000000, 0x00800000},
			{0x23600000, 0x04A00000},
			};


	if(jadeInit(2, memZone) < 0){
		printf("jadeInit Failed\n");
		return 0;
	}

#else //__KERNEL__
	printf("jade mode is entered\n");
#endif //__KERNEL__

        if(jadeDebugInit(DPRO_DEVICE,115200) < 0){
                printf("jadeDebugInit failed\n");
		return 0;
	} 

        while(1){

                if(jadeDebugRcv(rcvBuf, &size, 0) < 0){
                        printf("main jadeDebugRcv failed\n");
			break;
                }
		
		switch(*rcvBuf & 0xFF){
			case JCMD_READ:
				if(jadeReadAck(rcvBuf, size) != JACK_READ_SIZE)
					printf("READ Command is failed\n");
				break;
			case JCMD_BREAD:
				if(jadeBurstReadAck(rcvBuf, size) < 0) 
					printf("Burst READ Command is failed\n");
				break;
			case JCMD_READF:
				if(jadeReadFieldAck(rcvBuf, size) != JACK_READF_SIZE)
					printf("READ Field Command is failed\n");
				break;
			case JCMD_WRITE:
				if(jadeWrite(rcvBuf, size) != JCMD_WRITE_SIZE)
					printf("Write Command is failed\n");
				break;
			case JCMD_BWRITE:
				if(jadeBurstWrite(rcvBuf, size) < 0) 
					printf("Burst Write Command is failed\n");
				break;
			case JCMD_WRITEF:
				if(jadeWriteField(rcvBuf, size)!= JCMD_WRITEF_SIZE)
					printf("Write Field Command is failed %d\n",retVal);
				break;
			case JCMD_ECHO:
				if(size != jadeEcho(rcvBuf, size))		
					printf("Echo Command is failed\n");
				break;
			case JCMD_END:
				if(jadeEnd(rcvBuf) == JCMD_END_SIZE)
					goto exit_jadeTaget;
				break;
			default:
				break;
		}
        }

exit_jadeTaget:
       	jadeDebugExit();

        printf("Jade Target application end\n\n\n\n");

        return 0;

}

#ifdef __KERNEL__
U_BOOT_CMD(
    jade,     CFG_MAXARGS,     0,      do_jade,
        "jade     -  \n",
        "Enter Jade commution mode \n"
);
#endif //__KERNEL__


