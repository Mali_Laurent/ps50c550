/**
 * u-boot-1.1.3 port for S5H2111
 *
 * D-TV Development team, SYS.LSI
 * (C)Copyright 2005 Samsung Electronics, Inc.
 */
/**
 * lowlevel_init.S : Initialize board-specific functions.
 * 
 * @author   : Ikjoon Jang (ij.jang@samsung.com)
 * @see      : S5H2111 User's manual
 * @version  : $Revision$
 *
 * modification history
 * --------------------
 * 30,July,2007 : ij.jang port to u-boot 1.1.6
 * 1/26/2006 : ij.jang copy from s5h2111
 * 11/15/2005 : ij.jang created.
 *
 */
 
#include <config.h>
#include <version.h>
#include <asm/arch/s5h2111.h>

.globl lowlevel_init
lowlevel_init:
	/* backup the lr */
	mov		r6, lr
	
	/* init static memory controller */
	bl		init_smc

	mov		r1, #0x250
system_stable1:
	subs	r1, r1, #1
	bpl		system_stable1

	/* init DDR controller */
	bl		init_ddr
	
	mov		r1, #0x250
system_stable2:
	subs	r1, r1, #0x1
	bpl		system_stable2

	/* restore lr */
	mov		lr, r6
	mov		pc, lr

/**********************************************************************
 * register-set routine
 *   params
 *       r0 : base address, r1 : offset, r2: value
 *********************************************************************/
reg_set:
	add		r1, r1, r0
	str		r2, [r1]		
	nop
	nop
	nop
	nop
	mov		pc, lr

/**********************************************************************
 * Initialize the SMC
 **********************************************************************/ 
init_smc:
	/* backup lr */
	mov		r5, lr

	/* set SMC base address */
	ldr		r0, =SMC_BASE

	/* bank #0 KFG5616U1M OneNAND*/
	mov	r1,	#oSMC_SMBIDCYR2		@0038
	mov	r2,	#0x04
	bl	reg_set
	
	mov	r1,	#oSMC_SMBWST1R2		@003C
	mov	r2,	#0x09
	bl	reg_set
	
	mov	r1,	#oSMC_SMBWST2R2
	mov	r2,	#0x02
	bl	reg_set

	mov	r1,	#oSMC_SMBWSTOENR2	@0044
	mov	r2,	#0x02
	bl	reg_set

	mov	r1,	#oSMC_SMBWSTWENR2	@0048
	mov	r2,	#0x02
	bl	reg_set

	mov	r1,	#oSMC_SMBCR2		@004C
	mov	r2,	#0x41
	bl	reg_set

	mov	r1,	#oSMC_SMBSR2		@0050
	mov	r2,	#0x00
	bl	reg_set

	/* bank #1 Intel nor flash */
	mov	r1, #oSMC_SMBIDCYR1
	mov	r2, #0x08
	bl	reg_set

	mov	r1, #oSMC_SMBWST1R1
	mov	r2, #0x07
	bl	reg_set

	mov	r1, #oSMC_SMBWST2R1
	mov	r2, #0x07
	bl	reg_set

	mov	r1, #oSMC_SMBWSTOENR1
	mov	r2, #0x00
	bl    	reg_set

	mov	r1, #oSMC_SMBWSTWENR1
	mov	r2, #0x00
	bl    	reg_set

	mov	r1, #oSMC_SMBCR1
	mov	r2, #0x41
	bl	reg_set

	mov	r1, #oSMC_SMBSR1
	mov	r2, #0x00
	bl	reg_set

	/* bank #2 CS8900A */
	mov	r1, #oSMC_SMBIDCYR0
	mov	r2, #0x04
	bl	reg_set

	mov	r1, #oSMC_SMBWST1R0
	mov	r2, #0x0D
	bl	reg_set

	mov	r1, #oSMC_SMBWST2R0
	mov	r2, #0x09
	bl	reg_set

	mov	r1, #oSMC_SMBWSTOENR0
	mov	r2, #0x02
	bl    	reg_set

	mov	r1, #oSMC_SMBWSTWENR0
	mov	r2, #0x02
	bl    	reg_set

	mov	r1, #oSMC_SMBCR0
	mov	r2, #0x41
	bl	reg_set

	mov	r1, #oSMC_SMBSR0
	mov	r2, #0x00
	bl	reg_set

	/* smc common */
	mov	r1, #oSMC_SMBEWS		@0x00E0
	mov	r2, #0x01
	bl	reg_set

	/* OneNAND sync clock to bank #0, stop mode */
	mov	r1, #oSMC_SMCCLKSTOP
	mov	r2, #2
	bl	reg_set
	mov	r2, #1
	bl	reg_set

	mov	r1, #oSMC_SMCSYNCEN
	mov	r2, #1
	bl	reg_set

#if !defined(CONFIG_SATURN3)
	/* flash h/w lock control GPIO E port */
	ldr		r0, =rGPIO_SEL0
	ldr		r2, =0x1d6
	str		r2, [r0]
#endif
	nop
	nop
	nop
	nop

	mov		lr, r5
	mov		pc, lr

/**********************************************************************
 * Initialize the DDR controller
 **********************************************************************/
init_ddr:
	/* backup lr */
	mov		r5, lr

	ldr		r0, =DDRC_BASE
	
	mov 	r1, #oDDRC_TR
	ldr 	r2, =0xF251400
	bl 		reg_set

	mov 	r1, #oDDRC_MMU_CON
	ldr 	r2, =0x4F00208
	bl 		reg_set

	mov 	r1, #oDDRC_DCR1
	mov 	r2, #0x440000
	bl 		reg_set

	mov 	r1, #oDDRC_DCR3
	ldr	 	r2,=0x05050000
	bl 		reg_set

	mov 	r1, #oDDRC_DCR4
	ldr	 	r2,=0x05050000
	bl 		reg_set

	mov 	r1, #oDDRC_DCR5
	mov 	r2, #0x00
	bl 		reg_set

	mov 	r1, #oDDRC_DCR6
	ldr	 	r2,=0x05050000
	bl 		reg_set

	mov 	r1, #oDDRC_DCR7
	ldr	 	r2,=0x05050000
	bl 		reg_set

	mov 	r1, #oDDRC_DCR8
	mov 	r2, #0x00
	bl 		reg_set

	mov 	r1, #oDDRC_SSTLPADC
	ldr 	r2, =0x4444
	bl 		reg_set

	mov 	r1, #oDDRC_PCR
	mov 	r2, #0x0A
	bl 		reg_set

	mov 	r1, #oDDRC_PCR
	ldr		r2, =0x20008
	bl 		reg_set

	mov 	r1, #oDDRC_PCR
	ldr 	r2, =0x1638
	bl 		reg_set

	mov 	r1, #oDDRC_PCR
	mov 	r2, #0x0A
	bl 		reg_set

	mov 	r1, #oDDRC_PCR
	mov 	r2, #0x09
	bl 		reg_set

	mov 	r1, #oDDRC_PCR
	mov 	r2, #0x09
	bl 		reg_set

	mov 	r1, #oDDRC_PCR
	ldr 	r2, =0x40638
	bl 		reg_set

	ldr		r1, =DDRC_ADC
	ldr		r2,=0x2
	str		r2, [r1]
	
	mov		lr, r5
	mov		pc, lr

