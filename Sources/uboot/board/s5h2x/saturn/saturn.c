/**
 * u-boot-1.1.3 port for aspen jupiter
 *
 * D-TV Development team, SYS.LSI
 * (C)Copyright 2005 Samsung Electronics, Inc.
 */
/**
 * jupiter.c : exports board-specific functions.
 * 
 * @author   : Ikjoon Jang (ij.jang@samsung.com)
 * @see      : S5H2111 User's manual
 * @version  : $Revision$
 *
 * Modification history
 * ---------------------
 * 30, July, 2007 : ij.jang port to u-boot 1.1.6
 * 6, Mar, 2006 : ij.jang fixed the GPIO initial value
 * 1, Jan, 2006 : ij.jang copy from jupiter
 * 11/15/2005 - ij.jang created.
 *
 */

#include <common.h>
#include <asm/arch/s5h2111.h>
#include <arm926ejs_mmu.h>
#include <asm/mach-types.h>

#if defined(USE_926EJS_MMU_TRANS)
extern unsigned int create_mmu_mapping(struct mmu_mapinfo*, int);
/* first level table :
   	use physical address
 	total size should be 4GB
	minimum size per a section is 1MB
 */

static struct mmu_mapinfo mmu_fmap[] = {
	/* SMC Bank #0~2 */
	{ 0x00000000, 0x0C000000, FLD_SECTION, 0, 0 },
	/* SMC hole */
	{ 0x0C000000, 0x14000000, FLD_SECTION, 0, 0 },
	/* DRAM area 64MB*/
	{ 0x20000000, 0x04000000, FLD_SECTION | FLD_C | FLD_B, 0, 0 },
	/* DRAM area hole */
	{ 0x24000000, 0x0C000000, FLD_SECTION, 0, 0 },
	/* Internal registers */
	{ 0x30000000, 0x10000000, FLD_SECTION, 0, 0 },
	/* Rest */
	{ 0x40000000, 0xC0000000, FLD_SECTION, 0, 0 },
};
#endif

/**
 * board_init()
 * 		: Initialize jupiter board specific functions
 * @author		ij.jang
 * @return		0 on success, other value
 * @version		$Revison$
 */
int board_init (void)
{
	DECLARE_GLOBAL_DATA_PTR;
	
	REG_PUT(rSOFTRES, 0xFFFFFFFF);
	
	/* Iniialize GPIO control registers */
	REG_PUT(rGPIO_SEL0, 0x4);	/* pins for GPIO  are used as GPIO pin */
	
	REG_PUT(rGPIO0_DAT, 0x00);
	REG_PUT(rGPIO0_CON, 0x00);
	
	REG_PUT(rGPIO1_DAT, 0x00);
	REG_PUT(rGPIO1_CON, 0x00);
	
	REG_PUT(rGPIO2_DAT, 0x00);
	REG_PUT(rGPIO2_CON, 0x00);
	
	REG_PUT(rGPIO3_DAT, 0x00);
	REG_PUT(rGPIO3_CON, 0x00);
	
	REG_PUT(rGPIO4_DAT, 0x01);	/* high : FLASH VPEN */
	REG_PUT(rGPIO4_CON, 0x00);  /* output */

	/* need add codes for GPIO5~9 ?? */
	
	REG_PUT(rEXT_INT_CON, 0x00);
	REG_PUT(rPIN_SEL_CON, 0x00);
	
	REG_PUT(rSOFTRES, 0x7FFFFFFF);	
	
	/* SAMSUNG JUPITER board architecture information for arm linux kernel. */

	/* ij.jang 051025 : this value should defined in linux/arch/arm/tools/mach-types */
	gd->bd->bi_arch_number = MACH_TYPE_SATURN;

	/* address of boot parameters = RAM Base Address + 0x100 */
	gd->bd->bi_boot_params = PHYS_SDRAM_1 + 0x100;

#if defined(USE_926EJS_MMU_TRANS)
	/* create mmu table */
	create_mmu_mapping(mmu_fmap, ARRAY_SIZE(mmu_fmap));
#endif
	icache_enable();

	return 0;
}

/**
 * dram_init() : Initialize the dram information of gd
 * @author		ij.jang
 * @version		$Revison$
 */
int dram_init (void)
{
	DECLARE_GLOBAL_DATA_PTR;

	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size = PHYS_SDRAM_1_SIZE;

	return 0;
}

#if defined(CONFIG_BBML) || defined(CONFIG_TBML) || defined(CFG_ENV_IS_IN_ONENAND)
/* 02,Aug,2007 ij.jang : added for sync burst read */
void onenand_sync_set(void)
{
	*(volatile unsigned short*)(CFG_ONENAND_BASE + 0x1E442) =
		(unsigned short)0xC6E0;

	/* always bank 0 */
	/* Sync burst read, 16 word burst len */
	REG_PUT(rSMBIDCYR2, 0x4);
	REG_PUT(rSMBWST1R2, 0xA);
	REG_PUT(rSMBWST2R2, 0x1);
	REG_PUT(rSMBWSTOENR2, 0x0);
	REG_PUT(rSMBWSTWENR2, 0x0);
	REG_PUT(rSMBCR2, 0x61);

	REG_PUT(rSMCCLKSTOP, 0x2);
	REG_PUT(rSMCCLKSTOP, 0x1);
	REG_PUT(rSMCCLKSTOP, 0x0);
	REG_PUT(rSMCSYNCEN, 0x1);
}
#endif
