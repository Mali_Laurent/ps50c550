#include <common.h>
#include <asm/arch/s5h2150.h>

#ifdef CONFIG_PCI

#include <pci.h>

#define PCI_MEM_ADDR			0x40000000
#define PCI_IO_ADDR 			0x00000000
#define PCI_MEM_BAR0			0x20000000
#define PCI_MEM_BAR1			(0x20000000)
#define PCI_MEM_BAR2			(0x20000000 + 0x12000000)
#define PCI_MEM_SIZE			0x10000000
#define PCI_IO_SIZE			0x02000000
#define AHB_PCI_MEM_BASE		0x40000000
#define AHB_PCI_CONFIG_TYPE0_BASE	0x50000000
#define AHB_PCI_CONFIG_TYPE1_BASE	0x54000000
#define AHB_PCI_IO_BASE			0x5c000000

#define  AHB_ADDR_PCI_CFG0( device, functn, offset )			\
			(AHB_PCI_CONFIG_TYPE0_BASE | ((device)<<11) | \
			((	  (functn)	  &0x07)<< 8) | \
			((	  (offset)	  &0xFF)	)	)
#define  AHB_ADDR_PCI_CFG1( bus, device, functn, offset )		\
			(AHB_PCI_CONFIG_TYPE1_BASE | (((bus)&0xFF)<<16) | \
			((	  (device)	  &0x1F)<<11) |\
			((	  (functn)	  &0x07)<< 8) | \
			((	  (offset)	  &0xFF)	)	)

static struct pci_config_table s5h2x_pci_config_table[] = {
	{ PCI_ANY_ID, PCI_ANY_ID, PCI_ANY_ID,
		PCI_ANY_ID, PCI_ANY_ID, PCI_ANY_ID,
	  	pci_cfgfunc_config_device,
	  	{AHB_PCI_IO_BASE, AHB_PCI_MEM_BASE,
		PCI_COMMAND_MEMORY | PCI_COMMAND_MASTER}
	},
	{},
};

extern void pciauto_config_init(struct pci_controller *hose);

#define mb() __asm__ __volatile__ ("" : : : "memory")

static unsigned long s5h2x_open_config_window(int bus, pci_dev_t dev, int offset)
{
	if(bus == 0)
		return AHB_ADDR_PCI_CFG0(PCI_DEV(dev), PCI_FUNC(dev), offset);
	return AHB_ADDR_PCI_CFG1(PCI_BUS(dev), PCI_DEV(dev), PCI_FUNC(dev), offset);
}

static void s5h2x_close_config_window(void)
{
	mb();
}

static int s5h2x_read_byte (struct pci_controller *hose, pci_dev_t dev,int offset, u8 *val)
{
	*val=*(volatile u8 *)s5h2x_open_config_window(hose->current_busno,dev,offset);
	s5h2x_close_config_window();
	return 0;
}

static int s5h2x_read_word (struct pci_controller *hose, pci_dev_t dev,int offset, u16 *val)
{
	*val=*(volatile u16 *)s5h2x_open_config_window(hose->current_busno,dev,offset);
	s5h2x_close_config_window();
	return 0;
}

static int s5h2x_read_dword (struct pci_controller *hose, pci_dev_t dev,int offset, u32 *val)
{
	*val=*(volatile u32 *)s5h2x_open_config_window(hose->current_busno,dev,offset);
	s5h2x_close_config_window();
	return 0;
}

static int s5h2x_write_byte (struct pci_controller *hose,pci_dev_t dev, int offset,u8 val)
{
	*(volatile u8 *)s5h2x_open_config_window(hose->current_busno,dev,offset)=val;
	s5h2x_close_config_window();
	return 0;
}

static int s5h2x_write_word (struct pci_controller *hose,pci_dev_t dev, int offset,u16 val)
{
	*(volatile u16 *)s5h2x_open_config_window(hose->current_busno,dev,offset)=val;
	s5h2x_close_config_window();
	return 0;
}

static int s5h2x_write_dword (struct pci_controller *hose,pci_dev_t dev, int offset,u32 val)
{
	*(volatile u32 *)s5h2x_open_config_window(hose->current_busno,dev,offset)=val;
	s5h2x_close_config_window();
	return 0;
}


/******************************
 * PCI initialisation
 ******************************/

struct pci_controller s5h2x_hose = {
#if 0
#ifndef CONFIG_PCI_PNP
	config_table: s5h2x_pci_config_table,
#endif
#endif
};

#define barrier()

void pci_init_board (void)
{
	struct pci_controller *hose = &s5h2x_hose;

//	*(volatile unsigned int *)0x30090814=0x01010022;
//	*(volatile unsigned int *)0x30090c68|=0<<16;
//	*(volatile unsigned int *)0x3003015c|=0;

	printf("PCI Block init start\n");

//	*(volatile unsigned int *)0x30090c68|=0<<16;
//	*(volatile unsigned int *)0x3003015c|=0;

#ifndef CONFIG_EXT_CLOCK
	*(volatile unsigned int *)(0x303b0070)=0x16;	//33Mhz
//	*(volatile unsigned int *)0xd03B000C=0X27;		//30Mhz
//	*(volatile unsigned int *)(0xd03b0070)=0x17;	//16Mhz
#endif

	R_USB_PCI_SET	|= 1;
	udelay(20 * 1000);

	R_PCIINTEN	= 0x00000000;
	R_PCICON 	= 0x00000013;
	
	udelay (1000 * 10);
	R_PCIDIAG0	&= 0xfffffff0;

	barrier();
		
#ifdef CONFIG_EXT_CLOCK
	R_PCIDIAG0	|=0x1<<2;
	barrier();	
#endif
		
#ifdef CONFIG_EXT_RESET
	R_PCIDIAG0	|= (0x1<<1);
	barrier();
#endif
	R_PCIRCC 	&= 0xfffffff0;
	barrier();
	
#ifdef CONFIG_66_CLK
	R_PCIRCC 	|= 0x00000003;
	barrier();
#else
	R_PCIRCC 	|= 0x0000000b;
	barrier();
#endif

/* BAYHILL LCD v1 board bug */
#if defined(CONFIG_RTL8139_GPIO_RESET)
	{
		volatile unsigned int *dat, *con;
		dat = R_GPIO_DAT;
		con = R_GPIO_IOCON;

		/* GPIO7 -- RTL8139 RSTB connected.
		   assert RSTB low for minimum 120ns */
		*dat |= (1<<7);		// assert high
		*con &= ~(1<<7);	// output direction
		udelay(100*1000);	// delay 10ms

		*dat &= ~(1<<7);	// assert low(reset)
		udelay(10*1000);	// delay 10ms
		
		*dat |= (1<<7);	// assert high
		udelay(10*1000);	// delay 10ms
	}
#endif

	R_PCIBATAPM = 0x00000000;
	barrier();
	R_PCIBATAPI = 0x00000000;
	barrier();
	R_PCISET 	= 0x00000400;
	barrier();

	R_PCIHBAR0	= PCI_MEM_BAR0;
//	R_PCIBATPA0	= PCI_MEM_BAR0;
//	R_PCIBAM0	= ~((unsigned int)PCI_MEM_BAR0)+1;

//	R_PCIHBAR0	= 0x4000;
	R_PCIBATPA0	= PCI_MEM_BAR0;
	R_PCIBAM0	= ~((unsigned int)PCI_MEM_BAR0)+1;


	R_PCIHBAR1	= PCI_MEM_BAR2;
//	barrier();
	/* Base Address Translation */
	R_PCIBATPA1	= PCI_MEM_BAR2;
	barrier();
	/* Base Address Mask */
	R_PCIBAM1	= 0x0;
	barrier();
	/* I/O Base Address 2 from PCI to AHB */
	/* disable */
	/* location & size */
	R_PCIHBAR2	=0x30030100;
	barrier();
	/* Base Address Translation */
	R_PCIBATPA2	= 0x30030100;
	barrier();
	/* Base Address Mask */
	R_PCIBAM2	= 0x0;
	barrier();
	R_PCIHLINE	= 0x00001008;
	barrier();
	R_PCIHLTIT	&= 0xffff0000;
	barrier();
	/* Int Line:26(0x1a), Int Pin:INTA# */
	R_PCIHLTIT	|= 0x0000011a;
	barrier();
	R_PCIHTIMER	= 0x00008080;
	barrier();
	/* I/O, Memory, Bus Master, ..., SERR Enable and Clear Status */
	R_PCIHSC 	= 0xffff0357;
	barrier();
	R_PCICON 	= 0x00000313;
	printf("PCI Block init end\n");

	udelay (10*1000);	// delay 10ms
	/*
	 * Register the hose
	 */
	hose->first_busno = 0;
	hose->region_count = 2;	

	pci_set_region (hose->regions + 0, 0x4000, AHB_PCI_MEM_BASE+0x4000,
			0x10000000, PCI_REGION_MEM);
	pci_set_region (hose->regions + 1, 0x600, AHB_PCI_IO_BASE+0x600,
			0x00080000, PCI_REGION_IO);
	pci_set_ops (hose,s5h2x_read_byte,s5h2x_read_word,
			s5h2x_read_dword, s5h2x_write_byte,
			s5h2x_write_word,s5h2x_write_dword);
	pci_register_hose (hose);

	printf("PCI scan bus start\n");
	hose->last_busno = pci_hose_scan (hose);
	printf("PCI scan bus end\n");
}
#endif
