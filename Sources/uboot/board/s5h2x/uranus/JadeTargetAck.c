
#ifndef __KERNEL__
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

#include <Dprotocol.h>
#include <JCommand.h>

#else
#include "Dprotocol.h"
#include "JCommand.h"
#endif //__KERNEL__

#include "JadeTargetAck.h"

#define J_DEBUG_MODE  1
#undef J_DEBUG_MODE

#ifndef __KERNEL__
static struct jade_mem_man_t {
	int init;
	int fd;
	unsigned int* vAddr;
	struct jade_mem_t memZone;
} gMemManager[N_MEM_ZONE];
#endif

static int verifyAddress(volatile unsigned int** ppAddr);
static void ackWrongAddr(volatile unsigned int* pAddr);

static int verifyAddress(volatile unsigned int** ppAddr)
{
	int nrZone = 0;
#ifndef __KERNEL__
	int count = N_MEM_ZONE;
	struct jade_mem_t *pMemZone;
	struct jade_mem_man_t *pMemMan = gMemManager;
	off_t tAddr = (off_t) *ppAddr;
	
	do{
		if(!pMemMan->init) return -1;

		pMemZone = &pMemMan->memZone;		

		if( (tAddr < pMemZone->base) || (tAddr > (pMemZone->base + pMemZone->size)))
		   pMemMan++;

		else {
		   tAddr = tAddr - pMemZone->base + (off_t)pMemMan->vAddr;	
		   break;
		}
		
		nrZone++;
	}while(count--);

	if(count <= 0) return -1;
	
	*ppAddr = (volatile unsigned int*) tAddr;
#endif
	return nrZone;
}

static void ackWrongAddr(volatile unsigned int* pAddr)
{
	unsigned char ackBuf[JACK_WRONG_ADDR_SIZE];
	unsigned int bufCnt = 0;
	unsigned char* p8Addr = (unsigned char*) &pAddr;

	ackBuf[bufCnt++] = JACK_WRONG_ADDR;
	
	while(bufCnt < JACK_WRONG_ADDR_SIZE){
		ackBuf[bufCnt++] = *p8Addr++;
	}

	jadeDebugSend(ackBuf, (unsigned char)JACK_WRONG_ADDR_SIZE);

	return;
}

int jadeWrite(unsigned char *rcvBuf, unsigned char size)
{
	volatile unsigned int *pAddr = 0;
	unsigned int i, regVal = 0;
	unsigned char	*p8Addr = (unsigned char *) &pAddr;
	unsigned char 	*p8Data = (unsigned char *) &regVal;
	int bufCnt = 0;

// check Jade command
	if((rcvBuf[bufCnt++] & 0xFF) != JCMD_WRITE) return -1;
	if(size != JCMD_WRITE_SIZE) return -1;

// convert address and data	
	for(i = 0; i < 4; i++)
		*p8Addr++ = rcvBuf[bufCnt++] & 0xFF;
	for(i = 0; i < 4; i++)
		*p8Data++ = rcvBuf[bufCnt++] & 0xFF;

// check address 
	if(((unsigned int)pAddr & 0x3) ||
	   ((verifyAddress(&pAddr) < 0)) ){
		return -1;
	}
// write
	else
#ifdef J_DEBUG_MODE
		printf("Write Cmd 0x%08x: 0x%08x\n", pAddr, regVal);
#endif
		*pAddr = regVal;

	return bufCnt;
}

int jadeBurstWrite(unsigned char *rcvBuf, unsigned char size)
{
	volatile unsigned int* pAddr;
	unsigned int i, regVal[30];
	unsigned char nOfRegs;
	
	unsigned char *p8Addr = (unsigned char*) &pAddr;
	unsigned char *p8Data = (unsigned char*) regVal;
	unsigned char bufCnt = 0;

	if(rcvBuf[bufCnt++] != JCMD_BWRITE) return -1; // check Jade command

	nOfRegs = rcvBuf[bufCnt++]; // save number of register

	if(nOfRegs > 30) return -1;
	if(size != ((nOfRegs << 2) + 6)) return -1;

// convert address 
	for(i = 0; i < 4; i++)
		p8Addr[i] = rcvBuf[bufCnt++] & 0xFF;

// check address 
	if(((unsigned int)pAddr & 0x3) ||
	   ((verifyAddress(&pAddr) < 0)) ){
		return -1;
	}

// convert data
#if 1
	for(i = 0; i < (nOfRegs << 2); i++){
		p8Data[i] = rcvBuf[bufCnt++] & 0xFF;
	}

	for(i = 0; i < nOfRegs ; i++){
#ifdef J_DEBUG_MODE
		printf("Burst Write Cmd 0x%08x: 0x%08x\n", pAddr, regVal[i]);
#endif
		*pAddr++ = regVal[i];
	}
#else
/*
// convert data
	memcpy(regVal, &rcvBuf[bufCnt], (nOfRegs << 2));
// Burst write
	memcpy((void*)pAddr, regVal, (nOfRegs << 2));
*/
#endif

	return (nOfRegs << 2) + 2;
}

int jadeWriteField(unsigned char *rcvBuf, unsigned char size)
{
	volatile unsigned int* pAddr;
	unsigned int i, regVal, bitMask;
	unsigned char *p8Addr = (unsigned char*)&pAddr;
	unsigned char *p8Data = (unsigned char*)&regVal;
	unsigned char *p8BitMask = (unsigned char*)&bitMask;
	int bufCnt = 0;

	if((rcvBuf[bufCnt++] & 0xFF) != JCMD_WRITEF) return -1;
	if(size != JCMD_WRITEF_SIZE) return -1;

// convert Address
	for(i = 0; i < 4; i++)
		*p8Addr++ = rcvBuf[bufCnt++] & 0xFF;

// check address 
	if(((unsigned int)pAddr & 0x3) ||
	   ((verifyAddress(&pAddr) < 0)) ){
		return -1;
	}
// convert bitmask 
	for(i = 0; i < 4; i++)
		p8BitMask[i] = rcvBuf[bufCnt++] & 0xFF;
// convert data 
	for(i = 0; i < 4; i++)
		p8Data[i] = rcvBuf[bufCnt++] & 0xFF;
// write bitmask 
	i = (*pAddr) & (~bitMask);
	*pAddr = i | (regVal & bitMask);
#ifdef J_DEBUG_MODE
	printf("Write Cmd 0x%08x: 0x%08x, mask: 0x%08x\n", pAddr, regVal, bitMask);
#endif

	return bufCnt;
}

int jadeReadAck(unsigned char *rcvBuf, unsigned char size)
{
	volatile unsigned int *pAddr, *pVAddr;
	unsigned int i, regVal;
	unsigned char *p8Addr = (unsigned char*) &pAddr;
	unsigned char *p8Data = (unsigned char*) &regVal;

	unsigned char ackBuf[JACK_READ_SIZE];
	unsigned char bufCnt = 0;

// check Jade command
	if((rcvBuf[bufCnt++] & 0xFF) != JCMD_READ) return -1;
	if(size != JCMD_READ_SIZE)return -1;

// convert address and data	
	for(i = 0; i < 4; i++)
		p8Addr[i] = rcvBuf[bufCnt++] & 0xFF;
// need when send ack packet	
	pVAddr = pAddr ;

// check address 
	if(((unsigned int)pAddr & 0x3) ||
	   ((verifyAddress(&pVAddr) < 0)) ){
		ackWrongAddr(pAddr);
		return -1;
	}

	regVal = *pVAddr; 	// Read 
#ifdef J_DEBUG_MODE
	printf("Read Ack 0x%08x(0x%08x): 0x%08x\n", pAddr,pVAddr,regVal);
#endif
	bufCnt = 0;

	ackBuf[bufCnt++] = JACK_READ;	// Jade read ack 
	for (i = 0; i < 4; i++)
		ackBuf[bufCnt++] = (unsigned char)p8Addr[i] & 0xFF; // address
	for (i = 0; i < 4; i++)
		ackBuf[bufCnt++] = (unsigned char)p8Data[i] & 0xFF; // data 

#ifdef J_DEBUG_MODE
	for (i = 0; i < bufCnt; i++)
		printf("%02d Read Ack send Data 0x%02x\n", i, ackBuf[i]);
#endif
	if(jadeDebugSend(ackBuf, bufCnt) < 0){
		printf("Jade Read ack is failed 0x%08x: 0x%08x\n", pAddr, regVal);
		return -1;
	}

	return bufCnt;
}

int jadeReadFieldAck(unsigned char *rcvBuf, unsigned char size)
{
	volatile unsigned int* pAddr, *pVAddr;
	unsigned int i, regVal, bitMask;
	unsigned char *p8Addr = (unsigned char*)&pAddr;
	unsigned char *p8Data = (unsigned char*)&regVal;
	unsigned char *p8BitMask = (unsigned char*)&bitMask;

	unsigned char ackBuf[JACK_READF_SIZE];
	unsigned char bufCnt = 0;

// check Jade command
	if((rcvBuf[bufCnt++] & 0xFF) != JCMD_READF) return -1;
	if(size != JCMD_READF_SIZE)return -1;

// convert address and data	
	for(i = 0; i < 4; i++)
		p8Addr[i] = rcvBuf[bufCnt++] & 0xFF;
	for(i = 0; i < 4; i++)
		p8BitMask[i] = rcvBuf[bufCnt++] & 0xFF;

	pVAddr = pAddr;

// check address 
	if(((unsigned int)pAddr & 0x3) ||
	   ((verifyAddress(&pVAddr) < 0)) ){
		ackWrongAddr(pAddr);
		return -1;
	}

// Read field
	regVal = *pVAddr & bitMask;

#ifdef J_DEBUG_MODE
	printf("Read Field Ack 0x%08x(0x%08x): 0x%08x\n", pAddr,pVAddr,regVal);
#endif

	bufCnt = 0;

	ackBuf[bufCnt++] = JACK_READF;	// Jade read ack 
	for (i = 0; i < 4; i++)
		ackBuf[bufCnt++] = (unsigned char)p8Addr[i] & 0xFF; // address
	for (i = 0; i < 4; i++)
		ackBuf[bufCnt++] = (unsigned char)p8Data[i] & 0xFF; // data 

#ifdef J_DEBUG_MODE
	for (i = 0; i < bufCnt; i++)
		printf("%02d Read Field Ack send Data 0x%02x\n", i, ackBuf[i]);
#endif
	if(jadeDebugSend(ackBuf, bufCnt) < 0){
		printf("Jade Read field ack is failed 0x08x: 0x08x\n", pAddr, regVal);
		return -1;
	}

	return bufCnt;
}


int jadeBurstReadAck(unsigned char *rcvBuf, unsigned char size)
{
	volatile unsigned int *pAddr, *pVAddr;
	unsigned int i;
	unsigned char nOfRegs;
	unsigned char *p8Addr = (unsigned char*) &pAddr;
	unsigned char *p8Data;
	unsigned int *pRegVal;

	unsigned char pAckBuf[128];
	unsigned char bufCnt = 0;
#ifdef __KERNEL__
	unsigned char bReadData[256];
#endif


	if((rcvBuf[bufCnt++] & 0xFF)!= JCMD_BREAD) {
		printf("Burst Read Command is Wrong\n");
		return -1;
	}
	if(size != JCMD_BREAD_SIZE){
		printf("Burst Read Size is Wrong\n");
		return -1;
	}

	nOfRegs  = rcvBuf[bufCnt++] & 0xFF;
	if(nOfRegs > 30) return -1;


	for(i = 0; i < 4; i++)
		p8Addr[i] = rcvBuf[bufCnt++] & 0xFF;
	
	pVAddr = pAddr;

// check address 
	if(((unsigned int)pAddr & 0x3) ||
	   ((verifyAddress(&pVAddr) < 0)) ){
		printf("Burst Read Address is Wrong\n");
		ackWrongAddr(pAddr);
		return -1;
	}

// read data storage
#ifndef __KERNEL__
	pRegVal = malloc((nOfRegs << 2));
#else
	pRegVal = bReadData;
#endif

	if(pRegVal == NULL) {
		printf("request pRegVal is failed\n");
		return -1;
	}

	p8Data = (unsigned char*) pRegVal;

	for(i = 0; i < nOfRegs; i++){
		pRegVal[i] = pVAddr[i];
	}
	
	bufCnt = 0;

	pAckBuf[bufCnt++] = JACK_BREAD;
	pAckBuf[bufCnt++] = nOfRegs;

	for(i = 0; i < 4; i++)
		pAckBuf[bufCnt++] = p8Addr[i];
#if 1
	for(i = 0; i < (nOfRegs << 2); i++)
		pAckBuf[bufCnt++] = p8Data[i];

#ifdef J_DEBUG_MODE
	for (i = 0; i < bufCnt; i++)
		printf("%02d Burst Read Ack send Data 0x%02x\n", i, pAckBuf[i]);
#endif
	if( jadeDebugSend(pAckBuf, bufCnt) < 0)
		printf("Jade Burst Read ack is failed 0x%08x - nRegs: %02d\n", pAddr, nOfRegs);
#else
/*
	memcpy(&pAckBuf[bufCnt], p8Data, (nOfRegs << 2));

	if( jadeDebugSend(pAckBuf, ((nOfRegs << 2) + 6)) < 0)
		printf("Jade Burst Read ack is failed 0x%08x - nRegs: %02d\n", pAddr, nOfRegs);
*/
#endif

#ifndef __KERNEL__
	free(pRegVal);	
#endif

	return (bufCnt);
}

int jadeEcho(unsigned char *rcvBuf, unsigned char size)
{
	if(rcvBuf[0] != JCMD_ECHO) return -1;
	if(size > 128) return -1;

#ifdef J_DEBUG_MODE
	printf("%s",&rcvBuf[1]);
#endif 

	rcvBuf[0] = JACK_ECHO;

	if(jadeDebugSend(rcvBuf, size) < 0)
		printf("Jade Echo ack is failed \n");

	return size;
}


int jadeEnd(unsigned char *rcvBuf) 
{
	unsigned char bufCnt = 0;

	if(rcvBuf[bufCnt++] != JCMD_END) return -1;
	
	if((rcvBuf[bufCnt++] != 'E') && 
	   (rcvBuf[bufCnt++] != 'e')) 	return -1;

	if((rcvBuf[bufCnt++] != 'N') && 
	   (rcvBuf[bufCnt++] != 'n')) 	return -1;

	if((rcvBuf[bufCnt++] != 'D') && 
	   (rcvBuf[bufCnt++] != 'd')) 	return -1;

	return bufCnt;
}

#ifndef __KERNEL__
int jadeInit(int nr_zone, struct jade_mem_t* pJadeMem)
{
	struct jade_mem_man_t* pMemManager = gMemManager;
	static int init = 0;

	if(!init)
		memset(pMemManager, 0, (sizeof(struct jade_mem_man_t) * N_MEM_ZONE));

	if((nr_zone > N_MEM_ZONE) || (nr_zone == 0)) {
		printf("Jade memory zone limit is %d\n", N_MEM_ZONE);
		return -1;
	}
	

	while(nr_zone--){

		if(pMemManager->init) continue;

		pMemManager->fd = open("/dev/mem", O_RDWR | O_SYNC); 
		if (pMemManager->fd < 0) {
			printf("/dev/mem open failed: 0x%08x\n", pJadeMem->base);
			return -1;
		}
	
		pMemManager->memZone = *pJadeMem;

		pMemManager->vAddr = mmap((void*)0, pJadeMem->size, PROT_READ | PROT_WRITE,
                                    MAP_SHARED, pMemManager->fd, pJadeMem->base);

		pMemManager->init = 1;
		
		printf("Mmap Base 0x%08x(0x%08x) Size 0x%08x\n"
			,pJadeMem->base,pMemManager->vAddr,pJadeMem->size);

		pJadeMem++;
		pMemManager++;
	}

	init = 1;

	return 0;
}
#endif

