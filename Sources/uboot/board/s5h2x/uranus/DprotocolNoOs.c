#define DPRO_DEBUG

#include "Dprotocol.h"


#ifdef DPRO_DEBUG
unsigned char tc[2];
#endif


static struct debug_serial_t{
	int	init;		// init status
} debugSerial = {
	init: 		0,
};

static ssize_t rxPacket(unsigned char* rxBuf, ssize_t waitSec);
static ssize_t rxAck(unsigned char* rxBuf, ssize_t waitSec);
static void txData(unsigned char* txBuf, ssize_t size);

static ssize_t rxPacket(unsigned char* rxBuf, ssize_t waitSec)
{
	int	rcvFlag = 0;
	ssize_t pacCount = 0;

	while(1){
		if(pacCount > DPRO_PACK_MAX) {
#ifdef DPRO_DEBUG
			tc[0] = 0xC0;
			txData(tc, 1);
#endif
			rcvFlag = 0;
			break;
		}

		*rxBuf =(unsigned char)serial_getc();	

		switch(*rxBuf) {
			case DPRO_ACK_ACK:
			case DPRO_ACK_RETRY:
				return -1;
				break;
			case DPRO_PACK_S:
				rxBuf = rxBuf - pacCount;
				*rxBuf++ = DPRO_PACK_S;
				pacCount = 1;
				rcvFlag = 1;
				break;
			case DPRO_PACK_E:
				pacCount++;
				rcvFlag = 0;
				goto __exit_rxpacket;
				break;
			default:
				if(rcvFlag) {
					rxBuf++;
					pacCount++;
				}
				break;
		}
		
	}
__exit_rxpacket:

	return pacCount;
}

static ssize_t rxAck(unsigned char* rxBuf, ssize_t waitSec)
{
	int	retry = 16;
	ssize_t pacCount = 0;

	while(retry--) {
		*rxBuf = (unsigned char) serial_getc();	

		switch(*rxBuf){
			case (DPRO_ACK_ACK):
			case (DPRO_ACK_RETRY):
				rxBuf++;	
				pacCount++;
				retry = 2;
				break;
			default:
				if(pacCount) {
					pacCount++;
					return pacCount;
				}
				break;
		}	
	}

	return -1;
}
static void txData(unsigned char* txBuf, ssize_t size)
{
	if(!size) return;

	while(size--)
		debug_putc(*txBuf++);	
}

int jadeDebugSend(unsigned char* data, unsigned char size)
{
	int retVal = DPRO_OK;
// Common Resource
	struct debug_serial_t *pDebug = &debugSerial;

// Using to make packet
	static unsigned char count = 0;
	unsigned char sendBuf[DPRO_PACK_MAX];
	unsigned char ackBuf[2];
	unsigned char i, tempCount = 0;
	ssize_t pacCount = 0;
	
	if(!pDebug->init) return DPRO_ERROR;
	if((!size) ||
	   (size > DPRO_PACK_MAX)) return DPRO_ERROR;

	pacCount = 0;

	count = (++count) & 0x7F;

// header 
	sendBuf[pacCount++] = DPRO_PACK_S;  	// start
	sendBuf[pacCount++] = count;   // count
	sendBuf[pacCount++] = size - 1;	    // size : 1 ~ 128 -> 0 ~ 127

// data align to Packet	
	tempCount = 0;

	while (tempCount < size){
		switch(size - tempCount){
			case 0:
				break;
			case 1:
				sendBuf[pacCount++] = DPRO_PACK_MSB | ((data[tempCount] & 0x80) >> 7);
				sendBuf[pacCount++] = data[tempCount++] & 0x7F;
				break;
			case 2:			
				sendBuf[pacCount++] = DPRO_PACK_MSB | ((data[tempCount] & 0x80) >> 7)
						   		| ((data[tempCount + 1] & 0x80) >> 6);
				for(i = 0; i < 2; i++)
					sendBuf[pacCount++] = data[tempCount++] & 0x7F;
				break;
			case 3:			
				sendBuf[pacCount++] = DPRO_PACK_MSB | ((data[tempCount] & 0x80) >> 7)
						   		| ((data[tempCount + 1] & 0x80) >> 6)
						   		| ((data[tempCount + 2] & 0x80) >> 5);
				for(i = 0; i < 3; i++)
					sendBuf[pacCount++] = data[tempCount++] & 0x7F;
				break;
			default:	
				sendBuf[pacCount++] = DPRO_PACK_MSB | ((data[tempCount] & 0x80) >> 7)
						   		| ((data[tempCount + 1] & 0x80) >> 6)
						   		| ((data[tempCount + 2] & 0x80) >> 5)
						   		| ((data[tempCount + 3] & 0x80) >> 4);
				for(i = 0; i < 4; i++)
					sendBuf[pacCount++] = data[tempCount++] & 0x7F;
				break;
		}
	}

// Packet End of Data
	sendBuf[pacCount++] = DPRO_PACK_EOD;	
// checksum
	tempCount = 0;
	for (i = 0; i < pacCount; i++)
		tempCount += sendBuf[i];	

	sendBuf[pacCount++] = tempCount & 0x7F;	

//End of Packet
	sendBuf[pacCount++] = DPRO_PACK_E;	


	tempCount = MAX_RETRY;
#if 0
	write(pDebug->fd, (void*)sendBuf, pacCount);
#else
	txData(sendBuf, pacCount);
#endif
	do{
               
		retVal = rxAck(ackBuf, 0);

		if((ackBuf[1] & 0xFF) == count) {
                        switch(ackBuf[0]){
                                case DPRO_ACK_ACK:
                                        retVal = DPRO_OK;
                                        goto exit_jadeDebugSend;
                                        break;
                                case DPRO_ACK_RETRY:
					txData(sendBuf, pacCount);
                                        break;
                                default:
                                        break;
                        }
                }
                else {
                        if(((ackBuf[1] + 1) & 0x7F) == count) continue;
                        else
                                printf("SND: Wrong count number 0x%02x(0x%02x)\n", ackBuf[1], count);
                }
	}while(tempCount--);

exit_jadeDebugSend:
#if 0 
	mq_close(pDebug->mq_dproAck);
	pDebug->mq_dproAck = (mqd_t)NULL;
#endif

	return retVal;
}


int jadeDebugRcv(unsigned char* data, unsigned char* size, unsigned int waitSec)
{
	int retVal = DPRO_OK;
	struct debug_serial_t *pDebug = &debugSerial;

// Packet variable
	ssize_t pacSize;
	unsigned char rcvBuf[DPRO_PACK_MAX];
	unsigned char tempData, tempCount = 0;
	unsigned char msbByte, loop, i;
	unsigned char ack[2];

	unsigned mqRcvPrio = 1;

	if(!pDebug->init) return DPRO_ERROR;
		
	while(1){

		pacSize = rxPacket(rcvBuf, 0);

#ifdef DPRO_DEBUG
		tc[0] = 0xC1;
		tc[1] = pacSize;
		txData(tc, 2);
#endif


// packet max size check
		if(pacSize > DPRO_PACK_MAX) continue;	

// packet start stop check
		if((rcvBuf[pacSize - 1] != DPRO_PACK_E) || 
		   (rcvBuf[0] != DPRO_PACK_S)) continue;
		
// packet checksum check
		tempData = 0;
		for(i = 0; i < (pacSize-2); i++)
			tempData += rcvBuf[i];
		tempData &= 0x7F;
		if (tempData != rcvBuf[pacSize-2]) {
			ack[0] = DPRO_ACK_RETRY;	// retry
			ack[1] = rcvBuf[1]; 		// count
#if 0
			write(pDebug->fd, (void*)ack, (ssize_t)2);
#else
			txData(ack, (ssize_t)2);
#endif
			continue;
		}
#if 0
// EOD check
		if (rcvBuf[pacSize - 3] != DPRO_PACK_EOD){
			ack[0] = DPRO_ACK_RETRY;	// retry
			ack[1] = rcvBuf[1]; 		// count
			write(pDebug->fd, (void*)ack, (ssize_t)2);
			continue;
		}

// packet size check
		tempData = rcvBuf[2] + 1;
		tempData += (((tempData - 1) >> 2) + 1) + 6; 
		if (tempData != pacSize) {
			ack[0] = DPRO_ACK_RETRY;	// retry
			ack[1] = rcvBuf[1]; 		// count
			write(pDebug->fd, (void*)ack, (ssize_t)2);
			continue;
		}
#endif

// packet align to data
		*size = rcvBuf[2] + 1;
		tempCount = 0; 
		
		while(tempCount < *size){
			msbByte = ((tempCount >> 2) * 5) + 3;
			
			if ((rcvBuf[msbByte] & DPRO_PACK_MSB) != DPRO_PACK_MSB) {
				ack[0] = DPRO_ACK_RETRY;	// retry
				ack[1] = rcvBuf[1]; 		// count
#if 0
				write(pDebug->fd, (void*)ack, (ssize_t)2);
#else
				txData(ack, (ssize_t)2);

#endif
				retVal = DPRO_ERROR;
				printf("MSB Byte is error %02d. 0x%02x\n", msbByte, rcvBuf[msbByte]);

				break;
			}

			if((*size - tempCount) > 3) loop = 4;
			else loop = *size - tempCount;

			for (i = 0; i < loop; i++)
					data[tempCount++] = ((rcvBuf[msbByte] & (0x1 << i)) << (7 - i)) | rcvBuf[msbByte+1+i];
		}

// receive operation complete -> send ack
		if(tempCount == *size) {
			ack[0] = DPRO_ACK_ACK;	// ack
			ack[1] = rcvBuf[1] & 0xFF; 	// count
#if 0
			usleep(100);
			write(pDebug->fd, (void*)ack, (ssize_t)2);
#else
			udelay(100);
			txData(ack, (ssize_t)2);
#endif

			break;
		}


	}

exit_jadeDeRcv:

	return retVal;
}

int jadeDebugInit(char* dproDevice, int baudrate)
{
	int retVal = DPRO_OK;
	struct debug_serial_t *pDebug = &debugSerial;

// check arg
	if(pDebug->init) return DPRO_ERROR;

// open debug driver
	printf("open port name %s\n", dproDevice);


	pDebug->init = 1;

	return retVal;
}

void jadeDebugExit(void)
{
	struct debug_serial_t *pDebug = &debugSerial;
	int retVal = DPRO_OK;
	
	if(!pDebug->init) return;
		
// close uart fd
	pDebug->init = 0;

	printf("Jade Debug mode exit...bye \n"); 
}

