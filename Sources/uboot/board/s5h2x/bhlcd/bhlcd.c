/**
 */
/**
 *
 */

#include <config.h>
#include <common.h>
#include <asm/io.h>
#include <asm/arch/s5h2150.h>
#include <asm/mach-types.h>

DECLARE_GLOBAL_DATA_PTR;

/* ij.jang : add support ARM926 MMU/DCACHE SUPPORT */
#ifdef USE_926EJS_MMU_TRANS

#include <arm926ejs_mmu.h>

extern unsigned int create_mmu_mapping(struct mmu_mapinfo*, int);
/* first level table :
   	use physical address
 	total size should be 4GB
	minimum size per a section is 1MB
 */

static struct mmu_mapinfo mmu_fmap[] = {
	/* SMC Bank #0~2 */
	{ 0x00000000, 0x0C000000, FLD_SECTION, 0, 0 },
	/* SMC hole */
	{ 0x0C000000, 0x14000000, FLD_SECTION, 0, 0 },
#if defined(CONFIG_RTL8139)
	/* DRAM area 112MB*/
	{ 0x20000000, 0x07000000, FLD_SECTION | FLD_C | FLD_B, 0, 0 },
	/* RTL8139 NC/NW buffers 1MB */
	{ 0x27000000, 0x00100000, FLD_SECTION, 0, 0 },
	/* DRAM area rest 14MB*/
	{ 0x27100000, 0x00E00000, FLD_SECTION | FLD_C | FLD_B, 0, 0 },
#else
	/* DRAM area 128MB*/
	{ 0x20000000, 0x08000000, FLD_SECTION | FLD_C | FLD_B, 0, 0 },
#endif
	/* DRAM area hole */
	{ 0x28000000, 0x08000000, FLD_SECTION, 0, 0 },
	/* Internal registers */
	{ 0x30000000, 0x10000000, FLD_SECTION, 0, 0 },
	/* Rest */
	{ 0x40000000, 0xC0000000, FLD_SECTION, 0, 0 },
};
#endif

int board_init (void)
{
	/* ij.jang 051025 : this value should be matched with kernel */
	gd->bd->bi_arch_number = MACH_TYPE_BHLCD;

	/* address of boot parameters = RAM Base Address + 0x100 */
	gd->bd->bi_boot_params = PHYS_SDRAM_1 + 0x100;

	icache_enable();

#if defined(USE_926EJS_MMU_TRANS)
	create_mmu_mapping (mmu_fmap, ARRAY_SIZE(mmu_fmap));
#endif

#if defined(CONFIG_USB_OHCI)
	R_USB_PCI_SET = 0x64;	/* using crystal */
#endif
	return 0;
}

int dram_init (void)
{
	gd->bd->bi_dram[0].start = PHYS_SDRAM_1;
	gd->bd->bi_dram[0].size = PHYS_SDRAM_1_SIZE;

	return 0;
}

int misc_init_r(void)
{
#if defined(CONFIG_PCI)
        pci_init();
#endif
        return 0;
}

/* 02,Aug,2007 ij.jang : added for sync burst read */
#if defined(CONFIG_BBML) || defined(CONFIG_TBML) || defined(CFG_ENV_IS_IN_ONENAND)
void onenand_sync_set(void)
{
	/* SMC register bank address */
	volatile unsigned int *smc;
	unsigned int bank = (CFG_ONENAND_BASE >> 26);

	/* Onenand SYSCONF1 register */
	writew (0xC6E0, CFG_ONENAND_BASE + 0x1E442);

	/* Sync burst read, 16 word burst len */
	if (bank == 0) {
		writel (0x04, &R_SMBIDCYR2);
		writel (0x0B, &R_SMBWST1R2);
		writel (0x01, &R_SMBWST2R2);
		writel (0x00, &R_SMBWSTOENR2);
		writel (0x00, &R_SMBWSTWENR2);
		writel (0x61, &R_SMBCR2);
	} else if (bank == 1) {
		writel (0x04, &R_SMBIDCYR0);
		writel (0x0B, &R_SMBWST1R0);
		writel (0x01, &R_SMBWST2R0);
		writel (0x00, &R_SMBWSTOENR0);
		writel (0x00, &R_SMBWSTWENR0);
		writel (0x61, &R_SMBCR0);
	}
	writel (0x02, &R_SMCCLKSTOP);
	writel (0x01, &R_SMCCLKSTOP);
	writel (0x00, &R_SMCCLKSTOP);
	writel (1<<bank, &R_SMCSYNCEN);
}
#endif
