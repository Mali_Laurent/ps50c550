#ifndef _BML_DTV_H_
#define _BML_DTV_H_

#define PARTITION_ID_END		(0xFFFFFFFF)

struct xsr_parts {
        unsigned int id;
        unsigned int blocks;
        unsigned int attr;
        const char* desc;
	unsigned int buf_addr;	/* write addr */
};

#endif
