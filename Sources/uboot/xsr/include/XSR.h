/*****************************************************************************/
/*                                                                           */
/* PROJECT : AnyStore II XSR v1.4.0                                          */
/* MODULE  :                                                                 */
/* NAME    : XSR include file                                                */
/* FILE    : XSR.h                                                           */
/* PURPOSE : this file contains the include files of XSR                     */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/*          COPYRIGHT 2003-2005, SAMSUNG ELECTRONICS CO., LTD.               */
/*                          ALL RIGHTS RESERVED                              */
/*                                                                           */
/*   Permission is hereby granted to licensees of Samsung Electronics        */
/*   Co., Ltd. products to use or abstract this computer program for the     */
/*   sole purpose of implementing NAND/OneNAND based on Samsung              */
/*   Electronics Co., Ltd. products. No other rights to reproduce, use,      */
/*   or disseminate this computer program, whether in part or in whole,      */
/*   are granted.                                                            */
/*                                                                           */
/*   Samsung Electronics Co., Ltd. makes no representations or warranties    */
/*   with respect to the performance of this computer program, and           */
/*   specifically disclaims any responsibility for any damages,              */
/*   special or consequential, connected with the use of this program.       */
/*                                                                           */
/*---------------------------------------------------------------------------*/
/*                                                                           */
/* REVISION HISTORY                                                          */
/*                                                                           */
/*   23-DEC-2003 [HyoJun Kim]   : first writing                              */
/*   02-FEB-2004 [SongHo Yoon]  : added XSR_Init definition                  */
/*                                                                           */
/*****************************************************************************/

#ifndef _XSR_H_
#define _XSR_H_

#include <XsrTypes.h>
#include <OAM.h>
#include <PAM.h>
#include <LLD.h>
#include <BML.h>
#include <STL.h>

/*****************************************************************************/
/* En-/Dis-able printing debug message of BML                                */
/*****************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

#if defined(BML_DEBUG)
    #define     BML_DBG_PRINT(x)        XSR_DBG_PRINT(x)
#else
    #define     BML_DBG_PRINT(x)
#endif  /* BML_DEBUG */

#define     BML_RTL_PRINT(x)        XSR_RTL_PRINT(x)

#define     BIF_LOG_MSG_ON
#define     BIF_INF_MSG_ON

#define     BBM_LOG_MSG_ON
#define     BBM_INF_MSG_ON

#define     BIF_RTL_PRINT(x)            BML_RTL_PRINT(x)
#define     BIF_ERR_PRINT(x)            BML_RTL_PRINT(x)

#define     BBM_RTL_PRINT(x)            BML_RTL_PRINT(x)
#define     BBM_ERR_PRINT(x)            BML_RTL_PRINT(x)

#if defined(BIF_LOG_MSG_ON)
#define     BIF_LOG_PRINT(x)            BML_DBG_PRINT(x)
#else
#define     BIF_LOG_PRINT(x)
#endif

#if defined(BIF_INF_MSG_ON)
#define     BIF_INF_PRINT(x)            BML_DBG_PRINT(x)
#else
#define     BIF_INF_PRINT(x)
#endif

#if defined(BBM_LOG_MSG_ON)
#define     BBM_LOG_PRINT(x)            BML_DBG_PRINT(x)
#else
#define     BBM_LOG_PRINT(x)
#endif

#if defined(BBM_INF_MSG_ON)
#define     BBM_INF_PRINT(x)            BML_DBG_PRINT(x)
#else
#define     BBM_INF_PRINT(x)
#endif

/*****************************************************************************/
/* define for debug message of STL                                           */
/*****************************************************************************/

/* En-/Dis-able printing debug message of SectorMap */

#define SM_ERR_MSG_ON          1
#define SM_LOG_MSG_ON          0
#define SM_INF_MSG_ON          1

/* En-/Dis-able printing bebug message of Stl Interface */
#define STI_ERR_MSG_ON          1
#define STI_LOG_MSG_ON          0
#define STI_INF_MSG_ON          1

/* En-/Dis-able printing bebug message of Virtual Nand */
#define VN_ERR_MSG_ON           1
#define VN_LOG_MSG_ON           0
#define VN_INF_MSG_ON           1

/* En-/Dis-able printing bebug message of Sam Buf Manager */
#define SBM_ERR_MSG_ON           1
#define SBM_LOG_MSG_ON           0
#define SBM_INF_MSG_ON           0

/* En-/Dis-able printing bebug message of Ready Queue */
#define RQ_ERR_MSG_ON           1
#define RQ_LOG_MSG_ON           0
#define RQ_INF_MSG_ON           0

/* En-/Dis-able printing bebug message of Operation Queue */
#define OQ_ERR_MSG_ON          1
#define OQ_LOG_MSG_ON          0
#define OQ_INF_MSG_ON          0

/* En-/Dis-able printing bebug message of Garbage Queue */
#define GQ_ERR_MSG_ON           1
#define GQ_LOG_MSG_ON           0
#define GQ_INF_MSG_ON           0

/* debug message of Sector Map */
#if SM_ERR_MSG_ON
#define SM_ERR_PRINT(x)        STL_RTL_PRINT(x)
#else
#define SM_ERR_PRINT(x)        
#endif

#if SM_LOG_MSG_ON
#define SM_LOG_PRINT(x)        STL_DBG_PRINT(x)
#else
#define SM_LOG_PRINT(x)        
#endif

#if SM_INF_MSG_ON
#define SM_INF_PRINT(x)        STL_DBG_PRINT(x)
#else
#define SM_INF_PRINT(x)        
#endif

/* debug message of Stl Interface */
#if STI_ERR_MSG_ON
#define STI_ERR_PRINT(x)        STL_RTL_PRINT(x)
#else
#define STI_ERR_PRINT(x)        
#endif

#if STI_LOG_MSG_ON
#define STI_LOG_PRINT(x)        STL_DBG_PRINT(x)
#else
#define STI_LOG_PRINT(x)        
#endif

#if STI_INF_MSG_ON
#define STI_INF_PRINT(x)        STL_DBG_PRINT(x)
#else
#define STI_INF_PRINT(x)        
#endif

/* debug message of Virtual Nand */
#if VN_ERR_MSG_ON
#define VN_ERR_PRINT(x)         STL_RTL_PRINT(x)
#else
#define VN_ERR_PRINT(x)
#endif

#if VN_LOG_MSG_ON
#define VN_LOG_PRINT(x)         STL_DBG_PRINT(x)
#else
#define VN_LOG_PRINT(x)
#endif

#if VN_INF_MSG_ON
#define VN_INF_PRINT(x)         STL_DBG_PRINT(x)
#else
#define VN_INF_PRINT(x)
#endif

/* debug message of Sam Buf Manager */
#if SBM_ERR_MSG_ON
#define SBM_ERR_PRINT(x)         STL_RTL_PRINT(x)
#else
#define SBM_ERR_PRINT(x)
#endif

#if SBM_LOG_MSG_ON
#define SBM_LOG_PRINT(x)         STL_DBG_PRINT(x)
#else
#define SBM_LOG_PRINT(x)
#endif

#if SBM_INF_MSG_ON
#define SBM_INF_PRINT(x)         STL_DBG_PRINT(x)
#else
#define SBM_INF_PRINT(x)
#endif

/* debug message of Ready Queue */
#if RQ_ERR_MSG_ON
#define RQ_ERR_PRINT(x)         STL_RTL_PRINT(x)
#else
#define RQ_ERR_PRINT(x)
#endif

#if RQ_LOG_MSG_ON
#define RQ_LOG_PRINT(x)         STL_DBG_PRINT(x)
#else
#define RQ_LOG_PRINT(x)
#endif

#if RQ_INF_MSG_ON
#define RQ_INF_PRINT(x)         STL_DBG_PRINT(x)
#else
#define RQ_INF_PRINT(x)
#endif

/* debug message of Operation Queue */
#if OQ_ERR_MSG_ON
#define OQ_ERR_PRINT(x)        STL_RTL_PRINT(x)
#else
#define OQ_ERR_PRINT(x)        
#endif

#if OQ_LOG_MSG_ON
#define OQ_LOG_PRINT(x)        STL_DBG_PRINT(x)
#else
#define OQ_LOG_PRINT(x)        
#endif

#if OQ_INF_MSG_ON
#define OQ_INF_PRINT(x)        STL_DBG_PRINT(x)
#else
#define OQ_INF_PRINT(x)        
#endif

/* debug message of Garbage Queue */
#if GQ_ERR_MSG_ON
#define GQ_ERR_PRINT(x)         STL_RTL_PRINT(x)
#else
#define GQ_ERR_PRINT(x)
#endif

#if GQ_LOG_MSG_ON
#define GQ_LOG_PRINT(x)         STL_DBG_PRINT(x)
#else
#define GQ_LOG_PRINT(x)
#endif

#if GQ_INF_MSG_ON
#define GQ_INF_PRINT(x)         STL_DBG_PRINT(x)
#else
#define GQ_INF_PRINT(x)
#endif

#ifdef __cplusplus
};
#endif // __cplusplus

#endif /* _XSR_H_ */
