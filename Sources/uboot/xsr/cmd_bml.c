/**************************** u-boot parmeter block ****************************/
#include <common.h>

/* ij.jang : move to global variable */
DECLARE_GLOBAL_DATA_PTR;

#ifdef CFG_ENV_IS_IN_BBML

#include <command.h>
#include <environment.h>
#include <linux/stddef.h>
#include <malloc.h>

extern uchar default_environment[];
extern int default_environment_size;
char * env_name_spec = "BBML";

#ifdef ENV_IS_EMBEDDED
extern uchar environment[];
env_t *env_ptr = (env_t *) (&environment[0]);
#else /* ! ENV_IS_EMBEDDED */
/* env_relocate() will reallocate this pointer*/
env_t *env_ptr = NULL;
#endif /* ENV_IS_EMBEDDED */
#endif /*CFG_ENV_IS_IN_BBML*/

/*******************************************************************************/

#include <common.h>
#include <command.h>
#include "../xsr/u-boot_interface.h"
#include "../xsr/include/BML.h"
#include "../xsr/include/XsrTypes.h"
//#include "../include/asm-arm/arch-arm1136/mux.h"	/* for Get time */
#include "../xsr/include/LLD.h"
#include "../xsr/dtv_bml.h"

static struct BML_Info	stVol_Info[XSR_MAX_DEVICES];
static int nVol = 0;
int open_flag = -1;
struct partition_info partitions;
static u32 in_memory_partition = 0;
static u32 modified_partition = 0;

/* GBBM2 command function */
#define SPB(nVol)	(stVol_Info[nVol].nPgsPerBlk * stVol_Info[nVol].nSctsPerPg)	

/* ij.jang */
extern const char* dtv_def_getpartdesc(unsigned int id);

static void percent(int total, int cur);
static void bbm_device_info(void);

static int bbm_open_device(void);
static int bbm_close_device(void);
static int bbm_erase_all(void);
static int bbm_format_device(void);
static int bbm_show_partition(void);
static int bbm_save_partition(void);
static int bbm_del_partition(void);
static int bbm_load_kernel(unsigned char* addr);
static int bbm_erase_part(unsigned int start, unsigned int end);
static int bbm_save_image(unsigned int id, unsigned int size);
static int bbm_restore_image(unsigned int id, unsigned int size);
static int bbm_add_partition(unsigned int id, unsigned int attr,
		unsigned int blocks);
static int load_partition(void);
static int bbm_save_kernel(void);

/* ij.jang : see <board>.c */
extern void onenand_sync_set(void);

int do_bbm(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	u32 id, attr, blocks, size;
	u32 start, end;

	switch (argc) {
	case 2:
		if (strcmp(argv[1], "open") == 0)
			return bbm_open_device();
		if (strcmp(argv[1], "close") == 0)
			return bbm_close_device();
		if (strcmp(argv[1], "eraseall") == 0)
			return bbm_erase_all();
		if (strcmp(argv[1], "format") == 0)
			return bbm_format_device();
		break;

	case 3:
		if (strcmp(argv[1], "set") == 0) {
			int vol = simple_strtoul (argv[2], NULL, 10);
			if (open_flag != -1) {
				printf ("Device opened. close it first.\n");
				return -1;
			} else if (vol == 1 || vol == 0) {
				nVol = vol;
				printf ("XSR will use device #%d for operations.\n", vol);
				return 0;
			}
		}
		if (strcmp(argv[1], "show") == 0 &&
		    strcmp(argv[2], "partition") == 0)
			return bbm_show_partition();

		if (strcmp(argv[1], "save") == 0 &&
		    strcmp(argv[2], "partition") == 0)
			return bbm_save_partition();

		if (strcmp(argv[1], "del") == 0 && 
		    strcmp(argv[2], "partition") == 0)
			return bbm_del_partition();

		if (strcmp(argv[1], "save") == 0 &&
		    strcmp(argv[2], "kernel") == 0)
			return bbm_save_kernel();

		if (strcmp(argv[1], "load") == 0 &&
		    strcmp(argv[2], "kernel") == 0)
			return bbm_load_kernel(
				(unsigned char*)KERNEL_BUFFER_START);
		break;

	case 4:
		if (strcmp(argv[1], "erase") == 0) {
			start = simple_strtoul(argv[2], NULL, 10);
			end = simple_strtoul(argv[3], NULL, 10);
			return bbm_erase_part(start, end);
		}
		if (strcmp(argv[1], "load") == 0 && 
			strcmp(argv[2], "kernel") == 0) {
			char* addr = (char*)simple_strtoul(argv[3], NULL, 16);
			return bbm_load_kernel(addr);
		}
		break;

	case 5:
		if(strcmp(argv[1], "save") == 0 &&
			strcmp(argv[2], "image") == 0) {
			id = simple_strtoul(argv[3], NULL, 10);
			size = simple_strtoul(argv[4], NULL, 10);
			return bbm_save_image(id, size);
		}
		if(strcmp(argv[1], "restore") == 0 &&
			strcmp(argv[2], "image") == 0) {
			id = simple_strtoul(argv[3], NULL, 10);
			size = simple_strtoul(argv[4], NULL, 10);
			return bbm_restore_image(id, size);
		}
		break;

	default:
		if (strcmp(argv[1], "add") == 0 &&
		    strcmp(argv[2], "partition") == 0) {
			id = simple_strtoul(argv[3], NULL, 10);
			attr = simple_strtoul(argv[4], NULL, 10);
			blocks = simple_strtoul(argv[5], NULL, 10);
			printf("bbm add partition start \n");
			return bbm_add_partition(id, attr, blocks);
		}
		break;
	}

	printf("Usage:\n%s\n", cmdtp->usage);
	return 1;
}

U_BOOT_CMD(
	bbm, 6, 1, do_bbm,
	"bbm   - BBM sub-system\n",
	"bbm set - set default device.(0/1)\n"
	"bbm format - format device\n"
	"bbm open - open device\n"
	"bbm eraseall - erase all blocks\n"
	"bbm erase start end - erase part of blocks\n"
	"		- ex) bbm erase 0x4(start block) 0x14(end block)\n"
	"bbm save kernel - save kernel image\n"
	"	 	- downloader 0x21000000 from tftpboot\n"
	"bbm load kernel - load kernel image\n"
	"	 	- load kernel 0x21000000 from kernel partition\n"
	"bbm save image id size - save image to specific partition\n"
	"		- downloader 0x21000000 from tftpboot\n"
	"		- ex) bbm save image 0x8(id) 0xC746C(size)\n"
	"bbm restore image id size - save rfs image to RFS partition\n"
	"		- downloader 0x21000000 from tftpboot\n"
	"		- ex) bbm restore image 0x9(id) 0x1356000(size)\n"

	"\n- For partition -\n"
	"bbm show partition - show partition information\n"
	"bbm save partition - save partition information\n"
	"bbm del partition  - delete last partition information\n"
	"bbm add partition id attr block - add partition information\n"
	"		- ex) bbm add partition 0x(id) 0x1(attr) 0x10(blocks)\n"

);

static int bbm_open_device(void)
{
	int nret;

	/* ij.jang */
	//onenand_sync_set();

	(*(volatile unsigned *)0x300201E8) = 0x2;
	(*(volatile unsigned *)0x300201E8) = 0x1;
	(*(volatile unsigned *)0x300201E8) = 0x0;
	(*(volatile unsigned *)0x300201EC) = 0x3;

	nret =	bml_open(nVol, &stVol_Info[nVol]);
	if( nret == -1 ) {
		printf("bml open error\n");
		printf("error return value %x\n", nret);
		return -1;
	}
	
	open_flag = 1;
	bbm_device_info();
	
	return 0;	
}

/*
 * erase all block except reservoir area
 */
static int bbm_erase_all(void)
{
	int start_block, end_block;
	int ret;	

	start_block = 0;
	end_block = stVol_Info[nVol].nNumOfUsBlks;

	ret = bml_erase(nVol, start_block, end_block);
	if(ret){	
		printf("bml eraseall error\n");
		printf("error return value %x\n", ret);
		return -1;
	}
	else
		printf("bbm erase all success \n");	
	
	return 0;

}

/*
 * erase specific block
 * @param start : start block
 * @param end : end block
 */
static int bbm_erase_part(unsigned int start, unsigned int end)
{

	int start_block, end_block;
	int ret;
	
	start_block = start;
	end_block = end + 1;
	
	if(start_block > end_block) {
		printf("Invalid parmeter\n");
		return -1;
	}

	ret = bml_erase(nVol, start_block, end_block);

	if(ret){	
		printf("BML_Erase error code = 0x%x \n",ret);
		return -1;
	}

	printf("BML Erase part success \n");	
	printf("Erase from %dblock to %dblock \n",start, end);

	return 0;
}

static int bbm_format_device(void)
{
	int ret;

	printf("bml_format_device start \n");
	if (open_flag != -1) {
		printf ("Device is opened. close it first.\n");
		bml_close(nVol);
		open_flag = -1;
	}
	ret = bml_format(nVol);

	if(ret){	
		printf("bbm formt error \n");
		printf("error return value %x\n", ret);
		return -1;
	} else {
		open_flag = 1;
	}

	in_memory_partition = 0;
	ret = bbm_show_partition();
	if(ret<0) {
		return -1;
	}
	printf("bbm Format success \n");	
	
	return 0;
}

/* ij.jang */
static void print_part_info(const unsigned int id)
{
	int i;
	struct xsr_parts *part = NULL;

	i = dtv_def_parts_get_index(id);
	if (i != -1) {
		part = dtv_def_parts_get(i);
		printf(" id        : %s", part->desc);
		printf(" (0x%X)\n", id);
		printf(" load addr : 0x%08X\n", part->buf_addr);
	} else {
		printf(" id : User Defined");
		printf(" (0x%X)\n", id);
		printf(" load addr : unknown\n");
	}
}


static int bbm_show_partition(void)
{
        unsigned int i;
        struct partition_info * pinfo;
	int ret;
	
	if (open_flag == -1) {
        	printf("Device not opened\n");
                return -1;
        }
        
	ret = load_partition();
	if(ret < 0){
		printf("load partition error\n");
		return -1;
	}

        pinfo = &partitions;

        printf("<< PARTITION INFORMATION >>\r\n");

        for (i = 0; i < pinfo->no_part_entries ; i++) {

		/* ij.jang */
		print_part_info (pinfo->entry[i].id);

                switch (pinfo->entry[i].attr) {
                case (BML_PI_ATTR_FROZEN | BML_PI_ATTR_RO):
                        printf(" attr      : RO + FROZEN (0x%x)\r\n",
                                pinfo->entry[i].attr);
                        break;

                case BML_PI_ATTR_RO:
                        printf(" attr      : RO (0x%x)\r\n",
                                pinfo->entry[i].attr);
                        break;

                case BML_PI_ATTR_RW:
                        printf(" attr      : RW (0x%x)\r\n",
                                pinfo->entry[i].attr);
                        break;
                }

                printf(" first_blk    : %d\r\n",
                        pinfo->entry[i].first_blk);
                printf(" no_blks : %d\r\n",
                        pinfo->entry[i].no_blks);
                printf(" ---------------------  \r\n");
        }

        return 0;
}

static int load_partition(void)
{
        if (open_flag == -1) {
                DPRINT("Device not opened\n");
                return -1;
        }

        if (in_memory_partition){
                return 1;
        }

        if ( bml_load_partition(nVol, &partitions) < 0){
                printf("Error loading partition\n");
                return -1;
        }

        printf("Success loading partition\n");
        in_memory_partition = 1;

        return 0;
}


static int bbm_close_device(void)
{

	int ret;
	
	ret = bml_close(nVol);
	if(ret)	{	
		printf("bbm close fail \n");
		printf("error return value = %x\n", ret);
		return -1;
	}

	open_flag = -1;	
	printf("bbm close success\n");

	return 0;
}

static int bbm_del_partition(void)
{
	int ret;

        struct partition_info *pinfo;

        if (open_flag == -1) {
                printf("Device not opened\n");
                return -1;
        }

	ret =  load_partition();
        if(ret < 0){
                printf("load partition error\n");
                return -1;
        }

        pinfo = &partitions;

        pinfo->no_part_entries--;

        /* partition is modified */
        modified_partition = 1;

        ret = bbm_show_partition();
        if(ret){
                printf("show partition err\n");
                return -1;
        }

        return 0;
}

static int bbm_save_partition(void)
{
	int ret;

	ret = load_partition();
        if(ret < -1){
                printf("load partition error\n");
                return -1;
        }


        if (!modified_partition)
                return 1;

        if (bml_save_partition(nVol, &partitions) < 0){
                printf("Error saving partition\n");
                return -1;
        }

        printf("Success saving partition\n");
        modified_partition = 0;
 
	return 0;
}

static int bbm_add_partition(u32 id, u32 attr, u32 blocks)
{
        struct partition_info *pinfo;
        unsigned int first_blk, i;
	int ret;

        if (open_flag == -1) {
                printf("[cmd:err]Device not opened\n");
                return -1;
        }


        ret = load_partition();
        if(ret < 0){
                printf("load partition error\n");
                return -1;
        }

        pinfo = &partitions;

        for (i = 0; i < pinfo->no_part_entries; i++) {
                if (pinfo->entry[i].id == id) {
                        printf("[cmd:err]GBBM2 error: There's same id\n");
                        return 1;
                }
        }

        /* new last index */
        i = pinfo->no_part_entries;

        if (i == 0) /*1st partition*/
        {
                printf("First block : %d\n", i);
                first_blk = 0;
        }
        else
        {
                first_blk = pinfo->entry[i - 1].first_blk + pinfo->entry[i - 1].no_blks;
        }
        DPRINT(" first_blk = %d, blocks = %d\n", first_blk, blocks);

        DPRINT(" nNumOfUsBlks = %d\n", stVol_Info[nVol].nNumOfUsBlks);
        if(first_blk+blocks > stVol_Info[nVol].nNumOfUsBlks){
                printf("[cmd]Block size over \n");
                return 1;
        }

        pinfo->entry[i].id = id;
        pinfo->entry[i].attr = attr;
        pinfo->entry[i].first_blk = first_blk;
        pinfo->entry[i].no_blks = blocks;

        /* update the number of partition entry */
        pinfo->no_part_entries++;

        /* partition is modified */
        modified_partition = 1;

        ret = bbm_show_partition();
        if(ret){
                printf("show partition err\n");
                return -1;
        }

        return 0;
}

static int bbm_save_kernel(void)
{

        unsigned int i, block, end_block;
        unsigned char   *Mbuf = (unsigned char *) KERNEL_BUFFER_START;
        struct partition_entry * entry;
        int nRet;
        unsigned int nBytesReturned;

        if (open_flag == -1) {
                printf("Device not opened\n");
                return -1;
        }

        nRet = load_partition();
        if(nRet < 0){
                printf("load partition err\n");
                return -1;
        }


        for (i = 0; i < partitions.no_part_entries; i++){
                if (partitions.entry[i].id == PARTITION_ID_COPIEDOS)
                        break;
        }

        if (i >= partitions.no_part_entries){
                printf("Coudn't find kernel partition\n");
                return -1;
        }

        entry = &partitions.entry[i];
	DPRINT("entry->id= %d\n",entry->id);
	DPRINT("entry->first_blk= %d\n",entry->first_blk);
	DPRINT("entry->no_blks= %d\n",entry->no_blks);

        nRet = BML_IOCtl(nVol,
                     BML_IOCTL_UNLOCK_WHOLEAREA,
                     NULL, 0,
                     NULL, 0,
                     &nBytesReturned);


        if (nRet != BML_SUCCESS) {
                printf("[cmd] BML_IOCtl error\r\n");
                while(1);
        }

	block = entry->first_blk;
	end_block = (entry->no_blks + entry->first_blk);

	for (; block < end_block; block++) {
		nRet = bml_write_block(nVol, block, Mbuf, NULL);
		if (nRet) {
			printf("bml write block error\n");
			return -1;
		}
                Mbuf += (SPB(nVol) << SECTOR_BITS);
		DPRINT("block:%d  Mbuf:0x%08x\n", block, Mbuf);
	}
	printf("kernel write success to kernel partition\n");

	 return 0;
}

/*
 * load kernel from kernel partiton
 */
static int bbm_load_kernel(unsigned char* addr)
{
	//unsigned char   *buf = (unsigned char *) KERNEL_BUFFER_START;
	unsigned char   *buf = addr;
        struct partition_entry * entry;
        int nRet;
        unsigned int i;
        unsigned int block, end_block;

	if (open_flag == -1) {
                printf("Device not opened\n");
                return -1;
        }

        nRet =  load_partition();
        if(nRet < 0){
                printf("load partition error\n");
                return -1;
        }

        for (i = 0; i < partitions.no_part_entries; i++){
                if (partitions.entry[i].id == PARTITION_ID_COPIEDOS)
                        break;
        }

        if (i == partitions.no_part_entries){
                printf("Coudn't find kernel partition");
                return -1;
        }

        entry = &partitions.entry[i];
	DPRINT("entry->first_blk = %d, entry->no_blks= %d"
			,entry->first_blk, entry->no_blks);

	block = entry->first_blk;
	end_block = entry->no_blks + entry->first_blk;
	for (; block < end_block ; block++) {
		nRet = bml_read_block(nVol, block, buf);
		if(nRet < 0) {
			printf("bml read block error\n");
			return -1;
		}
                buf += (SPB(nVol) << SECTOR_BITS);
		DPRINT("block: %dbuf:0x%08x\n", block, buf);
	}

	printf("kernel read success from kernel partition\n");
	printf("Loaded kernel %08X from OneNAND kernel partition\n", addr);
	//DPRINT("\rTimer2:%x Timer3:%x\n", TIMER2_TCRR, TIMER3_TCRR);

#ifndef BBML_DEBUG
{
	/* Debug Memory Data */
        unsigned int *new = (unsigned int *) KERNEL_BUFFER;
	unsigned int *old = (unsigned int *) ORI_DATA;
	for (i = 0; i < 0x40000; i++) {
		if ( *(old+i) != *(new+i) ) {
			DPRINT("[0x%04x] Expected Data = %x, Loaded Data = %x", 
				i, *(old+i), *(new+i));
			DPRINT("org: 0x%08x, buf:0x%08x", old+i, new+i);
		}
	}
}
#endif 

	return 0;
}

/*
 * Save cramfs image to cramfs partition.
 * @param id : Partition ID
 * @param size : Image size
 */
static int bbm_save_image(unsigned int id, unsigned int size)
{

	unsigned int  i, block_nr, end_block, block;
	unsigned char   *Mbuf = (unsigned char *) KERNEL_BUFFER_START;
	struct partition_entry * entry;
	unsigned int nBytesReturned;
	int ret;

	printf("size = %d\n\n", size);
	if (open_flag == -1) {
		printf("Device not opened\n");
		return -1;
	}

	ret = load_partition();
	if(ret < 0) {
		printf("load partition error\n");
	}

	for (i = 0; i < partitions.no_part_entries; i++){
		if (partitions.entry[i].id == id)
			break;
	}

	if (i >= partitions.no_part_entries){
		printf("Coudn't find [%x] partition\n", id);
		return -1;
	}

	entry = &partitions.entry[i];
	DPRINT("entry->id= %d\n",entry->id);
	DPRINT("entry->first_blk= %d\n",entry->first_blk);
	DPRINT("entry->no_blks= %d\n",entry->no_blks);

	ret = BML_IOCtl(nVol,
					BML_IOCTL_UNLOCK_WHOLEAREA,
					NULL, 0,
					NULL, 0,
					&nBytesReturned);

	if (ret != BML_SUCCESS) {
		printf("[cmd] BML_IOCtl error\r\n");
		while(1);
	}

	block_nr = (unsigned int)
		((size-1) / ((SPB(nVol) << SECTOR_BITS))) + 1;
	
	DPRINT("debug block_nr=%d, SPB(nVol)=%d, size=%d\n", block_nr,SPB(nVol),size);

	if (block_nr > entry->no_blks ){
		printf("Exceed partition size\n");
		return -1;
	}

	block = entry->first_blk;
	end_block = (entry->no_blks + entry->first_blk);

	percent (end_block, 0);

	for (; block < end_block; block++) {
		ret = bml_write_block(nVol, block, Mbuf, NULL);
		if (ret) {
			printf("bml write block error\n");
			return -1;
		}
		Mbuf += (SPB(nVol) << SECTOR_BITS);

		DPRINT("block:%d  Mbuf:0x%08x\n", block, Mbuf);
		
		/* display percent */
		percent(end_block, block);

	}
	percent(end_block, block);
	printf(" Done\n \rWritten %d blocks.\n", block_nr);

	return 0;
}

/*
 * Save image to R/W partition(RFS).
 * @param id : Partition ID
 * @param size : Image size
 */
static int bbm_restore_image(unsigned int id, unsigned int size)
{

	unsigned int  i, block_nr, end_block, block;
    unsigned char   *buf = (unsigned char *) KERNEL_BUFFER_START;
	unsigned char *Mbuf, *Sbuf;
    struct partition_entry * entry;
	unsigned int nBytesReturned;
	int ret;

	printf("Image size = %d\n\n", size);
	if (open_flag == -1) {
		printf("Device not opened\n");
		return -1;
	}

	ret = load_partition();
	if(ret < 0) {
		printf("load partition error\n");
	}

	for (i = 0; i < partitions.no_part_entries; i++) {
		if (partitions.entry[i].id == id)
			break;
	}

	if (i >= partitions.no_part_entries){
		printf("Coudn't find [%x] partition\n", id);
		return -1;
	}

	entry = &partitions.entry[i];
	DPRINT("entry->id= %d\n",entry->id);
	DPRINT("entry->first_blk= %d\n",entry->first_blk);
	DPRINT("entry->no_blks= %d\n",entry->no_blks);

	ret = BML_IOCtl(nVol,
					BML_IOCTL_UNLOCK_WHOLEAREA,
					NULL, 0,
					NULL, 0,
					&nBytesReturned);

	if (ret != BML_SUCCESS) {
		printf("[cmd] BML_IOCtl error\r\n");
		while(1);
	}

	/* get number of block */
	block_nr = ( size / ( (SPB(nVol) + 8) << SECTOR_BITS) );// + 1;
	
	DPRINT("(SPB(nVol)<< SECTOR_BITS = %d\n",( SPB(nVol) << SECTOR_BITS));
	DPRINT("debug block_nr=%d, SPB(nVol)=%d, size=%d\n", block_nr,SPB(nVol),size);

	if (block_nr > entry->no_blks ){
		printf("Exceed partition size\n");
		printf("block = %d\n", block_nr);
		return -1;
	}

	block = entry->first_blk;
	end_block = (block_nr + entry->first_blk);
	
	Mbuf = buf;
	Sbuf = buf + 0x800;
	DPRINT("Mbuf:0x%08x, Sbuf:0x%08x\n", Mbuf, Sbuf);
	
	percent(end_block, 0);
	for (; block < end_block; block++){
    	ret = bml_write_block(nVol, block, Mbuf, Sbuf);
		if (ret) {
			printf("bml write block error\n");
			return -1;
		}
		DPRINT("[####]block:%d  Mbuf:0x%08x, Sbuf:0x%08x\n", block, Mbuf, Sbuf);
		Mbuf += ((SPB(nVol)+ 8) << SECTOR_BITS);
		Sbuf += ((SPB(nVol)+ 8) << SECTOR_BITS);
		
		/* display percent */
		percent(end_block, block);
	}
	percent(end_block, block);
    printf(" Done\n \rWritten %d blocks.\n", block_nr);

	return 0;

}

/*
 * Display percent
 * @param total : total value
 * @param cur	: current value
 */
static void percent(int total, int cur)
{
    int val;

    if (cur == 0)
        printf("\r %4d%c", cur, '%');
    else {
        val = (cur * 100)/total;
        printf("\r %4d%c", val, '%');
    }
}

/*
 * Display device information
 */
static void bbm_device_info(void)
{
	printf("**************** device info  *******************\n");
	printf("OneNAND bootloader version v2.3\n");
	printf("nPgsPerBlk = %d\n", stVol_Info[nVol].nPgsPerBlk);
	printf("nSctsPerPg = %d\n", stVol_Info[nVol].nSctsPerPg);
	printf("nNumOfUsBlks = %d\n", stVol_Info[nVol].nNumOfUsBlks);
	printf("*************************************************\n");
}

/******************************** u-boot parmeter ******************************/

#ifdef CFG_ENV_IS_IN_BBML
static int check_boot_param(void)
{
	int i;
	int ret;

	if (open_flag == -1) {
		printf("Device not opened\n");
		return -1;
	}
	
	ret = load_partition();
	if(ret < 0){
                printf("load partition error\n");
                return -1;
        }

	for (i = 0; i < partitions.no_part_entries; i++){
		if (partitions.entry[i].id == (PARTITION_USER_DEF_BASE+2))
			break;
	}

	if (i >= partitions.no_part_entries){
		printf("No BOOT_PARAM partition\n");
		return -1;
	}
		
	return i;
}

static void use_default(void)
{
	/* ij.jang */
#if 0
	DECLARE_GLOBAL_DATA_PTR;
#endif

	puts ("*** Warning - No Param Part, Input the enviroment \n\n");
	
	if (default_environment_size > CFG_ENV_SIZE){
		puts ("*** Error - default environment is too large\n\n");
		return;
	}

	memset (env_ptr, 0, sizeof(env_t));
	memcpy (env_ptr->data, default_environment, default_environment_size);

	env_ptr->crc = crc32(0, env_ptr->data, ENV_SIZE);
	gd->env_valid = 1;
}

uchar env_get_char_spec (int index)
{
	/* ij.jang */
#if 0
	DECLARE_GLOBAL_DATA_PTR;
#endif

	return ( *((uchar *)(gd->env_addr +  index)) );
}

int saveenv(void)
{
	int i;
	u8 *Mbuf = (u8 *)env_ptr;
	struct partition_entry * entry;

	i = check_boot_param();
	if (i < 0)
		return -1;

	entry = &partitions.entry[i];

	printf ("block no= %08X, cnt=%d\n", entry->first_blk, entry->no_blks);
	
	for (i = 0 ; i < entry->no_blks; i++){
		bml_write_block(nVol, (entry->first_blk + i), Mbuf, NULL );
                Mbuf += (SPB(nVol) << SECTOR_BITS);
	}

 	puts ("save env done\n");
  	return 0;
}

void env_relocate_spec (void)
{
	int part_no, ret;
	u8 *buf = (u8 *)env_ptr;
	struct partition_entry * entry;

	/* ij.jang */
	//onenand_sync_set();

	ret = bbm_open_device();
	if( ret == -1 ){
		puts("You should foramt the device\n");
		return use_default();
	}

	/* ij.jang */
	open_flag = 1;

	part_no = check_boot_param();
	if (part_no < 0) {
		return use_default();
	}

	entry = &partitions.entry[part_no];

	/*Read the environment here*/
	for (part_no = 0 ; part_no < entry->no_blks; part_no++){
		bml_read_block(nVol, (entry->first_blk + part_no), buf );
                buf += (SPB(nVol) << SECTOR_BITS);
		 
	}
 	puts ("load env done\n");

	if (crc32(0, env_ptr->data, ENV_SIZE) != env_ptr->crc)
		return use_default();
}

int env_init(void)
{
	/* ij.jang */
#if 0
	DECLARE_GLOBAL_DATA_PTR;
#endif
	gd->env_addr = (ulong)&default_environment[0];
	gd->env_valid = 1;

	return 0;
}
#endif /*CFG_ENV_IS_IN_BBML*/

/* 02,Aug,2007 ij.jang : onenand performance test code from S/W platform(V) */
#include <asm/hardware.h>

static unsigned int stc1, stc2;
static void start_tsd_stc_val(void)
{
	stc1 = *(volatile unsigned *)rTSD_STC_BASE;
}
static void end_tsd_stc_val(void)
{
	stc2 = *(volatile unsigned *)rTSD_STC_BASE;
}

static int gbbm2_load_image_ext(u32 id, int nVol)
{
    int             nRet;
    unsigned int    n1stVSN;
    unsigned int    nNumOfScts;
    unsigned int    nSctsPerPg;
    XSRPartEntry    stPEntry;
    BMLVolSpec      stVolSpec;
 
    unsigned char * aSBuf;	/* = (unsigned char *)(CFG_ONENAND_BUFADDR + (PART_3_NUM_BLK*128 * 1024)); */
    unsigned char * buf = (unsigned char *)CFG_ONENAND_BUFADDR;

    if (nVol != 1)
    {
        nVol = 0;
    }
 
    if (BML_GetVolInfo(nVol, &stVolSpec) != BML_SUCCESS)
    {
        printf("BML_GetVolInfo Error\r\n");
        return 1;
    }
    
    if (BML_LoadPIEntry(nVol, id, &stPEntry) != BML_SUCCESS)
    {
        printf("BML_LoadPIEntry Error! Part_ID: %d\r\n", id);
        return 1;
    }
 
    nSctsPerPg = stVolSpec.nSctsPerPg;
 
    n1stVSN = stPEntry.n1stVbn * stVolSpec.nPgsPerBlk * nSctsPerPg;
    nNumOfScts = stPEntry.nNumOfBlks * stVolSpec.nPgsPerBlk * nSctsPerPg;
	
	/* ij.jang */
 	aSBuf = buf + stPEntry.nNumOfBlks * 128 * 1024;
 	
    nRet = BML_MRead(nVol, n1stVSN, nNumOfScts, buf, NULL, //aSBuf, 
        (BML_FLAG_SYNC_OP | BML_FLAG_ECC_ON | BML_FLAG_BBM_ON));
      
    if (nRet != BML_SUCCESS)
    {
        printf("ERROR : BML_MRead - %d\n", nRet);
        return 1;
    }
 
    BML_FlushOp(nVol, BML_FLAG_SYNC_OP | BML_FLAG_ECC_ON);
  
    return 0;
}

#define MAX_ERR		100

static void check_gbbm2_data(void)
{
	unsigned int *addr = (unsigned int*)CFG_ONENAND_BUFADDR;
	int i, err=0;
	unsigned int data = 0;
	
	for (i=0; i<(20<<18); i++) {
		if ( data != *addr ) {
			err++;
			printf ("diff at %08X (%08X <--> %08X)\n",
					(i<<2),
					*addr, data);
			if (MAX_ERR < err) {
				puts ("100 errors, terminate!\n");
				break;
			}
		}
		addr++;
		data++;
	}
	printf ("finished..\n");
}

/* load kernel */
static int do_tbbm(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	start_tsd_stc_val();
	gbbm2_load_image_ext(PARTITION_ID_COPIEDOS, 0);
	end_tsd_stc_val();
	printf ("45Khz, %d\n", stc2-stc1);
	
	/* data compare */
	check_gbbm2_data();

	return 0;
}

U_BOOT_CMD (
	test_bbm, 1, 1, do_tbbm,
	"test_bbm	<dest addr> <bytes>	- load kernel using BML\n",
	"test_bbm	<dest addr> <bytes>	- load kernel using BML\n"
);
