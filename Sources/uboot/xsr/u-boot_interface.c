/*
 * (C) Copyright 2005 Samsung Electronics
 * DaeHo kim <daehosky.kim@samsung.com>
 *
 * This software is the confidential and proprietary information of
 * Samsung Electronics, Inc. ("Confidential Information"). You shall not
 * disclose such confidential information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with one of the above copyright holders
 *
 * Technical support:
 *   Samsung Flash Planning Group (flashsw@samsung.com)
 *
 */

#include <config.h>
#include "XsrTypes.h"
#include "OAM.h"
#include "PAM.h"
#include "BML.h"
#include "LLD.h"
#include "u-boot_interface.h"
#include "dtv_bml.h"

static	BMLVolSpec	vol_spec[XSR_MAX_DEVICES];
static	XSRPartI	part_spec[XSR_MAX_DEVICES];
static	int bml_initialized[2] = {0,0};	/* ij.jang : support 2 banks */
LLDSpec DeviceInfo;

#define NR_BLOCKS(pi, i)	(pi->stPEntry[i].n1stVbn + pi->stPEntry[i].nNumOfBlks)
#define SPP(volume)		(vol_spec[volume].nSctsPerPg)
#define SPB(volume)		(vol_spec[volume].nSctsPerPg * vol_spec[volume].nPgsPerBlk)

/* extern functions */
extern void printf (const char *fmt, ...);
extern void* memset(void* ,int, unsigned int);
extern XSRPartI *set_static_partition(unsigned int  volume);

/* local functions */
static XSRPartI *xsr_get_part_spec(unsigned int volume);
static BMLVolSpec *xsr_get_vol_spec(unsigned int volume);
static int xsr_update_vol_spec(unsigned int volume);

/*
 * bml_open perform BML_Init, BML_OPen.
 * Get a BML_Info informat.
 */
int bml_open(int volume, struct BML_Info *device_type)
{
	int ret;

        if (!bml_initialized[volume])
	{

		ret = BML_Init();
		if (ret == BML_ALREADY_INITIALIZED)
		{
       			printf("BML_Init Already opened.. Init success\n");
			return 0;
		}
		else if(ret != BML_SUCCESS)
    		{
       			printf("BML_Init Error\n");
        		return -1;
    		}
		printf("BML Init success\n");

		ret = BML_Open(volume);
    		if (ret != BML_SUCCESS)
    		{
        		printf("BML_Open Error\r\n");
        		return -1;
    		}
		bml_initialized[volume] = 1;
		printf("BML_Open success\n");

		if (BML_GetVolInfo(volume, &vol_spec[volume]) != BML_SUCCESS)
    		{
       			printf("[NW:ERR]  BML_GetVolInfo Fail!\r\n");
        		return -1;
    		}
		device_type->nPgsPerBlk = vol_spec[volume].nPgsPerBlk;
		device_type->nSctsPerPg = vol_spec[volume].nSctsPerPg;
		device_type->nNumOfUsBlks = vol_spec[volume].nNumOfUsBlks;
	}

	return 1;
}

int bml_close(int volume)
{
	int ret;
	bml_initialized[volume]=0;
	ret = BML_Close(volume);
	if (ret != BML_SUCCESS)	{
		printf("BML_Close Error code = 0x%x \n", ret);
		return ret;
	}

	return 0;
}

/*
 * This function read block at buf point
 */
int bml_read_block(int volume, unsigned int block, unsigned char *buf)
{
        int ret;
 	unsigned int i, first;
	/*
	 * SPB: sectors per block
	 * SPP: sectors per page
	 */
        first = block * SPB(volume);	/* start sector */

	/* ij.jang */
#if 0
	for (i = 0; i < SPB(volume); i += SPP(volume)) {
		/* ij.jang */
		//printf ("BML_MRead(%d, %d, %d ...)\n", volume, first+i, SPP(volume));
                ret = BML_MRead(volume, first + i , SPP(volume),
                                buf, NULL, BML_FLAG_ECC_ON);
                if (ret != BML_SUCCESS) {
                        printf("BML_Read error: 0x%08x\n", ret);
                        return -1;
                }
		buf += (SPP(volume) << SECTOR_BITS);
        }
#else
	/* ij.jang */
	ret = BML_MRead(volume, first, SPB(volume), buf,NULL, BML_FLAG_ECC_ON);
	if (ret != BML_SUCCESS) {
		printf("BML_MRead error: 0x%08x\n", ret);
		return -1;
	}
#endif
	return 0;
}

/*
 * This function write block at buf point
 * @param volume : device volume
 * @param block : start block
 * @param Mbuf : Main area address in page
 * @param Sbuf : Spare area address in page
 */
int bml_write_block(int volume, unsigned int block, unsigned char *Mbuf, unsigned char *Sbuf)
{
	int ret;
	unsigned int i, first;

	/* Erase block */
	ret = BML_EraseBlk(volume, block, BML_FLAG_SYNC_OP);

	if(ret != BML_SUCCESS){
		printf("BML_Erase error\n", ret);
		printf("error return value : %08x\n", ret);
		return -1;
	}
	DPRINT("BML_Erase success\n");

	/*
	 * SPB: sectors per block
	 * SPP: sectors per page
	 */
	first = block * SPB(volume);	/* start sector */
	DPRINT("SPB:%d, SPP:%d\n", SPB(volume), SPP(volume));

	for (i = 0; i <  SPB(volume); i += SPP(volume)) {
		if(Sbuf == NULL) {	/* nomal Image write */
			ret = BML_Write(volume, first + i, SPP(volume),
           	        Mbuf, Sbuf, BML_FLAG_ECC_ON);
           	if (ret != BML_SUCCESS) {
               	printf("BML_Write error: 0x%08x\n", ret);
               	return -1;
           	}
			Mbuf += (SPP(volume) << SECTOR_BITS);
		}
		else { /* mkrfs image write */
			ret = BML_Write(volume, first + i, SPP(volume),
					Mbuf, Sbuf, BML_FLAG_ECC_ON);
			if (ret != BML_SUCCESS) {
				printf("BML_Write error: 0x%08x\n", ret);
				return -1;
			}
			DPRINT("block:%d sectors:%d Mbuf:0x%08x, Sbuf:0x%08x\n", block, first+i, Mbuf, Sbuf);
			Mbuf += ((SPP(volume) + 29) << 6);
			Sbuf += ((SPP(volume) + 29) << 6);
		}
	}

	return 0;
}

int bml_erase(int volume, unsigned int start, unsigned int end)
{
	int nBytesReturned;
	int nSctsPerPg;
	int nSctsPerBlk;
	int nBlkNum;
	int ret;

	if(!bml_initialized[volume])
	{
		ret = BML_Open(volume);
        	if (ret != BML_SUCCESS)
        	{
			printf("[bml] BML_Open Error code = 0x%x\r\n", ret);
	                return ret;
	        }
        	bml_initialized[volume] = 1;
        	printf("[bml] BML_Open success\n");
	}

	/* Unlock area */
    	ret = BML_IOCtl(volume,
                     BML_IOCTL_UNLOCK_WHOLEAREA,
                     NULL, 0,
                     NULL, 0,
                     &nBytesReturned);

	if (ret != BML_SUCCESS)	{
        	printf("[bml] BML_IOCtl error code = 0x%x\r\n", ret);
        	return ret;
   	}
	printf("[bml] Success BML_IOCtl\n");

	nSctsPerPg  = SPP(volume);
	nSctsPerBlk = SPB(volume);

	printf("[bml] vol_spec[volume].nNumOfUsBlks = %d\n", vol_spec[volume].nNumOfUsBlks);

	for (nBlkNum = start; nBlkNum < end; nBlkNum++) {
               	ret = BML_EraseBlk(volume,           /* Volume Number    */
                       	        nBlkNum,            /* Block Number     */
                              	BML_FLAG_SYNC_OP);  /* Operation Flag   */

         	if (ret != BML_SUCCESS)	{
               		printf("[bml]  BML_EraseBlk error \r\n");
                	printf("[bml]  Err code = %x; nBlkNum = %d\r\n", ret, nBlkNum);
                	return ret;
            	}
      	}
        printf("[bml] Erase Success\r\n");

	return 0;

}

/*
 * This function Call BML_Format
 */
int bml_format(int volume)
{
	int ret;
	XSRPartI *pi;
	pi = set_static_partition(volume);

	/* if already opened */
	if (bml_initialized[volume]) {
		ret=BML_Close(volume);
       	 if (ret != BML_SUCCESS) {
               	printf(" BML_Close error\r\n");
               	return ret;
        	}
		/* Need to init*/
		bml_initialized[volume] = 0;
	}

	// ij.jang added : allow format before open operation
	if (bml_initialized[volume] == 0)
	{
		/* ij.jang : set sync mode */
		//onenand_sync_set();

		ret = BML_Init();
		if (ret == BML_SUCCESS || ret == BML_ALREADY_INITIALIZED ) {
			bml_initialized[volume] = 1;
		} else {
			printf ("[NW:ERR] BML_Init error\r\n");
			return ret;
		}
	}

	ret = BML_Format(volume, pi, BML_INIT_FORMAT);

	if (ret != BML_SUCCESS) {
		printf("[NW:ERR] BML_Format error\r\n");
		return ret;
	} else {
		BML_Open (volume);
	}

	return 0;
}

int bml_load_partition(int volume, struct partition_info *pinfo)
{
        int error;
        unsigned int partno;
        XSRPartI *pi;

        pi = xsr_get_part_spec(volume);

        /*could't find vaild part table*/
        if (!pi->nNumOfPartEntry) {
                error = xsr_update_vol_spec(volume);
                if (error) /*never action, because of low-formatting*/
                        printf("error(%x)", error);
        }

        printf("nNumOfPartEntry: %d\n", pi->nNumOfPartEntry);
        pinfo->no_part_entries =  pi->nNumOfPartEntry;
        for (partno = 0; partno < pi->nNumOfPartEntry; partno++) {
                pinfo->entry[partno].no_blks  = pi->stPEntry[partno].nNumOfBlks;
                pinfo->entry[partno].id       = pi->stPEntry[partno].nID;
                pinfo->entry[partno].attr    = pi->stPEntry[partno].nAttr;
                pinfo->entry[partno].first_blk = pi->stPEntry[partno].n1stVbn;
        }
        return 0;
}

int bml_save_partition(int volume, struct partition_info *pinfo)
{
        unsigned int partno, sum_blks;
        XSRPartI *pi;
	int ret;

        /*get part_spec to update from user's partition table*/
        pi = xsr_get_part_spec(volume);

        /*Don't need check return value*/
        BML_Close(volume);
        memset(pi, 0x00, sizeof(XSRPartI));


        pi->nNumOfPartEntry = pinfo->no_part_entries;
        printf("NumOfPart: %d\n", pi->nNumOfPartEntry);

        /*covert from user parttab*/
        for (partno = 0, sum_blks = 0; partno < pinfo->no_part_entries; partno++) {
                pi->stPEntry[partno].n1stVbn = sum_blks;
                pi->stPEntry[partno].nNumOfBlks = pinfo->entry[partno].no_blks;
/*
 * NOTE: this option on, to use old version of ftools
 */
                pi->stPEntry[partno].nID = pinfo->entry[partno].id;
                pi->stPEntry[partno].nAttr = pinfo->entry[partno].attr;
                sum_blks += pi->stPEntry[partno].nNumOfBlks;

                printf("start: %d, size: %d id : 0x%x\n",
                                pi->stPEntry[partno].n1stVbn,
                                pi->stPEntry[partno].nNumOfBlks,
                                pi->stPEntry[partno].nID);
        }

        ret = BML_Format(volume, pi, BML_REPARTITION);
        if (ret != BML_SUCCESS) {
		printf("Format error ");
		printf("Error code : %x\r\n", ret);
                return -1;
        }

	ret = BML_Open(volume);
	if (ret != BML_SUCCESS) {
                printf("[bml] BML_Open Error code = 0x%x\r\n", ret);
                return ret;
        }

        return 0;
}

/**
 * xsr_get_part_spec - get a partition instance
 */

static XSRPartI *xsr_get_part_spec(unsigned int volume)
{
        return &part_spec[volume];
}

/**
 * xsr_get_vol_spec - get a volume instance
 */
static BMLVolSpec *xsr_get_vol_spec(unsigned int volume)
{
        return &vol_spec[volume];
}


/**
 * xsr_update_vol_spec - update volume & partition instance from the device
 */
static int xsr_update_vol_spec(unsigned int volume)
{
        int error, len;
        BMLVolSpec *vs;
        XSRPartI *pi;

        vs = xsr_get_vol_spec(volume);
        pi = xsr_get_part_spec(volume);

        memset(vs, 0x00, sizeof(BMLVolSpec));
        memset(pi, 0x00, sizeof(XSRPartI));

        error = BML_GetVolInfo(volume, vs);
        if (error)
                return -1;

        error = BML_IOCtl(volume, BML_IOCTL_GET_FULL_PI,
                                NULL, 0, (unsigned char *)pi,(sizeof(XSRPartI)), &len);
        if (error)
                return -1;

        return 0;
}

static inline void PARTITION(XSRPartI *pi, unsigned int i, unsigned int id,
                unsigned int blocks, unsigned int attr)
{
        pi->stPEntry[i].nID = id;
        pi->stPEntry[i].nAttr = attr;

        if (i == 0)
                pi->stPEntry[i].n1stVbn = 0;
        else
                pi->stPEntry[i].n1stVbn = pi->stPEntry[i-1].n1stVbn +
			pi->stPEntry[i-1].nNumOfBlks;

        pi->stPEntry[i].nNumOfBlks = blocks;
}

static const char signature[BML_MAX_PART_SIG] = "XSRPARTI";

extern struct xsr_parts* get_dtv_def_parts(void);

/* exposed functions */
XSRPartI *set_static_partition(unsigned int volume)
{
        XSRPartI *pi;
        struct xsr_parts *p;

        pi = xsr_get_part_spec (volume);
        pi->nNumOfPartEntry = 0;
        pi->nVer = 0x00011000U;

	for(;;)
	{
		p = dtv_def_parts_get(pi->nNumOfPartEntry);
		if (p == NULL)
			break;
                PARTITION (pi, pi->nNumOfPartEntry, p->id, p->blocks, p->attr);
		pi->nNumOfPartEntry++;
		p++;
	}

        memcpy(pi->aSig, signature, sizeof(signature));
        return pi;
}
