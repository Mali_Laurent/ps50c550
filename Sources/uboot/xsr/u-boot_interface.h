#ifndef _BBML_H_
#define _BBML_H_

#define CONFIG_DEBUG_INTERFACE

#define OAM_DBGMSG_ENABLE

/*This is for u-boot*/
#define PRINT 	printf
#undef UBOOT_DEBUG
#undef BBML_DEBUG

#ifdef BBML_DEBUG
#define DPRINT(fmt, args...)\
do {									\
	PRINT("[%s+%d] " fmt "\n", __func__, __LINE__, ##args);	\
} while(0)		
#else
#define DPRINT(...)
#endif

#define MAX_PARTITION_NUM 10
#define XSR_MAX_DEVICES		10 
#define SECTOR_BITS    9
#define KERNEL_BUFFER_START	CFG_ONENAND_BUFADDR

/* used for debug */
#define KERNEL_BUFFER		0x86200000
#define ORI_DATA		0x86000000

struct partition_entry {
    unsigned int id;         /* partition entry ID */
    unsigned int attr;       /* Attribute          */
    unsigned int first_blk;  /* 1st block number   */
    unsigned int no_blks;    /* # of blocks        */
} ;

struct partition_info{
    unsigned char signature[8];   /* signature of partition information*/
    unsigned int version;          /* version of partition information*/
    unsigned int no_part_entries;   /* # of partition entry*/
    struct partition_entry  entry[MAX_PARTITION_NUM];
};

/**
 * struct ufd_info - unified flash device information
 */
struct BML_Info{
	unsigned int	nPgsPerBlk;
	unsigned int	nSctsPerPg;
	unsigned int	nNumOfUsBlks;
};

int bbml_open(int volume, struct BML_Info *device_type);
int bbml_close(int volume);

int bbml_read_block(int volume, unsigned int block, unsigned char *buf);
int bbml_write_block(int volume, unsigned int block, unsigned char *Mbuf, unsigned char *Sbuf);

int bbml_erase(int volume, unsigned int start, unsigned int end);
int bbml_format(int volume);

int bbml_load_partition(int volume, struct partition_info *pinfo);
#endif /*_BBML_H_*/
