#include <config.h>
#include "include/BML.h"

/**************************************************
 * DTV ONENAND default partition definition
 **************************************************/
#if defined(CONFIG_URANUS)
#include "dtv_bml_uranus.h"
#elif defined(CONFIG_BHLCD)
#include "dtv_bml_bhlcd.h"
#elif defined(CONFIG_SATURN)
#include "dtv_bml_saturn.h"
#elif defined(CONFIG_CRUX)
#include "dtv_bml_crux.h"
#elif defined(CONFIG_POLARIS)
#include "dtv_bml_polaris.h"
#endif

static struct xsr_parts dtv_def_parts[] = {
	DTV_DEF_PARTS
};

const unsigned int dtv_def_parts_num = sizeof(dtv_def_parts) / sizeof(dtv_def_parts[0]);

int dtv_def_parts_get_index(unsigned int id)
{
	int i;

	for (i=0; i<dtv_def_parts_num; i++) {
		if (dtv_def_parts[i].id == id)
			return i;
	}
	return -1;
}

struct xsr_parts* dtv_def_parts_get(unsigned int index)
{
	if (index < 0 || index >= dtv_def_parts_num ||
		dtv_def_parts[index].id == PARTITION_ID_END )
		return (struct xsr_parts*)0;
	return &dtv_def_parts[index];
}
