#! /bin/sh
#
#	Make whichever packages have been requested.
#	Defaults to RPMs.
#
LOGDIR=Logs

#
# If we're on a Debian or Debian-derived system generate deb packages by
# default, else rpm packages.
#
if [ -f /etc/debian_version ] ; then
    type=debian
else
    type=rpm
fi
verbose=false

MAKE=${MAKE:-make}
test ! -z "$MAKE" && make=$MAKE

for opt in $*
do
	case "$opt" in
	clean)
		;; # ignored, kept for backward compatibility
	rpm)
		type=rpm ;;
	debian)
		type=debian ;;
	verbose)
		verbose=true ;;
	*)
		echo "Usage: Makepkgs [verbose] [debian|rpm]"; exit 1 ;;
	esac
done

# start with a clean manifest
test -f files.rpm && rm -f files.rpm
test -f filesdevel.rpm && rm -f filesdevel.rpm
test -f fileslib.rpm && rm -f fileslib.rpm

test ! -d $LOGDIR && mkdir $LOGDIR
rm -rf $LOGDIR/* > /dev/null 2>&1

if [ $type = debian ] ; then
	SOURCE=`pwd`
	PACKAGE=`basename $SOURCE`
	LOGDEB=$SOURCE/$LOGDIR/$PACKAGE.log
	if $verbose ; then
		$MAKE include/builddefs 2>&1 | tee $LOGDIR/configure
	else
		$MAKE include/builddefs > $LOGDIR/configure 2>&1  || exit 1
	fi

	. ./VERSION
	VERSION=${PKG_MAJOR}.${PKG_MINOR}.${PKG_REVISION}
	export SRCLINK_ROOT="$SOURCE/build/deb/$PACKAGE-$VERSION"
	rm -fr "$SRCLINK_ROOT"
	mkdir -p "$SRCLINK_ROOT" || exit 1

	$MAKE source-link || exit 1

	cd "$SRCLINK_ROOT"
	SUDO=${SUDO:-fakeroot}
	test ! -z "$SUDO" && sudo=$SUDO
	if $verbose ; then
		dpkg-buildpackage -r$SUDO | tee $LOGDEB
	else
		dpkg-buildpackage -r$SUDO > $LOGDEB || exit 1
	fi
	exit 0
fi

echo "== clean, log is $LOGDIR/clean"
if $verbose ; then
	$MAKE clean 2>&1 | tee $LOGDIR/clean
else
	$MAKE clean > $LOGDIR/clean 2>&1  || exit 1
fi

echo
echo "== configure, log is $LOGDIR/configure"
rm -f .census	# force configure to run here
if $verbose ; then
	$MAKE configure 2>&1 | tee $LOGDIR/configure
else
	$MAKE configure > $LOGDIR/configure 2>&1 || exit 1
fi

echo
echo "== default, log is $LOGDIR/default"
if $verbose ; then
	$MAKE default 2>&1 | tee $LOGDIR/default
else
	$MAKE default > $LOGDIR/default 2>&1 || exit 1
fi

echo
echo "== dist, log is $LOGDIR/dist"
[ ! -f .census ] && touch .census
if $verbose ; then
	$MAKE -C build dist 2>&1 | tee $LOGDIR/dist
else
	$MAKE -C build dist > $LOGDIR/dist 2>&1 || exit 1
	grep '^Wrote:' $LOGDIR/dist | sed -e 's/\.\.\/\.\.\///'
fi

exit 0
