/* This code is adapted from busybox project
 *
 * Licensed under GPLv2
 */
#include "libbb.h"

/* From <linux/vt.h> */
struct vt_stat {
	unsigned short v_active;	/* active vt */
	unsigned short v_signal;	/* signal to send */
	unsigned short v_state;	/* vt bitmask */
};
enum { VT_GETSTATE = 0x5603 };	/* get global vt state info */

/* From <linux/serial.h> */
struct serial_struct {
	int	type;
	int	line;
	unsigned int	port;
	int	irq;
	int	flags;
	int	xmit_fifo_size;
	int	custom_divisor;
	int	baud_base;
	unsigned short	close_delay;
	char	io_type;
	char	reserved_char[1];
	int	hub6;
	unsigned short	closing_wait; /* time to wait before closing */
	unsigned short	closing_wait2; /* no longer used... */
	unsigned char	*iomem_base;
	unsigned short	iomem_reg_shift;
	unsigned int	port_high;
	unsigned long	iomap_base;	/* cookie passed into ioremap */
	int	reserved[1];
};

#define PROC_CMDLINE  "/proc/cmdline"

int cttyhack_main(int argc, char **argv) ATTRIBUTE_NORETURN;
int cttyhack_main(int argc, char **argv)
{
	int fd;
	char console[sizeof(int)*3 + 16];
	union {
		struct vt_stat vt;
		struct serial_struct sr;
		char paranoia[sizeof(struct serial_struct) * 3];
	} u;

/*  SELP patch by Geonho.Kim 09-12-08
    In UART mode, dtvlogd can't store printf string to USB.
    We want to store data in USB always. So, that is neccesary
    to detect other tty device for saving data. 
*/
	FILE *proc;
	char buf[128];
	char ttychar[12];

	/* For separate string from cmdline */
	char *f1 = NULL;
	char *f2 = NULL;
	int count, len;

	if (!*++argv) {
		bb_show_usage();
	}
	
	/* Read /proc/cmdline */
	proc = fopen(PROC_CMDLINE, "r");
	if (!proc)
	{
		printf("[busybox]ctty_hack:proc open fail\n");
		goto error;
	}

	fgets(buf, sizeof(buf), proc);

	/* Separating text that is main console device name */
	f1 = strstr(buf, "tty");

	if (f1)
		f1 += strlen("tty");
	else
	goto error;

	f2 = strstr(f1, ",");
	
	if (f2)
	{
		count = f2 - f1;
		if (count > strlen(f1))	
			count = strlen(f1);
		f1[count] = '\0';
	}
	else
	goto error;

	strncpy(ttychar, f1, sizeof(ttychar) - 1);
	len = strlen(ttychar);
	ttychar[len] = '\0';
	snprintf(console, len+9, "/dev/tty%s", ttychar);

	if (proc)
    	fclose(proc);

#if 0 
	strcpy(console, "/dev/tty");
    if (ioctl(0, TIOCGSERIAL, &u.sr) == 0) {
        /* this is a serial console */
        sprintf(console + 8, "S%d", u.sr.line);
    } else if (ioctl(0, VT_GETSTATE, &u.vt) == 0) {
        /* this is linux virtual tty */
        sprintf(console + 8, "S%d" + 1, u.vt.v_active);
    }
#endif

	if (console[8]) {
		fd = xopen(console, O_RDWR);
		if (fd) {
			dup2(fd, 0);
			dup2(fd, 1);
			dup2(fd, 2);
			while (fd > 2) close(fd--);
		}
		else
			printf("Error : xopen failed:%d\n", errno);
	}
	else
		printf("Error : Console was not setup\n");
	

	if (argv[0][0] == '-')
		execvp(argv[0]+1, argv);
	else
		execvp(argv[0], argv);

	bb_perror_msg_and_die("cannot exec '%s'", argv[0]);

error:
	if (proc)
	{
		fclose(proc);
		proc = NULL;
	}
}
