#ifndef __TRID_AES_KERNEL_H
#define __TRID_AES_KERNEL_H

void aes_init(void);
void aes_start(void);
void aes_stop(void);

int Trid_Masterkey_AES_ECB_Enc( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

int Trid_Masterkey_AES_ECB_Dec( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

int Trid_Securekey_AES_ECB_Enc( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

int Trid_Securekey_AES_ECB_Dec( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int * pu32OutDataLength);

int Trid_AES_CBC_Enc( unsigned char *key,  unsigned char * iv, unsigned int u32SrcAddr, unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

int Trid_AES_CBC_Dec( unsigned char *key,  unsigned char * iv, unsigned int u32SrcAddr, unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

//void Trid_AES_128_Enc(uint8_t  *key, uint8_t  *plainText, uint8_t  *cipherText) //ROUNDS=10
void Trid_AES_128_Enc(unsigned char *key, unsigned char *plainText, unsigned char *cipherText); //ROUNDS=10
void get_rand(unsigned int *buf, int num);
#endif
