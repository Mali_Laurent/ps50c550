/****************************************************************/
/* AES-CMAC with AES-128 bit                                    */
/* CMAC     Algorithm described in SP800-38B                    */
/* Author: Junhyuk Song (junhyuk.song@samsung.com)              */
/*         Jicheol Lee  (jicheol.lee@samsung.com)               */
/****************************************************************/

#ifndef _AES_CMAC_H_
#define _AES_CMAC_H_

#include <linux/aes.h>
#include <linux/rijndael-alg-fst.h>

#define MY_FILE_READ  sys_read
#define SZ_ENC_UNIT  (100*1024)         // Do not over 128k... 128k is limitation of kmalloc
#define SZ_AES_UNIT  16                   // Written in aes-cmac.c
#define AUTH_MAC_OFFSET	(4*1024)

#define SBOOT_NOT_NORMAL       		0xBAB0BAB0  
#define SBOOT_NORMAL       	0xFAFEF0F0  

#define SCU_PRINTK(fmt, args...) printk(KERN_INFO "[kAuth]  " fmt, ## args)
#define SCU_PRINTK_ERR(fmt, args...) printk(KERN_EMERG "[kAuth][%d]  " fmt,__LINE__ ,## args)

#define USE_AES_SW			0		
#define USE_AES_HW			1

void generate_subkey(unsigned char *key, unsigned char *K1, unsigned char *K2,int isHW);
void AES_CMAC_with_fd_and_size(unsigned char *key, int fd, int real_size, unsigned char *mac);
void Change16Order(unsigned char * input);
void xor_4_aligned(unsigned char *a, unsigned char *b, unsigned char *out, int len);
#endif
