#ifdef CONFIG_EXT3_FS_FILE_FUNC
//#define FTRUNCATERANGE		_IOW('f', 90, int)
#define FTRUNCATERANGE		0x9001
#define FSPLIT				0x9002

typedef struct {
	long long offset;
	long long end;
	char filename[64];	
} PVR_INFO;
#endif
