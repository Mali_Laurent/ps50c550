#ifndef _UBOOT_PARA_ACCESS_H_
#define _UBOOT_PARA_ACCESS_H_


#include <linux/types.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/delay.h>
#include <linux/utsname.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/smp_lock.h>
#include <linux/initrd.h>
#include <linux/hdreg.h>
#include <linux/bootmem.h>
#include <linux/tty.h>
#include <linux/gfp.h>
#include <linux/percpu.h>
#include <linux/kmod.h>
#include <linux/kernel_stat.h>
#include <linux/security.h>
#include <linux/workqueue.h>
#include <linux/profile.h>
#include <linux/rcupdate.h>
#include <linux/posix-timers.h>
#include <linux/moduleparam.h>
#include <linux/kallsyms.h>
#include <linux/writeback.h>
#include <linux/clockchips.h>
#include <linux/cpu.h>
#include <linux/cpuset.h>
#include <linux/efi.h>
#include <linux/taskstats_kern.h>
#include <linux/delayacct.h>
#include <linux/unistd.h>
#include <linux/rmap.h>
#include <linux/irq.h>
#include <linux/mempolicy.h>
#include <linux/key.h>
#include <linux/unwind.h>
#include <linux/buffer_head.h>
#include <linux/debug_locks.h>
#include <linux/lockdep.h>

#include <asm/io.h>
#include <asm/bugs.h>
#include <asm/setup.h>
#include <asm/sections.h>
#include <asm/cacheflush.h>
#include <linux/aes-cmac.h>

#define CI_CMAC_SIZE (16)
#define UBOOT_ENV_SIZE		(0)

#define KU_COMM_CHANNEL						("/dtv/.ku")
#define UK_COMM_CHANNEL						("/dtv/.uk")

#define UBOOT_PARAM_PARTITION  ("/dev/bml0/2")
#define NUM1_DATA_PART 	("/dev/bml0/5")


typedef struct {
	unsigned char mac[CI_CMAC_SIZE];	
	int msgLen;
} MacInfo_t;

typedef struct {
	MacInfo_t aa;
	MacInfo_t bb;
	MacInfo_t macAuthULD;
} macBSP_t;

typedef struct {
	int magic;
	unsigned char key[CI_CMAC_SIZE];
} cmacKey_t;

#endif //_UBOOT_PARA_ACCESS_H_

