/*
 * linux/include/asm-arm/arch-mb8aa0350/system.h
 *
 * Copyright (C) 2007 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARCH_SYSTEM_H
#define __ASM_ARCH_SYSTEM_H

#include <asm/hardware.h>
#include <linux/io.h>


static inline void arch_idle(void)
{
	/*
	 * This should do all the clock switching
	 * and wait for interrupt tricks
	 */
	cpu_do_idle();
}

static inline void arch_reset(char mode)
{
	void __iomem *crglp = __io_address(MB8AA0350_CRGLP_BASE);

	/*
	 * To reset, enable SWRSTM bit of CRRSC register
	 * and set SWRSTREQ bit of CRSWR register of the CRG11
	 * (Clock & Reset generator) module.
	 */
	__raw_writel(0x00070103, crglp + MB8AA0350_CRRSC_OFFSET);
	__raw_writel(0x00000001, crglp + MB8AA0350_CRSWR_OFFSET);
}

#endif /* __ASM_ARCH_SYSTEM_H */
