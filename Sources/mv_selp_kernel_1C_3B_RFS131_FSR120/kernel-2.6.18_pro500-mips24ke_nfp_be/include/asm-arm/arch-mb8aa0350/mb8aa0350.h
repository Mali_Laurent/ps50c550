/*
 * linux/include/asm-arm/arch-mb8aa0350/mb8aa0350.h
 *
 * Copyright (C) 2008 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARCH_MB8AA0350_H
#define __ASM_ARCH_MB8AA0350_H

/* Operationg frequency */

/* arm1176 core clock frequency */
#if ((CONFIG_MB8AA0350_MHZ == 400) | (CONFIG_MB8AA0350_MHZ == 500))

#if (CONFIG_MB8AA0350_MHZ == 400)
#define	MB8AA0350_CPU_HZ	400000000
#endif

#if (CONFIG_MB8AA0350_MHZ == 500)
#define	MB8AA0350_CPU_HZ	500000000
#endif

#else

/* consider default core frequency as : 400MHz*/
#define	MB8AA0350_CPU_HZ	400000000

#endif

#define	MB8AA0350_AXI_HZ	(MB8AA0350_CPU_HZ /  2 / 2)
#define	MB8AA0350_APB_A_HZ	(MB8AA0350_CPU_HZ /  2 / 4)
#define	MB8AA0350_APB_B_HZ	(MB8AA0350_CPU_HZ /  2 / 4)
#define	MB8AA0350_APB_TIMER_HZ	(MB8AA0350_CPU_HZ / 12 / 2)

/* Memory map (MB8AA0350) */
#define	MB8AA0350_EXTERN_BASE	0x02000000
#define	MB8AA0350_ETH_BASE	0xFFF40000
#define	MB8AA0350_XDMAC_BASE	0xFFF42000
#define	MB8AA0350_IPCU_BASE	0xFFF43000
#define	MB8AA0350_DAP_BASE	0xFFF50000
#define	MB8AA0350_CRGLP_BASE	0xFFF60000
#define	MB8AA0350_MEMC_BASE	0xFFF64000
#define	MB8AA0350_DMC_BASE	0xFFF65000
#define	MB8AA0350_MRBC_BASE	0xFFF68000
#define	MB8AA0350_GPIO_BASE	0xFFF69000
#define	MB8AA0350_UART0_BASE	0xFFF6A000
#define	MB8AA0350_UART1_BASE	0xFFF6B000
#define	MB8AA0350_UART2_BASE	0xFFF6C000
#define	MB8AA0350_GTIMER_BASE	0xFFF6E000
#define	MB8AA0350_DLLC_BASE	0xFFF6F000
#define	MB8AA0350_MCC_BASE	0xFFF70000
#define	MB8AA0350_TZPC0_BASE	0xFFF74000
#define	MB8AA0350_TZPC1_BASE	0xFFF75000
#define	MB8AA0350_TZPC2_BASE	0xFFF76000
#define	MB8AA0350_L2CC_BASE	0xFFF80000
#define	MB8AA0350_VIC0_BASE	0xFFF90000
#define	MB8AA0350_VIC1_BASE	0xFFF91000
#define	MB8AA0350_TZIC0_BASE	0xFFFA0000
#define	MB8AA0350_TZIC1_BASE	0xFFFA1000
#define	MB8AA0350_CIM_BASE	0xFFFB0000
#define	MB8AA0350_TIMER_BASE	0xFFFB4000
#define	MB8AA0350_WATCHDOG_BASE	0xFFFB5000
#define	MB8AA0350_EXIRC0_BASE	0xFFFB8000
#define	MB8AA0350_EXIRC1_BASE	0xFFFB9000
#define	MB8AA0350_EXIRC2_BASE	0xFFFBA000
#define	MB8AA0350_EXIRC3_BASE	0xFFFBB000
#define	MB8AA0350_EXIRC4_BASE	0xFFFBC000
#define	MB8AA0350_EXIRC5_BASE	0xFFFBD000

#define	MB8AA0350_CPLD_BASE	0x0FF00000

/* VIC base address */
#define	VIC0_BASE		MB8AA0350_VIC0_BASE
#define	VIC1_BASE		MB8AA0350_VIC1_BASE

/* UART base address and clock */
#define	UART0_BASE		MB8AA0350_UART0_BASE
#define	UART1_BASE		MB8AA0350_UART1_BASE
#define	UART2_BASE		MB8AA0350_UART2_BASE
#define	UART_HZ			MB8AA0350_APB_A_HZ

/* MB8AA0350 specific IRQ */
#define	IRQ_L2CC		8
#define	IRQ_IPCM3		15
#define	IRQ_EXTINT0		24
#define	IRQ_EXTINT1		25
#define	IRQ_EXTINT2		26
#define	IRQ_EXTINT3		27
#define	IRQ_EXTINT4		28
#define	IRQ_EXTINT5		29
#define	IRQ_EXTINT6		30
#define	IRQ_EXTINT7		31
#define	IRQ_EXTINT8		32
#define	IRQ_EXTINT9		33
#define	IRQ_EXTINT10		34
#define	IRQ_EXTINT11		35
#define	IRQ_EXTINT12		36
#define	IRQ_EXTINT13		37
#define	IRQ_EXTINT14		38
#define	IRQ_EXTINT15		39
#define	IRQ_EXTINT16		40
#define	IRQ_EXTINT17		41
#define	IRQ_EXTINT18		42
#define	IRQ_EXTINT19		43
#define	IRQ_EXTINT20		44
#define	IRQ_EXTINT21		45
#define	IRQ_EXTINT22		46
#define	IRQ_EXTINT23		47
#define	IRQ_GTIMER0		48
#define	IRQ_GTIMER1		49
#define	IRQ_RESERVED50		50
#define	IRQ_RESERVED51		51
#define	IRQ_DMAEXTERRIRQ	52
#define	IRQ_DMASIRQ		53
#define	IRQ_PMU			55
#define	IRQ_CTINT		57
#define	IRQ_NVALFIQ		58
#define	IRQ_NVALIRQ		59
#define	IRQ_RESERVED60		60
#define	IRQ_RESERVED61		61
#define	IRQ_RESERVED62		62
#define	IRQ_RESERVED63		63

#define	MB8AA0350_VIC0_VALID_IRQ_MASK	0xFFFFFFFF
#define	MB8AA0350_VIC1_VALID_IRQ_MASK	0x0FF3FFFF

#define	EXTINT_START		IRQ_EXTINT0
#define	EXTINT_END		IRQ_EXTINT23

/* RAM size */
#define	MB8AA0350_MEMSIZE	0x10000000

/* TIMER clock frequency */
#define TIMER_HZ		MB8AA0350_APB_TIMER_HZ

/* Ether */
#define MB8AA0350_ETH_SIZE	0x2000
#define ETH_MHZ			(MB8AA0350_AXI_HZ/1000000)

#define MB8AA0350_FMAC3H_WORKAROUND	1

/* CRG11 (Clock and reset generator module) registers */
#define MB8AA0350_CRRSC_OFFSET  0x20	/* Reset control Register */
#define MB8AA0350_CRSWR_OFFSET  0x24	/* Software reset request register */

#endif	/* __ASM_ARCH_MB8AA0350_H */

