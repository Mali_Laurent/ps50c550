/*
 * linux/include/asm-arm/arch-mb8aa0350/debug-macro.S
 *
 * Copyright (C) 2007 Fujitsu
 *
 * This program is free software ;  you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation	;  either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY ;  without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program ;  if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
	.macro addruart,rx
	mov     \rx,      #0xff000000    @ virtual address (UART0)
	orr     \rx,      #0x00f60000
	orr     \rx,      #0x0000a000
	.endm

	.macro senduart,rd,rx
	strb    \rd, [\rx, #0x00]       @ send to FIFO (little endian)
	.endm

	.macro waituart,rd,rx
10:	ldr     \rd, [\rx, #0x14]       @ line status register
	tst     \rd, #1 << 6            @ 1 when enmpty
	beq     10b
	.endm

	.macro busyuart,rd,rx
11:	ldr     \rd, [\rx, #0x14]       @ line status register
	tst     \rd, #1 << 6            @ 1 when enmpty
	beq     11b
	.endm
