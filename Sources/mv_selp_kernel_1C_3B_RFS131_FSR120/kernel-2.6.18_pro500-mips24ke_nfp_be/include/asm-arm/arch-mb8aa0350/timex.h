/*
 * linux/include/asm-arm/arch-mb8aa0350/timex.h
 *
 * Copyright (C) 2007 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARCH_TIMEX_H
#define __ASM_ARCH_TIMEX_H

#include <asm/hardware.h>

#define CLOCK_TICK_RATE		(TIMER_HZ)

/*
 * TICKS_PER_uSEC is APB cycle times for 1us
 * Expression is MB8AA0350_APB_TIMER_HZ/1000000=20.833333 and
 * be ransfered to 20. So the value is changed to 21 for less
 * error to ideal value of 20.833333.
 */
#define TICKS_PER_uSEC		(MB8AA0350_APB_TIMER_HZ/1000000)
					/* near 20.8333 for 500MHZ
					   near 16.6667 for 400MHZ*/

#endif	/* __ASM_ARCH_TIMEX_H */
