/*
 * linux/include/asm-arm/arch-mb8aa0350/uncompress.h
 *
 * Copyright (C) 2007 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARCH_UNCOMPRESS_H
#define __ASM_ARCH_UNCOMPRESS_H

#define FUART3_DR	(*(unsigned char *)0xFFF6A000)
#define FUART3_LSR	(*(unsigned char *)0xFFF6A014)

/*
 * This does not append a newline
 */
static inline void putc(int c)
{
#if 0
	while (FUART3_LSR & (1 << 5))
		barrier();

	FUART3_DR = c;
#endif
}

static inline void flush(void)
{
#if 0
	while (FUART3_LSR & (1 << 6))
		barrier();
#endif
}

#define arch_decomp_setup()
#define arch_decomp_wdog()

#endif	/* __ASM_ARCH_UNCOMPRESS_H */
