/*
 * linux/include/asm-arm/arch-mb8aa0350/irqs.h
 *
 * Copyright (C) 2007 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARM_ARCH_IRQS_H
#define __ASM_ARM_ARCH_IRQS_H

#include <asm/hardware.h>

#define IRQ_SWINT0		0
#define IRQ_SWINT1		1
#define IRQ_COMMRX		2
#define IRQ_COMMTX		3
#define IRQ_TIMER0		4
#define IRQ_TIMER1		5
#define IRQ_WATCHDOG		6
#define IRQ_ETHER		7
#define IRQ_UART0		9
#define IRQ_UART1		10
#define IRQ_UART2		11
#define IRQ_IPCM0		12
#define IRQ_IPCM1		13
#define IRQ_IPCM2		14
#define IRQ_XDMAC0		16
#define IRQ_XDMAC1		17
#define IRQ_XDMAC2		18
#define IRQ_XDMAC3	        19
#define IRQ_XDMAC4		20
#define IRQ_XDMAC5		21
#define IRQ_XDMAC6		22
#define IRQ_XDMAC7		23
#define IRQ_DMA			54
#define IRQ_PLLRDYINT		56

#define NR_IRQS			64

#define IRQMASK_TIMERINT0	(1 << IRQ_TIMER0)
#define IRQMASK_TIMERINT1	(1 << IRQ_TIMER1)
#define IRQMASK_UARTINT0	(1 << IRQ_UART0)
#define IRQMASK_UARTINT1	(1 << IRQ_UART1)
#define IRQMASK_UARTINT2	(1 << IRQ_UART2)
#define IRQMASK_EXTINT0		(1 << IRQ_EXTINT0)
#define IRQMASK_EXTINT1		(1 << IRQ_EXTINT1)
#define IRQMASK_EXTINT2		(1 << IRQ_EXTINT2)
#define IRQMASK_EXTINT3		(1 << IRQ_EXTINT3)

#endif /* __ASM_ARM_ARCH_IRQS_H */
