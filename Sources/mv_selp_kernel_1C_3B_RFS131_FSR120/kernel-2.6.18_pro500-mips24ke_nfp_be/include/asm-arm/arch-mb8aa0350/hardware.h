/*
 * linux/include/asm-arm/arch-mb8aa0350/hardware.h
 *
 * Copyright (C) 2007-2008 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARCH_HARDWARE_H
#define __ASM_ARCH_HARDWARE_H

#ifdef CONFIG_MACH_MB8AA0350_DEVKIT
#include <asm/arch/mb8aa0350.h>
#endif

#define TICKS2USECS(x)		((x) / TICKS_PER_uSEC)

/* macro to get at IO space when running virtually */
#define IO_ADDRESS(x)		((x) | 0xf0000000)
#define __io_address(n)		__io(IO_ADDRESS(n))

#endif /* __ASM_ARCH_HARDWARE_H */
