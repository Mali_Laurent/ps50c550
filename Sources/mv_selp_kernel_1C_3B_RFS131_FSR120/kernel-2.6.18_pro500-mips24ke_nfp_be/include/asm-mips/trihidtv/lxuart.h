
#ifndef _LXUART_H_
#define _LXUART_H_

#include <linux/autoconf.h>

/*
 * Formula to get LX_QUOT_DEFAULT:
 * 250M/(BAUDRATE*2)
 */
#ifdef CONFIG_TRIHIDTV_B9600
/* 9600 is slow but it works, this is the default for now */
#define LX_QUOT_DEFAULT 0x32DD
#define LX_BAUD_DEFAULT 9600		/* Default baud (9600) */
#endif /* CONFIG_TRIHIDTV_B9600 */

#ifdef CONFIG_TRIHIDTV_B19200
/* 19200 is an option, but it has only been lightly tested */
#define LX_QUOT_DEFAULT 0x196E	
#define LX_BAUD_DEFAULT 19200		/* Default baud (19200) */
#endif /* CONFIG_TRIHIDTV_B19200 */

#ifdef CONFIG_TRIHIDTV_B38400
/* 38400 is the fastest we've consistently had working. */
#define LX_QUOT_DEFAULT 0xCB7		
#define LX_BAUD_DEFAULT 38400		/* Default baud (38400) */
#endif /* CONFIG_TRIHIDTV_B38400 */

    
#ifdef CONFIG_TRIHIDTV_B57600
/* 57600 is the fastest we've had working. */
#define LX_QUOT_DEFAULT 0x87A		
#define LX_BAUD_DEFAULT 57600		/* Default baud (57600) */
#endif /* CONFIG_TRIHIDTV_B57600 */

#ifdef CONFIG_TRIHIDTV_B115200
/* 115200 is not listed in the Dev Board Users Manual, it is unsupported atm */
#define LX_QUOT_DEFAULT 0x43D		
#define LX_BAUD_DEFAULT 115200		/* Default baud (115200) */
#endif /* CONFIG_TRIHIDTV_B115200 */

#define LX_CBAUD	0xffff
//#define LX_BASE_BAUD	9216000		/* ( 1843200 / 2 ) */
/*
 * Careful here:
 * The default frequency is 250M. It is the same as CPU clock!
 * If we change the cpu clock, it will affect the baudrate.
 * How we could get the clock settings?
 * Alan Liu
 */
#define LX_BASE_BAUD	125000000	/* ( 250000000/ 2 ) */

/* UART Memory Locations */
#define LX_UART_REGS_BASE	0xb5027300
#define LX_UART_CONF		0x0
#define LX_UART_DATA		0x4

#define LX_UART_TX_READY	0x4000000
#define LX_UART_TIE		0x8000000
#define LX_UART_RX_READY	0x1000000
#define LX_UART_RIE		0x2000000

#define lx_inl(x) 	ReadRegWord(x)
#define lx_inb(x)	ReadRegByte(x)
#define lx_outb(v,x)	WriteRegByte(x,v)
#define lx_outl(v,x)	WriteRegWord(x,v)

#endif /* _LXUART_H_ */
