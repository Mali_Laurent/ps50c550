
#ifndef __LX_DEFS_H
#define __LX_DEFS_H

#include <linux/autoconf.h>
#include <asm/trihidtv/trihidtv_vectIntr.h>

#define ZMODEM_KERNEL_LOC		0x83000000
#define ZMODEM_INITRD_LOC		0x83800000

#define LX_PORT_BASE			0xa8000000	// base of PCI space

#define CMD_BUF_SIZE 512

#ifndef _LANGUAGE_ASSEMBLY
struct bootParams
{
	unsigned char *initRDBegin;
	unsigned char *initRDEnd;
	unsigned int cmd_line_size;
	char cmd_line[CMD_BUF_SIZE];
};
extern struct bootParams lxBootParams;
#endif

/**
 * Memory alias configuration registers.
 */
#define LX_PCI_BRIDGE_ALIAS0_ADDR_CONFIG	0xbc000100
#define LX_PCI_BRIDGE_ALIAS1_ADDR_CONFIG	0xbc000104

#define LX_SDRAM_REG0_ALIAS0_ADDR_CONFIG	0xbc000108
#define LX_SDRAM_REG1_ALIAS0_ADDR_CONFIG	0xbc00010c
#define LX_SDRAM_REG0_ALIAS1_ADDR_CONFIG	0xbc000110
#define LX_SDRAM_REG1_ALIAS1_ADDR_CONFIG	0xbc000114
#define LX_SDRAM_CONFIG_REG			0xbc000118

#define MSK(n)                    ((1 << (n)) - 1)

/**
 * PCI configuarion access defines.
 */
#define PCI_ACCESS_READ  0
#define PCI_ACCESS_WRITE 1

#define LX_PCI0_CFGADDR_OFS	    0xcf8
#define LX_PCI0_CFGDATA_OFS	    0xcfc
#define LX_PCI_INTR_ACK_OFS	    0xc34

#define LX_PCI0_CFGADDR_REGNUM_SHF	2
#define LX_PCI0_CFGADDR_REGNUM_MSK	(MSK(6) << LX_PCI0_CFGADDR_REGNUM_SHF)
#define LX_PCI0_CFGADDR_FUNCTNUM_SHF	8
#define LX_PCI0_CFGADDR_FUNCTNUM_MSK    (MSK(3) << LX_PCI0_CFGADDR_FUNCTNUM_SHF)
#define LX_PCI0_CFGADDR_DEVNUM_SHF	11
#define LX_PCI0_CFGADDR_DEVNUM_MSK	(MSK(5) << LX_PCI0_CFGADDR_DEVNUM_SHF)
#define LX_PCI0_CFGADDR_BUSNUM_SHF	16
#define LX_PCI0_CFGADDR_BUSNUM_MSK	(MSK(8) << LX_PCI0_CFGADDR_BUSNUM_SHF)
#define LX_PCI0_CFGADDR_CONFIGEN_SHF	31
#define LX_PCI0_CFGADDR_CONFIGEN_MSK	(MSK(1) << LX_PCI0_CFGADDR_CONFIGEN_SHF)
#define LX_PCI0_CFGADDR_CONFIGEN_BIT	LX_PCI0_CFGADDR_CONFIGEN_MSK

#define LX_EPROM_START			0xbfc00000
#define LX_MEM_MAP_CONFIG_REG

/*
 *  PCI related symbols
 */
#define LX_INTRCAUSE_TARABORT0_MSK	(MSK(1) << LX_INTRCAUSE_TARABORT0_SHF)
#define LX_INTRCAUSE_TARABORT0_BIT	LX_INTRCAUSE_TARABORT0_MSK

/**
 * Lexra controller register base.
 */
#define MIPS_LX_BASE    (KSEG1ADDR(0x14000000))		// PCI config space

/**
 * PCI bridge base addresses
 */
#define LX_PCI_BASE_0			0x08000000	/* PCI Bridge Base addresses */
#define LX_PCI_BASE_1			0x0c000000
#define LX_PCI_TOP			(LX_PCI_BASE_0 + (128<<20) - 1)  /* 128 MB from bottom */

#define LX_PCI_ALIAS_0			0x08000000	// virtual addr aliases for above
#define LX_PCI_ALIAS_1			0x0c000000

/**
 * Lexra PCI bridge configuration values.
 */
#define PCI_VENDOR_ID_LEXRA		0x0000
#define PCI_DEVICE_ID_LXPB20K		0x4146	

/** 
 * Lexra PCI configuration access macros.
 */
#define LX_PCI_WRITE(ofs, data)  \
	*(volatile u32 *)(MIPS_LX_BASE+ofs) = data
#define LX_PCI_READ(ofs, data)   \
	data = *(volatile u32 *)(MIPS_LX_BASE+ofs)
	
/*
 * Lexra Terminal/UART defines
 * LX4xxx/5xxx CPUs.
 */

#ifdef CONFIG_TRIHIDTV_B9600
/* 9600 is slow but it works, this is the default for now */
#define TRIHIDTV_BAUD_DEFAULT 9600		/* Default baud (9600) */
#endif /* CONFIG_TRIHIDTV_B9600 */

#ifdef CONFIG_TRIHIDTV_B19200
#define TRIHIDTV_BAUD_DEFAULT 19200		/* Default baud (19200) */
#endif /* CONFIG_TRIHIDTV_B19200 */

#ifdef CONFIG_TRIHIDTV_B38400
#define TRIHIDTV_BAUD_DEFAULT 38400		/* Default baud (38400) */
#endif /* CONFIG_TRIHIDTV_B38400 */

#ifdef CONFIG_TRIHIDTV_B57600
#define TRIHIDTV_BAUD_DEFAULT 57600		/* Default baud (57600) */
#endif /* CONFIG_TRIHIDTV_B57600 */

#ifdef CONFIG_TRIHIDTV_B115200
#define TRIHIDTV_BAUD_DEFAULT 115200	/* Default baud (115200) */
#endif /* CONFIG_TRIHIDTV_B115200 */

#endif /* __LX_DEFS_H */
