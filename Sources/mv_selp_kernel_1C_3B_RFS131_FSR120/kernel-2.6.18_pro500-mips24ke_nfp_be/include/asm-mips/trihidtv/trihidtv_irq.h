/*
 *
 * Alan Liu alanliu@trident.com.cn
 * TTI Shanghai
 * 09/10/2003
 * 2008-08-19	Alan Liu
 *   No need to use this file any more. 
 * @FILE
 * Miscellaneous definitions used to initialise the interrupt vector table
 * with the machine-specific interrupt routines.
 */

#ifndef __ASM_TRIHIDTV_INTERRUPTS_H 
#define __ASM_TRIHIDTV_INTERRUPTS_H 

#if 0
/**
 * Number of available Interrupts.
 */
#define NR_INTS	16

#ifdef CONFIG_MIPS_TRIHIDTV_PRO_AV_MIPS
/*
 * cpu pin definition for interrupts
 */
#define	AVTIMER_INT_PIN		15	
#define	VBS_INT0_PIN		14
#define	AUDIO_INTN_PIN		13
#define	INT_BF_NOSI_PIN		12
#define	INT_BF_SI_PIN		11
#define	LXUART_INT_PIN		10
#define	UMACDMA_INT_PIN		9
#define	MI2C_1_INT_PIN		8
#define	VIDEO_INTN_PIN		7
#define	DMINT_INT_PIN		6
#define	MI2C_0_INT_PIN		5
#define M_S_E_INT_PIN		4
#define UNMAP_INT_PIN		3
#define M_S_S_INT_PIN		2
#define	SW1_PIN			1
#define	SW0_PIN			0

/**
 * Lexra interrupt mapping.
 * ordered by priority!
 * First the Lexra vectored interrupts. 
 */
#define	AVTIMER_INT	0	//From timer
#define	VBS_INT0	1	//From DEMUX_QUARC block
#define	AUDIO_INTN	2	//From AUDIO block
#define	INT_BF_NOSI	3	//From DEMUX_QUARC
#define	INT_BF_SI	4	//From DEMUX_QUARC
#define	LXUART_INT	5	//From SIMPLE UART
#define	UMACDMA_INT	6	//From UMAC DMA pin
#define	MI2C_1_INT	7	//From MASTER I2C 1

/**
 * The MIPS standard interrupts.
 */
#define	VIDEO_INTN	8	//From VIDEO block.
#define	DMINT_INT	9	//From DEMUX_QUARC block.
#define	MI2C_0_INT	10	//From MASTER I2C 0
#define	M_S_E_INT	11	//MASTER SYSTEM tells slave,Master has gotten
				//the data slave sent out.
#define	UNMAP_INT	12	//From MASTER SYSTEM to tell slaver
#define	M_S_S_INT	13	//MASTER SYSTEM tells slave,Master has sent out
				//data to slave
#define	SW1		14	//Software Interrupt 1
#define	SW0		15	//Software Interrupt 0

#else

/*
 * cpu pin definition for interrupts
 */
#define	CPU_NMI_PIN		15	
#define	VBS_INT0_PIN		14
#define	AUDIO_INTN_PIN		13
#define	INT_BF_NOSI_PIN		12
#define	INT_BF_SI_PIN		11
#define	INT0_PIN		10
#define	EXT_INT1_PIN		9
#define	EXT_INT2_PIN		8

#define	VIDEO_INTN_PIN		7
#define	DMINT_INT_PIN		6
#define	PC_HIDTV_INT_PIN	5
#define S_M_E_INT_PIN		4
/* Reserved 3 */
#define S_M_S_INT_PIN		2
#define	SW1_PIN			1
#define	SW0_PIN			0

/**
 * Lexra interrupt mapping.
 * ordered by priority!
 * First the Lexra vectored interrupts. 
 */
#define	CPU_NMI		0	//From ITE south bridge block
#define	VBS_INT0	1	//From DEMUX_QUARC block
#define	AUDIO_INTN	2	//From AUDIO block
#define	INT_BF_NOSI	3	//From DEMUX_QUARC
#define	INT_BF_SI	4	//From DEMUX_QUARC
#define	INT0		5	//From ITE south bridge block
#define	EXT_INT1	6	//From HiDTV pin

/**
 * The MIPS standard interrupts.
 */
#define	VIDEO_INTN	8	//From VIDEO block.
#define	DMINT_INT	9	//From DEMUX_QUARC block.
#define	PC_HIDTV_INT	10	//From ITE block. It only is used as 
				//PC issue request to CPU when in pc mode.
#define S_M_E_INT	11
/* 12 reserved */
#define S_M_S_INT	13
#define	SW1		14	//Software Interrupt 1
#define	SW0		15	//Software Interrupt 0

#endif	/* CONFIG_MIPS_TRIHIDTV_PRO_AV_MIPS */

/**
 * First available interrupt number for PCI devices.
 */
#define PCI_INT_BASE	16

/**
 * Lexra vectored interrupt enable / cause bits.
 */
#define LX_IRQ8          (1<<16)
#define LX_IRQ9          (1<<17)
#define LX_IRQ10         (1<<18)
#define LX_IRQ11         (1<<19)
#define LX_IRQ12         (1<<20)
#define LX_IRQ13         (1<<21)
#define LX_IRQ14         (1<<22)
#define LX_IRQ15         (1<<23)

#ifndef __ASSEMBLY__



/**
 * Struct lxint_t
 * Data structure to hide the differences between Interrupts
 *
 * If LX_mask == NULL and cpu_mask != NULL the interrupt is directly 
 * handled by the CPU.  If LX_mask != NULL and cpu_mask == NULL this 
 * Interrupt is a Lexra vectored interrupt.  If LX_mask == NULL and 
 * cpu_mask == NULL this is a pci interrupt.
 */
typedef struct
{
	unsigned int	cpu_mask;	/* checking and enabling MIPS interrupts */
	unsigned int	LX_mask;	/* checking and enabling Lexra vectored interrupts	*/
	
} lxint_t;

/**
 * lx_interrupt[NR_INTS]
 * Array of interrupt masks.
 *
 */
extern lxint_t lx_interrupt[NR_INTS];

/**
 * Interrupt mask tables for use with the assembly language interrupt handlers.
 */
extern unsigned long cpu_mask_tbl[8];
extern unsigned long cpu_irq_nr[8];

extern unsigned long lx_irq_nr[8];
extern unsigned long lx_mask_tbl[8];

#endif

#endif

#endif 
