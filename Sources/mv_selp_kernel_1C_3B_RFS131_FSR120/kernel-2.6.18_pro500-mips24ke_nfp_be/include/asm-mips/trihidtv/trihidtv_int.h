/*
 *
 * BRIEF MODULE DESCRIPTION
 *	Trident HiDTV SOC Interrupt Numbering
 *
 * Copyright 2003 TMT Shanghai
 *
 * Alan Liu	alanliu@trident.com.cn
 *	initial release	for non-SX chips
 * Alan Liu 	2008-08-19   
 *	Set it for PRO-SX			
 */

#ifndef _MIPS_TRIHIDTVINT_H

#define _MIPS_TRIHIDTVINT_H

#define TRIHIDTV_EIC_IRQ_BASE	1	/* first EIC int number */

//Following is the physical irqs.

/* INTERRUPTS FROM HOST MIPS */
#define TRIHIDTV_24KE_PERF_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 0)
#define TRIHIDTV_24KE_TIMER_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 1)
#define TRIHIDTV_24KE_SW0_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 2)
#define TRIHIDTV_24KE_SW1_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 3)
#define TRIHIDTV_24KE_DSPDMA_IRQ	(TRIHIDTV_EIC_IRQ_BASE + 4)

#if defined(CONFIG_TRIHIDTV_CPU_350M)	/* TridentSX 1C EIC 32 source */
/* INTERRUPTS FROM AV MIPS */
#define TRIHIDTV_AVMIPS_VIRT_IRQ	(TRIHIDTV_EIC_IRQ_BASE + 5)
#define TRIHIDTV_3RDMIPS_VIRT_IRQ	(TRIHIDTV_EIC_IRQ_BASE + 6)

/* INTERRUPTS FROM INTERNAL PERIPHERAL */
#define TRIHIDTV_TIMER_0_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 7)
#define TRIHIDTV_TIMER_1_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 8)
#define TRIHIDTV_AV_TIMER_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 9)
#define TRIHIDTV_FLASH_DMA_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 10)
#define TRIHIDTV_GPIO_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 11)
#define TRIHIDTV_SCR0_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 12)
#define TRIHIDTV_CA_IRQ			(TRIHIDTV_EIC_IRQ_BASE + 13)
#define TRIHIDTV_CPUIF_NOAS_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 14)
#define TRIHIDTV_USB_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 15)
#define TRIHIDTV_SCTN_CVBSOUT_IRQ	(TRIHIDTV_EIC_IRQ_BASE + 16)
#define TRIHIDTV_ETHERNET_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 17)

/* This is reserved in SA1 */
/* #define TRIHIDTV_DEMOD_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 18) */
#define TRIHIDTV_VIDEO_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 19)
#define TRIHIDTV_STBY_RQST_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 20)
#define TRIHIDTV_STBY_RSPNS_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 21)

/*22~~26 is changed in SA1 */
/*
#define TRIHIDTV_SVP_AUDIOW_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 22)
#define TRIHIDTV_SVP_AUDIOR_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 23)
#define TRIHIDTV_PIN_SD2_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 24)
#define TRIHIDTV_HP_DETECT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 25)
#define TRIHIDTV_EXT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 26)
*/
#ifdef CONFIG_TRIHIDTV_CPU_425M
#define TRIHIDTV_GE_INTERRUPT_IRQ   (TRIHIDTV_EIC_IRQ_BASE + 22)
#define TRIHIDTV_LIPSYNC_FIFO_IRQ   (TRIHIDTV_EIC_IRQ_BASE + 23)
#define TRIHIDTV_PIN_SD2_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 24)
#define TRIHIDTV_HP_DETECT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 25)
#define TRIHIDTV_EXT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 26)
#elif CONFIG_TRIHIDTV_CPU_350M
#define TRIHIDTV_PIN_SD2_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 22)
#define TRIHIDTV_LYPSYNC_VIRT_IRQ	(TRIHIDTV_EIC_IRQ_BASE + 23)
#define TRIHIDTV_HP_DETECT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 24)
#define TRIHIDTV_EXT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 25)
#endif
/* 26 is reserved */
/* #define TRIHIDTV_EXT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 26)*/
#define TRIHIDTV_MI2C_VIRT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 27)
#define TRIHIDTV_UART_VIRT_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 28)

#define TRIHIDTV_SW1_TEST_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 29)
#define TRIHIDTV_SW0_TEST_IRQ		(TRIHIDTV_EIC_IRQ_BASE + 30)

//This is the max physical irqs.
#define TRIHIDTV_PHY_MAX_IRQ		(TRIHIDTV_SW0_TEST_IRQ)

//Following is the logical irqs.
/* Master I2C 0/1/2 share the same interrupt source */
#define TRIHIDTV_VIRT_BASE_IRQ		32
#define TRIHIDTV_MI2C0_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 0)
#define TRIHIDTV_MI2C1_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 1)
#ifdef CONFIG_TRIHIDTV_CPU_350M
#define TRIHIDTV_MI2C2_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 2)
#endif

#ifdef CONFIG_TRIHIDTV_CPU_425M
/* Master I2C 0/1 share the same interrupt source */
#define TRIHIDTV_UART0_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 2)
#define TRIHIDTV_UART1_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 3)
#define TRIHIDTV_UART2_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 4)

#define TRIHIDTV_EIC_LAST_IRQ		(TRIHIDTV_UART2_IRQ)
#elif CONFIG_TRIHIDTV_CPU_350M
/* UART 0/1/2 share the same interrupt source */
#define TRIHIDTV_UART0_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 3)
#define TRIHIDTV_UART1_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 4)
#define TRIHIDTV_UART2_IRQ		(TRIHIDTV_VIRT_BASE_IRQ + 5)

/* LYPSYNC FIFO 0/1/2/3 WR/RD share the same interrupt source */
#define TRIHIDTV_LYPSYNC0_RD_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 6)
#define TRIHIDTV_LYPSYNC0_WR_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 7)
#define TRIHIDTV_LYPSYNC1_RD_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 8)
#define TRIHIDTV_LYPSYNC1_WR_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 9)
#define TRIHIDTV_LYPSYNC2_RD_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 10)
#define TRIHIDTV_LYPSYNC2_WR_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 11)
#define TRIHIDTV_LYPSYNC3_RD_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 12)
#define TRIHIDTV_LYPSYNC3_WR_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 13)

/* these 2 share the same interrupt source for AVMIPS*/
#define TRIHIDTV_AVMIPS_RQST_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 14)
#define TRIHIDTV_AVMIPS_RSPNS_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 15)
/* these 2 share the same interrupt source for 3RDMIPS*/
#define TRIHIDTV_3RDMIPS_RQST_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 16)
#define TRIHIDTV_3RDMIPS_RSPNS_IRQ	(TRIHIDTV_VIRT_BASE_IRQ + 17)

#define TRIHIDTV_EIC_LAST_IRQ		(TRIHIDTV_3RDMIPS_RSPNS_IRQ)
#endif

#define TRIHIDTV_TIMER_IRQ		(TRIHIDTV_24KE_TIMER_IRQ)

#define SX_EIC_REG_BASE 0x15026000

/* 
 * SX EIC Registers
 */
struct sx_eic_regs {
	volatile unsigned int iscr[32];		/* interrupt source control register */
	volatile unsigned int imr;		/* interrupt mask register */
	volatile unsigned int ialr;		/* interrupt active level register */
	volatile unsigned int itmr;		/* interrupt trigger mode register */
	volatile unsigned int isr;		/* interrupt status register */
	volatile unsigned int ilcr;		/* interrupt latch clear register:Buggy: why assign i2c mask here... */
	volatile unsigned int i2cisr;		/* i2c interrupt status register */
	volatile unsigned int urtimr;		/* uart interrupt mask register */
	volatile unsigned int urtisr;		/* uart interrupt status register */
#ifdef CONFIG_TRIHIDTV_CPU_350M
	volatile unsigned int lypsimr;		/* lypsync interrupt mask register */
	volatile unsigned int lypsisr;		/* lypsync interrupt status register */
	volatile unsigned int avmipsimr;	/* AVMIPS interrupt mask register */
	volatile unsigned int avmipsisr;	/* AVMIPS interrupt status register */
	volatile unsigned int mips3rdimr;	/* 3RDMIPS interrupt mask register */
	volatile unsigned int mips3rdisr;	/* 3RDMIPS interrupt status register */
	volatile unsigned int reserved[2];	/* reserved */
#endif
	volatile unsigned int swir;		/* SW interrupt register */
};

#elif defined(CONFIG_TRIHIDTV_CPU_425M)	/* TridentSX 3B EIC 64 source */
/* INTERRUPTS FROM AV MIPS */
#define TRIHIDTV_AVMIPS_RSPNS_IRQ   (TRIHIDTV_EIC_IRQ_BASE + 5)
#define TRIHIDTV_AVMIPS_RQST_IRQ    (TRIHIDTV_EIC_IRQ_BASE + 6)

/* INTERRUPTS FROM AV MIPS */
#define TRIHIDTV_3RDMIPS_RSPNS_IRQ  (TRIHIDTV_EIC_IRQ_BASE + 7)
#define TRIHIDTV_3RDMIPS_RQST_IRQ   (TRIHIDTV_EIC_IRQ_BASE + 8)

/* INTERRUPTS FROM INTERNAL PERIPHERAL */
#define TRIHIDTV_TIMER_0_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 9)
#define TRIHIDTV_TIMER_1_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 10)
#define TRIHIDTV_AV_TIMER_IRQ       (TRIHIDTV_EIC_IRQ_BASE + 11)
#define TRIHIDTV_FLASH_DMA_IRQ      (TRIHIDTV_EIC_IRQ_BASE + 12)
#define TRIHIDTV_GPIO_IRQ       (TRIHIDTV_EIC_IRQ_BASE + 13)
#define TRIHIDTV_SCR0_IRQ       (TRIHIDTV_EIC_IRQ_BASE + 14)
#define TRIHIDTV_CA_IRQ         (TRIHIDTV_EIC_IRQ_BASE + 15)
#define TRIHIDTV_CPUIF_NOAS_IRQ     (TRIHIDTV_EIC_IRQ_BASE + 16)
#define TRIHIDTV_USB_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 17)
#define TRIHIDTV_CVBSOUT_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 18)
#define TRIHIDTV_ETHERNET_IRQ       (TRIHIDTV_EIC_IRQ_BASE + 19)
#define TRIHIDTV_USB2_IRQ       (TRIHIDTV_EIC_IRQ_BASE + 20)
#define TRIHIDTV_VIDEO_IRQ      (TRIHIDTV_EIC_IRQ_BASE + 21)
#define TRIHIDTV_STBY_RQST_IRQ      (TRIHIDTV_EIC_IRQ_BASE + 22)
#define TRIHIDTV_STBY_RSPNS_IRQ     (TRIHIDTV_EIC_IRQ_BASE + 23)
#define TRIHIDTV_GE_INTERRUPT_IRQ   (TRIHIDTV_EIC_IRQ_BASE + 24)
#define TRIHIDTV_PIN_SD2_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 25)
#define TRIHIDTV_HP_DETECT_IRQ      (TRIHIDTV_EIC_IRQ_BASE + 26)
#define TRIHIDTV_EXT_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 27)
/*
* LYP SYNC  FIFO interrupts
*/
#define TRIHIDTV_LYPSYNC0_RD_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 28)
#define TRIHIDTV_LYPSYNC0_WR_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 29)
#define TRIHIDTV_LYPSYNC1_RD_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 30)
#define TRIHIDTV_LYPSYNC1_WR_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 31)
#define TRIHIDTV_LYPSYNC2_RD_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 32)
#define TRIHIDTV_LYPSYNC2_WR_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 33)
#define TRIHIDTV_LYPSYNC3_RD_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 34)
#define TRIHIDTV_LYPSYNC3_WR_IRQ        (TRIHIDTV_EIC_IRQ_BASE + 35)
/*
* MASTER I2C interrupts
*/
#define TRIHIDTV_MI2C0_IRQ              (TRIHIDTV_EIC_IRQ_BASE + 36)
#define TRIHIDTV_MI2C1_IRQ              (TRIHIDTV_EIC_IRQ_BASE + 37)
#define TRIHIDTV_MI2C2_IRQ              (TRIHIDTV_EIC_IRQ_BASE + 38)
/*
* UART interrupts
*/
#define TRIHIDTV_UART0_IRQ              (TRIHIDTV_EIC_IRQ_BASE + 39)
#define TRIHIDTV_UART1_IRQ              (TRIHIDTV_EIC_IRQ_BASE + 40)
#define TRIHIDTV_UART2_IRQ              (TRIHIDTV_EIC_IRQ_BASE + 41)

#define TRIHIDTV_SCTN_IRQ               (TRIHIDTV_EIC_IRQ_BASE + 42)
/* 43~~60 is reserved for future use */

#define TRIHIDTV_SW1_TEST_IRQ       (TRIHIDTV_EIC_IRQ_BASE + 61)
#define TRIHIDTV_SW0_TEST_IRQ       (TRIHIDTV_EIC_IRQ_BASE + 62)

//This is the max physical irqs.
#define TRIHIDTV_PHY_MAX_IRQ        (TRIHIDTV_SW0_TEST_IRQ)

#define TRIHIDTV_EIC_LAST_IRQ       (TRIHIDTV_SW0_TEST_IRQ)

#define TRIHIDTV_TIMER_IRQ      (TRIHIDTV_24KE_TIMER_IRQ)

#define SX_EIC_REG_BASE 0x15026000


#define ISCR_NUM  64    /* 64 interrupt sources */

/* 
 * SX EIC Registers
 */
struct sx_eic_regs {
    volatile unsigned int iscr[64];     /* interrupt source control register */
        volatile unsigned long long int imr[0]; /* interrupt mask register */
    volatile unsigned int imr_h;        /* interrupt mask register */
    volatile unsigned int imr_l;        /* interrupt mask register */
    volatile unsigned long long int ialr[0];/* interrupt active level register */
    volatile unsigned int ialr_h;       /* interrupt active level register */
    volatile unsigned int ialr_l;       /* interrupt active level register */
    volatile unsigned long long int itmr[0];/* interrupt trigger mode register */
    volatile unsigned int itmr_h;       /* interrupt trigger mode register */
    volatile unsigned int itmr_l;       /* interrupt trigger mode register */
    volatile unsigned long long int isr[0]; /* interrupt status register */
    volatile unsigned int isr_h;        /* interrupt status register */
    volatile unsigned int isr_l;        /* interrupt status register */
    volatile unsigned long long int ilcr[0];/* interrupt latch clear register */
    volatile unsigned int ilcr_h;       /* interrupt latch clear register */
    volatile unsigned int ilcr_l;       /* interrupt latch clear register */
    volatile unsigned int reserved[6];  /* reserved */
    volatile unsigned int swir;     /* SW interrupt register */
};

#endif

#endif	/* _MIPS_TRIHIDTVINT_H */
