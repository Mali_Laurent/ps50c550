
/* lx_vectIntr.h
 * Lexra specific instruction defines used with Lexra vectored interrupts. 
 *
 * Definitions for the Lexra extended opcode which ccmips does not
 * recognize. It is only to be included by .s files. 
 *
 * Alan Liu
 * 09/12/2003
 *
 */

#ifndef __LX_VECTINTR_H
#define __LX_VECTINTR_H


#define MFLXCO 		0x40600000
#define MTLXCO 		0x40e00000
/*
** These are the registers used with the MTLXC0/MFLXC0 instructions.  Use
** of these registers is subject to COP0 useability exceptions.
** However, since they are not accessed by the MTC0/MFC0 instructions,
** they do not conflict with the COP0 list of register assignments.
*/
#define LX_ESTATUS	0
#define LX_ECAUSE	1
#define LX_INTVEC	2


/*
** Since Windriver assembler does not recognize Lexra proprietary opcode,
** mflxc0 and mtlxc0, they are defined below.
**  Rt takes 0 - 31 for general purpose register r0 - r31 respectively,
**  Rd takes LX_ESTATUS, LX_ECAUSE or LX_INTVEC
**
*/

#define OP_MOVELX_FORMAT(Op,Rt,Rd) \
	.word (((Op)<<21)|((Rt)<<16)|((Rd)<<11))

#define _mflxc0(Rt,Rd) OP_MOVELX_FORMAT(0x203,(Rt),(Rd))
#define _mtlxc0(Rt,Rd) OP_MOVELX_FORMAT(0x207,(Rt),(Rd))


#endif /* __LX_VECTINTR_H */
