/*
 *
 * Copyright (C) 2008 Trident Microsystems, Inc
 * Alan Liu 2008-08-13
 */
#ifndef __ASM_MACH_KERNEL_ENTRY_INIT_H
#define __ASM_MACH_KERNEL_ENTRY_INIT_H

#include <asm/cacheops.h>
#include <asm/addrspace.h>

#define INIT_ICACHE 1
#define INIT_DCACHE 1
#define INIT_TLB 1
//#define ENABLE_KSEG0_CACHABLE 1

#define CO_CONFIGPR_VALID  0x3F1F41FF    /* valid bits to write to ConfigPR */

#ifdef CONFIG_CPU_MIPS32_R2

//Alan: use this instead of more nop for 24KE
#define HAZARD_CP0 ehb
//#define HAZARD_CP0 nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;

#else

#define HAZARD_CP0 nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;

#endif

#define CACHE_OPC      		0xBC000000  /* MIPS cache instruction opcode */
#define ICACHE_LINE_SIZE        32      /* Instruction cache line size bytes */
#define DCACHE_LINE_SIZE        32      /* Data cache line size in bytes */

#define ICACHE_SET_COUNT        128     /* Instruction cache set count */
#define DCACHE_SET_COUNT        128     /* Data cache set count */

#define ICACHE_SET_SIZE         (ICACHE_SET_COUNT * ICACHE_LINE_SIZE) //4K
#define DCACHE_SET_SIZE         (DCACHE_SET_COUNT * DCACHE_LINE_SIZE) //4K

	.macro	kernel_entry_setup
	.set	push
	.set	noreorder
	/*
	 * 
	 * kernel. When loading a zImage, the head.S code in
	 * arch/mips/zboot/pnx8550 will init the caches and,
	 * decompress the kernel, and branch to kernel_entry.
		 */
cache_begin:	

        # Set up Status register:
        # Enable Coprocessor Usable bits
        # Turn off Reduce Power bit
        # Turn off reverse endian
        # Turn off BEV (use normal exception vectors)
        # Clear TS, SR, NMI bits
        # Clear Interrupt masks
        # Clear User Mode
        # Clear ERL
        # clear EXL
        # Clear Interrupt Enable
	#li	t0, (1<<28)
init_cp0_cause:
	mtc0 	zero, CP0_CAUSE
	HAZARD_CP0

init_cp0_status:
	li	t0, ST0_CU0
	mtc0	t0, CP0_STATUS /* cp0 usable */
	HAZARD_CP0

	/* CPU boots with kseg0 cache algo set to 0x2 -- uncached */

#ifdef INIT_ICACHE
init_sx_icache:
	init_icache
	nop
#endif

#ifdef INIT_DCACHE
init_sx_dcache:
	init_dcache
	nop
#endif

#ifdef INIT_TLB	
init_sx_tlb:
	# Determine if we have a TLB 
	mfc0	$11, $16

	sll	$11, 22
	srl	$11, 29

	li	$15, 0x1	# MT = 1  => TLB
	
	bne	$11, $15, 15f
	nop

	mfc0	$10, $16, 1			# .word 0x400a8001

	sll	$11, $10, 1
	srl	$11, 26		# Number of TLB entries (-1)

	mtc0	$0, $2		# EntryLo0 
	mtc0	$0, $3		# EntryLo1
	mtc0	$0, $5		# PageMask
	mtc0	$0, $6		# Wired
	
	li	$12, 0x80000000

1:	
	mtc0	$11, $0		# Index register
	mtc0	$12, $10	# EntryHi
	ssnop			#.word 0x00000040	
	ssnop			#.word 0x00000040	
	TLBWI
	add	$12, (2<<13)	# Add 8K to the address to avoid TLB conflict with previous entry

	bne	$11, $0, 1b
	add	$11, -1
	 

15:
#endif	

#if 0
	cachePr4450ICReset
	nop

	cachePr4450DCReset
	nop

	/* read ConfigPR into t0 */
	mfc0	t0, CP0_CONFIG, 7
	HAZARD_CP0

	/*  enable the TLB */
	or      t0, (1<<19)

	/* disable the ICACHE: at least 10x slower */
	/* or      t0, (1<<26) */

	/* disable the DCACHE; CONFIG_CPU_HAS_LLSC should not be set  */
	/* or      t0, (1<<27) */

	and	t0, CO_CONFIGPR_VALID

	/* enable TLB. */
	mtc0	t0, CP0_CONFIG, 7
	HAZARD_CP0

cache_end:
	/* Setup CMEM_0 to MMIO address space, 2MB */
	lui    t0, 0x1BE0
	addi   t0, t0, 0x3
	mtc0   $8, $22, 4
	nop

	/* Setup CMEM_1, 128MB */
	lui    t0, 0x1000
	addi   t0, t0, 0xf
	mtc0   $8, $22, 5
	nop


	/* Setup CMEM_2, 32MB */
	lui    t0, 0x1C00
	addi   t0, t0, 0xb
	mtc0   $8, $22, 6
	nop

	/* Setup CMEM_3, 0MB */
	lui    t0, 0x0
	addi   t0, t0, 0x0
	mtc0   $8, $22, 7
	nop
#endif

#ifdef ENABLE_KSEG0_CACHABLE

enable_cache_in_cp0_config:
	/* Enable cache */
	mfc0	t0, CP0_CONFIG
	HAZARD_CP0
	and	t0, t0, 0xFFFFFFF8
	or	t0, t0, CONF_CM_CACHABLE_NONCOHERENT
	mtc0	t0, CP0_CONFIG
	HAZARD_CP0

#endif

	.set	pop
	.endm

#ifdef INIT_ICACHE
	.macro	init_icache
	.set	push
	.set	noreorder

	/* Get Cache Configuration */
	mfc0	t3, CP0_CONFIG, 1
	HAZARD_CP0

	/* get cache Line size */

	srl   t1, t3, 19   /* C0_CONFIGPR_IL_SHIFT */
	andi  t1, t1, 0x7  /* C0_CONFIGPR_IL_MASK */
	beq   t1, zero, pro_sx_instr_cache_invalidated /* if zero instruction cache is absent */
	nop
	addiu t0, t1, 1
	ori   t1, zero, 1
	sllv  t1, t1, t0	/* t1 holds the cache line size:32 */

	/* get max cache Index */
	srl   t2, t3, 22  /* C0_CONFIGPR_IS_SHIFT */
	andi  t2, t2, 0x7 /* C0_CONFIGPR_IS_MASK */
	addiu t0, t2, 6
	ori   t2, zero, 1
	sllv  t2, t2, t0	/* t2 holds the max cache index:128 */

	/* get max cache way */
	srl   t3, t3, 16  /* C0_CONFIGPR_IA_SHIFT */
	andi  t3, t3, 0x7 /* C0_CONFIGPR_IA_MASK */
	addiu t3, t3, 1		/* t3 holds the max cache way :4*/

	/* total no of cache lines */
	multu t2, t3             /* max index * max way */
	mflo  t2		/*128 * 4 =512*/
	addiu t2, t2, -1

	#
	# Alan Liu
	# We need use Kseg0 instead of zero
	# to issue index type of ops.
#if 0
	move  t0, zero
#else
	lui   t0, 0x8000	
#endif

pro_sx_next_instruction_cache_set:
	#	
	#use Store Tag...
	#
#if 0
	cache  Index_Invalidate_I, 0(t0)
	addu  t0, t0, t1         /* add bytes in a line */
	bne   t2, zero, pro_sx_next_instruction_cache_set
	addiu t2, t2, -1   /* reduce no of lines to invalidate by one */
#else
	#cache  Index_Invalidate_I, 0(t0)
	cache  Index_Store_Tag_I, 0(t0)
	addu  t0, t0, t1         /* add bytes in a line */
	bne   t2, zero, pro_sx_next_instruction_cache_set
	addiu t2, t2, -1   /* reduce no of lines to invalidate by one */
#endif
pro_sx_instr_cache_invalidated:

	.set	pop
	.endm
#endif

#ifdef INIT_DCACHE
	.macro	init_dcache
	.set	push
	.set	noreorder

	#
	# Alan Liu
	# We need use Kseg0 instead of zero
	# to issue index type of ops.
#if 0
	move t1, zero
#else
	lui   t1, 0x8000	
#endif

	/* Store Tag Information */
	mtc0	zero, CP0_TAGLO, 0
	HAZARD_CP0
	mtc0	zero, CP0_TAGLO, 2
	HAZARD_CP0

	mtc0	zero, CP0_TAGHI, 0
	HAZARD_CP0

	/* Cache size is 16384 = 512 lines x 32 bytes per line */
	or       t2, zero, (128*4)-1  /* 512 lines  */
	/* Invalidate all lines */
2:
	cache Index_Store_Tag_D, 0(t1)
	addiu    t2, t2, -1
	bne      t2, zero, 2b
	addiu    t1, t1, DCACHE_LINE_SIZE        /* 32 bytes in a line */

	.set pop
	.endm
#endif

#if 0
	.macro	cachePr4450ICReset
	.set	push
	.set	noreorder

	/* Save CP0 status reg on entry; */
	/* disable interrupts during cache reset */
	mfc0    t0, CP0_STATUS      /* T0 = interrupt status on entry */
	HAZARD_CP0

	mtc0    zero, CP0_STATUS   /* disable CPU interrupts */
	HAZARD_CP0

	or      t1, zero, zero              /* T1 = starting cache index (0) */
	ori     t2, zero, (ICACHE_SET_COUNT - 1) /* T2=inst cache set cnt - 1 */

	icache_invd_loop:
	/* 9 == register t1 */
	.word   (CACHE_OPC | (9 << 21) | (Index_Invalidate_I << 16) | \
		(0 * ICACHE_SET_SIZE))  /* invalidate inst cache WAY0 */
	.word   (CACHE_OPC | (9 << 21) | (Index_Invalidate_I << 16) | \
		(1 * ICACHE_SET_SIZE))  /* invalidate inst cache WAY1 */

	addiu   t1, t1, ICACHE_LINE_SIZE    /* T1 = next cache line index */
	bne     t2, zero, icache_invd_loop /* T2 = 0 if all sets invalidated */
	addiu   t2, t2, -1        /* decrement T2 set cnt (delay slot) */

	/* Initialize the latches in the instruction cache tag */
	/* that drive the way selection tri-state bus drivers, by doing a */
	/* dummy load while the instruction cache is still disabled. */
	/* TODO: Is this needed ? */
	la      t1, KSEG0            /* T1 = cached memory base address */
	lw      zero, 0x0000(t1)      /* (dummy read of first memory word) */

	mtc0    t0, CP0_STATUS        /* restore interrupt status on entry */
	HAZARD_CP0
	.set	pop
	.endm

	.macro	cachePr4450DCReset
	.set	push
	.set	noreorder
	mfc0    t0, CP0_STATUS           /* T0 = interrupt status on entry */
	HAZARD_CP0
	mtc0    zero, CP0_STATUS         /* disable CPU interrupts */
	HAZARD_CP0

	/* Writeback/invalidate entire data cache sets/ways/lines */
	or      t1, zero, zero              /* T1 = starting cache index (0) */
	ori     t2, zero, (DCACHE_SET_COUNT - 1) /* T2 = data cache set cnt - 1 */

	dcache_wbinvd_loop:
	/* 9 == register t1 */
	.word   (CACHE_OPC | (9 << 21) | (Index_Writeback_Inv_D << 16) | \
		(0 * DCACHE_SET_SIZE))  /* writeback/invalidate WAY0 */
	.word   (CACHE_OPC | (9 << 21) | (Index_Writeback_Inv_D << 16) | \
		(1 * DCACHE_SET_SIZE))  /* writeback/invalidate WAY1 */
	.word   (CACHE_OPC | (9 << 21) | (Index_Writeback_Inv_D << 16) | \
		(2 * DCACHE_SET_SIZE))  /* writeback/invalidate WAY2 */
	.word   (CACHE_OPC | (9 << 21) | (Index_Writeback_Inv_D << 16) | \
		(3 * DCACHE_SET_SIZE))  /* writeback/invalidate WAY3 */

	addiu   t1, t1, DCACHE_LINE_SIZE  /* T1 = next data cache line index */
	bne     t2, zero, dcache_wbinvd_loop /* T2 = 0 when wbinvd entire cache */
	addiu   t2, t2, -1          /* decrement T2 set cnt (delay slot) */

	/* Initialize the latches in the data cache tag that drive the way
	selection tri-state bus drivers, by doing a dummy load while the
	data cache is still in the disabled mode.  TODO: Is this needed ? */
	la      t1, KSEG0            /* T1 = cached memory base address */
	lw      zero, 0x0000(t1)      /* (dummy read of first memory word) */

	mtc0    t0, CP0_STATUS       /* restore interrupt status on entry */
	HAZARD_CP0
	.set	pop
	.endm
#endif

#endif /* __ASM_MACH_KERNEL_ENTRY_INIT_H */
