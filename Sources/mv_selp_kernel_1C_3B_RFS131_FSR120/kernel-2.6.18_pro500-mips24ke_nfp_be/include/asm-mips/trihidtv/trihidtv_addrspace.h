/*
 * TTI Shanghai 2003
 * Author: Alan Liu	alanliu@trident.com.cn
 * 09/17/2003
 *
 * addrspace definitions for trident HiDTV SOC
 *
 */

#ifndef __TRIHIDTV_ADDRSPACE__H__
#define __TRIHIDTV_ADDRSPACE__H__

#include <asm/addrspace.h>

#define IO_PTR(n)                       ((volatile void *)(n))
#define TRIHIDTV_BASE			0x1B000000	

/*
 * We should skip this region,for it is allocated to
 * IDE controller!
 * Very strange that ide controller(from ITE) should
 * keep the default low 12bits.so we should skip this region
 * and assign to it.
 *
 * Alan Liu
 */
#define TRIHIDTV_PCI_IO_BASE_SKIP		0x1000

#define TRIHIDTV_PCI_IO_BASE		(0x14000000)
#define TRIHIDTV_PCI_IO_SIZE		(0x1000000)	/* 16M */

#define TRIHIDTV_PCI_MEM_BASE		0x10000000
#define TRIHIDTV_PCI_MEM_SIZE		0x4000000	/* 64M */

#define TRIHIDTV_RAM_BASE		0x0
#define TRIHIDTV_RAM_DEFAULT_SIZE	0x4000000	/* 64M ,this value may be modified in future ,need revisit here.*/

#define TRIHIDTV_FLASH_BASE		0x1c000000
#define TRIHIDTV_FLASH_SIZE		0x3c00000	/* 60M */

#define TRIHIDTV_ROM_BASE		0x1FC00000
#define TRIHIDTV_ROM_SIZE		0x400000	/* 4M */

#define TRIHIDTV_EHCI_BASE		0x1b003100
#define TRIHIDTV_EHCI_LEN		0x1000

/* SELP.4.2.1.x Add patch No.37, TridentSX3 chip porting, 2009-08-07 */
/* 
 * New HC is added into system.
 * Alan Liu 
 * 2009-06-03
 */
#define TRIHIDTV_EHCI2_BASE            0x1b007100
#define TRIHIDTV_EHCI2_LEN             0x1000

#define TRIHIDTV_EHCI_PTR               IO_PTR(TRIHIDTV_EHCI_BASE)

#define TRIHIDTV_SYSCFG_PTR             IO_PTR(0x1b000000)

// System registers offsets from TRIHIDTV_BASE
// some are not listed here.
#define TRIHIDTV_CMFPCR			0x0
#define TRIHIDTV_DSRR			0x2
#define TRIHIDTV_GECSCR			0x4
#define TRIHIDTV_CRNR			0x12
#define TRIHIDTV_CPUTR			0x14
#define TRIHIDTV_CTCR			0x16
#define TRIHIDTV_SDPR			0xF0

// Power management register offset from TRIHIDTV_PCI_IO_BASE
// Power Management Device Standby Register
/*
#define TRIHIDTV_PM_DSR			0x15800
#define TRIHIDTV_PM_DSR_TMR0SB		0x0001
#define TRIHIDTV_PM_DSR_TMR1SB		0x0002
#define TRIHIDTV_PM_DSR_CIR0SB		0x0004
#define TRIHIDTV_PM_DSR_CIR1SB		0x0008
#define TRIHIDTV_PM_DSR_SCR0SB		0x0010
#define TRIHIDTV_PM_DSR_SCR1SB		0x0020
#define TRIHIDTV_PM_DSR_PPSB		0x0040
#define TRIHIDTV_PM_DSR_I2CSB		0x0080
#define TRIHIDTV_PM_DSR_UARTSB		0x0100
#define TRIHIDTV_PM_DSR_IDESB		0x0200
#define TRIHIDTV_PM_DSR_ACSB		0x0400
#define TRIHIDTV_PM_DSR_M68KSB		0x0800

// Power Management PCI Device Software Reset Register
#define TRIHIDTV_PM_PCISR             0x15802
#define TRIHIDTV_PM_PCISR_IDESR       0x0001
#define TRIHIDTV_PM_PCISR_CDMASR      0x0002
#define TRIHIDTV_PM_PCISR_USBSR       0x0004
#define TRIHIDTV_PM_PCISR_DMASR       0x0008
#define TRIHIDTV_PM_PCISR_ACSR        0x0010
#define TRIHIDTV_PM_PCISR_MEMSR       0x0020
#define TRIHIDTV_PM_PCISR_68KSR       0x0040
*/

// PCI Configuration address and data register offsets
// from TRIHIDTV_BASE
#define TRIHIDTV_CONFADDR			0x4000
#define TRIHIDTV_CONFDATA			0x4004

#define TRIHIDTV_BUSNUM_SHF                   16
#define TRIHIDTV_DEVNUM_SHF                   11
#define TRIHIDTV_FUNCNUM_SHF                  8
#define TRIHIDTV_REGNUM_SHF                   2


// PCI configuration header common register offsets
#define TRIHIDTV_VID			0x00
#define TRIHIDTV_DID			0x02
#define TRIHIDTV_PCICMD			0x04
#define TRIHIDTV_PCISTS			0x06
#define TRIHIDTV_RID			0x08
#define TRIHIDTV_CLASSC			0x09
#define TRIHIDTV_HEADT			0x0E
#define TRIHIDTV_SERIRQC		0x49

// PCI to Internal/LPC Bus Bridge configuration header register offset
/*
#define TRIHIDTV_P2I_BCR				0x4C
#define TRIHIDTV_P2I_D0IOSC				0x50 
#define TRIHIDTV_P2I_D1IOSC				0x54
#define TRIHIDTV_P2I_D2IOSC				0x58 
#define TRIHIDTV_P2I_D3IOSC				0x5C 
#define TRIHIDTV_P2I_D4IOSC				0x60 
#define TRIHIDTV_P2I_D5IOSC				0x64 
#define TRIHIDTV_P2I_D6IOSC				0x68 
#define TRIHIDTV_P2I_D7IOSC				0x6C 
#define TRIHIDTV_P2I_D8IOSC				0x70 
#define TRIHIDTV_P2I_D9IOSC				0x74 
#define TRIHIDTV_P2I_D10IOSC				0x78 
#define TRIHIDTV_P2I_D11IOSC				0x7C 
*/

// Memory controller register offsets from TRIHIDTV_BASE
/*
#define TRIHIDTV_MC_SDRMR					0x1000
#define TRIHIDTV_MC_SDRTR					0x1004
#define TRIHIDTV_MC_MCR					0x1008
#define TRIHIDTV_MC_SDTYPE					0x100C
#define TRIHIDTV_MC_WPBA					0x1010
#define TRIHIDTV_MC_WPTA					0x1014
#define TRIHIDTV_MC_HATR					0x1018
#define TRIHIDTV_MC_PCICR					0x101C
*/

// Flash/ROM control register offsets from TRIHIDTV_BASE
/*
#define TRIHIDTV_FC_BRCR					0x2000
#define TRIHIDTV_FC_FCR					0x2004
#define TRIHIDTV_FC_DCR					0x2008
*/

// M68K interface bridge configuration header register offset
/*
#define TRIHIDTV_M68K_MBCSR					0x54
#define TRIHIDTV_M68K_TMR					0x58
#define TRIHIDTV_M68K_BCR					0x5C
#define TRIHIDTV_M68K_BSR					0x5D
#define TRIHIDTV_M68K_DTR					0x5F
*/

// Register offset from TRIHIDTV_PCI_IO_BASE 
// These registers are accessible through trihidtv PCI IO window.

// INTC 
#define TRIHIDTV_INTC_BASE				0x10000
#define TRIHIDTV_INTC_LBDNIRR				0x10000
#define TRIHIDTV_INTC_LBDNIMR				0x10002
#define TRIHIDTV_INTC_LBDNITR				0x10004
#define TRIHIDTV_INTC_LBDNIAR				0x10006
#define TRIHIDTV_INTC_PDNIRR				0x10020
#define TRIHIDTV_INTC_PDNIMR				0x10022
#define TRIHIDTV_INTC_PDNITR				0x10024
#define TRIHIDTV_INTC_PDNIAR				0x10026
#define TRIHIDTV_INTC_NMNIRR				0x10030
#define TRIHIDTV_INTC_NMNIMR				0x10032
#define TRIHIDTV_INTC_NMNITR				0x10034
#define TRIHIDTV_INTC_NMNIAR				0x10036
#define TRIHIDTV_INTC_NMNIOR				0x1003E
#define TRIHIDTV_INTC_TYPER				0x100FE

// TRIHIDTV PCI device number
#define TRIHIDTV_C2P_DEVICE				0
#define TRIHIDTV_P2U_DEVICE				1
#define TRIHIDTV_P2I_DEVICE				1
#define TRIHIDTV_IDE_DEVICE				1
#define TRIHIDTV_M68K_DEVICE				1

// TRIHIDTV PCI function number
#define TRIHIDTV_C2P_FUNCION				0
#define TRIHIDTV_P2U_FUNCION				0
#define TRIHIDTV_P2I_FUNCTION				4
#define TRIHIDTV_IDE_FUNCTION				5
#define TRIHIDTV_M68K_FUNCTION				6

// TRIHIDTV GPIO
/*
#define TRIHIDTV_GPADR				0x13800
#define TRIHIDTV_GPBDR				0x13808
#define TRIHIDTV_GPCDR				0x13810
#define TRIHIDTV_GPACR				0x13802
#define TRIHIDTV_GPBCR				0x1380A
#define TRIHIDTV_GPCCR				0x13812
#define TRIHIDTV_GPAICR				0x13804
#define TRIHIDTV_GPBICR				0x1380C
#define TRIHIDTV_GPCICR				0x13814
#define TRIHIDTV_GPAISR				0x13806
#define TRIHIDTV_GPBISR				0x1380E
#define TRIHIDTV_GPCISR				0x13816
#define TRIHIDTV_GCR				0x13818
*/

// TRIHIDTV RTC
#define TRIHIDTV_RTC_BASE				0x14800
#define TRIHIDTV_RTC_RIR0				0x00
#define TRIHIDTV_RTC_RTR0				0x01
#define TRIHIDTV_RTC_RIR1				0x02
#define TRIHIDTV_RTC_RTR1				0x03
#define TRIHIDTV_RTC_RIR2				0x04
#define TRIHIDTV_RTC_RTR2				0x05
#define TRIHIDTV_RTC_RCTR				0x08
#define TRIHIDTV_RTC_RA					0x0A
#define TRIHIDTV_RTC_RB					0x0B
#define TRIHIDTV_RTC_RC					0x0C
#define TRIHIDTV_RTC_RD					0x0D

#define RTC_SEC_INDEX				0x00
#define RTC_MIN_INDEX				0x02
#define RTC_HOUR_INDEX				0x04
#define RTC_DAY_INDEX				0x06
#define RTC_DATE_INDEX				0x07
#define RTC_MONTH_INDEX				0x08
#define RTC_YEAR_INDEX				0x09

// TRIHIDTV internal device registers
#define TRIHIDTV_TIMER_BASE				0x15027000
/*
 * Macros about timer2.
 */
#define TCVR2   KSEG1ADDR(0x1502700c)   /* Timer2 Constant Value Register */
#define TRVR2   KSEG1ADDR(0x15027010)   /* Timer2 Read value Register */
#define TCR2    KSEG1ADDR(0x15027014)   /* Timer2 Control Register */
#define TIRR    KSEG1ADDR(0x15027018)   /* Timer Interrupt Request Register */
#define TIDR    KSEG1ADDR(0x1502701c)   /* Timer Interrupt Disable Register */

#define TRIHIDTV_UART_BASE				0x11800
#define TRIHIDTV_UART1_BASE				0x14000
#define TRIHIDTV_UART2_BASE				0x12800
/*
#define TRIHIDTV_CIR0_BASE				0x11000
#define TRIHIDTV_CIR1_BASE				0x15000
*/
#define TRIHIDTV_SCR0_BASE				0x12000
#define TRIHIDTV_SCR1_BASE				0x12800
/*
#define TRIHIDTV_PP_BASE				0x13000
#define TRIHIDTV_I2C_BASE				0x14000
*/

// TRIHIDTV Smart Card Reader offsets from TRIHIDTV_SCR*_BASE
/*
#define TRIHIDTV_SCR_SFR				0x08
#define TRIHIDTV_SCR_SCDR				0x09
*/

// TRIHIDTV TRIHIDTV_SCR_SFR bit definition & mask
/*
#define	TRIHIDTV_SCR_SFR_GATE_UART			0x40
#define	TRIHIDTV_SCR_SFR_GATE_UART_BIT		6
#define TRIHIDTV_SCR_SFR_GATE_UART_OFF		0
#define	TRIHIDTV_SCR_SFR_GATE_UART_ON			1
#define	TRIHIDTV_SCR_SFR_FET_CHARGE			0x30
#define	TRIHIDTV_SCR_SFR_FET_CHARGE_BIT		4
#define TRIHIDTV_SCR_SFR_FET_CHARGE_3_3_US		3
#define TRIHIDTV_SCR_SFR_FET_CHARGE_13_US		2
#define TRIHIDTV_SCR_SFR_FET_CHARGE_53_US		1
#define TRIHIDTV_SCR_SFR_FET_CHARGE_213_US		0
#define	TRIHIDTV_SCR_SFR_CARD_FREQ			0x0C
#define	TRIHIDTV_SCR_SFR_CARD_FREQ_BIT		2
#define TRIHIDTV_SCR_SFR_CARD_FREQ_STOP		3
#define TRIHIDTV_SCR_SFR_CARD_FREQ_3_5_MHZ		0
#define TRIHIDTV_SCR_SFR_CARD_FREQ_7_1_MHZ		2
#define TRIHIDTV_SCR_SFR_CARD_FREQ_96_DIV_MHZ		1
#define	TRIHIDTV_SCR_SFR_FET_ACTIVE			0x02
#define	TRIHIDTV_SCR_SFR_FET_ACTIVE_BIT		1
#define TRIHIDTV_SCR_SFR_FET_ACTIVE_INVERT		0
#define TRIHIDTV_SCR_SFR_FET_ACTIVE_NONINVERT		1
#define	TRIHIDTV_SCR_SFR_ENABLE			0x01
#define	TRIHIDTV_SCR_SFR_ENABLE_BIT			0
#define TRIHIDTV_SCR_SFR_ENABLE_OFF			0
#define TRIHIDTV_SCR_SFR_ENABLE_ON			1
*/

// TRIHIDTV TRIHIDTV_SCR_SCDR bit definition & mask
/*
#define	TRIHIDTV_SCR_SCDR_RESET_MODE			0x80
#define	TRIHIDTV_SCR_SCDR_RESET_MODE_BIT		7
#define	TRIHIDTV_SCR_SCDR_RESET_MODE_ASYNC		0
#define TRIHIDTV_SCR_SCDR_RESET_MODE_SYNC		1
#define	TRIHIDTV_SCR_SCDR_DIVISOR			0x7F
#define	TRIHIDTV_SCR_SCDR_DIVISOR_BIT			0
#define	TRIHIDTV_SCR_SCDR_DIVISOR_STOP_VAL_1		0x00
#define	TRIHIDTV_SCR_SCDR_DIVISOR_STOP_VAL_2		0x01
#define	TRIHIDTV_SCR_SCDR_DIVISOR_STOP_VAL_3		0x7F
*/

// TRIHIDTV DMA
/*
#define TRIHIDTV_DMAC_BASE				0x16000
#define TRIHIDTV_DMAC_BCAR0				0x00
#define TRIHIDTV_DMAC_BCAR1				0x04
#define TRIHIDTV_DMAC_BCAR2				0x08
#define TRIHIDTV_DMAC_BCAR3				0x0C
#define TRIHIDTV_DMAC_BCCR0				0x02
#define TRIHIDTV_DMAC_BCCR1				0x06
#define TRIHIDTV_DMAC_BCCR2				0x0a
#define TRIHIDTV_DMAC_BCCR3				0x0e
#define TRIHIDTV_DMAC_CR				0x10
#define TRIHIDTV_DMAC_SR				0x12
#define TRIHIDTV_DMAC_ESR				0x13
#define TRIHIDTV_DMAC_RQR				0x14
#define TRIHIDTV_DMAC_MR				0x16
#define TRIHIDTV_DMAC_EMR				0x17
#define TRIHIDTV_DMAC_MKR				0x18
#define TRIHIDTV_DMAC_PAR0				0x20
#define TRIHIDTV_DMAC_PAR1				0x22
#define TRIHIDTV_DMAC_PAR2				0x24
#define TRIHIDTV_DMAC_PAR3				0x26
*/

// TRIHIDTV IDE
/*
#define TRIHIDTV_IDE_BASE				0x17800
#define TRIHIDTV_IDE_STATUS				0x1F7
*/

// TRIHIDTV Audio Controller
/*
#define TRIHIDTV_AC_BASE				0x17000
#define	TRIHIDTV_AC_PCMOV				0x00
#define TRIHIDTV_AC_FMOV				0x02
#define	TRIHIDTV_AC_I2SV				0x04
#define TRIHIDTV_AC_DRSS				0x06
#define TRIHIDTV_AC_PCC				0x08
#define TRIHIDTV_AC_PCDL				0x0A
#define TRIHIDTV_AC_PCB1STA				0x0C
#define TRIHIDTV_AC_PCB2STA				0x10
#define TRIHIDTV_AC_CAPCC				0x14
#define TRIHIDTV_AC_CAPCDL				0x16
#define TRIHIDTV_AC_CAPB1STA				0x18
#define TRIHIDTV_AC_CAPB2STA				0x1C
#define TRIHIDTV_AC_CODECC				0x22
#define TRIHIDTV_AC_I2SMC				0x24
#define TRIHIDTV_AC_VS				0x26
#define TRIHIDTV_AC_SRCS				0x28
#define TRIHIDTV_AC_CIRCP				0x2A
#define TRIHIDTV_AC_CIRDP				0x2C
#define TRIHIDTV_AC_TM				0x4A
#define TRIHIDTV_AC_PFDP				0x4C
#define TRIHIDTV_AC_GC				0x54
#define TRIHIDTV_AC_IMC				0x56
#define TRIHIDTV_AC_ISC				0x5B
#define TRIHIDTV_AC_OPL3SR				0x68
#define TRIHIDTV_AC_OPL3DWDR				0x69
#define TRIHIDTV_AC_OPL3AB1W				0x6A
#define TRIHIDTV_AC_OPL3DW				0x6B
#define TRIHIDTV_AC_BPDC				0x70
*/


// TRIHIDTV Timer
#define TRIHIDTV_TIMER_BASE			0x15027000
#define	TIMER_TCVR1				0x00
#define TIMER_TRVR1				0x02
#define	TIMER_TCR1				0x04
#define TIMER_TIRR				0x06
#define	TIMER_TCVR2				0x08
#define TIMER_TRVR2				0x0A
#define	TIMER_TCR2				0x0C
#define TIMER_TIDR				0x0E

//should use Magellan's header files 
#define TRIHIDTV_WRITE(ofs, data) *(volatile u32 *)KSEG1ADDR((TRIHIDTV_BASE+ofs)) = data
#define TRIHIDTV_READ(ofs, data)  data = *(volatile u32 *)KSEG1ADDR((TRIHIDTV_BASE+ofs))

#define TRIHIDTV_IO_WRITE(ofs, data) *(volatile u32 *)KSEG1ADDR((TRIHIDTV_PCI_IO_BASE+ofs)) = data
#define TRIHIDTV_IO_READ(ofs, data)  data = *(volatile u32 *)KSEG1ADDR((TRIHIDTV_PCI_IO_BASE+ofs))

#define TRIHIDTV_IO_WRITE16(ofs, data) *(volatile u16 *)KSEG1ADDR((TRIHIDTV_PCI_IO_BASE+ofs)) = data
#define TRIHIDTV_IO_READ16(ofs, data)  data = *(volatile u16 *)KSEG1ADDR((TRIHIDTV_PCI_IO_BASE+ofs))

#endif
