/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 1994, 95, 96, 97, 98, 99, 2000 by Ralf Baechle
 * Copyright (C) 1999, 2000 Silicon Graphics, Inc.
 */
#ifndef _ASM_PTRACE_H
#define _ASM_PTRACE_H


#include <asm/isadep.h>

/* 0 - 31 are integer registers, 32 - 63 are fp registers.  */
#define FPR_BASE	32
#define PC		64
#define CAUSE		65
#define BADVADDR	66
#define MMHI		67
#define MMLO		68
#define FPC_CSR		69
#define FPC_EIR		70
#define DSP_BASE	71		/* 3 more hi / lo register pairs */
#define DSP_CONTROL	77

/*
 * This struct defines the way the registers are stored on the stack during a
 * system call/exception. As usual the registers k0/k1 aren't being saved.
 */
struct pt_regs {
#ifdef CONFIG_32BIT
	/* Pad bytes for argument save space on the stack. */
	unsigned long pad0[6];
#endif

	/* Saved main processor registers. */
	unsigned long regs[32];

	/* Saved special registers. */
	unsigned long cp0_status;
	unsigned long hi;
	unsigned long lo;
	unsigned long cp0_badvaddr;
	unsigned long cp0_cause;
	unsigned long cp0_epc;
#ifdef CONFIG_MIPS_MT_SMTC
	unsigned long cp0_tcstatus;
	unsigned long smtc_pad;
#endif /* CONFIG_MIPS_MT_SMTC */

#ifdef CONFIG_CPU_LX45XXX
	unsigned long cp0_estatus;
	unsigned long lxr_m0l8;
	unsigned long lxr_m0h8;
#ifdef CONFIG_FULL_LXRADIAX
	unsigned long lxr_m0l32;
	unsigned long lxr_m0h32;

	unsigned long lxr_m1l32;
	unsigned long lxr_m1l8;
	unsigned long lxr_m1h32;
	unsigned long lxr_m1h8;

	unsigned long lxr_m2l32;
	unsigned long lxr_m2l8;
	unsigned long lxr_m2h32;
	unsigned long lxr_m2h8;

	unsigned long lxr_m3l32;
	unsigned long lxr_m3l8;
	unsigned long lxr_m3h32;
	unsigned long lxr_m3h8;

	unsigned long lxr_cbs0;
	unsigned long lxr_cbs1;
	unsigned long lxr_cbs2;
	unsigned long lxr_cbe0;
	unsigned long lxr_cbe1;
	unsigned long lxr_cbe2;

	unsigned long lxr_lps0;
	unsigned long lxr_lpe0;
	unsigned long lxr_lpc0;
	unsigned long lxr_mmd;
#endif /* CONFIG_FULL_LXRADIAX */
#endif /* CONFIG_CPU_LX45XXX   */
} __attribute__ ((aligned (8)));

/* Arbitrarily choose the same ptrace numbers as used by the Sparc code. */
#define PTRACE_GETREGS		12
#define PTRACE_SETREGS		13
#define PTRACE_GETFPREGS		14
#define PTRACE_SETFPREGS		15
/* #define PTRACE_GETFPXREGS		18 */
/* #define PTRACE_SETFPXREGS		19 */

#define PTRACE_OLDSETOPTIONS	21

#define PTRACE_GET_THREAD_AREA	25
#define PTRACE_SET_THREAD_AREA	26

/* Calls to trace a 64bit program from a 32bit program.  */
#define PTRACE_PEEKTEXT_3264	0xc0
#define PTRACE_PEEKDATA_3264	0xc1
#define PTRACE_POKETEXT_3264	0xc2
#define PTRACE_POKEDATA_3264	0xc3
#define PTRACE_GET_THREAD_AREA_3264	0xc4

#ifdef __KERNEL__

#include <linux/linkage.h>

/*
 * Does the process account for user or for system time?
 */
#define user_mode(regs) (((regs)->cp0_status & KU_MASK) == KU_USER)

#define instruction_pointer(regs) ((regs)->cp0_epc)
#define profile_pc(regs) instruction_pointer(regs)

extern void show_regs(struct pt_regs *);

extern asmlinkage void do_syscall_trace(struct pt_regs *regs, int entryexit);

#endif

#endif /* _ASM_PTRACE_H */
