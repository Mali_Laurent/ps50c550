#! /usr/bin/env bash
# Patch: -common_ARMv6_fix_alignment_fault
# Date: Mon Apr 27 09:45:22 2009
# Source: MontaVista Software, Inc. 
# MR: 32870
# Type: Defect Fix 
# Disposition: Backport from
# http://git.kernel.org/?p=linux/kernel/git/tmlind/linux-omap-2.6.git;a=commitdiff_plain;h=baa745a3378046ca1c5477495df6ccbec7690
# Signed-off-by: Armin Kuster <akuster@mvista.com>
# Description: Changed line number to retrofit Mob5.0 kernel.  
# 
# From: Russell King <rmk@dyn-67.arm.linux.org.uk>
# Date: Sun, 7 Dec 2008 09:44:55 +0000 (+0000)
# Subject: [ARM] Fix alignment fault handling for ARMv6 and later CPUs
# X-Git-Tag: v2.6.28-rc8~34^2
# X-Git-Url: http://source.mvista.com/git/gitweb.cgi?p=linux-omap-2.6.git;a=commitdiff_plain;h=baa745a3378046ca1c5477495df6ccbec7690428
# 
# [ARM] Fix alignment fault handling for ARMv6 and later CPUs
# 
# On ARMv6 and later CPUs, it is possible for userspace processes to
# get stuck on a misaligned load or store due to the "ignore fault"
# setting; unlike previous CPUs, retrying the instruction without
# the 'A' bit set does not always cause the load to succeed.
# 
# We have no real option but to default to fixing up alignment faults
# on these CPUs, and having the CPU fix up those misaligned accesses
# which it can.
# 
# Reported-by: Wolfgang Grandegger <wg@grandegger.com>
# Signed-off-by: Russell King <rmk+kernel@arm.linux.org.uk>
# Signed-off-by: Min Zhang <mzhang@mvista.com>

PATCHNUM=2300
LSPINFO=include/linux/lsppatchlevel.h
TMPFILE=/tmp/mvl_patch_$$

function dopatch() {
    patch $* >${TMPFILE} 2>&1 <<"EOF"
Source: MontaVista Software, Inc. 
MR: 32870
Type: Defect Fix 
Disposition: Backport from
http://git.kernel.org/?p=linux/kernel/git/tmlind/linux-omap-2.6.git;a=commitdiff_plain;h=baa745a3378046ca1c5477495df6ccbec7690
Signed-off-by: Armin Kuster <akuster@mvista.com>
Description: Changed line number to retrofit Mob5.0 kernel.  

From: Russell King <rmk@dyn-67.arm.linux.org.uk>
Date: Sun, 7 Dec 2008 09:44:55 +0000 (+0000)
Subject: [ARM] Fix alignment fault handling for ARMv6 and later CPUs
X-Git-Tag: v2.6.28-rc8~34^2
X-Git-Url: http://source.mvista.com/git/gitweb.cgi?p=linux-omap-2.6.git;a=commitdiff_plain;h=baa745a3378046ca1c5477495df6ccbec7690428

[ARM] Fix alignment fault handling for ARMv6 and later CPUs

On ARMv6 and later CPUs, it is possible for userspace processes to
get stuck on a misaligned load or store due to the "ignore fault"
setting; unlike previous CPUs, retrying the instruction without
the 'A' bit set does not always cause the load to succeed.

We have no real option but to default to fixing up alignment faults
on these CPUs, and having the CPU fix up those misaligned accesses
which it can.

Reported-by: Wolfgang Grandegger <wg@grandegger.com>
Signed-off-by: Russell King <rmk+kernel@arm.linux.org.uk>
Signed-off-by: Min Zhang <mzhang@mvista.com>
Index: linux-2.6.18/arch/arm/mm/alignment.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/alignment.c
+++ linux-2.6.18/arch/arm/mm/alignment.c
@@ -71,6 +71,10 @@ static unsigned long ai_dword;
 static unsigned long ai_multi;
 static int ai_usermode = (4 | 1);
 
+#define UM_WARN		(1 << 0)
+#define UM_FIXUP	(1 << 1)
+#define UM_SIGNAL	(1 << 2)
+
 #ifdef CONFIG_PROC_FS
 static const char *usermode_action[] = {
 	"ignored",
@@ -755,7 +759,7 @@ do_alignment(unsigned long addr, unsigne
  user:
 	ai_user += 1;
 
-	if (ai_usermode & 1)
+	if (ai_usermode & UM_WARN)
 		printk("Alignment trap: %s (%d) PC=0x%08lx Instr=0x%0*lx "
 		       "Address=0x%08lx FSR 0x%03x\n", current->comm,
 			current->pid, instrptr,
@@ -763,10 +767,10 @@ do_alignment(unsigned long addr, unsigne
 		        thumb_mode(regs) ? tinstr : instr,
 		        addr, fsr);
 
-	if (ai_usermode & 2)
+	if (ai_usermode & UM_FIXUP)
 		goto fixup;
 
-	if (ai_usermode & 4)
+	if (ai_usermode & UM_SIGNAL)
 		force_sig(SIGBUS, current);
 	else
 		set_cr(cr_no_alignment);
@@ -797,6 +801,22 @@ static int __init alignment_init(void)
 	res->write_proc = proc_alignment_write;
 #endif
 
+	/*
+	 * ARMv6 and later CPUs can perform unaligned accesses for
+	 * most single load and store instructions up to word size.
+	 * LDM, STM, LDRD and STRD still need to be handled.
+	 *
+	 * Ignoring the alignment fault is not an option on these
+	 * CPUs since we spin re-faulting the instruction without
+	 * making any progress.
+	 */
+	if (cpu_architecture() >= CPU_ARCH_ARMv6 && (cr_alignment & CR_U)) {
+		cr_alignment &= ~CR_A;
+		cr_no_alignment &= ~CR_A;
+		set_cr(cr_alignment);
+		ai_usermode = UM_FIXUP;
+	}
+
 	hook_fault_code(1, do_alignment, SIGILL, "alignment exception");
 	hook_fault_code(3, do_alignment, SIGILL, "alignment exception");
 
Index: linux-2.6.18/mvl_patches/pro50-2300.c
===================================================================
--- /dev/null
+++ linux-2.6.18/mvl_patches/pro50-2300.c
@@ -0,0 +1,16 @@
+/*
+ * Author: MontaVista Software, Inc. <source@mvista.com>
+ *
+ * 2009 (c) MontaVista Software, Inc. This file is licensed under
+ * the terms of the GNU General Public License version 2. This program
+ * is licensed "as is" without any warranty of any kind, whether express
+ * or implied.
+ */
+#include <linux/init.h>
+#include <linux/mvl_patch.h>
+
+static __init int regpatch(void)
+{
+        return mvl_register_patch(2300);
+}
+module_init(regpatch);
EOF

    rv=0
    cat /tmp/mvl_patch_$$
    if [ "$?" != "0" ]; then
	# Patch had a hard error, return 2
	rv=2
    elif grep '^Hunk' ${TMPFILE}; then
	rv=1
    fi

    rm -f ${TMPFILE}
    return $rv
}

function options() {
    echo "Options are:"
    echo "  --force-unsupported - Force the patch to be applied even if the"
    echo "      patch is out of order or the current kernel is unsupported."
    echo "      Use of this option is strongly discouraged."
    echo "  --force-apply-fuzz - If the patch has fuzz, go ahead and apply"
    echo "      it anyway.  This can occur if the patch is applied to an"
    echo "      unsupported kernel or applied out of order or if you have"
    echo "      made your own modifications to the kernel.  Use with"
    echo "      caution."
    echo "  --remove - Remove the patch"
}


function checkpatchnum() {
    local level;

    if [ ! -e ${1} ]; then
	echo "${1} does not exist, make sure you are in the kernel" 1>&2
	echo "base directory" 1>&2
	exit 1;
    fi

    # Extract the current patch number from the lsp info file.
    level=`grep '#define LSP_.*PATCH_LEVEL' ${1} | sed 's/^.*\"\\(.*\\)\".*\$/\\1/'`
    if [ "a$level" = "a" ]; then
	echo "No patch level defined in ${1}, are you sure this is" 1>&2
	echo "a valid MVL kernel LSP?" 1>&2
	exit 1;
    fi

    expr $level + 0 >/dev/null 2>&1
    isnum=$?

    # Check if the kernel is supported
    if [ "$level" = "unsupported" ]; then
	echo "**Current kernel is unsupported by MontaVista due to patches"
	echo "  begin applied out of order."
	if [ $force_unsupported == 't' ]; then
	    echo "  Application is forced, applying patch anyway"
	    unsupported=t
	    fix_patch_level=f
	else
	    echo "  Patch application aborted.  Use --force-unsupported to"
	    echo "  force the patch to be applied, but the kernel will not"
	    echo "  be supported by MontaVista."
	    exit 1;
	fi

    # Check the patch number from the lspinfo file to make sure it is
    # a valid number
    elif [ $isnum = 2 ]; then
	echo "**Patch level from ${1} was not a valid number, " 1>&2
	echo "  are you sure this is a valid MVL kernel LSP?" 1>&2
	exit 1;

    # Check that this is the right patch number to be applied.
    elif [ `expr $level $3` ${4} ${2} ]; then
	echo "**Application of this patch is out of order and will cause the"
	echo "  kernel to be unsupported by MontaVista."
	if [ $force_unsupported == 't' ]; then
	    echo "  application is forced, applying patch anyway"
	    unsupported=t
	else
	    echo "  Patch application aborted.  Please get all the patches in"
	    echo "  proper order from MontaVista Zone and apply them in order"
	    echo "  If you really want to apply this patch, use"
	    echo "  --force-unsupported to force the patch to be applied, but"
	    echo "  the kernel will not be supported by MontaVista."
	    exit 1;
	fi
    fi
}

#
# Update the patch level in the file.  Note that we use patch to do
# this.  Certain weak version control systems don't take kindly to
# arbitrary changes directly to files, but do have a special version
# of "patch" that understands this.
#
function setpatchnum() {
    sed "s/^#define LSP_\(.*\)PATCH_LEVEL[ \t*]\"[0-9]*\".*$/#define LSP_\1PATCH_LEVEL \"${2}\"/" <${1} >/tmp/$$.tmp1
    diff -u ${1} /tmp/$$.tmp1 >/tmp/$$.tmp2
    rm /tmp/$$.tmp1
    sed "s/^+++ \/tmp\/$$.tmp1/+++ include\/linux\/lsppatchlevel.h/" </tmp/$$.tmp2 >/tmp/$$.tmp1
    rm /tmp/$$.tmp2
    patch -p0 </tmp/$$.tmp1
    rm /tmp/$$.tmp1
}

force_unsupported=f
force_apply_fuzz=""
unsupported=f
fix_patch_level=t
reverse=f
common_patchnum_diff='+ 1'
common_patchnum=$PATCHNUM
patch_extraopts=''

# Extract command line parameters.
while [ $# -gt 0 ]; do
    if [ "a$1" == 'a--force-unsupported' ]; then
	force_unsupported=t
    elif [ "a$1" == 'a--force-apply-fuzz' ]; then
	force_apply_fuzz=y
    elif [ "a$1" == 'a--remove' ]; then
	reverse=t
	common_patchnum_diff=''
	common_patchnum=`expr $PATCHNUM - 1`
	patch_extraopts='--reverse'
    else
	echo "'$1' is an invalid command line parameter."
	options
	exit 1
    fi
    shift
done

echo "Checking patch level"
checkpatchnum ${LSPINFO} ${PATCHNUM} "${common_patchnum_diff}" "-ne"

if ! dopatch -p1 --dry-run --force $patch_extraopts; then
    if [ $? = 2 ]; then
	echo -n "**Patch had errors, application aborted" 1>&2
	exit 1;
    fi

    # Patch has warnings
    clean_apply=${force_apply_fuzz}
    while [ "a$clean_apply" != 'ay' -a "a$clean_apply" != 'an' ]; do
	echo -n "**Patch did not apply cleanly.  Do you still want to apply? (y/n) > "
	read clean_apply
	clean_apply=`echo "$clean_apply" | tr '[:upper:]' '[:lower:]'`
    done
    if [ $clean_apply = 'n' ]; then
	exit 1;
    fi
fi

dopatch -p1 --force $patch_extraopts

if [ $fix_patch_level = 't' ]; then 
    if [ $unsupported = 't' ]; then
	common_patchnum="unsupported"
    fi

    setpatchnum ${LSPINFO} ${common_patchnum}
fi

# Move the patch file into the mvl_patches directory if we are not reversing
if [ $reverse != 't' ]; then 
    if echo $0 | grep '/' >/dev/null; then
	# Filename is a path, either absolute or from the current directory.
	srcfile=$0
    else
	# Filename is from the path
	for i in `echo $PATH | tr ':;' '  '`; do
	    if [ -e ${i}/$0 ]; then
		srcfile=${i}/$0
	    fi
	done
    fi

    fname=`basename ${srcfile}`
    diff -uN mvl_patches/${fname} ${srcfile} | (cd mvl_patches; patch)
fi

