#! /usr/bin/env bash
# Patch: -Add_support_for_SMSC_LAN8187_and_LAN8700_PHYs
# Date: Mon Apr 27 09:45:34 2009
# Source k.o
# MR: 32748
# Type: Enhancement
# Disposition: backport
# Signed-off-by: David Singleton <dsingleton@mvista.com>
# Description:
# From 4d9b1a022a33c57ca8f31a1364cef682c8c817d6 Mon Sep 17 00:00:00 2001
# From: Steve Glendinning <steve.glendinning@smsc.com>
# Date: Mon, 28 Apr 2008 18:37:29 +0100
# Subject: [PATCH] Add support for SMSC LAN8187 and LAN8700 PHYs
# 
# Add support for two additional SMSC PHY models with identical interrupt
# source and mask registers to the LAN83C185
# 
# Signed-off-by: Steve Glendinning <steve.glendinning@smsc.com>
# Signed-off-by: Jeff Garzik <jgarzik@redhat.com>
# 

PATCHNUM=2302
LSPINFO=include/linux/lsppatchlevel.h
TMPFILE=/tmp/mvl_patch_$$

function dopatch() {
    patch $* >${TMPFILE} 2>&1 <<"EOF"
Source k.o
MR: 32748
Type: Enhancement
Disposition: backport
Signed-off-by: David Singleton <dsingleton@mvista.com>
Description:
From 4d9b1a022a33c57ca8f31a1364cef682c8c817d6 Mon Sep 17 00:00:00 2001
From: Steve Glendinning <steve.glendinning@smsc.com>
Date: Mon, 28 Apr 2008 18:37:29 +0100
Subject: [PATCH] Add support for SMSC LAN8187 and LAN8700 PHYs

Add support for two additional SMSC PHY models with identical interrupt
source and mask registers to the LAN83C185

Signed-off-by: Steve Glendinning <steve.glendinning@smsc.com>
Signed-off-by: Jeff Garzik <jgarzik@redhat.com>

Index: linux-2.6.18/drivers/net/phy/smsc.c
===================================================================
--- linux-2.6.18.orig/drivers/net/phy/smsc.c
+++ linux-2.6.18/drivers/net/phy/smsc.c
@@ -83,13 +83,78 @@ static struct phy_driver lan83c185_drive
 	.driver		= { .owner = THIS_MODULE, }
 };
 
+static struct phy_driver lan8187_driver = {
+	.phy_id		= 0x0007c0b0, /* OUI=0x00800f, Model#=0x0b */
+	.phy_id_mask	= 0xfffffff0,
+	.name		= "SMSC LAN8187",
+
+	.features	= (PHY_BASIC_FEATURES | SUPPORTED_Pause
+				| SUPPORTED_Asym_Pause),
+	.flags		= PHY_HAS_INTERRUPT | PHY_HAS_MAGICANEG,
+
+	/* basic functions */
+	.config_aneg	= genphy_config_aneg,
+	.read_status	= genphy_read_status,
+	.config_init	= smsc_phy_config_init,
+
+	/* IRQ related */
+	.ack_interrupt	= smsc_phy_ack_interrupt,
+	.config_intr	= smsc_phy_config_intr,
+
+	.driver		= { .owner = THIS_MODULE, }
+};
+
+static struct phy_driver lan8700_driver = {
+	.phy_id		= 0x0007c0c0, /* OUI=0x00800f, Model#=0x0c */
+	.phy_id_mask	= 0xfffffff0,
+	.name		= "SMSC LAN8700",
+
+	.features	= (PHY_BASIC_FEATURES | SUPPORTED_Pause
+				| SUPPORTED_Asym_Pause),
+	.flags		= PHY_HAS_INTERRUPT | PHY_HAS_MAGICANEG,
+
+	/* basic functions */
+	.config_aneg	= genphy_config_aneg,
+	.read_status	= genphy_read_status,
+	.config_init	= smsc_phy_config_init,
+
+	/* IRQ related */
+	.ack_interrupt	= smsc_phy_ack_interrupt,
+	.config_intr	= smsc_phy_config_intr,
+
+	.driver		= { .owner = THIS_MODULE, }
+};
+
 static int __init smsc_init(void)
 {
-	return phy_driver_register (&lan83c185_driver);
+	int ret;
+
+	ret = phy_driver_register (&lan83c185_driver);
+	if (ret)
+		goto err1;
+
+	ret = phy_driver_register (&lan8187_driver);
+	if (ret)
+		goto err2;
+
+	ret = phy_driver_register (&lan8700_driver);
+	if (ret)
+		goto err3;
+
+	return 0;
+
+err3:
+	phy_driver_unregister (&lan8187_driver);
+err2:
+	phy_driver_unregister (&lan83c185_driver);
+err1:
+	return ret;
 }
 
 static void __exit smsc_exit(void)
 {
+	phy_driver_unregister (&lan8700_driver);
+	phy_driver_unregister (&lan8187_driver);
 	phy_driver_unregister (&lan83c185_driver);
 }
 
Index: linux-2.6.18/drivers/net/phy/Kconfig
===================================================================
--- linux-2.6.18.orig/drivers/net/phy/Kconfig
+++ linux-2.6.18/drivers/net/phy/Kconfig
@@ -54,7 +54,7 @@ config SMSC_PHY
 	tristate "Drivers for SMSC PHYs"
 	depends on PHYLIB
 	---help---
-	  Currently supports the LAN83C185 PHY
+	  Currently supports the LAN83C185, LAN8187 and LAN8700 PHYs
 
 config ICPLUS_PHY
 	tristate "Drivers for ICPlus PHYs"
Index: linux-2.6.18/mvl_patches/pro50-2302.c
===================================================================
--- /dev/null
+++ linux-2.6.18/mvl_patches/pro50-2302.c
@@ -0,0 +1,16 @@
+/*
+ * Author: MontaVista Software, Inc. <source@mvista.com>
+ *
+ * 2009 (c) MontaVista Software, Inc. This file is licensed under
+ * the terms of the GNU General Public License version 2. This program
+ * is licensed "as is" without any warranty of any kind, whether express
+ * or implied.
+ */
+#include <linux/init.h>
+#include <linux/mvl_patch.h>
+
+static __init int regpatch(void)
+{
+        return mvl_register_patch(2302);
+}
+module_init(regpatch);
EOF

    rv=0
    cat /tmp/mvl_patch_$$
    if [ "$?" != "0" ]; then
	# Patch had a hard error, return 2
	rv=2
    elif grep '^Hunk' ${TMPFILE}; then
	rv=1
    fi

    rm -f ${TMPFILE}
    return $rv
}

function options() {
    echo "Options are:"
    echo "  --force-unsupported - Force the patch to be applied even if the"
    echo "      patch is out of order or the current kernel is unsupported."
    echo "      Use of this option is strongly discouraged."
    echo "  --force-apply-fuzz - If the patch has fuzz, go ahead and apply"
    echo "      it anyway.  This can occur if the patch is applied to an"
    echo "      unsupported kernel or applied out of order or if you have"
    echo "      made your own modifications to the kernel.  Use with"
    echo "      caution."
    echo "  --remove - Remove the patch"
}


function checkpatchnum() {
    local level;

    if [ ! -e ${1} ]; then
	echo "${1} does not exist, make sure you are in the kernel" 1>&2
	echo "base directory" 1>&2
	exit 1;
    fi

    # Extract the current patch number from the lsp info file.
    level=`grep '#define LSP_.*PATCH_LEVEL' ${1} | sed 's/^.*\"\\(.*\\)\".*\$/\\1/'`
    if [ "a$level" = "a" ]; then
	echo "No patch level defined in ${1}, are you sure this is" 1>&2
	echo "a valid MVL kernel LSP?" 1>&2
	exit 1;
    fi

    expr $level + 0 >/dev/null 2>&1
    isnum=$?

    # Check if the kernel is supported
    if [ "$level" = "unsupported" ]; then
	echo "**Current kernel is unsupported by MontaVista due to patches"
	echo "  begin applied out of order."
	if [ $force_unsupported == 't' ]; then
	    echo "  Application is forced, applying patch anyway"
	    unsupported=t
	    fix_patch_level=f
	else
	    echo "  Patch application aborted.  Use --force-unsupported to"
	    echo "  force the patch to be applied, but the kernel will not"
	    echo "  be supported by MontaVista."
	    exit 1;
	fi

    # Check the patch number from the lspinfo file to make sure it is
    # a valid number
    elif [ $isnum = 2 ]; then
	echo "**Patch level from ${1} was not a valid number, " 1>&2
	echo "  are you sure this is a valid MVL kernel LSP?" 1>&2
	exit 1;

    # Check that this is the right patch number to be applied.
    elif [ `expr $level $3` ${4} ${2} ]; then
	echo "**Application of this patch is out of order and will cause the"
	echo "  kernel to be unsupported by MontaVista."
	if [ $force_unsupported == 't' ]; then
	    echo "  application is forced, applying patch anyway"
	    unsupported=t
	else
	    echo "  Patch application aborted.  Please get all the patches in"
	    echo "  proper order from MontaVista Zone and apply them in order"
	    echo "  If you really want to apply this patch, use"
	    echo "  --force-unsupported to force the patch to be applied, but"
	    echo "  the kernel will not be supported by MontaVista."
	    exit 1;
	fi
    fi
}

#
# Update the patch level in the file.  Note that we use patch to do
# this.  Certain weak version control systems don't take kindly to
# arbitrary changes directly to files, but do have a special version
# of "patch" that understands this.
#
function setpatchnum() {
    sed "s/^#define LSP_\(.*\)PATCH_LEVEL[ \t*]\"[0-9]*\".*$/#define LSP_\1PATCH_LEVEL \"${2}\"/" <${1} >/tmp/$$.tmp1
    diff -u ${1} /tmp/$$.tmp1 >/tmp/$$.tmp2
    rm /tmp/$$.tmp1
    sed "s/^+++ \/tmp\/$$.tmp1/+++ include\/linux\/lsppatchlevel.h/" </tmp/$$.tmp2 >/tmp/$$.tmp1
    rm /tmp/$$.tmp2
    patch -p0 </tmp/$$.tmp1
    rm /tmp/$$.tmp1
}

force_unsupported=f
force_apply_fuzz=""
unsupported=f
fix_patch_level=t
reverse=f
common_patchnum_diff='+ 1'
common_patchnum=$PATCHNUM
patch_extraopts=''

# Extract command line parameters.
while [ $# -gt 0 ]; do
    if [ "a$1" == 'a--force-unsupported' ]; then
	force_unsupported=t
    elif [ "a$1" == 'a--force-apply-fuzz' ]; then
	force_apply_fuzz=y
    elif [ "a$1" == 'a--remove' ]; then
	reverse=t
	common_patchnum_diff=''
	common_patchnum=`expr $PATCHNUM - 1`
	patch_extraopts='--reverse'
    else
	echo "'$1' is an invalid command line parameter."
	options
	exit 1
    fi
    shift
done

echo "Checking patch level"
checkpatchnum ${LSPINFO} ${PATCHNUM} "${common_patchnum_diff}" "-ne"

if ! dopatch -p1 --dry-run --force $patch_extraopts; then
    if [ $? = 2 ]; then
	echo -n "**Patch had errors, application aborted" 1>&2
	exit 1;
    fi

    # Patch has warnings
    clean_apply=${force_apply_fuzz}
    while [ "a$clean_apply" != 'ay' -a "a$clean_apply" != 'an' ]; do
	echo -n "**Patch did not apply cleanly.  Do you still want to apply? (y/n) > "
	read clean_apply
	clean_apply=`echo "$clean_apply" | tr '[:upper:]' '[:lower:]'`
    done
    if [ $clean_apply = 'n' ]; then
	exit 1;
    fi
fi

dopatch -p1 --force $patch_extraopts

if [ $fix_patch_level = 't' ]; then 
    if [ $unsupported = 't' ]; then
	common_patchnum="unsupported"
    fi

    setpatchnum ${LSPINFO} ${common_patchnum}
fi

# Move the patch file into the mvl_patches directory if we are not reversing
if [ $reverse != 't' ]; then 
    if echo $0 | grep '/' >/dev/null; then
	# Filename is a path, either absolute or from the current directory.
	srcfile=$0
    else
	# Filename is from the path
	for i in `echo $PATH | tr ':;' '  '`; do
	    if [ -e ${i}/$0 ]; then
		srcfile=${i}/$0
	    fi
	done
    fi

    fname=`basename ${srcfile}`
    diff -uN mvl_patches/${fname} ${srcfile} | (cd mvl_patches; patch)
fi

