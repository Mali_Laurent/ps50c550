#! /usr/bin/env bash
# Patch: -ARM_Unuse_another_Linux_PTE_bit
# Date: Mon Apr 27 09:46:33 2009
# Source: K.O
# MR: 32995
# Type: Defect Fix 
# Disposition: backport
# Signed-off-by: Armin Kuster <akuster@mvista.com>
# Description:
# This fixes a segfault when looping
# dd if=/dev/zero of=test.bin bs=100k count=1
# 
# From ad1ae2fe7fe68414ef29eab3c87b48841f8b72f2 Mon Sep 17 00:00:00 2001
# From: Russell King <rmk@dyn-67.arm.linux.org.uk>
# Date: Wed, 13 Dec 2006 14:34:43 +0000
# Subject: [PATCH] [ARM] Unuse another Linux PTE bit
# 
# L_PTE_ASID is not really required to be stored in every PTE, since we
# can identify it via the address passed to set_pte_at().  So, create
# set_pte_ext() which takes the address of the PTE to set, the Linux
# PTE value, and the additional CPU PTE bits which aren't encoded in
# the Linux PTE value.
# 
# Signed-off-by: Russell King <rmk+kernel@arm.linux.org.uk>

PATCHNUM=2314
LSPINFO=include/linux/lsppatchlevel.h
TMPFILE=/tmp/mvl_patch_$$

function dopatch() {
    patch $* >${TMPFILE} 2>&1 <<"EOF"
Source: K.O
MR: 32995
Type: Defect Fix 
Disposition: backport
Signed-off-by: Armin Kuster <akuster@mvista.com>
Description:
This fixes a segfault when looping
dd if=/dev/zero of=test.bin bs=100k count=1

From ad1ae2fe7fe68414ef29eab3c87b48841f8b72f2 Mon Sep 17 00:00:00 2001
From: Russell King <rmk@dyn-67.arm.linux.org.uk>
Date: Wed, 13 Dec 2006 14:34:43 +0000
Subject: [PATCH] [ARM] Unuse another Linux PTE bit

L_PTE_ASID is not really required to be stored in every PTE, since we
can identify it via the address passed to set_pte_at().  So, create
set_pte_ext() which takes the address of the PTE to set, the Linux
PTE value, and the additional CPU PTE bits which aren't encoded in
the Linux PTE value.

Signed-off-by: Russell King <rmk+kernel@arm.linux.org.uk>
Index: linux-2.6.18/arch/arm/mm/consistent.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/consistent.c
+++ linux-2.6.18/arch/arm/mm/consistent.c
@@ -239,7 +239,7 @@ __dma_alloc(struct device *dev, size_t s
 			 * x86 does not mark the pages reserved...
 			 */
 			SetPageReserved(page);
-			set_pte(pte, mk_pte(page, prot));
+			set_pte_ext(pte, mk_pte(page, prot), 0);
 			page++;
 			pte++;
 			off++;
Index: linux-2.6.18/arch/arm/mm/copypage-v4mc.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/copypage-v4mc.c
+++ linux-2.6.18/arch/arm/mm/copypage-v4mc.c
@@ -77,7 +77,7 @@ void v4_mc_copy_user_page(void *kto, con
 
 	spin_lock(&minicache_lock);
 
-	set_pte(TOP_PTE(0xffff8000), pfn_pte(__pa(kfrom) >> PAGE_SHIFT, minicache_pgprot));
+	set_pte_ext(TOP_PTE(0xffff8000), pfn_pte(__pa(kfrom) >> PAGE_SHIFT, minicache_pgprot), 0);
 	flush_tlb_kernel_page(0xffff8000);
 
 	mc_copy_user_page((void *)0xffff8000, kto);
Index: linux-2.6.18/arch/arm/mm/copypage-v6.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/copypage-v6.c
+++ linux-2.6.18/arch/arm/mm/copypage-v6.c
@@ -74,8 +74,8 @@ static void v6_copy_user_page_aliasing(v
 	 */
 	spin_lock(&v6_lock);
 
-	set_pte(TOP_PTE(from_address) + offset, pfn_pte(__pa(kfrom) >> PAGE_SHIFT, PAGE_KERNEL));
-	set_pte(TOP_PTE(to_address) + offset, pfn_pte(__pa(kto) >> PAGE_SHIFT, PAGE_KERNEL));
+	set_pte_ext(TOP_PTE(from_address) + offset, pfn_pte(__pa(kfrom) >> PAGE_SHIFT, PAGE_KERNEL), 0);
+	set_pte_ext(TOP_PTE(to_address) + offset, pfn_pte(__pa(kto) >> PAGE_SHIFT, PAGE_KERNEL), 0);
 
 	from = from_address + (offset << PAGE_SHIFT);
 	to   = to_address + (offset << PAGE_SHIFT);
@@ -114,7 +114,7 @@ static void v6_clear_user_page_aliasing(
 	 */
 	spin_lock(&v6_lock);
 
-	set_pte(TOP_PTE(to_address) + offset, pfn_pte(__pa(kaddr) >> PAGE_SHIFT, PAGE_KERNEL));
+	set_pte_ext(TOP_PTE(to_address) + offset, pfn_pte(__pa(kaddr) >> PAGE_SHIFT, PAGE_KERNEL), 0);
 	flush_tlb_kernel_page(to);
 	clear_page((void *)to);
 
Index: linux-2.6.18/arch/arm/mm/copypage-xscale.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/copypage-xscale.c
+++ linux-2.6.18/arch/arm/mm/copypage-xscale.c
@@ -99,7 +99,7 @@ void xscale_mc_copy_user_page(void *kto,
 
 	spin_lock(&minicache_lock);
 
-	set_pte(TOP_PTE(COPYPAGE_MINICACHE), pfn_pte(__pa(kfrom) >> PAGE_SHIFT, minicache_pgprot));
+	set_pte_ext(TOP_PTE(COPYPAGE_MINICACHE), pfn_pte(__pa(kfrom) >> PAGE_SHIFT, minicache_pgprot), 0);
 	flush_tlb_kernel_page(COPYPAGE_MINICACHE);
 
 	mc_copy_user_page((void *)COPYPAGE_MINICACHE, kto);
Index: linux-2.6.18/arch/arm/mm/fault-armv.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/fault-armv.c
+++ linux-2.6.18/arch/arm/mm/fault-armv.c
@@ -61,7 +61,7 @@ static int adjust_pte(struct vm_area_str
 	if (pte_present(entry) && pte_val(entry) & shared_pte_mask) {
 		flush_cache_page(vma, address, pte_pfn(entry));
 		pte_val(entry) &= ~shared_pte_mask;
-		set_pte(pte, entry);
+		set_pte_at(vma->vm_mm, address, pte, entry);
 		flush_tlb_page(vma, address);
 		ret = 1;
 	}
Index: linux-2.6.18/arch/arm/mm/flush.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/flush.c
+++ linux-2.6.18/arch/arm/mm/flush.c
@@ -26,7 +26,7 @@ static void flush_pfn_alias(unsigned lon
 	unsigned long to = ALIAS_FLUSH_START + (CACHE_COLOUR(vaddr) << PAGE_SHIFT);
 	const int zero = 0;
 
-	set_pte(TOP_PTE(to), pfn_pte(pfn, PAGE_KERNEL));
+	set_pte_ext(TOP_PTE(to), pfn_pte(pfn, PAGE_KERNEL), 0);
 	flush_tlb_kernel_page(to);
 
 	asm(	"mcrr	p15, 0, %1, %0, c14\n"
Index: linux-2.6.18/arch/arm/mm/ioremap.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/ioremap.c
+++ linux-2.6.18/arch/arm/mm/ioremap.c
@@ -40,7 +40,7 @@
 
 static inline void
 remap_area_pte(pte_t * pte, unsigned long address, unsigned long size,
-	       unsigned long phys_addr, pgprot_t pgprot)
+	       unsigned long phys_addr, pgprot_t prot)
 {
 	unsigned long end;
 
@@ -53,7 +53,7 @@ remap_area_pte(pte_t * pte, unsigned lon
 		if (!pte_none(*pte))
 			goto bad;
 
-		set_pte(pte, pfn_pte(phys_addr >> PAGE_SHIFT, pgprot));
+		set_pte_ext(pte, pfn_pte(phys_addr >> PAGE_SHIFT, prot), 0);
 		address += PAGE_SIZE;
 		phys_addr += PAGE_SIZE;
 		pte++;
Index: linux-2.6.18/arch/arm/mm/mmu.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/mmu.c
+++ linux-2.6.18/arch/arm/mm/mmu.c
@@ -294,12 +294,6 @@ static void __init build_mem_type_table(
 		mem_types[MT_DEVICE].prot_pte |= L_PTE_BUFFERABLE;
 		mem_types[MT_DEVICE].prot_sect |= PMD_SECT_BUFFERED;
 
-		/*
-		 * User pages need to be mapped with the ASID
-		 * (iow, non-global)
-		 */
-		user_pgprot |= L_PTE_ASID;
-
 #ifdef CONFIG_SMP
 		/*
 		 * Mark memory with the "shared" attribute for SMP systems
@@ -408,7 +402,7 @@ alloc_init_page(unsigned long virt, unsi
 	}
 	ptep = pte_offset_kernel(pmdp, virt);
 
-	set_pte(ptep, pfn_pte(phys >> PAGE_SHIFT, prot));
+	set_pte_ext(ptep, pfn_pte(phys >> PAGE_SHIFT, prot), 0);
 }
 
 /*
Index: linux-2.6.18/arch/arm/mm/pgd.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/pgd.c
+++ linux-2.6.18/arch/arm/mm/pgd.c
@@ -57,7 +57,7 @@ pgd_t *get_pgd_slow(struct mm_struct *mm
 
 		init_pmd = pmd_offset(init_pgd, 0);
 		init_pte = pte_offset_map_nested(init_pmd, 0);
-		set_pte(new_pte, *init_pte);
+		set_pte_ext(new_pte, *init_pte, 0);
 		pte_unmap_nested(init_pte);
 		pte_unmap(new_pte);
 	}
Index: linux-2.6.18/arch/arm/mm/proc-arm1020.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm1020.S
+++ linux-2.6.18/arch/arm/mm/proc-arm1020.S
@@ -397,7 +397,7 @@ ENTRY(cpu_arm1020_switch_mm)
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_arm1020_set_pte)
+ENTRY(cpu_arm1020_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -477,7 +477,7 @@ arm1020_processor_functions:
 	.word	cpu_arm1020_do_idle
 	.word	cpu_arm1020_dcache_clean_area
 	.word	cpu_arm1020_switch_mm
-	.word	cpu_arm1020_set_pte
+	.word	cpu_arm1020_set_pte_ext
 	.size	arm1020_processor_functions, . - arm1020_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-arm1020e.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm1020e.S
+++ linux-2.6.18/arch/arm/mm/proc-arm1020e.S
@@ -381,7 +381,7 @@ ENTRY(cpu_arm1020e_switch_mm)
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_arm1020e_set_pte)
+ENTRY(cpu_arm1020e_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -458,7 +458,7 @@ arm1020e_processor_functions:
 	.word	cpu_arm1020e_do_idle
 	.word	cpu_arm1020e_dcache_clean_area
 	.word	cpu_arm1020e_switch_mm
-	.word	cpu_arm1020e_set_pte
+	.word	cpu_arm1020e_set_pte_ext
 	.size	arm1020e_processor_functions, . - arm1020e_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-arm1022.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm1022.S
+++ linux-2.6.18/arch/arm/mm/proc-arm1022.S
@@ -358,12 +358,12 @@ ENTRY(cpu_arm1022_switch_mm)
 	mov	pc, lr
         
 /*
- * cpu_arm1022_set_pte(ptep, pte)
+ * cpu_arm1022_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_arm1022_set_pte)
+ENTRY(cpu_arm1022_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -441,7 +441,7 @@ arm1022_processor_functions:
 	.word	cpu_arm1022_do_idle
 	.word	cpu_arm1022_dcache_clean_area
 	.word	cpu_arm1022_switch_mm
-	.word	cpu_arm1022_set_pte
+	.word	cpu_arm1022_set_pte_ext
 	.size	arm1022_processor_functions, . - arm1022_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-arm1026.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm1026.S
+++ linux-2.6.18/arch/arm/mm/proc-arm1026.S
@@ -347,12 +347,12 @@ ENTRY(cpu_arm1026_switch_mm)
 	mov	pc, lr
         
 /*
- * cpu_arm1026_set_pte(ptep, pte)
+ * cpu_arm1026_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_arm1026_set_pte)
+ENTRY(cpu_arm1026_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -436,7 +436,7 @@ arm1026_processor_functions:
 	.word	cpu_arm1026_do_idle
 	.word	cpu_arm1026_dcache_clean_area
 	.word	cpu_arm1026_switch_mm
-	.word	cpu_arm1026_set_pte
+	.word	cpu_arm1026_set_pte_ext
 	.size	arm1026_processor_functions, . - arm1026_processor_functions
 
 	.section .rodata
Index: linux-2.6.18/arch/arm/mm/proc-arm6_7.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm6_7.S
+++ linux-2.6.18/arch/arm/mm/proc-arm6_7.S
@@ -209,14 +209,14 @@ ENTRY(cpu_arm7_switch_mm)
 		mov	pc, lr
 
 /*
- * Function: arm6_7_set_pte(pte_t *ptep, pte_t pte)
+ * Function: arm6_7_set_pte_ext(pte_t *ptep, pte_t pte, unsigned int ext)
  * Params  : r0 = Address to set
  *	   : r1 = value to set
  * Purpose : Set a PTE and flush it out of any WB cache
  */
 		.align	5
-ENTRY(cpu_arm6_set_pte)
-ENTRY(cpu_arm7_set_pte)
+ENTRY(cpu_arm6_set_pte_ext)
+ENTRY(cpu_arm7_set_pte_ext)
 #ifdef CONFIG_MMU
 		str	r1, [r0], #-2048		@ linux version
 
@@ -299,7 +299,7 @@ ENTRY(arm6_processor_functions)
 		.word	cpu_arm6_do_idle
 		.word	cpu_arm6_dcache_clean_area
 		.word	cpu_arm6_switch_mm
-		.word	cpu_arm6_set_pte
+		.word	cpu_arm6_set_pte_ext
 		.size	arm6_processor_functions, . - arm6_processor_functions
 
 /*
@@ -315,7 +315,7 @@ ENTRY(arm7_processor_functions)
 		.word	cpu_arm7_do_idle
 		.word	cpu_arm7_dcache_clean_area
 		.word	cpu_arm7_switch_mm
-		.word	cpu_arm7_set_pte
+		.word	cpu_arm7_set_pte_ext
 		.size	arm7_processor_functions, . - arm7_processor_functions
 
 		.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-arm720.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm720.S
+++ linux-2.6.18/arch/arm/mm/proc-arm720.S
@@ -88,13 +88,13 @@ ENTRY(cpu_arm720_switch_mm)
 		mov	pc, lr
 
 /*
- * Function: arm720_set_pte(pte_t *ptep, pte_t pte)
+ * Function: arm720_set_pte_ext(pte_t *ptep, pte_t pte, unsigned int ext)
  * Params  : r0 = Address to set
  *	   : r1 = value to set
  * Purpose : Set a PTE and flush it out of any WB cache
  */
 		.align	5
-ENTRY(cpu_arm720_set_pte)
+ENTRY(cpu_arm720_set_pte_ext)
 #ifdef CONFIG_MMU
 		str	r1, [r0], #-2048		@ linux version
 
@@ -204,7 +204,7 @@ ENTRY(arm720_processor_functions)
 		.word	cpu_arm720_do_idle
 		.word	cpu_arm720_dcache_clean_area
 		.word	cpu_arm720_switch_mm
-		.word	cpu_arm720_set_pte
+		.word	cpu_arm720_set_pte_ext
 		.size	arm720_processor_functions, . - arm720_processor_functions
 
 		.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-arm920.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm920.S
+++ linux-2.6.18/arch/arm/mm/proc-arm920.S
@@ -344,12 +344,12 @@ ENTRY(cpu_arm920_switch_mm)
 	mov	pc, lr
 
 /*
- * cpu_arm920_set_pte(ptep, pte)
+ * cpu_arm920_set_pte(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_arm920_set_pte)
+ENTRY(cpu_arm920_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -423,7 +423,7 @@ arm920_processor_functions:
 	.word   cpu_arm920_do_idle
 	.word	cpu_arm920_dcache_clean_area
 	.word	cpu_arm920_switch_mm
-	.word	cpu_arm920_set_pte
+	.word	cpu_arm920_set_pte_ext
 	.size	arm920_processor_functions, . - arm920_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-arm922.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm922.S
+++ linux-2.6.18/arch/arm/mm/proc-arm922.S
@@ -348,12 +348,12 @@ ENTRY(cpu_arm922_switch_mm)
 	mov	pc, lr
 
 /*
- * cpu_arm922_set_pte(ptep, pte)
+ * cpu_arm922_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_arm922_set_pte)
+ENTRY(cpu_arm922_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -427,7 +427,7 @@ arm922_processor_functions:
 	.word   cpu_arm922_do_idle
 	.word	cpu_arm922_dcache_clean_area
 	.word	cpu_arm922_switch_mm
-	.word	cpu_arm922_set_pte
+	.word	cpu_arm922_set_pte_ext
 	.size	arm922_processor_functions, . - arm922_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-arm925.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm925.S
+++ linux-2.6.18/arch/arm/mm/proc-arm925.S
@@ -391,12 +391,12 @@ ENTRY(cpu_arm925_switch_mm)
 	mov	pc, lr
 
 /*
- * cpu_arm925_set_pte(ptep, pte)
+ * cpu_arm925_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_arm925_set_pte)
+ENTRY(cpu_arm925_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -490,7 +490,7 @@ arm925_processor_functions:
 	.word   cpu_arm925_do_idle
 	.word	cpu_arm925_dcache_clean_area
 	.word	cpu_arm925_switch_mm
-	.word	cpu_arm925_set_pte
+	.word	cpu_arm925_set_pte_ext
 	.size	arm925_processor_functions, . - arm925_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-arm926.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-arm926.S
+++ linux-2.6.18/arch/arm/mm/proc-arm926.S
@@ -348,12 +348,12 @@ ENTRY(cpu_arm926_switch_mm)
 	mov	pc, lr
 
 /*
- * cpu_arm926_set_pte(ptep, pte)
+ * cpu_arm926_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_arm926_set_pte)
+ENTRY(cpu_arm926_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -439,7 +439,7 @@ arm926_processor_functions:
 	.word	cpu_arm926_do_idle
 	.word	cpu_arm926_dcache_clean_area
 	.word	cpu_arm926_switch_mm
-	.word	cpu_arm926_set_pte
+	.word	cpu_arm926_set_pte_ext
 	.size	arm926_processor_functions, . - arm926_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-sa110.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-sa110.S
+++ linux-2.6.18/arch/arm/mm/proc-sa110.S
@@ -146,12 +146,12 @@ ENTRY(cpu_sa110_switch_mm)
 #endif
 
 /*
- * cpu_sa110_set_pte(ptep, pte)
+ * cpu_sa110_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_sa110_set_pte)
+ENTRY(cpu_sa110_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -222,7 +222,7 @@ ENTRY(sa110_processor_functions)
 	.word	cpu_sa110_do_idle
 	.word	cpu_sa110_dcache_clean_area
 	.word	cpu_sa110_switch_mm
-	.word	cpu_sa110_set_pte
+	.word	cpu_sa110_set_pte_ext
 	.size	sa110_processor_functions, . - sa110_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-sa1100.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-sa1100.S
+++ linux-2.6.18/arch/arm/mm/proc-sa1100.S
@@ -159,12 +159,12 @@ ENTRY(cpu_sa1100_switch_mm)
 #endif
 
 /*
- * cpu_sa1100_set_pte(ptep, pte)
+ * cpu_sa1100_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  */
 	.align	5
-ENTRY(cpu_sa1100_set_pte)
+ENTRY(cpu_sa1100_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
@@ -237,7 +237,7 @@ ENTRY(sa1100_processor_functions)
 	.word	cpu_sa1100_do_idle
 	.word	cpu_sa1100_dcache_clean_area
 	.word	cpu_sa1100_switch_mm
-	.word	cpu_sa1100_set_pte
+	.word	cpu_sa1100_set_pte_ext
 	.size	sa1100_processor_functions, . - sa1100_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-syms.c
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-syms.c
+++ linux-2.6.18/arch/arm/mm/proc-syms.c
@@ -17,7 +17,7 @@
 
 #ifndef MULTI_CPU
 EXPORT_SYMBOL(cpu_dcache_clean_area);
-EXPORT_SYMBOL(cpu_set_pte);
+EXPORT_SYMBOL(cpu_set_pte_ext);
 #else
 EXPORT_SYMBOL(processor);
 #endif
Index: linux-2.6.18/arch/arm/mm/proc-v6.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-v6.S
+++ linux-2.6.18/arch/arm/mm/proc-v6.S
@@ -103,13 +103,14 @@ ENTRY(cpu_v6_switch_mm)
 	mov	pc, lr
 
 /*
- *	cpu_v6_set_pte(ptep, pte)
+ *	cpu_v6_set_pte_ext(ptep, pte, ext)
  *
  *	Set a level 2 translation table entry.
  *
  *	- ptep  - pointer to level 2 translation table entry
  *		  (hardware version is stored at -1024 bytes)
  *	- pte   - PTE value to store
+ *	- ext	- value for extended PTE bits
  *
  *	Permissions:
  *	  YUWD  APX AP1 AP0	SVC	User
@@ -121,33 +122,34 @@ ENTRY(cpu_v6_switch_mm)
  *	  11x0   0   1   0	r/w	r/o
  *	  1111   0   1   1	r/w	r/w
  */
-ENTRY(cpu_v6_set_pte)
+ENTRY(cpu_v6_set_pte_ext)
 #ifdef CONFIG_MMU
 	str	r1, [r0], #-2048		@ linux version
 
-	bic	r2, r1, #0x000003f0
-	bic	r2, r2, #0x00000003
-	orr	r2, r2, #PTE_EXT_AP0 | 2
+	bic	r3, r1, #0x000003f0
+	bic	r3, r3, #0x00000003
+	orr	r3, r3, r2
+	orr	r3, r3, #PTE_EXT_AP0 | 2
 
 	tst	r1, #L_PTE_WRITE
 	tstne	r1, #L_PTE_DIRTY
-	orreq	r2, r2, #PTE_EXT_APX
+	orreq	r3, r3, #PTE_EXT_APX
 
 	tst	r1, #L_PTE_USER
-	orrne	r2, r2, #PTE_EXT_AP1
-	tstne	r2, #PTE_EXT_APX
-	bicne	r2, r2, #PTE_EXT_APX | PTE_EXT_AP0
+	orrne	r3, r3, #PTE_EXT_AP1
+	tstne	r3, #PTE_EXT_APX
+	bicne	r3, r3, #PTE_EXT_APX | PTE_EXT_AP0
 
 	tst	r1, #L_PTE_YOUNG
-	biceq	r2, r2, #PTE_EXT_APX | PTE_EXT_AP_MASK
+	biceq	r3, r3, #PTE_EXT_APX | PTE_EXT_AP_MASK
 
 	tst	r1, #L_PTE_EXEC
-	orreq	r2, r2, #PTE_EXT_XN
+	orreq	r3, r3, #PTE_EXT_XN
 
 	tst	r1, #L_PTE_PRESENT
-	moveq	r2, #0
+	moveq	r3, #0
 
-	str	r2, [r0]
+	str	r3, [r0]
 	mcr	p15, 0, r0, c7, c10, 1 @ flush_pte
 #endif
 	mov	pc, lr
@@ -238,7 +240,7 @@ ENTRY(v6_processor_functions)
 	.word	cpu_v6_do_idle
 	.word	cpu_v6_dcache_clean_area
 	.word	cpu_v6_switch_mm
-	.word	cpu_v6_set_pte
+	.word	cpu_v6_set_pte_ext
 	.size	v6_processor_functions, . - v6_processor_functions
 
 	.type	cpu_arch_name, #object
Index: linux-2.6.18/arch/arm/mm/proc-xsc3.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-xsc3.S
+++ linux-2.6.18/arch/arm/mm/proc-xsc3.S
@@ -362,13 +362,13 @@ ENTRY(cpu_xsc3_switch_mm)
 	cpwait_ret lr, ip
 
 /*
- * cpu_xsc3_set_pte(ptep, pte)
+ * cpu_xsc3_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  *
  */
 	.align	5
-ENTRY(cpu_xsc3_set_pte)
+ENTRY(cpu_xsc3_set_pte_ext)
 	str	r1, [r0], #-2048		@ linux version
 
 	bic	r2, r1, #0xdf0			@ Keep C, B, coherency bits
@@ -462,7 +462,7 @@ ENTRY(xsc3_processor_functions)
 	.word	cpu_xsc3_do_idle
 	.word	cpu_xsc3_dcache_clean_area
 	.word	cpu_xsc3_switch_mm
-	.word	cpu_xsc3_set_pte
+	.word	cpu_xsc3_set_pte_ext
 	.size	xsc3_processor_functions, . - xsc3_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/arch/arm/mm/proc-xscale.S
===================================================================
--- linux-2.6.18.orig/arch/arm/mm/proc-xscale.S
+++ linux-2.6.18/arch/arm/mm/proc-xscale.S
@@ -421,14 +421,14 @@ ENTRY(cpu_xscale_switch_mm)
 	cpwait_ret lr, ip
 
 /*
- * cpu_xscale_set_pte(ptep, pte)
+ * cpu_xscale_set_pte_ext(ptep, pte, ext)
  *
  * Set a PTE and flush it out
  *
  * Errata 40: must set memory to write-through for user read-only pages.
  */
 	.align	5
-ENTRY(cpu_xscale_set_pte)
+ENTRY(cpu_xscale_set_pte_ext)
 	str	r1, [r0], #-2048		@ linux version
 
 	bic	r2, r1, #0xff0
@@ -540,7 +540,7 @@ ENTRY(xscale_processor_functions)
 	.word	cpu_xscale_do_idle
 	.word	cpu_xscale_dcache_clean_area
 	.word	cpu_xscale_switch_mm
-	.word	cpu_xscale_set_pte
+	.word	cpu_xscale_set_pte_ext
 	.size	xscale_processor_functions, . - xscale_processor_functions
 
 	.section ".rodata"
Index: linux-2.6.18/include/asm-arm/cpu-multi32.h
===================================================================
--- linux-2.6.18.orig/include/asm-arm/cpu-multi32.h
+++ linux-2.6.18/include/asm-arm/cpu-multi32.h
@@ -50,9 +50,10 @@ extern struct processor {
 	 */
 	void (*switch_mm)(unsigned long pgd_phys, struct mm_struct *mm);
 	/*
-	 * Set a PTE
+	 * Set a possibly extended PTE.  Non-extended PTEs should
+	 * ignore 'ext'.
 	 */
-	void (*set_pte)(pte_t *ptep, pte_t pte);
+	void (*set_pte_ext)(pte_t *ptep, pte_t pte, unsigned int ext);
 } processor;
 
 #define cpu_proc_init()			processor._proc_init()
@@ -60,5 +61,5 @@ extern struct processor {
 #define cpu_reset(addr)			processor.reset(addr)
 #define cpu_do_idle()			processor._do_idle()
 #define cpu_dcache_clean_area(addr,sz)	processor.dcache_clean_area(addr,sz)
-#define cpu_set_pte(ptep, pte)		processor.set_pte(ptep, pte)
+#define cpu_set_pte_ext(ptep,pte,ext)	processor.set_pte_ext(ptep,pte,ext)
 #define cpu_do_switch_mm(pgd,mm)	processor.switch_mm(pgd,mm)
Index: linux-2.6.18/include/asm-arm/cpu-single.h
===================================================================
--- linux-2.6.18.orig/include/asm-arm/cpu-single.h
+++ linux-2.6.18/include/asm-arm/cpu-single.h
@@ -28,7 +28,7 @@
 #define cpu_do_idle			__cpu_fn(CPU_NAME,_do_idle)
 #define cpu_dcache_clean_area		__cpu_fn(CPU_NAME,_dcache_clean_area)
 #define cpu_do_switch_mm		__cpu_fn(CPU_NAME,_switch_mm)
-#define cpu_set_pte			__cpu_fn(CPU_NAME,_set_pte)
+#define cpu_set_pte_ext			__cpu_fn(CPU_NAME,_set_pte_ext)
 
 #include <asm/page.h>
 
@@ -40,5 +40,5 @@ extern void cpu_proc_fin(void);
 extern int cpu_do_idle(void);
 extern void cpu_dcache_clean_area(void *, int);
 extern void cpu_do_switch_mm(unsigned long pgd_phys, struct mm_struct *mm);
-extern void cpu_set_pte(pte_t *ptep, pte_t pte);
+extern void cpu_set_pte_ext(pte_t *ptep, pte_t pte, unsigned int ext);
 extern void cpu_reset(unsigned long addr) __attribute__((noreturn));
Index: linux-2.6.18/include/asm-arm/pgtable.h
===================================================================
--- linux-2.6.18.orig/include/asm-arm/pgtable.h
+++ linux-2.6.18/include/asm-arm/pgtable.h
@@ -21,6 +21,7 @@
 
 #include <asm/memory.h>
 #include <asm/arch/vmalloc.h>
+#include <asm/pgtable-hwdef.h>
 
 /*
  * Just any arbitrary offset to the start of the vmalloc VM area: the
@@ -171,7 +172,6 @@ extern void __pgd_error(const char *file
 #define L_PTE_DIRTY		(1 << 7)
 #define L_PTE_COHERENT		(1 << 9)	/* I/O coherent (xsc3) */
 #define L_PTE_SHARED		(1 << 10)	/* shared between CPUs (v6) */
-#define L_PTE_ASID		(1 << 11)	/* non-global (use ASID, v6) */
 
 #ifndef __ASSEMBLY__
 
@@ -229,7 +229,7 @@ extern struct page *empty_zero_page;
 #define pfn_pte(pfn,prot)	(__pte(((pfn) << PAGE_SHIFT) | pgprot_val(prot)))
 
 #define pte_none(pte)		(!pte_val(pte))
-#define pte_clear(mm,addr,ptep)	set_pte_at((mm),(addr),(ptep), __pte(0))
+#define pte_clear(mm,addr,ptep)	set_pte_ext(ptep, __pte(0), 0)
 #define pte_page(pte)		(pfn_to_page(pte_pfn(pte)))
 #define pte_offset_kernel(dir,addr)	(pmd_page_vaddr(*(dir)) + __pte_index(addr))
 #define pte_offset_map(dir,addr)	(pmd_page_vaddr(*(dir)) + __pte_index(addr))
@@ -237,8 +237,11 @@ extern struct page *empty_zero_page;
 #define pte_unmap(pte)		do { } while (0)
 #define pte_unmap_nested(pte)	do { } while (0)
 
-#define set_pte(ptep, pte)	cpu_set_pte(ptep,pte)
-#define set_pte_at(mm,addr,ptep,pteval) set_pte(ptep,pteval)
+#define set_pte_ext(ptep,pte,ext) cpu_set_pte_ext(ptep,pte,ext)
+
+#define set_pte_at(mm,addr,ptep,pteval) do { \
+	set_pte_ext(ptep, pteval, (addr) >= PAGE_OFFSET ? 0 : PTE_EXT_NG); \
+ } while (0)
 
 /*
  * The following only work if pte_present() is true.
Index: linux-2.6.18/mvl_patches/pro50-2314.c
===================================================================
--- /dev/null
+++ linux-2.6.18/mvl_patches/pro50-2314.c
@@ -0,0 +1,16 @@
+/*
+ * Author: MontaVista Software, Inc. <source@mvista.com>
+ *
+ * 2009 (c) MontaVista Software, Inc. This file is licensed under
+ * the terms of the GNU General Public License version 2. This program
+ * is licensed "as is" without any warranty of any kind, whether express
+ * or implied.
+ */
+#include <linux/init.h>
+#include <linux/mvl_patch.h>
+
+static __init int regpatch(void)
+{
+        return mvl_register_patch(2314);
+}
+module_init(regpatch);
EOF

    rv=0
    cat /tmp/mvl_patch_$$
    if [ "$?" != "0" ]; then
	# Patch had a hard error, return 2
	rv=2
    elif grep '^Hunk' ${TMPFILE}; then
	rv=1
    fi

    rm -f ${TMPFILE}
    return $rv
}

function options() {
    echo "Options are:"
    echo "  --force-unsupported - Force the patch to be applied even if the"
    echo "      patch is out of order or the current kernel is unsupported."
    echo "      Use of this option is strongly discouraged."
    echo "  --force-apply-fuzz - If the patch has fuzz, go ahead and apply"
    echo "      it anyway.  This can occur if the patch is applied to an"
    echo "      unsupported kernel or applied out of order or if you have"
    echo "      made your own modifications to the kernel.  Use with"
    echo "      caution."
    echo "  --remove - Remove the patch"
}


function checkpatchnum() {
    local level;

    if [ ! -e ${1} ]; then
	echo "${1} does not exist, make sure you are in the kernel" 1>&2
	echo "base directory" 1>&2
	exit 1;
    fi

    # Extract the current patch number from the lsp info file.
    level=`grep '#define LSP_.*PATCH_LEVEL' ${1} | sed 's/^.*\"\\(.*\\)\".*\$/\\1/'`
    if [ "a$level" = "a" ]; then
	echo "No patch level defined in ${1}, are you sure this is" 1>&2
	echo "a valid MVL kernel LSP?" 1>&2
	exit 1;
    fi

    expr $level + 0 >/dev/null 2>&1
    isnum=$?

    # Check if the kernel is supported
    if [ "$level" = "unsupported" ]; then
	echo "**Current kernel is unsupported by MontaVista due to patches"
	echo "  begin applied out of order."
	if [ $force_unsupported == 't' ]; then
	    echo "  Application is forced, applying patch anyway"
	    unsupported=t
	    fix_patch_level=f
	else
	    echo "  Patch application aborted.  Use --force-unsupported to"
	    echo "  force the patch to be applied, but the kernel will not"
	    echo "  be supported by MontaVista."
	    exit 1;
	fi

    # Check the patch number from the lspinfo file to make sure it is
    # a valid number
    elif [ $isnum = 2 ]; then
	echo "**Patch level from ${1} was not a valid number, " 1>&2
	echo "  are you sure this is a valid MVL kernel LSP?" 1>&2
	exit 1;

    # Check that this is the right patch number to be applied.
    elif [ `expr $level $3` ${4} ${2} ]; then
	echo "**Application of this patch is out of order and will cause the"
	echo "  kernel to be unsupported by MontaVista."
	if [ $force_unsupported == 't' ]; then
	    echo "  application is forced, applying patch anyway"
	    unsupported=t
	else
	    echo "  Patch application aborted.  Please get all the patches in"
	    echo "  proper order from MontaVista Zone and apply them in order"
	    echo "  If you really want to apply this patch, use"
	    echo "  --force-unsupported to force the patch to be applied, but"
	    echo "  the kernel will not be supported by MontaVista."
	    exit 1;
	fi
    fi
}

#
# Update the patch level in the file.  Note that we use patch to do
# this.  Certain weak version control systems don't take kindly to
# arbitrary changes directly to files, but do have a special version
# of "patch" that understands this.
#
function setpatchnum() {
    sed "s/^#define LSP_\(.*\)PATCH_LEVEL[ \t*]\"[0-9]*\".*$/#define LSP_\1PATCH_LEVEL \"${2}\"/" <${1} >/tmp/$$.tmp1
    diff -u ${1} /tmp/$$.tmp1 >/tmp/$$.tmp2
    rm /tmp/$$.tmp1
    sed "s/^+++ \/tmp\/$$.tmp1/+++ include\/linux\/lsppatchlevel.h/" </tmp/$$.tmp2 >/tmp/$$.tmp1
    rm /tmp/$$.tmp2
    patch -p0 </tmp/$$.tmp1
    rm /tmp/$$.tmp1
}

force_unsupported=f
force_apply_fuzz=""
unsupported=f
fix_patch_level=t
reverse=f
common_patchnum_diff='+ 1'
common_patchnum=$PATCHNUM
patch_extraopts=''

# Extract command line parameters.
while [ $# -gt 0 ]; do
    if [ "a$1" == 'a--force-unsupported' ]; then
	force_unsupported=t
    elif [ "a$1" == 'a--force-apply-fuzz' ]; then
	force_apply_fuzz=y
    elif [ "a$1" == 'a--remove' ]; then
	reverse=t
	common_patchnum_diff=''
	common_patchnum=`expr $PATCHNUM - 1`
	patch_extraopts='--reverse'
    else
	echo "'$1' is an invalid command line parameter."
	options
	exit 1
    fi
    shift
done

echo "Checking patch level"
checkpatchnum ${LSPINFO} ${PATCHNUM} "${common_patchnum_diff}" "-ne"

if ! dopatch -p1 --dry-run --force $patch_extraopts; then
    if [ $? = 2 ]; then
	echo -n "**Patch had errors, application aborted" 1>&2
	exit 1;
    fi

    # Patch has warnings
    clean_apply=${force_apply_fuzz}
    while [ "a$clean_apply" != 'ay' -a "a$clean_apply" != 'an' ]; do
	echo -n "**Patch did not apply cleanly.  Do you still want to apply? (y/n) > "
	read clean_apply
	clean_apply=`echo "$clean_apply" | tr '[:upper:]' '[:lower:]'`
    done
    if [ $clean_apply = 'n' ]; then
	exit 1;
    fi
fi

dopatch -p1 --force $patch_extraopts

if [ $fix_patch_level = 't' ]; then 
    if [ $unsupported = 't' ]; then
	common_patchnum="unsupported"
    fi

    setpatchnum ${LSPINFO} ${common_patchnum}
fi

# Move the patch file into the mvl_patches directory if we are not reversing
if [ $reverse != 't' ]; then 
    if echo $0 | grep '/' >/dev/null; then
	# Filename is a path, either absolute or from the current directory.
	srcfile=$0
    else
	# Filename is from the path
	for i in `echo $PATH | tr ':;' '  '`; do
	    if [ -e ${i}/$0 ]; then
		srcfile=${i}/$0
	    fi
	done
    fi

    fname=`basename ${srcfile}`
    diff -uN mvl_patches/${fname} ${srcfile} | (cd mvl_patches; patch)
fi

