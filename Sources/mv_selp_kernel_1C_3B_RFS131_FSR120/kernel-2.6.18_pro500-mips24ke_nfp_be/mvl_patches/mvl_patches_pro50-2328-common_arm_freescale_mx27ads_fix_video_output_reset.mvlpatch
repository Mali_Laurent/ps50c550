#! /usr/bin/env bash
# Patch: -common_arm_freescale_mx27ads_fix_video_output_reset
# Date: Thu May  7 10:18:16 2009
# Source: MontaVista
# MR: 32469
# Type: BugFix
# Disposition: local
# Signed-off-by: Aleksey Makarov <amakarov@ru.mvista.com>
# Description:
# 
# 	Reset encoder each time it is turned on.
# 

PATCHNUM=2328
LSPINFO=include/linux/lsppatchlevel.h
TMPFILE=/tmp/mvl_patch_$$

function dopatch() {
    patch $* >${TMPFILE} 2>&1 <<"EOF"
Source: MontaVista
MR: 32469
Type: BugFix
Disposition: local
Signed-off-by: Aleksey Makarov <amakarov@ru.mvista.com>
Description:

	Reset encoder each time it is turned on.

Index: linux-2.6.18/drivers/video/mxc/fs453.c
===================================================================
--- linux-2.6.18.orig/drivers/video/mxc/fs453.c
+++ linux-2.6.18/drivers/video/mxc/fs453.c
@@ -444,11 +444,6 @@ static int fs453_enable(struct i2c_clien
 		return fs453_write(client, FS453_PWR_MGNT, 0x3BFF, 2);
 }
 
-#ifdef CONFIG_MACH_MX27ADS
-extern void gpio_fs453_reset_low(void);
-extern void gpio_fs453_reset_high(void);
-#endif
-
 /*!
  * @brief FS453 control routine
  * @param	cmd	Control command
@@ -460,11 +455,6 @@ int fs453_ioctl(unsigned int cmd, void *
 	/* check for deferred I2C registration */
 	if (!fs453_client) {
 		int err;
-#ifdef CONFIG_MACH_MX27ADS
-		/* reset the FS453 via the CLS/GPIOA25 line */
-		gpio_fs453_reset_low();
-		gpio_fs453_reset_high();
-#endif
 
 		if ((err = i2c_add_driver(&fs453_driver))) {
 			pr_info("FS453: driver registration failed\n");
Index: linux-2.6.18/drivers/video/mxc/mx2fb.c
===================================================================
--- linux-2.6.18.orig/drivers/video/mxc/mx2fb.c
+++ linux-2.6.18/drivers/video/mxc/mx2fb.c
@@ -45,6 +45,10 @@
 extern void gpio_lcdc_active(void);
 extern void gpio_lcdc_inactive(void);
 extern void board_power_lcd(int on);
+#ifdef CONFIG_MACH_MX27ADS
+extern void gpio_fs453_reset_low(void);
+extern void gpio_fs453_reset_high(void);
+#endif
 
 static char *fb_mode = 0;
 static int fb_enabled = 0;
@@ -197,6 +201,21 @@ static struct fb_ops mx2fb_ops = {
 	.fb_ioctl = mx2fb_ioctl,
 };
 
+static void board_power_on(void)
+{
+	board_power_lcd(1);
+#ifdef CONFIG_MACH_MX27ADS
+	/* reset the FS453 via the CLS/GPIOA25 line */
+	gpio_fs453_reset_low();
+	gpio_fs453_reset_high();
+#endif
+}
+
+static void board_power_off(void)
+{
+	board_power_lcd(0);
+}
+
 /*!
  * @brief Validates a var passed in.
  *
@@ -847,7 +866,7 @@ static void _enable_lcdc(struct fb_info 
 	else if (!fb_enabled) {
 		clk_enable(lcdc_clk);
 		gpio_lcdc_active();
-		board_power_lcd(1);
+		board_power_on();
 		_set_brightness(brightness);
 		fb_enabled++;
 #ifdef CONFIG_FB_MXC_TVOUT
@@ -880,7 +899,7 @@ static void _disable_lcdc(struct fb_info
 	else {
 		if (fb_enabled) {
 			gpio_lcdc_inactive();
-			board_power_lcd(0);
+			board_power_off();
 			_set_brightness(0);
 			clk_disable(lcdc_clk);
 			fb_enabled = 0;
Index: linux-2.6.18/mvl_patches/pro50-2328.c
===================================================================
--- /dev/null
+++ linux-2.6.18/mvl_patches/pro50-2328.c
@@ -0,0 +1,16 @@
+/*
+ * Author: MontaVista Software, Inc. <source@mvista.com>
+ *
+ * 2009 (c) MontaVista Software, Inc. This file is licensed under
+ * the terms of the GNU General Public License version 2. This program
+ * is licensed "as is" without any warranty of any kind, whether express
+ * or implied.
+ */
+#include <linux/init.h>
+#include <linux/mvl_patch.h>
+
+static __init int regpatch(void)
+{
+        return mvl_register_patch(2328);
+}
+module_init(regpatch);
EOF

    rv=0
    cat /tmp/mvl_patch_$$
    if [ "$?" != "0" ]; then
	# Patch had a hard error, return 2
	rv=2
    elif grep '^Hunk' ${TMPFILE}; then
	rv=1
    fi

    rm -f ${TMPFILE}
    return $rv
}

function options() {
    echo "Options are:"
    echo "  --force-unsupported - Force the patch to be applied even if the"
    echo "      patch is out of order or the current kernel is unsupported."
    echo "      Use of this option is strongly discouraged."
    echo "  --force-apply-fuzz - If the patch has fuzz, go ahead and apply"
    echo "      it anyway.  This can occur if the patch is applied to an"
    echo "      unsupported kernel or applied out of order or if you have"
    echo "      made your own modifications to the kernel.  Use with"
    echo "      caution."
    echo "  --remove - Remove the patch"
}


function checkpatchnum() {
    local level;

    if [ ! -e ${1} ]; then
	echo "${1} does not exist, make sure you are in the kernel" 1>&2
	echo "base directory" 1>&2
	exit 1;
    fi

    # Extract the current patch number from the lsp info file.
    level=`grep '#define LSP_.*PATCH_LEVEL' ${1} | sed 's/^.*\"\\(.*\\)\".*\$/\\1/'`
    if [ "a$level" = "a" ]; then
	echo "No patch level defined in ${1}, are you sure this is" 1>&2
	echo "a valid MVL kernel LSP?" 1>&2
	exit 1;
    fi

    expr $level + 0 >/dev/null 2>&1
    isnum=$?

    # Check if the kernel is supported
    if [ "$level" = "unsupported" ]; then
	echo "**Current kernel is unsupported by MontaVista due to patches"
	echo "  begin applied out of order."
	if [ $force_unsupported == 't' ]; then
	    echo "  Application is forced, applying patch anyway"
	    unsupported=t
	    fix_patch_level=f
	else
	    echo "  Patch application aborted.  Use --force-unsupported to"
	    echo "  force the patch to be applied, but the kernel will not"
	    echo "  be supported by MontaVista."
	    exit 1;
	fi

    # Check the patch number from the lspinfo file to make sure it is
    # a valid number
    elif [ $isnum = 2 ]; then
	echo "**Patch level from ${1} was not a valid number, " 1>&2
	echo "  are you sure this is a valid MVL kernel LSP?" 1>&2
	exit 1;

    # Check that this is the right patch number to be applied.
    elif [ `expr $level $3` ${4} ${2} ]; then
	echo "**Application of this patch is out of order and will cause the"
	echo "  kernel to be unsupported by MontaVista."
	if [ $force_unsupported == 't' ]; then
	    echo "  application is forced, applying patch anyway"
	    unsupported=t
	else
	    echo "  Patch application aborted.  Please get all the patches in"
	    echo "  proper order from MontaVista Zone and apply them in order"
	    echo "  If you really want to apply this patch, use"
	    echo "  --force-unsupported to force the patch to be applied, but"
	    echo "  the kernel will not be supported by MontaVista."
	    exit 1;
	fi
    fi
}

#
# Update the patch level in the file.  Note that we use patch to do
# this.  Certain weak version control systems don't take kindly to
# arbitrary changes directly to files, but do have a special version
# of "patch" that understands this.
#
function setpatchnum() {
    sed "s/^#define LSP_\(.*\)PATCH_LEVEL[ \t*]\"[0-9]*\".*$/#define LSP_\1PATCH_LEVEL \"${2}\"/" <${1} >/tmp/$$.tmp1
    diff -u ${1} /tmp/$$.tmp1 >/tmp/$$.tmp2
    rm /tmp/$$.tmp1
    sed "s/^+++ \/tmp\/$$.tmp1/+++ include\/linux\/lsppatchlevel.h/" </tmp/$$.tmp2 >/tmp/$$.tmp1
    rm /tmp/$$.tmp2
    patch -p0 </tmp/$$.tmp1
    rm /tmp/$$.tmp1
}

force_unsupported=f
force_apply_fuzz=""
unsupported=f
fix_patch_level=t
reverse=f
common_patchnum_diff='+ 1'
common_patchnum=$PATCHNUM
patch_extraopts=''

# Extract command line parameters.
while [ $# -gt 0 ]; do
    if [ "a$1" == 'a--force-unsupported' ]; then
	force_unsupported=t
    elif [ "a$1" == 'a--force-apply-fuzz' ]; then
	force_apply_fuzz=y
    elif [ "a$1" == 'a--remove' ]; then
	reverse=t
	common_patchnum_diff=''
	common_patchnum=`expr $PATCHNUM - 1`
	patch_extraopts='--reverse'
    else
	echo "'$1' is an invalid command line parameter."
	options
	exit 1
    fi
    shift
done

echo "Checking patch level"
checkpatchnum ${LSPINFO} ${PATCHNUM} "${common_patchnum_diff}" "-ne"

if ! dopatch -p1 --dry-run --force $patch_extraopts; then
    if [ $? = 2 ]; then
	echo -n "**Patch had errors, application aborted" 1>&2
	exit 1;
    fi

    # Patch has warnings
    clean_apply=${force_apply_fuzz}
    while [ "a$clean_apply" != 'ay' -a "a$clean_apply" != 'an' ]; do
	echo -n "**Patch did not apply cleanly.  Do you still want to apply? (y/n) > "
	read clean_apply
	clean_apply=`echo "$clean_apply" | tr '[:upper:]' '[:lower:]'`
    done
    if [ $clean_apply = 'n' ]; then
	exit 1;
    fi
fi

dopatch -p1 --force $patch_extraopts

if [ $fix_patch_level = 't' ]; then 
    if [ $unsupported = 't' ]; then
	common_patchnum="unsupported"
    fi

    setpatchnum ${LSPINFO} ${common_patchnum}
fi

# Move the patch file into the mvl_patches directory if we are not reversing
if [ $reverse != 't' ]; then 
    if echo $0 | grep '/' >/dev/null; then
	# Filename is a path, either absolute or from the current directory.
	srcfile=$0
    else
	# Filename is from the path
	for i in `echo $PATH | tr ':;' '  '`; do
	    if [ -e ${i}/$0 ]; then
		srcfile=${i}/$0
	    fi
	done
    fi

    fname=`basename ${srcfile}`
    diff -uN mvl_patches/${fname} ${srcfile} | (cd mvl_patches; patch)
fi

