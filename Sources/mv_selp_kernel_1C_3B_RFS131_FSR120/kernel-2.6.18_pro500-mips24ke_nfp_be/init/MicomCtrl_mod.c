#include <linux/termios.h>
#include <linux/unistd.h>
#include <linux/types.h>
#include <linux/stat.h>
#include <linux/fcntl.h>
#include <linux/string.h>
#include <linux/delay.h>
#include <linux/syscalls.h>

struct termios newtio;

int SerialInit(char *dev)
{
//	struct termios newtio;
	int fd;
	
	fd = sys_open(dev, O_RDWR | O_NOCTTY,0 );
	if (fd <0) 
	{
		printk("Error..............\n");
		return (-1);
	}
	memset((void *)&newtio, 0x0, sizeof(newtio));

#ifdef CONFIG_TRIHIDTV_CPU_425M
	newtio.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
#elif CONFIG_TRIHIDTV_CPU_350M
	newtio.c_cflag = B57600 | CS8 | CLOCAL | CREAD;
#endif
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = OPOST;
	
	/* set input mode (non-canonical, no echo,...) */
	newtio.c_lflag = 0;

	newtio.c_cc[VTIME]    = 0;   /* 문자 사이의 timer를 disable */
	newtio.c_cc[VMIN]     = 1;   /* 최소 1 문자 받을 때까진 blocking */
	
//Add your other flags setting here...
	sys_ioctl(fd, TCIOFLUSH, (unsigned long) &newtio);
	sys_ioctl(fd, TCSETS, (unsigned long) &newtio);

	return fd;

}

int ReadSerialData(int fd, unsigned char *value)
{
	int res;
	unsigned char buf[255];
	int i;
	
	res = sys_read(fd, buf,255);
	buf[res] = 0;
	
	for(i = 0; i < res; i++)
		value[i] = buf[i];
#ifdef POCKET_TEST
	if(!res)
		printk("[!@#]MI_Err, ------------, -------\r\n");
#endif // #ifdef POCKET_TEST		
	return res;
}

int WriteSerialData(int fd, unsigned char *value, unsigned char num)
{
	int res;
	
	if(sys_write(fd, value, num) <0)
		printk("Write Error!!\n");

#ifdef POCKET_TEST
	if(!res)
		printk("[!@#]MI_Err, ------------, -------\r\n");
#endif // #ifdef POCKET_TEST
	return res;
}

int SerialClose(int fd)
{
	int retVar = 0;

	if(sys_close(fd)) retVar = 1;
	return retVar;
	
}

//int MicomCtrl(int argc, char* argv[])
int MicomCtrl(unsigned char ctrl)
{
	int fd;
	unsigned char databuff[9];
	memset(databuff, 0, 9);
#ifdef CONFIG_TRIHIDTV_CPU_425M
	fd = SerialInit("/dev/ttyS2");		// only 3B for external micom (This is the final design)
#elif CONFIG_TRIHIDTV_CPU_350M
	fd = SerialInit("/dev/ttyS1");		// 1C & 3B  for internal micom
#endif	

	databuff[0] = 0xff;
	databuff[1] = 0xff;
//	if(argc>6) argc=6;
//
	databuff[2] = ctrl;
	databuff[8] += databuff[2];
/*
	for(i=0;i<argc-1; i++)
	{
		databuff[i+2] = atoi(argv[i+1]);
		databuff[8]   += databuff[i+2];
	}
*/
	WriteSerialData(fd, databuff, 9);
	msleep(10);
	SerialClose(fd);
	return 0;
}


