/*
 *  linux/init/main.c
 *
 *  Copyright (C) 1991, 1992  Linus Torvalds
 *
 *  GK 2/5/95  -  Changed to support mounting root fs via NFS
 *  Added initrd & change_root: Werner Almesberger & Hans Lermen, Feb '96
 *  Moan early if gcc is old, avoiding bogus kernels - Paul Gortmaker, May '96
 *  Simplified starting of init:  Michael A. Griffith <grif@acm.org> 
 */

#define __KERNEL_SYSCALLS__

#include <linux/types.h>
#include <linux/module.h>
#include <linux/proc_fs.h>
#include <linux/kernel.h>
#include <linux/syscalls.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/delay.h>
#include <linux/utsname.h>
#include <linux/ioport.h>
#include <linux/init.h>
#include <linux/smp_lock.h>
#include <linux/initrd.h>
#include <linux/hdreg.h>
#include <linux/bootmem.h>
#include <linux/tty.h>
#include <linux/gfp.h>
#include <linux/percpu.h>
#include <linux/kmod.h>
#include <linux/kernel_stat.h>
#include <linux/security.h>
#include <linux/workqueue.h>
#include <linux/profile.h>
#include <linux/rcupdate.h>
#include <linux/posix-timers.h>
#include <linux/moduleparam.h>
#include <linux/kallsyms.h>
#include <linux/writeback.h>
#include <linux/clockchips.h>
#include <linux/cpu.h>
#include <linux/cpuset.h>
#include <linux/efi.h>
#include <linux/taskstats_kern.h>
#include <linux/delayacct.h>
#include <linux/unistd.h>
#include <linux/rmap.h>
#include <linux/irq.h>
#include <linux/mempolicy.h>
#include <linux/key.h>
#include <linux/unwind.h>
#include <linux/buffer_head.h>
#include <linux/debug_locks.h>
#include <linux/lockdep.h>

#include <asm/io.h>
#include <asm/bugs.h>
#include <asm/setup.h>
#include <asm/sections.h>
#include <asm/cacheflush.h>

#include <linux/random.h>
#include <asm/trihidtv/trihidtv_int.h> 

#ifdef CONFIG_EXECUTE_AUTHULD
//#include <linux/ci_header.h>
#include <linux/uboot_para_access.h>
#include <linux/aes-cmac.h>
#include "asm/trihidtv/trihidtv.h"
#include "asm/addrspace.h"
extern char rootfs_name[64];
//static int gSecureOn = 0;
#endif

/* VDLinux, based SELP.4.3.1.x default patch No.9, 
   SELP version print, SP Team 2009-05-08 */
#include <linux/lspinfo.h> /* to display SELP LSP info */

#ifdef CONFIG_X86_LOCAL_APIC
#include <asm/smp.h>
#endif

/*
 * This is one of the first .c files built. Error out early if we have compiler
 * trouble.
 *
 * Versions of gcc older than that listed below may actually compile and link
 * okay, but the end product can have subtle run time bugs.  To avoid associated
 * bogus bug reports, we flatly refuse to compile with a gcc that is known to be
 * too old from the very beginning.
 */
#if (__GNUC__ < 3) || (__GNUC__ == 3 && __GNUC_MINOR__ < 2)
#error Sorry, your GCC is too old. It builds incorrect kernels.
#endif

static int init(void *);
#ifdef CONFIG_EXECUTE_AUTHULD
extern int MicomCtrl(unsigned char ctrl);
#endif
extern void init_IRQ(void);
extern void fork_init(unsigned long);
extern void mca_init(void);
extern void sbus_init(void);
extern void sysctl_init(void);
extern void signals_init(void);
extern void pidhash_init(void);
extern void pidmap_init(void);
extern void prio_tree_init(void);
extern void radix_tree_init(void);
extern void free_initmem(void);
extern void populate_rootfs(void);
extern void driver_init(void);
extern void prepare_namespace(void);
#ifdef	CONFIG_ACPI
extern void acpi_early_init(void);
#else
static inline void acpi_early_init(void) { }
#endif
#ifndef CONFIG_DEBUG_RODATA
static inline void mark_rodata_ro(void) { }
#endif

#ifdef CONFIG_TC
extern void tc_init(void);
#endif

enum system_states system_state;
EXPORT_SYMBOL(system_state);


/*
 * Boot command-line arguments
 */
#define MAX_INIT_ARGS CONFIG_INIT_ENV_ARG_LIMIT
#define MAX_INIT_ENVS CONFIG_INIT_ENV_ARG_LIMIT

extern void time_init(void);
/* Default late time init is NULL. archs can override this later. */
void (*late_time_init)(void);
extern void softirq_init(void);

/* Untouched command line (eg. for /proc) saved by arch-specific code. */
char saved_command_line[COMMAND_LINE_SIZE];

static char *execute_command;
static char *ramdisk_execute_command;

/* Setup configured maximum number of CPUs to activate */
static unsigned int max_cpus = NR_CPUS;

/*
 * Setup routine for controlling SMP activation
 *
 * Command-line option of "nosmp" or "maxcpus=0" will disable SMP
 * activation entirely (the MPS table probe still happens, though).
 *
 * Command-line option of "maxcpus=<NUM>", where <NUM> is an integer
 * greater than 0, limits the maximum number of CPUs activated in
 * SMP mode to <NUM>.
 */
static int __init nosmp(char *str)
{
	max_cpus = 0;
	return 1;
}

__setup("nosmp", nosmp);
#ifdef CONFIG_EXECUTE_AUTHULD
static char * auth_argv_init[MAX_INIT_ARGS+2] = { "/bin/authuld", NULL, };
#endif
static int __init maxcpus(char *str)
{
	get_option(&str, &max_cpus);
	return 1;
}

__setup("maxcpus=", maxcpus);

static char * argv_init[MAX_INIT_ARGS+2] = { "init", NULL, };
char * envp_init[MAX_INIT_ENVS+2] = { "LD_LIBRARY_PATH=/mtd_exe", "TERM=linux", NULL, };
static const char *panic_later, *panic_param;

extern struct obs_kernel_param __setup_start[], __setup_end[];

static int __init obsolete_checksetup(char *line)
{
	struct obs_kernel_param *p;

	p = __setup_start;
	do {
		int n = strlen(p->str);
		if (!strncmp(line, p->str, n)) {
			if (p->early) {
				/* Already done in parse_early_param?  (Needs
				 * exact match on param part) */
				if (line[n] == '\0' || line[n] == '=')
					return 1;
			} else if (!p->setup_func) {
				printk(KERN_WARNING "Parameter %s is obsolete,"
				       " ignored\n", p->str);
				return 1;
			} else if (p->setup_func(line + n))
				return 1;
		}
		p++;
	} while (p < __setup_end);
	return 0;
}

/*
 * This should be approx 2 Bo*oMips to start (note initial shift), and will
 * still work even if initially too large, it will just take slightly longer
 */
unsigned long loops_per_jiffy = (1<<12);

EXPORT_SYMBOL(loops_per_jiffy);

static int __init debug_kernel(char *str)
{
	if (*str)
		return 0;
	console_loglevel = 10;
	return 1;
}

static int __init quiet_kernel(char *str)
{
	if (*str)
		return 0;
	console_loglevel = 4;
	return 1;
}

__setup("debug", debug_kernel);
__setup("quiet", quiet_kernel);

static int __init loglevel(char *str)
{
	get_option(&str, &console_loglevel);
	return 1;
}

__setup("loglevel=", loglevel);

/*
 * Unknown boot options get handed to init, unless they look like
 * failed parameters
 */
static int __init unknown_bootoption(char *param, char *val)
{
	/* Change NUL term back to "=", to make "param" the whole string. */
	if (val) {
		/* param=val or param="val"? */
		if (val == param+strlen(param)+1)
			val[-1] = '=';
		else if (val == param+strlen(param)+2) {
			val[-2] = '=';
			memmove(val-1, val, strlen(val)+1);
			val--;
		} else
			BUG();
	}

	/* Handle obsolete-style parameters */
	if (obsolete_checksetup(param))
		return 0;

	/*
	 * Preemptive maintenance for "why didn't my mispelled command
	 * line work?"
	 */
	if (strchr(param, '.') && (!val || strchr(param, '.') < val)) {
		printk(KERN_ERR "Unknown boot option `%s': ignoring\n", param);
		return 0;
	}

	if (panic_later)
		return 0;

	if (val) {
		/* Environment option */
		unsigned int i;
		for (i = 0; envp_init[i]; i++) {
			if (i == MAX_INIT_ENVS) {
				panic_later = "Too many boot env vars at `%s'";
				panic_param = param;
			}
			if (!strncmp(param, envp_init[i], val - param))
				break;
		}
		envp_init[i] = param;
	} else {
		/* Command line option */
		unsigned int i;
		for (i = 0; argv_init[i]; i++) {
			if (i == MAX_INIT_ARGS) {
				panic_later = "Too many boot init vars at `%s'";
				panic_param = param;
			}
		}
		argv_init[i] = param;
	}
	return 0;
}

static int __init init_setup(char *str)
{
	unsigned int i;

	execute_command = str;
	/*
	 * In case LILO is going to boot us with default command line,
	 * it prepends "auto" before the whole cmdline which makes
	 * the shell think it should execute a script with such name.
	 * So we ignore all arguments entered _before_ init=... [MJ]
	 */
	for (i = 1; i < MAX_INIT_ARGS; i++)
		argv_init[i] = NULL;
	return 1;
}
__setup("init=", init_setup);

static int __init rdinit_setup(char *str)
{
	unsigned int i;

	ramdisk_execute_command = str;
	/* See "auto" comment in init_setup */
	for (i = 1; i < MAX_INIT_ARGS; i++)
		argv_init[i] = NULL;
	return 1;
}
__setup("rdinit=", rdinit_setup);

#ifndef CONFIG_SMP

#ifdef CONFIG_X86_LOCAL_APIC
static void __init smp_init(void)
{
	APIC_init_uniprocessor();
}
#else
#define smp_init()	do { } while (0)
#endif

static inline void setup_per_cpu_areas(void) { }
static inline void smp_prepare_cpus(unsigned int maxcpus) { }

#else

#ifdef __GENERIC_PER_CPU
unsigned long __per_cpu_offset[NR_CPUS] __read_mostly;

EXPORT_SYMBOL(__per_cpu_offset);

static void __init setup_per_cpu_areas(void)
{
	unsigned long size, i;
	char *ptr;
	unsigned long nr_possible_cpus = num_possible_cpus();

	/* Copy section for each CPU (we discard the original) */
	size = ALIGN(__per_cpu_end - __per_cpu_start, SMP_CACHE_BYTES);
#ifdef CONFIG_MODULES
	if (size < PERCPU_ENOUGH_ROOM)
		size = PERCPU_ENOUGH_ROOM;
#endif
	ptr = alloc_bootmem(size * nr_possible_cpus);

	for_each_possible_cpu(i) {
		__per_cpu_offset[i] = ptr - __per_cpu_start;
		memcpy(ptr, __per_cpu_start, __per_cpu_end - __per_cpu_start);
		ptr += size;
	}
}
#endif /* !__GENERIC_PER_CPU */

/* Called by boot processor to activate the rest. */
static void __init smp_init(void)
{
	unsigned int i;

	/* FIXME: This should be done in userspace --RR */
	for_each_present_cpu(i) {
		if (num_online_cpus() >= max_cpus)
			break;
		if (!cpu_online(i))
			cpu_up(i);
	}

	/* Any cleanup work */
	printk(KERN_INFO "Brought up %ld CPUs\n", (long)num_online_cpus());
	smp_cpus_done(max_cpus);
#if 0
	/* Get other processors into their bootup holding patterns. */

	smp_commence();
#endif
}

#endif

/*
 * We need to finalize in a non-__init function or else race conditions
 * between the root thread and the init thread may cause start_kernel to
 * be reaped by free_initmem before the root thread has proceeded to
 * cpu_idle.
 *
 * gcc-3.4 accidentally inlines this function, so use noinline.
 */

static void noinline rest_init(void)
	__releases(kernel_lock)
{
	system_state = SYSTEM_BOOTING_SCHEDULER_OK;

	kernel_thread(init, NULL, CLONE_FS | CLONE_SIGHAND);
	numa_default_policy();
	unlock_kernel();

	/*
	 * The boot idle thread must execute schedule()
	 * at least one to get things moving:
	 */
	__preempt_enable_no_resched();
	schedule();
	preempt_disable();

	/* Call into cpu_idle with preempt disabled */
	cpu_idle();
} 

/* Check for early params. */
static int __init do_early_param(char *param, char *val)
{
	struct obs_kernel_param *p;

	for (p = __setup_start; p < __setup_end; p++) {
		if (p->early && strcmp(param, p->str) == 0) {
			if (p->setup_func(val) != 0)
				printk(KERN_WARNING
				       "Malformed early option '%s'\n", param);
		}
	}
	/* We accept everything at this stage. */
	return 0;
}

/* Arch code calls this early on, or if not, just before other parsing. */
void __init parse_early_param(void)
{
	static __initdata int done = 0;
	static __initdata char tmp_cmdline[COMMAND_LINE_SIZE];
#ifdef CONFIG_EXECUTE_AUTHULD
//	char *pOffset =NULL;
#endif
	if (done)
		return;

	/* All fall through to do_early_param. */
	strlcpy(tmp_cmdline, saved_command_line, COMMAND_LINE_SIZE);
#ifdef CONFIG_EXECUTE_AUTHULD
/*
	pOffset = strstr(tmp_cmdline,"secure_on");
	if(pOffset != NULL)
	{
		gSecureOn = 1;
	}
	else
	{
		gSecureOn = 0;
	}
*/
#endif
	parse_args("early options", tmp_cmdline, NULL, 0, do_early_param);
	done = 1;
}

/*
 *	Activate the first processor.
 */

static void __init boot_cpu_init(void)
{
	int cpu = smp_processor_id();
	/* Mark the boot cpu "present", "online" etc for SMP and UP case */
	cpu_set(cpu, cpu_online_map);
	cpu_set(cpu, cpu_present_map);
	cpu_set(cpu, cpu_possible_map);
}

void __init __attribute__((weak)) smp_setup_processor_id(void)
{
}

asmlinkage void __init start_kernel(void)
{
	char * command_line;
	extern struct kernel_param __start___param[], __stop___param[];

	smp_setup_processor_id();

	/*
	 * Need to run as early as possible, to initialize the
	 * lockdep hash:
	 */
	lockdep_init();

	start_trace();

	local_irq_disable();
	early_boot_irqs_off();
	early_init_irq_lock_class();

/*
 * Interrupts are still disabled. Do necessary setups, then
 * enable them
 */
	lock_kernel();
	boot_cpu_init();
	page_address_init();
	printk(KERN_NOTICE);
	printk(linux_banner);
	setup_arch(&command_line);
	setup_per_cpu_areas();
	smp_prepare_boot_cpu();	/* arch-specific boot-cpu hooks */

	/*
	 * Set up the scheduler prior starting any interrupts (such as the
	 * timer interrupt). Full topology setup happens at smp_init()
	 * time - but meanwhile we still have a functioning scheduler.
	 */
	sched_init();
	/*
	 * Disable preemption - early bootup scheduling is extremely
	 * fragile until we cpu_idle() for the first time.
	 */
	preempt_disable();

	build_all_zonelists();
	page_alloc_init();
	early_init_hardirqs();
	printk(KERN_NOTICE "Kernel command line: %s\n", saved_command_line);
	parse_early_param();
	parse_args("Booting kernel", command_line, __start___param,
		   __stop___param - __start___param,
		   &unknown_bootoption);
	sort_main_extable();
	unwind_init();
	trap_init();
	rcu_init();
	init_IRQ();
	pidhash_init();
	clockevents_init();
	init_timers();
	hrtimers_init();
	softirq_init();
	timekeeping_init();
	time_init();
	profile_init();
	if (!irqs_disabled())
		printk("start_kernel(): bug: interrupts were enabled early\n");
	early_boot_irqs_on();
	local_irq_enable();

	/*
	 * HACK ALERT! This is early. We're enabling the console before
	 * we've done PCI setups etc, and console_init() must be aware of
	 * this. But we do want output early, in case something goes wrong.
	 */
	console_init();
	if (panic_later)
		panic(panic_later, panic_param);

	lockdep_info();

	/*
	 * Need to run this when irqs are enabled, because it wants
	 * to self-test [hard/soft]-irqs on/off lock inversion bugs
	 * too:
	 */
	locking_selftest();

#ifdef CONFIG_BLK_DEV_INITRD
	if (initrd_start && !initrd_below_start_ok &&
			initrd_start < min_low_pfn << PAGE_SHIFT) {
		printk(KERN_CRIT "initrd overwritten (0x%08lx < 0x%08lx) - "
		    "disabling it.\n",initrd_start,min_low_pfn << PAGE_SHIFT);
		initrd_start = 0;
	}
#endif
	vfs_caches_init_early();
	cpuset_init_early();
	mem_init();
	kmem_cache_init();
	setup_per_cpu_pageset();
	numa_policy_init();
	if (late_time_init)
		late_time_init();
	calibrate_delay();
	pidmap_init();
	pgtable_cache_init();
	prio_tree_init();
	anon_vma_init();
#ifdef CONFIG_X86
	if (efi_enabled)
		efi_enter_virtual_mode();
#endif
	fork_init(num_physpages);
	proc_caches_init();
	buffer_init();
	unnamed_dev_init();
	key_init();
	security_init();
	vfs_caches_init(num_physpages);
	radix_tree_init();
	signals_init();
	/* rootfs populating might need page-writeback */
	page_writeback_init();
#ifdef CONFIG_PROC_FS
	proc_root_init();
#endif
	cpuset_init();
	taskstats_init_early();
	delayacct_init();

	check_bugs();

	acpi_early_init(); /* before LAPIC and SMP init */

#ifdef CONFIG_PREEMPT_RT
	WARN_ON(irqs_disabled());
#endif
	/* Do the rest non-__init'ed, we're now alive */
	rest_init();
}

static int __initdata initcall_debug;

static int __init initcall_debug_setup(char *str)
{
	initcall_debug = 1;
	return 1;
}
__setup("initcall_debug", initcall_debug_setup);

struct task_struct *child_reaper = &init_task;

extern initcall_t __initcall_start[], __initcall_end[];

static void __init do_initcalls(void)
{
	initcall_t *call;
	int count = preempt_count();

	for (call = __initcall_start; call < __initcall_end; call++) {
		char *msg = NULL;
		char msgbuf[40];
		int result;

		if (initcall_debug) {
			printk("Calling initcall 0x%p", *call);
			print_fn_descriptor_symbol(": %s()",
					(unsigned long) *call);
			printk("\n");
		}

		result = (*call)();

		if (result && result != -ENODEV && initcall_debug) {
			sprintf(msgbuf, "error code %d", result);
			msg = msgbuf;
		}
		if (preempt_count() != count) {
			msg = "preemption imbalance";
			preempt_count() = count;
		}
		if (irqs_disabled()) {
			msg = "disabled interrupts";
			local_irq_enable();
		}
#ifdef CONFIG_PREEMPT_RT
		if (irqs_disabled()) {
			msg = "disabled hard interrupts";
			local_irq_enable();
		}
#endif
		if (msg) {
			printk(KERN_WARNING "initcall at 0x%p", *call);
			print_fn_descriptor_symbol(": %s()",
					(unsigned long) *call);
			printk(": returned with %s\n", msg);
		}
	}

	/* Make sure there is no pending stuff from the initcall sequence */
	flush_scheduled_work();
}

/*
 * Ok, the machine is now initialized. None of the devices
 * have been touched yet, but the CPU subsystem is up and
 * running, and memory and process management works.
 *
 * Now we can finally start doing some real work..
 */
static void __init do_basic_setup(void)
{
	/* drivers will send hotplug events */
	init_workqueues();
	usermodehelper_init();
	driver_init();

#ifdef CONFIG_SYSCTL
	sysctl_init();
#endif

	do_initcalls();
}

static void do_pre_smp_initcalls(void)
{
	extern int spawn_ksoftirqd(void);
	extern int spawn_desched_task(void);
#ifdef CONFIG_SMP
	extern int migration_init(void);

	migration_init();
#endif
	posix_cpu_thread_init();
	spawn_ksoftirqd();
	spawn_softlockup_task();
	spawn_desched_task();
}

static void run_init_process(char *init_filename)
{
	argv_init[0] = init_filename;
	execve(init_filename, argv_init, envp_init);
}

#ifdef CONFIG_EXECUTE_AUTHULD
static int send_to_authuld(void) 
{
	int fd = -1;
	unsigned int nonce = 0;
	sys_unlink(UK_COMM_CHANNEL);

	get_rand(&nonce, 1);

	fd=sys_open(KU_COMM_CHANNEL, O_WRONLY | O_CREAT,0);
	if(fd < 0)
	{
		SCU_PRINTK_ERR("open error");
		return -1;
	}

	if(sys_write(fd, (void *)&nonce, 4) < 0)
	{
		SCU_PRINTK_ERR("write error");
		sys_close(fd);
		return -1;
	}

	sys_close(fd);

	return nonce;
}

static void changePartName(char * src)
{
// rfs location is already fixed.. so we just use fixed mapping.
// if device node changed, we need to change this part also.

	if(strstr(src, "tfsr7"))
	{
		memcpy(src,"/dev/tbml0/7",16);
	}
	else if (strstr(src, "tfsr9"))
	{
		memcpy(src,"/dev/tbml0/9",16);		
	}
	else if (strstr(src, "tbml7"))
	{
		memcpy(src,"/dev/tbml0/7",16);
	}
	else if (strstr(src, "tbml9"))
	{
		memcpy(src,"/dev/tbml0/9",16);	
	}

	return;
}
static int wait_authuld(unsigned char *mkey, int nonce, pid_t pid) 
{
	int fd=-1;
	int success_result =0;
	int i;
	int read_len=0;
	unsigned char buf[SZ_AES_UNIT] ={0,};
	unsigned char input_buf[SZ_AES_UNIT]={0};
	unsigned char mac[SZ_AES_UNIT] ={0,};
	
	for(i=0;i<(CONFIG_TIMEOUT_ACK_AUTHULD*3);i++) 
	{
		ssleep(20);

		fd=sys_open(UK_COMM_CHANNEL, O_RDONLY,0);
		read_len = sys_read(fd, buf, SZ_AES_UNIT);
		if( fd >= 0 && read_len >= SZ_AES_UNIT) 
		{
			sys_close(fd);

			sys_unlink(KU_COMM_CHANNEL);
			sys_unlink(UK_COMM_CHANNEL);
			sys_unlink("/dtv/.finalized");

			memcpy(input_buf, &nonce, 4);
			success_result = 23;
			memcpy(&input_buf[4], &success_result, 4);

			AES_128(mkey, input_buf,  mac );

			if( memcmp( buf, mac, SZ_AES_UNIT) == 0 ) 
			{
				return 0;
			}
			else 
			{
				return -1;
			}
		}
		else if(sys_wait4(pid, NULL, WNOHANG, NULL) == pid)
		{
			if(sys_open("/dtv/.finalized", O_RDONLY,0) < 0)
			{
				return -2;
			}
			else
			{
				sys_close(fd);
				sys_unlink("/dtv/.finalized");
			}
		}
	}
	return -3;
}

void Exception_from_authuld(const unsigned char *msg)
{
	SCU_PRINTK_ERR("%s", msg);

#ifdef CONFIG_SHUTDOWN
		MicomCtrl(115); 
		msleep(100);
		panic("[kernel] System down\n"); 
#endif
}

static int check_ci_app_integrity_with_size(unsigned char *key, char *filename, int input_size, unsigned char *mac) 
{
	
	int fd =-1;
	mm_segment_t old_fs;
	unsigned char result[SZ_AES_UNIT];

	SCU_PRINTK("%d\n",input_size);
	if(input_size < 50*1024 || input_size > 150*1024)
	{
		SCU_PRINTK_ERR("Weird size %d\n", input_size);
		return -1;
	}

	/* key copy routine should be required here */
	old_fs = get_fs();
	set_fs(KERNEL_DS);
	fd = sys_open(filename, O_RDONLY, 0);
	if (fd >= 0) 
	{
		AES_CMAC_with_fd_and_size(key, fd, input_size, result);
	  	sys_close(fd);

		if(memcmp(result, mac, SZ_AES_UNIT) == 0 ) 
		{
			set_fs(old_fs);
			return 0;
		}
	} 
	else 
	{
		SCU_PRINTK_ERR("unable to open Auth\n");
	}
	
	set_fs(old_fs);	
	return -1;

}

static void execute_authuld(void) 
{
	auth_argv_init[0] = CONFIG_AUTHULD_PATH;
#ifdef CONFIG_SHUTDOWN
		auth_argv_init[1] = "0";
#else
		auth_argv_init[1] = "1";
#endif
	execve(CONFIG_AUTHULD_PATH, auth_argv_init, envp_init);
}

void print128(unsigned char *bytes)
{
    int         j;
    for (j=0; j<SZ_AES_UNIT;j++) {
        printk("%02x",bytes[j]);
        if ( (j%4) == 3 ) printk(" ");
    }
    printk("\n\n");
}

static void check_ci_app(void) 
{
	unsigned int outLength=0;
	unsigned char mkey[SZ_AES_UNIT]={0,} ;

	MacInfo_t macList;
	cmacKey_t cmackey;

	int fd=0;
	int i=0;
	int uboot_open_flag = 0;
	int nonce=0;
	pid_t pid = 0;

#ifdef CONFIG_TRIHIDTV_CPU_425M	
	unsigned short reg16;
#endif
	memset(&macList,0,sizeof(macList));
	memset(&cmackey,0,sizeof(cmackey));

	aes_init();
	aes_start(); // aes_stop() need to called. before loading avdecoder.bin  

#ifdef CONFIG_TRIHIDTV_CPU_425M
// To communicate external micom in 3B, prior setting of 3rd serial is needed
        reg16 = ReadRegHWord(0x1b00002a); //bit11 = 0
        reg16 &= 0xf7ff;
        WriteRegHWord(0x1b00002a, reg16);

        reg16 = ReadRegHWord(0x1b000154); // bit0 = 1
        reg16 &= 0xfffe;
	  reg16 |= 0x1;	
        WriteRegHWord(0x1b000154, reg16);

        reg16 = ReadRegHWord(0x1b000034); // bit10 = 1
        reg16 &= 0xfbff;
	  reg16 |= 0x400;	
        WriteRegHWord(0x1b000034, reg16);	
		
#endif

#ifdef CONFIG_SHUTDOWN
		printk("[kAuth] Release Version\n");
#else
		printk("[kAuth] Debug Version\n");
#endif

	for(i=0;i<1000;i++) 
	{
		if((fd=sys_open(NUM1_DATA_PART, O_RDONLY, 0 ))>= 0 ) 
		{
			SCU_PRINTK("---------------------------------\n");
			SCU_PRINTK("Got 1st data (after=%d)\n",i);

			if(sys_read(fd, (void *)&cmackey, sizeof(cmacKey_t)) < 0)
			{
				SCU_PRINTK_ERR("read failed \n");
				sys_close(fd);
				break;
			}			
			sys_close(fd);
			uboot_open_flag = 1;
			break;
		}
		msleep(10);
	}
	
	if( uboot_open_flag == 0 ) 
	{
		aes_stop();
		Exception_from_authuld("Unable to open 1st data\n"); 
		do_exit(0);	// for development version. Exception_from_authuld() is the ending point in release version
	}

	uboot_open_flag = 0;
	
	changePartName(rootfs_name);	
				
	for(i=0;i<1000;i++) 
	{
		if( (fd=sys_open(rootfs_name, O_RDONLY, 0 ) )>= 0 ) {
			SCU_PRINTK("Got 2nd data (after=%d)\n",i);

			sys_lseek(fd, -AUTH_MAC_OFFSET, SEEK_END);
			if(sys_read(fd, (void *)&macList, sizeof(MacInfo_t)) < 0)
			{
				SCU_PRINTK_ERR("read failed\n");
				sys_close(fd);
				break;
			}

			sys_close(fd);
			uboot_open_flag = 1;
			break;
		}
		msleep(10);
	}
	
	if( uboot_open_flag == 0 ) 
	{
		aes_stop();
		Exception_from_authuld("Unable to open 2nd data\n");
		do_exit(0);	// for development version. Exception_from_authuld() is the ending point in release version		
	}

	if((((unsigned int)cmackey.key & 0x3) != 0) || (((unsigned int)mkey & 0x3) != 0))
	{
		SCU_PRINTK_ERR(" addr is not 4 bytes aligned: cmackey.key %p, mkey %p", cmackey.key, mkey);
	}

	if (cmackey.magic == SBOOT_NORMAL) 
	{
		// ECB need 4 bytes aligned
		SCU_PRINTK("N_BOOT\n");
		Trid_Securekey_AES_ECB_Dec( (unsigned int) cmackey.key, SZ_AES_UNIT,  (unsigned int) mkey,  (unsigned int *) &outLength);
	}
	else if(cmackey.magic == SBOOT_NOT_NORMAL)
	{
		SCU_PRINTK("F_BOOT\n");
		Trid_Masterkey_AES_ECB_Enc( (unsigned int) cmackey.key,  SZ_AES_UNIT,  (unsigned int) mkey,  (unsigned int *)&outLength);
	}
	else
	{
		aes_stop();
		Exception_from_authuld("No correct flag\n");
		do_exit(0);	// for development version. Exception_from_authuld() is the ending point in release version
	}
		
	uboot_open_flag = 0;
	for(i=0;i<3;i++) 
	{
		if( (fd=sys_open(CONFIG_AUTHULD_PATH, O_RDONLY, 0 ) )>= 0 ) 
		{
			SCU_PRINTK("Now ready (after=%d)    \n", i);
			SCU_PRINTK("---------------------------------\n");
			sys_close(fd);
			uboot_open_flag = 1;
			break;
		}
		msleep(10);
	}
	
	if( uboot_open_flag == 0 ) 
	{
		aes_stop();
		Exception_from_authuld("Unable to open Auth\n");
		do_exit(0); // for development version. Exception_from_authuld() is the ending point in release version
	}

	if(check_ci_app_integrity_with_size(mkey, CONFIG_AUTHULD_PATH, macList.msgLen, macList.mac) == 0 ) 
	{
		aes_stop(); // Call aes_stop() after send_to_authuld(). get_rand() is a function of libaes.a
		
		// don't increase booting time by loading. 
		ssleep(15);

		nonce = send_to_authuld();

		SCU_PRINTK("Auth run\n");

		pid = kernel_thread(execute_authuld,NULL,CLONE_FS|CLONE_SIGHAND|SIGCHLD);

		switch(wait_authuld(mkey, nonce, pid))
		{
			case 0:
				SCU_PRINTK("Keep Going!\n");
				break;
				
			case -1:
				Exception_from_authuld("Oops 1!\n");			
				break;
				
			case -2:
				Exception_from_authuld("Oops 2!\n");			
				break;
				
			case -3:
			default:
				Exception_from_authuld("It's the time\n");			
				break;
		}
	}
	else 
	{
		aes_stop();
		Exception_from_authuld("Auth?\n");
	}
	
	do_exit(0);
}

#endif

static int init(void * unused)
{
	lock_kernel();
	/*
	 * init can run on any cpu.
	 */
	set_cpus_allowed(current, CPU_MASK_ALL);
	/*
	 * Tell the world that we're going to be the grim
	 * reaper of innocent orphaned children.
	 *
	 * We don't want people to have to make incorrect
	 * assumptions about where in the task array this
	 * can be found.
	 */
	child_reaper = current;

	smp_prepare_cpus(max_cpus);

	init_hardirqs();

	do_pre_smp_initcalls();

	smp_init();
	sched_init_smp();

	cpuset_init_smp();

	/*
	 * Do this before initcalls, because some drivers want to access
	 * firmware files.
	 */
	populate_rootfs();

	do_basic_setup();

	// reference from setup_irq()
	rand_initialize_irq(TRIHIDTV_MI2C0_IRQ);
	rand_initialize_irq(TRIHIDTV_MI2C1_IRQ);
	rand_initialize_irq(TRIHIDTV_MI2C2_IRQ);

	/*
	 * check if there is an early userspace init.  If yes, let it do all
	 * the work
	 */

	if (!ramdisk_execute_command)
		ramdisk_execute_command = "/init";

	if (sys_access((const char __user *) ramdisk_execute_command, 0) != 0) {
		ramdisk_execute_command = NULL;
		prepare_namespace();
	}
#ifdef CONFIG_PREEMPT_RT
	WARN_ON(irqs_disabled());
#endif

#define DEBUG_COUNT (defined(CONFIG_DEBUG_RT_MUTEXES) + defined(CONFIG_DEBUG_PREEMPT) + defined(CONFIG_CRITICAL_PREEMPT_TIMING) + defined(CONFIG_CRITICAL_IRQSOFF_TIMING) + defined(CONFIG_LATENCY_TRACE) + defined(CONFIG_DEBUG_SLAB) + defined(CONFIG_DEBUG_PAGEALLOC) + defined(CONFIG_LOCKDEP))

#if DEBUG_COUNT > 0
	printk(KERN_ERR "*****************************************************************************\n");
	printk(KERN_ERR "*                                                                           *\n");
#if DEBUG_COUNT == 1
	printk(KERN_ERR "*  REMINDER, the following debugging option is turned on in your .config:   *\n");
#else
	printk(KERN_ERR "*  REMINDER, the following debugging options are turned on in your .config: *\n");
#endif
	printk(KERN_ERR "*                                                                           *\n");
#ifdef CONFIG_DEBUG_RT_MUTEXES
	printk(KERN_ERR "*        CONFIG_DEBUG_RT_MUTEXES                                             *\n");
#endif
#ifdef CONFIG_DEBUG_PREEMPT
	printk(KERN_ERR "*        CONFIG_DEBUG_PREEMPT                                               *\n");
#endif
#ifdef CONFIG_CRITICAL_PREEMPT_TIMING
	printk(KERN_ERR "*        CONFIG_CRITICAL_PREEMPT_TIMING                                     *\n");
#endif
#ifdef CONFIG_CRITICAL_IRQSOFF_TIMING
	printk(KERN_ERR "*        CONFIG_CRITICAL_IRQSOFF_TIMING                                     *\n");
#endif
#ifdef CONFIG_LATENCY_TRACE
	printk(KERN_ERR "*        CONFIG_LATENCY_TRACE                                               *\n");
#endif
#ifdef CONFIG_DEBUG_SLAB
	printk(KERN_ERR "*        CONFIG_DEBUG_SLAB                                                  *\n");
#endif
#ifdef CONFIG_DEBUG_PAGEALLOC
	printk(KERN_ERR "*        CONFIG_DEBUG_PAGEALLOC                                             *\n");
#endif
#ifdef CONFIG_LOCKDEP
	printk(KERN_ERR "*        CONFIG_LOCKDEP                                                     *\n");
#endif
	printk(KERN_ERR "*                                                                           *\n");
#if DEBUG_COUNT == 1
	printk(KERN_ERR "*  it may increase runtime overhead and latencies.                          *\n");
#else
	printk(KERN_ERR "*  they may increase runtime overhead and latencies.                        *\n");
#endif
	printk(KERN_ERR "*                                                                           *\n");
	printk(KERN_ERR "*****************************************************************************\n");
#endif
	/*
	 * Ok, we have completed the initial bootup, and
	 * we're essentially up and running. Get rid of the
	 * initmem segments and start the user-mode stuff..
	 */
	free_initmem();
	unlock_kernel();
	mark_rodata_ro();
	system_state = SYSTEM_RUNNING;
	numa_default_policy();

	if (sys_open((const char __user *) "/dev/console", O_RDWR, 0) < 0)
		printk(KERN_WARNING "Warning: unable to open an initial console.\n");

	(void) sys_dup(0);
	(void) sys_dup(0);

	if (ramdisk_execute_command) {
		run_init_process(ramdisk_execute_command);
		printk(KERN_WARNING "Failed to execute %s\n",
				ramdisk_execute_command);
	}
#ifdef CONFIG_PREEMPT_RT
	WARN_ON(irqs_disabled());
#endif

	/*
	 * We try each of these until one succeeds.
	 *
	 * The Bourne shell can be used instead of init if we are 
	 * trying to recover a really broken machine.
	 */
	if (execute_command) {
		run_init_process(execute_command);
		printk(KERN_WARNING "Failed to execute %s.  Attempting "
					"defaults...\n", execute_command);
	}

#if 1
        printk(KERN_ALERT"================================================================================\n");
        printk(KERN_ALERT" SAMSUNG: %s(%s)\n", LSP_SELP_VERSION, LSP_REVISION);
        printk(KERN_ALERT"         (Detailed Information: /sys/selp/vd/lspinfo/summary)                   \n");
#ifdef CONFIG_TRIHIDTV_CPU_425M
	printk(KERN_ALERT" TridentSX 3B kernel ver : 0171		                                  \n");

#elif CONFIG_TRIHIDTV_CPU_350M
	printk(KERN_ALERT" TridentSX 1C kernel ver : 1006		                                  \n");
#endif
        printk(KERN_ALERT"================================================================================\n");
        printk(KERN_ALERT"SPECIAL CONFIG CHECK....\n");
        printk(KERN_ALERT"--------------------------------------------------------------------------------\n");
#ifdef CONFIG_KDEBUGD
        printk(KERN_ALERT"KDEBUGD ON\n");
#else
        printk(KERN_ALERT"KDEBUGD OFF\n");
#endif
#ifdef CONFIG_EXECUTE_AUTHULD
        printk(KERN_ALERT"Secure Boot ON\n");
#else
        printk(KERN_ALERT"Secure Boot OFF\n");
#endif
#ifdef CONFIG_HIGHMEM
        printk(KERN_ALERT"HIGHMEM ON\n");
#else
        printk(KERN_ALERT"HIGHMEM OFF\n");
#endif
        printk(KERN_ALERT"================================================================================\n");
#endif
#ifdef CONFIG_EXECUTE_AUTHULD
	kernel_thread(check_ci_app, NULL, SIGCHLD);
#endif
	run_init_process("/sbin/init");
	run_init_process("/etc/init");
	run_init_process("/bin/init");
	run_init_process("/bin/sh");

	panic("No init found.  Try passing init= option to kernel.");
}
