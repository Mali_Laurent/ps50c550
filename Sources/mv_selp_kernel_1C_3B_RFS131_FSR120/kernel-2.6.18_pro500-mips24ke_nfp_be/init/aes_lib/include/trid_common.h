//trid_common.h

#ifndef	__TRID_COMMON_H__
#define __TRID_COMMON_H__

#define ReadRegWord(addr)	({	\
		unsigned int tmpAddr = ((addr)&0x1fffffff)|0xa0000000;	\
		*((volatile unsigned int *)tmpAddr);})

#define	WriteRegWord(addr, value)	do{	\
		unsigned int tmpAddr = ((addr)&0x1fffffff)|0xa0000000;	\
		*((volatile unsigned int *)tmpAddr) = (value);	\
		}while(0)

#endif
