#ifndef __ACE_H
#define __ACE_H
typedef enum _tagDecryptMode
{
	TRID_MODE_AES_CBC = 0,
	TRID_MODE_AES_ECB = 1,
	TRID_MODE_DES_ECB = 2
}tridDecryptMode_e;

typedef enum
{
    TRID_USER_KEY = 0, 
    TRID_SECURE_KEY,
    TRID_MASTER_KEY,
    TRID_ONLY_KEY,
} tridKeyType_e;


typedef struct packet{
        void * send;
	void * recv;
        unsigned int len;
	unsigned char eMode;
	unsigned char bEncrypt;
	unsigned char KeyType;
	unsigned char dummy;
	unsigned int dwIV0;
	unsigned int dwIV1;
	unsigned int dwIV2;
	unsigned int dwIV3;
	unsigned int dwKey0;
	unsigned int dwKey1;
	unsigned int dwKey2;
	unsigned int dwKey3;
	
}packet_t;

#define VIRTTOPHYS(adr)     ((adr)&0x1fffffff)
#define PHYSTOKSEG0(adr)    ((adr)|0x80000000)
#define PHYSTOKSEG1(adr)    ((adr)|0xA0000000)


static inline int write_mask_reg(unsigned int reg, unsigned int mask, unsigned value)
{
        unsigned int volatile  *p = (unsigned int *)reg;
        *p = ((*p & mask) | value);
         return 0;


}

void aes_init();
void aes_start();
void aes_stop();

int Trid_Masterkey_AES_ECB_Enc( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

int Trid_Masterkey_AES_ECB_Dec( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

int Trid_Securekey_AES_ECB_Enc( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

int Trid_Securekey_AES_ECB_Dec( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int * pu32OutDataLength);

int Trid_AES_CBC_Enc( unsigned char *key,  unsigned char * iv, unsigned int u32SrcAddr, unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

int Trid_AES_CBC_Dec( unsigned char *key,  unsigned char * iv, unsigned int u32SrcAddr, unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength);

//void Trid_AES_128_Enc(uint8_t  *key, uint8_t  *plainText, uint8_t  *cipherText) //ROUNDS=10
void Trid_AES_128_Enc(unsigned char *key, unsigned char *plainText, unsigned char *cipherText); //ROUNDS=10
int decrypt(tridDecryptMode_e eMode, 
		char bEncrypt, 
		unsigned char* pInputData, 
		unsigned int dwDataLen, 
		unsigned char* pOutputData,
		 tridKeyType_e KeyType,
		 unsigned int dwIV0,
		 unsigned int dwIV1,
		 unsigned int dwIV2,
		 unsigned int dwIV3,
		 unsigned int dwKey0,
		 unsigned int dwKey1,
		 unsigned int dwKey2,
		 unsigned int dwKey3);
void get_rand(unsigned int *buf, int num);
#endif
