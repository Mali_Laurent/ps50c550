#ifndef _TRID_KERNEL_CHIPID_H_
#define _TRID_KERNEL_CHIPID_H_

typedef enum _tagTridChipIDInfo
{
	TRID_HIDTVPRO_UNKNOW = -1,

	TRID_HIDTVPRO_LX_ = 0x50,
	
	TRID_HIDTVPRO_CX_ = 0x100,

	TRID_HIDTVPRO_FX170_ = 0x200,

	TRID_HIDTVPRO_QX_REVA = 0x300,
	TRID_HIDTVPRO_QX_REVB = 0x301,
	TRID_HIDTVPRO_QX_REVC = 0x302,
	TRID_HIDTVPRO_QX_REVC1 = 0x303,
	TRID_HIDTVPRO_QX_REVD = 0x304,
	TRID_HIDTVPRO_QX2_NEW,
	
	TRID_HIDTVPRO_WX_REVA = 0x310,
	TRID_HIDTVPRO_WX_REVB = 0x311,
	TRID_HIDTVPRO_WX_REVC = 0x312,
	TRID_HIDTVPRO_WX_REVC1 = 0x313,
	TRID_HIDTVPRO_WX_REVD = 0x314,
	TRID_HIDTVPRO_WX2_NEW,

	TRID_HIDTVPRO_UX_REVA = 0x320,
	TRID_HIDTVPRO_UX_REVB = 0x321,
	TRID_HIDTVPRO_UX_REVC = 0x322,
	TRID_HIDTVPRO_UX_REVC1 = 0x323,
	TRID_HIDTVPRO_UX_REVD = 0x324,
	TRID_HIDTVPRO_UX2_NEW,

	TRID_HIDTVPRO_AX_REVA = 0x400,
	TRID_HIDTVPRO_AX_REVB = 0x401,
	TRID_HIDTVPRO_AX_REVC = 0x402,
	TRID_HIDTVPRO_AX_NEW,

	TRID_HIDTVPRO_SX_REVA = 0x500,
	TRID_HIDTVPRO_SX_REVB = 0x501,
	TRID_HIDTVPRO_SX_NEW,

	TRID_HIDTVPRO_SA_1 = 0x550,
	TRID_HIDTVPRO_SA_2 = 0x551,
	TRID_HIDTVPRO_SA_NEW,

	TRID_HIDTVPRO_SAX2_REVA = 0x610,
	TRID_HIDTVPRO_SAX2_REVB = 0x611,
	TRID_HIDTVPRO_SAX2_NEW,

	TRID_HIDTVPRO_SAX3_REVA = 0x710,
	TRID_HIDTVPRO_SAX3_REVB = 0x711,
	TRID_HIDTVPRO_SAX3_NEW,
	
	TRID_HIDTVPRO_SX4_REVA = 0x810,
	TRID_HIDTVPRO_SX4_REVB = 0x811,
	TRID_HIDTVPRO_SX4_NEW,

}TridHidtvChipID_e;

static inline TridHidtvChipID_e Trid_Util_ChipID_Detect(void)
{
#define AX_CHIP_ID 		0x88
#define SX_CHIP_ID 		0x89
#define SA_CHIP_ID 		0x8a
#define SAX2_CHIP_ID 		0x90
#define SAX3_CHIP_ID 		0x91
	
#define TridGetBits(RegValue, StartBit, Bits) (((RegValue) >> (StartBit)) & ((0x1<<(Bits))-1))

	unsigned int RegValue1;
	TridHidtvChipID_e TridHidtvChipID = TRID_HIDTVPRO_UNKNOW;

	RegValue1 = *((unsigned short *)0xbb00001e);
	if(RegValue1 != 0)
	{
		//This is not FX nor QX Chip, check if it's AX Chip.
		if(TridGetBits(RegValue1, 8, 8) == AX_CHIP_ID)
		{
			if(TridGetBits(RegValue1, 0, 5) == 0)
				TridHidtvChipID = TRID_HIDTVPRO_AX_REVA;
			else if(TridGetBits(RegValue1, 0, 5) == 1)
				TridHidtvChipID = TRID_HIDTVPRO_AX_REVB;
			else if(TridGetBits(RegValue1, 0, 5) == 2)
				TridHidtvChipID = TRID_HIDTVPRO_AX_REVC;
			else
			{
				//printk("Unknown AX Revision: %d\n", TridGetBits(RegValue1, 0, 5));
				TridHidtvChipID = TRID_HIDTVPRO_AX_NEW;
			}
		}
		else if(TridGetBits(RegValue1, 8, 8) == SX_CHIP_ID)
		{
			if(TridGetBits(RegValue1, 0, 5) == 0)
				TridHidtvChipID = TRID_HIDTVPRO_SX_REVA;
			else if(TridGetBits(RegValue1, 0, 5) == 1)
				TridHidtvChipID = TRID_HIDTVPRO_SX_REVB;
			else
			{
				//printk("Unknown SX Revision: %d\n", TridGetBits(RegValue1, 0, 5));
				TridHidtvChipID = TRID_HIDTVPRO_SX_NEW;
			}
		}
		else if(TridGetBits(RegValue1, 8, 8) == SA_CHIP_ID)
		{
			if(TridGetBits(RegValue1, 0, 3) == 0)
			{
				TridHidtvChipID = TRID_HIDTVPRO_SA_1;
			}
			else
			{
				//printk("Unknown SA Revision: %d\n", TridGetBits(RegValue1, 0, 3));
				TridHidtvChipID = TRID_HIDTVPRO_SA_NEW;
			}
		}
		else if(TridGetBits(RegValue1, 8, 8) == SAX2_CHIP_ID)
		{
			if(TridGetBits(RegValue1, 0, 3) == 0)
			{
				TridHidtvChipID = TRID_HIDTVPRO_SAX2_REVA;
			}
			else
			{
				//printk("Unknown SAX2 Revision: %d\n", TridGetBits(RegValue1, 0, 3));
				TridHidtvChipID = TRID_HIDTVPRO_SAX2_NEW;
			}
		}		
		else if(TridGetBits(RegValue1, 8, 8) == SAX3_CHIP_ID)
		{
			if(TridGetBits(RegValue1, 0, 3) == 0)
			{
				TridHidtvChipID = TRID_HIDTVPRO_SAX3_REVA;
			}
			else
			{
				//printk("Unknown SAX3 Revision: %d\n", TridGetBits(RegValue1, 0, 3));
				TridHidtvChipID = TRID_HIDTVPRO_SAX3_NEW;
			}
		}		
		else
		{
			//printk("Unkown ChipID ID\n");
		}
	}
	else
	{
		//printk("Not support chip old than AX\n");
	}
	return TridHidtvChipID;
}

static inline int Trid_Util_ChipID_IsSXSerial(TridHidtvChipID_e ChipID)
{
	return (ChipID == TRID_HIDTVPRO_SX_REVA) || 
			(ChipID == TRID_HIDTVPRO_SX_REVB) ||
			(ChipID == TRID_HIDTVPRO_SX_NEW);
}

static inline int  Trid_Util_ChipID_IsSASerial(TridHidtvChipID_e ChipID)
{
	return (ChipID == TRID_HIDTVPRO_SA_1) || 
			(ChipID == TRID_HIDTVPRO_SA_2) ||
			(ChipID == TRID_HIDTVPRO_SA_NEW);
}

static inline int Trid_Util_ChipID_IsSAX2Serial(TridHidtvChipID_e ChipID)
{
	return (ChipID == TRID_HIDTVPRO_SAX2_REVA) || 
			(ChipID == TRID_HIDTVPRO_SAX2_NEW);
}

static inline int Trid_Util_ChipID_IsSAX3Serial(TridHidtvChipID_e ChipID)
{
	return (ChipID == TRID_HIDTVPRO_SAX3_REVA) || 
			(ChipID == TRID_HIDTVPRO_SAX3_NEW);
}


#endif

