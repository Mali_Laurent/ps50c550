/**
*   @file       trid_util_buffer.h
*   @author 	He Jun
*	@version 	
*	@date    Jun 13th 2009
* 
* @if copyright_display
*		Copyright (c) 2007 Trident Microsystems, Inc.                  
*		All rights reserved                                
*	                                                                          
*	 	The content of this file or document is CONFIDENTIAL and PROPRIETARY     
*		to Trident Microsystems, Inc.  It is subject to the terms of a           
*		License Agreement between Licensee and Trident Microsystems, Inc.        
*		restricting among other things, the use, reproduction, distribution      
*		and transfer.  Each of the embodiments, including this information and   
*		any derivative work shall retain this copyright notice.
*	@endif
*	
*	More description ...
*	
*	@if modification_History
*	
*		<b>Modification History:\n</b>                                                    
*		Date				Name			Comment \n\n                             
*
*					                    
*	@endif                                                                          
*/

#ifndef __TRID_UTIL_BUFFER_H__
#define __TRID_UTIL_BUFFER_H__


#if (defined __cplusplus || defined __cplusplus__)
extern "C" 
{
#endif


typedef struct _tagTridRingBuffer{
	int	in;			/* write pointer */
	int	out;		/* read pointer */
	int	trial;		/* trial pointer */
	int	size;		/* specify buffer size, it's asked to be 2^n*/
	char buffer[0];	/* start address of buffer */
}TridRingBuffer_t;

static inline void * memcpy(void * dest, const void *src, int count)
{
	unsigned int  *tmp = (unsigned int *) dest, *s = (unsigned int *) src;
	unsigned int  tail;
	tail = (count & 0x3);
	count = (count >> 2);
	while (count > 0)
	{
		*tmp++ = *s++;
		count--;
	}
	if(0 != tail)
	{
		unsigned char *dd = (unsigned char *)(tmp);
		unsigned char *ss = (unsigned char *)(s);
		while(tail > 0)
		{
			*dd++ = *s++;
			tail--;
		}
	}
	return dest;
}

#if 0
//concern align issue later
static inline void * memcpy(void * dest, const void *src, int count)
{
	int * from = (int *)src;
	int * to = (int *)dest;
	int word_num = count/4 - 1;
	int slice = count%4 - 1;

	while(word_num >= 0)
	{
		*to= *from;
		to += 4;
		from += 4;
		word_num--;
	}

	while(slice >= 0)
	{
		*((char *)to + slice) = *((char *)from + slice);
		slice--;
	}

	return dest;
}
#endif

int Trid_Util_Ring_BufInit(volatile TridRingBuffer_t *pRingBuf, int size);
int Trid_Util_Ring_BufWrite(volatile TridRingBuffer_t *pRingBuf, void * buf, int count);
int Trid_Util_Ring_BufRead(volatile TridRingBuffer_t * pRingBuf, void * buf, int count);
int Trid_Util_Ring_BufLen(volatile TridRingBuffer_t *pRingBuf);
int Trid_Util_Ring_BufFreeLen(volatile TridRingBuffer_t *pRingBuf);


#if (defined __cplusplus || defined __cplusplus__)
}
#endif
#endif

