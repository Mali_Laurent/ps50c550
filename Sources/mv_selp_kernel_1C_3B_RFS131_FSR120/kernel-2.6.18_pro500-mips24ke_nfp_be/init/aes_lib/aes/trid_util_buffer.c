/***************************************************************************
 *
 *  The content of this file or document is CONFIDENTIAL and PROPRIETARY
 *  to Trident Microsystems, Inc.  It is subject to the terms of a
 *  License Agreement between Licensee and Trident Microsystems, Inc.
 *  restricting among other things, the use, reproduction, distribution
 *  and transfer.  Each of the embodiments, including this information and
 *  any derivative work shall retain this copyright notice.
 *
 *  Copyright (c) 2008 Trident Microsystems, Inc.
 *  All rights reserved.
 *
 ****************************************************************************
 *
 *  File:        trid_util_buffer.c
 *  Created:     Jun 13, 2009
 *  Author: He Jun
 *  Description: .
 *
 *
*****************************************************************************/

/*****************************************************************************
*

*
*****************************************************************************/

/*****************************************************************************
*                    Includes
*****************************************************************************/

#include "trid_util_buffer.h"

/*****************************************************************************
*                    Macro Definitions
*****************************************************************************/  


/*****************************************************************************
*                    Type Definitions
*****************************************************************************/


/*****************************************************************************
*                    External Data Declarations
*****************************************************************************/


/*****************************************************************************
*                    Static Data Definitions
*****************************************************************************/


/*****************************************************************************
*                    Static Function Prototypes
*****************************************************************************/


/*****************************************************************************
*                    Static Function Definitions
*****************************************************************************/

static int inline min_int(int a, int b)
{
	return (a<b ? a : b);
}

/* Return total length of data in buffer */
static int inline buff_dlen(volatile TridRingBuffer_t* bp)
{
	return ((int)((bp->in - bp->out) & (bp->size - 1)));
}

/* Return length of untried (i.e. unsent) data in buffer */
static int inline buff_untriedlen(volatile TridRingBuffer_t* bp)
{
	return ((int)((bp->in - bp->trial) & (bp->size - 1)));
}

/* Return length of trial data in buffer (i.e. data sent but unacked) */
static int inline buff_trylen(volatile TridRingBuffer_t* bp)
{
	return ((int)((bp->trial - bp->out) & (bp->size - 1)));
}

/* Return length of free space in buffer 
** free space is always 1 byte less than actual availabe space, so I/P and O/P ptr are free of overlap risk */
static int inline buff_freelen(volatile TridRingBuffer_t* bp)
{
	return (bp->size ? bp->size-1-buff_dlen(bp) : 0);
}

/* Set all the buffer poinnters to a starting value*/
static void inline buff_setall(volatile TridRingBuffer_t* bp, int start)
{
	bp->in = bp->out = bp->trial = start;
}

/*Load data into buffer, return num of bytes that could be accepted.
**If data pointer is NULL, adjust pointer but don't transfer data */
static int buff_in(volatile TridRingBuffer_t *bp, char *data, int len)
{
	int in, n, n1, n2;

	in = (int)(bp->in & (bp->size - 1));			/* Mask I/P ptr to buffer area */
	n = min_int(len, buff_freelen(bp));			/* Get max allowable length */
	n1 = min_int(n, (int)(bp->size - in));		/* Length up to end of buffer */
	n2 = n - n1;								/* Length from start to buffer */
	if(n1 && data)								/* If anything to copy .. */
		memcpy(&bp->buffer[in], data, n1);		/* .. copy up to end of buffer .. */
	if(n2 && data)								/* .. and maybe also .. */
		memcpy(bp->buffer, &data[n1], n2);		/* .. copy into start of buffer */
	bp->in += n;								/* Bump I/P pointer */
	return n;
}

/* Remove data from buffer, return number of bytes
** If data pointner is NULL, adjust pointers but don't transfer data */
static int buff_out(volatile TridRingBuffer_t* bp, char* data, int maxlen)
{
	int out, n, n1, n2;

	out = (int)(bp->out & (bp->size - 1));		/* Mask O/P ptr to buffer area */
	n = min_int(maxlen, buff_dlen(bp));			/* Get max allowable length */
	n1 = min_int(n, (int)(bp->size - out));		/* Length up to end of buff */
	n2 = n - n1;								/* Length from start of buff */
	if(n1 && data)								/* If anything to copy.. */
		memcpy(data, &bp->buffer[out], n1);		/* ..copy up to end of buffer.. */
	if(n2 && data)								/* ..and maybe also.. */
		memcpy(&data[n1], bp->buffer, n2);		/* ..copy from start of buffer.. */
	bp->out += n;								/* Bump O/P pointer */
	if(buff_untriedlen(bp) > buff_dlen(bp))		/* ..and maybe trial pointer */
		bp->trial = bp->out;
	return n;
}

#if 0	//[hejun] not enable trial feature yet
/* Remove waiting data from buffer, return number of bytes 
** If data pointer is NULL, adjust pointers but don't transfer data */
static int buff_try(TridRingBuffer_t* bp, char* data, int maxlen)
{
	int trial, n, n1, n2;

	trial = (int)(bp->trial & (bp->size - 1));	/* Mask trial ptr to buffer area */
	n = min_int(maxlen, buff_untriedlen(bp));	/* Get max allowable length */
	n1 = min_int(n, (int)(bp->size - trial));	/* Length up to end of buffer */
	n2 = n - n1;								/* Length from start of buffer */
	if(n1 && data)								/* If anything to copy .. */
		memcpy(data, &bp->buffer[trial], n1);	/* .. copy up to end of buffer .. */
	if(n2 && data)								/* .. and maybe also .. */
		memcpy(&data[n1], bp->buffer, n2);		/* .. copy from start of buffer */
	bp->trial += n;								/* Bump trial pointer */
	return n;
}

/* Rewind the trial pointer by the given byte count, return actual count */
static int buff_retry(TridRingBuffer_t* bp, int len)
{
	len = min_int(len, buff_trylen(bp));
	bp->trial -= len;
	return len;
}
#endif
/*****************************************************************************
*                    Function Definitions
*****************************************************************************/

int Trid_Util_Ring_BufInit(volatile TridRingBuffer_t *pRingBuf, int size)
{
	if(size & (size-1))
	{
		//TRACE_ERROR("Init RingBuffer failed! Its size should be 2^n bytes. ActSz:%#x\n", size);
		return -1;
	}
	
	pRingBuf->size = size;
	buff_setall(pRingBuf, 0);
	return 0;
}

int Trid_Util_Ring_BufLen(volatile TridRingBuffer_t *pRingBuf)
{
	return buff_dlen(pRingBuf);
}

int Trid_Util_Ring_BufFreeLen(volatile TridRingBuffer_t *pRingBuf)
{
	return buff_freelen(pRingBuf);
}

int Trid_Util_Ring_BufWrite(volatile TridRingBuffer_t *pRingBuf, void * buf, int count)
{
	int n;
	n = buff_in(pRingBuf, buf, count);
	return n;
}

int Trid_Util_Ring_BufRead(volatile TridRingBuffer_t * pRingBuf, void * buf, int count)
{
	int n;
	n = buff_out(pRingBuf, buf, count);
	return n;
}

/*****************************************************************************
*                       End Of File
*****************************************************************************/

