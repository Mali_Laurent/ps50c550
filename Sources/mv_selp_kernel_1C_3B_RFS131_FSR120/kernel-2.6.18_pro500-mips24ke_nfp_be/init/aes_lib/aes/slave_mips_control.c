
#include "trid_chipid.h"
#include "trid_common.h"

#define SlaveMIPS_CTRLREG	(0x15024000)

int stop_slave_mips(void)
{
	TridHidtvChipID_e chip_id;
	unsigned int val ;
	unsigned int MipsCtrlReg;

	MipsCtrlReg = SlaveMIPS_CTRLREG;
	
	chip_id = Trid_Util_ChipID_Detect();
	if (!(Trid_Util_ChipID_IsSXSerial(chip_id) ||
		Trid_Util_ChipID_IsSASerial(chip_id) ||
		Trid_Util_ChipID_IsSAX2Serial(chip_id) ||
		Trid_Util_ChipID_IsSAX3Serial(chip_id)))
	{	
		val = ReadRegWord(MipsCtrlReg);
		WriteRegWord(MipsCtrlReg , (val & (~0xFF000000)));
		//printf("Stop MIPS:Set MIPS 0x%x \n", (val & (~0xFF000000))); 
	//	printf("Stop MIPS !\n"); 
	}	
	return 0;
}


int start_slave_mips_at_address(unsigned int addr)
{
	unsigned int tmp ;
	TridHidtvChipID_e chip_id;
	unsigned int MipsCtrlReg;

	MipsCtrlReg = SlaveMIPS_CTRLREG;
	
	tmp  = ( (unsigned int)addr >> 8 ) | 0x00a00000;
	tmp |= ( (unsigned int)addr >> 20) | 0x00000800;

	WriteRegWord(MipsCtrlReg , tmp);

	chip_id = Trid_Util_ChipID_Detect();
	if (Trid_Util_ChipID_IsSXSerial(chip_id) ||
		Trid_Util_ChipID_IsSASerial(chip_id) ||
		Trid_Util_ChipID_IsSAX2Serial(chip_id) ||
		Trid_Util_ChipID_IsSAX3Serial(chip_id))
	{
		tmp = (unsigned int)(addr) | 0xa0000000;
		tmp = (tmp & 0x3fffffff)>>12;

	//	printf("Start MIPS @ addr 0x%x \n" ,tmp);
		// revB norm mode will run after slave reset
		WriteRegWord(MipsCtrlReg , (tmp|0x80000000));	
			
	}						
	else
	{
		//printf("Start MIPS @ addr 0x%x \n" ,tmp);
		// revB norm mode will run after slave reset
		WriteRegWord(MipsCtrlReg , (tmp|0x80000000));	
	}
	
	return 0;
}




