#include "aes.h"
#include "trid_util_buffer.h"
#include "trid_common.h"
#include "cacheops.h"
#define CACHE_SIZE  (32)
#define HEAD_SIZE 0x40
//#define BUFF_SIZE (512*1024)
#define DDR_WIDTH  (16)
#define KSEG0     (0x80000000)
#define KSEG1     (0xa0000000)
int stop_slave_mips(void);
int start_slave_mips_at_address(unsigned int addr);
int Init_ShareBuffer(unsigned int addr, int size);
int main_send(char *buff, int size);
int main_recv(char *buff, int size);

#define wmb()  __asm__ __volatile__( \
        "       .set    push                                    \n"     \
        "       .set    noreorder                               \n"     \
        "        sync                                             \n"     \
        "       .set    reorder                                 \n"     \
        "       .set    pop                                     \n"     \
        );

static unsigned int share_buff[0x40];

static int swap_endian_16(unsigned char * addr, unsigned int len)
{
        int i;
        unsigned char * p;
	if(len & 0xf)
		return -1;

        for(i=0,p=addr; i<len; i+=16, p+=16)
        {
                int j;
                for(j=0; j<8; j++)
                {
                        unsigned char tmp;
                        tmp = *(p+j);
                        *(p+j) = *(p+15-j);
                        *(p+15-j) = tmp;
                }
        }

	return 0;
}

/*
static void reverseorder16byte(unsigned char * input)
{
	char temp;
	int i;
	for(i=0; i<8; i++)
	{
		temp = *(input + i);
		*(input + i) = *(input + 15 - i);
		*(input + 15 - i) = temp;
	}
}
*/
void aes_init(unsigned int address)
{
	write_mask_reg(0xb5022044, 0xffffff00, 0x0a);
	write_mask_reg(0xb5022044, 0xffff03ff, 0x2200);
	write_mask_reg(0xb5022048, 0x00ffffff, 0x22000000);
	write_mask_reg(0xb5022050, 0x00ffffff, 0x22000000);
	write_mask_reg(0xb5024004, 0x0, 0x0);
	write_mask_reg(0xb5024008, 0x0, 0x0);
	write_mask_reg(0xb5022050, 0xffff00ff, 0x4100);
	//input = (unsigned char *)(address | KSEG0);
	//output = (unsigned char *)((address + BUFF_SIZE) |KSEG0);
	Init_ShareBuffer(VIRTTOPHYS((unsigned int)share_buff), sizeof(share_buff));
	//printf("share buffer addr = %08x\n", share_buff);

}

void aes_start()
{
	//start_slave_mips_at_address(0x3800000);
	write_mask_reg(0xb5022050, 0xffff00ff, 0x4100);
	start_slave_mips_at_address(0xd000000);
}

void aes_stop()
{
	stop_slave_mips();
	write_mask_reg(0xb5022050, 0xffff00ff, 0x00);
}

int Trid_Masterkey_AES_ECB_Enc( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength)
{
	swap_endian_16(u32SrcAddr, u32InDataLength & (~0xf));
	decrypt(TRID_MODE_AES_ECB, 1,  (unsigned char *)u32SrcAddr, u32InDataLength, (unsigned char *)u32DstAddr, TRID_MASTER_KEY, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
	
	*pu32OutDataLength = u32InDataLength;
	swap_endian_16(u32SrcAddr, u32InDataLength & (~0xf));
	swap_endian_16(u32DstAddr, *pu32OutDataLength & (~0xf));

	return 0;
}

/*
int Trid_Masterkey_AES_ECB_Dec( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength)
{
	swap_endian_16(u32SrcAddr, u32InDataLength & (~0xf));
	decrypt(TRID_MODE_AES_ECB, 0,  (unsigned char *)u32SrcAddr, u32InDataLength, (unsigned char *)u32DstAddr, TRID_MASTER_KEY, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
	
	*pu32OutDataLength = u32InDataLength;	
	swap_endian_16(u32SrcAddr, u32InDataLength & (~0xf));
	swap_endian_16(u32DstAddr, *pu32OutDataLength & (~0xf));
	return 0;
}

int Trid_Securekey_AES_ECB_Enc( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength)
{
	swap_endian_16(u32SrcAddr, u32InDataLength & (~0xf));
	decrypt(TRID_MODE_AES_ECB, 1,  (unsigned char *)u32SrcAddr, u32InDataLength, (unsigned char *)u32DstAddr, TRID_SECURE_KEY, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
	
	*pu32OutDataLength = u32InDataLength;	
	swap_endian_16(u32SrcAddr, u32InDataLength & (~0xf));
	swap_endian_16(u32DstAddr, *pu32OutDataLength & (~0xf));
	return 0;
}
*/

int Trid_Securekey_AES_ECB_Dec( unsigned int u32SrcAddr,  unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int * pu32OutDataLength)
{
	swap_endian_16(u32SrcAddr, u32InDataLength & (~0xf));
	decrypt(TRID_MODE_AES_ECB, 0,  (unsigned char *)u32SrcAddr, u32InDataLength, (unsigned char *)u32DstAddr, TRID_SECURE_KEY, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00);
	
	*pu32OutDataLength = u32InDataLength;	
	swap_endian_16(u32SrcAddr, u32InDataLength & (~0xf));
	swap_endian_16(u32DstAddr, *pu32OutDataLength & (~0xf));
	return 0;
}

int Trid_AES_CBC_Enc( unsigned char *key,  unsigned char * iv, unsigned int u32SrcAddr, unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength)
{
        unsigned int dwKey[4]= {0};
        unsigned int dwIv[4]= {0};
	if(0 == u32InDataLength) return -1;
	if(u32InDataLength & 0xf) return -1;
        memcpy(dwKey, key, 16);
        memcpy(dwIv,  iv, 16);
        decrypt(TRID_MODE_AES_CBC, 1,  (unsigned char *)u32SrcAddr, u32InDataLength, (unsigned char *)u32DstAddr, TRID_USER_KEY,
                dwIv[0], dwIv[1], dwIv[2], dwIv[3], dwKey[0], dwKey[1], dwKey[2], dwKey[3]);

        *pu32OutDataLength = u32InDataLength;
}

/*
int Trid_AES_CBC_Dec( unsigned char *key,  unsigned char * iv, unsigned int u32SrcAddr, unsigned int u32InDataLength,  unsigned int u32DstAddr,  unsigned int *pu32OutDataLength)
{
	unsigned int dwKey[4]= {0};
    	unsigned int dwIv[4]= {0};
	if(0 == u32InDataLength) return -1;
	if(u32InDataLength & 0xf) return -1;
    	memcpy(dwKey, key, 16);
    	memcpy(dwIv,  iv, 16);  
	decrypt(TRID_MODE_AES_CBC, 0,  (unsigned char *)u32SrcAddr, u32InDataLength, (unsigned char *)u32DstAddr, TRID_USER_KEY, dwIv[0], dwIv[1], dwIv[2], dwIv[3], dwKey[0], dwKey[1], dwKey[2], dwKey[3]);
	
	*pu32OutDataLength = u32InDataLength;	
	return 0;
}
*/

//void Trid_AES_128_Enc(uint8_t  *key, uint8_t  *plainText, uint8_t  *cipherText) //ROUNDS=10
/*
void Trid_AES_128_Enc(unsigned char  *key, unsigned char  *plainText, unsigned char  *cipherText) //ROUNDS=10
{}
*/
extern int printf(const char *fmt, ...);

/*
static inline turnover_buffer(unsigned char *buff, unsigned int len)
{
	unsigned char *start, *end;
	unsigned int register i;
	unsigned char register ch;
	end = buff + len - 1;
	start = buff;
	i = len>>1;
	while(i--)
	{
		ch = *end;
		*end = *start;
		*start = ch;
		start++;
		end--;
		
	}
}
*/

int decrypt(tridDecryptMode_e eMode, 
		char bEncrypt, 
		unsigned char* pInputData, 
		unsigned int dwDataLen, 
		unsigned char* pOutputData,
		 tridKeyType_e KeyType,
		 unsigned int dwIV0,
		 unsigned int dwIV1,
		 unsigned int dwIV2,
		 unsigned int dwIV3,
		 unsigned int dwKey0,
		 unsigned int dwKey1,
		 unsigned int dwKey2,
		 unsigned int dwKey3)
{
	//unsigned int  len = 0;
	//unsigned int  *p;
	//unsigned int offset = 0;
	//unsigned int remnant = dwDataLen;
	packet_t pack;
#if 0	
	while( len + DDR_WIDTH <= dwDataLen)
	{
		turnover_buffer(pInputData + len, DDR_WIDTH);
		len += DDR_WIDTH;
	}
	while(offset < dwDataLen)
	{
		if(remnant > (BUFF_SIZE - HEAD_SIZE)) len = BUFF_SIZE - HEAD_SIZE ;
		else len = remnant;
#endif
#if 0
	//	if(remnant > (16)) len = 16 ;
	//	else len = remnant;
	//	memcpy(input + HEAD_SIZE, pInputData + offset, len); 
		input[0] = (unsigned char)eMode;
		input[1] = (unsigned char)bEncrypt;
		input[2] = (unsigned char)KeyType;
		input[3] = 0;
		*p++ = dwIV0;
		*p++ = dwIV1;
		*p++ = dwIV2;
		*p++ = dwIV3;
		*p++ = dwKey0;
		*p++ = dwKey1;
		*p++ = dwKey2;
		*p++ = dwKey3;
#endif 
       		pack.send = VIRTTOPHYS((unsigned int)pInputData);
		pack.recv = VIRTTOPHYS((unsigned int)pOutputData);
	        pack.len = dwDataLen;
		pack.eMode = (unsigned char )eMode;
		pack.bEncrypt = (unsigned char)bEncrypt;
		pack.KeyType = (unsigned char)KeyType;
		pack.dwIV0 = dwIV0;
		pack.dwIV1 = dwIV1;
		pack.dwIV2 = dwIV2;
		pack.dwIV3 = dwIV3;
		pack.dwKey0 = dwKey0;
		pack.dwKey1 = dwKey1;
		pack.dwKey2 = dwKey2;
		pack.dwKey3 = dwKey3;
		wmb();
       		flush_dcache_range(pInputData, pInputData + dwDataLen);
//printf("\npara:%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p,%p\n",pack.send, pack.recv,pack.len, pack.eMode, pack.bEncrypt, pack.KeyType, pack.dwIV0, pack.dwIV1, pack.dwIV2, pack.dwIV3, pack.dwKey0, pack.dwKey1, pack.dwKey2, pack.dwKey3);
		main_send((char *)&pack, sizeof(packet_t));//8??taolin
		main_recv((char *)&pack, sizeof(packet_t));//8??taolin
      		//output = PHYSTOKSEG0((unsigned int )pack.phy);
       		blast_dcache_range(pOutputData, pOutputData + dwDataLen);
		/*memcpy(pOutputData + offset, output, len);
		offset += len;
		remnant -= len;*/
#if 0
		if (eMode == TRID_MODE_AES_CBC)
		{
			if(remnant > 0)
			{
				 if(bEncrypt)
   				 {
					unsigned char *pHandleOutput = pOutputData + offset;
			        	dwIV0 = ((*(pHandleOutput-1))<<24)|((*(pHandleOutput-2))<<16)|((*(pHandleOutput-3))<<8)|(*(pHandleOutput-4)); 
      					dwIV1 = ((*(pHandleOutput-5))<<24)|((*(pHandleOutput-6))<<16)|((*(pHandleOutput-7))<<8)|(*(pHandleOutput-8)); 
    					dwIV2 = ((*(pHandleOutput-9))<<24)|((*(pHandleOutput-10))<<16)|((*(pHandleOutput-11))<<8)|(*(pHandleOutput-12));		
        				dwIV3 = ((*(pHandleOutput-13))<<24)|((*(pHandleOutput-14))<<16)|((*(pHandleOutput-15))<<8)|(*(pHandleOutput-16));
    				}
    				else
    				{
					unsigned char *pHandleInput = pInputData + offset;
		dwIV0 = ((*(pHandleInput-1))<<24)|((*(pHandleInput-2))<<16)|((*(pHandleInput-3))<<8)|(*(pHandleInput-4)); 
        	dwIV1 = ((*(pHandleInput-5))<<24)|((*(pHandleInput-6))<<16)|((*(pHandleInput-7))<<8)|(*(pHandleInput-8)); 
        	dwIV2 = ((*(pHandleInput-9))<<24)|((*(pHandleInput-10))<<16)|((*(pHandleInput-11))<<8)|(*(pHandleInput-12)); 
        	dwIV3 = ((*(pHandleInput-13))<<24)|((*(pHandleInput-14))<<16)|((*(pHandleInput-15))<<8)|(*(pHandleInput-16));
			}	
			}
		}
	}
#endif
#if 0
	len = 0;
	while( len + DDR_WIDTH <= dwDataLen)
	{
		turnover_buffer(pOutputData + len, DDR_WIDTH);
		len += DDR_WIDTH;
	}
#endif
	return dwDataLen;

}

void get_rand(unsigned int *buf, int num)
{
	//volatile int i = 10000;
	int i;
	unsigned int tmp, addr;

	WriteRegWord(0x1502b314, 0xfa0f4c83);
	WriteRegWord(0x1502b318, 0xb11d6e25);
	WriteRegWord(0x1502b31c, 0xdd0951cb);
	WriteRegWord(0x1502b320, 0x32da592b);

	WriteRegWord(0x1502b324, 0x798d12bf);
	WriteRegWord(0x1502b328, 0x7119863c);
	WriteRegWord(0x1502b32c, 0xfdd027bc);
	WriteRegWord(0x1502b330, 0x11d15102);

	tmp = ReadRegWord(0x1502b310);
	WriteRegWord(0x1502b310, tmp | (1 << 0));
	ReadRegWord(0x1502b310);
	//delay 1000us
	//udelay(1000);
	//while(i--);

	tmp = ReadRegWord(0x1502b310);
	WriteRegWord(0x1502b310, tmp & (~(1 << 16)));
	tmp = ReadRegWord(0x1502b310);
	WriteRegWord(0x1502b310, tmp | (1 << 8));

	while((ReadRegWord(0x1502b310) & (1 << 8)));

//	printk("random: %08x, %08x, %08x, %08x\n", ReadRegWord(0x1502b338),ReadRegWord(0x1502b33c),ReadRegWord(0x1502b340),ReadRegWord(0x1502b344));
	addr = 0x1502b338;
	num = (num > 4) ? 4 : num;
	for(i=0; i<num; i++)
	{
		buf[i] = ReadRegWord(addr + i*4);
	}
}
