//For master mips

#include "trid_common.h"
#include "trid_util_buffer.h"

#define MIN_FIFO_DEPTH	16
#define NULL	((void*)0)

#define TOVIRTADRS(Addr) ((Addr&0x1fffffff) | 0xa0000000) 
#define VIRTTOPHYS(Addr) (Addr&0x1fffffff)

//For SA1
#define MasterCMD	(0x15024100)	// M0
#define MReg1		(0x15024104)	// M1
#define MReg2		(0x15024108)	// M2
#define MasterRdy	(0x1502410c)	// M3

#define SlaveCMD	(0x15024140)	// S0

#if 0
//For SX
#define MasterCMD	(0x15024080)	// M0
#define MReg1		(0x15024084)	// M1
#define MReg2		(0x15024088)	// M2
#define MasterRdy	(0x1502408c)	// M3

#define SlaveCMD	(0x150240c0)	// S0

#endif

#define CMD_SESSION_ID_MASK	(0xffff)

#define TridSetBits(RegValue, StartBit, Bits, NewValue) 		\
									do{ 											\
											(RegValue) &= ~(((0x1<<(Bits))-1)<<(StartBit)); 	\
											(RegValue) |= ((NewValue)&((0x1<<(Bits))-1))<<(StartBit);\
									}while(0)

#define TridGetBits(RegValue, StartBit, Bits) (((RegValue) >> (StartBit)) & ((0x1<<(Bits))-1))


static unsigned short session_id = 0;
static int	bSendDone = 0;
static volatile TridRingBuffer_t *pM2SFIFO=NULL, *pS2MFIFO=NULL;

static void UpdateStat(int WrLen, int RdLen)
{
	//	bit31 ~ bit20	|	bit19 ~ bit8	|	bit7 ~ bit1	|	bit0
	//	TotalWrLen	|	TotalRdLen	|	Reserved		|	MasterReady
	unsigned int RegValue, TotalWr, TotalRd;
	RegValue = ReadRegWord(MasterRdy);
	TotalWr = TridGetBits(RegValue, 20, 12);
	TotalRd = TridGetBits(RegValue, 8, 12);
	TotalWr += WrLen;
	TotalRd += RdLen;
	TridSetBits(RegValue, 20, 12, TotalWr);
	TridSetBits(RegValue, 8, 12, TotalRd);
	WriteRegWord(MasterRdy, RegValue);
	//printf("StatReg: %#x, TotalWr:%#x, TotalRd:%#x\n", RegValue, TotalWr, TotalRd);
}

int Init_ShareBuffer(unsigned int addr, int size)
{
	int len;

	if(size < 2*(sizeof(TridRingBuffer_t) + MIN_FIFO_DEPTH))
	{
		// size is too small
		return -1;
	}

	len = MIN_FIFO_DEPTH;
	while((2*len) <= (size/2 - sizeof(TridRingBuffer_t)))
		len *= 2;

	//Setup FIFO
	addr = TOVIRTADRS(addr);//uncached
	pM2SFIFO = (TridRingBuffer_t*)addr;
	pS2MFIFO = (TridRingBuffer_t*)(addr + size/2);
	
	Trid_Util_Ring_BufInit(pM2SFIFO, len);
	Trid_Util_Ring_BufInit(pS2MFIFO, len);

	//printk("=====================FIFO Information=====================\n");
	//printk("[M2SFIFO]in:%d, out:%d, trial:%d, size:%#x, buffer:%#x\n", pM2SFIFO->in, pM2SFIFO->out, pM2SFIFO->trial, pM2SFIFO->size, pM2SFIFO->buffer);
	//printk("[S2MFIFO]in:%d, out:%d, trial:%d, size:%#x, buffer:%#x\n", pS2MFIFO->in, pS2MFIFO->out, pS2MFIFO->trial, pS2MFIFO->size, pS2MFIFO->buffer);
	//printk("*********************************************************************\n");

	//Notify slave of FIFO addr
	WriteRegWord(MReg1,	VIRTTOPHYS((unsigned int)pM2SFIFO));
	WriteRegWord(MReg2, VIRTTOPHYS((unsigned int)pS2MFIFO));
	//init CMD
	WriteRegWord(MasterCMD, 0);
	//Notify slave of Master is ready
	WriteRegWord(MasterRdy, 1);
	return 0;
}


int main_send(char buff[], int size)
{
	int Wrlen = -1;
	if(pM2SFIFO != NULL && !bSendDone)
	{
		unsigned int value;
		session_id ++;
		Wrlen = Trid_Util_Ring_BufWrite(pM2SFIFO, (void*)buff, size);
		value = Wrlen<<16;
		value |= (session_id & CMD_SESSION_ID_MASK);
		WriteRegWord(MasterCMD, value);
		UpdateStat(Wrlen, 0);
		bSendDone = 1;
	}
	return Wrlen;
}

int main_recv(char buff[], int size)
{
	int Rdlen = -1;
	if(pS2MFIFO != NULL && bSendDone)
	{
		while((ReadRegWord(SlaveCMD) & CMD_SESSION_ID_MASK) != session_id);
		//printk("GetSID:%#x, session_id:%#x\n", (ReadRegWord(SlaveCMD) & CMD_SESSION_ID_MASK), session_id);
		Rdlen = Trid_Util_Ring_BufRead(pS2MFIFO, (void*)buff, size);
		UpdateStat(0, Rdlen);
		bSendDone = 0;
	}
	return Rdlen;
}


