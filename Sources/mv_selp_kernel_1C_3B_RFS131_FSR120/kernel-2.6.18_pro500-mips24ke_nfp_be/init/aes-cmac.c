/****************************************************************/
/* AES-CMAC with AES-128 bit                                    */
/* CMAC     Algorithm described in SP800-38B                    */
/* Author: Junhyuk Song (junhyuk.song@samsung.com)              */
/*         Jicheol Lee  (jicheol.lee@samsung.com)               */
/****************************************************************/

#include <linux/syscalls.h>
#include <linux/aes-cmac.h>

/* For CMAC Calculation */
unsigned char const_Rb[SZ_AES_UNIT] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x87
};

// file���� ���̻� ������ �� ���� ��� FF�� ��� return�Ѵ�.
int read_from_file_or_ff (int fd, unsigned char *buf, int size)
{
	int read_cnt;
	int i;

	read_cnt = sys_read(fd, buf, size);

	if( read_cnt <= 0 ) {
		read_cnt = 0;
	}
	
	for(i=read_cnt; i<size; i++) {
		buf[i] = 0xff;
	}
	return size;
}

void xor_4_aligned(unsigned char *a, unsigned char *b, unsigned char *out, int len)
{
    int i;
	if((len & 0x3) == 0)
	{
		unsigned int *aa = (unsigned int *)a;
		unsigned int *bb = (unsigned int *) b;
		unsigned int *cc = (unsigned int *) out;
		for (i=0;i<(len/4); i++)
		{
		    cc[i] = aa[i] ^ bb[i];
		}
	}
	else
	{
	    for (i=0;i<len; i++)
	    {
	        out[i] = a[i] ^ b[i];
	    }
	}
	return ;
}

void leftshift_onebit(unsigned char *input,unsigned char *output)
{
    int         i;
    unsigned char overflow = 0;

    for ( i=15; i>=0; i-- ) {
        output[i] = input[i] << 1;
        output[i] |= overflow;
        overflow = (input[i] & 0x80)?1:0;
    }
    return;
}

void Change16Order(unsigned char * input)
{

// Be careful.. it's changed src itself... 
    char temp;
	int i;
    for( i= 0; i<8; i++)
    {
        temp = *(input + i);
        *(input+i) = *(input+15-i);
        *(input+15-i) =temp;
    }
}

void generate_subkey(unsigned char *key, unsigned char *K1, unsigned char *K2, int isHW)
{

	static  unsigned char __attribute__ ((aligned(64)))   L[SZ_AES_UNIT]={0,};
	static  unsigned char __attribute__ ((aligned(64)))  Z[SZ_AES_UNIT]={0,};

	unsigned char tmp[SZ_AES_UNIT]={0,};
	unsigned char iv[SZ_AES_UNIT]={0,};
	unsigned int dummy =0;

	if((((unsigned int)L & 0x3f) != 0) || (((unsigned int)Z & 0x3f) != 0) )
	{
		SCU_PRINTK_ERR(" addr is not 64 bytes aligned: L %p, Z %p", L, Z);
	}

	if(isHW == USE_AES_HW)
	{
		Trid_AES_CBC_Enc(key, iv, (unsigned int) Z, SZ_AES_UNIT, (unsigned int)L, (unsigned int *)&dummy);  // Same result w/ AES_128 if src & iv is zero
		Change16Order(L); // To make same order w/ standard order
	}
	else
	{
	    AES_128(key,Z,L);
	}
    if ( (L[0] & 0x80) == 0 ) { /* If MSB(L) = 0, then K1 = L << 1 */
        leftshift_onebit(L,K1);
    } else {    /* Else K1 = ( L << 1 ) (+) Rb */
        leftshift_onebit(L,tmp);
        xor_4_aligned(tmp,const_Rb,K1,SZ_AES_UNIT);
    }

    if ( (K1[0] & 0x80) == 0 ) {
        leftshift_onebit(K1,K2);
    } else {
        leftshift_onebit(K1,tmp);
        xor_4_aligned(tmp,const_Rb,K2,SZ_AES_UNIT);
    }
    return;
}

void padding ( unsigned char *lastb, unsigned char *pad, int length )
{
    int         j;

    /* original last block */
    for ( j=0; j<SZ_AES_UNIT; j++ ) {
        if ( j < length ) {
            pad[j] = lastb[j];
        } else if ( j == length ) {
            pad[j] = 0x80;
        } else {
            pad[j] = 0x00;
        }
    }
	return;
}

void AES_CMAC_with_fd_and_size(unsigned char *key, int fd, int real_size, unsigned char *mac)
{

	static unsigned char __attribute__ ((aligned(64))) src[SZ_ENC_UNIT] ={0,};
	static unsigned char __attribute__ ((aligned(64))) dest[SZ_ENC_UNIT] ={0,};

	unsigned char iv[SZ_AES_UNIT]={0,};
	unsigned char K1[SZ_AES_UNIT]={0,};
	unsigned char K2[SZ_AES_UNIT] = {0,};
	unsigned char padded[SZ_AES_UNIT] = {0,};
	unsigned char M_last[SZ_AES_UNIT] = {0,};
	unsigned char subkey[SZ_AES_UNIT] = {0,};
	unsigned int dummy = 0;
	int total_size = 0;
	int read_cnt = 0;
	int flag = 0;
	int enuri = 0;
	
	if((((unsigned int)src &0x3f) != 0) || (((unsigned int)dest &0x3f) != 0) )
	{
		SCU_PRINTK_ERR(" addr is not 64 bytes aligned: src %p, dest %p", src, dest);
	}


	generate_subkey(key,K1,K2,USE_AES_HW);

	if( (enuri=(real_size%SZ_AES_UNIT)) != 0) {
		memcpy(subkey, K2, SZ_AES_UNIT);
		flag = 1;
	} else {
		memcpy(subkey, K1, SZ_AES_UNIT);
		flag = 0;
	}
	
	read_cnt = read_from_file_or_ff(fd, src, SZ_ENC_UNIT);

	if(read_cnt > real_size) {
		read_cnt = real_size;
	}
	total_size = read_cnt;
	while(total_size < real_size ) { 

		// CBC should be 64 bytes aligned..

		Trid_AES_CBC_Enc(key, iv, (unsigned int) src, read_cnt, (unsigned int) dest, (unsigned int *)&dummy);  

		memcpy(iv, &dest[read_cnt-SZ_AES_UNIT], SZ_AES_UNIT);
		Change16Order(iv);	// IV should be reversed for the next step w/ HW AES engine.
		read_cnt = read_from_file_or_ff(fd, src, SZ_ENC_UNIT);
	
		if( (total_size+read_cnt) > real_size ) {
			read_cnt = real_size - total_size;			
		}
		total_size += read_cnt;
	}

	// 1. padding: if non-complete block
	if(flag == 1) {
		padding(&src[read_cnt-enuri], padded, enuri);
		memcpy(&src[read_cnt-enuri], padded, SZ_AES_UNIT);
		read_cnt = read_cnt - enuri + SZ_AES_UNIT;
	}

	// 2. xor
	memcpy(M_last, &src[read_cnt-SZ_AES_UNIT], SZ_AES_UNIT);

	xor_4_aligned(M_last, subkey, &src[read_cnt-SZ_AES_UNIT],SZ_AES_UNIT);
	Trid_AES_CBC_Enc(key, iv, (unsigned int) src, read_cnt, (unsigned int)dest, (unsigned int *)&dummy);  

	memcpy(mac, &dest[read_cnt-SZ_AES_UNIT], SZ_AES_UNIT);

	return;
}

