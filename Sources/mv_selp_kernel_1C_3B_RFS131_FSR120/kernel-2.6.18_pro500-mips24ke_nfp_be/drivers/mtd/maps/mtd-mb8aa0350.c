/*
 * Flash memory access on Fujitsu ARM1176 evaluation board using
 * spansion S29GL128M90TF1R1 NOR Flash.
 *
 * Initially based on drivers/mtd/maps/h720x-flash.c,
 * (C) 2002 Jungjun Kim <jungjun.kim@hynix.com>
 *     2003 Thomas Gleixner <tglx@linutronix.de>
 *
 * Author: Santosh Shukla <sshukla@sh.mvista.com>
 *
 * 2009 (c) MontaVista Software, Inc. This file is licensed under
 * the terms of the GNU General Public License version 2. This program
 * is licensed "as is" without any warranty of any kind, whether express
 * or implied.
 */

#include <linux/module.h>
#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/errno.h>
#include <linux/slab.h>

#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <asm/hardware.h>
#include <linux/io.h>

#define MB8AA_FLASH_SIZE	0x02000000
#define MB8AA_FLASH_PHYS	0x10000000

static struct mtd_info *mymtd;

static struct map_info mb8aa0350_map = {
	.name =		"mb8aa0350",
	.bankwidth =	4,
	.size =		MB8AA_FLASH_SIZE,
	.phys =		MB8AA_FLASH_PHYS,
};

static struct mtd_partition mb8aa0350_partitions[] = {
	{
		.name = "u-boot",
		.size = 0x00040000,
		.offset = 0x00000000,
	}, {
		.name = "kernel",
		.size = 0x00600000,
		.offset = 0x00040000,
	}, {
		.name = "rootfs",
		.size = 0x01000000,
		.offset = 0x00640000,
	}, {
		.name = "reserved",
		.size = 0x009c0000,
		.offset = MTDPART_OFS_APPEND
	}

};

#define NUM_PARTITIONS ARRAY_SIZE(mb8aa0350_partitions)

static int                   nr_mtd_parts;
static struct mtd_partition *mtd_parts;
static const char *probes[] = { "cmdlinepart", NULL };

/*
 * Initialize FLASH support
 */
static int __init mb8aa0350_mtd_init(void)
{

	char	*part_type = NULL;

	mb8aa0350_map.virt = ioremap(MB8AA_FLASH_PHYS, MB8AA_FLASH_SIZE);

	if (!mb8aa0350_map.virt) {
		printk(KERN_ERR "mb8aa0350-MTD: ioremap failed\n");
		return -EIO;
	}

	simple_map_init(&mb8aa0350_map);

	/* Probe for flash bankwidth 4 */
	printk(KERN_INFO "mb8aa0350-MTD probing 32bit FLASH\n");
	mymtd = do_map_probe("cfi_probe", &mb8aa0350_map);
	if (!mymtd) {
		printk(KERN_INFO "mb8aa0350-MTD probing 16bit FLASH\n");
	    /* Probe for bankwidth 2 */
	    mb8aa0350_map.bankwidth = 2;
	    mymtd = do_map_probe("cfi_probe", &mb8aa0350_map);
	}

	if (mymtd) {
		mymtd->owner = THIS_MODULE;

#ifdef CONFIG_MTD_PARTITIONS
		nr_mtd_parts = parse_mtd_partitions(mymtd, probes,
							&mtd_parts, 0);
		if (nr_mtd_parts > 0)
			part_type = "command line";
#endif
		if (nr_mtd_parts <= 0) {
			mtd_parts = mb8aa0350_partitions;
			nr_mtd_parts = NUM_PARTITIONS;
			part_type = "builtin";
		}

		printk(KERN_INFO "Using %s partition table\n", part_type);
		add_mtd_partitions(mymtd, mtd_parts, nr_mtd_parts);
		return 0;
	}

	iounmap((void *)mb8aa0350_map.virt);
	return -ENXIO;
}

/*
 * Cleanup
 */
static void __exit mb8aa0350_mtd_cleanup(void)
{

	if (mymtd) {
		del_mtd_partitions(mymtd);
		map_destroy(mymtd);
	}

	/* Free partition info, if commandline partition was used */
	if (mtd_parts && (mtd_parts != mb8aa0350_partitions))
		kfree(mtd_parts);

	if (mb8aa0350_map.virt) {
		iounmap((void *)mb8aa0350_map.virt);
		mb8aa0350_map.virt = 0;
	}
}


module_init(mb8aa0350_mtd_init);
module_exit(mb8aa0350_mtd_cleanup);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Santosh Shukla <sshukla@in.mvista.com>");
MODULE_DESCRIPTION("MTD map driver for FUJITSU ARM11 MB8AA0350 Prototypic Kit");
