#ifndef DMA_SYNC_API_H
#define DMA_SYNC_API_H

//#define CACHE_LINE_SIZE         0x20
#define Index_Invalidate_I      	0x00
#define Index_Writeback_Inv_D   0x01
#define Index_Load_Tag_I		0x04
#define Index_Load_Tag_D		0x05
#define Index_Store_Tag_I		0x08
#define Index_Store_Tag_D		0x09
#define Hit_Invalidate_I			0x10
#define Hit_Invalidate_D			0x11
#define Hit_Writeback_Inv_D		0x15


#define SYNC_STATUS_REG 0x1b00201c
#define SYNC_STATUS_SHIFT 26
#define SYNC_STATUS_MASK 3
#define SYNC_STATUS_MAX_RETRY 100

#define TIMER0_TCVRN    0x15027000
#define TIMER0_TRVRN    0x15027004
#define TIMER0_TCRN     0x15027008

#define TIMER0_INIT()	WriteRegWord(TIMER0_TCVRN, 0xffffffff)
#define TIMER0_START()	WriteRegHWord(TIMER0_TCRN, 0x3)
#define TIMER0_STOP()	WriteRegHWord(TIMER0_TCRN, 0)
#define TIMER0_GETCIRCLE()	(0xffffffff - ReadRegWord(TIMER0_TRVRN))

#define GLOBAL_DATA_ADDR 0xa0080000


/* for OneNand DMA and Sync mode */
#define  CONFIG_ONENAND_128MB 1
#define  CONFIG_FLASH_DMA_ACCESS 1
#define  CONFIG_ONENAND_SYNC_MODE 1

//#define  CONFIG_ONENAND_SYNC_22M 1
//#define  CONFIG_ONENAND_SYNC_33M 1
#define  CONFIG_ONENAND_SYNC_66M 1
//#define  CONFIG_ONENAND_SYNC_48M 1	//for debug


/* OneNAND memory base address */
#define ONENAND_REG_BASE	0xbc000000
#define ONENAND_MEM_BASE	0x9c000000



static inline unsigned short OneNANDReadRegHWord(unsigned long reg)
{
	unsigned short val;
	val = ReadRegHWord_S(ONENAND_REG_BASE + (reg));
#ifdef DEBUG
	printf("Read register hword: %#010x value: %#06x\n",
		ONENAND_REG_BASE + (reg), val);
#endif
	return val;

}


static inline void OneNANDWriteRegHWord(unsigned long reg, unsigned short val)
{
#ifdef DEBUG
	printf("Write register hword: %#10x with value: %#06x\n",
		ONENAND_REG_BASE + (reg), val);
#endif
	WriteRegHWord_S(ONENAND_REG_BASE + (reg), val);
}

static inline unsigned short OneNANDReadDataBuffHWord(unsigned long reg)
{
	unsigned short val;
	val = *((volatile unsigned short *)(ONENAND_MEM_BASE + reg));
#ifdef DEBUG
	printf("Read data buffer hword: %#010x value: %#06x\n",
		ONENAND_MEM_BASE + (reg), val);
#endif
	return val;
}

/* Memory Address Map Translation (Word order) */
#define ONENAND_MEMORY_MAP(x)		((x) << 1)

/*
 * External BufferRAM area
 */
#define	ONENAND_BOOTRAM				ONENAND_MEMORY_MAP(0x0000)
#define	ONENAND_DATARAM				ONENAND_MEMORY_MAP(0x0200)
#define	ONENAND_SPARERAM			ONENAND_MEMORY_MAP(0x8010)

/*
 * OneNAND Registers
 */
#define ONENAND_REG_MANUFACTURER_ID		ONENAND_MEMORY_MAP(0xF000)
#define ONENAND_REG_DEVICE_ID			ONENAND_MEMORY_MAP(0xF001)
#define ONENAND_REG_VERSION_ID			ONENAND_MEMORY_MAP(0xF002)
#define ONENAND_REG_DATA_BUFFER_SIZE	ONENAND_MEMORY_MAP(0xF003)
#define ONENAND_REG_BOOT_BUFFER_SIZE	ONENAND_MEMORY_MAP(0xF004)
#define ONENAND_REG_NUM_BUFFERS			ONENAND_MEMORY_MAP(0xF005)
#define ONENAND_REG_TECHNOLOGY			ONENAND_MEMORY_MAP(0xF006)

#define ONENAND_REG_START_ADDRESS1		ONENAND_MEMORY_MAP(0xF100)
#define ONENAND_REG_START_ADDRESS2		ONENAND_MEMORY_MAP(0xF101)
#define ONENAND_REG_START_ADDRESS3		ONENAND_MEMORY_MAP(0xF102)
#define ONENAND_REG_START_ADDRESS4		ONENAND_MEMORY_MAP(0xF103)
#define ONENAND_REG_START_ADDRESS5		ONENAND_MEMORY_MAP(0xF104)
#define ONENAND_REG_START_ADDRESS6		ONENAND_MEMORY_MAP(0xF105)
#define ONENAND_REG_START_ADDRESS7		ONENAND_MEMORY_MAP(0xF106)
#define ONENAND_REG_START_ADDRESS8		ONENAND_MEMORY_MAP(0xF107)

#define ONENAND_REG_START_BUFFER		ONENAND_MEMORY_MAP(0xF200)
#define ONENAND_REG_COMMAND				ONENAND_MEMORY_MAP(0xF220)
#define ONENAND_REG_SYS_CFG1			ONENAND_MEMORY_MAP(0xF221)
#define ONENAND_REG_SYS_CFG2			ONENAND_MEMORY_MAP(0xF222)
#define ONENAND_REG_CTRL_STATUS			ONENAND_MEMORY_MAP(0xF240)
#define ONENAND_REG_INTERRUPT			ONENAND_MEMORY_MAP(0xF241)
#define ONENAND_REG_START_BLOCK_ADDRESS	ONENAND_MEMORY_MAP(0xF24C)
#define ONENAND_REG_END_BLOCK_ADDRESS	ONENAND_MEMORY_MAP(0xF24D)
#define ONENAND_REG_WP_STATUS			ONENAND_MEMORY_MAP(0xF24E)

#define ONENAND_REG_ECC_STATUS			ONENAND_MEMORY_MAP(0xFF00)
#define ONENAND_REG_ECC_M0				ONENAND_MEMORY_MAP(0xFF01)
#define ONENAND_REG_ECC_S0				ONENAND_MEMORY_MAP(0xFF02)
#define ONENAND_REG_ECC_M1				ONENAND_MEMORY_MAP(0xFF03)
#define ONENAND_REG_ECC_S1				ONENAND_MEMORY_MAP(0xFF04)
#define ONENAND_REG_ECC_M2				ONENAND_MEMORY_MAP(0xFF05)
#define ONENAND_REG_ECC_S2				ONENAND_MEMORY_MAP(0xFF06)
#define ONENAND_REG_ECC_M3				ONENAND_MEMORY_MAP(0xFF07)
#define ONENAND_REG_ECC_S3				ONENAND_MEMORY_MAP(0xFF08)


/* function name: dma_init
 * parameter:
 * return value:
 *	void
 */
void tbml_dma_init(void);

/* function name: dma_read
 * please hold a lock before this operation to prevent other access to flash.
 * parameter:
 *	src, source address
 *	dst, destination address
 *	size, reading size (the number of unsigned short)
 * return value:
 *	copied size
 */
unsigned long tbml_dma_read(unsigned short *dst, unsigned short *src, unsigned long size);

/* function name: dma_write
 * please hold a lock before this operation to prevent other access to flash.
 * parameter:
 *	src, source address
 *	dst, destination address
 *	size, writing size (the number of unsigned short)
 * return value:
 *	copied size
 */
unsigned long tbml_dma_write(unsigned short *src, unsigned short *dst, unsigned long size);

#endif /* DMA_SYNC_API_H */

