
#ifdef __KERNEL__
/* in Linux Kernel */
#include "asm/trihidtv/trihidtv.h"
#include "asm/addrspace.h"
#else
/* in Boot Loader */
#include "trihidtv.h"
#include "addrspace.h"
#endif

#include "tbml_dma_sync.h"
#include "linux/gfp.h"
#include "linux/slab.h"


#define cache_op(op,addr)                                               \
        __asm__ __volatile__(                                           \
        "       .set    push                                    \n"     \
        "       .set    noreorder                               \n"     \
        "       .set    mips3\n\t                               \n"     \
        "       cache   %0, %1                                  \n"     \
        "       .set    pop                                     \n"     \
        :                                                               \
        : "i" (op), "R" (*(unsigned char *)(addr)))


/* function name: dma_init
 * parameter:
 *	nsec, nano second, flash accessing timing
 * return value:
 *	void
 */
void tbml_dma_init(void)
{
	unsigned int reg32;
	
#ifdef CONFIG_TRIHIDTV_CPU_350M
	reg32 = ReadRegWord(0x1502204c); //bit27262524 = 0000
	reg32 &= 0xff00ffff; //66Mhz
	reg32 |= 0x00210000; 
	WriteRegWord(0x1502204c, reg32); /* open flash dma access */
	
#elif CONFIG_TRIHIDTV_CPU_425M
	reg32 = ReadRegWord(0x15022058);
	reg32 &= 0xffffff00;
	reg32 |= 0x00000021;
	WriteRegWord(0x15022058, reg32 ); /* open flash dma access */
#endif
         	
}



/* function name: dma_read
 * please hold a lock before this operation to prevent other access to flash.
 * parameter:
 *	src, source address
 *	dst, destination address
 *	size, reading size (the number of unsigned short)
 * return value:
 *	copied size
 */
unsigned long tbml_dma_read(unsigned short *dst, unsigned short *src, unsigned long size)
{

#ifdef CONFIG_TRIHIDTV_CPU_350M
#define CACHE_LINE_SIZE 32 
	volatile unsigned int int_indicator[CACHE_LINE_SIZE/sizeof(unsigned int)] ____cacheline_aligned;
	unsigned int volatile *ptr_int_indicator;

	unsigned int data_buff = (unsigned int)src;
	unsigned int local_buff = (unsigned int)dst;
	unsigned int buff_len = (unsigned int)size /* * sizeof(unsigned short)*/;
	int i;
	int clock_loop=0;

	unsigned short reg16;
	unsigned int reg32;

	ptr_int_indicator = (unsigned int *)PHYSTOKSEG1(((unsigned int)int_indicator));

        for(i = 0; i < buff_len + CACHE_LINE_SIZE ; i += CACHE_LINE_SIZE)
        {
                cache_op(Hit_Writeback_Inv_D, local_buff + i);
        }

#ifdef CONFIG_ONENAND_SYNC_MODE
	//make OneNAND chip enter SYNC mode...
	reg32 = ReadRegWord(0x1b002008); //bit27262524 = 0000
	reg32 &= 0xf0ffffff; //66Mhz
	WriteRegWord(0x1b002008, reg32);

	OneNANDWriteRegHWord(ONENAND_REG_SYS_CFG1, 0xe8e0);
	while(!(0x8000&OneNANDReadRegHWord(ONENAND_REG_SYS_CFG1)));

	//set it to Async mode to write registers in advance.
	reg32 = ReadRegWord(0x1b002008); //bit27262524 = 0000
	reg32 &= 0xf0ffffff;
	WriteRegWord(0x1b002008, reg32);

#ifdef CONFIG_ONENAND_SYNC_33M
        reg32 = ReadRegWord(0x1b002020); //bit1514 = 01
        reg32 &= 0xffff3fff;
        reg32 |= 0x4000;
        WriteRegWord(0x1b002020, reg32);
#elif  CONFIG_ONENAND_SYNC_35M
        reg16 = ReadRegHWord(0x1b000006); //bit43 = 01
        reg16 &= 0xffe7;
        reg16 |= 0x8;
        WriteRegHWord(0x1b000006, reg16);
#elif CONFIG_ONENAND_SYNC_48M
        reg32 = ReadRegWord(0x1b002020);//bit1514 = 11
        reg32 |= 0xc000;
        WriteRegWord(0x1b002020, reg32);
        reg32 = ReadRegWord(0x1b000034);//bit1514 = 01
        reg32 &= 0xffff3fff;
        reg32 |= 0x4000;
        WriteRegWord(0x1b000034, reg32);
#elif CONFIG_ONENAND_SYNC_66M
        reg32 = ReadRegWord(0x1b002020);//bit1514 = 11
        reg32 |= 0xc000;
        WriteRegWord(0x1b002020, reg32);
        reg32 = ReadRegWord(0x1b000034);//bit1514 = 10
        reg32 &= 0xffff3fff;
        reg32 |= 0x8000;
        WriteRegWord(0x1b000034, reg32);
        clock_loop = 1;
#elif CONFIG_ONENAND_SYNC_83M
        reg32 = ReadRegWord(0x1b002020);//bit1514 = 11
        reg32 |= 0xc000;
        WriteRegWord(0x1b002020, reg32);
        reg32 = ReadRegWord(0x1b000034);//bit1514 = 11
        reg32 |= 0xc000;
        WriteRegWord(0x1b000034, reg32);
        clock_loop = 1;
#else

#endif

	//enable clock loop back for 66M,83M. set bit31 to 1
	if(clock_loop = 1)
	{
		reg32 = ReadRegWord(0x1b002020); 
		reg32 |= 0x80000000;
		WriteRegWord(0x1b002020, reg32);
	}
	else  //bit 30 
	{
		reg32 = ReadRegWord(0x1b002020); 
		reg32 |= 0x40000000;
		WriteRegWord(0x1b002020, reg32);
	}

	//FlashRom sync mode control
	//set burst length
	WriteRegWord(0x1b002010, 0x0434); //66

	//async mode, clk set low
	reg32 = ReadRegWord(0x1b002020); 
        reg32 &=(~(0x1<<13));
        WriteRegWord(0x1b002020, reg32);	

	//enter OneNAND SYNC mode(for flash controller...
	reg32 = ReadRegWord(0x1b002008); //bit2726 = 10
	reg32 &= 0xf3ffffff; //66
	reg32 |= 0xa000000;
	WriteRegWord(0x1b002008, reg32);//enter sync mode
	
	//make sure sync mode ready...
	reg32 = ReadRegWord(0x1b00201c);
	while((reg32 & 0x0c000000) != 0x08000000)
	{
		reg32 = ReadRegWord(0x1b00201c);
	}	
#endif	/* CONFIG_ONENAND_SYNC_MODE */


	*ptr_int_indicator = 0;
	
	/* DMA source physical address */
//	WriteRegHWord(0x1b0000b0, VIRTTOPHYS((unsigned int)data_buff) & 0xffff);
//	WriteRegHWord(0x1b0000b2, (VIRTTOPHYS((unsigned int)data_buff) >> 16) & 0xffff);
	WriteRegHWord(0x1b0000b0, data_buff & 0xffff);
	WriteRegHWord(0x1b0000b2, (data_buff >> 16) & 0xffff);
	/* DMA destination physical address */
	WriteRegHWord(0x1b0000b4, VIRTTOPHYS(local_buff) & 0xffff);
	WriteRegHWord(0x1b0000b6, (VIRTTOPHYS(local_buff) >> 16) & 0xffff);
	/* interrupt indicator */
	WriteRegHWord(0x1b0000b8, VIRTTOPHYS(((unsigned int)ptr_int_indicator)) & 0xffff);
	WriteRegHWord(0x1b0000ba, (VIRTTOPHYS(((unsigned int)ptr_int_indicator)) >> 16) & 0xffff);
	/* DMA length */
	WriteRegHWord(0x1b0000bc, (buff_len - 1) & 0xffff);
	WriteRegHWord(0x1b0000be, ((buff_len - 1) >> 16) & 0xffff);

	reg16 = ReadRegHWord(0x1b000006);
	WriteRegHWord(0x1b000006, reg16|0x1); /* enable flash DMA read access */

	/* check DMA interrupt indicator */
	while (1)
       if(*ptr_int_indicator != 0)
			break;
	
	WriteRegHWord(0x1b000006, reg16 & 0xfffe); /* disable flash DMA read access */

        for(i = 0; i < buff_len + CACHE_LINE_SIZE ; i += CACHE_LINE_SIZE)
        {
                cache_op(Hit_Invalidate_D, local_buff + i);
        }
	
#ifdef CONFIG_ONENAND_SYNC_MODE

	//disable clock loop back. bit31
	if(clock_loop = 1)
	{
		reg32 = ReadRegWord(0x1b002020); 
		reg32 &= 0x7fffffff;
		WriteRegWord(0x1b002020, reg32);
	}
	else  //bit 30 
	{
		reg32 = ReadRegWord(0x1b002020); 
		reg32 &= 0xbfffffff;
		WriteRegWord(0x1b002020, reg32);
	}
	
	// async mode, clk set low
	reg32 = ReadRegWord(0x1b002020);
        reg32 |= (1 << 13);
        WriteRegWord(0x1b002020, reg32);

	
	reg32 = ReadRegWord(0x1b002008); //bit27262524 = 0000 //66
	reg32 &= 0xf0ffffff;
	WriteRegWord(0x1b002008, reg32);
	
	reg32 = ReadRegWord(0x1b00201c);
	while((reg32 & 0x0c000000) != 0x00000000)
	{
		reg32 = ReadRegWord(0x1b00201c);
	}


#ifdef CONFIG_ONENAND_SYNC_33M
                reg32 = ReadRegWord(0x1b002020); //1514 = 01
                reg32 &= 0xffffbfff;
                WriteRegWord(0x1b002020, reg32);
#elif CONFIG_ONENAND_SYNC_35M
                reg16 = ReadRegHWord(0x1b000006);
                reg16 &= 0xfff7;
                WriteRegHWord(0x1b000006, reg16);
#elif CONFIG_ONENAND_SYNC_48M
                reg32 = ReadRegWord(0x1b002020);
                reg32 &= 0xffff3fff;
                WriteRegWord(0x1b002020, reg32);
                reg32 = ReadRegWord(0x1b000034);
                reg32 &= 0xffffbfff;
                WriteRegWord(0x1b000034, reg32);
#elif CONFIG_ONENAND_SYNC_66M
                reg32 = ReadRegWord(0x1b002020);
                reg32 &= 0xffff3fff;
                WriteRegWord(0x1b002020, reg32);
                reg32 = ReadRegWord(0x1b000034);
                reg32 &= 0xffff7fff;
                WriteRegWord(0x1b000034, reg32);
#elif CONFIG_ONENAND_SYNC_83M
                reg32 = ReadRegWord(0x1b002020);
                reg32 &= 0xffff3fff;
                WriteRegWord(0x1b002020, reg32);
                reg32 = ReadRegWord(0x1b000034);
                reg32 &= 0xffff3fff;
                WriteRegWord(0x1b000034, reg32);
#else
        //default for 22.5M
#endif

	OneNANDWriteRegHWord(ONENAND_REG_SYS_CFG1, 0x6860);
	while((0x8000&OneNANDReadRegHWord(ONENAND_REG_SYS_CFG1)));
         
#endif	/* CONFIG_ONENAND_SYNC_MODE */


#elif CONFIG_TRIHIDTV_CPU_425M

	volatile u_int int_indicator = 0;
    	volatile u_int *ptr_int_indicator = (u_int *)(PHYSTOKSEG1(((u_int)&int_indicator)));
	
	unsigned int data_buff = (unsigned int)src;
	unsigned int local_buff = (unsigned int)dst;
	unsigned int buff_len = (unsigned int)size /* * sizeof(unsigned short)*/;
	int i;
	unsigned short reg16;
	unsigned int reg32;
	
	//printk("++ %s \n", "tfsr_dma_read()");

#define CACHE_LINE_SIZE 32 
        for(i = 0; i < buff_len + CACHE_LINE_SIZE ; i += CACHE_LINE_SIZE)
        {
                cache_op(Hit_Writeback_Inv_D, local_buff + i);
        }

#ifdef CONFIG_ONENAND_SYNC_MODE
	//make OneNAND chip enter SYNC mode...
	reg32 = ReadRegWord(0x1b002008); //bit2928_27262524 = 00_0000
	reg32 &= 0xc0ffffff;
	WriteRegWord(0x1b002008, reg32);

	OneNANDWriteRegHWord(ONENAND_REG_SYS_CFG1, 0xe8e0);
	while(!(0x8000&OneNANDReadRegHWord(ONENAND_REG_SYS_CFG1)));

#if CONFIG_ONENAND_SYNC_33M
	reg32 = ReadRegWord(0x1b002020); //bit76 = 01
	reg32 &= 0xffffff3f;
	reg32 |= 0x40;
	WriteRegWord(0x1b002020, reg32);

#elif  CONFIG_ONENAND_SYNC_35M
	reg32 = ReadRegWord(0x1b002020); //bit76 = 01
	reg32 &= 0xffffff3f;
	reg32 |= 0x40;
	WriteRegWord(0x1b002020, reg32);

#elif CONFIG_ONENAND_SYNC_48M
	reg32 = ReadRegWord(0x1b002020);//bit76 = 11
	reg32 |= 0xc0;
	WriteRegWord(0x1b002020, reg32);
	reg16 = ReadRegHWord(0x1b000034);//bit15 = 1
	reg16 |= 0x8000;
	WriteRegHWord(0x1b000034, reg16);	
#elif CONFIG_ONENAND_SYNC_66M
	reg32 = ReadRegWord(0x1b002020);//bit76 = 11
//	reg32 |= 0xc0; //spi
	reg32 |= 0xc000;
	WriteRegWord(0x1b002020, reg32);
	reg16 = ReadRegHWord(0x1b000188);//bit151413 = 010
	reg16 &= 0x1fff;
	reg16 |= 0x4000;
	WriteRegHWord(0x1b000188, reg16);
#else
	//default is 22.5M
	
#endif

	//enable clock loop back for 66M,83M. set bit31 to 1
	reg32 = ReadRegWord(0x1b002020);
	reg32 |= 0x80000000;
	WriteRegWord(0x1b002020, reg32);
	
	WriteRegWord(0x1b002010, 0x0434);

	//enter OneNAND SYNC mode(for flash controller...
	reg32 = ReadRegWord(0x1b002008); //bit2928_27262524 = 00_0010
	reg32 &= 0xc0ffffff;
	reg32 |= 0x22000000;
	WriteRegWord(0x1b002008, reg32);//enter sync mode

	//make sure sync mode ready...
	reg32 = ReadRegWord(0x1b00201c);
	while((reg32 & 0x0c000000) != 0x08000000)
	{
		reg32 = ReadRegWord(0x1b00201c);
	}

#endif

	//ReadRegHWord(0x1b000400); // flush accessing flash fifo
	
	/* do DMA operation firstly */
	*ptr_int_indicator = 0;	

	/* DMA source physical address */
	WriteRegHWord(0x1b0000b0, VIRTTOPHYS((unsigned int)data_buff) & 0xffff);
	WriteRegHWord(0x1b0000b2, (VIRTTOPHYS((unsigned int)data_buff) >> 16) & 0xffff);
	/* DMA destination physical address */
	WriteRegHWord(0x1b0000b4, VIRTTOPHYS(local_buff) & 0xffff);
	WriteRegHWord(0x1b0000b6, (VIRTTOPHYS(local_buff) >> 16) & 0xffff);
	/* interrupt indicator */
	WriteRegHWord(0x1b0000b8, VIRTTOPHYS(((unsigned int)ptr_int_indicator)) & 0xffff);
	WriteRegHWord(0x1b0000ba, (VIRTTOPHYS(((unsigned int)ptr_int_indicator)) >> 16) & 0xffff);
	/* DMA length */
	WriteRegHWord(0x1b0000bc, (buff_len - 1) & 0xffff);
	WriteRegHWord(0x1b0000be, ((buff_len - 1) >> 16) & 0xffff);

	reg16 = ReadRegHWord(0x1b000006);
	WriteRegHWord(0x1b000006, reg16 |0x1); /* enable flash DMA read access */

	/* check DMA interrupt indicator */
	while(1)
	{
		if(*ptr_int_indicator != 0)
			break;
		
	}

	WriteRegHWord(0x1b000006, reg16 & 0xfffe); /* disable flash DMA read access */
        
	for(i = 0; i < buff_len + CACHE_LINE_SIZE ; i += CACHE_LINE_SIZE)
        {
                cache_op(Hit_Invalidate_D, local_buff + i);
        }

	//disable clock loop back. bit31
	reg32 = ReadRegWord(0x1b002020);
	reg32 &= 0x7fffffff;
	WriteRegWord(0x1b002020, reg32);

	
#ifdef CONFIG_ONENAND_SYNC_MODE

	reg32 = ReadRegWord(0x1b002008); //bit2928_27262524 = 0000
	reg32 &= 0xc0ffffff;
	WriteRegWord(0x1b002008, reg32);

	reg32 = ReadRegWord(0x1b00201c);	
	while((reg32 & 0x0c000000) != 0x00000000)
	{
		reg32 = ReadRegWord(0x1b00201c);
	}
	
#if CONFIG_ONENAND_SYNC_33M
	reg32 = ReadRegWord(0x1b002020); //bit76 = 01
	reg32 &= 0xffffffbf;
	WriteRegWord(0x1b002020, reg32);
	
#elif CONFIG_ONENAND_SYNC_35M
	reg32 = ReadRegWord(0x1b002020); //bit76 = 01
	reg32 &= 0xffffffbf;
	WriteRegWord(0x1b002020, reg32);	

#elif CONFIG_ONENAND_SYNC_48M
	reg32 = ReadRegWord(0x1b002020);  //bit76 = 11
	reg32 &= 0xffffff3f;
	WriteRegWord(0x1b002020, reg32);
	reg16 = ReadRegHWord(0x1b000034);
	reg16 &= 0x7fff;
	WriteRegHWord(0x1b000034, reg16);

#elif CONFIG_ONENAND_SYNC_66M
	reg32 = ReadRegWord(0x1b002020); //bit76 = 11
	//reg32 &= 0xffffff3f; //spi
	reg32 &= 0xffff3fff;
	WriteRegWord(0x1b002020, reg32);
	reg16 = ReadRegHWord(0x1b000188);//bit151413 = 010
	reg16 &= 0x1fff;
	WriteRegHWord(0x1b000188, reg16);
#else
	//default for 22.5M
#endif

	OneNANDWriteRegHWord(ONENAND_REG_SYS_CFG1, 0x6860);
	while((0x8000&OneNANDReadRegHWord(ONENAND_REG_SYS_CFG1)));

#endif
	
#endif


	return size;
}



/* function name: dma_write
 * please hold a lock before this operation to prevent other access to flash.
 * parameter:
 *	src, source address
 *	dst, destination address
 *	size, writing size (the number of unsigned short)
 * return value:
 *	copied size
 */
unsigned long tbml_dma_write(unsigned short *src, unsigned short *dst, unsigned long size)
{
	unsigned long s = (size >> 1);
	while(s--)
		*dst++ = *src++;
	return size;
}





