/* Change Logs
   2009-11-03 : ver 1.0 : trident sx 1c / 3b usb fault support 
   2009-12-11 : ver 1.1 : trident sx 1c usb hub support, remove some code which is not used.
   2009-12-18 : ver 1.2 : trident sx 1c usb port1 gpio support, change register names for readability, remove all warnings
   2009-12-29 : ver 1.3 : INTCNTR_EXT4 -> INTCOTR_EXT4, INTCNTR_EXT4 add & setting, interrupt trigger setting. 
 */

#define __KERNEL_SYSCALLS__

#include <linux/config.h>
#include <linux/init.h>
#include <linux/unistd.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/pci.h>
#include <linux/miscdevice.h>
#include <linux/fs.h>
#include <linux/slab.h>
#include <linux/semaphore.h>
#include <asm/semaphore.h>

#include <asm/io.h>
#include <asm/uaccess.h>
#include <linux/interrupt.h>
#include <linux/irq.h>

#include <linux/fcntl.h>    //  
#include <linux/syscalls.h> //  
#include <asm/uaccess.h>    //   

#include <linux/cdev.h>
#include <linux/mm.h>

#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_int.h>
#include <asm/mipsregs.h>

#define UNABLE_TO_READ  0
#define READY_TO_READ   1
#define DRM_DEVICE_NAME "usbfault"
#define DRV_DEBUG 0 // 1 - enabled, 0 - disabled
#define USBFAULT_BLOCKED_READ   0
#define USBFAULT_UNBLOCKED_READ 1
#define USBFAULT_READ_DISABLE   2

#define RST_UNABLE_TO_READ  0
#define RST_READY_TO_READ   1
#define RST_SKIP_READ       2

#define USB_FAULT_MAJOR 234

#define LOG(a...) if (DRV_DEBUG){ \
	                printk("[USB_FAULT:%d] ", __LINE__);\
	                printk(a);\
	                printk("\n");\
	               }

#ifdef CONFIG_TRIHIDTV_CPU_350M // TRIDENT_1C
// Front USB
#define INTSTAT_EXT4 0x1b005598
#define INTCOTR_EXT4 0x1b0055b6
#define INTCNTR_EXT4 0x1b0055a0
#define INVT (0x1 << 11)
#define EXT_INT_3 (0x1 << 3)
#define GPIO43_EN ((0x3 << 6) & 0xffff)
#define FALL_TRIG_43 (0x1 << 11)
// Rear USB
#define INTSTAT_EXT5 0x1b0055d8
#define INTCOTR_EXT5 0x1b0055d6
#define INTCNTR_EXT5 0x1b0055c0
#define EXT_INT_12 (0x1 << 7)
#define GPIO55_EN ((0x3 << 14) & 0xffff)
#define FALL_TRIG_55 (0x1 << 15)

#elif CONFIG_TRIHIDTV_CPU_425M  // TRIDENT_3B
#define USB1_Base_addr 0x1b003000
#define USB2_Base_addr 0x1b007000
#define USB_PORTSC 0x184
#define USB_OCA 0x10
#endif


typedef struct
{   
	struct semaphore sem;
	int iAttribute;
	int iCount;
	char bName[16];
} CSem;

static CSem semUsb;
static dev_t chardev;
static struct cdev  *chdev;
static volatile int g_ReadState = RST_UNABLE_TO_READ;

unsigned int usb_fault = 0;


static unsigned int loc_read_blocked (void)
{
    if (g_ReadState != RST_SKIP_READ) 
	{
		g_ReadState = RST_READY_TO_READ;
		down_interruptible(&semUsb.sem);
		if (g_ReadState != RST_SKIP_READ) 
			g_ReadState = RST_UNABLE_TO_READ;
	}
	else
	{
		usb_fault = 0;
	}
	return usb_fault;
}

static ssize_t usbfault_read_blocked (struct file *pFile, char *pUserBuf, size_t count, loff_t *pOffset)
{
	int result = 0;
	unsigned int buff = loc_read_blocked();
	result = copy_to_user((unsigned int*)pUserBuf, &buff, sizeof(unsigned int));
	return 1;
}

static int usbfault_ioctl (struct inode *pInode, struct file *pFile, unsigned int cmd, unsigned long arg)
{
	int result = 0;
	unsigned int buff = 0;
	switch(cmd)
	{
		case USBFAULT_BLOCKED_READ:
			buff = loc_read_blocked();
			break;
		case USBFAULT_READ_DISABLE:
			if (g_ReadState == RST_READY_TO_READ)
			{
				g_ReadState = RST_SKIP_READ;
				up(&semUsb.sem);
			}
			else
			{
				g_ReadState = RST_SKIP_READ;
			}
			buff = 0;
			break;
		default:
			buff = 0;
			break;
	}
	result = copy_to_user((unsigned int*)arg, &buff, sizeof(unsigned int));

	return result;
}

static const struct file_operations drm_fops = {
	.owner = THIS_MODULE,
	.read = usbfault_read_blocked,
	.ioctl = usbfault_ioctl,
};

irqreturn_t usbfault_irq(int irq, void *devid, struct pt_regs *regs)
{
#ifdef CONFIG_TRIHIDTV_CPU_350M // TRIDENT_1C
	if(ReadRegHWord((volatile void *)INTSTAT_EXT4) & INVT) {
		up(&semUsb.sem);
		usb_fault = 0x08;
		WriteRegHWord((volatile void *)INTSTAT_EXT4, INVT);
    }
	else if(ReadRegHWord((volatile void *)INTSTAT_EXT5) & EXT_INT_12) {
		up(&semUsb.sem);
		usb_fault = 0x10;
		WriteRegHWord((volatile void *)INTSTAT_EXT5, EXT_INT_12 );
    }
#elif CONFIG_TRIHIDTV_CPU_425M //TRIDENT_3B	
	if(ReadRegWord((volatile void *)(USB1_Base_addr + USB_PORTSC)) & USB_OCA) {
		up(&semUsb.sem);
		usb_fault = 0x8;
	}
	else if(ReadRegWord((volatile void *)USB2_Base_addr + USB_PORTSC) & USB_OCA) {
		up(&semUsb.sem);
		usb_fault = 0x10;
	}
#endif
	
	return IRQ_HANDLED;
}
		
static int __init usbfault_init(void) 
{
	int major;
	int    result;
	int ret;
	char *name = "usbfault_sem";
	int size = sizeof(name);

	chardev = MKDEV(USB_FAULT_MAJOR, 0);
	// register char device
	result = register_chrdev_region(chardev, 1, DRM_DEVICE_NAME);
	if(result)
	{
		printk("Error in alloc_chrdev_region %d \n", result);
		return result;
	}
	chdev = (struct cdev*) kmalloc(sizeof(struct cdev), GFP_ATOMIC);
	if (!chdev) {
			printk(KERN_INFO "Failed to allocate chdev structure.\n");
			return -ENOMEM;
	}
	cdev_init(chdev, &drm_fops);

	major  = MAJOR(chardev);
	chdev->owner = THIS_MODULE;
	chdev->ops = &drm_fops;

	if((result = cdev_add(chdev, chardev, 1)))
	{
		printk("cdev_add fail!!!!!!\n");
		unregister_chrdev_region(chardev, 3);
		kfree(chdev);
		return result;
	}
	
	// chr drv end
	sema_init(&semUsb.sem, 0);

	semUsb.iCount = 0;

	memcpy((void*)semUsb.bName, (const void*)name, (size_t)size);
	semUsb.bName[size+1] = '\0';

#ifdef CONFIG_TRIHIDTV_CPU_350M // TRIDENT_1C
	// Front USB Port
	// Set PG43 as GPIO input
	WriteRegHWord((volatile void *)INTCNTR_EXT4, ReadRegHWord((volatile void *)INTCNTR_EXT4) & ~GPIO43_EN ); 
	// Set PG43 as Interrupt enable & falling edge trigger
	WriteRegHWord((volatile void *)INTCOTR_EXT4, (ReadRegHWord((volatile void *)INTCOTR_EXT4) | EXT_INT_3) & ~FALL_TRIG_43 );
	if((ret = request_irq(TRIHIDTV_GPIO_IRQ, &usbfault_irq, IRQF_SHARED, "usb fault", (void *)0x1)) != 0)
		printk("reqeust interrupt %d error \n", TRIHIDTV_GPIO_IRQ);
	
	// Rear USB Port
	// Set PG55 as GPIO input
	WriteRegHWord((volatile void *)INTCNTR_EXT5, ReadRegHWord((volatile void *)INTCNTR_EXT5) & ~GPIO55_EN ); 
	// Set PG55 as Interrupt enable & falling edge trigger
	WriteRegHWord((volatile void *)INTCOTR_EXT5, (ReadRegHWord((volatile void *)INTCOTR_EXT5) | EXT_INT_12) & ~FALL_TRIG_55 );
	if((ret = request_irq(TRIHIDTV_GPIO_IRQ, &usbfault_irq, IRQF_SHARED, "usb fault", (void *)0x2)) != 0)
		printk("reqeust interrupt %d error \n", TRIHIDTV_GPIO_IRQ);
#elif CONFIG_TRIHIDTV_CPU_425M //TRIDENT_3B
	if((ret = request_irq(TRIHIDTV_USB_IRQ, &usbfault_irq, IRQF_SHARED, "usb fault", (void *)0x1)) != 0)
		printk("reqeust interrupt %d error \n", TRIHIDTV_USB_IRQ);
	if((ret = request_irq(TRIHIDTV_USB2_IRQ, &usbfault_irq, IRQF_SHARED, "usb fault", (void *)0x2)) != 0)
		printk("reqeust interrupt %d error \n", TRIHIDTV_USB2_IRQ);
#endif
	printk("registered usb fault driver \n");
	return 0;
}

static void __exit usbfault_exit(void)
{
#ifdef CONFIG_TRIHIDTV_CPU_350M //TRIDENT_1C
	free_irq(TRIHIDTV_GPIO_IRQ, (void *)0x1);
	free_irq(TRIHIDTV_GPIO_IRQ, (void *)0x2);
#elif CONFIG_TRIHIDTV_CPU_425M //TRIDENT_3B
	free_irq(TRIHIDTV_USB_IRQ, (void *)0x1);
	free_irq(TRIHIDTV_USB2_IRQ, (void *)0x2);
#endif
	cdev_del(chdev);
	kfree(chdev);
	unregister_chrdev_region(chardev, 1);
}

module_init(usbfault_init);
module_exit(usbfault_exit);

MODULE_LICENSE("SAMSUNG");
MODULE_AUTHOR("NJ.Jeon");
MODULE_DESCRIPTION("Driver for usb fault");


