/*
 * EHCI HCD (Host Controller Driver) for USB.
 *
 * Alan Liu ported this for HiDTV chip
 * 2007-9-11
 *
 * This file is licenced under the GPL.
 *
 */

#include <linux/platform_device.h>
#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_addrspace.h>

extern int usb_disabled(void);

/*-------------------------------------------------------------------------*/

static void trihidtv_start_ehc(struct platform_device *dev)
{
	unsigned int temp;
	pr_debug(__FILE__ ": starting TriHidtv EHCI USB Controller\n");

	/* enable host controller */
	
#if 0
        /*
	 * we enable 004c here..., it seems this is ok timing.
	 * Alan Liu
	 */
	temp = ReadRegHWord(IO_PTR(0xbb00008e));

	/*
	 * shutdown it now, and we will power it on later
	 * after host controller is ready...
	 */
	if( (temp & 0x8000) == 0x0 ) {
/* SELP.4.2.1.x Add patch No.37, TridentSX3 chip porting, 2009-08-07 */
#ifdef CONFIG_TRIHIDTV_CPU_350M
		temp = ReadRegHWord(TRIHIDTV_SYSCFG_PTR + 0x4c);
		WriteRegHWord(TRIHIDTV_SYSCFG_PTR + 0x4c,temp&~0x0100);
		temp = ReadRegHWord(TRIHIDTV_SYSCFG_PTR + 0x4c);
#elif defined(CONFIG_TRIHIDTV_CPU_425M)
        temp = ReadRegHWord((volatile void *)0x1b00004c);
        WriteRegHWord((volatile void *)0x1b00004c,temp&~0x0100);
        temp = ReadRegHWord((volatile void *)0x1b00004c);
        temp = ReadRegHWord((volatile void *)0x1b000178);
                WriteRegHWord((volatile void *)0x1b000178,temp&~0x40);
                temp = ReadRegHWord((volatile void *)0x1b000178);
#endif
	} else {
#endif
	{
/* SELP.4.2.1.x Add patch No.37, TridentSX3 chip porting, 2009-08-07 */
#ifdef CONFIG_TRIHIDTV_CPU_350M
		temp = ReadRegHWord(TRIHIDTV_SYSCFG_PTR + 0x4c);
		WriteRegHWord(TRIHIDTV_SYSCFG_PTR + 0x4c,temp|0x0100);
		temp = ReadRegHWord(TRIHIDTV_SYSCFG_PTR + 0x4c);
#elif defined(CONFIG_TRIHIDTV_CPU_425M)
        temp = ReadRegHWord((volatile void *)0x1b00004c);
        WriteRegHWord((volatile void *)0x1b00004c,temp|0x0100);
        temp = ReadRegHWord((volatile void *)0x1b00004c);
        temp = ReadRegHWord((volatile void *)0x1b000178);
                WriteRegHWord((volatile void *)0x1b000178,temp|0x40);
                temp = ReadRegHWord((volatile void *)0x1b000178);
#endif
	}
}

static void trihidtv_stop_ehc(struct platform_device *dev)
{
	pr_debug(__FILE__ ": stopping TriHiDTV EHCI USB Controller\n");
}

/*-------------------------------------------------------------------------*/

/* configure so an HC device and id are always provided */
/* always called with process context; sleeping is OK */

/**
 * usb_ehci_trihidtv_probe - initialize TriHiDTV-based HCDs
 * Context: !in_interrupt()
 *
 * Allocates basic resources for this USB host controller, and
 * then invokes the start() method for the HCD associated with it
 * through the hotplug entry's driver_data.
 *
 */
int usb_ehci_trihidtv_probe(const struct hc_driver *driver,
			  struct usb_hcd **hcd_out, struct platform_device *dev)
{
	int retval;
	struct usb_hcd *hcd;
	struct ehci_hcd *ehci;

	trihidtv_start_ehc(dev);

	if (dev->resource[1].flags != IORESOURCE_IRQ) {
		pr_debug("resource[1] is not IORESOURCE_IRQ");
		retval = -ENOMEM;
	}
	hcd = usb_create_hcd(driver, &dev->dev, "trihidtv");
	if (!hcd)
		return -ENOMEM;
	hcd->rsrc_start = dev->resource[0].start;
	hcd->rsrc_len = dev->resource[0].end - dev->resource[0].start + 1;

	if (!request_mem_region(hcd->rsrc_start, hcd->rsrc_len, hcd_name)) {
		pr_debug("request_mem_region failed");
		retval = -EBUSY;
		goto err1;
	}

	hcd->regs = ioremap(hcd->rsrc_start, hcd->rsrc_len);
	if (!hcd->regs) {
		pr_debug("ioremap failed");
		retval = -ENOMEM;
		goto err2;
	}

	ehci = hcd_to_ehci(hcd);
	ehci->caps = hcd->regs;
	ehci->regs = hcd->regs + HC_LENGTH(readl(&ehci->caps->hc_capbase));
	/* cache this readonly data; minimize chip reads */
	ehci->hcs_params = readl(&ehci->caps->hcs_params);
#if 1
	printk("This is the TDI USB 2.0 HC\n");
	ehci->is_tdi_rh_tt = 1;
#endif

	/* ehci_hcd_init(hcd_to_ehci(hcd)); */

	retval =
	    usb_add_hcd(hcd, dev->resource[1].start, IRQF_DISABLED | IRQF_SHARED);
	if (retval == 0)
		return retval;

	trihidtv_stop_ehc(dev);
	iounmap(hcd->regs);
err2:
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
err1:
	usb_put_hcd(hcd);
	return retval;
}

/* may be called without controller electrically present */
/* may be called with controller, bus, and devices active */

/**
 * usb_ehci_hcd_trihidtv_remove - shutdown processing for trihidtv-based HCDs
 * @dev: USB Host Controller being removed
 * Context: !in_interrupt()
 *
 * Reverses the effect of usb_ehci_hcd_trihidtv_probe(), first invoking
 * the HCD's stop() method.  It is always called from a thread
 * context, normally "rmmod", "apmd", or something similar.
 *
 */
void usb_ehci_trihidtv_remove(struct usb_hcd *hcd, struct platform_device *dev)
{
	usb_remove_hcd(hcd);
	iounmap(hcd->regs);
	release_mem_region(hcd->rsrc_start, hcd->rsrc_len);
	usb_put_hcd(hcd);
	trihidtv_stop_ehc(dev);
}

/*-------------------------------------------------------------------------*/

static const struct hc_driver ehci_trihidtv_hc_driver = {
	.description = hcd_name,
	.product_desc = "TriHiDTV EHCI",
	.hcd_priv_size = sizeof(struct ehci_hcd),

	/*
	 * generic hardware linkage
	 */
	.irq = ehci_irq,
	.flags = HCD_MEMORY | HCD_USB2,

	/*
	 * basic lifecycle operations
	 */
	.reset = ehci_init,
	.start = ehci_run,
	.stop = ehci_stop,
	.shutdown = ehci_shutdown,

	/*
	 * managing i/o requests and associated device resources
	 */
	.urb_enqueue = ehci_urb_enqueue,
	.urb_dequeue = ehci_urb_dequeue,
	.endpoint_disable = ehci_endpoint_disable,

	/*
	 * scheduling support
	 */
	.get_frame_number = ehci_get_frame,

	/*
	 * root hub support
	 */
	.hub_status_data = ehci_hub_status_data,
	.hub_control = ehci_hub_control,
#ifdef	CONFIG_PM
	.hub_suspend = ehci_hub_suspend,
	.hub_resume = ehci_hub_resume,
#endif
};

/*-------------------------------------------------------------------------*/

static int ehci_hcd_trihidtv_drv_probe(struct platform_device *pdev)
{
	struct usb_hcd *hcd = NULL;
	int ret;

	pr_debug("In ehci_hcd_trihidtv_drv_probe\n");

	if (usb_disabled())
		return -ENODEV;

	ret = usb_ehci_trihidtv_probe(&ehci_trihidtv_hc_driver, &hcd, pdev);
	return ret;
}

static int ehci_hcd_trihidtv_drv_remove(struct platform_device *pdev)
{
	struct usb_hcd *hcd = platform_get_drvdata(pdev);

	usb_ehci_trihidtv_remove(hcd, pdev);
	return 0;
}

 /*TBD*/
/*static int ehci_hcd_trihidtv_drv_suspend(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct usb_hcd *hcd = dev_get_drvdata(dev);

	return 0;
}
static int ehci_hcd_trihidtv_drv_resume(struct device *dev)
{
	struct platform_device *pdev = to_platform_device(dev);
	struct usb_hcd *hcd = dev_get_drvdata(dev);

	return 0;
}
*/
MODULE_ALIAS("trihidtv-ehci");
static struct platform_driver ehci_hcd_trihidtv_driver = {
	.probe = ehci_hcd_trihidtv_drv_probe,
	.remove = ehci_hcd_trihidtv_drv_remove,
	.shutdown = usb_hcd_platform_shutdown,
	/*.suspend      = ehci_hcd_trihidtv_drv_suspend, */
	/*.resume       = ehci_hcd_trihidtv_drv_resume, */
	.driver = {
		.name = "trihidtv-ehci",
		.bus = &platform_bus_type
	}
};
