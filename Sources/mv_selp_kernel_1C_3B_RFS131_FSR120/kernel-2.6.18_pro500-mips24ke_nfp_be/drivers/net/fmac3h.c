/*
 *  linux/drivers/net/fmac3h.c
 *
 * Copyright (C) 2007-2008 FUJITSU LIMITED
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#define DRV_NAME       "F_MAC3H"
#define DRV_VERSION    "1.0"

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/init.h>
#include <linux/pci.h>
#include <linux/delay.h>
#include <linux/ethtool.h>
#include <linux/compiler.h>
#include <linux/rtnetlink.h>
#include <linux/crc32.h>
#include <linux/mii.h>
#include <linux/platform_device.h>
#include <linux/spinlock.h>
#include <linux/types.h>
#include <linux/skbuff.h>

#include <linux/io.h>
#include <linux/irq.h>
#include <linux/uaccess.h>
#include <asm/unaligned.h>
#include <asm/arch/hardware.h>

#include "fmac3h.h"


/*
 * The definition used in this driver
 */

/* frame MTU size */
#define MTU_SIZE	1500

/* frame MTU (1500) + Ethernet Header(14) + FCS(4) */
#define FRAME_SIZE		(MTU_SIZE + 18)
#define VLAN_FRAME_SIZE		(FRAME_SIZE + 4)

/* maintain frame alignment size multiple of 4 */
#define VLAN_FRAME_SIZE_ALIGN	(VLAN_FRAME_SIZE + 2)


/* Jumbo Frame MTU size */
#define JUMBO_FRAME_MTU  9000

/* Jumbo Frame MTU(9000) + Ethernet Header(14) + FCS(4) */
#define JUMBO_FRAME_SIZE  (JUMBO_FRAME_MTU + 18)
#define VLAN_JUMBO_FRAME_SIZE	(JUMBO_FRAME_SIZE + 4)


/* MAX address of 32 PHYs */
#define MAX_PHY_ADR  31

/* hope the operation will end within this time */
#define PHY_WAIT_TIME 1000		/* Max Time out For Phy Reg Write */
#define ANEG_TIME_OUT	(PHY_WAIT_TIME * 500)/* Auto Negotiation Max timeout */
					     /* Period, each count repressents
						* 2uSec, timeout is = 1sec */

/* check carrier every 2 seconds */
#define CHECK_CARRIER_TIME  (jiffies + (2 * HZ))

/* number of free descriptors */
#define TX_RING_AVAIL(RP) \
	 (((RP)->tx_tail <= (RP)->tx_head) ? \
	 (RP)->tx_tail + (FMAC3H_RDESC_NUM - 1) - (RP)->tx_head : \
	 (RP)->tx_tail - (RP)->tx_head - 1)

#define NEXT_RX(n) ((n) < (FMAC3H_RDESC_NUM - 1) ? ((n) + 1) : 0)
#define NEXT_TX(n) ((n) < (FMAC3H_TDESC_NUM - 1) ? ((n) + 1) : 0)


/* the size of descriptor spaces */
#define FMAC3H_DESC_BYTES (((sizeof(struct fmac3h_desc) * FMAC3H_TDESC_NUM)) + \
			  ((sizeof(struct fmac3h_desc) * FMAC3H_RDESC_NUM)))

/* TX engine error number */
#define FMAC3H_TX_OK    NETDEV_TX_OK	/* driver takes care of packet */
#define FMAC3H_TX_BUSY  NETDEV_TX_BUSY	/* driver tx path was busy */

/* SYS_CLK is based on AHB */
#define SYS_CLK ETH_MHZ

/* suggested PHY type's ID */
#define LXT971A_ID    0x001378E2

/*
 * if you have tested this driver on a new type PHY and make sure the driver
 * supports the new type PHY, you can add the ID(get from register ID1&ID2)
 * to this array.
 */
static u32 known_phy_id[] = {
	LXT971A_ID,

	/* Add the ID above here
	 * 0 should always be the last one of array */
	0
};


/*
 * fmac3h_change_mtu -- change the MTU of system
 * @dev: target net device structure
 * @new_mtu: new mtu value
 *
 * Description: change the MTU of system.
 */
static int fmac3h_change_mtu(struct net_device *dev, int new_mtu)
{
	if (new_mtu < ETH_ZLEN || new_mtu > MTU_SIZE)
		return -EINVAL;

	dev->mtu = new_mtu;

	return 0;
}

/*
 * fmac3h_reg_read -- read FMAC3H's registers
 * @base_addr: the base address of FMAC3H's register space
 * @reg: register offset
 *
 * Description: Function to read value from fmac3h register.
 */
static u32 fmac3h_reg_read(void __iomem *base_addr, u32 reg)
{
	return __raw_readl(base_addr + reg);
}

/*
 * fmac3h_reg_write -- write FMAC3H's registers
 * @base_addr: the base address of FMAC3H's register space
 * @reg: register offset
 * @val: value to write
 *
 * Description: Function to write value on fmac3h register.
 */
static void fmac3h_reg_write(void __iomem *base_addr, u32 reg, u32 val)
{
	__raw_writel(val, base_addr + reg);
}

/*
 * fmac3h_phy_wait -- confirm that PHY is not busy
 * @base_addr: the base address of FMAC3H's registers space.
 *
 * Description: this function will confirm whether PHY is
 * busy or not, return value 0 : Phy is free
 *			     1 : Phy is busy.
 */
static int fmac3h_phy_wait(void __iomem *base_addr)
{
	u32 wtime;		/* waiting time */
	u32 val;

	wtime = PHY_WAIT_TIME;
	while (wtime--) {
		val = __raw_readl(base_addr + FMAC3H_GAR);
		if (!(val & GAR_GB))
			return 0;
		udelay(2);
	}

	PMSG(KERN_INFO, "PHY is busy!\n");

	return 1;
}

/*
 * fmac3h_phy_read -- read PHY's register
 * @dev: target net device structure
 * @phy_id: the id of phy. range:0-31
 * @reg: phy register offset
 * @read_val: read PHY register value
 *
 * Description: read PHY's register by setting FMAC3H GAR register and
 * GDR register.
 *  return : 0 :: Phy read successed.
 *	     1 :: Phy is busy, read failed.
 */
static int
fmac3h_phy_read(struct net_device *dev, u32 phy_id, u32 reg, u16 *read_val)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	void __iomem *base_addr;
	u32 wval;
	int ret;		/* the return value */

	/* Set the GAR register */
	wval = (phy_id << 11) | ((reg & 0x1f) << 6) | GAR_GW_R
	    | ((lp->mdc_clk & 0x07) << 2) | GAR_GB;

#ifndef MB8AA0350_FMAC3H_WORKAROUND
	base_addr = lp->base_addr;
#else
	base_addr = (void __iomem *)IO_ADDRESS(MB8AA0350_CPLD_BASE);
#endif

	spin_lock_irq(&lp->phy_lock);

	/* Wait until GMII/MII gets free */
	ret = fmac3h_phy_wait(base_addr);
	if (ret) {
		spin_unlock_irq(&lp->phy_lock);
		return ret;
	}

	fmac3h_reg_write(base_addr, FMAC3H_GAR, wval);

	/* Wait until GMII/MII gets free */
	ret = fmac3h_phy_wait(base_addr);
	if (ret) {
		spin_unlock_irq(&lp->phy_lock);
		return ret;
	}

	/* truncate the Upper 2 MSB bytes ,as they are reserved */
	*read_val = (u16)fmac3h_reg_read(base_addr, FMAC3H_GDR);

	spin_unlock_irq(&lp->phy_lock);

	return 0;
}

/*
 * fmac3h_phy_write -- write PHY's register
 * @dev: target net device structure
 * @phy_id: the id of phy. range:0-31
 * @reg: phy register offset
 * @val: value to write
 *
 * Description: write PHY's register by setting FMAC3H's GAR register and
 *  GDR register.
 * return : 0 :: Phy write successed.
 *	    1 :: Phy is busy, Write failed.
 */
static int
fmac3h_phy_write(struct net_device *dev, u32 phy_id, u32 reg, u16 val)
{
	u32 wval;
	struct fmac3h_private *lp = netdev_priv(dev);
	void __iomem *base_addr;
	int ret;

	wval = (phy_id << 11) | (reg & 0x1f) << 6 | GAR_GW_W
	    | ((lp->mdc_clk & 0x07) << 2) | GAR_GB;

#ifndef MB8AA0350_FMAC3H_WORKAROUND
	base_addr = lp->base_addr;
#else
	base_addr = (void __iomem *)IO_ADDRESS(MB8AA0350_CPLD_BASE);
#endif

	spin_lock_irq(&lp->phy_lock);

	/* Wait until GMII/MII is not busy */
	ret = fmac3h_phy_wait(base_addr);
	if (ret) {
		spin_unlock_irq(&lp->phy_lock);
		return ret;
	}

	/* write value in PHY register */
	fmac3h_reg_write(base_addr, FMAC3H_GDR, val);

	/* Set PHY Register access information */
	fmac3h_reg_write(base_addr, FMAC3H_GAR, wval);

	/* Wait until GMII/MII is not busy */
	ret = fmac3h_phy_wait(base_addr);
	if (ret) {
		spin_unlock_irq(&lp->phy_lock);
		return ret;
	}

	spin_unlock_irq(&lp->phy_lock);

	return 0;
}

/*
 * fmac3h_mii_read -- ethool use this function to read PHY's registers
 * @dev: target net device structure
 * @phy_id: the id of phy. range:0-31
 * @reg: phy register offset
 *
 * Description: ethool use this function to read PHY's registers.
 */
static int fmac3h_mii_read(struct net_device *dev, int phy_id, int reg)
{
	u16 val;
	int ret;		/* the return value */

	ret = fmac3h_phy_read(dev, phy_id, reg, &val);
	if (ret)
		PMSG(KERN_WARNING, "MII Read Error!!!\n");

	return val;
}

/*
 * fmac3h_mii_write -- ethool use this function to write PHY's registers
 * @dev: target net device structure
 * @phy_id: the id of phy. range:0-31
 * @reg: phy register offset
 * @val: value to write
 *
 * Description: ethool use this function to write PHY's registers.
 */
static void
fmac3h_mii_write(struct net_device *dev, int phy_id, int reg, int val)
{
	int ret;

	ret = fmac3h_phy_write(dev, phy_id, reg, val);
	if (ret)
		PMSG(KERN_WARNING, "MII Write Error!!!\n");
}

/*
 * fmac3h_stop_rxtx -- stop rx and tx engine
 * @lp: taget F_MAC3H device structure
 *
 * Description: this function will stop RX/TX DMA by setting
 *  OMR register.
 */
static void fmac3h_stop_rxtx(struct fmac3h_private *lp)
{
	u32 val;
	u32 wtime;		/* waiting time */
	u32 i;

	val = fmac3h_reg_read(lp->base_addr, FMAC3H_OMR);

	/* Stop RX:set 0 to OMR[bit1]; Stop TX:set 0 to OMR[bit13] */
	val &= ~(START_TX | START_RX);

	/* Send stop command to TX and RX process */
	fmac3h_reg_write(lp->base_addr, FMAC3H_OMR, val);

	/* TX/RX process will not stop immediately with stop command, until
	 * they finish the work which has been started.
	 * wait untill phy_wait time */
	wtime = PHY_WAIT_TIME;
	while (wtime--) {
		val = fmac3h_reg_read(lp->base_addr, FMAC3H_OMR);
		if (!(val & (START_TX | START_RX))) {
			DBG_PRINT("TX/RX Stoped.\n");
			goto drop_packet;
		}
		udelay(2);
	}

drop_packet:
	/* check for those packets, that has not been
	 * Txmitted/Received will be dropped */
	for (i = 0; i < FMAC3H_TDESC_NUM; i++) {
		if (lp->tx_ring[i].opts1 & OWN_BIT) {
			PMSG(KERN_DEBUG,
			     "The packet of TX descirptor(%d) will be \
				dropped!!!\n",
			     i);
			lp->net_stats.tx_dropped++;
		}
	}

	for (i = 0; i < FMAC3H_RDESC_NUM; i++) {
		if (!(lp->rx_ring[i].opts1 & OWN_BIT)) {
			PMSG(KERN_DEBUG,
			     "The packet of RX descirptor(%d) will be \
				dropped!!!\n",
			     i);
			lp->net_stats.rx_dropped++;
		}
	}

}

/*
 * fmac3h_get_stats_from_mfc -- get RX status by reading MFC register
 * @lp: taget F_MAC3H device structure
 *
 * Description: this function will read MFC register to fill
 *  net_stats structure.
 */
static void fmac3h_get_stats_from_mfc(struct fmac3h_private *lp)
{
	u32 val;

	val = fmac3h_reg_read(lp->base_addr, FMAC3H_MFC);
	/*
	 *  Miss error = Number of missed frame by FMAC3H (MFC[bit27:17])
	 *             + Number of missed frame by HOST (MFC[bit15:0])
	 */
	lp->net_stats.rx_missed_errors += ((val & 0x0000FFFF)	/* NMFF */
					   +((val >> 17) & 0x000003FF));

}

/*
 * fmac3h_setup_aneg -- setup autonegotiation
 * @lp: taget F_MAC3H device structure
 *
 * Description: In this function, the driver will check whether
 *  auto-negotiation is permitted before setup auto-negotiation.
 */
static int fmac3h_setup_aneg(struct fmac3h_private *lp)
{
	struct mii_if_info *mii;
	u16 adv, ctl;
	u32 wtime;
	int ret;

	mii = &lp->mii;

	if (mii->force_media) {
		PMSG(KERN_WARNING, "Not allow for autonegotiation!!!\n");
		return -EACCES;
	}

	/* reset phy */
	ret = fmac3h_phy_read(lp->dev, mii->phy_id, MII_BMCR, &ctl);
	if (ret) {
		PMSG(KERN_WARNING, "Failed to read PHY register!!!\n");
		return -EACCES;
	}

	ctl |= BMCR_RESET;
	ret = fmac3h_phy_write(lp->dev, mii->phy_id, MII_BMCR, ctl);
	if (ret) {
		PMSG(KERN_WARNING, "Failed to write PHY register!!!\n");
		return -EACCES;
	}

	/* to sure reset complete in time */
	wtime = PHY_WAIT_TIME;
	while (wtime--) {
		ret = fmac3h_phy_read(lp->dev, mii->phy_id, MII_BMCR, &ctl);
		if (ret) {
			PMSG(KERN_WARNING, "Failed to read PHY register!!!\n");
			return -EACCES;
		}

		if (ctl & BMCR_RESET) {
			wtime = 0;
			break;
		}
		udelay(2);
	}

	if (!(ctl & BMCR_RESET)) {
		PMSG(KERN_ERR, "Failed to reset BMCR !!!\n");
		return -EBUSY;
	}

	/* setup standard advertise */
	ret = fmac3h_phy_read(lp->dev, mii->phy_id, MII_ADVERTISE, &adv);
	if (ret) {
		PMSG(KERN_WARNING, "Failed to read PHY register!!!\n");
		return -EACCES;
	}

	adv &= ~(ADVERTISE_ALL | ADVERTISE_100BASE4
		 | ADVERTISE_PAUSE_CAP | ADVERTISE_PAUSE_ASYM);
	if (mii->advertising & ADVERTISED_10baseT_Half)
		adv |= ADVERTISE_10HALF;
	if (mii->advertising & ADVERTISED_10baseT_Full)
		adv |= ADVERTISE_10FULL;
	if (mii->advertising & ADVERTISED_100baseT_Half)
		adv |= ADVERTISE_100HALF;
	if (mii->advertising & ADVERTISED_100baseT_Full)
		adv |= ADVERTISE_100FULL;
	if (mii->advertising & ADVERTISED_Pause)
		adv |= ADVERTISE_PAUSE_CAP;
	if (mii->advertising & ADVERTISED_Asym_Pause)
		adv |= ADVERTISE_PAUSE_ASYM;
	ret = fmac3h_phy_write(lp->dev, mii->phy_id, MII_ADVERTISE, adv);
	if (ret) {
		PMSG(KERN_WARNING, "Failed to write PHY register!!!\n");
		return -EACCES;
	}

	if (mii->supports_gmii) {
		PMSG(KERN_INFO, "GMII Port\n");
		ret = fmac3h_phy_read(lp->dev, mii->phy_id, MII_CTRL1000, &adv);
		if (ret) {
			PMSG(KERN_WARNING, "Failed to read PHY register!!!\n");
			return -EACCES;
		}

		adv &= ~(ADVERTISE_1000FULL | ADVERTISE_1000HALF);
		if (mii->advertising & ADVERTISED_1000baseT_Full)
			adv |= ADVERTISE_1000FULL;
		if (mii->advertising & ADVERTISED_1000baseT_Half)
			adv |= ADVERTISE_1000HALF;
		ret = fmac3h_phy_write(lp->dev, mii->phy_id, MII_CTRL1000, adv);
		if (ret) {
			PMSG(KERN_WARNING, "Failed to write PHY register!!!\n");
			return -EACCES;
		}
	}

	/* start auto negotiation */
	PMSG(KERN_INFO, "Start autonegotiation\n");
	ret = fmac3h_phy_read(lp->dev, mii->phy_id, MII_BMCR, &ctl);
	if (ret) {
		PMSG(KERN_WARNING, "Failed to read PHY register!!!\n");
		return -EACCES;
	}

	ctl |= BMCR_ANENABLE | BMCR_ANRESTART;
	ret = fmac3h_phy_write(lp->dev, mii->phy_id, MII_BMCR, ctl);
	if (ret) {
		PMSG(KERN_WARNING, "Failed to write PHY register!!!\n");
		return -EACCES;
	}

	/* sure autonegotiation success */
	wtime = ANEG_TIME_OUT;
	while (wtime--) {
		ret = fmac3h_phy_read(lp->dev, mii->phy_id, MII_BMSR, &ctl);
		if (ret) {
			PMSG(KERN_WARNING, "Failed to read PHY register!!!\n");
			return -EACCES;
		}

		if ((ctl & BMSR_LSTATUS) && (ctl & BMSR_ANEGCOMPLETE)) {
			wtime = 0;
			return 0;
		}
		udelay(2);
	}

	if (!((ctl & BMSR_LSTATUS) && (ctl & BMSR_ANEGCOMPLETE))) {
		PMSG(KERN_ERR, "Setup Autonegotiation is failed!!!\n");
		return -EBUSY;
	}

	return 0;
}

/*
 * fmac3h_init_hw -- initialise the registers of F_MAC3H
 * @lp: taget F_MAC3H device structure
 *
 * Description: this function will setup
 * autonegotiation and Initialise registers of FMAC3H.
 */
static int fmac3h_init_hw(struct fmac3h_private *lp)
{
	u32 val;
	u16 lpa, advert;
	int err;
	dma_addr_t rx_adr, tx_adr;

	/* Set BMR(MDC Bus Mode Register) */
	val = (BMR_4XPBL & SET_1)	/* MAX burst is 4times of PBL */
	    |(BMR_UPS & SET_1)	/* RXburst is RPBL, TXburst is PBL */
	    |(BMR_FB & SET_1)	/* AHB Master Burst:Single, INCR   */
	    |(BMR_BLE & SET_0)	/* F_MAC3H->HOST Little Endian     */
	    |(BMR_DA & SET_1)	/* DMA Arbitration Scheme:RX First */
	    | BMR_RPBL_32	/* RX Burst Length is 32 Bytest    */
	    | BMR_PBL_32	/* TX Burst Length is 32 Bytes     */
	    | BMR_DSL;		/* Descripter Skip Length is 0     */

	fmac3h_reg_write(lp->base_addr, FMAC3H_BMR, val);

	/* Set IER(MDC Interrupt Enable Register) */
	val = (INT_EARLY_RX & INT_MASK)	/* Early Receive Int Disable        */
	    |(INT_BUS_ERR & INT_ENABLE)	/* Fatal Bus Error Int Enable       */
	    |(INT_EARLY_TX & INT_MASK)	/* Early Transmit Int Disable       */
	    |(INT_RX_WT & INT_MASK)	/* RX Watchdog Timeout Int Disable  */
	    |(INT_RX_STOP & INT_ENABLE)	/* Receive Process Stop Int Enable  */
	    |(INT_RXB_UNAV & INT_ENABLE)/* RX Buffer Unavailable Int Enable */
	    |(INT_RX_INT & INT_ENABLE)	/* Receive Int Enable               */
	    |(INT_TX_UFLOW & INT_ENABLE)/* Transmit Underflow Int Enable    */
	    |(INT_RX_OVERF & INT_ENABLE)/* Receive Overlow Int Enable       */
	    |(INT_TX_JT & INT_MASK)	/* TX Jabber Timeout Int Disable    */
	    |(INT_TXB_UNAV & INT_ENABLE)/* TX Buffer Unavailable Int Enable */
	    |(INT_TX_STOP & INT_ENABLE)	/* Transmit Process Stop Int Enable */
	    |(INT_TX_INT & INT_ENABLE)	/* Transmit Int Enable              */
	    |(INT_NORMAL & INT_ENABLE)	/* Normal Interrupt Summary Enable  */
	    |(INT_ABNORMAL & INT_ENABLE);/* Abnormal Interrupt Summary      */

	fmac3h_reg_write(lp->base_addr, FMAC3H_IER, val);

	/*
	 * Set PMTR(PMT Register)
	 * Disable Wake Up Frame | Disable Magic Packet
	 */
	val = (PMTR_WE & INT_MASK)	/* Wake Up Frame Disable */
	    |(PMTR_ME & INT_ENABLE);	/* Magic Packet Disable  */

	fmac3h_reg_write(lp->base_addr, FMAC3H_PMTR, val);
	/* Mask MMC interrupt */
	fmac3h_reg_write(lp->base_addr, FMAC3H_MMC_INTR_MASK_RX, MMC_INTR_MASK);
	fmac3h_reg_write(lp->base_addr, FMAC3H_MMC_INTR_MASK_TX, MMC_INTR_MASK);

	/*
	 * Set MFFR(MAC Frame Filter Register)
	 *   With this set the LAN can do:
	 *   (1).Receive broadcast frames
	 *   (2).Receive packets that has the same multicast address as MARn
	 *   (3).Receive packets that has the same unicast address as MARn
	 */
	val = REC_ALL_ON	/* Receive all frame        */
	    | PROMIS_MODE;	/* Promiscuous Mode         */

	fmac3h_reg_write(lp->base_addr, FMAC3H_MFFR, val);

	/*
	 * Set MHTRH(MAC Hash Table Register High)
	 *       MHTRL(MAC Hash Table Register Low)
	 */
	fmac3h_reg_write(lp->base_addr, FMAC3H_MHTRH, 0);
	fmac3h_reg_write(lp->base_addr, FMAC3H_MHTRL, 0);

	/* Set MMC Register */
	fmac3h_reg_write(lp->base_addr, FMAC3H_MMC_CNTL, CLEAR_ALL);
	val = RESET_ON_READ	/* Auto clear after is read */
	    | STOP_ROLL_OVER;	/* Stop count when reached MAX */

	fmac3h_reg_write(lp->base_addr, FMAC3H_MMC_CNTL, val);
	/* Mask all interrupt of MMC */
	fmac3h_reg_write(lp->base_addr, FMAC3H_MMC_INTR_MASK_RX, 0x00FFFFFF);
	fmac3h_reg_write(lp->base_addr, FMAC3H_MMC_INTR_MASK_TX, 0x01FFFFFF);

	/* Setup PHY Autonegotiation */
	err = fmac3h_setup_aneg(lp);
	if (err)
		PMSG(KERN_ERR, "Failed to setup AutoNegotiation!!!\n");

	/* Set MCR(MAC Configuration Register) */
	val = (MCR_WD & SET_1)	/* Disable RX Watchdog timeout     */
	    |(MCR_JD & SET_1)	/* Disable TX Jabber timer         */
	    |(MCR_BE & SET_1)	/* Frame Burst Enable              */
	    |(MCR_JE & SET_0)	/* Disable Jumbo Frame             */
	    |(MCR_DCRS & SET_0)	/* Enable Carrier During Trans     */
	    |(MCR_DO & SET_0)	/* Enable Receive Own              */
	    |(MCR_LM & SET_0)	/* Not Loop-back Mode              */
	    |(MCR_DR & SET_0)	/* Enable Retry                    */
	    |(MCR_ACS & SET_0)	/* Not Automatic Pad/CRC Stripping */
	    |(MCR_DC & SET_0)	/* Deferral Check                  */
	    |(MCR_TX_ENABLE & SET_1)	/* Enable Transmitter      */
	    |(MCR_RX_ENABLE & SET_1)	/* Enable Receiver         */
	    | MCR_BL_00;		/* Back-off Limit is set 0 */

	/* MCR[bit15,11] is set based on PHY`s setting */
	if (!lp->mii.supports_gmii)	/* MII Port */
		val |= MII_PORT;

	err = fmac3h_phy_read(lp->dev, lp->mii.phy_id, MII_LPA, &lpa);
	if (err) {
		PMSG(KERN_WARNING, "Failed to read PHY register!!!\n");
		return err;
	}
	err = fmac3h_phy_read(lp->dev, lp->mii.phy_id, MII_ADVERTISE, &advert);
	if (err) {
		PMSG(KERN_WARNING, "Failed to read PHY register!!!\n");
		return err;
	}

	/* Full Duplex */
	if ((lpa & advert) & (LPA_10FULL | LPA_100FULL))
		val |= FULL_DUPLEX;

	fmac3h_reg_write(lp->base_addr, FMAC3H_MCR, val);

	/* add wait time after setting MCR over 1.6us */
	udelay(10);		/* wait 10us */

	/*
	 * CAUTION!
	 * For F_MAC3H V3.0, RDLAR/TDLAR must be set after setting the MCR.
	 */

	/*
	 * 8.Set RDLAR(MDC Receive Descriptor List Address Register)
	 *       TDLAR(MDC Transmit Descriptor List Address Register)
	 */
	rx_adr = lp->ring_dma;
	tx_adr = lp->ring_dma + (sizeof(struct fmac3h_desc) * FMAC3H_RDESC_NUM);
	fmac3h_reg_write(lp->base_addr, FMAC3H_RDLAR, rx_adr);
	fmac3h_reg_write(lp->base_addr, FMAC3H_TDLAR, tx_adr);

	/* 9.Set OMR to start RX and TX */
	val =			/* OMR_SF */
	    OMR_TTC_64B		/* TX after 64byte written in FIFO
				 * OR In addition, full frames with a
				 * length less than the threshold are
				 * also transmitted */
	    | START_RX;		/* Start Receive     */

	fmac3h_reg_write(lp->base_addr, FMAC3H_OMR, val);

	return 0;
}

/*
 * fmac3h_clean_ring -- free the skbuff of every descriptor
 * @lp: taget F_MAC3H device structure
 *
 * Description: Free the skbuff which has not been freed and clean
 *  up the all descriptors.
 */
static void fmac3h_clean_ring(struct fmac3h_private *lp)
{
	int i;

	memset(lp->tx_ring, 0, sizeof(struct fmac3h_desc) * FMAC3H_TDESC_NUM);
	lp->tx_ring[FMAC3H_TDESC_NUM - 1].opts2 = END_RING;
	/* Flush Tx descriptor ring and Make sure: TER bit (TDES1:: bit[25])
	 * gets set, i.e indicates that descriptor list reached to its
	 * final descriptor */
	wmb();

	memset(lp->rx_ring, 0, sizeof(struct fmac3h_desc) * FMAC3H_RDESC_NUM);
	lp->rx_ring[FMAC3H_RDESC_NUM - 1].opts2 = END_RING;
	/* Flush Rx descriptor ring and Make sure: RER bit (RDES1:: bit[25])
	 * gets set, i.e indicates that descriptor list reached to its
	 * final descriptor */
	wmb();

	/* free the receive skb's */
	for (i = 0; i < FMAC3H_RDESC_NUM; i++) {
		if (lp->rx_skb[i].skb) {
			DBG_PRINT("SKB(RX DESC:%d) free\n", i);
			/* delete the mapping of skbuff */
			dma_unmap_single(NULL, lp->rx_skb[i].mapping,
					 lp->rx_buf_sz, DMA_FROM_DEVICE);
			/* free buffer space */
			dev_kfree_skb_any(lp->rx_skb[i].skb);
			lp->rx_skb[i].skb = NULL;
			lp->rx_skb[i].mapping = 0;
		}
	}

#ifdef MULTIPLE_DESC_HANDLING
	/* free the transmit skb's */
	for (i = 0; i < FMAC3H_TDESC_NUM; i++) {

		if (lp->tx_skb[i].mapping) {

			/* delete the mapping of skbuff */
			dma_unmap_single(NULL, lp->tx_skb[i].mapping,
				lp->tx_skb[i].tx_length, DMA_TO_DEVICE);

			lp->tx_skb[i].mapping = 0;
		}

		if (lp->tx_skb[i].skb) {

			/* free buffer space */
			DBG_PRINT("SKB(TX DESC:%d) free\n", i);
			dev_kfree_skb_any(lp->tx_skb[i].skb);
			lp->tx_skb[i].skb = NULL;
		}
	}
#else

	/* free the transmit skb's */
	for (i = 0; i < FMAC3H_TDESC_NUM; i++) {

		if (lp->tx_skb[i].skb) {

			/* delete the mapping with skbuffer */
			dma_unmap_single(NULL, lp->tx_skb[i].mapping,
				 lp->tx_skb[i].skb->len, DMA_TO_DEVICE);

			lp->tx_skb[i].mapping = 0;

			/* free buffer space */
			DBG_PRINT("SKB(TX DESC:%d) free\n", i);
			dev_kfree_skb_any(lp->tx_skb[i].skb);
			lp->tx_skb[i].skb = NULL;
		}
	}
#endif

}

/*
 * fmac3h_free_ring -- unmap and free descirptor and buffer spaces
 * @lp: target F_MAC3H device structure
 *
 * Description: Check every descriptor's skbuff pointer. If the skbuff has
 *  not been freed then free it. At last free all of descriptor spaces
 */
static void fmac3h_free_ring(struct fmac3h_private *lp)
{
	/* free allocated buffer and clean descriptor */
	fmac3h_clean_ring(lp);

	/* delete the head and tail count of descriptor ring */
	lp->tx_head = lp->tx_tail = 0;
	lp->rx_tail = 0;

	/* free descriptor space */
	dma_free_coherent(NULL, FMAC3H_DESC_BYTES,
			  lp->rx_ring, lp->ring_dma);
	lp->rx_ring = NULL;
	lp->tx_ring = NULL;
	lp->ring_dma = 0;
}

/*
 * fmac3h_refill_rx -- refill rx descriptors
 * @lp: target F_MAC3H device structure
 *
 * Description: this function will allocate a skbuffer for
 *  every rx descriptor and set's OWN bit.
 *  return : 0 : success else error.
 */
static int fmac3h_refill_rx(struct fmac3h_private *lp)
{
	int i, err;

	/* alloc a receive buffer for every receive descriptor */
	for (i = 0; i < FMAC3H_RDESC_NUM; i++) {
		struct sk_buff *skb;

		/* alloc receive buffer */
		skb = dev_alloc_skb(lp->rx_buf_sz);
		if (!skb) {
			err = -ENOMEM;
			goto err_out;
		}

		skb->dev = lp->dev;

		/* set the alloced skb in receive skb array */
		lp->rx_skb[i].mapping = dma_map_single(NULL, skb->data,
						       lp->rx_buf_sz,
						       DMA_FROM_DEVICE);
		lp->rx_skb[i].skb = skb;

		/* set receive descriptor */
		if (i == (FMAC3H_RDESC_NUM - 1)) /* last one in ring */
			lp->rx_ring[i].opts2 = (END_RING | lp->rx_buf_sz);
		else
			lp->rx_ring[i].opts2 = lp->rx_buf_sz;

		lp->rx_ring[i].addr1 = lp->rx_skb[i].mapping;
		lp->rx_ring[i].addr2 = 0;
		lp->rx_ring[i].opts1 = OWN_BIT;	/* set OWN bit */

	}

	return 0;

err_out:
	fmac3h_clean_ring(lp);
	return err;
}

/*
 * fmac3h_init_rings -- initialise TX and RX ring
 * @lp: target F_MAC3H device structure
 *
 * Description: this function will initialise the head and
 * tail count of TX/RX rings and refill rx descriptors.
 * return : 0 : success else error.
 */
static int fmac3h_init_rings(struct fmac3h_private *lp)
{
	lp->rx_tail = 0;
	lp->tx_head = lp->tx_tail = 0;

	return fmac3h_refill_rx(lp);
}

/*
 * fmac3h_ioctl -- performs interface-specific ioctl commands
 * @dev: target net device structure
 * @rq: the rq pointer points to a kernel-space address that holds a copy
 *      of the structure passed by the user.
 * @cmd: ioctl command
 *
 * Description: In FMAC3H driver, three commands are supported. SIOCGMIIPHY,
 *  SIOCGMIIREG and SIOCSMIIREG.
 */
static int fmac3h_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	int ret = 0;

	switch (cmd) {
	case SIOCGMIIPHY:
	case SIOCGMIIREG:
	case SIOCSMIIREG:
		ret = generic_mii_ioctl(&lp->mii, if_mii(rq), cmd, NULL);
		break;
	default:
		PMSG(KERN_ERR, "command not supported(0x%04x)!!!\n", cmd);
		ret = -EOPNOTSUPP;
		break;
	}

	return ret;
}

/*
 * fmac3h_get_drvinfo -- report driver information
 * @dev: target net device structure
 * @info: ethtool driver information structure
 *
 * Description: set driver information of structure ethtool_drvinfo.
 */
static void
fmac3h_get_drvinfo(struct net_device *dev, struct ethtool_drvinfo *info)
{
	strlcpy(info->driver, DRV_NAME, sizeof(info->driver));
	strlcpy(info->version, DRV_VERSION, sizeof(info->version));
	strlcpy(info->bus_info, dev->class_dev.dev->parent->bus_id,
		sizeof(info->bus_info));
}

/*
 * fmac3h_get_settings -- get device-specific settings
 * @dev: target net device structure
 * @cmd: ethtool command
 *
 * Description: fmac3h_get_settings is passed an &ethtool_cmd to fill in.
 *  It returns an negative errno or zero.
 */
static int fmac3h_get_settings(struct net_device *dev, struct ethtool_cmd *cmd)
{
	struct fmac3h_private *lp = netdev_priv(dev);

	return mii_ethtool_gset(&lp->mii, cmd);
}

/*
 * fmac3h_set_settings -- Set device-specific settings
 * @dev: target net device structure
 * @cmd: ethtool command
 *
 * Description: fmac3h_set_settings is passed an &ethtool_cmd and should
 *  attempt to set all the settings this device supports. It returns an
 *  error value if something goes wrong (otherwise 0).
 */
static int fmac3h_set_settings(struct net_device *dev, struct ethtool_cmd *cmd)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	u16 lpa, advert;
	u32 val;
	u32 ctl;
	u32 wtime;
	int ret;

	ret = mii_ethtool_sset(&lp->mii, cmd);
	/* MCR[bit15,11] is setted based on PHY`s setting */
	if (!ret) {
		val = fmac3h_reg_read(lp->base_addr, FMAC3H_MCR);
		if (lp->mii.supports_gmii)	/* GMII Port */
			/* MCR[bit15]: 0 = GMII Port; 1 = MII Port */
			val &= ~MII_PORT;
		else	/* MII Port */
			val |= MII_PORT;


		if (!lp->mii.force_media) {
			/* sure autonegotiation success */
			wtime = ANEG_TIME_OUT;
			while (wtime--) {
				ctl = lp->mii.mdio_read(dev,
							lp->mii.phy_id,
							MII_BMSR);

				if ((ctl & BMSR_LSTATUS)
				    && (ctl & BMSR_ANEGCOMPLETE)) {
					wtime = 0;
					break;
				}
				udelay(2);
			}

			if (!((ctl & BMSR_LSTATUS)
			      && (ctl & BMSR_ANEGCOMPLETE))) {
				PMSG(KERN_ERR,
				     "Setup Autonegotiation failed!!!\n");
				return -EBUSY;
			}
		}

		advert = lp->mii.mdio_read(dev, lp->mii.phy_id, MII_ADVERTISE);
		lpa = lp->mii.mdio_read(dev, lp->mii.phy_id, MII_LPA);
		/* Full Duplex */
		if ((lpa & advert) & (LPA_10FULL | LPA_100FULL))
			val |= FULL_DUPLEX;
		else	/* Half Duplex */
			val &= ~FULL_DUPLEX;

		fmac3h_reg_write(lp->base_addr, FMAC3H_MCR, val);
	} else
		PMSG(KERN_ERR, "Failed to set device!!!");

	return ret;
}

/*
 * fmac3h_nway_reset -- restart autonegotiation
 * @dev: target net device structure
 *
 * Description: this function will restart autonegotiation
 *  and set the result of autonegotiation to MCR registers.
 */
static int fmac3h_nway_reset(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	u32 val, lpa, advert;
	u32 ctl;
	u32 wtime;
	int ret;

	ret = mii_nway_restart(&lp->mii);
	/* MCR[bit15,11] is seted based on PHY`s setting */
	if (!ret) {
		val = fmac3h_reg_read(lp->base_addr, FMAC3H_MCR);
		if (lp->mii.supports_gmii)	/* GMII Port */
			/* MCR[bit15]: 0 = GMII Port; 1 = MII Port */
			val &= ~MII_PORT;
		else	/* MII Port */
			val |= MII_PORT;


		if (!lp->mii.force_media) {
			/* sure autonegotiation success */
			wtime = ANEG_TIME_OUT;
			while (wtime--) {
				ctl = lp->mii.mdio_read(dev,
							lp->mii.phy_id,
							MII_BMSR);

				if ((ctl & BMSR_LSTATUS)
				    && (ctl & BMSR_ANEGCOMPLETE)) {
					wtime = 0;
					break;
				}
				udelay(2);
			}

			if (!((ctl & BMSR_LSTATUS)
			      && (ctl & BMSR_ANEGCOMPLETE))) {
				PMSG(KERN_ERR,
				     "Setup Autonegotiation failed!!!\n");
				return -EBUSY;
			}
		}

		advert = lp->mii.mdio_read(dev, lp->mii.phy_id, MII_ADVERTISE);
		lpa = lp->mii.mdio_read(dev, lp->mii.phy_id, MII_LPA);
		/* Full Duplex */
		if ((lpa & advert) & (LPA_10FULL | LPA_100FULL))
			val |= FULL_DUPLEX;
		else /* Half Duplex */
			val &= ~FULL_DUPLEX;


		fmac3h_reg_write(lp->base_addr, FMAC3H_MCR, val);
	} else
		PMSG(KERN_ERR, "Failed to restart device!!!");


	return ret;
}

/* Function to update 15-MAC registers used for perfect filtering,
 * depending on mc_count val. */
static void build_perfect_filter(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	u32 mac_addrl, mac_addrh;
	u32 i, tmp;

	struct dev_mc_list *cur;
	cur = dev->mc_list;

	/* use the alternate mac address registers for the
	 * first 15 multicast addresses
	 */
	for (i = 0, tmp = 0; i < 15; i++) {

		/* Flush MAC register if mc_list is empty */
		if (!cur) {
			fmac3h_reg_write(lp->base_addr,
						(FMAC3H_MAR1H + tmp), 0x0);
			fmac3h_reg_write(lp->base_addr,
						(FMAC3H_MAR1L + tmp), 0x0);
			continue;
		}

		/* Prepare the MAC address from dmi_addr.
		 * (Note: Validate that cur->dmi_addr[0] starts with
		 * MAC LSB Byte and cur->dmi_addr[5] has MSB Byte,
		 * then only below is valid calc) */
		mac_addrh = ((u32)cur->dmi_addr[5] << 8)
				| (u32)cur->dmi_addr[4];
		mac_addrl = ((u32)cur->dmi_addr[3] << 24)
				| ((u32)cur->dmi_addr[2] << 16)
				| ((u32)cur->dmi_addr[1] << 8)
				| (u32)cur->dmi_addr[0];

		/* Load the mac addr in respective
		 * MARnL & MARnH register */
		fmac3h_reg_write(lp->base_addr,
				(FMAC3H_MAR1H + tmp), mac_addrh);
		fmac3h_reg_write(lp->base_addr,
				(FMAC3H_MAR1L + tmp), mac_addrl);

		/* move to read next mulicast dmi_address */
		cur = cur->next;
		tmp += 8;

	}

}

/* Function to update Hash table register ie, (MHTRH & MHTRL)
 * (Table size : 64-bit) */
static void build_hash_filter(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	u32 mc_filter[2];       /* Multicast hash filter */
	u32 i, bitnr;
	u32 index;
	struct dev_mc_list *curr;

	curr = dev->mc_list;
	for (i = 0; i < dev->mc_count; i++, curr = curr->next) {
		if (!curr) {
			mc_filter[0] = 0;
			mc_filter[1] = 0;
			break;  /* unexpected end of list */
		}

		index = ether_crc_le(ETH_ALEN, curr->dmi_addr);
		bitnr = index & 0x3f;
		mc_filter[(bitnr >> 5)] |= 1 << (bitnr & 31);
	}

	fmac3h_reg_write(lp->base_addr, FMAC3H_MHTRH,  mc_filter[0]);
	fmac3h_reg_write(lp->base_addr, FMAC3H_MHTRL,  mc_filter[1]);
}


/*
 * Enable/Disable promiscuous and multicast modes.
 * Update the hash table/MAC register based on the current
 * list of multicast addresses we subscribe to.
 * Also, change the promiscuity of
 * the device based on the flags (this function is called
 * whenever dev->flags is changed)
 */
static void fmac3h_set_rx_mode(struct net_device *dev)
{
	u32 cfg;
	struct fmac3h_private *lp = netdev_priv(dev);

	cfg = fmac3h_reg_read(lp->base_addr, FMAC3H_MFFR);

	if (dev->flags & IFF_PROMISC) {		/* Enable promiscuous mode */
		cfg |= (PROMIS_MODE | REC_ALL_ON);
	} else if (dev->flags & (~IFF_PROMISC))	/* Disable promiscuous mode */
		cfg &= ~(PROMIS_MODE);

	if (dev->flags & IFF_ALLMULTI) {	/* Enable all multicast mode */

		cfg |= PASS_ALL_MUL;
		fmac3h_reg_write(lp->base_addr, FMAC3H_MHTRH,  0xffffffff);
		fmac3h_reg_write(lp->base_addr, FMAC3H_MHTRL,  0xffffffff);

	} else if (dev->mc_count > 0) {		/* Enable specific multicasts */

		/* if mc_count less than 15 then look
		 * for perfect filter condition */
		if (dev->mc_count <= 14) {

			build_perfect_filter(dev);

			/* set the hash pass filter bit(HPF) and reset the
			 * HMC bit for "perfect filter operation"
			 * in mulicast mode */
			cfg |= HPF_SET_BIT;
			cfg &= ~(HMC_SET_BIT);

		} else { /* process for hash filter condition */

			build_hash_filter(dev);
			/* set the hash pass filter bit(HPF) and set the
			 * HMC bit for "Hash filter opration"
			 * in multicast mode */
			cfg |= (HPF_SET_BIT | HMC_SET_BIT);

		}
		/* Disable all multicast mode */
	} else if (dev->flags & (~IFF_ALLMULTI)) {
		cfg &= ~PASS_ALL_MUL;
		fmac3h_reg_write(lp->base_addr, FMAC3H_MHTRH,  0x0);
		fmac3h_reg_write(lp->base_addr, FMAC3H_MHTRL,  0x0);
	}
	/* update MFFR with the current changes */
	fmac3h_reg_write(lp->base_addr, FMAC3H_MFFR, cfg);
}


/* Ethtool supported by F_MAC3H */
static const struct ethtool_ops fmac3h_ethtool_ops = {
	.get_link = ethtool_op_get_link,
	.get_drvinfo = fmac3h_get_drvinfo,
	.get_settings = fmac3h_get_settings,
	.set_settings = fmac3h_set_settings,
	.nway_reset = fmac3h_nway_reset,
};


/*
 * fmac3h_check_carrier -- check the status of carrier
 * @data: target net device structure
 *
 * Description: this function will be called every one two to check
 *  the status of carrier.
 */
static void fmac3h_check_carrier(unsigned long data)
{
	struct net_device *dev = (struct net_device *)data;
	struct fmac3h_private *lp = netdev_priv(dev);
	u32 oldlink, newlink;

	/* check the old link state of carrier
	 * the return value of netif_carrier_ok()
	 *  1: carrier on
	 *  0: carrier off */
	oldlink = netif_carrier_ok(dev);

	/* check the new link state of carrier
	 * the return value of mii_link_ok()
	 *  1: carrier on
	 *  0: carrier off */
	newlink = (unsigned int)mii_link_ok(&lp->mii);

	/* update link state in dev->state */
	if (!oldlink && newlink) {
		/* Link OFF->ON */
		netif_carrier_on(dev);
		if (netif_queue_stopped(dev) && (TX_RING_AVAIL(lp) > 0))
			netif_wake_queue(dev);

		PMSG(KERN_INFO, "Link UP\n");
	} else if (oldlink && !newlink) {
		/* Link ON->OFF */
		netif_carrier_off(dev);
		netif_stop_queue(dev);

		PMSG(KERN_INFO, "Link DOWN\n");
	}

	/* set the next check */
	mod_timer(&lp->timer, CHECK_CARRIER_TIME);
}


/*
 * fmac3h_tx -- Handle for fmac3h Txmit Interrupts.
 * @dev: target net device structure
 *
 * Description: In this function, the driver will check tx descriptor and
 *  free the used skbuffer of tx descriptor.
 */
static void fmac3h_tx(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	int txtail;
	int txhead;

	txtail = lp->tx_tail;
	txhead = lp->tx_head;

	while (txtail != txhead) {
		u32 dstatus;	/* the status of Tx descriptor */

		dstatus = lp->tx_ring[txtail].opts1;
		if (dstatus & OWN_BIT)
			break;

		/* Check for Tx Descriptor error */
		if (dstatus & TX_DESC_ERR) {

			/* renew the value of errors */
			lp->net_stats.tx_errors++;

			/*
			 * When carrier error, late collision error,
			 * Excessive collision error,
			 * underflow error happened, the packet is aborted.
			 */
			if ((dstatus & TX_DESC_LC_ERR) |
			    (dstatus & TX_DESC_NC_ERR)
			    | ((dstatus & TX_DESC_LCO_ERR) &
			       !(dstatus & TX_DESC_UF_ERR))
			    | (dstatus & TX_DESC_EC_ERR)
			    | (dstatus & TX_DESC_UF_ERR)) {

				lp->net_stats.tx_aborted_errors++;

				if ((dstatus & TX_DESC_EC_ERR) |
					(dstatus & TX_DESC_ED_ERR))
					lp->net_stats.tx_dropped++;

				if ((dstatus & TX_DESC_LC_ERR) |
				    (dstatus & TX_DESC_NC_ERR)) {
					/* Loss of Carrier | No Carrier */
					lp->net_stats.tx_carrier_errors++;
				}

				if (dstatus & TX_DESC_UF_ERR) {
					/* Undeflow Error */
					lp->net_stats.tx_fifo_errors++;
					lp->net_stats.tx_dropped++;
				} else if (dstatus & TX_DESC_LCO_ERR) {
					/* Late Collision without
					 * Undeflow Error */
					lp->net_stats.tx_window_errors++;
					lp->net_stats.tx_dropped++;
				}
			}
		}

		/* renew the status of collisions count */
		if (!(dstatus & TX_DESC_EC_ERR))
			/* Update collision count */
			lp->net_stats.collisions += (dstatus >> 3) & 0x0000000F;
		else  {
			/* Frame is aborted after 16 collisions */
			lp->net_stats.collisions += 16;
			lp->net_stats.tx_dropped++;
		}

		lp->net_stats.tx_packets++;

#ifdef MULTIPLE_DESC_HANDLING
		lp->net_stats.tx_bytes += lp->tx_skb[txtail].tx_length;

		/* delete the mapping with skbuffer */
		dma_unmap_single(NULL, lp->tx_skb[txtail].mapping,
				 lp->tx_skb[txtail].tx_length, DMA_TO_DEVICE);


		/* free the skbuffer */
		if (lp->tx_skb[txtail].skb != NULL) {
			dev_kfree_skb_any(lp->tx_skb[txtail].skb);
			lp->tx_skb[txtail].skb = NULL;
		}

#else

		lp->net_stats.tx_bytes += lp->tx_skb[txtail].skb->len;

		/* free the skbuffer */
		if (lp->tx_skb[txtail].skb != NULL) {

			/* delete the mapping with skbuffer */
			dma_unmap_single(NULL, lp->tx_skb[txtail].mapping,
				 lp->tx_skb[txtail].skb->len, DMA_TO_DEVICE);

			dev_kfree_skb_any(lp->tx_skb[txtail].skb);
			lp->tx_skb[txtail].skb = NULL;
		}
#endif

		/* Flush Tx_SKB and clean desciptor status */
		lp->tx_skb[txtail].mapping = 0;
		lp->tx_ring[txtail].opts1 = 0;
		lp->tx_ring[txtail].opts2 = 0;
		lp->tx_ring[txtail].addr1 = 0;
		lp->tx_ring[txtail].addr2 = 0;

		/* get next descriptor */
		txtail = NEXT_TX(txtail);
	}

	/* update txtail count */
	lp->tx_tail = txtail;

	if (netif_carrier_ok(dev) &&
	    netif_queue_stopped(dev) && (TX_RING_AVAIL(lp) > 0)) {
		/* wake queue */
		netif_wake_queue(dev);
	}

}

/* function to update the receive error count statistics */
static void fmac3h_rx_err_stat(struct fmac3h_private *lp, u32 status)
{
	/* renew the value of error */
	lp->net_stats.rx_errors++;

	/* when descripter error happen,
	 * it means rx ring buffer overflow. */
	if (status & RX_DESC_DE_ERR)
		lp->net_stats.rx_over_errors++;

	if ((status & RX_DESC_LE_ERR)
	    && !(status & RX_DESC_GF_ERR))
		/* Length Error without Giant Frame setting */
		lp->net_stats.rx_length_errors++;

	if (status & RX_DESC_CE_ERR)
		/* CRC Error */
		lp->net_stats.rx_crc_errors++;


	if (status & RX_DESC_OE_ERR)
		/* Overflow Error */
		lp->net_stats.rx_fifo_errors++;
}



#ifdef CONFIG_FMAC3H_NAPI
/* NAPI poll function */
static int fmac3h_poll(struct net_device *dev, int *budget)
#else
/*
 * Description: In this function, the driver will check rx descriptor and
 *  post skb to next network layer.
 */
static void fmac3h_rx(struct net_device *dev)
#endif
{
	struct fmac3h_private *lp = netdev_priv(dev);
	u32 rxtail, desc_num;
	u32 fdesc, ldesc;
#ifdef CONFIG_FMAC3H_NAPI
	int npackets = 0;
	int quota = min(dev->quota, *budget);

	desc_num = quota;		/* check for the quota(64) count
					 * only in RX ring */
#else
	desc_num = FMAC3H_RDESC_NUM;	/* check the whole RX ring */
#endif

	rxtail = lp->rx_tail;	/* start check from this descriptor */

	/*
	 * Check every RxDesc. if its OWN bit is 0,
	 * post its skbuf to n/w layer.
	 */
#ifdef CONFIG_FMAC3H_NAPI
	while (npackets < quota) {
#else
	while (desc_num) {
#endif
		struct sk_buff *skb;
		struct sk_buff *reskb;	/* skbuf passed to the upper layer */
		u32 rx_sta, datalen, buflen;
		u32 bnum, i;

		rx_sta = lp->rx_ring[rxtail].opts1;

		/* Check if "rxtail" desc no. is owned by FMAC4H */
		if (rx_sta & OWN_BIT) /* OWN bit is 1 */
			break;

		fdesc = rxtail;
		ldesc = rxtail;

		/* Traverse the Rx ring buffer,
		 * Search for the Last Descriptor(LS bit) so as to find
		 * "No. of descriptor (i.e bnum) per Rxed frame".
		 * or if Reached at the end of List
		 * then move out of the Rx routine.
		 */
		for (bnum = 1; bnum <= desc_num; bnum++) {

			rx_sta = lp->rx_ring[ldesc].opts1;
			/* OWN bit is 1 */
			if (rx_sta & OWN_BIT)
				goto rx_end;

			/* check for Last Desc */
			if (rx_sta & RX_DESC_LS)
				break;

			if (fdesc == NEXT_RX(ldesc)) {
				/* Reached at End of list */
				goto rx_end;
			} else
				ldesc = NEXT_RX(ldesc);

		}

		if (bnum > desc_num) {
			/* didn't found last Desc in complete
			 * Ring buffer traversal */
			goto rx_end;
		}

		/* check for Rx desc error and accordingly update
		 * the net_stats error vars */
		if (rx_sta & RX_DESC_ERR) {

			fmac3h_rx_err_stat(lp, rx_sta);
			goto rx_next;
		}

		/* Get data length from desc without 4bytes FCS */
		datalen = ((rx_sta >> 16) & 0x3FFF) - 4;
		/* DestinationAddr(6)+SourceAddr(6)+
		 * Type(2)+NET_IP_ALIGN(2)=16Bytes */
		buflen = datalen + NET_IP_ALIGN;

		/* alloc a properly sized skbuff */
		reskb = dev_alloc_skb(buflen);
		if (unlikely(reskb == NULL)) {
			PMSG(KERN_WARNING,
			     "Failed to alloc skb, \
				Drop! RxDesc(%d)  DataLen(%d)!!!",
			     rxtail, datalen);
			/* update dropped packet status */
			lp->net_stats.rx_dropped++;

			goto rx_next;
		}
		reskb->dev = dev;

		skb_reserve(reskb, NET_IP_ALIGN);

		/* Copy the "bnum" Descriptors buffer data from DMA Device
		 * to newly allocated reskb so as to send Rxed frame to
		 * n/w layer */
		i = bnum;
		while (i) {
			skb = lp->rx_skb[fdesc].skb;

			/* let cpu get ownership of skbuf from DMA */
			dma_sync_single_for_cpu(NULL,
						lp->rx_skb[fdesc].mapping,
						lp->rx_buf_sz, DMA_FROM_DEVICE);

			/*
			 * copy data from RX skbuffer
			 * to a properly sized skbuffer
			 */
			if (fdesc == ldesc) {
				memcpy(skb_put(reskb, datalen),
				       skb->data, datalen);
			} else {
				memcpy(skb_put(reskb, lp->rx_buf_sz),
				       skb->data, lp->rx_buf_sz);
				datalen -= lp->rx_buf_sz;
			}

			/* return the ownership of skbuf to DMA */
			dma_sync_single_for_device(NULL,
						   lp->rx_skb[fdesc].mapping,
						   lp->rx_buf_sz,
						   DMA_FROM_DEVICE);

			fdesc = NEXT_RX(fdesc);
			i--;
		}

		/*
		 * determine the packet type which will be
		 * used to identify network
		 */
		reskb->protocol = eth_type_trans(reskb, dev);

		lp->net_stats.rx_packets++;
		lp->net_stats.rx_bytes += reskb->len;

		dev->last_rx = jiffies;

		/* post skb to next network layer */
#ifdef CONFIG_FMAC3H_NAPI
		netif_receive_skb(reskb);
		npackets++;
#else
		netif_rx(reskb);
#endif

rx_next:	/* Mark the bnum no. of descriptor's as Rxed Descriptor status
		 * in the Rx Ring buffer */
		for (i = 0; i < bnum; i++) {
			/* initializate descriptor */
			lp->rx_ring[rxtail].addr1 = lp->rx_skb[rxtail].mapping;
			lp->rx_ring[rxtail].opts1 = 0;
			if (rxtail == (FMAC3H_RDESC_NUM - 1)) {
				lp->rx_ring[rxtail].opts2 =
				    END_RING | lp->rx_buf_sz;
			} else {
				lp->rx_ring[rxtail].opts2 = lp->rx_buf_sz;
			}
			/*
			* Make sure, the RX descriptor field gets updated,
			* then only Set the OWN Bit (that indicates,
			* the descriptor is owned by FMAC4H),
			* before moving to next descriptor
			* in the descriptor list.
			*/
			wmb();
			/* set own bit to 1 */
			lp->rx_ring[rxtail].opts1 = OWN_BIT;

			/* get next descriptor */
			rxtail = NEXT_RX(rxtail);
		}

		desc_num -= bnum;
	}
#ifdef CONFIG_FMAC3H_NAPI

	*budget -= npackets;
	dev->quota -= npackets;
#endif

rx_end:
	/* send RX POLL Demand to wake suspend receive process */
	fmac3h_reg_write(lp->base_addr, FMAC3H_RPDR, 1);

	lp->rx_tail = rxtail;

#ifdef CONFIG_FMAC3H_NAPI

	if (npackets < quota) {
		u32 temp;
		/* We processed all packets available.  Tell NAPI it can
		 * stop polling then re-enable rx interrupts */
		netif_rx_complete(dev);
		temp = fmac3h_reg_read(lp->base_addr, FMAC3H_IER);
		temp |= INT_RX_INT;
		fmac3h_reg_write(lp->base_addr, FMAC3H_IER, temp);

		return 0;
	}

	/* There are still packets waiting */
	return 1;
#endif

}


/*
 * fmac3h_interrupt -- Handle for fmac3h TX/RX interrupts.
 * @irq: the interrupt number
 * @dev_id: target net device id
 * Description: In this function, the driver will handle the device's
 * interrupts.
 */
static irqreturn_t fmac3h_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	struct net_device *dev = (struct net_device *)dev_id;
	struct fmac3h_private *lp = netdev_priv(dev);
	u32 ints;		/* recived interrupts */
	u32 temp;


	/* get interrupts from SR register */
	ints = fmac3h_reg_read(lp->base_addr, FMAC3H_SR);

	/* clear interrupt */
	fmac3h_reg_write(lp->base_addr, FMAC3H_SR, ints);

	if (!(ints & CHECK_INT)) {/* did not find any interrupt */
		return IRQ_NONE;
	}

	/*
	 * RX stop interrupt. RPS(SR[bit8])
	 * OR Receive Buffer Unavallable interrupt. RU(SR[bit7])
	 * OR Receive FIFO overflow interrupt. OVF(SR[bit4])
	 */
	if (ints & (INT_RX_STOP | INT_RXB_UNAV | INT_RX_OVERF)) {

		/* collect status */
		fmac3h_get_stats_from_mfc(lp);
	}

	/* Receive interrupt. RI(SR[bit6]) */
	if (ints & INT_RX_INT) {

#ifdef CONFIG_FMAC3H_NAPI
		/*
		 * Disable Rx interrupts and schedule NAPI poll
		 */
		temp =  fmac3h_reg_read(lp->base_addr, FMAC3H_IER);
		temp &= (~INT_RX_INT);
		fmac3h_reg_write(lp->base_addr, FMAC3H_IER, temp);

		netif_rx_schedule(dev);
#else
		/*
		 * Start passing received packet and
		 * send RX poll demand to wake suspending RX
		 */
		fmac3h_rx(dev);

#endif
	}

	/* Transmit finish interrupt. TI(SR[bit0]) */
	if (ints & INT_TX_INT) {
		/* Check every TX descriptor's
		 * status and free skbuff */
		fmac3h_tx(dev);

	}

	/* Fatal bus error interrupt. FBE(SR[bit13]) */
	if (ints & INT_BUS_ERR) {

		/* collect status */
		fmac3h_get_stats_from_mfc(lp);
	}


	return IRQ_HANDLED;
}

/*
 * fmac3h_stop_hw -- stop hardware
 * @lp: target F_MAC3H device structure
 *
 * Description: this function will stop the TX/RX engine and
 *  clear the status of hardware.
 */
static void fmac3h_stop_hw(struct fmac3h_private *lp)
{
	u32 val;

	/* disable interrupt */
	fmac3h_reg_write(lp->base_addr, FMAC3H_IER, 0);

	/* stop TX and RX process */
	fmac3h_stop_rxtx(lp);

	/* clear status register */
	val = fmac3h_reg_read(lp->base_addr, FMAC3H_SR);
	fmac3h_reg_write(lp->base_addr, FMAC3H_SR, val);

}

/*
 * fmac3h_timeout -- Handle for fmac3h timeout.
 * @dev: target net device structure
 *
 * Description: this function will reset the controller and
 * re-initialise it back.
 */
static void fmac3h_timeout(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	int err;

	/* collect the status of transfer before reset */
	fmac3h_get_stats_from_mfc(lp);

	/* stop network queue */
	netif_stop_queue(dev);
	netif_carrier_off(dev);

	/* stop hardware */
	fmac3h_stop_hw(lp);

	/* Reset the controller */
	fmac3h_reg_write(lp->base_addr, FMAC3H_BMR, BMR_SOFTWARE_RESET);

	/* have to wait more than 1600ns before access the register */
	udelay(2);

	/* initialise the hardware */
	err = fmac3h_init_hw(lp);
	if (err) {
		PMSG(KERN_WARNING, "Failed to initialise device!!!\n");
		return;
	}

	/* free the buffer space and initialise the descripor */
	fmac3h_clean_ring(lp);

	/* initialise the TX/RX ring */
	err = fmac3h_init_rings(lp);
	if (err) {
		PMSG(KERN_ERR, "Failed to initialise ring!!!\n");
		return;
	}

	/* wakeup queue to start transfer */
	netif_wake_queue(dev);
}

#ifdef CONFIG_NET_POLL_CONTROLLER
void fmac3h_poll_controller(struct net_device *dev)
{
	disable_irq(dev->irq);
	fmac3h_interrupt(dev->irq, dev, NULL);
	enable_irq(dev->irq);
}
#endif

/*
 * fmac3h_stats -- Get System Network Statistics
 * @dev: target net device structure
 *
 * Description: In this function, the driver collect the status of
 * the device statistics structure.
 */
static struct net_device_stats *fmac3h_stats(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);

	if (netif_running(dev))
		/* Get status from MFC registers */
		fmac3h_get_stats_from_mfc(lp);

	return &lp->net_stats;
}

/*
 * fmac3h_get_phymap -- find out all the PHYs on the device
 * @dev: target net device structure
 *
 * Description: Reads the ID registers of the every PHY addr(0-31) on the
 *   device. If the ID<>0xFFFFFFFF then set the bit of phy_map.
 */
static int fmac3h_get_phymap(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	u16 phy_reg;
	int i;
	int ret;

	/* initialise PHY present information */
	lp->phy_map = 0;

	for (i = 0; i <= MAX_PHY_ADR; i++) {
		u32 phy_id, temp_id;
		int j;

		/* Read phy ID from PHY's register1 & register2 */
		ret = fmac3h_phy_read(dev, i, MII_PHYSID1, &phy_reg);
		if (ret) {
			PMSG(KERN_WARNING, "Failed to Read PHY register\n");
			return -EACCES;
		}

		if (phy_reg == 0xffff) {
			DBG_PRINT("No PHY in slot(%d).\n", i);
			continue;
		}

		phy_id = (u32)phy_reg << 16;	/* get PHYID high */

		ret = fmac3h_phy_read(dev, i, MII_PHYSID2, &phy_reg);
		if (ret) {
			PMSG(KERN_WARNING, "Failed to read PHY register\n");
			return -EACCES;
		}

		if (phy_reg == 0xffff) {
			DBG_PRINT("No PHY in slot(%d).\n", i);
			continue;
		}

		phy_id |= phy_reg;	/* get PHYID low */
		DBG_PRINT("PHY%d: ID:0x%08x\n", i, phy_id);

		/* check if read PHY-ID is 0, then ignore it */
		if (phy_id == 0x0) {
			DBG_PRINT("No PHY in slot(%d).\n", i);
			continue;
		}

		/* determine whether this phy type is known */
		for (j = 0; (temp_id = known_phy_id[j]) != 0; j++) {
			if (phy_id == temp_id)
				break;
		}

		if (!temp_id) {
			PMSG(KERN_WARNING,
				"Unknown Phy ID: 0x%08x at ADDRESS:%d\n",
				phy_id, i);
		}

		/* set the phy present bit for this address */
		lp->phy_map |= (1 << i);
	}

	return 0;
}


/*
 * fmac3h_start_xmit -- this function queues the skbuffer to the hardware
 *			for xmit.
 * @skb: the socket buffer to send
 * @dev: target net device structure
 *
 * Description:  queues n/w skbuffer to the hardware for xmit.
 * */
static int fmac3h_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	struct fmac3h_desc *desp;	/* the pointer to descriptor */
	u32 tx_free_desc;		/* the number of free descriptors */
	u32 skblen, flag;
	u32 val;
	dma_addr_t mapping;

#ifdef MULTIPLE_DESC_HANDLING
	u32 bnum, i;
	struct sk_buff *temp_skb = skb;
	u32 txhead;	/* The first descriptor of the transfer ring */


	/* validate the skb frame len */
	skblen = ETH_HLEN < skb->len ? skb->len : ETH_HLEN;

	/* Use bnum descriptors to send one frame */
	if ((skblen % lp->rx_buf_sz) == 0)
		bnum = skblen / lp->rx_buf_sz;
	else
		bnum = skblen / lp->rx_buf_sz + 1;

	/* get the number of xmit free descriptors from Tx Ring */
	tx_free_desc = TX_RING_AVAIL(lp);
	/* check no. of Tx Desc's less than bnum,
	 * otherwise, stop the n/w queue, becuase H/W still busy
	 * in TXmitting, the n/w queue will be awaked
	 * when Tx ring gets freed in fmac3h_tx() func.
	 * */
	if (tx_free_desc < bnum) {
		netif_stop_queue(dev);
		return FMAC3H_TX_BUSY;
	}

	txhead = lp->tx_head;

	/* code to handle multiple descriptor routine */
	for (i = 1; i <= bnum; i++) {
		unsigned char *newskb_data = temp_skb->data;
		u32	newskb_len;

		desp = &lp->tx_ring[txhead];

		if (i == bnum) {
			/* Either first or last segment in the frame chain */
			newskb_len = skblen;

			/* Load the addr of skb at last tx_skb[] in
			 * list and free skb (i.e in fmac3h_tx() func) only
			 * after trasmitting the complete frame. */
			lp->tx_skb[txhead].skb = skb;
			lp->tx_skb[txhead].tx_length = skblen;

		} else {
			newskb_len = lp->rx_buf_sz;

			/* avoid freeing skb for each segment */
			lp->tx_skb[txhead].skb = NULL;

			lp->tx_skb[txhead].tx_length = lp->rx_buf_sz;

			/* decrease the length of SKB */
			skblen -= lp->rx_buf_sz;
			skb_pull(temp_skb, lp->rx_buf_sz);
		}

		mapping =
		    dma_map_single(NULL, newskb_data, newskb_len,
				   DMA_TO_DEVICE);

		/* Setting the descriptor's field */
		flag = INT_COMPLET | newskb_len;

		if (i == 1)	/* The first one of frame chain */
			flag |= FIRST_SEG;


		if (i == bnum)	/* The last one of frame chain  */
			flag |= LAST_SEG;


		if (txhead == (FMAC3H_TDESC_NUM - 1))
			/* This descriptor is the end of ring */
			flag |= END_RING;


		desp->opts1 = 0;
		desp->opts2 = flag;
		desp->addr1 = mapping;
		desp->addr2 = 0;

		/* Load the map address */
		lp->tx_skb[txhead].mapping = mapping;

		/* set OWN bit Other than the first descriptor */
		if (i > 1)
			desp->opts1 = OWN_BIT;

		/* Reached at the Last Tx Desc (i.e bnum), use the wmb(),
		 * then set the First Desc own bit */
		if (i == bnum) {
			/* Make sure, All the Desc field gets updated
			 * including OWN bit, and then only set the
			 * First Desc OWN Bit So as to avoid Tx underrun.*/
			wmb();
			desp = &lp->tx_ring[lp->tx_head];
			desp->opts1 = OWN_BIT;
		}

		/* get the next descriptor */
		txhead = NEXT_TX(txhead);
	}

	/* update the txhead count of TX ring */
	lp->tx_head = txhead;
#else

	tx_free_desc = TX_RING_AVAIL(lp);
	if (tx_free_desc == 0) {
		netif_stop_queue(dev);
		return FMAC3H_TX_BUSY;
	}

	/* Recieved skb will be less or equal to
	* 1524 bytes for one frame only*/
	skblen = ETH_HLEN < skb->len ? skb->len : ETH_HLEN;

	desp = &lp->tx_ring[lp->tx_head];

	mapping =
	    dma_map_single(NULL, skb->data, skblen, DMA_TO_DEVICE);

	/* Setting the descriptor's field */
	flag = INT_COMPLET | skblen;

	/* Mark this desc first & last segment both for the current frame */
	flag |= (LAST_SEG | FIRST_SEG);

	if (lp->tx_head == (FMAC3H_TDESC_NUM - 1))
		/* This descriptor is the end of ring */
		flag |= END_RING;

	desp->opts1 = 0;
	desp->opts2 = flag;
	desp->addr1 = mapping;
	desp->addr2 = 0;

	lp->tx_skb[lp->tx_head].skb = skb;

	/* Load the map address */
	lp->tx_skb[lp->tx_head].mapping = mapping;

	wmb();

	desp->opts1 = OWN_BIT;

	/* get the next descriptor */
	lp->tx_head = NEXT_TX(lp->tx_head);
#endif

	/* Set OMR to start TX */
	val = fmac3h_reg_read(lp->base_addr, FMAC3H_OMR);
	if (!(val & START_TX)) {
		val |= START_TX;	/* Start Transmition */
		fmac3h_reg_write(lp->base_addr, FMAC3H_OMR, val);
	}

	/* send TX POLL Demand to awake suspend transmit process */
	fmac3h_reg_write(lp->base_addr, FMAC3H_TPDR, 1);
	dev->trans_start = jiffies;

	return FMAC3H_TX_OK;
}

/* function to read the MAC address from CPLD register,
 * validate it and set the read MAC addr in
 * MAR0H & MAROL Mac register*/
void fmac3h_set_mac_address(struct net_device *dev,
				struct fmac3h_private *lp)
{
	u32 mac_addrl, mac_addrh;
	u32 cpldLow, cpldhigh;

	/*
	 * Read MAC address from the CPLD reg.
	 * As per "Workaround" information specified in mb8aa0350
	 * errata manual "FML_MB8AA0350_ER_R1.0E.pdf".
	 */
	cpldLow = fmac3h_reg_read((void __iomem *)
					IO_ADDRESS(MB8AA0350_CPLD_BASE), 0x10);
	cpldhigh = fmac3h_reg_read((void __iomem *)
					IO_ADDRESS(MB8AA0350_CPLD_BASE), 0x14);

	dev->dev_addr[0] = (cpldLow & 0x000000ff);
	dev->dev_addr[1] = ((cpldLow & 0x0000ff00) >> 8);
	dev->dev_addr[2] = ((cpldLow & 0x00ff0000) >> 16);
	dev->dev_addr[3] = ((cpldLow & 0xff000000) >> 24);
	dev->dev_addr[4] = (cpldhigh & 0x000000ff);
	dev->dev_addr[5] = ((cpldhigh & 0x0000ff00) >> 8);
	PMSG(KERN_DEBUG,
	     "CPLD MAC address: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x.\n",
	     dev->dev_addr[0], dev->dev_addr[1], dev->dev_addr[2],
	     dev->dev_addr[3], dev->dev_addr[4], dev->dev_addr[5]);

	/* Check validity of CPLD read MAC address. */
	if (!is_valid_ether_addr(dev->dev_addr)) {
		/* generate random MAC address */
		random_ether_addr(dev->dev_addr);
		PMSG(KERN_INFO,
		     "Random MAC address: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x.\n",
		     dev->dev_addr[0], dev->dev_addr[1], dev->dev_addr[2],
		     dev->dev_addr[3], dev->dev_addr[4], dev->dev_addr[5]);

	}

	/* write values in MAC address register */
	mac_addrh = (dev->dev_addr[0] << 8) | dev->dev_addr[1];
	mac_addrl = (dev->dev_addr[2] << 24)
			| (dev->dev_addr[3] << 16)
			| (dev->dev_addr[4] << 8)
			| dev->dev_addr[5];
	fmac3h_reg_write(lp->base_addr, FMAC3H_MAR0H, mac_addrh);
	fmac3h_reg_write(lp->base_addr, FMAC3H_MAR0L, mac_addrl);
}

/*
 * fmac3h_open -- Handle to open device.
 * @dev: target net device structure
 *
 * Description: In this function, the driver will initialise the descriptor,
 * buffer space, set the device's registers, register the interrupt handler
 * and starts the queue.
 */
static int fmac3h_open(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);
	int err;

	/* set the mac address in fmac3h
	 * MAR0H & MAR0L register */
	fmac3h_set_mac_address(dev, lp);

	/* Install the interrupt handler */
	err = request_irq(dev->irq, fmac3h_interrupt, 0, dev->name, dev);
	if (err) {
		PMSG(KERN_ERR, "Failed to register IRQ%d, ERRNO:%d!!!\n",
		     dev->irq, err);

		return -EBUSY;
	}

	/* Descriptor and Buffer initializate */
	lp->rx_ring = (struct fmac3h_desc *)
				dma_alloc_coherent(NULL,
						   FMAC3H_DESC_BYTES,
						   &lp->ring_dma,
						   GFP_KERNEL);
	if (lp->rx_ring == NULL) {
		PMSG(KERN_ERR, "Failed to alloc DMA descriptor memory!!!\n");
		err = -ENOMEM;
		goto err_out_freeirq;
	}

	lp->tx_ring = &lp->rx_ring[FMAC3H_RDESC_NUM];
	memset(lp->tx_ring, 0, sizeof(struct fmac3h_desc) * FMAC3H_TDESC_NUM);
	lp->tx_ring[FMAC3H_TDESC_NUM - 1].opts2 = END_RING;
	lp->rx_buf_sz = VLAN_FRAME_SIZE_ALIGN;	/* 1524 bytes */

	err = fmac3h_init_rings(lp);
	if (err) {
		PMSG(KERN_ERR, "Failed to initialize DMA descriptor!!!\n");
		goto err_out_cleanring;
	}

	/* Initialise hardware */
	err = fmac3h_init_hw(lp);
	if (err) {
		PMSG(KERN_ERR, "Failed to initialize LAN's hardware!!!\n");
		goto err_out_cleanring;
	}

	/* Initiate timer function which will check
	 * the state of carrier timely */
	init_timer(&lp->timer);
	lp->timer.expires = CHECK_CARRIER_TIME;
	lp->timer.data = (unsigned long)dev;
	lp->timer.function = fmac3h_check_carrier;
	add_timer(&lp->timer);

	/* start queue to allow transmit */
	netif_carrier_on(dev);
	netif_start_queue(dev);

	return 0;

err_out_cleanring:
	fmac3h_free_ring(lp);
err_out_freeirq:
	free_irq(dev->irq, dev);
	return err;
}

/*
 * fmac3h_close -- Handle to close the device.
 * @dev: target net device structure
 *
 * Description: In this function, the driver will stop the device,
 *  free the irq, and de-allocate ring buffer space.
 */
static int fmac3h_close(struct net_device *dev)
{
	struct fmac3h_private *lp = netdev_priv(dev);

	/* delete timer function which will check the state of carrier timely */
	del_timer_sync(&lp->timer);

	/* stop network queue */
	netif_stop_queue(dev);
	netif_carrier_off(dev);

	/* stop hardware first */
	fmac3h_stop_hw(lp);

	/* free irq  */
	free_irq(dev->irq, dev);

	/* free descriptor and buffer space */
	fmac3h_free_ring(lp);

	return 0;
}


/* Function to update the mdc_clock from SYS_CLK */
static int set_mdc_clk(struct fmac3h_private *lp)
{
	/* get MDC clock from SYS_CLOCK */
	if (SYS_CLK < 20) {
		PMSG(KERN_ERR, "Invalid SYS_CLK(%d), aborting!!!\n",
		     SYS_CLK);
		return -EINVAL;
	} else if (SYS_CLK < 35) {
		lp->mdc_clk = 2;
	} else if (SYS_CLK < 60) {
		lp->mdc_clk = 3;
	} else if (SYS_CLK < 100) {
		lp->mdc_clk = 0;
	} else if (SYS_CLK < 150) {
		lp->mdc_clk = 1;
	/* Add for new */
	} else if (SYS_CLK < 250) {
		lp->mdc_clk = 4;
	} else if (SYS_CLK < 300) {
		lp->mdc_clk = 5;
	} else {
		PMSG(KERN_ERR, "Invalid SYS_CLK(%d), aborting!!!\n",
		     SYS_CLK);
		return -EINVAL;
	}

	DBG_PRINT("SYS CLK:%d    MDC CLK:%d\n", SYS_CLK, lp->mdc_clk);

	return 0;
}


/*
 * fmac3h_probe -- Device Initialization Routine
 * @pdev: target platform device structure
 *
 * Description: In this function, the driver will probe for its device and
 *  its hardware location(I/O ports and IRQ line).
 */
static int __init fmac3h_probe(struct platform_device *pdev)
{
	struct net_device *dev;
	struct fmac3h_private *lp;
	int i, err;

	if (unlikely(pdev == NULL)) {
		PMSG(KERN_INFO,
		     "Can't find proper platform device, aborting!!!\n");
		return -ENODEV;
	}

	/* allocate a new ethernet device */
	DBG_PRINT("alloc etherdev\n");
	dev = alloc_etherdev(sizeof(struct fmac3h_private));
	if (!dev) {
		PMSG(KERN_ERR, "Etherdev alloc failed, aborting!!!\n");
		return -ENOMEM;
	}
	SET_NETDEV_DEV(dev, &pdev->dev);

	/* get private struct's pointer */
	lp = netdev_priv(dev);
	lp->dev = dev;

	/* register ethernet device */
	platform_set_drvdata(pdev, dev);

	/* initialise Phy spin lock */
	spin_lock_init(&lp->phy_lock);

	/* fill structure net_device in defaults */
	ether_setup(dev);

	/* fill structure net_device */
	dev->open = fmac3h_open;
	dev->stop = fmac3h_close;
	dev->hard_start_xmit = fmac3h_start_xmit;
	dev->set_multicast_list = fmac3h_set_rx_mode;
	dev->get_stats = fmac3h_stats;
	dev->ethtool_ops = &fmac3h_ethtool_ops;
	dev->do_ioctl = fmac3h_ioctl;
	dev->tx_timeout = fmac3h_timeout;
	dev->watchdog_timeo = 5 * HZ;	/* 5sec */
	dev->irq = pdev->resource[1].start;/* set IRQ	*/
	dev->change_mtu = fmac3h_change_mtu;

#ifdef CONFIG_NET_POLL_CONTROLLER
	dev->poll_controller = fmac3h_poll_controller;
#endif

#ifdef CONFIG_FMAC3H_NAPI
	dev->poll = fmac3h_poll;
	dev->weight = 64;
#endif

	/* Get F_MAC3H register memory space */
	lp->regs = request_mem_region(pdev->resource[0].start,
				      pdev->resource[0].end -
				      pdev->resource[0].start + 1, DRV_NAME);
	if (lp->regs == NULL) {
		PMSG(KERN_ERR,
		     "Can't alloc register memory space, aborting!!!\n");
		err = -ENOMEM;
		goto err_out_freedev;
	}

	/* map register address to virtual address space */
	lp->base_addr = ioremap(pdev->resource[0].start,
				pdev->resource[0].end -
				pdev->resource[0].start + 1);
	if (lp->base_addr == NULL) {
		PMSG(KERN_ERR, "Failed to ioremap ethernet registers!!!\n");
		err = -ENOMEM;
		goto err_out_freesource;
	}

	/* set mdc clock */
	if (set_mdc_clk(lp) < 0) {
		err = -EINVAL;
		goto err_out_unmap;
	}

	/* get phy present information */
	err = fmac3h_get_phymap(dev);
	if (err | (!lp->phy_map)) {
		PMSG(KERN_ERR, "PHY not found, aborting!!!\n");
		err = -EINVAL;
		goto err_out_unmap;
	}

	/* update the phy-id */
	for (i = 0; i <= MAX_PHY_ADR; i++) {
		if (lp->phy_map & (1 << i)) {
			lp->mii.phy_id = i;
			break;
		}
	}

	lp->mii.dev = dev;
	lp->mii.advertising = ADVERTISED_10baseT_Half | ADVERTISED_10baseT_Full
	    | ADVERTISED_100baseT_Half | ADVERTISED_100baseT_Full
	    | ADVERTISED_Autoneg | ADVERTISED_MII;
	lp->mii.full_duplex = 1;	/* Full duplex */
	lp->mii.force_media = 0;	/* Auto negotiation */
	lp->mii.supports_gmii = 0;	/* not support GMII */
	lp->mii.mdio_read = fmac3h_mii_read;
	lp->mii.mdio_write = fmac3h_mii_write;
	lp->mii.phy_id_mask = 0x1f;
	lp->mii.reg_num_mask = 0x1f;

	err = register_netdev(dev);
	if (err) {
		PMSG(KERN_ERR, "Failed to register netdev. ERRNO:%d!!!\n",
		     err);
		goto err_out_unmap;
	}

	PMSG(KERN_INFO, "FMAC3H Driver Loaded.\n");

	return 0;

err_out_unmap:
	iounmap(lp->base_addr);
err_out_freesource:
	release_resource(lp->regs);
err_out_freedev:
	free_netdev(dev);
	platform_set_drvdata(pdev, NULL);
	return err;
}

/*
 * fmac3h_remove -- remove the device from the system
 * @pdev: target platform device structure
 *
 * Description: Free the resource that was requested in probe and open
 *  process, if it has not been freed.
 */
static int fmac3h_remove(struct platform_device *pdev)
{
	struct net_device *dev;
	struct fmac3h_private *lp;

	if (unlikely(pdev == NULL)) {
		PMSG(KERN_INFO,
		     "Can't find proper platform device, aborting!!!\n");
		return -ENODEV;
	}

	dev = platform_get_drvdata(pdev);
	if (unlikely(dev == NULL)) {
		PMSG(KERN_ERR, "Can't  find proper device, aborting!!!\n");
		return -ENODEV;
	}

	lp = netdev_priv(dev);

	/* Unregister the device */
	unregister_netdev(dev);

	/* Unmap an ioremapped region */
	if (lp->base_addr != NULL) {
		DBG_PRINT("unmap memory\n");
		iounmap(lp->base_addr);
	}

	/* release previously reserved resource region */
	if (lp->regs != NULL) {
		DBG_PRINT("release resource\n");
		release_resource(lp->regs);
		kfree(lp->regs);
	}

	/* delete driver data */
	platform_set_drvdata(pdev, NULL);

	/* free the allocated net device */
	free_netdev(dev);

	return 0;

}

/* initializate the platform_driver structure */
static struct platform_driver fmac3h_driver = {
	.probe = fmac3h_probe,
	.remove = fmac3h_remove,
	.driver = {
		   .name = DRV_NAME,
		   .owner = THIS_MODULE,
		   },
};

static int __init fmac3h_module_init(void)
{
	return platform_driver_register(&fmac3h_driver);
}

static void __exit fmac3h_module_exit(void)
{
	platform_driver_unregister(&fmac3h_driver);
}

module_init(fmac3h_module_init)
module_exit(fmac3h_module_exit)
MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("Fujitsu F_MAC3H Ethernet driver");
