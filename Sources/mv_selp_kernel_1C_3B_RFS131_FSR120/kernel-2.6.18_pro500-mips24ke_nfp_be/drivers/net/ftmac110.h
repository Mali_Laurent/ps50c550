/*
 *  drivers/net/ftmac110.h
 *
 *  $Id: ftmac110.h,v 1.4 2009/10/09 10:00:12 xiangl Exp $
 *  
 *  Faraday FTMAC110 Device Driver
 *
 *  Copyright (C) 2005 Faraday Corp. (http://www.faraday-tech.com)
 *  
 *  All Rights Reserved
 *
 * ChangeLog:
 * 
 *  $Log: ftmac110.h,v $
 *  Revision 1.4  2009/10/09 10:00:12  xiangl
 *  Update with ftmac110.c.
 *
 *  Revision 1.3  2009/10/09 07:36:13  xiangl
 *  Update with ftmac110.c.
 *
 *  Revision 1.2  2009/09/23 07:08:07  xiangl
 *  Changed these files
 *
 *  Revision 1.1.1.1  2009/08/20 09:53:43  xiangl
 *  initial import of mv_pro_50_sx3
 *
 *  Revision 1.1.1.1  2009/04/08 09:44:38  hongming
 *  this is the initial import
 *
 *  Revision 1.2  2008/09/26 08:42:13  hongming
 *  Modified these files.
 *  Alan
 *
 *  Revision 1.1.1.1  2008/09/08 09:35:34  hongming
 *  initial import of kernel_sx
 *
 *  Revision 1.1.1.1  2006/03/31 06:56:24  ijsung
 *  	Initial check-in to CTD repository
 *
 *  Revision 1.6  2005/10/04 08:19:32  lukelee
 *  -
 *
 *  Revision 1.5  2005/10/04 08:12:23  lukelee
 *  Free DMA buffers on error or removing module
 *
 *  Revision 1.4  2005/09/29 09:03:30  lukelee
 *  *** empty log message ***
 *
 *  Revision 1.3  2005/09/29 08:57:31  lukelee
 *  Update according to our Linux Development Model spec.
 *
 *  Revision 1.2  2005/09/28 07:37:41  lukelee
 *  Checkin into CVS
 *
 *  Luke Lee   08/23/2005  Ported onto Linux 2.6 for Faraday CPE platform
 *  
 *  Ivan Wang  08/26/2004  Modified
 *  
 *  lmc83      11/29/2002  Modified from smc91111.h
 */  
    
#ifndef                     _FTMAC110_H_
#define                     _FTMAC110_H_
    
#define ISR_REG             0x00	// interrups status register
#define IMR_REG             0x04	// interrupt maks register
#define MAC_MADR_REG        0x08	// MAC address (Most significant)
#define MAC_LADR_REG        0x0c	// MAC address (Least significant)
    
#define MAHT0_REG	    0x10	// Multicast Address Hash Table 0 register
#define MAHT1_REG	    0x14	// Multicast Address Hash Table 1 register
#define TXPD_REG            0x18	// Transmit Poll Demand register
#define RXPD_REG            0x1c	// Receive Poll Demand register
#define TXR_BADR_REG        0x20	// Transmit Ring Base Address register
#define RXR_BADR_REG        0x24	// Receive Ring Base Address register
#define ITC_REG	            0x28	// interrupt timer control register
#define APTC_REG            0x2c	// Automatic Polling Timer control register
#define DBLAC_REG           0x30	// DMA Burst Length and Arbitration control register
    
#define MACCR_REG           0x88	// MAC control register
#define MACSR_REG           0x8c	// MAC status register
#define PHYCR_REG           0x90	// PHY control register
#define PHYWDATA_REG        0x94	// PHY Write Data register
#define FCR_REG             0x98	// Flow Control register
#define BPR_REG             0x9c	// back pressure register
#define WOLCR_REG           0xa0	// Wake-On-Lan control register
#define WOLSR_REG           0xa4	// Wake-On-Lan status register
#define WFCRC_REG           0xa8	// Wake-up Frame CRC register
#define WFBM1_REG           0xb0	// wake-up frame byte mask 1st double word register
#define WFBM2_REG           0xb4	// wake-up frame byte mask 2nd double word register
#define WFBM3_REG           0xb8	// wake-up frame byte mask 3rd double word register
#define WFBM4_REG           0xbc	// wake-up frame byte mask 4th double word register
#define TM_REG		    0xcc	// test mode register
    
#define PHYSTS_CHG_bit		(1UL<<9)
#define AHB_ERR_bit		(1UL<<8)
#define RPKT_LOST_bit		(1UL<<7)
#define RPKT_SAV_bit		(1UL<<6)
#define XPKT_LOST_bit		(1UL<<5)
#define XPKT_OK_bit		(1UL<<4)
#define NOTXBUF_bit		(1UL<<3)
#define XPKT_FINISH_bit		(1UL<<2)
#define NORXBUF_bit		(1UL<<1)
#define RPKT_FINISH_bit		(1UL<<0)
typedef struct  {
	unsigned int RXPOLL_CNT:4;
	unsigned int RXPOLL_TIME_SEL:1;
	unsigned int Reserved1:3;
	unsigned int TXPOLL_CNT:4;
	unsigned int TXPOLL_TIME_SEL:1;
	unsigned int Reserved2:19;
} FTMAC110_APTCR_Status;

#define SPEED100_bit	    (1UL<<18)	// speed mode  1:100Mbps  0:10Mbps
#define RX_BROADPKT_bit	    (1UL<<17)	// Receiving broadcast packet
#define RX_MULTIPKT_bit	    (1UL<<16)	// receiving multicast packet
#define FULLDUP_bit	    (1UL<<15)	// full duplex
#define CRC_APD_bit	    (1UL<<14)	// append crc to transmit packet
#define MDC_SEL_bit	    (1UL<<13)	// set MDC as TX_CK/10
#define RCV_ALL_bit	    (1UL<<12)	// not check incoming packet's destination address
#define RX_FTL_bit	    (1UL<<11)	// Store incoming packet even its length is great than 1518 byte
#define RX_RUNT_bit	    (1UL<<10)	// Store incoming packet even its length is les than 64 byte
#define HT_MULTI_EN_bit	    (1UL<<9)            
#define RCV_EN_bit	    (1UL<<8)	// receiver enable
#define XMT_EN_bit	    (1UL<<5)	// transmitter enable
#define CRC_DIS_bit         (1UL<<4)           
#define LOOP_EN_bit         (1UL<<3)	// Internal loop-back
#define SW_RST_bit	    (1UL<<2)	// software reset/
#define RDMA_EN_bit         (1UL<<1)	// enable DMA receiving channel
#define XDMA_EN_bit         (1UL<<0)	// enable DMA transmitting channel
    
#if 1
enum TXDES0 { TXDMA_OWN = 0x80000000, TXPKT_EXSCOL = 0x2, TXPKT_LATECOL =
	    0x1, 
};
enum TXDES1 { EDOTR = 0x80000000, TXIC = 0x40000000, TX2FIC =
	    0x20000000, FTS = 0x10000000, LTS = 0x08000000, TXBUF_SIZE =
	    0x000007FF, 
};
typedef struct {
	unsigned int desc0;
	unsigned int desc1;
	unsigned int desc2;
	
	    /*null descriptor for 16 bytes alignment */ 
	unsigned int desc3;
} TX_DESC;
enum RXDES0 { RXDMA_OWN = 0x80000000, FRS = 0x20000000, LRS =
	    0x10000000, RX_ODD_NB = 0x00400000, RUNT = 0x00200000, FTL =
	    0x00100000, CRC_ERR = 0x00080000, RX_ERR = 0x00040000, BROADCAST =
	    0x00020000, MULTICAST = 0x00010000, RFL = 0x000007FF, RX_STATUS =
	    0x307F07FF, 
};
enum RXDES1 { EDORR = 0x80000000, RXBUF_SIZE = 0x000007FF, 
};
typedef struct {
	unsigned int desc0;
	unsigned int desc1;
	unsigned int desc2;
	
	    /*null descriptor for 16 bytes alignment */ 
	unsigned int desc3;
} RX_DESC;

#else	/*  */
// --------------------------------------------------------------------
//              Receive Ring descriptor structure
// --------------------------------------------------------------------
typedef struct  {
	
	    // RXDES0
	unsigned int ReceiveFrameLength:11;	//0~10
	unsigned int Reserved1:5;	//11~15
	unsigned int MULTICAST:1;	//16
	unsigned int BROARDCAST:1;	//17
	unsigned int RX_ERR:1;	//18
	unsigned int CRC_ERR:1;	//19
	unsigned int FTL:1;
	unsigned int RUNT:1;
	unsigned int RX_ODD_NB:1;
	unsigned int Reserved2:5;
	unsigned int LRS:1;
	unsigned int FRS:1;
	unsigned int Reserved3:1;
	unsigned int RXDMA_OWN:1;	// 1 ==> owned by FTMAC110, 0 ==> owned by software
	
	    // RXDES1
	unsigned int RXBUF_Size:11;
	unsigned int Reserved:20;
	unsigned int EDOTR:1;
	
	    // RXDES2
	unsigned int RXBUF_BADR;
	unsigned int VIR_RXBUF_BADR;	// not defined, 我們拿來放 receive buffer 的 virtual address 
} RX_DESC;
typedef struct  {
	
	    // TXDES0
	unsigned int TXPKT_LATECOL:1;
	unsigned int TXPKT_EXSCOL:1;
	unsigned int Reserved1:29;
	unsigned int TXDMA_OWN:1;
	
	    // TXDES1
	unsigned int TXBUF_Size:11;
	unsigned int Reserved2:16;
	unsigned int LTS:1;
	unsigned int FTS:1;
	unsigned int TX2FIC:1;
	unsigned int TXIC:1;
	unsigned int EDOTR:1;
	
	    // RXDES2
	unsigned int TXBUF_BADR;
	unsigned int VIR_TXBUF_BADR;
} TX_DESC;

#endif	/*  */
    
// waiting to do:
#define	TXPOLL_CNT          8
#define RXPOLL_CNT          0
    
#define OWNBY_SOFTWARE	    0
#define OWNBY_FTMAC110	    1
    
// --------------------------------------------------------------------
//              driver related definition
// --------------------------------------------------------------------
#define MIN_RXDES_NUM           64 // must be 2's power
#define DEF_RXDES_NUM           256// must be 2's power
#define RX_BUF_SIZE         1536
#define MIN_TXDES_NUM           64 // must be 2's power
#define DEF_TXDES_NUM		128// must be 2's power
#define TX_BUF_SIZE         1536	// Luke Lee : Just bigger than 1518
struct ftmac110_local  {
	
	    // these are things that the kernel wants me to keep, so users
	    // can find out semi-useless statistics of how well the card is
	    // performing
	struct net_device_stats stats;
	
	    // Set to true during the auto-negotiation sequence
	int autoneg_active;
	
	    // Address of our PHY port
	unsigned int phyaddr;
	
	    // Type of PHY
	unsigned int phytype;
	
	    // Last contents of PHY Register 18
	unsigned int lastPhy18;
	spinlock_t lock;
	unsigned int dma_base;
	unsigned int dma_size;
	int rx_descs_num;
	int tx_descs_num;
	volatile RX_DESC *rx_descs;	// receive ring base address
	unsigned int rx_descs_dma;	// receive ring physical base address
	char *rx_buf;		// receive buffer cpu address
	int rx_buf_dma;		// receive buffer physical address
	int rx_idx;		// receive descriptor
	struct sk_buff **rx_skbuff;
	volatile TX_DESC *tx_descs;
	unsigned int tx_descs_dma;
	char *tx_buf;
	int tx_buf_dma;
	int tx_idx;
	int old_tx;
	struct sk_buff **tx_skbuff;
	int maccr_val;
	unsigned int use_external_dma:1;
	unsigned int full_duplex:1;
	unsigned int speed_100m:1;
	unsigned int force_media:1;
	struct mii_if_info mii;
};

#endif	/*  */
    
