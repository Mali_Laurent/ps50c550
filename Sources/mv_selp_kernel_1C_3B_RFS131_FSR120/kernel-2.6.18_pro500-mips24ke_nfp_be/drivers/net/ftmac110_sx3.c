/*
 *  drivers/net/ftmac110.c
 *
 *  $Id: ftmac110.c,v 1.5 2009/10/09 09:59:58 xiangl Exp $
 *  
 *  Faraday FTMAC110 Device Driver
 *
 *  Copyright (C) 2005 Faraday Corp. (http://www.faraday-tech.com)
 *  
 *  All Rights Reserved
 *
 * ChangeLog:
 * 
 *  $Log: ftmac110.c,v $
 *  Revision 1.5  2009/10/09 09:59:58  xiangl
 *  Support both external and internal DMA space.
 *
 *  Revision 1.4  2009/10/09 07:35:54  xiangl
 *  1.Fix /proc entry bug. 2.Update /proc entry write/read methods for power saving.
 *  3.Add ftmac110.mem=xM@xxM cmdline parse for DMA space allocation.
 *
 *  Revision 1.3  2009/09/23 07:08:07  xiangl
 *  Changed these files
 *
 *  Revision 1.2  2009/05/08 12:17:13  qipengz
 *  agent setting
 *
 *  Revision 1.1.1.1  2009/04/08 09:44:38  hongming
 *  this is the initial import
 *
 *  Revision 1.4  2008/09/26 08:42:13  hongming
 *  Modified these files.
 *  Alan
 *
 *  Revision 1.2  2008/09/19 10:07:56  hongming
 *  Modified these files for debug.
 *  Alan
 *
 *  Revision 1.1.1.1  2008/09/08 09:35:34  hongming
 *  initial import of kernel_sx
 *
 *  Revision 1.4  2007/02/27 02:00:57  francish
 *  Driver's zero copy has bug,so disable it.
 *
 *  Revision 1.3  2006/12/26 12:11:46  francish
 *  Add auto-negotiation for deciding PHY Speed.
 *
 *  Revision 1.2  2006/12/25 03:39:30  francish
 *  Modify for support FTMAC110.
 *
 *  Revision 1.1.1.1  2006/09/27 06:44:06  wdshih
 *
 *
 *  Revision 1.5  2006/09/21 09:43:45  ijsung
 *  	2006/9/21:	I-Jui Sung:
 *  			Fix panic issue after up-ping-down-up-ping sequence
 *
 *  Revision 1.4  2006/05/19 16:20:06  ijsung
 *  	Comment out debug info for fiq
 *
 *  Revision 1.3  2006/05/19 15:55:38  ijsung
 *  	FIQ relay support
 *
 *  Revision 1.2  2006/04/20 06:24:42  ijsung
 *  	Remove trailing cr in ftmac110.c; Change DBLAC to not burst
 *
 *  Revision 1.1.1.1  2006/03/31 06:56:24  ijsung
 *  	Initial check-in to CTD repository
 *
 *  Revision 1.8  2005/11/03 13:22:40  lukelee
 *  Revise error handling, and add "devnum" parameter
 *
 *  Revision 1.7  2005/10/04 08:19:06  lukelee
 *  -
 *
 *  Revision 1.6  2005/10/04 08:12:23  lukelee
 *  Free DMA buffers on error or removing module
 *
 *  Revision 1.5  2005/09/29 09:03:30  lukelee
 *  *** empty log message ***
 *
 *  Revision 1.4  2005/09/29 08:57:31  lukelee
 *  Update according to our Linux Development Model spec.
 *
 *  Revision 1.3  2005/09/28 07:37:41  lukelee
 *  Checkin into CVS
 *
 *  Luke Lee   08/23/2005  Ported onto Linux 2.6 for Faraday CPE platform
 *
 *  Ivan Wang  08/26/2004  Modified  
 *
 *  lmc83      11/29/2002  
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/moduleparam.h>

#include <linux/sched.h>
#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/interrupt.h>

#include <linux/in.h>
#include <linux/netdevice.h>	/* struct device, and other headers */
#include <linux/etherdevice.h>	/* eth_type_trans */
#include <linux/ip.h>		/* struct iphdr */
#include <linux/tcp.h>		/* struct tcphdr */
#include <linux/skbuff.h>

#include <linux/in6.h>
#include <asm/checksum.h>

#include <linux/version.h>
#include <linux/kernel.h>
#include <linux/fcntl.h>
#include <linux/ptrace.h>
#include <linux/ioport.h>
#include <linux/slab.h>
#include <linux/string.h>
#include <linux/proc_fs.h>
#include <asm/bitops.h>
#include <asm/io.h>
#include <linux/pci.h>
#include <linux/delay.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/skbuff.h>
#include <linux/ctype.h>
#include <linux/mii.h>
#include <linux/ethtool.h>

#include <asm/cacheflush.h>	// dma_clean_range
#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_int.h>

#include "ftmac110.h"

#define FTMAC110_IO_BASE 0x1B004000
#define FTMAC110_IRQ	 TRIHIDTV_ETHERNET_IRQ

/*
 * Dont use inl/outl here.
 * it seems there is bug.
 * Alan Liu
 */
#undef inl
#undef outl
#undef ins
#undef outs
#define inl(a) ReadRegWord((volatile void *)(a))
#define outl(d,a) WriteRegWord((volatile void *)(a),(d))
#define ins(a) ReadRegHWord((volatile void *)(a))
#define outs(d,a) WriteRegHWord((volatile void *)(a),(unsigned short)(d))

#define IPMODULE MAC

#define IPNAME	FTMAC110
#define CARDNAME "FTMAC110"
static const char version[] =
    "Faraday FTMAC110 Driver (Linux 2.6) 12/25/06 - (C) 2006 Faraday Corp.\n";

#define IP_COUNT 1

#define ENABLE_BOTTOM_HALF  0
//#define ZERO_COPY           1 //1
#define ZERO_COPY           0

#define FTMAC110_DEBUG    0

MODULE_AUTHOR("Faraday Corp.");
MODULE_LICENSE("Faraday License");

static volatile int trans_busy = 0;
static const char mac_string[] = "Faraday MAC";
static struct proc_dir_entry *proc_ftmac110;

#if FTMAC110_DEBUG > 2
static void print_packet(unsigned char *buf, int length);
#endif

#if FTMAC110_DEBUG > 0

#define PRINTK printk

#else

#define PRINTK(x...)

#endif

static int ftmac110_open(struct net_device *dev);
static void ftmac110_timeout(struct net_device *dev);
static int ftmac110_close(struct net_device *dev);
static struct net_device_stats *ftmac110_query_statistics(struct net_device
							  *dev);
static void ftmac110_set_multicast_list(struct net_device *dev);
static void ftmac110_phy_configure(struct net_device *dev);
//static int ftmac110_phy_reset(struct net_device *dev);
static irqreturn_t ftmac110_interrupt(int irq, void *dev_id,
				      struct pt_regs *regs);
static void ftmac110_rcv(void *dev);
static int ftmac110_probe(struct net_device *dev);
static int ftmac110_reset(struct net_device *dev);
static void ftmac110_enable(struct net_device *dev);
//static word ftmac110_read_phy_register(unsigned int ioaddr, unsigned char phyaddr, unsigned char phyreg);
//static void ftmac110_write_phy_register(unsigned int ioaddr, unsigned char phyaddr, unsigned char phyreg, word phydata);

static void clear_etherbridge_fifo(void);
static unsigned int phy_check_media(struct net_device *dev,
				    unsigned int ok_to_print,
				    unsigned int init_media);

static struct net_device *ftmac110_netdev[IP_COUNT];
static struct resource ftmac110_resource[IP_COUNT];

static void
put_mac(int base, unsigned char *mac_addr)
{
	unsigned int val;
/*
	printk("%02x,%02x,%02x,%02x,%02x,%02x\n",
		mac_addr[0],
		mac_addr[1],
		mac_addr[2],
		mac_addr[3],
		mac_addr[4],
		mac_addr[5]);
*/
	//printk("base is %x\n", base);

	val = ((u32) mac_addr[0]) << 8 | (u32) mac_addr[1];
	outl(val, base);
	val = ((((u32) mac_addr[2]) << 24) & 0xff000000) |
	    ((((u32) mac_addr[3]) << 16) & 0xff0000) |
	    ((((u32) mac_addr[4]) << 8) & 0xff00) |
	    ((((u32) mac_addr[5]) << 0) & 0xff);
	outl(val, base + 4);
}

static void
auto_get_mac(int id, char *mac_addr)
{
	mac_addr[0] = 0;
	mac_addr[1] = 0x84;
	mac_addr[2] = 0x14;
	mac_addr[3] = mac_string[2];
	mac_addr[4] = mac_string[3];
	mac_addr[5] = id;
}

#if 0

static void
get_mac(int base, char *mac_addr)
{
	unsigned int val;

	//printk("+get_mac\n");

	val = inl(base);
	mac_addr[0] = (val >> 8) & 0xff;
	mac_addr[1] = val & 0xff;
	val = inl(base + 4);	//john add +4
	mac_addr[2] = (val >> 24) & 0xff;
	mac_addr[3] = (val >> 16) & 0xff;
	mac_addr[4] = (val >> 8) & 0xff;
	mac_addr[5] = val & 0xff;
}

/*
 * 	Print the Ethernet address
 */
static void
print_mac(char *mac_addr)
{
	int i;

	printk("ADDR: ");
	for (i = 0; i < 5; i++) {
		printk("%2.2x:", mac_addr[i]);
	}
	printk("%2.2x \n", mac_addr[5]);
}

#endif

/*
 * 	Finds the CRC32 of a set of bytes.
 *	Again, from Peter Cammaert's code.
 */
static int
crc32(char *s, int length)
{
	/* indices */
	int perByte;
	int perBit;
	/* crc polynomial for Ethernet */
	const unsigned long poly = 0xedb88320;
	/* crc value - preinitialized to all 1's */
	unsigned long crc_value = 0xffffffff;

	//printk("+crc32\n");

	for (perByte = 0; perByte < length; perByte++) {
		unsigned char c;

		c = *(s++);
		for (perBit = 0; perBit < 8; perBit++) {
			crc_value = (crc_value >> 1) ^
			    (((crc_value ^ c) & 0x01) ? poly : 0);
			c >>= 1;
		}
	}
	return crc_value;
}

static int
ftmac110_reset(struct net_device *dev)
{
	unsigned ioaddr = dev->base_addr;
	int rcount;

	PRINTK("+ftmac110_reset:I/O addr=%X\n", ioaddr);

	if((ins(0x1b000184) & 0xc) == 0){
		printk(KERN_ERR "FTMAC110 clock is off.\n");
		return -ENODEV;
	}
	outl(SW_RST_bit, ioaddr + MACCR_REG);

	/* this should pause enough for the chip to be happy */
	for (rcount = 0; (inl(ioaddr + MACCR_REG) & SW_RST_bit) != 0; rcount++) {
		//mdelay(10);
		msleep_interruptible(10);
		if (rcount > 5)	// Retry 5 times
			return -ENODEV;
	}

	outl(0, ioaddr + IMR_REG);	/* Disable all interrupts */
	if (inl(ioaddr + IMR_REG) != 0)
		return -ENODEV;

	return 0;
}

/*
 . Function: ftmac110_enable
 . Purpose: let the chip talk to the outside work
 . Method:
 .	1.  Enable the transmitter
 .	2.  Enable the receiver
 .	3.  Enable interrupts
*/
static void
ftmac110_enable(struct net_device *dev)
{
	unsigned int ioaddr = dev->base_addr;
	int i;
	struct ftmac110_local *priv =
	    (struct ftmac110_local *) netdev_priv(dev);
	//char mac_addr[6];

	//printk("+ftmac110_enable\n");
	PRINTK("%s:ftmac110_enable ioaddr=%X\n", dev->name, ioaddr);

	for (i = 0; i < priv->rx_descs_num; ++i) {
		priv->rx_descs[i].desc0 |= cpu_to_le32(RXDMA_OWN);	// owned by FTMAC110
	}
	priv->rx_idx = 0;

#if 0
	printk("rx desc base: 0x%08x\n", &priv->rx_descs[0].desc0);
	printk("tx desc base: 0x%08x\n", &priv->tx_descs[0].desc0);
#endif

	for (i = 0; i < priv->tx_descs_num; ++i) {
		priv->tx_descs[i].desc0 &= cpu_to_le32(~TXDMA_OWN);	// owned by software
	}
	priv->tx_idx = 0;

	/* set the MAC address */
	put_mac(ioaddr + MAC_MADR_REG, dev->dev_addr);
	//john add
	//get_mac(ioaddr + MAC_MADR_REG, mac_addr);
	//print_mac(mac_addr);

	outl(priv->rx_descs_dma, ioaddr + RXR_BADR_REG);
	outl(priv->tx_descs_dma, ioaddr + TXR_BADR_REG);
//      outl( 0x00001010, ioaddr + ITC_REG); // TODO: threshold too small 
	outl(0x0, ioaddr + ITC_REG);
	outl((0UL << TXPOLL_CNT) | (0x1 << RXPOLL_CNT), ioaddr + APTC_REG);	// TODO: bandwidth overhead maybe too big, polling too often ?!
//      outl( 0x1d8, ioaddr + DBLAC_REG ); //Luke Lee: 1 110 010 111 (0x397)
	outl(0x390, ioaddr + DBLAC_REG);

	/* now, enable interrupts */
	outl(PHYSTS_CHG_bit | AHB_ERR_bit | RPKT_LOST_bit
//              | RPKT_SAV_bit  
	     | XPKT_LOST_bit | XPKT_OK_bit | NOTXBUF_bit
//              | XPKT_FINISH_bit
	     | NORXBUF_bit | RPKT_FINISH_bit, ioaddr + IMR_REG);

	/// enable trans/recv,...
	outl(priv->maccr_val, ioaddr + MACCR_REG);
	PRINTK("%s:ftmac110_enable DONE\n", dev->name);
}

/*
 . Function: ftmac110_shutdown
 . Purpose:  closes down the SMC91xxx chip.
 . Method:
 .	1. zero the interrupt mask
 .	2. clear the enable receive flag
 .	3. clear the enable xmit flags
 .
 . TODO:
 .   (1) maybe utilize power down mode.
 .	Why not yet?  Because while the chip will go into power down mode,
 .	the manual says that it will wake up in response to any I/O requests
 .	in the register space.   Empirical results do not show this working.
*/
static void
ftmac110_shutdown(unsigned int ioaddr)
{
	//printk("+ftmac110_shutdown\n");

	outl(0, ioaddr + IMR_REG);
	outl(0, ioaddr + MACCR_REG);
}

/*
 * Function: ftmac110_start_xmit( struct sk_buff * skb, struct device * )
 * Purpose:
 *    Attempt to allocate memory for a packet, if chip-memory is not
 *    available, then tell the card to generate an interrupt when it
 *    is available*
 *
 * Algorithm:
 *
 * o	if the saved_skb is not currently null, then drop this packet
 *		on the floor.  This should never happen, because of TBUSY.
 * o	if the saved_skb is null, then replace it with the current packet,
 * o	See if I can sending it now.
 * o 	(NO): Enable interrupts and let the interrupt handler deal with it.
 * o	(YES):Send it now.
 */
static int
ftmac110_start_xmit(struct sk_buff *skb, struct net_device *dev)
{
	struct ftmac110_local *priv =
	    (struct ftmac110_local *) netdev_priv(dev);
	unsigned int ioaddr = dev->base_addr;
	volatile TX_DESC *cur_desc;
	int length;
	unsigned long flags;
#if ZERO_COPY
	struct sk_buff *old_skb;
#endif
	//printk("+ftmac110_start_xmit\n");

	spin_lock_irqsave(&priv->lock, flags);
	if (skb == NULL) {
		printk("%s(%d): NULL skb???\n", __FILE__, __LINE__);
		spin_unlock_irqrestore(&priv->lock, flags);
		return 0;
	}

	cur_desc = &priv->tx_descs[priv->tx_idx];

//#ifdef not_complete_yet       
	if ((cur_desc->desc0 & cpu_to_le32(TXDMA_OWN)) != OWNBY_SOFTWARE)	/// no empty transmit descriptor 
	{
//              DO_PRINT("no empty transmit descriptor\n");
//              DO_PRINT("jiffies = %d\n", jiffies);
		priv->stats.tx_dropped++;
		netif_stop_queue(dev);	/// waiting to do: 
		spin_unlock_irqrestore(&priv->lock, flags);

		return 1;
	}
//#endif /* end_of_not */        

#if 0
	for (; (cur_desc->desc0 & cpu_to_le32(TXDMA_OWN)) != OWNBY_SOFTWARE;) {
		PRINTK(KERN_WARNING "NO empty TX\n");
		//printk("no empty TX descriptor:0x%x:0x%x\n",cur_desc,cur_desc[0]);
		udelay(1);
	}
#endif

	length = ETH_ZLEN < skb->len ? skb->len : ETH_ZLEN;
	length = min(length, TX_BUF_SIZE);	// truncate jumbo packets

#if ZERO_COPY
	if (priv->tx_skbuff[priv->tx_idx]) {
		old_skb = priv->tx_skbuff[priv->tx_idx];
		dev_kfree_skb_any(old_skb);
	}
#endif

#if FTMAC110_DEBUG > 4
	printk("Transmitting Packet at 0x%x\n", (unsigned int) cur_desc->desc3);
	print_packet(skb->data, length);
#endif

#if ZERO_COPY
	cur_desc->desc3 = (unsigned) skb->data;
	cur_desc->desc2 = cpu_to_le32(virt_to_phys(skb->data));
//Alan Liu
#if 0
	//dmac_clean_range((unsigned)skb->data, (unsigned)(char*)(skb->data + length));
#else
	dma_cache_wback_inv((unsigned) skb->data,
			    (unsigned) (char *) (skb->data + length));
#endif

//++ Odin
//      dmac_clean_range((unsigned)cur_desc, (unsigned)(char*)(cur_desc + sizeof(TX_DESC)) );
//-- Odin

#else
	memcpy((char *) cur_desc->desc3, skb->data, length);
#endif

	cur_desc->desc1 &= cpu_to_le32(~TXBUF_SIZE);
	cur_desc->desc1 |= cpu_to_le32(length & TXBUF_SIZE);
	cur_desc->desc1 |= cpu_to_le32(LTS);
	cur_desc->desc1 |= cpu_to_le32(FTS);

	cur_desc->desc1 &= cpu_to_le32(~TX2FIC);
	cur_desc->desc1 |= cpu_to_le32(TXIC);

	cur_desc->desc0 = cpu_to_le32(TXDMA_OWN);
	if(cur_desc->desc0 != cpu_to_le32(TXDMA_OWN))
		printk("BUG ON TXDMA OWN bit.\n");

#if ZERO_COPY
	/* Record buffer address to be freed later */
	priv->tx_skbuff[priv->tx_idx] = skb;
#endif
	clear_etherbridge_fifo();
	outl(0xffffffff, ioaddr + TXPD_REG);

	priv->tx_idx = (priv->tx_idx + 1) & (priv->tx_descs_num - 1);
	priv->stats.tx_packets++;
	priv->stats.tx_bytes += skb->len;
#if !ZERO_COPY
	dev_kfree_skb_any(skb);
#endif
	dev->trans_start = jiffies;
	spin_unlock_irqrestore(&priv->lock, flags);
	return 0;
}

#if 1
#define dma_allocate(x,y,z,w) dma_alloc_coherent((x),(y),(dma_addr_t*)(z),(w))
#else
#define dma_allocate(x,y,z,w) dma_alloc_writecombine((x),(y),(dma_addr_t*)(z),(w))
#endif

static int
ftmac110_ringbuf_alloc(struct net_device *dev, struct ftmac110_local *priv)
{
	int i;

	//printk("+ftmac110_ringbuf_alloc\n");
	if(priv->use_external_dma == 1){
		int index = 0;
		i = priv->dma_size / (RX_BUF_SIZE + sizeof(RX_DESC));
		while(i >= (1 << ++index))
			;
		priv->rx_descs_num = priv->tx_descs_num = (1 << (index - 2));

	#if ZERO_COPY
		priv->rx_skbuff = (struct sk_buff **)kmalloc( priv->rx_descs_num * sizeof(void *), GFP_ATOMIC);
		priv->tx_skbuff = (struct sk_buff **)kmalloc( priv->tx_descs_num * sizeof(void *), GFP_ATOMIC);
		if(priv->rx_skbuff == NULL || priv->tx_skbuff == NULL){
			printk("Skb buffer allocation fail\n");
			return -ENOMEM;
		}
	#endif
	}

	if(priv->use_external_dma == 1){
	//	printk("start:%08x size:%08x num:%08x\n", priv->dma_base, priv->dma_size, priv->rx_descs_num);
		priv->rx_descs_dma = priv->dma_base;
		priv->rx_descs = (volatile RX_DESC *)PHYSTOKSEG1(priv->rx_descs_dma);
	}else{
	    	priv->rx_descs = dma_allocate(NULL, sizeof (RX_DESC) * priv->rx_descs_num,
				 &(priv->rx_descs_dma), GFP_KERNEL);
	}
	if (priv->rx_descs == NULL || (((u32) priv->rx_descs & 0xf) != 0)) {
		printk("Receive Ring Buffer allocation error\n");
		return -ENOMEM;
	}
#if FTMAC110_DEBUG > 2
	else
		printk(KERN_INFO
		       "* Allocated RX descs=%X, bus addr=%X, size=%d*%d=%d\n",
		       (unsigned) priv->rx_descs, (unsigned) priv->rx_descs_dma,
		       sizeof (RX_DESC), priv->rx_descs_num,
		       sizeof (RX_DESC) * priv->rx_descs_num);
#endif
	memset((unsigned int *) priv->rx_descs, 0,
	       sizeof (RX_DESC) * priv->rx_descs_num);


	if(priv->use_external_dma == 1){
		priv->rx_buf_dma = (priv->rx_descs_dma + sizeof(RX_DESC) * priv->rx_descs_num + 31) & (~31);
		priv->rx_buf = (char *)PHYSTOKSEG1(priv->rx_buf_dma);
	}else{
	    	priv->rx_buf = dma_allocate(NULL, RX_BUF_SIZE * priv->rx_descs_num, &(priv->rx_buf_dma),
			 GFP_KERNEL);
	}
	if (priv->rx_buf == NULL || (((u32) priv->rx_buf & 3) != 0)) {
		printk("Receive Ring Buffer allocation error\n");
		return -ENOMEM;
	}
#if FTMAC110_DEBUG > 2
	else
		printk(KERN_INFO
		       "* Allocated RX buf=%X, bus addr=%X, size=%d*%d=%d\n",
		       (unsigned) priv->rx_buf, (unsigned) priv->rx_buf_dma,
		       RX_BUF_SIZE, priv->rx_descs_num, RX_BUF_SIZE * priv->rx_descs_num);
#endif

	memset((void *) priv->rx_buf, 0, sizeof (RX_BUF_SIZE) * priv->rx_descs_num);

	for (i = 0; i < priv->rx_descs_num; ++i) {
		priv->rx_descs[i].desc1 = cpu_to_le32(RX_BUF_SIZE);
//                priv->rx_descs[i].EDOTR = 0;                                                  // not last descriptor
		priv->rx_descs[i].desc2 =
		    cpu_to_le32(priv->rx_buf_dma + RX_BUF_SIZE * i);
		priv->rx_descs[i].desc3 =
		    (unsigned int) priv->rx_buf + RX_BUF_SIZE * i;
	}
	priv->rx_descs[priv->rx_descs_num - 1].desc1 |= cpu_to_le32(EDORR);	// is last descriptor


	if(priv->use_external_dma == 1){
		priv->tx_descs_dma = (priv->rx_buf_dma + RX_BUF_SIZE * priv->rx_descs_num + 31) & (~31);
		priv->tx_descs = (volatile TX_DESC *)PHYSTOKSEG1(priv->tx_descs_dma);
	}else{
	    	priv->tx_descs = dma_allocate(NULL, sizeof (TX_DESC) * priv->tx_descs_num,
			 &(priv->tx_descs_dma), GFP_KERNEL);
	}
	if (priv->tx_descs == NULL || (((u32) priv->tx_descs & 0xf) != 0)) {
		printk("Transmit Ring Buffer allocation error\n");
		return -ENOMEM;
	}
#if FTMAC110_DEBUG > 2
	else
		printk(KERN_INFO
		       "* Allocated TX descs=%X, bus addr=%X, size=%d*%d=%d\n",
		       (unsigned) priv->tx_descs, (unsigned) priv->tx_descs_dma,
		       sizeof (TX_DESC), priv->tx_descs_num,
		       sizeof (TX_DESC) * priv->tx_descs_num);
#endif
	memset((void *) priv->tx_descs, 0, sizeof (TX_DESC) * priv->tx_descs_num);

	if(priv->use_external_dma == 1){
		priv->tx_buf_dma = (priv->tx_descs_dma + sizeof(TX_DESC) * priv->tx_descs_num + 31) & (~31);
		priv->tx_buf = (char *)PHYSTOKSEG1(priv->tx_buf_dma);
	}else{
	    	priv->tx_buf = dma_allocate(NULL, TX_BUF_SIZE * priv->tx_descs_num, &(priv->tx_buf_dma),
			 GFP_KERNEL);
	}
	if (priv->tx_buf == NULL || (((u32) priv->tx_buf & 0x3) != 0)) {
		printk("Transmit Ring Buffer allocation error\n");
		return -ENOMEM;
	}
#if FTMAC110_DEBUG > 2
	else
		printk(KERN_INFO
		       "* Allocated TX buf=%X, bus addr=%X, size=%d*%d=%d\n",
		       (unsigned) priv->tx_buf, (unsigned) priv->tx_buf_dma,
		       TX_BUF_SIZE, priv->tx_descs_num, TX_BUF_SIZE * priv->tx_descs_num);
#endif

	memset((void *) priv->tx_buf, 0, sizeof (TX_BUF_SIZE) * priv->tx_descs_num);

	for (i = 0; i < priv->tx_descs_num; ++i) {
		priv->tx_descs[i].desc1 = 0;	// not last descriptor
		priv->tx_descs[i].desc2 =
		    cpu_to_le32(priv->tx_buf_dma + TX_BUF_SIZE * i);
		priv->tx_descs[i].desc3 =
		    (unsigned int) priv->tx_buf + TX_BUF_SIZE * i;
	}
	priv->tx_descs[priv->tx_descs_num - 1].desc1 |= cpu_to_le32(EDOTR);	// is last descriptor
	return 0;
}

#if 1
static DEFINE_MUTEX(ftmac110_power_mutex); 

static int
ftmac110_read_proc(char *page, char **start, off_t off, int count, int *eof,
		   void *data)
{
	struct net_device *dev = (struct net_device *) data;
	struct ftmac110_local *priv =
	    (struct ftmac110_local *) netdev_priv(dev);
	int num = 0;
	int i, line;
	unsigned short val;
	off_t begin = 0;

	//printk("+ftmac110_read_proc\n");
	val = ins(0x1b000184);
	if((val & 0xc) == 0xc)
		num = sprintf(page, "on\n");
	if((val & 0xc) == 0)
		num = sprintf(page, "off\n");

	num += sprintf(page + num, "priv->rx_idx = %d\n", priv->rx_idx);
	for (i = 0; i < priv->rx_descs_num; ++i){
		line =
		    sprintf(page + num, "[%d].RXDMA_OWN = %d\n", i,
			    priv->rx_descs[i].desc0 & cpu_to_le32(RXDMA_OWN));
		num += line;
		if(begin + num > off + count){
			num -= line;
			goto done;
		}
		if(begin + num <= off){
			begin += num;
			num = 0;
		}
	}
	*eof = 1;
done:
	*start = page;
	return num;
}


static int
ftmac110_write_proc(struct file *file, const char __user *buffer,
	unsigned long count, void *data)
{
	unsigned short val;
	char cmd[8];
	
	memset(cmd, 0, 8);
	if(count > 5)
		return -EINVAL;
	if(copy_from_user(cmd, buffer, count))
		return -EFAULT;

	mutex_lock(&ftmac110_power_mutex);
	val = ins(0x1b000184);	
	if(strcmp(cmd, "on\n") == 0){
		if((val & 0xc) == 0)
			outs(0x1b000184, (val | 0xc));
	}
	else if(strcmp(cmd, "off\n") == 0){
		if((val & 0xc) == 0xc)
			outs(0x1b000184, (val & (~0xc)));
	}
	else{
		mutex_unlock(&ftmac110_power_mutex);
		return -EINVAL;
	}
	mutex_unlock(&ftmac110_power_mutex);
	return count;
}

#endif

#if 1
static void
clear_etherbridge_fifo()
{

	unsigned short v6c, v6e;

	v6c = ReadRegHWord((volatile void *)0x1b00016c);
	v6e = ReadRegHWord((volatile void *)0x1b00016e);

	WriteRegHWord((volatile void *)0x1b00016c, v6c & ~0x8);
	WriteRegHWord((volatile void *)0x1b00016e, v6e);

	WriteRegHWord((volatile void *)0x1b00016c, v6c | 0x8);
	WriteRegHWord((volatile void *)0x1b00016e, v6e);
}

static int
read_phy_reg(int addr)
{
	unsigned int data;
	//data = 0x041f0000;
	data = 0x04100000;
	data |= addr << 21;
	WriteRegWord((volatile void *)0x1b004090, data);
	udelay(1000);
	return (ReadRegWord((volatile void *)0x1b004090) & 0x0000ffff);
}

#if 0
static unsigned
write_phy_reg(int addr, unsigned int data)
{
	//unsigned int cmd = 0x081f0000;
	unsigned int cmd = 0x08100000;
	cmd |= addr << 21;
	WriteRegWord((volatile void *)0x1b004094, data);
	WriteRegWord((volatile void *)0x1b004090, cmd);
	return 0;
}
#endif

/*Added by Seanlong begin---------------------------------------------*/

static unsigned int
phy_check_media(struct net_device *dev,
		unsigned int ok_to_print, unsigned int init_media)
{
	unsigned int old_carrier, new_carrier;
	int advertise, lpa, media, duplex, speed;
	struct ftmac110_local *priv = netdev_priv(dev);

	if (priv->force_media)
		return 0;	/* duplex did not change */

	old_carrier = netif_carrier_ok(dev) ? 1 : 0;
	read_phy_reg(MII_BMSR);
	if (read_phy_reg(MII_BMSR) & BMSR_LSTATUS)
		new_carrier = 1;
	else
		new_carrier = 0;

	/* if carrier state did not change, this is a "bounce",
	 * just exit as everything is already set correctly
	 */
	if ((!init_media) && (old_carrier == new_carrier))
		return 0;	/* duplex did not change */

	/* no carrier, nothing much to do */
	if (!new_carrier) {
		netif_carrier_off(dev);
		if (ok_to_print)
			printk(KERN_INFO "%s: link down\n", dev->name);
		return 0;	/* duplex did not change */
	}

	/*
	 * we have carrier, see who's on the other end
	 */
	netif_carrier_on(dev);

	/* get MII advertise and LPA values */
	advertise = read_phy_reg(MII_ADVERTISE);
	lpa = read_phy_reg(MII_LPA);

	/* figure out media and duplex from advertise and LPA values */
	media = mii_nway_result(lpa & advertise);
	duplex = (media & ADVERTISE_FULL) ? 1 : 0;
	speed = (media & (ADVERTISE_100FULL | ADVERTISE_100HALF)) ? 1 : 0;

	if (ok_to_print)
		printk(KERN_INFO "%s: link up, %sMbps, %s-duplex, lpa 0x%04X\n",
		       dev->name,
		       media & (ADVERTISE_100FULL | ADVERTISE_100HALF) ? "100" :
		       "10", duplex ? "full" : "half", lpa);

	if ((init_media) || (priv->full_duplex != duplex)
	    || (priv->speed_100m != speed)) {
		priv->full_duplex = duplex;
		priv->speed_100m = speed;

		if (priv->full_duplex)
			priv->maccr_val |= FULLDUP_bit;
		else
			priv->maccr_val &= ~FULLDUP_bit;

		if (priv->speed_100m)
			priv->maccr_val |= SPEED100_bit;
		else
			priv->maccr_val &= ~SPEED100_bit;
		return 1;	/* duplex or speed changed */
	}

	return 0;
}

static unsigned int ftmac110_get_link(struct net_device *dev)
{
        return netif_carrier_ok(dev) ? 1 : 0;   
}

static const struct ethtool_ops ftmac110_ethtool_ops = {
        .get_link       = ftmac110_get_link,
};


static int mdio_read(struct net_device *dev, int phy_id, int location)
{
	unsigned int ioaddr = dev->base_addr;
	unsigned int cmd = (1 << 26) | (phy_id << 16) | (location << 21);
	int val, i;

	outl(cmd, ioaddr + PHYCR_REG);

	for(i = 0; i < 10; i++){
		udelay(1000);
		val = inl(ioaddr + PHYCR_REG);
		if((val & (1 << 26)) == 0)
			return (val & 0xffff);
	}
	return -1;
}

static void mdio_write(struct net_device *dev, int phy_id, int location, int value)
{
	unsigned int ioaddr = dev->base_addr;
	unsigned int cmd = (1 << 27) | (phy_id << 16) | (location << 21);
	int ret, i;
	
	outl(value & 0xffff, ioaddr + PHYWDATA_REG);
	outl(cmd, ioaddr + PHYCR_REG);

	for(i = 0; i < 10; i++){
		udelay(1000);
		ret = inl(ioaddr + PHYCR_REG);
		if((ret & (1 << 27)) == 0)
			return ;
	}
}

static int netdev_ioctl(struct net_device *dev, struct ifreq *rq, int cmd)
{
	struct ftmac110_local *priv = netdev_priv(dev);
	int rc;
	
	if(!netif_running(dev))
		return -EINVAL;

	spin_lock_irq(&priv->lock);
	rc = generic_mii_ioctl(&priv->mii, if_mii(rq), cmd, NULL);
	spin_unlock_irq(&priv->lock);
	
	return rc;
}

/*Added by Seanlong end -----------------------------------------------*/

#endif

/*
 * Function: ftmac110_probe( struct net_device *dev )
 *
 * Purpose:
 *	Tests to see if the device 'dev' points to an ftmac110 chip.
 *	Returns a 0 on success
 */
static int __init
ftmac110_probe(struct net_device *dev)
{
	unsigned short led;
	unsigned int temp;
	int retval;
	static unsigned version_printed = 0;
	struct ftmac110_local *priv;
	char proc_name[15];	//Odin  
	//unsigned int v_sts, v_ctrl;

	if (version_printed++ == 0)
		printk(KERN_INFO "%s", version);

	/* Now, print out the card info, in a short format.. */
	printk(KERN_INFO "%s: device at %#3x IRQ:%d NOWAIT:%d\n", dev->name,
	       (unsigned) dev->base_addr, dev->irq, dev->dma);

	/* Initialize priviate data */
	dev->priv = netdev_priv(dev);
	priv = (struct ftmac110_local *) dev->priv;
//	memset(priv, 0, sizeof (struct ftmac110_local));
	spin_lock_init(&priv->lock);
	/*
	 * Study the bits more next days...
	 * Alan Liu
	 */
	priv->maccr_val =
//      FULLDUP_bit
	    CRC_APD_bit
	    //             | MDC_SEL_bit //Del by Seanlong
	    | RCV_EN_bit | XMT_EN_bit | RDMA_EN_bit | XDMA_EN_bit | RX_BROADPKT_bit;	//Odin Seanlong Del

/* Added by Alan Liu */
	led = ReadRegHWord((volatile void *)0x1b000026);
	WriteRegHWord((volatile void *)0x1b000026, led & ~0x1000);
	printk("Ethernet PHY LED turn on now.\n");

#if 1

	//Set UMAc Agent
	temp = ReadRegWord((volatile void *)0x1502206c);
//	temp &= ~0xff00;
	temp |= 0x220000;
	WriteRegWord((volatile void *)0x1502206c, temp);

	//Set DMA bridge endian: Little
	WriteRegHWord((volatile void *)0x1b00016c, 0x0021);
	WriteRegHWord((volatile void *)0x1b00016e, 0x0000);

#endif

/*Deleted by Seanlong begin -----------------------------------*/
/* Added by Seanlong begin ------------------------------------*/
#if 0

	v_sts = read_phy_reg(1);
	//if v &0x20, it means auto negotiation completes
	while (!(v_sts & 0x20)) {
		printk("Wait PHY AUTO NEGOTIATION  %08x\n", v_sts);
		v_sts = read_phy_reg(1);
	}

	v_sts = read_phy_reg(31);
	switch (v_sts & 0x1C) {
	case 0x18:
		priv->maccr_val |= (SPEED100_bit | FULLDUP_bit);
		printk("FULL DUPLEX SPEED 100M detected\n");
		break;
	case 0x08:
		priv->maccr_val |= SPEED100_bit;
		priv->maccr_val &= ~FULLDUP_bit;
		printk("HALF DUPLEX SPEED 100M detected\n");
		break;
	case 0x14:
		priv->maccr_val &= ~SPEED100_bit;
		priv->maccr_val |= FULLDUP_bit;
		printk("FULL DUPLEX SPEED 10M detected\n");
		break;
	case 0x04:
		priv->maccr_val &= ~(SPEED100_bit | FULLDUP_bit);
		printk("HALF DUPLEX SPEED 10M detected\n");
	}

#endif
/* Added by Seanlong end ------------------------------------*/
/*Deleted by Seanlong end -----------------------------------*/

	retval = ftmac110_ringbuf_alloc(dev, priv);
	if (retval)
		goto err_out;

	/* now, reset the chip, and put it into a known state */
	retval = ftmac110_reset(dev);
	if (retval) {
		printk("%s: unable to reset.\n", dev->name);
		goto err_out;
	}

	/* Fill in the fields of the device structure with ethernet values. */
	ether_setup(dev);

	/* Grab the IRQ */
	retval =
	    request_irq(dev->irq, &ftmac110_interrupt, SA_INTERRUPT, dev->name,
			dev);
	if (retval) {
		printk("%s: unable to get IRQ %d (irqval=%d).\n", dev->name,
		       dev->irq, retval);
		goto err_out;
	}

	dev->open = ftmac110_open;
	dev->stop = ftmac110_close;
	dev->hard_start_xmit = ftmac110_start_xmit;
	dev->get_stats = ftmac110_query_statistics;
	dev->tx_timeout = ftmac110_timeout;
#ifdef	HAVE_MULTICAST
	dev->set_multicast_list = &ftmac110_set_multicast_list;
#endif
	dev->ethtool_ops = &ftmac110_ethtool_ops;

	strcpy(proc_name, "ftmac110-");	//Odin
	strcat(proc_name, dev->name);	//Odin
#if 1
//        if ((proc_ftmac110 = create_proc_entry( "ftmac110", 0, 0 ))) {
	if ((proc_ftmac110 = create_proc_entry(proc_name, 0, 0))) {	//Odin
		proc_ftmac110->read_proc = ftmac110_read_proc;
		proc_ftmac110->write_proc = ftmac110_write_proc;
		proc_ftmac110->data = dev;
		proc_ftmac110->owner = THIS_MODULE;
	}
#endif

	dev->do_ioctl = netdev_ioctl;

	priv->phyaddr =  0x10;
	priv->mii.phy_id = priv->phyaddr;
	priv->mii.dev = dev;
	priv->mii.mdio_read = mdio_read;
	priv->mii.mdio_write = mdio_write;
	priv->mii.phy_id_mask = 0x1f;
	priv->mii.reg_num_mask = 0x1f;

	return 0;

      err_out:
	if(priv->use_external_dma == 0){
		dma_free_coherent(NULL, sizeof (RX_DESC) * priv->rx_descs_num,
				  (void *) priv->rx_descs,
				  (dma_addr_t) priv->rx_descs_dma);
		dma_free_coherent(NULL, RX_BUF_SIZE * priv->rx_descs_num, (void *) priv->rx_buf,
				  (dma_addr_t) priv->rx_buf_dma);
		dma_free_coherent(NULL, sizeof (TX_DESC) * priv->tx_descs_num,
				  (void *) priv->tx_descs,
				  (dma_addr_t) priv->tx_descs_dma);
		dma_free_coherent(NULL, TX_BUF_SIZE * priv->tx_descs_num, (void *) priv->tx_buf,
				  (dma_addr_t) priv->tx_buf_dma);
	}
	priv->rx_descs = NULL;
	priv->rx_descs_dma = 0;
	priv->rx_buf = NULL;
	priv->rx_buf_dma = 0;
	priv->tx_descs = NULL;
	priv->tx_descs_dma = 0;
	priv->tx_buf = NULL;
	priv->tx_buf_dma = 0;
	return retval;
}

#if FTMAC110_DEBUG > 2
static void
print_packet(unsigned char *buf, int length)
{
#if FTMAC110_DEBUG > 3
	int i;
	int remainder;
	int lines;
#endif

	printk("Packet of length %d \n", length);

#if FTMAC110_DEBUG > 3
	lines = length >> 4;
	remainder = length & 15;

	for (i = 0; i < lines; i++) {
		int cur;
		for (cur = 0; cur < 8; cur++) {
			unsigned char a, b;
			a = *(buf++);
			b = *(buf++);
			printk("%02x%02x ", a, b);
		}
		printk("\n");
	}
	for (i = 0; i < remainder / 2; i++) {
		unsigned char a, b;

		a = *(buf++);
		b = *(buf++);
		printk("%02x%02x ", a, b);
	}
	printk("\n");
#endif
}
#endif

/*
 * Open and Initialize the board
 *
 * Set up everything, reset the card, etc ..
 *
 */
static int
ftmac110_open(struct net_device *dev)
{
	int retval = 0;
	//struct ftmac110_local *priv =
	    //(struct ftmac110_local *) netdev_priv(dev);

	PRINTK("+%s:ftmac110_open\n", dev->name);

	/* reset the hardware */
	retval = ftmac110_reset(dev);
	if (retval) {
		printk("%s: unable to reset.\n", dev->name);
		retval = -ENODEV;
	} else {

/*Modified by Seanlong begin -----------------------------------*/

		/* Configure the PHY */
		//ftmac110_phy_configure(dev);

		phy_check_media(dev, 1, 1);
		ftmac110_enable(dev);
		netif_start_queue(dev);

/*Modified by Seanlong end -----------------------------------*/
		PRINTK("+%s:ftmac110_open DONE\n", dev->name);
	}

	return retval;
}

/*
 * Called by the kernel to send a packet out into the void
 * of the net.  This routine is largely based on
 * skeleton.c, from Becker.
 *
 */
static void
ftmac110_timeout(struct net_device *dev)
{
	/* If we get here, some higher level has decided we are broken.
	   There should really be a "kick me" function call instead. */
	printk(KERN_WARNING "%s: transmit timed out?\n", dev->name);

	//printk("+ftmac110_timeout\n");

	ftmac110_reset(dev);
	ftmac110_enable(dev);
	ftmac110_phy_configure(dev);
	netif_wake_queue(dev);
	dev->trans_start = jiffies;
}

#if ZERO_COPY
/*
 * Free transmitted skb buffer when it's safe.
 */
static void
ftmac110_free_tx(struct net_device *dev, int irq)
{
	struct ftmac110_local *priv =
	    (struct ftmac110_local *) netdev_priv(dev);
	volatile TX_DESC *wait_free_desc;
	struct sk_buff *skb;	// Odin
	int entry = priv->old_tx & (priv->tx_descs_num - 1);

	//enter spinlock
	if (!irq)
		spin_lock(&priv->lock);

	/* Free used tx skbuffs */
	while (entry != priv->tx_idx) {
//                struct sk_buff *skb;

		wait_free_desc = &priv->tx_descs[entry];
		if ((wait_free_desc->desc0 & cpu_to_le32(TXDMA_OWN)) !=
		    OWNBY_SOFTWARE) {
			break;
		}

		skb = priv->tx_skbuff[entry];
		if (skb) {
			dev_kfree_skb_any(skb);
			priv->tx_skbuff[entry] = 0;
		}

		entry = (entry + 1) & (priv->tx_descs_num - 1);
	}

	if (!irq)
		spin_unlock(&priv->lock);
	//exit spinloc

	priv->old_tx = entry;
	netif_wake_queue(dev);
}
#endif

/*
 .
 . This is the main routine of the driver, to handle the net_device when
 . it needs some attention.
 .
 . So:
 .   first, save state of the chipset
 .   branch off into routines to handle each case, and acknowledge
 .	    each to the interrupt register
 .   and finally restore state.
 .
 */

#if FTMAC110_DEBUG >4
static void
show_intstatus(unsigned int status)
{
	static int count = 0;
	if (status & PHYSTS_CHG_bit)
		printk("[%d]%s ", count, "PHYSTS_CHG");
	if (status & AHB_ERR_bit)
		printk("[%d]%s ", count, "AHB_ERR");
	if (status & RPKT_LOST_bit)
		printk("[%d]%s ", count, "RPKT_LOST");
	if (status & RPKT_SAV_bit)
		printk("[%d]%s ", count, "RPKT_SAV");
	if (status & XPKT_LOST_bit)
		printk("[%d]%s ", count, "XPKT_LOST");
	if (status & XPKT_OK_bit)
		printk("[%d]%s ", count, "XPKT_OK");
	if (status & NOTXBUF_bit)
		printk("[%d]%s ", count, "NOTXBUF");
	if (status & XPKT_FINISH_bit)
		printk("[%d]%s ", count, "XPKT_FINISH");
	if (status & NORXBUF_bit)
		printk("[%d]%s ", count, "NORXBUF");
	if (status & RPKT_FINISH_bit)
		printk("[%d]%s ", count, "RPKT_FINISH");
	if (status &
	    ~(PHYSTS_CHG_bit | AHB_ERR_bit | RPKT_LOST_bit | RPKT_SAV_bit |
	      XPKT_LOST_bit | XPKT_OK_bit | NOTXBUF_bit | XPKT_FINISH_bit |
	      NORXBUF_bit | RPKT_FINISH_bit))
		printk("[%d]%s ", count, "<Unknown>");
	count++;
}
#else
#define show_intstatus(x)
#endif

/*
 * The interrupt handler
 */
static irqreturn_t
ftmac110_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	struct net_device *dev = dev_id;
	unsigned int ioaddr = dev->base_addr;
	unsigned int status;	// interrupt status
	unsigned int mask;	// interrupt mask
	struct ftmac110_local *priv =
	    (struct ftmac110_local *) netdev_priv(dev);
	//unsigned long flags;
	//unsigned int maccr_val;

	unsigned int nr_serviced = 0;

	//       spin_lock_irqsave(&priv->lock,flags); // Luke 08/18/2005 ins
	//     PRINTK(KERN_INFO "+ftmac110_interrupt\n");

	if (dev == NULL || priv == NULL) {
		printk(KERN_WARNING "%s: irq %d for unknown device.\n",
		       "ftmac110_interrupt", irq);
		return IRQ_HANDLED;
	}

	/* read the interrupt status register */
	mask = inl(ioaddr + IMR_REG);

	/* read the status flag, and mask it */
	status = inl(ioaddr + ISR_REG) & mask;

	while (status && ++nr_serviced < 8) {

		show_intstatus(status);

		if (status & RPKT_FINISH_bit)
			ftmac110_rcv(dev);

		if (status & NORXBUF_bit) {
			//printk("<0x%x:NORXBUF>",status);
			outl(mask & ~NORXBUF_bit, ioaddr + IMR_REG);
			trans_busy = 1;

#if ENABLE_BOTTOM_HALF
			priv->rcv_tq.sync = 0;
			priv->rcv_tq.routine = ftmac110_rcv;
			priv->rcv_tq.data = dev;
			queue_task(&priv->rcv_tq, &tq_timer);
			//queue_task(&priv->rcv_tq, &tq_immediate);
#else
			ftmac110_rcv(dev);
#endif
		}

		if (status & AHB_ERR_bit)
			printk("<0x%x:AHB_ERR>", status);

		if (status & XPKT_FINISH_bit)
			printk("[XPKT_FINISH]");

		/*
		   if (status & PHYSTS_CHG_bit) {
		   }
		 */
		if (status & XPKT_OK_bit) {
#if ZERO_COPY
			ftmac110_free_tx(dev, 1);
#endif
			netif_wake_queue(dev);
		}
		/*
		   if (status & NOTXBUF_bit) {
		   }
		 */

		if (status & RPKT_LOST_bit) {
			priv->stats.rx_errors++;
			ftmac110_rcv(dev);
		}

		if (status & XPKT_LOST_bit) {
#if ZERO_COPY
			ftmac110_free_tx(dev, 1);
#endif
			priv->stats.tx_errors++;
			netif_wake_queue(dev);
		}

/*Added by Seanlong begin -----------------------------------*/
		if (status & PHYSTS_CHG_bit) {
			phy_check_media(dev, 1, 0);
#if 0
			maccr_val = inl(ioaddr + MACCR_REG);
			if (priv->full_duplex)
				maccr_val |= FULLDUP_bit;
			else
				maccr_val &= ~FULLDUP_bit;
			if (priv->speed_100m)
				maccr_val |= SPEED100_bit;
			else
				maccr_val &= ~SPEED100_bit;
#endif
			outl(priv->maccr_val, ioaddr + MACCR_REG);
		}
/*Added by Seanlong end -------------------------------------*/

//        PRINTK(KERN_INFO "+ftmac110_interrupt DONE\n");
		mask = inl(ioaddr + IMR_REG);
		status = inl(ioaddr + ISR_REG) & mask;
	}
//        PRINTK(KERN_INFO "\n");
//        spin_unlock_irqrestore(&priv->lock,flags); // Luke 08/18/2005 ins

	return IRQ_HANDLED;
}

/*
 . ftmac110_rcv -  receive a packet from the card
 .
 . There is ( at least ) a packet waiting to be read from
 . chip-memory.
 .
 . o Read the status
 . o If an error, record it
 . o otherwise, read in the packet
 */

static void
ftmac110_rcv(void *devp)
{
	struct net_device *dev = (struct net_device *) devp;
	struct ftmac110_local *priv =
	    (struct ftmac110_local *) netdev_priv(dev);
	unsigned int ioaddr = dev->base_addr;
	int packet_length;
	int rcv_cnt;
	volatile RX_DESC *cur_desc;
	int cpy_length;
	int cur_idx;
	int seg_length;
	int have_package;
	int have_frs;
	int start_idx;
	int first_idx;
	char *t_dma_vaddr;
	struct sk_buff *skb;
	unsigned char *data;

//        PRINTK("+ ftmac110_rcv\n");

	start_idx = priv->rx_idx;

	for (rcv_cnt = 0; rcv_cnt < 8; ++rcv_cnt) {
		packet_length = 0;
		first_idx = cur_idx = priv->rx_idx;

		have_package = 0;
		have_frs = 0;
		for (;
		     ((cur_desc =
		       &priv->rx_descs[priv->rx_idx])->
		      desc0 & cpu_to_le32(RXDMA_OWN)) == 0;) {
			have_package = 1;
			priv->rx_idx = (priv->rx_idx + 1) & (priv->rx_descs_num - 1);
			if (cur_desc->desc0 & cpu_to_le32(FRS)) {
				have_frs = 1;
				if (cur_desc->
				    desc0 & cpu_to_le32(RX_ERR | CRC_ERR | FTL |
							RUNT | RX_ODD_NB)) {
					priv->stats.rx_errors++;	// error frame....
					break;
				}
				if (cur_desc->desc0 & cpu_to_le32(MULTICAST))
					priv->stats.multicast++;
				packet_length = le32_to_cpu(cur_desc->desc0) & RFL;	// normal frame
			}
			if (cur_desc->desc0 & cpu_to_le32(LRS))	// packet's last frame
				break;
		}
		if (have_package == 0)
			goto done;
		if (have_frs == 0)
			priv->stats.rx_over_errors++;

		if (packet_length > 0) {
			// Allocate enough memory for entire receive frame, to be safe
			skb = dev_alloc_skb(packet_length + 2);

			if (skb == NULL) {
				printk(KERN_NOTICE
				       "%s: Low memory, packet dropped.\n",
				       dev->name);
				priv->stats.rx_dropped++;
				goto done;
			}

			skb_reserve(skb, 2);	/* 16 bit alignment */
			skb->dev = dev;

			data = skb_put(skb, packet_length);
			cpy_length = 0;
			for (; cur_idx != priv->rx_idx;
			     cur_idx = (cur_idx + 1) & (priv->rx_descs_num - 1)) {
				seg_length =
				    min(packet_length - cpy_length,
					RX_BUF_SIZE);
				t_dma_vaddr =
				    (char *) priv->rx_descs[cur_idx].desc3;
#if 1
				if (have_frs && (first_idx == cur_idx)) {
					t_dma_vaddr += 2;
					if (seg_length > (RX_BUF_SIZE - 2))
						seg_length = RX_BUF_SIZE - 2;
				}
#endif
				memcpy(data + cpy_length, t_dma_vaddr,
				       seg_length);
				cpy_length += seg_length;
			}

			skb->protocol = eth_type_trans(skb, dev);
			netif_rx(skb);
			dev->last_rx = jiffies;
			priv->stats.rx_packets++;
			priv->stats.rx_bytes += skb->len;
		}
	}

      done:
	if (start_idx != priv->rx_idx) {
		for (cur_idx = (start_idx + 1) % priv->rx_descs_num;
		     cur_idx != priv->rx_idx;
		     cur_idx = (cur_idx + 1) % priv->rx_descs_num) {
			priv->rx_descs[cur_idx].desc0 |= cpu_to_le32(RXDMA_OWN);
		}
		priv->rx_descs[start_idx].desc0 |= cpu_to_le32(RXDMA_OWN);
	}
	if (trans_busy == 1) {
		printk("rx busy\n");
		trans_busy = 0;
		outl(priv->maccr_val, ioaddr + MACCR_REG);
		outl(inl(ioaddr + IMR_REG) | NORXBUF_bit, ioaddr + IMR_REG);
	}
//        PRINTK("+ ftmac110_rcv DONE\n");

	return;
}

/*
 . ftmac110_close
 .
 . this makes the board clean up everything that it can
 . and not talk to the outside world.   Caused by
 . an 'ifconfig ethX down'
 .
 */
static int
ftmac110_close(struct net_device *dev)
{
	//printk("+ftmac110_close\n");
	netif_stop_queue(dev);

	/*Added by Seanlong */
	netif_carrier_off(dev);

	ftmac110_shutdown(dev->base_addr);
#if ZERO_COPY
	ftmac110_free_tx(dev, 0);
#endif
	return 0;
}

/*
 . Get the current statistics.
 . This may be called with the card open or closed.
 */
static struct net_device_stats *
ftmac110_query_statistics(struct net_device *dev)
{
	struct ftmac110_local *priv =
	    (struct ftmac110_local *) netdev_priv(dev);

	PRINTK("+ftmac110_query_statistics\n");

	return &priv->stats;
}

#ifdef HAVE_MULTICAST

/*
 . Function: ftmac110_setmulticast( unsigned int ioaddr, int count, dev_mc_list * adds )
 . Purpose:
 .    This sets the internal hardware table to filter out unwanted multicast
 .    packets before they take up memory.
 */

static void
ftmac110_setmulticast(unsigned int ioaddr, int count, struct dev_mc_list *addrs)
{
	struct dev_mc_list *cur_addr;
	int crc_val;

	//printk("+ftmac110_setmulticast\n");

	for (cur_addr = addrs; cur_addr != NULL; cur_addr = cur_addr->next) {
		if (!(*cur_addr->dmi_addr & 1))
			continue;
		crc_val = crc32(cur_addr->dmi_addr, 6);
		crc_val = (crc_val >> 26) & 0x3f;	// �� MSB 6 bit
		if (crc_val >= 32)
			outl(inl(ioaddr + MAHT1_REG) | (1UL << (crc_val - 32)),
			     ioaddr + MAHT1_REG);
		else
			outl(inl(ioaddr + MAHT0_REG) | (1UL << crc_val),
			     ioaddr + MAHT0_REG);
	}
}

/*
 . ftmac110_set_multicast_list
 .
 . This routine will, depending on the values passed to it,
 . either make it accept multicast packets, go into
 . promiscuous mode ( for TCPDUMP and cousins ) or accept
 . a select set of multicast packets
*/
static void
ftmac110_set_multicast_list(struct net_device *dev)
{
	unsigned int ioaddr = dev->base_addr;
	struct ftmac110_local *priv = (struct ftmac110_local *) dev->priv;

	//printk("+ftmac110_set_multicast_list\n");

	if (dev->flags & IFF_PROMISC)
		priv->maccr_val |= RCV_ALL_bit;
	else
		priv->maccr_val &= ~RCV_ALL_bit;

	if (!(dev->flags & IFF_ALLMULTI))
		priv->maccr_val |= RX_MULTIPKT_bit;
	else
		priv->maccr_val &= ~RX_MULTIPKT_bit;

	if (dev->mc_count) {
		priv->maccr_val |= HT_MULTI_EN_bit;
		ftmac110_setmulticast(ioaddr, dev->mc_count, dev->mc_list);
	} else
		priv->maccr_val &= ~HT_MULTI_EN_bit;

	outl(priv->maccr_val, ioaddr + MACCR_REG);
}
#endif

/*
 * Module initialization function
 */

static char *
strdup(const char *s)
{
	char *dups = kmalloc(strlen(s) + 1, GFP_KERNEL);
	if (dups)
		strcpy(dups, s);
	return dups;
}

static inline void
set_io_resource(int id, struct resource *mac_res)
{
	char buf[32];
	sprintf(buf, "FTMAC110 MAC controller %d", id + 1);
	mac_res->name = strdup(buf);
#if 0
	mac_res->start = IP_va_base[id];
	mac_res->end = IP_va_limit[id];
#else
	mac_res->start = FTMAC110_IO_BASE;
	mac_res->end = FTMAC110_IO_BASE + 0x100;
#endif
}

static int devnum = IP_COUNT;
module_param(devnum, int, S_IRUGO);

/*Add by Seanlong begin------------------------*/

static unsigned int cmdline_mac[6];

/*Old __setup method for command line parameter setup*/
#if 0

static int __init
mac_setup(char *str)
{
	int i;
	char *pstr = str;

	if ((pstr == NULL) || (*pstr == '\0'))
		goto error;

	for (i = 0; i < 6; i++) {
		if (!isxdigit(*pstr) || !isdigit(*(++pstr)))
			goto error;
		pstr++;
		pstr++;
	}
	if (*(--pstr))
		goto error;

	sscanf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
	       &cmdline_mac[0], &cmdline_mac[1], &cmdline_mac[2],
	       &cmdline_mac[3], &cmdline_mac[4], &cmdline_mac[5]);

	return 1;

      error:
	printk(KERN_ERR "Invalid command line mac address.\n");
	return 1;
}

__setup("mac=", mac_setup);
#endif

static char *mac;
module_param(mac, charp, 0);

static int __init
cmdline_get_mac(char *str, unsigned char *mac_addr)
{
	int i;
	char *pstr = str;

	if ((pstr == NULL) || (*pstr == '\0'))
		return 0;

	for (i = 0; i < 6; i++) {
		if (!isxdigit(*pstr) || !isxdigit(*(++pstr))) {
			printk(KERN_ERR
			       "FTMAC110 invalid command line MAC address: NOT A HEX.\n");
			return 0;
		}
		/*':' , '-' or any other one byte gap is ok */
		pstr++;
		pstr++;
	}
	if (*(--pstr)) {
		printk(KERN_ERR
		       "FTMAC110 invalid command line MAC address: BAD ENDING.\n");
		return 0;
	}

	sscanf(str, "%02x:%02x:%02x:%02x:%02x:%02x",
	       &cmdline_mac[0], &cmdline_mac[1], &cmdline_mac[2],
	       &cmdline_mac[3], &cmdline_mac[4], &cmdline_mac[5]);

	for (i = 0; i < 6; i++)
		mac_addr[i] = (unsigned char) cmdline_mac[i];

	return 1;
}


#include <asm/bootinfo.h>
static char *mem;
module_param(mem, charp, 0);

/*ftmac110.mem=xM@xxM ftmac110.mem=@xxM*/
static int __init
cmdline_get_mem(char *str, unsigned int *start, unsigned int *size)
{
	int i;
	char *p = str;
	unsigned int end;

	if((p == NULL) || (*p == '\0'))
		return 0;
	
	*size = 0;		
	if(*p != '@')
		*size = memparse(p, &p);
	else
		*size = 0x100000;
	if(*p != '@' || !isdigit(*(p+1))){
		printk(KERN_ERR "FTMAC110 DMA space allocation bad format.\n");
		return -1;
	}
	*start = memparse(p + 1, &p);
	end = *start + *size;

	if(*size < (MIN_RXDES_NUM + MIN_TXDES_NUM) * (sizeof(RX_DESC) + RX_BUF_SIZE)){
		printk(KERN_ERR "FTMAC110 DMA space allocation too small.\n");
		return -1;
	}

	for(i = 0; i < boot_mem_map.nr_map; i++){
		if(*start >= (boot_mem_map.map[i].addr + boot_mem_map.map[i].size)){
			if(i+1 == boot_mem_map.nr_map || 
				(i+1 < boot_mem_map.nr_map && end <= boot_mem_map.map[i+1].addr))
				return 1;
		}
	}
	printk(KERN_ERR "FTMAC110 DMA space allocation overlap occurs.\n");
	return -1;
}

/*Add by Seanlong end------------------------*/

static int __init
ftmac110_module_init(void)
{
	int result, id, thisresult;
	struct net_device *dev;
	struct ftmac110_local *priv;

	PRINTK("+init_module\n");

	result = -ENODEV;
	for (id = 0; id < devnum; id++) {
		dev = alloc_etherdev(sizeof (struct ftmac110_local));
		if (!dev) {
			printk(KERN_ERR "Fail allocating ethernet device");
			return -ENODEV;
		}
		priv = (struct ftmac110_local *)netdev_priv(dev);
		memset(priv, 0, sizeof (struct ftmac110_local));
		set_io_resource(id, &ftmac110_resource[id]);
		if (request_resource(&ioport_resource, &ftmac110_resource[id])) {
			printk(KERN_ERR
			       "Fail to request IO address 0x%08X~0x%08X.",
			       ftmac110_resource[id].start,
			       ftmac110_resource[id].end);
		}
		ftmac110_netdev[id] = NULL;
		SET_MODULE_OWNER(dev);

		/* Copy the parameters from the platform specification */
#if 0
		dev->base_addr = IP_va_base[id];
		dev->irq = IP_irq[id];
#else
		//dev->base_addr = FTMAC110_IO_BASE;
		dev->base_addr =
		    (unsigned long) ioremap(ftmac110_resource[id].start, 0x100);
		if ((void *)dev->base_addr == NULL) {
			printk(KERN_ERR "ioremap failed %08x\n",
			       FTMAC110_IO_BASE);
			release_resource(&ftmac110_resource[id]);
			free_netdev(dev);
			result = -ENOMEM;
			break;
		}
		//printk(KERN_INFO "ftmac110's base_addr is %08x\n",dev->base_addr);
		dev->irq = FTMAC110_IRQ;
#endif

		//dev->dma = nowait; // Use DMA field for nowait
		/* Setup initial mac address */

		/*Edit by Seanlong Begin------------------------ */
		if (cmdline_get_mac(mac, dev->dev_addr)) 
			;
		else
			auto_get_mac(id, dev->dev_addr);
	
		result = cmdline_get_mem(mem, &priv->dma_base, &priv->dma_size); 
		if ( result == 1)
			priv->use_external_dma = 1;
		else if( result == 0){
			priv->rx_descs_num = DEF_RXDES_NUM;
			priv->tx_descs_num = DEF_TXDES_NUM;
			priv->use_external_dma = 0;
		}else{
			result = -ENOMEM;
			break;
		}
		/*Edit by Seanlong End------------------------- */

		dev->init = ftmac110_probe;
		if ((thisresult = register_netdev(dev)) != 0) {
			free_irq(dev->irq, dev);
			release_resource(&ftmac110_resource[id]);
			iounmap((void *)dev->base_addr);
			free_netdev(dev);
		} else {
			ftmac110_netdev[id] = dev;
		}
		if (thisresult == 0)	// any of the devices initialized, run
			result = 0;
	}

	return result;
}

/*
 * Cleanup when module is removed with rmmod
 */

static void
ftmac110_module_exit(void)
{
	int id;
	struct net_device *dev;
	struct ftmac110_local *priv;
	PRINTK("+cleanup_module\n");

	for (id = 0; id < devnum; id++) {
		dev = ftmac110_netdev[id];

		if (dev == NULL)
			continue;

		priv = (struct ftmac110_local *) netdev_priv(dev);
		if (priv->use_external_dma == 0){
			if (priv->rx_descs)
				dma_free_coherent(NULL, sizeof (RX_DESC) * priv->rx_descs_num,
					  (void *) priv->rx_descs,
					  (dma_addr_t) priv->rx_descs_dma);
			if (priv->rx_buf)
				dma_free_coherent(NULL, RX_BUF_SIZE * priv->rx_descs_num,
					  (void *) priv->rx_buf,
					  (dma_addr_t) priv->rx_buf_dma);
			if (priv->tx_descs)
				dma_free_coherent(NULL, sizeof (TX_DESC) * priv->tx_descs_num,
					  (void *) priv->tx_descs,
					  (dma_addr_t) priv->tx_descs_dma);
			if (priv->tx_buf)
				dma_free_coherent(NULL, TX_BUF_SIZE * priv->tx_descs_num,
					  (void *) priv->tx_buf,
					  (dma_addr_t) priv->tx_buf_dma);
		}
		priv->rx_descs = NULL;
		priv->rx_descs_dma = 0;
		priv->rx_buf = NULL;
		priv->rx_buf_dma = 0;
		priv->tx_descs = NULL;
		priv->tx_descs_dma = 0;
		priv->tx_buf = NULL;
		priv->tx_buf_dma = 0;

		/* No need to check MOD_IN_USE, as sys_delete_module() checks. */
		unregister_netdev(dev);

		free_irq(dev->irq, dev);
		//TODO: where is the request_region ?
		//release_region(devFMAC.base_addr, SMC_IO_EXTENT);
		free_netdev(dev);
		ftmac110_netdev[id] = NULL;
		// free resource, free allocated memory
		release_resource(&ftmac110_resource[id]);
		iounmap((void *)dev->base_addr);
	}
}

module_init(ftmac110_module_init);
module_exit(ftmac110_module_exit);

#if 1

/*
 * we need reset phy first, then we enable auto-negotiation
 * and check the full-duplex or half-duplex.
 * Then we need set the mac MACCR
 */
//int
//ftmac110_phy_reset()
#if 0
static int
ftmac110_phy_reset(struct net_device *dev)
{
	//unsigned int v;

	//delay 1000 us to wait for the first HW reset to PHY
	udelay(1000);

	//enable sw reset.
	WriteRegWord((volatile void *)0x1b004094, 0x8000);
	WriteRegWord((volatile void *)0x1b004090, 0x08100000);

	//delay 1000us again.
	udelay(1000);

	//print the PHY control register now.
	WriteRegWord((volatile void *)0x1b004090, 0x04100000);
	//printk("%s:%d 0x1b004090 is %08x\n",__FILE__,__LINE__,ReadRegWord(0x1b004090));
	return 0;
}
static void
ftmac110_write_phy_register(unsigned int ioaddr,
			    unsigned char phyaddr, unsigned char phyreg,
			    u16 phydata)
{

}
static u16
ftmac110_read_phy_register(unsigned int ioaddr, unsigned char phyaddr,
			   unsigned char phyreg)
{
	return 0;
}
#endif
/*
 * Configures the specified PHY using Autonegotiation.
 */
static void
ftmac110_phy_configure(struct net_device *dev)
{
	return;
}

#endif
