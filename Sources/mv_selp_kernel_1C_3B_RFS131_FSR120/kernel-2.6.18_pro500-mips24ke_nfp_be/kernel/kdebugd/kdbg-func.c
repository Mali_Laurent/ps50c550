/*
 * kdbg-func.c
 *
 * Copyright (C) 2009 Samsung Electronics
 * Created by lee jung-seung (js07.lee@samsung.com)
 *
 * 2009-11-17 : Added, Virt to physical ADDR converter.
 * 2009-11-20 : Modified, show_task_state_backtrace for taking real backtrace on kernel.
 * NOTE:
 */

#include <linux/poll.h>
#include <linux/kthread.h>
#include <linux/ktime.h>
#include <linux/kdebugd.h>
#include <linux/proc_fs.h>
#include <asm/pgtable.h>

extern struct task_struct* get_task_with_pid(void);
extern unsigned int debugd_get_event(void);
extern int show_state_prio(void);
extern void turnoff_cpu_usage(void);
extern int show_cpu_usage(void);
extern int show_sched_history(void);
extern void wrap_show_task(struct task_struct*);

extern struct proc_dir_entry *kdebugd_dir;
struct proc_dir_entry *kdebugd_cpu_dir;
unsigned int kdebugd_nobacktrace = 0;

static int pctracing = 0;
/* kdebugd Functions */

static int show_task_state(void)
{
    kdebugd_nobacktrace = 1;
    show_state();
    task_state_help();

    return 1;
}

static int show_task_state_backtrace(void)
{
	struct task_struct* tsk;
    kdebugd_nobacktrace = 0;

	tsk = get_task_with_pid();

	if(tsk == NULL ){
    	show_state();
		return 1;	/* turn on the menu */
	}

	wrap_show_task(tsk);

	return 1;
}

static int kill_task_for_coredump(void)
{
	struct task_struct* tsk;

	tsk = get_task_with_pid();
	if(tsk == NULL ){
		printk("\n[ALERT] NO Thread");
		return 1;	/* turn on the menu */
	}

	/* Send Signal */
	force_sig(SIGABRT, tsk);

	return 1;	/* turn on the menu */
}

/* 6. Dump task register with pid */
static int show_user_thread_regs(void)
{
    struct task_struct* tsk;
    struct pt_regs* regs;

    tsk = get_task_with_pid();
    if(tsk == NULL ){
        printk("\n[ALERT] NO Thread");
        return 1;	/* turn on the menu */
    }
    regs = task_pt_regs(tsk);
    show_regs(regs);

    return 1;	/* turn on the menu */
}

/* 7. Dump task pid maps with pid */
static void __show_user_maps_with_pid(void)
{
    struct task_struct* tsk;

	tsk = find_user_task_with_pid();
	if (tsk == NULL) return;

    dump_pid_maps(tsk);
}

static int show_user_maps_with_pid(void)
{
    __show_user_maps_with_pid();

    return 1;
}

extern void show_user_stack(struct task_struct *task, struct pt_regs * regs);

/* 8. Dump user stack with pid */
static void __show_user_stack_with_pid(void)
{
    struct task_struct* tsk=NULL;
    struct pt_regs* regs=NULL;

	tsk = find_user_task_with_pid();

	if (tsk == NULL) return;

	regs = task_pt_regs(tsk);

    show_user_stack(tsk, regs);
}

static int show_user_stack_with_pid(void)
{
	__show_user_stack_with_pid();

	return 1;
}


/* 9. Convert Virt Addr(User) to Physical Addr */
extern unsigned long get_physaddr(struct task_struct* , unsigned long, int);

static void __physical_memory_converter(void)
{
	struct task_struct* tsk;
	unsigned long p_addr,u_addr;

	tsk = get_task_with_pid();
	if(tsk == NULL || !tsk->mm){
		printk("\n[ALERT] %s Thread", (tsk==NULL) ? "No":"Kernel");
		return;
	}

	/* get address */
	printk("\nEnter memory address....\n===>  ");
	u_addr = debugd_get_event();

	/* Convert User Addr => Kernel mapping Addr */
	p_addr = get_physaddr(tsk,u_addr, 1);
	if(!p_addr) {
		printk("\n [KDEBUGD] "
			"The virtual Address(user:0x%08lx) is not mapped to real memory"
			,u_addr);
	}else {
		printk("\n [KDEBUGD] physical address :0x%08lx",p_addr);
	}

	return;
}

static int physical_memory_converter(void)
{
	__physical_memory_converter();
	return 1;
}

/* 11. Trace thread execution(look at PC) */

struct timespec timespec_interval = { .tv_sec = 0 , .tv_nsec = 100000 } ;

pid_t trace_tsk_pid;
struct hrtimer trace_thread_timer;
static unsigned int prev_pc  = 0;

static enum hrtimer_restart show_pc(struct hrtimer* timer)
{
    unsigned int cur_pc;
    struct task_struct* trace_tsk = 0;
    ktime_t now;
    // Check whether timer is still tracing already dead thread or not
    trace_tsk = find_task_by_pid(trace_tsk_pid);

    if(!trace_tsk) {
        printk("\n [SP_DEBUG] traced task is killed...\n");
        prev_pc = 0;
        return HRTIMER_NORESTART;
    }

    now = ktime_get();
    cur_pc = KSTK_EIP(trace_tsk);

    if(prev_pc != cur_pc)
        printk("\n[SP_DEBUG] Pid:%d  PC:0x%08x\t\t(TIME:%d.%d)",
                trace_tsk_pid,cur_pc,now.tv.sec,now.tv.nsec);

    prev_pc = cur_pc;

    hrtimer_forward(&trace_thread_timer,now,timespec_to_ktime(timespec_interval));
    return HRTIMER_RESTART;
}

static void start_trace_timer(void)
{
    hrtimer_init(&trace_thread_timer, CLOCK_MONOTONIC, HRTIMER_MODE_REL);
    trace_thread_timer.expires = timespec_to_ktime(timespec_interval);
    trace_thread_timer.function = show_pc;
    hrtimer_start(&trace_thread_timer, trace_thread_timer.expires, HRTIMER_MODE_REL);
}

static inline void end_trace_timer(void)
{
    hrtimer_cancel( &trace_thread_timer );
}

static void turnoff_trace_thread_pc(void)
{
    if( pctracing != 0) {
        printk("\ntrace thread pc OFF!!\n");
        end_trace_timer();
        prev_pc = 0;
        pctracing = 0;
    }
}

static void __trace_thread_pc(void)
{
    struct task_struct* trace_tsk;

    if(pctracing == 0)
    {
        printk("trace thread pc ON!!\n");
        trace_tsk = get_task_with_pid();

        if(trace_tsk == NULL || !trace_tsk->mm){ 
            printk("[ALERT] %s Thread\n", trace_tsk==NULL? "No":"Kernel");
            return;
        }
        trace_tsk_pid = trace_tsk->pid;
        start_trace_timer();
        pctracing = 1;
    }else
    {
        printk("trace thread pc OFF!!\n");
        end_trace_timer();
        prev_pc = 0;
        pctracing = 0;
    }
}

static int trace_thread_pc(void)
{
	__trace_thread_pc();

	return 1;
}

extern char cpu_usage_buffer[1024];
static int proc_cpu_usage(char *page, char **start,
        off_t off, int count,
        int *eof, void *data)
{
    int len;

    len = sprintf(page, "%s",cpu_usage_buffer);

    return len;
}


static int __init kdbg_misc_init(void)
{
    int retval = 0;
    kdebugd_cpu_dir = create_proc_read_entry("cpu_usage",
            0444, kdebugd_dir,
            proc_cpu_usage,
            NULL);
    if( kdebugd_cpu_dir == NULL) {
        remove_proc_entry("cpu_usage",kdebugd_dir);
        retval = -ENOMEM;
        return retval;
    }

    kdbg_register("A list of tasks and their relation information",show_task_state, NULL);
    kdbg_register("A list of tasks and their priority information",show_state_prio, NULL);
    kdbg_register("A list of tasks and their inforamtion + backtrace(kernel)",show_task_state_backtrace, NULL);
	kdbg_register("Kill the task to create coredump", kill_task_for_coredump, NULL);
    kdbg_register("Turn On/Off O(1) Scheduler prints",show_cpu_usage, turnoff_cpu_usage);
	kdbg_register("Virt(User) to physical ADDR Converter ",physical_memory_converter, NULL);
    kdbg_register("Dump task register with pid",show_user_thread_regs, NULL);
    kdbg_register("Dump task maps with pid",show_user_maps_with_pid, NULL);
    kdbg_register("Dump user stack with pid",show_user_stack_with_pid, NULL);
    kdbg_register("Trace thread execution(look at PC)",trace_thread_pc, turnoff_trace_thread_pc);
    kdbg_register("Schedule history logger",show_sched_history, NULL);

    return retval;
}

module_init(kdbg_misc_init)
