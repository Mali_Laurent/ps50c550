/*
 *  linux/arch/arm/mach-mb8aa0350/exirc.c
 *
 * Copyright (C) 2008 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/io.h>
#include <asm/hardware.h>
#include <linux/irq.h>

#include "core.h"

#define EXIRC0_VA_BASEP		__io_address(EXIRC0_VA_BASE)
#define EXIRC1_VA_BASEP		__io_address(EXIRC1_VA_BASE)
#define EXIRC2_VA_BASEP		__io_address(EXIRC2_VA_BASE)
#define EXIRC3_VA_BASEP		__io_address(EXIRC3_VA_BASE)
#define EXIRC4_VA_BASEP		__io_address(EXIRC4_VA_BASE)
#define EXIRC5_VA_BASEP		__io_address(EXIRC5_VA_BASE)

#define	EIENB	0x00 /* External Interrupt Enable register */
#define	EIREQ	0x04 /* IRQ Request register */
#define	EILM	0x08 /* Interrupt level register */

#define	EXIRC_INT_ENABLE		1
#define	EXIRC_INT_DISABLE		0

#define	EXIRC_LVL_LOW			0x0	/* Low level */
#define	EXIRC_LVL_HIGH			0x1	/* High level */
#define	EXIRC_LVL_REDGE			0x2	/* Rising edge */
#define	EXIRC_LVL_FEDGE			0x3	/* Falling edge */

#define	EXIRC_INT0			0
#define	EXIRC_INT1			1
#define	EXIRC_INT2			2
#define	EXIRC_INT3			3

#define	MASK_INT0			(1<<EXIRC_INT0)
#define	MASK_INT1			(1<<EXIRC_INT1)
#define	MASK_INT2			(1<<EXIRC_INT2)
#define	MASK_INT3			(1<<EXIRC_INT3)

struct exirq_res {
	void __iomem	*base;
	unsigned int	irq;
	unsigned int	mask;
	unsigned int	enable;
	unsigned int	lvl;
} ;

static struct exirq_res exirq[] = {
{EXIRC0_VA_BASEP, EXIRC_INT0, MASK_INT0, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC0_VA_BASEP, EXIRC_INT1, MASK_INT1, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC0_VA_BASEP, EXIRC_INT2, MASK_INT2, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC0_VA_BASEP, EXIRC_INT3, MASK_INT3, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC1_VA_BASEP, EXIRC_INT0, MASK_INT0, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC1_VA_BASEP, EXIRC_INT1, MASK_INT1, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC1_VA_BASEP, EXIRC_INT2, MASK_INT2, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC1_VA_BASEP, EXIRC_INT3, MASK_INT3, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC2_VA_BASEP, EXIRC_INT0, MASK_INT0, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC2_VA_BASEP, EXIRC_INT1, MASK_INT1, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC2_VA_BASEP, EXIRC_INT2, MASK_INT2, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC2_VA_BASEP, EXIRC_INT3, MASK_INT3, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC3_VA_BASEP, EXIRC_INT0, MASK_INT0, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC3_VA_BASEP, EXIRC_INT1, MASK_INT1, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC3_VA_BASEP, EXIRC_INT2, MASK_INT2, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC3_VA_BASEP, EXIRC_INT3, MASK_INT3, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC4_VA_BASEP, EXIRC_INT0, MASK_INT0, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC4_VA_BASEP, EXIRC_INT1, MASK_INT1, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC4_VA_BASEP, EXIRC_INT2, MASK_INT2, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC4_VA_BASEP, EXIRC_INT3, MASK_INT3, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC5_VA_BASEP, EXIRC_INT0, MASK_INT0, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC5_VA_BASEP, EXIRC_INT1, MASK_INT1, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC5_VA_BASEP, EXIRC_INT2, MASK_INT2, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{EXIRC5_VA_BASEP, EXIRC_INT3, MASK_INT3, EXIRC_INT_ENABLE, EXIRC_LVL_HIGH},
{NULL, 0, 0, 0, 0}
};

static inline void __iomem *get_exirc_chip_data(unsigned int irq,
						unsigned long *bit)
{
	unsigned int offset_irq = irq - EXTINT_START;
	void __iomem *base = exirq[offset_irq].base;
	*bit = exirq[offset_irq].mask;
	return base;
}

void exirc_mask_irq(unsigned int irq)
{
	unsigned long mask, bit;
	void __iomem *base = get_exirc_chip_data(irq, &bit);

	mask = readl((base + EIENB));

	writel(mask & ~bit, (base + EIENB));
}

void exirc_unmask_irq(unsigned int irq)
{
	unsigned long mask, bit;
	void __iomem *base = get_exirc_chip_data(irq, &bit);

	mask = readl((base + EIENB));

	writel(mask | bit, (base + EIENB));
}

void __init exirc_init(void)
{
	int i;
	unsigned int mask;
	unsigned int lvl;
	void __iomem *base;

	for (i = 0; exirq[i].base;) {
		mask = 0;
		lvl = 0;
		base = exirq[i].base;
		for (; exirq[i].base == base; i++) {
			if (exirq[i].enable == EXIRC_INT_ENABLE) {
				mask |= exirq[i].mask;
				lvl |= (exirq[i].lvl << (exirq[i].irq*2));
			}
		}
		writel(0x0, (base + EIENB));
		writel(lvl, (base + EILM));
		writel(0x0, (base + EIREQ));
		writel(mask, (base + EIENB));
	}
}

