/*
 * linux/arch/arm/mach-mb8aa0350/mb8aa0350_devkit.h
 *
 * Copyright (C) 2008 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARCH_MB8AA0350_DEVKIT_H
#define __ASM_ARCH_MB8AA0350_DEVKIT_H

#include <asm/arch/mb8aa0350.h>

#define TIMER0_VA_BASE		IO_ADDRESS(MB8AA0350_TIMER_BASE)
#define TIMER1_VA_BASE		(IO_ADDRESS(MB8AA0350_TIMER_BASE) + 0x20)

#define VA_VIC_BASE		IO_ADDRESS(MB8AA0350_VIC0_BASE)

#define VA_VIC0_BASE		IO_ADDRESS(MB8AA0350_VIC0_BASE)
#define VA_VIC1_BASE		IO_ADDRESS(MB8AA0350_VIC1_BASE)

#define EXIRC0_VA_BASE		IO_ADDRESS(MB8AA0350_EXIRC0_BASE)
#define EXIRC1_VA_BASE		IO_ADDRESS(MB8AA0350_EXIRC1_BASE)
#define EXIRC2_VA_BASE		IO_ADDRESS(MB8AA0350_EXIRC2_BASE)
#define EXIRC3_VA_BASE		IO_ADDRESS(MB8AA0350_EXIRC3_BASE)
#define EXIRC4_VA_BASE		IO_ADDRESS(MB8AA0350_EXIRC4_BASE)
#define EXIRC5_VA_BASE		IO_ADDRESS(MB8AA0350_EXIRC5_BASE)

extern void exirc_init(void);
extern void exirc_mask_irq(unsigned int irq);
extern void exirc_unmask_irq(unsigned int irq);

#endif /* __ASM_ARCH_MB8AA0350_DEVKIT_H */
