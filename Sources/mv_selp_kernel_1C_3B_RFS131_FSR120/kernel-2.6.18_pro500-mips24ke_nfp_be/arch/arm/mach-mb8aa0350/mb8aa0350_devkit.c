/*
 *  linux/arch/arm/mach-mb8aa0350/mb8aa0350_devkit.c
 *
 * Copyright (C) 2008 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <linux/init.h>
#include <linux/device.h>
#include <linux/sysdev.h>
#include <linux/platform_device.h>
#include <asm/hardware.h>
#include <linux/io.h>
#include <linux/irq.h>
#include <asm/mach-types.h>
#include <asm/arch/mb8aa0350.h>
#include <asm/mach/arch.h>
#include <asm/mach/map.h>
#include <asm/mach/time.h>
#include <linux/serial_8250.h>

#include "core.h"

static struct map_desc mb8aa0350_io_desc[] __initdata = {
	{
		.virtual = IO_ADDRESS(MB8AA0350_UART0_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_UART0_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_UART1_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_UART1_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_UART2_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_UART2_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_VIC0_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_VIC0_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_VIC1_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_VIC1_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_TIMER_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_TIMER_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_EXIRC0_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_EXIRC0_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_EXIRC1_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_EXIRC1_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_EXIRC2_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_EXIRC2_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_EXIRC3_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_EXIRC3_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_EXIRC4_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_EXIRC4_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_EXIRC5_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_EXIRC5_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_CPLD_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_CPLD_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	},
	{
		.virtual = IO_ADDRESS(MB8AA0350_CRGLP_BASE),
		.pfn     = __phys_to_pfn(MB8AA0350_CRGLP_BASE),
		.length  = 0x1000,
		.type    = MT_DEVICE
	}
};

static struct resource fmac3h_resources[] = {
	[0] = {
		.start = MB8AA0350_ETH_BASE,
		.end   = MB8AA0350_ETH_BASE + MB8AA0350_ETH_SIZE - 1,
		.flags = IORESOURCE_MEM,
	},
	[1] = {
		.start = IRQ_ETHER,
		.end   = IRQ_ETHER,
		.flags = IORESOURCE_IRQ,
	},
};

static struct platform_device fmac3h_device = {
	.name		= "F_MAC3H",
	.id		= 0,
	.num_resources	= ARRAY_SIZE(fmac3h_resources),
	.resource	= fmac3h_resources,
};

static struct plat_serial8250_port fuart3_ports[] = {
	{
		.membase = (void *)IO_ADDRESS(UART0_BASE),
		.mapbase = UART0_BASE,
		.iotype = UPIO_MEM,
		.regshift = 2,
		.irq = IRQ_UART0,
		.uartclk = UART_HZ,
		.flags = UPF_BOOT_AUTOCONF,
	 },
	{
		.membase = (void *)IO_ADDRESS(UART1_BASE),
		.mapbase = UART1_BASE,
		.iotype = UPIO_MEM,
		.regshift = 2,
		.irq = IRQ_UART1,
		.uartclk = UART_HZ,
		.flags = UPF_BOOT_AUTOCONF,
	},
	{
		.membase = (void *)IO_ADDRESS(UART2_BASE),
		.mapbase = UART2_BASE,
		.iotype = UPIO_MEM,
		.regshift = 2,
		.irq = IRQ_UART2,
		.uartclk = UART_HZ,
		.flags = UPF_BOOT_AUTOCONF,
	 },
};

static struct platform_device fuart_device = {
	.name		= "serial8250",
	.id		= 0,
	.dev		= {
		.platform_data = &fuart3_ports,
	},
};

static struct platform_device *mb8aa0350_devs[] __initdata = {
#if defined(CONFIG_FMAC3H) || defined(CONFIG_FMAC3H_MODULE)
	&fmac3h_device,
#endif
	&fuart_device,
};

static void __init mb8aa0350_init_machine(void)
{
	platform_add_devices(mb8aa0350_devs, ARRAY_SIZE(mb8aa0350_devs));
}

static struct sys_timer mb8aa0350_timer = {
	.init		= mb8aa0350_timer_init,
};

static void __init mb8aa0350_init_irq(void)
{
	/* Setup VIC0 and VIC1. */
	vic_init((void *)VA_VIC0_BASE, 0, MB8AA0350_VIC0_VALID_IRQ_MASK);
	vic_init((void *)VA_VIC1_BASE, 32, MB8AA0350_VIC1_VALID_IRQ_MASK);

	/* Setup External Interrupt Controller (EXIRC). */
	exirc_init();
}

static void __init mb8aa0350_map_io(void)
{
	iotable_init(mb8aa0350_io_desc, ARRAY_SIZE(mb8aa0350_io_desc));

#ifdef CONFIG_KGDB_8250
	kgdb8250_add_platform_port(CONFIG_KGDB_PORT_NUM,
					&fuart3_ports[CONFIG_KGDB_PORT_NUM]);
#endif
}

MACHINE_START(MB8AA0350_DEVKIT, "Fujitsu MB8AA0350 DEVKIT")
	.phys_io	= IO_PHYS,
	.io_pg_offst	= 0,
	.boot_params	= 0x0,
	.map_io		= mb8aa0350_map_io,
	.init_irq	= mb8aa0350_init_irq,
	.timer		= &mb8aa0350_timer,
	.init_machine	= mb8aa0350_init_machine,
MACHINE_END
