/*
 *  linux/arch/arm/mach-mb8aa0350/core.c
 *
 * Copyright (C) 2007-08 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */

#include <linux/types.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/interrupt.h>
#include <linux/sched.h>
#include <linux/irq.h>
#include <linux/clocksource.h>
#include <linux/clockchips.h>

#include <asm/hardware.h>
#include <linux/io.h>
#include <asm/mach/time.h>
#include <asm/hardware/arm_timer.h>

#include "core.h"

/*
 * TIMER_INTERVAL is APB cycle times for 10ms.
 * So the expression is as above.
 * Reffer to include/asm-arm/arch-mb8aa0350/mb8aa0350.h
 */
#define TIMER_INTERVAL		(MB8AA0350_APB_TIMER_HZ/100)

#define TIMER_RELOAD		(TIMER_INTERVAL)
#define TIMER_CTRL_DIVISOR	(TIMER_CTRL_DIV1)

static void timer_set_mode(enum clock_event_mode mode,
			   struct clock_event_device *clk)
{
	unsigned long ctrl;

	switch (mode) {
	case CLOCK_EVT_PERIODIC:
		writel(TIMER_RELOAD, TIMER0_VA_BASE + TIMER_LOAD);

		ctrl = TIMER_CTRL_PERIODIC;
		ctrl |= TIMER_CTRL_32BIT | TIMER_CTRL_IE | TIMER_CTRL_ENABLE;
		break;
	case CLOCK_EVT_ONESHOT:
		/* period set, and timer enabled in 'next_event' hook */
		ctrl = TIMER_CTRL_ONESHOT;
		ctrl |= TIMER_CTRL_32BIT | TIMER_CTRL_IE;
		break;
	default:
		ctrl = 0;
	}

	writel(ctrl, TIMER0_VA_BASE + TIMER_CTRL);
}

static void timer_set_next_event(unsigned long evt,
				struct clock_event_device *unused)
{
	unsigned long ctrl = readl(TIMER0_VA_BASE + TIMER_CTRL);

	writel(evt, TIMER0_VA_BASE + TIMER_LOAD);
	writel(ctrl | TIMER_CTRL_ENABLE, TIMER0_VA_BASE + TIMER_CTRL);
}

static struct clock_event_device timer0_clockevent =	 {
	.name		= "timer0",
	.shift		= 32,
	.capabilities   = CLOCK_CAP_TICK | CLOCK_CAP_UPDATE |
			CLOCK_CAP_NEXTEVT | CLOCK_CAP_PROFILE,
	.set_mode	= timer_set_mode,
	.set_next_event	= timer_set_next_event,
};

static void __init mb8aa0350_clockevents_init(void)
{
	timer0_clockevent.mult =
		div_sc(MB8AA0350_APB_TIMER_HZ, NSEC_PER_SEC,
				timer0_clockevent.shift);
	timer0_clockevent.max_delta_ns =
		clockevent_delta2ns(0xffffffff, &timer0_clockevent);
	timer0_clockevent.min_delta_ns =
		clockevent_delta2ns(0xf, &timer0_clockevent);

	register_global_clockevent(&timer0_clockevent);
}

static irqreturn_t mb8aa0350_timer_interrupt(int irq, void *dev_id,
						struct pt_regs *regs)
{
	struct clock_event_device *evt = &timer0_clockevent;

	/* Clear timer interrupt */
	writel(1, TIMER0_VA_BASE + TIMER_INTCLR);

	evt->event_handler(regs);

	return IRQ_HANDLED;
}

static struct irqaction mb8aa0350_timer_irq = {
	.name		= "Timer Tick",
	.flags		= IRQF_DISABLED | IRQF_TIMER,
	.handler	= mb8aa0350_timer_interrupt,
};

static cycle_t mb8aa0350_get_cycles(void)
{
	return ~readl(TIMER1_VA_BASE + TIMER_VALUE);
}

static struct clocksource clocksource_mb8aa0350 = {
	.name	= "timer1",
	.rating	= 200,
	.read	= mb8aa0350_get_cycles,
	.mask	= CLOCKSOURCE_MASK(32),
	.shift	= 20,
	.is_continuous  = 1,
};

static void __init mb8aa0350_clocksource_init(void)
{
	/* setup timer 1 as free-running clocksource */
	writel(0, TIMER1_VA_BASE + TIMER_CTRL);
	writel(0xffffffff, TIMER1_VA_BASE + TIMER_LOAD);
	writel(0xffffffff, TIMER1_VA_BASE + TIMER_VALUE);
	writel(TIMER_CTRL_32BIT | TIMER_CTRL_ENABLE | TIMER_CTRL_PERIODIC,
		TIMER1_VA_BASE + TIMER_CTRL);

	clocksource_mb8aa0350.mult =
		clocksource_khz2mult((MB8AA0350_APB_TIMER_HZ/1000),
				clocksource_mb8aa0350.shift);
	clocksource_register(&clocksource_mb8aa0350);
}

void __init mb8aa0350_timer_init()
{

	/* Disable timer1, timer2. */
	writel(0, TIMER0_VA_BASE + TIMER_CTRL);
	writel(0, TIMER1_VA_BASE + TIMER_CTRL);

	/* Set up timer interrupt handler and flags. */
	setup_irq(IRQ_TIMER0, &mb8aa0350_timer_irq);

	mb8aa0350_clocksource_init();
	mb8aa0350_clockevents_init();
}
