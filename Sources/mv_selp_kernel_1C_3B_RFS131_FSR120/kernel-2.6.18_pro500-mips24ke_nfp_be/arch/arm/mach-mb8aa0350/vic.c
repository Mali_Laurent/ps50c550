/*
 * linux/arch/arm/mach-mb8aa0350/vic.c
 *
 * Copyright (C) 2007 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#include <linux/init.h>
#include <linux/irq.h>
#include <asm/hardware.h>
#include <linux/io.h>
#include <linux/irq.h>

#include "core.h"
#include "vic.h"

static void vic_mask_irq(unsigned int irq)
{
	u32 vic_irq;
	void __iomem *base = get_irq_chip_data(irq);

	vic_irq = irq & 31;
	writel(1 << vic_irq, base + VIC_INT_ENABLE_CLEAR);
	if (irq >= EXTINT_START && irq <= EXTINT_END)
		exirc_mask_irq(irq);
}

static void vic_unmask_irq(unsigned int irq)
{
	u32 vic_irq;
	void __iomem *base = get_irq_chip_data(irq);

	vic_irq = irq & 31;
	writel(1 << vic_irq, base + VIC_INT_ENABLE);
	if (irq >= EXTINT_START && irq <= EXTINT_END)
		exirc_unmask_irq(irq);
}

static struct irq_chip vic_chip = {
	.name   = "VIC",
	.ack    = vic_mask_irq,
	.mask   = vic_mask_irq,
	.unmask = vic_unmask_irq,
};

/*
 * vic_init - initialize a vectored interrupt controller
 * @base: iomem base address
 * @irq_start: starting interrupt number
 * @vic_sources: bitmask of interrupt sources to allow
 */
void __init vic_init(void __iomem *base, unsigned int irq_start,
			   u32 vic_sources)
{
	unsigned int i;

	writel(0, base + VIC_INT_SELECT);
	writel(0, base + VIC_INT_ENABLE);
	writel(~0, base + VIC_INT_ENABLE_CLEAR);
	writel(0, base + VIC_ITCR);
	writel(~0, base + VIC_INT_SOFT_CLEAR);

	/* Clear current interrupt vector address. */
	writel(0, base + VIC_ADDRESS);

	for (i = 0; i < 32; i++) {
		unsigned int irq = irq_start + i;
		void __iomem *reg = base + VIC_VECT_ADDR0 + (i * 4);

		/* Set up irq vector address for all interrupts.
		 * irq vector address: 0xffff0018.
		 */
		writel(IRQ_VECTOR_ADDR, reg);

		set_irq_chip(irq, &vic_chip);
		set_irq_chip_data(irq, base);

		if (vic_sources & (1 << i)) {
			set_irq_handler(irq, handle_level_irq);
			set_irq_flags(irq, IRQF_VALID | IRQF_PROBE);
		}
	}
}
