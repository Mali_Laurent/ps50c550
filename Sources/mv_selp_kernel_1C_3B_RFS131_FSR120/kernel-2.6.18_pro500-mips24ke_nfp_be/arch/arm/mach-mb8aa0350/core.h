/*
 * linux/arch/arm/mach-mb8aa035a/core.h
 *
 * Copyright (C) 2007-2008 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARCH_MB8AA0350_CORE_H
#define __ASM_ARCH_MB8AA0350_CORE_H


#ifdef CONFIG_MACH_MB8AA0350_DEVKIT
#include "mb8aa0350_devkit.h"
#include "vic.h"
#endif

extern void mb8aa0350_timer_init(void);

#endif /* __ASM_ARCH_MB8AA0350_CORE_H */
