/*
 * linux/arch/arm/mach-mb8aa0350/vic.h
 *
 * Copyright (C) 2007 Fujitsu
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
#ifndef __ASM_ARCH_MB8AA0350_VIC_H
#define __ASM_ARCH_MB8AA0350_VIC_H

#define VIC_IRQ_STATUS			0x00
#define VIC_FIQ_STATUS			0x04
#define VIC_RAW_STATUS			0x08
#define VIC_INT_SELECT			0x0c
#define VIC_INT_ENABLE			0x10
#define VIC_INT_ENABLE_CLEAR		0x14
#define VIC_INT_SOFT			0x18
#define VIC_INT_SOFT_CLEAR		0x1c
#define VIC_PROTECT			0x20
#define VIC_VECT_ADDR0			0x100
#define VIC_ADDRESS			0xf00

#define VIC_ITCR			0x300

#define IRQ_VECTOR_ADDR			0xffff0018

extern void vic_init(void __iomem *base, unsigned int irq_start,
			u32 vic_sources);

#endif /* __ASM_ARCH_MB8AA0350_VIC_H */
