/*
 * This file is subject to the terms and conditions of the GNU General Public
 * License.  See the file "COPYING" in the main directory of this archive
 * for more details.
 *
 * Copyright (C) 1994 - 1999, 2000, 01, 06 Ralf Baechle
 * Copyright (C) 1995, 1996 Paul M. Antoine
 * Copyright (C) 1998 Ulf Carlsson
 * Copyright (C) 1999 Silicon Graphics, Inc.
 * Kevin D. Kissell, kevink@mips.com and Carsten Langgaard, carstenl@mips.com
 * Copyright (C) 2000, 01 MIPS Technologies, Inc.
 * Copyright (C) 2002, 2003, 2004, 2005  Maciej W. Rozycki
 *
 * KGDB specific changes - Manish Lachwani (mlachwani@mvista.com)
 */
#include <linux/init.h>
#include <linux/mm.h>
#include <linux/module.h>
#include <linux/sched.h>
#include <linux/smp.h>
#include <linux/smp_lock.h>
#include <linux/spinlock.h>
#include <linux/unistd.h>
#include <linux/kallsyms.h>
#include <linux/bootmem.h>
#include <linux/kgdb.h>

#include <asm/bootinfo.h>
#include <asm/branch.h>
#include <asm/break.h>
#include <asm/cpu.h>
#include <asm/dsp.h>
#include <asm/fpu.h>
#include <asm/mipsregs.h>
#include <asm/mipsmtregs.h>
#include <asm/module.h>
#include <asm/pgtable.h>
#include <asm/ptrace.h>
#include <asm/sections.h>
#include <asm/system.h>
#include <asm/tlbdebug.h>
#include <asm/traps.h>
#include <asm/uaccess.h>
#include <asm/mmu_context.h>
#include <asm/watch.h>
#include <asm/types.h>
#include <asm/kdebug.h>

extern asmlinkage void handle_int(void);
extern asmlinkage void handle_tlbm(void);
extern asmlinkage void handle_tlbl(void);
extern asmlinkage void handle_tlbs(void);
extern asmlinkage void handle_adel(void);
extern asmlinkage void handle_ades(void);
extern asmlinkage void handle_ibe(void);
extern asmlinkage void handle_dbe(void);
extern asmlinkage void handle_sys(void);
extern asmlinkage void handle_bp(void);
extern asmlinkage void handle_ri(void);
extern asmlinkage void handle_cpu(void);
extern asmlinkage void handle_ov(void);
extern asmlinkage void handle_tr(void);
extern asmlinkage void handle_fpe(void);
extern asmlinkage void handle_mdmx(void);
extern asmlinkage void handle_watch(void);
extern asmlinkage void handle_mt(void);
extern asmlinkage void handle_dsp(void);
extern asmlinkage void handle_mcheck(void);
extern asmlinkage void handle_reserved(void);

extern int fpu_emulator_cop1Handler(struct pt_regs *xcp,
	struct mips_fpu_struct *ctx, int has_fpu);

void (*board_be_init)(void);
int (*board_be_handler)(struct pt_regs *regs, int is_fixup);
void (*board_nmi_handler_setup)(void);
void (*board_ejtag_handler_setup)(void);
void (*board_bind_eic_interrupt)(int irq, int regset);

/*
 * These constant is for searching for possible module text segments.
 * MODULE_RANGE is a guess of how much space is likely to be vmalloced.
 */
#define MODULE_RANGE (8*1024*1024)

ATOMIC_NOTIFIER_HEAD(mips_die_head);
static spinlock_t die_notifier_lock = SPIN_LOCK_UNLOCKED;

int register_die_notifier(struct notifier_block *nb)
{
	int err = 0;
	unsigned long flags;

	spin_lock_irqsave(&die_notifier_lock, flags);
	err = atomic_notifier_chain_register(&mips_die_head, nb);
	spin_unlock_irqrestore(&die_notifier_lock, flags);

	return err;
}

/* VDLinux, based SELP.4.2.2.x default patch No.5,show fsult user stack, 2009-03-27 */
#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
void dump_pid_maps(struct task_struct* task)
{
        struct vm_area_struct * vma;
        struct file * file;
        char path_buf[256];
        unsigned long ino = 0;
        dev_t dev = 0; 

        printk(KERN_ALERT "-----------------------------------------------------------\n");
        printk(KERN_ALERT "* dump maps on pid (%d)\n", task->pid);
        printk(KERN_ALERT "-----------------------------------------------------------\n");
        down_read(&task->mm->mmap_sem);
        vma = task->mm->mmap;
        while(vma) {
                file = vma->vm_file;
                if (file) {
                        struct inode *inode = file->f_dentry->d_inode;
                        dev = inode->i_sb->s_dev;
                        ino = inode->i_ino;
                }

                printk(KERN_ALERT "%08lx-%08lx %c%c%c%c %08lx %02x:%02x %lu ",
                                vma->vm_start,
                                vma->vm_end,
                                vma->vm_flags & VM_READ ? 'r' : '-',
                                vma->vm_flags & VM_WRITE ? 'w' : '-',
                                vma->vm_flags & VM_EXEC ? 'x' : '-',
                                vma->vm_flags & VM_MAYSHARE ? 's' : 'p',
                                vma->vm_pgoff << PAGE_SHIFT,
                                MAJOR(dev), MINOR(dev), ino);

                if (file) {
                        char* p = d_path(file->f_dentry, file->f_vfsmnt, path_buf, 256);
                        if (!IS_ERR(p)) printk("%s", p);
                } 
                printk("\n");

                vma = vma->vm_next;
        }
        up_read(&task->mm->mmap_sem);
        printk(KERN_ALERT "-----------------------------------------------------------\n\n");
}

/*
 * SELP 4.3.x Trident 10.26 added 
 * mem dump for kernel area
 */
static void dump_mem_kernel(const char *str, unsigned long bottom, unsigned long top)
{
        unsigned long p = bottom & ~31;
        int i;

        printk("%s(0x%08lx to 0x%08lx)\n", str, bottom, top);

        for (p = bottom & ~31; p <= top;) {
                printk("%04lx: ", p & 0xffff);

                for (i = 0; i < 8; i++, p += 4) {
                        unsigned int val;

                        if (p < bottom || p > top)
                                printk("         ");
			else
                                printk("%08x ", *(unsigned long*)p);
                }   
                printk ("\n");
        }   
}

static void dump_mem(const char *str, unsigned long bottom, unsigned long top)
{
        unsigned long p = bottom & ~31;
        mm_segment_t fs; 
        int i;

        /*  
         * We need to switch to kernel mode so that we can use __get_user
         * to safely read from kernel space.  Note that we now dump the
         * code first, just in case the backtrace kills us.
         */
        fs = get_fs();
        set_fs(KERNEL_DS);

        printk("%s(0x%08lx to 0x%08lx)\n", str, bottom, top);

        for (p = bottom & ~31; p <= top;) {
                printk("%04lx: ", p & 0xffff);

                for (i = 0; i < 8; i++, p += 4) {
                        unsigned int val;

                        if (p < bottom || p > top)
                                printk("         ");
                        else {
                                __get_user(val, (unsigned long *)p);
                                printk("%08x ", val);
                        }   
                }   
                printk ("\n");
        }   

        set_fs(fs);
}

/*
 * SELP 4.2.x Chelsea Kernel Patch
 * Show user stack
 *  : assumes that user program uses frame pointer
 *  : TODO : consider context safety
 */
void show_user_stack(struct task_struct *task, struct pt_regs * regs)
{
       struct vm_area_struct *vma;

       vma = find_vma(task->mm, task->user_ssp);
       if (vma) {
               printk(
                               "task stack info : pid(%d) stack area (0x%08lx ~ 0x%08lx)\n",
                               (int)task->pid, vma->vm_start, vma->vm_end );
       }
       else
       {
	       printk("pid(%d) : printing user stack failed.\n", (int)task->pid);
	       return;
       }

       if( regs->regs[29] < vma->vm_start ) 
       {
		printk( "pid(%d) : seems stack overflow.\n"
			"  sp(%lx), stack vma (0x%08lx ~ 0x%08lx)\n"
			, (int)task->pid, regs->regs[29], vma->vm_start, vma->vm_end );
		
		return;
       }

       printk("-----------------------------------------------------------\n");
       printk("* dump user stack\n");
       printk("-----------------------------------------------------------\n");

       dump_mem( "dump user stack", regs->regs[29], task->user_ssp);
       printk("-----------------------------------------------------------\n\n");
}

#ifndef OFFSET_MASK
#define OFFSET_MASK (PAGE_SIZE - 1)
#endif

#define OFFSET_ALIGN_MASK OFFSET_MASK & ~(0x3)
#define DUMP_SIZE 0x400

/* Look up the first VMA which satisfies  addr < vm_end,  NULL if none. */
struct vm_area_struct*
find_vma2(struct mm_struct * mm, unsigned long addr)
{
    struct vm_area_struct *vma = NULL;
    
    if (mm) {
        struct rb_node * rb_node;
    
        rb_node = mm->mm_rb.rb_node;
        vma = NULL;

        while (rb_node) {
            struct vm_area_struct * vma_tmp;

            vma_tmp = rb_entry(rb_node,
                    struct vm_area_struct, vm_rb);

            if (vma_tmp->vm_end > addr) {
                if (vma_tmp->vm_start <= addr)
                {
                    vma = vma_tmp;
                    break;
                }
                rb_node = rb_node->rb_left;
            } else
                rb_node = rb_node->rb_right;
        }
    }
    return vma;
}

static inline unsigned long pfn_to_physaddr(unsigned long pfn)
{
	unsigned long pfn_addr;

#ifdef _ASM_MACH_TRIHIDTV_SPACES_H
	pfn_addr = pfn << PAGE_SHIFT;
#else
	printk("Function(pfn_to_physaddr()) Doesn't work in this machine..!\n")	
	return 0;
#endif

	return pfn_addr;
}

static void print_get_physaddr(unsigned long pfn,
		unsigned long p_addr,
		unsigned long k_addr)
{

	unsigned long dump_limit = k_addr + DUMP_SIZE;

	if((PAGE_MASK & k_addr) != (PAGE_MASK & dump_limit))
	{
		dump_limit = (PAGE_MASK & k_addr) + OFFSET_MASK;
	}

	/* Inform page table walking procedure */
	printk("\n===============================================================");
	printk("\nPhysical Addr:0x%08lx, Kernel Addr:0x%08lx",p_addr,k_addr);
	printk("\n===============================================================");
	printk("\n Page Table walking...! [Aligned addresses]");
	printk("\n PFN :0x%08lx\n PHYS_ADDR: 0x%08lx ", pfn, pfn_to_physaddr(pfn));
	printk("\n VIRT_ADDR: 0x%08lx + 0x%08lx"
			,PAGE_MASK & k_addr
			,OFFSET_ALIGN_MASK & p_addr);
	printk("\n VALUE    : 0x%08lx (addr:0x%08lx)",*(unsigned long*)k_addr,k_addr);
	printk("\n==============================================================");
	dump_mem("\nKERNEL_ADDR", k_addr, dump_limit);
	printk("\n==============================================================");
}

unsigned long get_physaddr(struct task_struct* tsk, unsigned long u_addr, int detail)
{
	struct page* page;
	struct vm_area_struct *vma = NULL;
	unsigned long k_addr,k_preaddr, p_addr,pfn,pfn_addr;

	/* find vma w/ user address */
	vma = find_vma2(tsk->mm, u_addr);
	if(!vma){
		printk("NO VMA\n");
		goto out;
	}

	/* get page struct w/ user address */
	page = follow_page(vma,u_addr,0);

	if(!page){
		printk("NO PAGE\n");
		goto out;
	}

	if(PageReserved(page))
		printk("[Zero Page]\n");

	if(PageHighMem(page))
		printk("[Higmem Page]\n");

	/* Calculate pfn, physical address, kernel mapped address w/ page struct */
	pfn  = page_to_pfn(page);

	printk("pfn: %ld  maxpfn:%ld\n",pfn,max_pfn);
	if(pfn > max_pfn){
		printk("PFN IS NOT VALID\n");
		goto out;
	}

	pfn_addr = pfn_to_physaddr(pfn);

	if(!pfn_addr){
		printk("CAN'T CONVERT PFN TO PHYSICAL ADDR\n");
		goto out;
	}

	p_addr = pfn_addr + (OFFSET_ALIGN_MASK & u_addr);
	k_preaddr = (unsigned long)page_address(page);
	
	if(!k_preaddr) {
		printk("KERNEL ADDRESS CONVERSION FAILED (k_preaddr:0x%08lx)\n",k_preaddr);
		goto out;
	}

	k_addr = k_preaddr + (OFFSET_ALIGN_MASK & u_addr);

	if(!virt_addr_valid(k_addr)){
		printk("INVALID KERNEL ADDRESS OR HIGHMEM ADDRESS\n");
	}
	
	if( detail == 1 )
		print_get_physaddr(pfn,p_addr,k_addr);
	else
		printk("Physical Addr:0x%08lx, Kernel Addr:0x%08lx\n",p_addr,k_addr);

	return k_addr;
out:
	return 0;
}

#ifdef CONFIG_SHOW_EPC_RA_INFO
void show_epc_ra(struct task_struct* task, struct pt_regs * regs) 
{
	struct vm_area_struct *epc_vma = NULL;
	struct vm_area_struct *ra_vma = NULL;
	unsigned long addr_epc, addr_ra;

	printk("\n");
	printk("--------------------------------------------------------------------------------------\n");
	printk("EPC, RA MEMINFO\n");
	printk("--------------------------------------------------------------------------------------\n");

	addr_epc = regs->cp0_epc - 0x400; 	// for 1024 byte
	addr_ra = regs->regs[31] - 0x800; 	// for 2048 byte

	// find vma including epc, ra
	// calculate a print range according to vma
	epc_vma = find_vma( task->mm, regs->cp0_epc);
	if ( epc_vma == NULL )
	{
		printk("No VMA for epc\n");
		return ;
	}
	if( addr_epc < epc_vma->vm_start )
		addr_epc = epc_vma->vm_start;

	ra_vma = find_vma( task->mm, regs->regs[31]);
	if ( ra_vma == NULL )
	{
		printk("No VMA for ra\n");
		return ;
	}
	if( addr_ra < ra_vma->vm_start )
		addr_ra = ra_vma->vm_start;
	
	// find a duplicated address rage case1
	if( (addr_ra<=regs->cp0_epc) && (regs->cp0_epc<regs->regs[31]) )
	{
		addr_ra = regs->cp0_epc + 0x4;
	}
	// find a duplicated address rage case2
	else if( (addr_epc<=regs->regs[31]) && (regs->regs[31]<regs->cp0_epc) )
	{
		addr_epc = regs->regs[31] + 0x4;
	}
	
	// dump
	printk("epc:%x, ra:%x\n", regs->cp0_epc, regs->regs[31]);
	printk("--------------------------------------------------------------------------------------\n");
	printk("epc physical addr information\n");
	get_physaddr(task, regs->cp0_epc, 0);
	printk("--------------------------------------------------------------------------------------\n");
	printk("ra physical addr information\n");
	get_physaddr(task, regs->regs[31], 0);

	printk("--------------------------------------------------------------------------------------\n");
	dump_mem("EPC meminfo ", addr_epc, regs->cp0_epc );
	printk("--------------------------------------------------------------------------------------\n");
	dump_mem("RA meminfo ", addr_ra, regs->regs[31]);
	printk("--------------------------------------------------------------------------------------\n");

	printk("\n");
}

/*
 * SELP 4.3.x Trident 10.26 added 
 * epc, ra area dump for kernel area
 */
static void show_epc_ra_kernel(struct pt_regs * regs) 
{
	unsigned long addr_epc, addr_ra;

	addr_epc = regs->cp0_epc - 0x400; 	// for 1024 byte
	addr_ra = regs->regs[31] - 0x800; 	// for 2048 byte

	if( regs->cp0_epc < 0x80000000 )
	{
		printk("[SELP] Invalid epc addr\n");
		return;
	}

	if( regs->regs[31]  < 0x80000000 )
	{
		printk("[SELP] Invalid ra addr\n");
		return;
	}

	if( addr_epc < 0x80000000 )
		addr_epc = 0x80000000;
	if( addr_ra < 0x80000000 )
		addr_ra = 0x80000000;

	// find a duplicated address rage case1
	if( (addr_ra<=regs->cp0_epc) && (regs->cp0_epc<regs->regs[31]) )
	{
		addr_ra = regs->cp0_epc + 0x4;
	}
	// find a duplicated address rage case2
	else if( (addr_epc<=regs->regs[31]) && (regs->regs[31]<regs->cp0_epc) )
	{
		addr_epc = regs->regs[31] + 0x4;
	}
	
	printk("--------------------------------------------------------------------------------------\n");
	printk("[SELP] DISPLAY EPC, RA in KERNEL Level\n");
	printk("epc:%x, ra:%x\n", regs->cp0_epc, regs->regs[31]);
	printk("--------------------------------------------------------------------------------------\n");
	dump_mem_kernel("EPC meminfo in kernel", addr_epc, regs->cp0_epc );
	printk("--------------------------------------------------------------------------------------\n");
	dump_mem_kernel("RA meminfo in kernel", addr_ra, regs->regs[31]);
	printk("--------------------------------------------------------------------------------------\n");

	printk("\n");

}

int is_valid_kernel_addr(unsigned long register_value)
{
	if(register_value < PAGE_OFFSET || !virt_addr_valid(register_value)) //includes checking NULL and user address
		return 0;
	else
		return 1;
}

void show_register_memory_kernel(struct pt_regs * regs) 
{
	unsigned long start_addr_for_printing = 0, end_addr_for_printing = 0;
	int register_num;

	printk("--------------------------------------------------------------------------------------\n");
	printk("REGISTER MEMORY INFO\n");
	printk("--------------------------------------------------------------------------------------\n");

	for(register_num = 0; register_num<32; register_num++) {

		printk("\n\n* REGISTER : r%d\n",register_num);

		start_addr_for_printing = (regs->regs[register_num] & PAGE_MASK) - 0x1000;   		//-4kbyte
		end_addr_for_printing = (regs->regs[register_num] & PAGE_MASK) + PAGE_SIZE + 0xfff;	//+about 8kbyte

		if(!is_valid_kernel_addr(regs->regs[register_num])) {
	
			printk("# Register value 0x%x is wrong address.\n", regs->regs[register_num]);
			printk("# We can't do anything.\n"); 
			printk("# So, we search next register.\n");

			continue;
		}

		if(!is_valid_kernel_addr(start_addr_for_printing)) {

			printk("# 'start_addr_for_printing' is wrong address.\n");
			printk("# So, we use just 'regs->regs[register_num] & PAGE_MASK)'\n");

			start_addr_for_printing = (regs->regs[register_num] & PAGE_MASK);
		}

		if(!is_valid_kernel_addr(end_addr_for_printing)) {

			printk("# 'end_addr_for_printing' is wrong address.\n");
			printk("# So, we use 'PAGE_ALIGN(regs->regs[register_num]) + PAGE_SIZE-1'\n");

			end_addr_for_printing = (regs->regs[register_num] & PAGE_MASK) + PAGE_SIZE-1;
		}

		// dump
		printk("# r%d register :0x%x, start_addr : 0x%x, end_addr : 0x%x\n", register_num, regs->regs[register_num], start_addr_for_printing, end_addr_for_printing);
		printk("--------------------------------------------------------------------------------------\n");
		dump_mem_kernel("meminfo ", start_addr_for_printing, end_addr_for_printing );
		printk("--------------------------------------------------------------------------------------\n");
		printk("\n");
	}
}
#endif

static int vd_action_sequence=0;

void show_info(struct task_struct *task, struct pt_regs * regs)
{
	preempt_disable();
	if( vd_action_sequence != 0 )
	{
		preempt_enable();
		return;
	}
	
	vd_action_sequence = 1;
	
#ifdef CONFIG_SHOW_EPC_RA_INFO
	show_epc_ra(task, regs);
#endif
	show_regs(regs);
	dump_pid_maps(task);
	show_user_stack(task, regs);
	preempt_enable();
}
#endif /* CONFIG_SHOW_FAULT_TRACE_INFO */

/*
 * This routine abuses get_user()/put_user() to reference pointers
 * with at least a bit of error checking ...
 */
void show_stack(struct task_struct *task, unsigned long *sp)
{
	const int field = 2 * sizeof(unsigned long);
	long stackdata;
	int i;

	if (!sp) {
		if (task && task != current)
			sp = (unsigned long *) task->thread.reg29;
		else
			sp = (unsigned long *) &sp;
	}

	printk("Stack :");
	i = 0;
	while ((unsigned long) sp & (PAGE_SIZE - 1)) {
		if (i && ((i % (64 / field)) == 0))
			printk("\n       ");
		if (i > 39) {
			printk(" ...");
			break;
		}

		if (__get_user(stackdata, sp++)) {
			printk(" (Bad stack address)");
			break;
		}

		printk(" %0*lx", field, stackdata);
		i++;
	}
	printk("\n");
}

void show_trace(struct task_struct *task, unsigned long *stack)
{
	const int field = 2 * sizeof(unsigned long);
	unsigned long addr;

	if (!stack) {
		if (task && task != current)
			stack = (unsigned long *) task->thread.reg29;
		else
			stack = (unsigned long *) &stack;
	}

	printk("Call Trace:");
#ifdef CONFIG_KALLSYMS
	printk("\n");
#endif
	while (!kstack_end(stack)) {
		addr = *stack++;
		if (__kernel_text_address(addr)) {
			printk(" [<%0*lx>] ", field, addr);
			print_symbol("%s\n", addr);
		}
	}
	printk("\n");
}

/*
 * The architecture-independent dump_stack generator
 */
void dump_stack(void)
{
	unsigned long stack;

	show_trace(current, &stack);
}
EXPORT_SYMBOL(dump_stack);

void show_code(unsigned int *pc)
{
	long i;

	printk("\nCode:");

	for(i = -3 ; i < 6 ; i++) {
		unsigned int insn;
		if (__get_user(insn, pc + i)) {
			printk(" (Bad address in epc)\n");
			break;
		}
		printk("%c%08x%c", (i?' ':'<'), insn, (i?' ':'>'));
	}
}

void show_regs(struct pt_regs *regs)
{
	const int field = 2 * sizeof(unsigned long);
	unsigned int cause = regs->cp0_cause;
	int i;

	printk("Cpu %d\n", smp_processor_id());

	/*
	 * Saved main processor registers
	 */
	for (i = 0; i < 32; ) {
		if ((i % 4) == 0)
			printk("$%2d   :", i);
		if (i == 0)
			printk(" %0*lx", field, 0UL);
		else if (i == 26 || i == 27)
			printk(" %*s", field, "");
		else
			printk(" %0*lx", field, regs->regs[i]);

		i++;
		if ((i % 4) == 0)
			printk("\n");
	}

	printk("Hi    : %0*lx\n", field, regs->hi);
	printk("Lo    : %0*lx\n", field, regs->lo);

	/*
	 * Saved cp0 registers
	 */
	printk("epc   : %0*lx ", field, regs->cp0_epc);
	print_symbol("%s ", regs->cp0_epc);
	printk("    %s\n", print_tainted());
	printk("ra    : %0*lx ", field, regs->regs[31]);
	print_symbol("%s\n", regs->regs[31]);

	printk("Status: %08x    ", (uint32_t) regs->cp0_status);

	if (current_cpu_data.isa_level == MIPS_CPU_ISA_I) {
		if (regs->cp0_status & ST0_KUO)
			printk("KUo ");
		if (regs->cp0_status & ST0_IEO)
			printk("IEo ");
		if (regs->cp0_status & ST0_KUP)
			printk("KUp ");
		if (regs->cp0_status & ST0_IEP)
			printk("IEp ");
		if (regs->cp0_status & ST0_KUC)
			printk("KUc ");
		if (regs->cp0_status & ST0_IEC)
			printk("IEc ");
	} else {
		if (regs->cp0_status & ST0_KX)
			printk("KX ");
		if (regs->cp0_status & ST0_SX)
			printk("SX ");
		if (regs->cp0_status & ST0_UX)
			printk("UX ");
		switch (regs->cp0_status & ST0_KSU) {
		case KSU_USER:
			printk("USER ");
			break;
		case KSU_SUPERVISOR:
			printk("SUPERVISOR ");
			break;
		case KSU_KERNEL:
			printk("KERNEL ");
			break;
		default:
			printk("BAD_MODE ");
			break;
		}
		if (regs->cp0_status & ST0_ERL)
			printk("ERL ");
		if (regs->cp0_status & ST0_EXL)
			printk("EXL ");
		if (regs->cp0_status & ST0_IE)
			printk("IE ");
	}
	printk("\n");

	printk("Cause : %08x\n", cause);

	cause = CAUSE_EXCCODE(cause);
	if (1 <= cause && cause <= 5)
		printk("BadVA : %0*lx\n", field, regs->cp0_badvaddr);

	printk("PrId  : %08x\n", read_c0_prid());
}

void show_registers(struct pt_regs *regs)
{
	show_regs(regs);
	print_modules();
	printk("Process %s (pid: %d, threadinfo=%p, task=%p)\n",
	        current->comm, current->pid, current_thread_info(), current);
	show_stack(current, (long *) regs->regs[29]);
	show_trace(current, (long *) regs->regs[29]);
	show_code((unsigned int *) regs->cp0_epc);
	printk("\n");
}

static DEFINE_RAW_SPINLOCK(die_lock);

#define DIE_LOOP_CNT	10000000
#define DILPLAY_CNT	100
NORET_TYPE void ATTRIB_NORET die(const char * str, struct pt_regs * regs)
{
	static unsigned int i=DIE_LOOP_CNT-1;
	static unsigned int display_cnt=DILPLAY_CNT-1;
	static int die_counter;
#ifdef CONFIG_MIPS_MT_SMTC
	unsigned long dvpret = dvpe();
#endif /* CONFIG_MIPS_MT_SMTC */

	printk("#############################################################################\n");
	printk("[SELP]Display die() message in Infinite loop for preserving Oops situation\n");
	printk("#############################################################################\n");

	local_irq_disable();
	die_counter++;
	while(1)
	{
		i++;
		if( i == DIE_LOOP_CNT )	
		{
			display_cnt++;
			i=0;
		}

		if( display_cnt == DILPLAY_CNT )	
		{
			console_verbose();
			spin_lock_irq(&die_lock);
			/*
			 * SELP 4.3.x Trident 10.26 added 
			 * epc, ra area dump for kernel area
			 */
#ifdef CONFIG_SHOW_EPC_RA_INFO
			show_epc_ra_kernel(regs); 
#endif
			bust_spinlocks(1);
#ifdef CONFIG_MIPS_MT_SMTC
			mips_mt_regdump(dvpret);
#endif /* CONFIG_MIPS_MT_SMTC */
			printk("%s[#%d]:\n", str, die_counter);
			show_registers(regs);
#ifdef CONFIG_SHOW_EPC_RA_INFO
			show_register_memory_kernel(regs); 
#endif
			spin_unlock_irq(&die_lock);

#if 0
			/* VDLinux, based SELP.4.2.1.x default patch No.16, 
			   USB dtvlogd, SP Team 2009-06-03 */
#ifdef CONFIG_DTVLOGD
			/* Add for DTVLOGD */
			do_dtvlog(5, NULL, 0);
#endif
#endif
			display_cnt = 0;
			printk("\n\n\n");
		}
	}
	local_irq_enable();

	do_exit(SIGSEGV);
}

extern const struct exception_table_entry __start___dbe_table[];
extern const struct exception_table_entry __stop___dbe_table[];

void __declare_dbe_table(void)
{
	__asm__ __volatile__(
	".section\t__dbe_table,\"a\"\n\t"
	".previous"
	);
}

/* Given an address, look for it in the exception tables. */
static const struct exception_table_entry *search_dbe_tables(unsigned long addr)
{
	const struct exception_table_entry *e;

	e = search_extable(__start___dbe_table, __stop___dbe_table - 1, addr);
	if (!e)
		e = search_module_dbetables(addr);
	return e;
}

asmlinkage void do_be(struct pt_regs *regs)
{
	const int field = 2 * sizeof(unsigned long);
	const struct exception_table_entry *fixup = NULL;
	int data = regs->cp0_cause & 4;
	int action = MIPS_BE_FATAL;

	/* XXX For now.  Fixme, this searches the wrong table ...  */
	if (data && !user_mode(regs))
		fixup = search_dbe_tables(exception_epc(regs));

	if (fixup)
		action = MIPS_BE_FIXUP;

	if (board_be_handler)
		action = board_be_handler(regs, fixup != 0);

	switch (action) {
	case MIPS_BE_DISCARD:
		return;
	case MIPS_BE_FIXUP:
		if (fixup) {
			regs->cp0_epc = fixup->nextinsn;
			return;
		}
		break;
	default:
		break;
	}

	/*
	 * Assume it would be too dangerous to continue ...
	 */
	printk(KERN_ALERT "%s bus error, epc == %0*lx, ra == %0*lx\n",
	       data ? "Data" : "Instruction",
	       field, regs->cp0_epc, field, regs->regs[31]);
	die_if_kernel("Oops", regs);
	force_sig(SIGBUS, current);
}

static inline int get_insn_opcode(struct pt_regs *regs, unsigned int *opcode)
{
	unsigned int __user *epc;

	epc = (unsigned int __user *) regs->cp0_epc +
	      ((regs->cp0_cause & CAUSEF_BD) != 0);
	if (!get_user(*opcode, epc))
		return 0;

	force_sig(SIGSEGV, current);
	return 1;
}

/*
 * ll/sc emulation
 */

#define OPCODE 0xfc000000
#define BASE   0x03e00000
#define RT     0x001f0000
#define OFFSET 0x0000ffff
#define LL     0xc0000000
#define SC     0xe0000000
#define SPEC3  0x7c000000
#define RD     0x0000f800
#define FUNC   0x0000003f
#define RDHWR  0x0000003b

/*
 * The ll_bit is cleared by r*_switch.S
 */

unsigned long ll_bit;

static struct task_struct *ll_task = NULL;

static inline void simulate_ll(struct pt_regs *regs, unsigned int opcode)
{
	unsigned long value, __user *vaddr;
	long offset;
	int signal = 0;

	/*
	 * analyse the ll instruction that just caused a ri exception
	 * and put the referenced address to addr.
	 */

	/* sign extend offset */
	offset = opcode & OFFSET;
	offset <<= 16;
	offset >>= 16;

	vaddr = (unsigned long __user *)
	        ((unsigned long)(regs->regs[(opcode & BASE) >> 21]) + offset);

	if ((unsigned long)vaddr & 3) {
		signal = SIGBUS;
		goto sig;
	}
	if (get_user(value, vaddr)) {
		signal = SIGSEGV;
		goto sig;
	}

	preempt_disable();

	if (ll_task == NULL || ll_task == current) {
		ll_bit = 1;
	} else {
		ll_bit = 0;
	}
	ll_task = current;

	preempt_enable();

#ifndef CONFIG_CPU_LX45XXX
	compute_return_epc(regs);
#endif

	regs->regs[(opcode & RT) >> 16] = value;

	return;

sig:
	force_sig(signal, current);
}

static inline void simulate_sc(struct pt_regs *regs, unsigned int opcode)
{
	unsigned long __user *vaddr;
	unsigned long reg;
	long offset;
	int signal = 0;

	/*
	 * analyse the sc instruction that just caused a ri exception
	 * and put the referenced address to addr.
	 */

	/* sign extend offset */
	offset = opcode & OFFSET;
	offset <<= 16;
	offset >>= 16;

	vaddr = (unsigned long __user *)
	        ((unsigned long)(regs->regs[(opcode & BASE) >> 21]) + offset);
	reg = (opcode & RT) >> 16;

	if ((unsigned long)vaddr & 3) {
		signal = SIGBUS;
		goto sig;
	}

	preempt_disable();

	if (ll_bit == 0 || ll_task != current) {
#ifndef CONFIG_CPU_LX45XXX
		compute_return_epc(regs);
#endif
		regs->regs[reg] = 0;
		preempt_enable();
		return;
	}

	preempt_enable();

	if (put_user(regs->regs[reg], vaddr)) {
		signal = SIGSEGV;
		goto sig;
	}

#ifndef CONFIG_CPU_LX45XXX
	compute_return_epc(regs);
#endif
	regs->regs[reg] = 1;

	return;

sig:
	force_sig(signal, current);
}

/*
 * ll uses the opcode of lwc0 and sc uses the opcode of swc0.  That is both
 * opcodes are supposed to result in coprocessor unusable exceptions if
 * executed on ll/sc-less processors.  That's the theory.  In practice a
 * few processors such as NEC's VR4100 throw reserved instruction exceptions
 * instead, so we're doing the emulation thing in both exception handlers.
 */
static inline int simulate_llsc(struct pt_regs *regs)
{
	unsigned int opcode;

	if (unlikely(get_insn_opcode(regs, &opcode)))
		return -EFAULT;

	if ((opcode & OPCODE) == LL) {
		simulate_ll(regs, opcode);
		return 0;
	}
	if ((opcode & OPCODE) == SC) {
		simulate_sc(regs, opcode);
		return 0;
	}

	return -EFAULT;			/* Strange things going on ... */
}

/*
 * Simulate trapping 'rdhwr' instructions to provide user accessible
 * registers not implemented in hardware.  The only current use of this
 * is the thread area pointer.
 */
static inline int simulate_rdhwr(struct pt_regs *regs)
{
	struct thread_info *ti = task_thread_info(current);
	unsigned int opcode;

	if (unlikely(get_insn_opcode(regs, &opcode)))
		return -EFAULT;

	if (unlikely(compute_return_epc(regs)))
		return -EFAULT;

	if ((opcode & OPCODE) == SPEC3 && (opcode & FUNC) == RDHWR) {
		int rd = (opcode & RD) >> 11;
		int rt = (opcode & RT) >> 16;
		switch (rd) {
			case 29:
				regs->regs[rt] = ti->tp_value;
				return 0;
			default:
				return -EFAULT;
		}
	}

	/* Not ours.  */
	return -EFAULT;
}

asmlinkage void do_ov(struct pt_regs *regs)
{
	siginfo_t info;

	die_if_kernel("Integer overflow", regs);

	info.si_code = FPE_INTOVF;
	info.si_signo = SIGFPE;
	info.si_errno = 0;
	info.si_addr = (void __user *) regs->cp0_epc;

#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_ov() : sending SIGFPE to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif
	force_sig_info(SIGFPE, &info, current);
}

/*
 * XXX Delayed fp exceptions when doing a lazy ctx switch XXX
 */
asmlinkage void do_fpe(struct pt_regs *regs, unsigned long fcr31)
{
	die_if_kernel("FP exception in kernel code", regs);

	MARK(kernel_trap_entry, "%d struct pt_regs %p", CAUSE_EXCCODE(regs->cp0_cause), regs);
	if (fcr31 & FPU_CSR_UNI_X) {
		int sig;

		preempt_disable();

#ifdef CONFIG_PREEMPT
		if (!is_fpu_owner()) {
			/* We might lose fpu before disabling preempt... */
			own_fpu();
			BUG_ON(!used_math());
			restore_fp(current);
		}
#endif
		/*
		 * Unimplemented operation exception.  If we've got the full
		 * software emulator on-board, let's use it...
		 *
		 * Force FPU to dump state into task/thread context.  We're
		 * moving a lot of data here for what is probably a single
		 * instruction, but the alternative is to pre-decode the FP
		 * register operands before invoking the emulator, which seems
		 * a bit extreme for what should be an infrequent event.
		 */
		save_fp(current);
		/* Ensure 'resume' not overwrite saved fp context again. */
		lose_fpu();

		preempt_enable();

		/* Run the emulator */
		sig = fpu_emulator_cop1Handler (regs, &current->thread.fpu, 1);

		preempt_disable();

		own_fpu();	/* Using the FPU again.  */
		/*
		 * We can't allow the emulated instruction to leave any of
		 * the cause bit set in $fcr31.
		 */
		current->thread.fpu.fcr31 &= ~FPU_CSR_ALL_X;

		/* Restore the hardware register state */
		restore_fp(current);

		preempt_enable();

		/* If something went wrong, signal */
		if (sig) {
#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        	{
               		sep_printk_start();

               		printk("do_fpe() : sending SIG:%d to %s, PID:%d\n", sig, current->comm, current->pid);
               		show_info(current, regs);
                
                	sep_printk_end();
        	}
#endif
			force_sig(sig, current);
		}

		return;
	}

#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_fpe() : sending SIGFPE to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif
	force_sig(SIGFPE, current);
}

asmlinkage void do_bp(struct pt_regs *regs)
{
	unsigned int opcode, bcode;
	siginfo_t info;

	die_if_kernel("Break instruction in kernel code", regs);

	if (get_insn_opcode(regs, &opcode))
		return;

	/*
	 * There is the ancient bug in the MIPS assemblers that the break
	 * code starts left to bit 16 instead to bit 6 in the opcode.
	 * Gas is bug-compatible, but not always, grrr...
	 * We handle both cases with a simple heuristics.  --macro
	 */
	bcode = ((opcode >> 6) & ((1 << 20) - 1));
	if (bcode < (1 << 10))
		bcode <<= 10;

	/*
	 * (A short test says that IRIX 5.3 sends SIGTRAP for all break
	 * insns, even for break codes that indicate arithmetic failures.
	 * Weird ...)
	 * But should we continue the brokenness???  --macro
	 */
	switch (bcode) {
	case BRK_OVERFLOW << 10:
	case BRK_DIVZERO << 10:
		if (bcode == (BRK_DIVZERO << 10))
			info.si_code = FPE_INTDIV;
		else
			info.si_code = FPE_INTOVF;
		info.si_signo = SIGFPE;
		info.si_errno = 0;
		info.si_addr = (void __user *) regs->cp0_epc;

#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_bp() : sending SIGFPE to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif
		force_sig_info(SIGFPE, &info, current);
		break;
	default:
		force_sig(SIGTRAP, current);
	}
}

asmlinkage void do_tr(struct pt_regs *regs)
{
	unsigned int opcode, tcode = 0;
	siginfo_t info;

	die_if_kernel("Trap instruction in kernel code", regs);

	if (get_insn_opcode(regs, &opcode))
		return;

	/* Immediate versions don't provide a code.  */
	if (!(opcode & OPCODE))
		tcode = ((opcode >> 6) & ((1 << 10) - 1));

	/*
	 * (A short test says that IRIX 5.3 sends SIGTRAP for all trap
	 * insns, even for trap codes that indicate arithmetic failures.
	 * Weird ...)
	 * But should we continue the brokenness???  --macro
	 */
	switch (tcode) {
	case BRK_OVERFLOW:
	case BRK_DIVZERO:
		if (tcode == BRK_DIVZERO)
			info.si_code = FPE_INTDIV;
		else
			info.si_code = FPE_INTOVF;
		info.si_signo = SIGFPE;
		info.si_errno = 0;
		info.si_addr = (void __user *) regs->cp0_epc;

#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_tr() : sending SIGFPE to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif
		force_sig_info(SIGFPE, &info, current);
		break;
	default:
		force_sig(SIGTRAP, current);
	}
}

asmlinkage void do_ri(struct pt_regs *regs)
{
	die_if_kernel("Reserved instruction in kernel code", regs);

	if (!cpu_has_llsc)
		if (!simulate_llsc(regs))
			return;

	if (!simulate_rdhwr(regs))
		return;

#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_ri() : sending SIGILL to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif
	force_sig(SIGILL, current);
}

asmlinkage void do_cpu(struct pt_regs *regs)
{
	unsigned int cpid;

	die_if_kernel("do_cpu invoked from kernel context!", regs);

	MARK(kernel_trap_entry, "%d struct pt_regs %p", CAUSE_EXCCODE(regs->cp0_cause), regs);
	cpid = (regs->cp0_cause >> CAUSEB_CE) & 3;

	switch (cpid) {
	case 0:
		if (!cpu_has_llsc)
			if (!simulate_llsc(regs)) {
				MARK(kernel_trap_exit, MARK_NOARGS);
				return;
			}

		if (!simulate_rdhwr(regs)) {
			MARK(kernel_trap_exit, MARK_NOARGS);
			return;
		}

		break;

	case 1:
		preempt_disable();

		own_fpu();
		if (used_math()) {	/* Using the FPU again.  */
			restore_fp(current);
		} else {			/* First time FPU user.  */
			init_fpu();
			set_used_math();
		}

		if (cpu_has_fpu) {
			preempt_enable();
		} else {
			int sig;
			preempt_enable();
			sig = fpu_emulator_cop1Handler(regs,
						&current->thread.fpu, 0);
			if (sig) {
#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
                {
                        sep_printk_start();

                        printk("do_cpu() : sending SIG:%d to %s, PID:%d\n", sig, current->comm, current->pid);
                        show_info(current, regs);

                        sep_printk_end();
                }
#endif
				force_sig(sig, current);
			}
#ifdef CONFIG_MIPS_MT_FPAFF
			else {
			/*
			 * MIPS MT processors may have fewer FPU contexts
			 * than CPU threads. If we've emulated more than
			 * some threshold number of instructions, force
			 * migration to a "CPU" that has FP support.
			 */
			 if(mt_fpemul_threshold > 0
			 && ((current->thread.emulated_fp++
			    > mt_fpemul_threshold))) {
			  /*
			   * If there's no FPU present, or if the
			   * application has already restricted
			   * the allowed set to exclude any CPUs
			   * with FPUs, we'll skip the procedure.
			   */
			  if (cpus_intersects(current->cpus_allowed,
			  			mt_fpu_cpumask)) {
			    cpumask_t tmask;

			    cpus_and(tmask,
					current->thread.user_cpus_allowed,
					mt_fpu_cpumask);
			    set_cpus_allowed(current, tmask);
			    current->thread.mflags |= MF_FPUBOUND;
			  }
			 }
			}
#endif /* CONFIG_MIPS_MT_FPAFF */
		}
		MARK(kernel_trap_exit, MARK_NOARGS);
		return;

	case 2:
	case 3:
		die_if_kernel("do_cpu invoked from kernel context!", regs);
		break;
	}

#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_cpu() : sending SIGILL to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif

	force_sig(SIGILL, current);
	MARK(kernel_trap_exit, MARK_NOARGS);
}

asmlinkage void do_mdmx(struct pt_regs *regs)
{
#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_mdmx() : sending SIGILL to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif

	force_sig(SIGILL, current);
}

asmlinkage void do_watch(struct pt_regs *regs)
{
	/*
	 * We use the watch exception where available to detect stack
	 * overflows.
	 */
	dump_tlb_all();
	show_regs(regs);
	panic("Caught WATCH exception - probably caused by stack overflow.");
}

asmlinkage void do_mcheck(struct pt_regs *regs)
{
	const int field = 2 * sizeof(unsigned long);
	int multi_match = regs->cp0_status & ST0_TS;

	show_regs(regs);

	if (multi_match) {
		printk("Index   : %0x\n", read_c0_index());
		printk("Pagemask: %0x\n", read_c0_pagemask());
		printk("EntryHi : %0*lx\n", field, read_c0_entryhi());
		printk("EntryLo0: %0*lx\n", field, read_c0_entrylo0());
		printk("EntryLo1: %0*lx\n", field, read_c0_entrylo1());
		printk("\n");
		dump_tlb_all();
	}

	show_code((unsigned int *) regs->cp0_epc);

	/*
	 * Some chips may have other causes of machine check (e.g. SB1
	 * graduation timer)
	 */
	panic("Caught Machine Check exception - %scaused by multiple "
	      "matching entries in the TLB.",
	      (multi_match) ? "" : "not ");
}

asmlinkage void do_mt(struct pt_regs *regs)
{
	int subcode;

	subcode = (read_vpe_c0_vpecontrol() & VPECONTROL_EXCPT)
			>> VPECONTROL_EXCPT_SHIFT;
	switch (subcode) {
	case 0:
		printk(KERN_DEBUG "Thread Underflow\n");
		break;
	case 1:
		printk(KERN_DEBUG "Thread Overflow\n");
		break;
	case 2:
		printk(KERN_DEBUG "Invalid YIELD Qualifier\n");
		break;
	case 3:
		printk(KERN_DEBUG "Gating Storage Exception\n");
		break;
	case 4:
		printk(KERN_DEBUG "YIELD Scheduler Exception\n");
		break;
	case 5:
		printk(KERN_DEBUG "Gating Storage Schedulier Exception\n");
		break;
	default:
		printk(KERN_DEBUG "*** UNKNOWN THREAD EXCEPTION %d ***\n",
			subcode);
		break;
	}
	die_if_kernel("MIPS MT Thread exception in kernel", regs);

#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_mt() : sending SIGILL to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif

	force_sig(SIGILL, current);
}


asmlinkage void do_dsp(struct pt_regs *regs)
{
	if (cpu_has_dsp)
		panic("Unexpected DSP exception\n");

#ifdef CONFIG_SHOW_FAULT_TRACE_INFO
        {
                sep_printk_start();

                printk("do_dsp() : sending SIGILL to %s, PID:%d\n", current->comm, current->pid);
                show_info(current, regs);
                
                sep_printk_end();
        }
#endif

	force_sig(SIGILL, current);
}

asmlinkage void do_reserved(struct pt_regs *regs)
{
	/*
	 * Game over - no way to handle this if it ever occurs.  Most probably
	 * caused by a new unknown cpu type or after another deadly
	 * hard/software error.
	 */
	show_regs(regs);
	panic("Caught reserved exception %ld - should not happen.",
	      (regs->cp0_cause & 0x7f) >> 2);
}

asmlinkage void do_default_vi(struct pt_regs *regs)
{
	show_regs(regs);
	panic("Caught unexpected vectored interrupt.");
}

/*
 * Some MIPS CPUs can enable/disable for cache parity detection, but do
 * it different ways.
 */
static inline void parity_protection_init(void)
{
	switch (current_cpu_data.cputype) {
	case CPU_24K:
	case CPU_34K:
	case CPU_5KC:
		write_c0_ecc(0x80000000);
		back_to_back_c0_hazard();
		/* Set the PE bit (bit 31) in the c0_errctl register. */
		printk(KERN_INFO "Cache parity protection %sabled\n",
		       (read_c0_ecc() & 0x80000000) ? "en" : "dis");
		break;
	case CPU_20KC:
	case CPU_25KF:
		/* Clear the DE bit (bit 16) in the c0_status register. */
		printk(KERN_INFO "Enable cache parity protection for "
		       "MIPS 20KC/25KF CPUs.\n");
		clear_c0_status(ST0_DE);
		break;
	default:
		break;
	}
}

asmlinkage void cache_parity_error(void)
{
	const int field = 2 * sizeof(unsigned long);
	unsigned int reg_val;

	/* For the moment, report the problem and hang. */
	printk("Cache error exception:\n");
	printk("cp0_errorepc == %0*lx\n", field, read_c0_errorepc());
	reg_val = read_c0_cacheerr();
	printk("c0_cacheerr == %08x\n", reg_val);

	printk("Decoded c0_cacheerr: %s cache fault in %s reference.\n",
	       reg_val & (1<<30) ? "secondary" : "primary",
	       reg_val & (1<<31) ? "data" : "insn");
	printk("Error bits: %s%s%s%s%s%s%s\n",
	       reg_val & (1<<29) ? "ED " : "",
	       reg_val & (1<<28) ? "ET " : "",
	       reg_val & (1<<26) ? "EE " : "",
	       reg_val & (1<<25) ? "EB " : "",
	       reg_val & (1<<24) ? "EI " : "",
	       reg_val & (1<<23) ? "E1 " : "",
	       reg_val & (1<<22) ? "E0 " : "");
	printk("IDX: 0x%08x\n", reg_val & ((1<<22)-1));

#if defined(CONFIG_CPU_MIPS32) || defined(CONFIG_CPU_MIPS64)
	if (reg_val & (1<<22))
		printk("DErrAddr0: 0x%0*lx\n", field, read_c0_derraddr0());

	if (reg_val & (1<<23))
		printk("DErrAddr1: 0x%0*lx\n", field, read_c0_derraddr1());
#endif

	panic("Can't handle the cache error!");
}

/*
 * SDBBP EJTAG debug exception handler.
 * We skip the instruction and return to the next instruction.
 */
void ejtag_exception_handler(struct pt_regs *regs)
{
	const int field = 2 * sizeof(unsigned long);
	unsigned long depc, old_epc;
	unsigned int debug;

	printk(KERN_DEBUG "SDBBP EJTAG debug exception - not handled yet, just ignored!\n");
	depc = read_c0_depc();
	debug = read_c0_debug();
	printk(KERN_DEBUG "c0_depc = %0*lx, DEBUG = %08x\n", field, depc, debug);
	if (debug & 0x80000000) {
		/*
		 * In branch delay slot.
		 * We cheat a little bit here and use EPC to calculate the
		 * debug return address (DEPC). EPC is restored after the
		 * calculation.
		 */
		old_epc = regs->cp0_epc;
		regs->cp0_epc = depc;
		__compute_return_epc(regs);
		depc = regs->cp0_epc;
		regs->cp0_epc = old_epc;
	} else
		depc += 4;
	write_c0_depc(depc);

#if 0
	printk(KERN_DEBUG "\n\n----- Enable EJTAG single stepping ----\n\n");
	write_c0_debug(debug | 0x100);
#endif
}

/*
 * NMI exception handler.
 */
void nmi_exception_handler(struct pt_regs *regs)
{
#ifdef CONFIG_MIPS_MT_SMTC
	unsigned long dvpret = dvpe();
	bust_spinlocks(1);
	printk("NMI taken!!!!\n");
	mips_mt_regdump(dvpret);
#else
	bust_spinlocks(1);
	printk("NMI taken!!!!\n");
#endif /* CONFIG_MIPS_MT_SMTC */
	die("NMI", regs);
	while(1) ;
}

#define VECTORSPACING 0x100	/* for EI/VI mode */

unsigned long ebase;
unsigned long exception_handlers[32];
unsigned long vi_handlers[64];

/*
 * As a side effect of the way this is implemented we're limited
 * to interrupt handlers in the address range from
 * KSEG0 <= x < KSEG0 + 256mb on the Nevada.  Oh well ...
 */
void *set_except_vector(int n, void *addr)
{
	unsigned long handler = (unsigned long) addr;
	unsigned long old_handler = exception_handlers[n];

	exception_handlers[n] = handler;
	if (n == 0 && cpu_has_divec) {
		*(volatile u32 *)(ebase + 0x200) = 0x08000000 |
		                                 (0x03ffffff & (handler >> 2));
		flush_icache_range(ebase + 0x200, ebase + 0x204);
	}
	return (void *)old_handler;
}

#ifdef CONFIG_CPU_MIPSR2_SRS
/*
 * MIPSR2 shadow register set allocation
 * FIXME: SMP...
 */

static struct shadow_registers {
	/*
	 * Number of shadow register sets supported
	 */
	unsigned long sr_supported;
	/*
	 * Bitmap of allocated shadow registers
	 */
	unsigned long sr_allocated;
} shadow_registers;

static void mips_srs_init(void)
{
	shadow_registers.sr_supported = ((read_c0_srsctl() >> 26) & 0x0f) + 1;
	printk(KERN_INFO "%d MIPSR2 register sets available\n",
	       shadow_registers.sr_supported);
	shadow_registers.sr_allocated = 1;	/* Set 0 used by kernel */
}

int mips_srs_max(void)
{
	return shadow_registers.sr_supported;
}

int mips_srs_alloc(void)
{
	struct shadow_registers *sr = &shadow_registers;
	int set;

again:
	set = find_first_zero_bit(&sr->sr_allocated, sr->sr_supported);
	if (set >= sr->sr_supported)
		return -1;

	if (test_and_set_bit(set, &sr->sr_allocated))
		goto again;

	return set;
}

void mips_srs_free(int set)
{
	struct shadow_registers *sr = &shadow_registers;

	clear_bit(set, &sr->sr_allocated);
}

static void *set_vi_srs_handler(int n, void *addr, int srs)
{
	unsigned long handler;
	unsigned long old_handler = vi_handlers[n];
	u32 *w;
	unsigned char *b;

	if (!cpu_has_veic && !cpu_has_vint)
		BUG();

	if (addr == NULL) {
		handler = (unsigned long) do_default_vi;
		srs = 0;
	} else
		handler = (unsigned long) addr;
	vi_handlers[n] = (unsigned long) addr;

	b = (unsigned char *)(ebase + 0x200 + n*VECTORSPACING);

	if (srs >= mips_srs_max())
		panic("Shadow register set %d not supported", srs);

	if (cpu_has_veic) {
		if (board_bind_eic_interrupt)
			board_bind_eic_interrupt (n, srs);
	} else if (cpu_has_vint) {
		/* SRSMap is only defined if shadow sets are implemented */
		if (mips_srs_max() > 1)
			change_c0_srsmap (0xf << n*4, srs << n*4);
	}

	if (srs == 0) {
		/*
		 * If no shadow set is selected then use the default handler
		 * that does normal register saving and a standard interrupt exit
		 */

		extern char except_vec_vi, except_vec_vi_lui;
		extern char except_vec_vi_ori, except_vec_vi_end;
#ifdef CONFIG_MIPS_MT_SMTC
		/*
		 * We need to provide the SMTC vectored interrupt handler
		 * not only with the address of the handler, but with the
		 * Status.IM bit to be masked before going there.
		 */
		extern char except_vec_vi_mori;
		const int mori_offset = &except_vec_vi_mori - &except_vec_vi;
#endif /* CONFIG_MIPS_MT_SMTC */
		const int handler_len = &except_vec_vi_end - &except_vec_vi;
		const int lui_offset = &except_vec_vi_lui - &except_vec_vi;
		const int ori_offset = &except_vec_vi_ori - &except_vec_vi;

		if (handler_len > VECTORSPACING) {
			/*
			 * Sigh... panicing won't help as the console
			 * is probably not configured :(
			 */
			panic ("VECTORSPACING too small");
		}

		memcpy (b, &except_vec_vi, handler_len);
#ifdef CONFIG_MIPS_MT_SMTC
		if (n > 7)
			printk("Vector index %d exceeds SMTC maximum\n", n);
		w = (u32 *)(b + mori_offset);
		*w = (*w & 0xffff0000) | (0x100 << n);
#endif /* CONFIG_MIPS_MT_SMTC */
		w = (u32 *)(b + lui_offset);
		*w = (*w & 0xffff0000) | (((u32)handler >> 16) & 0xffff);
		w = (u32 *)(b + ori_offset);
		*w = (*w & 0xffff0000) | ((u32)handler & 0xffff);
		flush_icache_range((unsigned long)b, (unsigned long)(b+handler_len));
	}
	else {
		/*
		 * In other cases jump directly to the interrupt handler
		 *
		 * It is the handlers responsibility to save registers if required
		 * (eg hi/lo) and return from the exception using "eret"
		 */
		w = (u32 *)b;
		*w++ = 0x08000000 | (((u32)handler >> 2) & 0x03fffff); /* j handler */
		*w = 0;
		flush_icache_range((unsigned long)b, (unsigned long)(b+8));
	}

	return (void *)old_handler;
}

void *set_vi_handler(int n, void *addr)
{
	return set_vi_srs_handler(n, addr, 0);
}

#else

static inline void mips_srs_init(void)
{
}

#endif /* CONFIG_CPU_MIPSR2_SRS */

/*
 * This is used by native signal handling
 */
asmlinkage int (*save_fp_context)(struct sigcontext *sc);
asmlinkage int (*restore_fp_context)(struct sigcontext *sc);

extern asmlinkage int _save_fp_context(struct sigcontext *sc);
extern asmlinkage int _restore_fp_context(struct sigcontext *sc);

extern asmlinkage int fpu_emulator_save_context(struct sigcontext *sc);
extern asmlinkage int fpu_emulator_restore_context(struct sigcontext *sc);

#ifdef CONFIG_SMP
static int smp_save_fp_context(struct sigcontext *sc)
{
	return cpu_has_fpu
	       ? _save_fp_context(sc)
	       : fpu_emulator_save_context(sc);
}

static int smp_restore_fp_context(struct sigcontext *sc)
{
	return cpu_has_fpu
	       ? _restore_fp_context(sc)
	       : fpu_emulator_restore_context(sc);
}
#endif

static inline void signal_init(void)
{
#ifdef CONFIG_SMP
	/* For now just do the cpu_has_fpu check when the functions are invoked */
	save_fp_context = smp_save_fp_context;
	restore_fp_context = smp_restore_fp_context;
#else
	if (cpu_has_fpu) {
		save_fp_context = _save_fp_context;
		restore_fp_context = _restore_fp_context;
	} else {
		save_fp_context = fpu_emulator_save_context;
		restore_fp_context = fpu_emulator_restore_context;
	}
#endif
}

#ifdef CONFIG_MIPS32_COMPAT

/*
 * This is used by 32-bit signal stuff on the 64-bit kernel
 */
asmlinkage int (*save_fp_context32)(struct sigcontext32 *sc);
asmlinkage int (*restore_fp_context32)(struct sigcontext32 *sc);

extern asmlinkage int _save_fp_context32(struct sigcontext32 *sc);
extern asmlinkage int _restore_fp_context32(struct sigcontext32 *sc);

extern asmlinkage int fpu_emulator_save_context32(struct sigcontext32 *sc);
extern asmlinkage int fpu_emulator_restore_context32(struct sigcontext32 *sc);

static inline void signal32_init(void)
{
	if (cpu_has_fpu) {
		save_fp_context32 = _save_fp_context32;
		restore_fp_context32 = _restore_fp_context32;
	} else {
		save_fp_context32 = fpu_emulator_save_context32;
		restore_fp_context32 = fpu_emulator_restore_context32;
	}
}
#endif

extern void cpu_cache_init(void);
extern void tlb_init(void);
extern void flush_tlb_handlers(void);

void __init per_cpu_trap_init(void)
{
	unsigned int cpu = smp_processor_id();
	unsigned int status_set = ST0_CU0;
#ifdef CONFIG_MIPS_MT_SMTC
	int secondaryTC = 0;
	int bootTC = (cpu == 0);

	/*
	 * Only do per_cpu_trap_init() for first TC of Each VPE.
	 * Note that this hack assumes that the SMTC init code
	 * assigns TCs consecutively and in ascending order.
	 */

	if (((read_c0_tcbind() & TCBIND_CURTC) != 0) &&
	    ((read_c0_tcbind() & TCBIND_CURVPE) == cpu_data[cpu - 1].vpe_id))
		secondaryTC = 1;
#endif /* CONFIG_MIPS_MT_SMTC */

	/*
	 * Disable coprocessors and select 32-bit or 64-bit addressing
	 * and the 16/32 or 32/32 FPR register model.  Reset the BEV
	 * flag that some firmware may have left set and the TS bit (for
	 * IP27).  Set XX for ISA IV code to work.
	 */
#ifdef CONFIG_64BIT
	status_set |= ST0_FR|ST0_KX|ST0_SX|ST0_UX;
#endif
	if (current_cpu_data.isa_level == MIPS_CPU_ISA_IV)
		status_set |= ST0_XX;
	change_c0_status(ST0_CU|ST0_MX|ST0_RE|ST0_FR|ST0_BEV|ST0_TS|ST0_KX|ST0_SX|ST0_UX,
			 status_set);

	if (cpu_has_dsp)
		set_c0_status(ST0_MX);

#ifdef CONFIG_CPU_MIPSR2
	write_c0_hwrena (0x0000000f); /* Allow rdhwr to all registers */
#endif

#ifdef CONFIG_MIPS_MT_SMTC
	if (!secondaryTC) {
#endif /* CONFIG_MIPS_MT_SMTC */

	/*
	 * Interrupt handling.
	 */
	if (cpu_has_veic || cpu_has_vint) {
		write_c0_ebase (ebase);
		/* Setting vector spacing enables EI/VI mode  */
		change_c0_intctl (0x3e0, VECTORSPACING);
	}
	if (cpu_has_divec) {
		if (cpu_has_mipsmt) {
			unsigned int vpflags = dvpe();
			set_c0_cause(CAUSEF_IV);
			evpe(vpflags);
		} else
			set_c0_cause(CAUSEF_IV);
	}
#ifdef CONFIG_MIPS_MT_SMTC
	}
#endif /* CONFIG_MIPS_MT_SMTC */

	cpu_data[cpu].asid_cache = ASID_FIRST_VERSION;
	TLBMISS_HANDLER_SETUP();

	atomic_inc(&init_mm.mm_count);
	current->active_mm = &init_mm;
	BUG_ON(current->mm);
	enter_lazy_tlb(&init_mm, current);

#ifdef CONFIG_MIPS_MT_SMTC
	if (bootTC) {
#endif /* CONFIG_MIPS_MT_SMTC */
		cpu_cache_init();
		tlb_init();
#ifdef CONFIG_MIPS_MT_SMTC
	} else if (!secondaryTC) {
		/*
		 * First TC in non-boot VPE must do subset of tlb_init()
		 * for MMU countrol registers.
		 */
		write_c0_pagemask(PM_DEFAULT_MASK);
		write_c0_wired(0);
	}
#endif /* CONFIG_MIPS_MT_SMTC */
}

/* Install CPU exception handler */
void __init set_handler (unsigned long offset, void *addr, unsigned long size)
{
	memcpy((void *)(ebase + offset), addr, size);
	flush_icache_range(ebase + offset, ebase + offset + size);
}

/* Install uncached CPU exception handler */
void __init set_uncached_handler (unsigned long offset, void *addr, unsigned long size)
{
#ifdef CONFIG_32BIT
	unsigned long uncached_ebase = KSEG1ADDR(ebase);
#endif
#ifdef CONFIG_64BIT
	unsigned long uncached_ebase = TO_UNCAC(ebase);
#endif

	memcpy((void *)(uncached_ebase + offset), addr, size);
}

void __init trap_init(void)
{
	extern char except_vec3_generic, except_vec3_r4000;
	extern char except_vec4;
	unsigned long i;

#if defined(CONFIG_KGDB)
	if (kgdb_early_setup)
		return;	/* Already done */
#endif

	if (cpu_has_veic || cpu_has_vint)
		ebase = (unsigned long) alloc_bootmem_low_pages (0x200 + VECTORSPACING*64);
	else
		ebase = CAC_BASE;

	mips_srs_init();

	per_cpu_trap_init();

	/*
	 * Copy the generic exception handlers to their final destination.
	 * This will be overriden later as suitable for a particular
	 * configuration.
	 */
	set_handler(0x180, &except_vec3_generic, 0x80);

	/*
	 * Setup default vectors
	 */
	for (i = 0; i <= 31; i++)
		set_except_vector(i, handle_reserved);

	/*
	 * Copy the EJTAG debug exception vector handler code to it's final
	 * destination.
	 */
	if (cpu_has_ejtag && board_ejtag_handler_setup)
		board_ejtag_handler_setup ();

	/*
	 * Only some CPUs have the watch exceptions.
	 */
	if (cpu_has_watch)
		set_except_vector(23, handle_watch);

	/*
	 * Initialise interrupt handlers
	 */
	if (cpu_has_veic || cpu_has_vint) {
		int nvec = cpu_has_veic ? 64 : 8;
		for (i = 0; i < nvec; i++)
			set_vi_handler(i, NULL);
	}
	else if (cpu_has_divec)
		set_handler(0x200, &except_vec4, 0x8);

	/*
	 * Some CPUs can enable/disable for cache parity detection, but does
	 * it different ways.
	 */
	parity_protection_init();

	/*
	 * The Data Bus Errors / Instruction Bus Errors are signaled
	 * by external hardware.  Therefore these two exceptions
	 * may have board specific handlers.
	 */
	if (board_be_init)
		board_be_init();

#ifdef CONFIG_CPU_LX45XXX
	/* It will be set in lx.c - Alan Liu */
#else
	set_except_vector(0, handle_int);
#endif
	set_except_vector(1, handle_tlbm);
	set_except_vector(2, handle_tlbl);
	set_except_vector(3, handle_tlbs);

	set_except_vector(4, handle_adel);
	set_except_vector(5, handle_ades);

	set_except_vector(6, handle_ibe);
	set_except_vector(7, handle_dbe);

	set_except_vector(8, handle_sys);
	set_except_vector(9, handle_bp);
	set_except_vector(10, handle_ri);
	set_except_vector(11, handle_cpu);
	set_except_vector(12, handle_ov);
	set_except_vector(13, handle_tr);

	if (current_cpu_data.cputype == CPU_R6000 ||
	    current_cpu_data.cputype == CPU_R6000A) {
		/*
		 * The R6000 is the only R-series CPU that features a machine
		 * check exception (similar to the R4000 cache error) and
		 * unaligned ldc1/sdc1 exception.  The handlers have not been
		 * written yet.  Well, anyway there is no R6000 machine on the
		 * current list of targets for Linux/MIPS.
		 * (Duh, crap, there is someone with a triple R6k machine)
		 */
		//set_except_vector(14, handle_mc);
		//set_except_vector(15, handle_ndc);
	}


	if (board_nmi_handler_setup)
		board_nmi_handler_setup();

	if (cpu_has_fpu && !cpu_has_nofpuex)
		set_except_vector(15, handle_fpe);

	set_except_vector(22, handle_mdmx);

	if (cpu_has_mcheck)
		set_except_vector(24, handle_mcheck);

	if (cpu_has_mipsmt)
		set_except_vector(25, handle_mt);

	if (cpu_has_dsp)
		set_except_vector(26, handle_dsp);

	if (cpu_has_vce)
		/* Special exception: R4[04]00 uses also the divec space. */
		memcpy((void *)(CAC_BASE + 0x180), &except_vec3_r4000, 0x100);
	else if (cpu_has_4kex)
		memcpy((void *)(CAC_BASE + 0x180), &except_vec3_generic, 0x80);
	else
		memcpy((void *)(CAC_BASE + 0x080), &except_vec3_generic, 0x80);

	signal_init();
#ifdef CONFIG_MIPS32_COMPAT
	signal32_init();
#endif

	flush_icache_range(ebase, ebase + 0x400);
	flush_tlb_handlers();
}
