/*
 * TTI(Shanghai),Co.Ltd
 * Alan Liu(Liu Hongming)
 * Copied from it8172 boards related files
 * 8/15/2003
 *
 * Routines for manipulating the time related(RTC,timer interrupt) on chips.
 *
 */

#include <linux/init.h>
#include <linux/kernel_stat.h>
#include <linux/sched.h>
#include <linux/spinlock.h>
#include <linux/irq.h>
#include <linux/clocksource.h>

#include <asm/mipsregs.h>
#include <asm/ptrace.h>
//#include <asm/it8172/it8172_int.h>
#include <asm/debug.h>

//#include <linux/mc146818rtc.h>
#include <asm/mc146818-time.h>
#include <linux/timex.h>
#include <linux/rtc.h>
#include <linux/interrupt.h>

#include <asm/trihidtv/trihidtv_irq.h>
#include <asm/trihidtv/trihidtv_int.h>
#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_addrspace.h>


static unsigned long timer_constant_value; /* Value to fill the timer2 Constant Value Register */
extern unsigned int mips_hpt_frequency;

extern void (*board_time_init)(void);

extern unsigned long (*rtc_mips_get_time)(void);
extern int (*rtc_mips_set_time)(unsigned long);
extern int (*rtc_mips_set_mmss)(unsigned long);
extern void (*mips_timer_ack)(void);

extern unsigned long (*do_gettimeoffset)(void);
extern void to_tm(unsigned long,struct rtc_time *);

/* usecs per counter cycle, shifted to left by 32 bits */
unsigned int sll32_usecs_per_cycle;

//extern struct clocksource clocksource_mips;

void trihidtv_time_init(void);
void trihidtv_timer_setup(struct irqaction *irq);
static unsigned long lx5180_rtc_get_time(void);
static int lx5180_rtc_set_time(unsigned long);

/* This is for machines which generate the exact clock. */
//#define USECS_PER_JIFFY (1000000/HZ)

/*
 * Figure out the timer constant filled into the TCVR2 at first for each time tick. 
 * Use the RTC to calculate the TCVR2.
 */
static unsigned long __init cal_timer_constant(void)
{
#if defined(CONFIG_TRIHIDTV_CPU_AUTODETECT) && !defined(CONFIG_MIPS_TRIHIDTV_PRO_AV_MIPS)
	unsigned int flags;
	unsigned short tmpsh;
	unsigned int tmplong;
	unsigned char save_control;
#endif

#ifdef CONFIG_TRIHIDTV_CPU_350M
	printk("\nCPU Clock Freq is set to 353.979538MHz in menuconfig\n");
	/*
	 * Should we use this value?
	 * We need get it on HOST MIPS?
	 * Based on the PLL precision???
	 * Alan Liu
	 */
	//mips_hpt_frequency = 353979538;
	mips_hpt_frequency = 353979538/2;
	return mips_hpt_frequency/HZ;

#elif defined(CONFIG_TRIHIDTV_CPU_425M)
	
       //unsigned short dcr = ReadRegHWord((volatile void *)0x1b000094);
	unsigned int dcr;
	unsigned short r1a4 = ReadRegHWord((volatile void *)0x1b0001a4);
	unsigned short r1a6 = ReadRegHWord((volatile void *)0x1b0001a6);
	dcr = (unsigned int)(r1a6<<16 | r1a4);
	printk("\ndcr is %08x\n",dcr);

	if((dcr == 0x0568 || dcr == 0x3fd33 || dcr == 0x3f168)){
		mips_hpt_frequency = 424000000/2;
		printk("\nCPU Clock Freq is set to 425MHz in menuconfig\n");
	} else{
		unsigned int NN = (dcr & 0xff) + 2;
		unsigned int MM = ((dcr & 0x300) >> 8) + 2;
		unsigned int KK = ((dcr & 0x3e000) >> 13) + 2;
		unsigned int rem, fout;
		unsigned int times;

		/*
		 * Need check later.
		 * Alan
		 */
		if(KK == 33)
			KK = 1;

		if((dcr & 0x400) !=0) 
			times = 1;
		else
			times = 2;
		
		fout = ((unsigned long long int)(24000000 * NN ))/((unsigned long long)(MM * KK * times));
#if 0
		rem = 24000000 % (MM * KK * times);
		fout = (24000000/(MM * KK * times)) * NN;
		fout += (rem * NN + (MM * KK))/(MM * KK * times);
#endif
		mips_hpt_frequency = fout/2;
		printk("\nWARNING! CPU Clock Freq is set to %u Hz instead of 425MHz!\n", fout);
       }
#endif

	/*
	 * Should we use this value?
	 * We need get it on HOST MIPS?
	 * Based on the PLL precision???
	 * Alan Liu
	 */
	//mips_hpt_frequency = 353979538;
//	mips_hpt_frequency = 353979538/2;
	return mips_hpt_frequency/HZ;

#ifdef CONFIG_TRIHIDTV_CPU_AUTODETECT

	local_irq_save(flags);
	
	tmpsh = ReadRegHWord(TCR2);
	tmpsh &= (~3);
	WriteRegHWord(TCR2, tmpsh); 

	tmpsh = ReadRegHWord(TIDR);
	tmpsh |= 2;
	WriteRegHWord(TIDR, tmpsh); 

	tmpsh = ReadRegHWord(TIRR);
	tmpsh |= 0x2;
	WriteRegHWord(TIRR, tmpsh);

	/* 
	 * set RTC_RA's 6-4 bits as 010
	 * What is the default value?
	 */	
	tmpsh = CMOS_READ(RTC_REG_A);
	CMOS_WRITE(tmpsh|0x20,RTC_REG_A);

	/* save here */
	save_control = CMOS_READ(RTC_CONTROL);

	/* enable updates for clock setting */
	CMOS_WRITE(save_control&~RTC_SET,RTC_CONTROL);

#define TIMER_LOAD_VALUE 0xffffffff

	WriteRegWord(TCVR2, TIMER_LOAD_VALUE); 

	/* Start counter exactly on falling edge of update flag */
	while (!(CMOS_READ(RTC_REG_A) & RTC_UIP));
	while (CMOS_READ(RTC_REG_A) & RTC_UIP);

	tmpsh = ReadRegHWord(TCR2);
	tmpsh |= 3;
	WriteRegHWord(TCR2, tmpsh);

//while(1){

	/* Read counter exactly on falling edge of update flag */
	while (!(CMOS_READ(RTC_REG_A) & RTC_UIP));
	while (CMOS_READ(RTC_REG_A) & RTC_UIP);
	/*
	 * Should NOT STOP Timer2's counting 
	 * or TRVR2's value is reset to 32'h02
	 */
	tmplong = 0;
	tmplong = ReadRegWord(TRVR2);
	mips_hpt_frequency = TIMER_LOAD_VALUE - tmplong;

#if 0
	dump_timer2_reg();
#endif
//}

	tmpsh = ReadRegHWord(TCR2);
	tmpsh &= ~3;
	WriteRegHWord(TCR2, tmpsh); 

	/* restore */
	CMOS_WRITE(save_control,RTC_CONTROL);

	/* restore interrupts */
	//__restore_flags(flags);
	local_irq_restore(flags);
		
	return (mips_hpt_frequency / HZ);
#endif
}

/*
 * Routine for rtc_get_time function pointer
 */
unsigned long lx5180_rtc_get_time(void)
{

#if 0
	unsigned int year, mon, day, hour, min, sec;
	unsigned char save_control;

	save_control = CMOS_READ(RTC_CONTROL);

	/* Freeze it. */
	CMOS_WRITE(save_control | RTC_SET, RTC_CONTROL);

	/* Read regs. */
	sec = CMOS_READ(RTC_SECONDS);
	min = CMOS_READ(RTC_MINUTES);
	hour = CMOS_READ(RTC_HOURS);

	/* format convertion  */
	if (!(save_control & RTC_24H))
	{
		if ((hour & 0xf) == 0xc)
		        hour &= 0x80;
	        if (hour & 0x80)
		        hour = (hour & 0xf) + 12;     
	}
	day = CMOS_READ(RTC_DAY_OF_MONTH);
	mon = CMOS_READ(RTC_MONTH);
	year = CMOS_READ(RTC_YEAR);

	/* Unfreeze clock. */
	CMOS_WRITE(save_control, RTC_CONTROL);

	if ((year += 1900) < 1970)
	        year += 100;
	return mktime(year, mon, day, hour, min, sec);
#else
	/* SELP Patch 2009.09.24 */
	// UTC set 1970.1.1
	//return mktime(2009,1,1,1,1,1);
	return mktime(1970,1,1,1,1,1);
#endif

}

/*
 * Routine for rtc_set_time function pointer.
 */
static int lx5180_rtc_set_time(unsigned long sec)
{
#if 0
	struct rtc_time tm;
	unsigned char save_control;

	/* convert */
	to_tm(sec,&tm);

	/* Freeze it. */
	save_control = CMOS_READ(RTC_CONTROL);
	CMOS_WRITE(save_control | RTC_SET, RTC_CONTROL);
	
	/* check each field one by one */
	if(tm.tm_year != CMOS_READ(RTC_YEAR)){
		CMOS_WRITE(tm.tm_year,RTC_YEAR);
	}

	if(tm.tm_mon != CMOS_READ(RTC_MONTH)){
		CMOS_WRITE(tm.tm_mon,RTC_MONTH);
	}

	if(tm.tm_mday != CMOS_READ(RTC_DAY_OF_MONTH)){
		CMOS_WRITE(tm.tm_mday,RTC_DAY_OF_MONTH);
	}

	if(tm.tm_hour != CMOS_READ(RTC_HOURS)){
		CMOS_WRITE(tm.tm_hour,RTC_HOURS);
	}

	if(tm.tm_min != CMOS_READ(RTC_MINUTES)){
		CMOS_WRITE(tm.tm_min,RTC_MINUTES);
	}

	if(tm.tm_sec != CMOS_READ(RTC_SECONDS)){
		CMOS_WRITE(tm.tm_sec,RTC_SECONDS);
	}

	/* Unfreeze clock. */
	CMOS_WRITE(save_control, RTC_CONTROL);

	return 0;	
#else
	return 0;	
#endif
}

#if 0
/*
 * Return the cycles left in timer.
 * Alan Liu
 */
cycle_t trihidtv_hpt_read()
{
	return (cycle_t)(timer_constant_value-ReadRegWord(TRVR2));
}
/*
 * we use this function to ack the timer interrupt
 * Alan Liu
 * 2007/7/25
 */
void trihidtv_timer_ack()
{
	unsigned short tmpsh;
	tmpsh = ReadRegHWord(TIRR);
	//clear timer2 interrupt request bit here...
	tmpsh |= 0x2;
	tmpsh &= ~0x1;	//make sure dont touch the TIMER1 interrupt.
	WriteRegHWord(TIRR, tmpsh);
}
#endif
/*
 * We use this function to register our own irqaction,
 *
 *
 * Alan Liu
 * 2007/7/25
 */
void __init plat_timer_setup(struct irqaction *irq)
{
	trihidtv_timer_setup(irq);
}

/*
 * Call asm/mc146818-time.h/mc146818_set_rtc_mmss()
 */
static int trihidtv_set_rtc_mmss(unsigned long nowtime)
{
	return mc146818_set_rtc_mmss(nowtime);
}

/*
 * Routine for board_time_init function pointer.
 * The specification exists:
 *	a) (optional) set up RTC routines,
 *	b) (optional) calibrate and set the mips_hpt_frequency
 *	(only needed if you intended to use fixed_rate_gettimeoffset
 *	or use cpu counter as timer interrupt source)
 * What we do:
 * 	1) Setup RTC routines(rtc_get_time,rtc_set_time)
 * 	2) Get the Memory Clock frequency used by timer2
 *
 */
void __init trihidtv_time_init(void)
{
        unsigned int flags;
	/*
	 * set up RTC routines,not the default null_rtc_get_time
	 * and null_rtc_set_time
	 */
	rtc_mips_get_time = lx5180_rtc_get_time;
	rtc_mips_set_time = lx5180_rtc_set_time;
	/* 
	 * set it to NULL now.
	 * Not sure whether we could implement it now.
	 * Alan Liu 2007/7/11
	 */
	rtc_mips_set_mmss = trihidtv_set_rtc_mmss; 
	//Alan Liu
	//we will not use the timer1 any more.
	//use timer in cp0
        //mips_timer_ack = trihidtv_timer_ack;
	//clocksource_mips.read = trihidtv_hpt_read;

	local_irq_save(flags);

        /* Set Data mode - binary. */ 
        CMOS_WRITE(CMOS_READ(RTC_CONTROL) | RTC_DM_BINARY, RTC_CONTROL);

	/*
	 * Calculating the Memory Clock frequency,based on RTC.
	 * The frequency is stored into mips_hpt_frequency
	 */
	printk("Calculating CPU frequency(xxxMHZ ?)... ");
	//calculate it self
	//timer_constant_value = 1<<20;
	timer_constant_value = cal_timer_constant();
	
	//No need to print...
	//printk("0x%08lx(%u)\n", timer_constant_value*HZ, (int) timer_constant_value*HZ);

	/* restore interrupts */
	local_irq_restore(flags);
}

/* SELP.4.2.1.x Add patch No.37, TridentSX3 chip porting, 2009-08-07 */
#if (defined(CONFIG_TRIHIDTV_CPU_350M) && defined(CONFIG_OPROFILE)) || defined(CONFIG_TRIHIDTV_CPU_425M)
extern int (*perf_irq)(struct pt_regs *regs);

irqreturn_t mips24ke_sx_perf_interrupt(int irq, void *dev_id, struct pt_regs *regs)
{
	//if ((read_c0_cause() & (1 << 26)))
	if(perf_irq)
		perf_irq(regs);
	return IRQ_HANDLED;
}

static struct irqaction perf_irqaction = {
        .handler = mips24ke_sx_perf_interrupt,
        .flags = IRQF_NODELAY | IRQF_DISABLED,
        .name = "Performance Counter",
};
#endif

/*
 * Routine for board_timer_setup function pointer.
 * board_timer_setup() -
 *	a) (optional) over-write any choices made above by time_init().
 *	b) machine specific code should setup the timer irqaction.
 *	c) enable the timer interrupt
 * What we should do:
 * 	1) Override the unsigned long (* do_gettimeoffset)(void) function
 * 	   pointer set in arch/mips/kernel/time.c,since our LX5180 have no
 * 	   COUNTER REGISTER (reported by cpu_probe,called in init_arch()).
 * 	   so in time.c, do_gettimeoffset is set to null_gettimeoffset,
 * 	   We should set our own do_gettimeoffset.
 *	2) Setup the timer irqaction,since our chip has timer2,use it. Its
 *	   IRQ is 15.
 *	3) Enable the timer interrupt.
 *
 */
void __init trihidtv_timer_setup(struct irqaction *irq)
{
	
	//unsigned int tmpsh;
	/*
	 * Debug here...
	 * Set this flag to disable global interrupt ENABLE bit in CPU.
	 * So the interrupt service routine could not be interfered...
	 * Alan Liu
	 */
	/*
	 * For AV MIPS,since two timers will share the same
	 * IRQ line, so we should use SA_SHIRQ here.
	 * Now we dont set it to SA_SHIRQ,so if others
	 * will use the other timers,he will fail on
	 * request_irq...
	 * Alan Liu
	 * 2005/7/20
	 */
	//irq->flags |=  SA_INTERRUPT;
	//irq->flags = 0;

	//irq->mask = 0;
	//irq->name = "timer interrupt";
	irq->dev_id = NULL;
	irq->next = NULL;
	setup_irq(TRIHIDTV_TIMER_IRQ, irq);

/* SELP.4.2.1.x Add patch No.37, TridentSX3 chip porting, 2009-08-07 */
#if (defined(CONFIG_TRIHIDTV_CPU_350M) && defined(CONFIG_OPROFILE)) || defined(CONFIG_TRIHIDTV_CPU_425M)
	//Setup performance counter interrupt here.
	//To support oprofile...
	//Alan Liu
	//2009-05-15
	perf_irqaction.handler = mips24ke_sx_perf_interrupt;
	perf_irqaction.dev_id = NULL;
	perf_irqaction.next = NULL;
	setup_irq(TRIHIDTV_24KE_PERF_IRQ, &perf_irqaction);
#endif

	/* sll32_usecs_per_cycle = 10^6 * 2^32 / mips_counter_freq */
	/* any better way to do this? */
	sll32_usecs_per_cycle = mips_hpt_frequency / 100000;
	sll32_usecs_per_cycle = 0xffffffff / sll32_usecs_per_cycle;
	sll32_usecs_per_cycle *= 10;
}
