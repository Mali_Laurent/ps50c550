/*
 * TTI Shanghai
 * 09/10/2003
 * Alan Liu alanliu@trident.com.cn
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/mm.h>
#include <linux/swap.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/timex.h>
#include <linux/kernel_stat.h>
#include <asm/bootinfo.h>
#include <asm/page.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/processor.h>
#include <asm/ptrace.h>
#include <asm/reboot.h>
#include <asm/mc146818rtc.h>
#include <linux/version.h>
#include <linux/bootmem.h>
#include <asm/irq.h>

#include <linux/mc146818rtc.h>
#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_irq.h>
#include <asm/trihidtv/trihidtv_defs.h>
#include <asm/trihidtv/trihidtv_addrspace.h>

//char command_str[]="mem=54M console=ttyS0,115200n8 initrd_start=0xbc000000 initrd_size=0x800000";
//char command_str[]="mem=54M console=ttyS0,115200n8 initrd_start=0x82000000 initrd_size=0x1a140e";
//char command_str[]="mem=54M console=ttyS0,115200n8 root=/dev/nfs nfsroot=172.16.10.20:/home/alan/ramdisk/ramdisk.8m ip=172.16.10.21:172.16.10.20:172.16.0.24:255.255.0.0:alanliu:eth0:off";
	//char command_str[]="mem=32M console=ttyS0,115200n8 ip=dhcp root=/dev/nfs nfsroot=172.16.10.90:/home/alan/ramdisk/ramdisk.8m";

#ifdef CONFIG_MIPS_TRIHIDTV_PRO_AV_MIPS
char hidtvpro_def_cmd[] = "mem=32M@64M console=ttyS0,115200n8";
#else
char hidtvpro_def_cmd[] = "mem=32M console=ttyS0,115200n8 ip=dhcp root=/dev/mtdblock2";
#endif

void
hidtvpro_hw_misc(void)
{

	unsigned int temp;

#if 0
	/*
	 * Open UMAC pci agent here,
	 * And set P2U here.
	 * Temporarily put here,for debug.
	 * We should put it in bootloader in future...
	 */
	// UMAC pci agent
	// P2U is remove from
	//temp = ReadRegWord(0x1502204c);
	//WriteRegWord(0x1502204c, temp|0x00220000);

	// P2U
	//WriteRegWord(0x1b004000, 0x00000810);	//set P2U bar
	//WriteRegWord(0x1b004004,0x40000000);  //set P2U bar value
	//WriteRegWord(0x1b004004, 0x00000000);	//set P2U bar value

	//enable P2U memory access
	//WriteRegWord(0x1b004000, 0x00000804);	//set P2U bar
	//WriteRegWord(0x1b004004, 0x02000002);	//set P2U bar value
#endif

/*
#ifdef CONFIG_FTMAC110
        //Set UMAc Agent
        WriteRegWord(0x15022058, 0x0000fe00);

        //Set DMA bridge endian: Little
        WriteRegHWord(0x1b00016c, 0x0021);
        WriteRegHWord(0x1b00016e, 0x0000);
#endif
*/
#if (defined(CONFIG_USB) || defined(CONFIG_USB_MODULE))
/* SELP.4.2.1.x Add patch No.37, TridentSX3 chip porting, 2009-08-07 */
#ifdef CONFIG_TRIHIDTV_CPU_350M
	temp = ReadRegWord((volatile void *)0x15022048);
	WriteRegWord((volatile void *)0x15022048,temp|0x22);
	WriteRegHWord((volatile void *)0x1b00001c,0x8000);
	WriteRegHWord((volatile void *)0x1b000084,0x41A1);

	temp = ReadRegHWord((volatile void *)0x1b00008e);
	//shutdown it now, and we will power it on later 
	//after host controller is ready...
	if((temp & 0x8000) == 0x0){
		temp = ReadRegHWord((volatile void *)0x1b00004c);
		WriteRegHWord((volatile void *)0x1b00004c,temp|0x0100);
		temp = ReadRegHWord((volatile void *)0x1b00004c);
	}else{
		temp = ReadRegHWord((volatile void *)0x1b00004c);
		WriteRegHWord((volatile void *)0x1b00004c,temp&~0x0100);
		temp = ReadRegHWord((volatile void *)0x1b00004c);
	}
#elif defined(CONFIG_TRIHIDTV_CPU_425M)
    //Agent for HC1
    temp = ReadRegWord((volatile void *)0x1502204c);
    WriteRegWord((volatile void *)0x1502204c,temp|0x22);
        
    //Agent for HC2
    temp = ReadRegWord((volatile void *)0x15022060);
    WriteRegWord((volatile void *)0x15022060,temp|0x22);
        
    WriteRegHWord((volatile void *)0x1b00001c,0x8000);
    WriteRegHWord((volatile void *)0x1b000084,0x41A1);
        
    temp = ReadRegHWord((volatile void *)0x1b00008e);
    //shutdown it now, and we will power it on later 
    //after host controller is ready...
    if((temp & 0x8000) == 0x0){
        //HC1
        temp = ReadRegHWord((volatile void *)0x1b00004c);
        WriteRegHWord((volatile void *)0x1b00004c,temp|0x0100);
        temp = ReadRegHWord((volatile void *)0x1b00004c);
        //HC2
        temp = ReadRegHWord((volatile void *)0x1b000178);
        WriteRegHWord((volatile void *)0x1b000178,temp|0x40);
        temp = ReadRegHWord((volatile void *)0x1b000178);
        }else{ 
        //HC1
        temp = ReadRegHWord((volatile void *)0x1b00004c);
        WriteRegHWord((volatile void *)0x1b00004c,temp&~0x0100);
        temp = ReadRegHWord((volatile void *)0x1b00004c);
        //HC2
        temp = ReadRegHWord((volatile void *)0x1b000178);
        WriteRegHWord((volatile void *)0x1b000178,temp&~0x40);
        temp = ReadRegHWord((volatile void *)0x1b000178);
    }
#endif

#endif

#ifdef CONFIG_IDE
	/*
	 * Alan Liu
	 * ITE initial setting...
	 */
	WriteRegByte(0x15021f00, 0x19);		//PIN SHARE with VIDEO(RGB...)
	WriteRegHWord(0x1b000024, 0xc4cc);	//IDE ENABLE

	WriteRegWord(0x1b004000, 0x00000d04);
	WriteRegWord(0x1b004004, 0x02800005);	// config the command

	// need not set it here.
	//WriteRegWord(0x1b004000,0x00000d08); 
	//WriteRegWord(0x1b004004,0x01018f01); //io address mode

	WriteRegWord(0x1b004000, 0x00000d0c);
	WriteRegWord(0x1b004004, 0x00008000);	//Latency timing

	WriteRegWord(0x1b004000, 0x00000d40);
	WriteRegWord(0x1b004004, 0x00800000);	//channel control register,the bit 0 is set to 1 to enable ultra dma...

/*------------------------------------------------------------------------------*/
	/*
	 * Following is for Ultra DMA...
	 * Alan Liu
	 */
	WriteRegWord(0x1b004000, 0x00000d40);
	WriteRegWord(0x1b004004, 0x00800003);	//channel control register,the bit 0 is set to 1 to enable ultra dma...

	WriteRegWord(0x1b004000, 0x00000d48);
	WriteRegWord(0x1b004004, 0xcc50cc50);

	WriteRegWord(0x1b004000, 0x00000d4c);
	WriteRegWord(0x1b004004, 0x00008888);

	//should use this read here to ensure the
	//previous writing has been done in HW!!!
	//Alan Liu
	ReadRegWord(0x1b004004);
/*------------------------------------------------------------------------------*/
#endif
}
