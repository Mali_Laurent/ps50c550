/**
 *
 * This file contains code to interface generic Linux interrupt handling
 * functions in arch/mips/kernel/irq.c. Additional 
 * code added to implement the Lexra vectored interrupts.
 *
 * SHOULD PAY much attention to the issues of Endian!!!!!
 *
 * Alan Liu 	alanliu@trident.com.cn
 * TTI Shanghai
 * 09/10/2003
 *
 * Alan Liu 	alanliu@trident.com.cn
 * redesigned for SX chip
 * 08/19/2008
 */

#include <linux/errno.h>
#include <linux/init.h>
#include <linux/kernel_stat.h>
#include <linux/signal.h>
#include <linux/sched.h>
#include <linux/types.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <linux/timex.h>
#include <linux/random.h>
#include <linux/irq.h>

#include <asm/bitops.h>
#include <asm/bootinfo.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/mipsregs.h>
#include <asm/system.h>

#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_int.h>
#include <asm/trihidtv/trihidtv_irq.h>
#include <asm/trihidtv/trihidtv_defs.h>
#include <asm/trihidtv/trihidtv_addrspace.h>

struct sx_eic_regs volatile * eic_regs
        = (struct sx_eic_regs volatile *)(KSEG1ADDR(SX_EIC_REG_BASE));

#ifdef CONFIG_TRIHIDTV_CPU_350M
void enable_eic_irq(unsigned int irq_nr)
{
	unsigned int tmp;
	unsigned int shift;

	if ( (irq_nr >= TRIHIDTV_EIC_IRQ_BASE) && (irq_nr <= TRIHIDTV_PHY_MAX_IRQ)) {
		//Simple, just mapping it to corresponding bits in imr...	
		tmp = ReadRegWord(&eic_regs->imr);
		WriteRegWord(&eic_regs->imr,tmp &~(1<<(irq_nr-1)));
	} else if(irq_nr >= TRIHIDTV_VIRT_BASE_IRQ && irq_nr <= TRIHIDTV_EIC_LAST_IRQ){
		//Buggy design: 
		//AVMIPS command/status share one interrupt source,
		//3RDMIPS command/status share one interrupt source,
		//LYPSYNC command/status share one interrupt source,
		//MI2C 0/1/2 share one interrupt source,
		//and uart1,2,3 share one interrupt source.
		//So, we need disable the corresponding bits here...
		//we could not disable the mask bits in imr, it is the total control...
		switch(irq_nr){
			case TRIHIDTV_MI2C0_IRQ :
			case TRIHIDTV_MI2C1_IRQ :
			case TRIHIDTV_MI2C2_IRQ :
				shift = irq_nr - TRIHIDTV_MI2C0_IRQ;
				tmp = ReadRegWord(&eic_regs->ilcr);
				WriteRegWord(&eic_regs->ilcr,tmp & ~(1<<shift));
				break;
			case TRIHIDTV_UART0_IRQ :
			case TRIHIDTV_UART1_IRQ :
			case TRIHIDTV_UART2_IRQ :
				shift = irq_nr - TRIHIDTV_UART0_IRQ;
				tmp = ReadRegWord(&eic_regs->urtimr);
				WriteRegWord(&eic_regs->urtimr,tmp & ~(1<<shift));
				break;
			case TRIHIDTV_LYPSYNC0_WR_IRQ :
			case TRIHIDTV_LYPSYNC1_WR_IRQ :
			case TRIHIDTV_LYPSYNC2_WR_IRQ :
			case TRIHIDTV_LYPSYNC3_WR_IRQ :
			case TRIHIDTV_LYPSYNC0_RD_IRQ :
			case TRIHIDTV_LYPSYNC1_RD_IRQ :
			case TRIHIDTV_LYPSYNC2_RD_IRQ :
			case TRIHIDTV_LYPSYNC3_RD_IRQ :
				shift = irq_nr - TRIHIDTV_LYPSYNC0_RD_IRQ;
				tmp = ReadRegWord(&eic_regs->lypsimr);
				WriteRegWord(&eic_regs->lypsimr,tmp & ~(1<<shift));
				break;
			case TRIHIDTV_AVMIPS_RQST_IRQ : 
			case TRIHIDTV_AVMIPS_RSPNS_IRQ :
				shift = irq_nr - TRIHIDTV_AVMIPS_RQST_IRQ;
				tmp = ReadRegWord(&eic_regs->avmipsimr);
				WriteRegWord(&eic_regs->avmipsimr,tmp & ~(1<<shift));
				break;
			case TRIHIDTV_3RDMIPS_RQST_IRQ : 
			case TRIHIDTV_3RDMIPS_RSPNS_IRQ :
				shift = irq_nr - TRIHIDTV_3RDMIPS_RQST_IRQ;
				tmp = ReadRegWord(&eic_regs->mips3rdimr);
				WriteRegWord(&eic_regs->mips3rdimr,tmp & ~(1<<shift));
				break;
			default:
				break;
		}
	} else {
		//panic("%s: bad irq %d", __FUNCTION__,irq_nr);
		/* SELP TridentSX debugging, to check invalid IRQ num, 2009-12-16 */
        printk(KERN_ALERT " ##### Bad IRQ, num : %d, func : %s\n", irq_nr, __func__);
	}
}

void disable_eic_irq(unsigned int irq_nr)
{
	unsigned int tmp;
	unsigned int shift;

	if ( (irq_nr >= TRIHIDTV_EIC_IRQ_BASE) && (irq_nr <= TRIHIDTV_PHY_MAX_IRQ)) {
		//Simple, just mapping it to corresponding bits in imr...	
		tmp = ReadRegWord(&eic_regs->imr);
		WriteRegWord(&eic_regs->imr,tmp | (1<<(irq_nr-1)));
	} else if(irq_nr >= TRIHIDTV_VIRT_BASE_IRQ && irq_nr <= TRIHIDTV_EIC_LAST_IRQ){
		//Buggy design: MI2C 0/1 share one interrupt source,
		//and uart1,2,3 share one interrupt source.
		//So, we need disable the corresponding bits here...
		//we could not disable the mask bits in imr, it is the total control...
		switch(irq_nr){
			case TRIHIDTV_MI2C0_IRQ :
			case TRIHIDTV_MI2C1_IRQ :
			case TRIHIDTV_MI2C2_IRQ :
				shift = irq_nr - TRIHIDTV_MI2C0_IRQ;
				tmp = ReadRegWord(&eic_regs->ilcr);
				WriteRegWord(&eic_regs->ilcr,tmp | (1<<shift));
				break;
			case TRIHIDTV_UART0_IRQ :
			case TRIHIDTV_UART1_IRQ :
			case TRIHIDTV_UART2_IRQ :
				shift = irq_nr - TRIHIDTV_UART0_IRQ;
				tmp = ReadRegWord(&eic_regs->urtimr);
				WriteRegWord(&eic_regs->urtimr,tmp | (1<<shift));
				break;
			case TRIHIDTV_LYPSYNC0_WR_IRQ :
			case TRIHIDTV_LYPSYNC1_WR_IRQ :
			case TRIHIDTV_LYPSYNC2_WR_IRQ :
			case TRIHIDTV_LYPSYNC3_WR_IRQ :
			case TRIHIDTV_LYPSYNC0_RD_IRQ :
			case TRIHIDTV_LYPSYNC1_RD_IRQ :
			case TRIHIDTV_LYPSYNC2_RD_IRQ :
			case TRIHIDTV_LYPSYNC3_RD_IRQ :
				shift = irq_nr - TRIHIDTV_LYPSYNC0_RD_IRQ;
				tmp = ReadRegWord(&eic_regs->lypsimr);
				WriteRegWord(&eic_regs->lypsimr,tmp | (1<<shift));
				break;
			case TRIHIDTV_AVMIPS_RQST_IRQ : 
			case TRIHIDTV_AVMIPS_RSPNS_IRQ :
				shift = irq_nr - TRIHIDTV_AVMIPS_RQST_IRQ;
				tmp = ReadRegWord(&eic_regs->avmipsimr);
				WriteRegWord(&eic_regs->avmipsimr,tmp | (1<<shift));
				break;
			case TRIHIDTV_3RDMIPS_RQST_IRQ : 
			case TRIHIDTV_3RDMIPS_RSPNS_IRQ :
				shift = irq_nr - TRIHIDTV_3RDMIPS_RQST_IRQ;
				tmp = ReadRegWord(&eic_regs->mips3rdimr);
				WriteRegWord(&eic_regs->mips3rdimr,tmp | (1<<shift));
				break;
			default:
				break;
		}
	} else {
		//panic("%s: bad irq %d", __FUNCTION__,irq_nr);
		/* SELP TridentSX debugging, to check invalid IRQ num, 2009-12-16 */
        printk(KERN_ALERT " ##### Bad IRQ, num : %d, func : %s\n", irq_nr, __func__);
	}
}
#elif CONFIG_TRIHIDTV_CPU_425M
void enable_eic_irq(unsigned int irq_nr)
{
	unsigned int tmp;

	if ( (irq_nr >= TRIHIDTV_EIC_IRQ_BASE) && (irq_nr <= TRIHIDTV_PHY_MAX_IRQ)) {
		//SX3 EIC supports 64 external interrupts.
		//identify it first.
		if(irq_nr <= 32){
			//low 32 interrupts.    
			tmp = ReadRegWord(&eic_regs->imr_l);
			WriteRegWord(&eic_regs->imr_l,tmp &~(1<<(irq_nr-1)));
		}else{
			//high 32 interrupts.   
			tmp = ReadRegWord(&eic_regs->imr_h);
			WriteRegWord(&eic_regs->imr_h,tmp &~(1<<(irq_nr-32-1)));
		}
	} else {
		//panic("%s: bad irq %d", __FUNCTION__,irq_nr);
		/* SELP TridentSX debugging, to check invalid IRQ num, 2009-12-16 */
        printk(KERN_ALERT " ##### Bad IRQ, num : %d, func : %s\n", irq_nr, __func__);
	}
}

void disable_eic_irq(unsigned int irq_nr)
{
	unsigned int tmp;

	if ( (irq_nr >= TRIHIDTV_EIC_IRQ_BASE) && (irq_nr <= TRIHIDTV_PHY_MAX_IRQ)) {
       //SX3 EIC supports 64 external interrupts.
       //identify it first.
       if(irq_nr <= 32){
           //low 32 interrupts.    
           tmp = ReadRegWord(&eic_regs->imr_l);
           WriteRegWord(&eic_regs->imr_l,tmp |(1<<(irq_nr-1)));
       }else{
           //high 32 interrupts.   
           tmp = ReadRegWord(&eic_regs->imr_h);
           WriteRegWord(&eic_regs->imr_h,tmp |(1<<(irq_nr-32-1)));
		}
	} else {
		//panic("%s: bad irq %d", __FUNCTION__,irq_nr);
		/* SELP TridentSX debugging, to check invalid IRQ num, 2009-12-16 */
        printk(KERN_ALERT " ##### Bad IRQ, num : %d, func : %s\n", irq_nr, __func__);
	}
}
#endif

static inline void mask_irq(unsigned int irq_nr)
{		
	disable_eic_irq(irq_nr);
}
static inline void unmask_irq(unsigned int irq_nr)
{	
	enable_eic_irq(irq_nr);
}


/**
 * Following four functions are needed by arch/mips/kernel/irq.c
 */
static unsigned int startup_irq(unsigned int irq_nr)
{
	unmask_irq(irq_nr);
	return 0; 
}


static void shutdown_irq(unsigned int irq_nr)
{
	mask_irq(irq_nr);
}

/*
 *
 */
static inline void ack_level_irq(unsigned int irq_nr)
{
        mask_irq(irq_nr);
}


static inline void end_irq(unsigned int irq_nr)
{
	if (!(irq_desc[irq_nr].status & (IRQ_DISABLED|IRQ_INPROGRESS)))
		unmask_irq(irq_nr); 
	else {
		//irq_desc_t *desc = irq_desc + irq_nr;
		//dump_desc_t(desc);
	}
}

static struct irq_chip trihidtv_irq_chip = {
	.name = "HiDTV IRQ CHIP",
	.startup = startup_irq,
	.shutdown = shutdown_irq,
	.unmask = unmask_irq,
	.mask = mask_irq,
	.ack = ack_level_irq,
	.end = end_irq,
	NULL
};

#ifdef CONFIG_HIGH_RES_TIMERS
extern asmlinkage void event_timer_handler(struct pt_regs *regs);
#endif

#if 1
void plat_irq_dispatch(struct pt_regs *regs)
{

        unsigned int irq;
        unsigned int irq_n;
	unsigned int cause;
	unsigned int status;

#ifdef CONFIG_TRIHIDTV_CPU_425M
/* VDLinux, based SELP.4.2.1.x default patch No.12, 
   detect kernel stack overflow, SP Team 2009-05-08 */
#ifdef CONFIG_DEBUG_STACKOVERFLOW
#ifndef STACK_WARN
# define STACK_WARN (THREAD_SIZE/8)
#endif
    /* Debugging check for stack overflow */
    {
	register unsigned long sp __asm__("sp");
	register unsigned long thread_info __asm__("$28");
	extern void print_modules(void);

	if (unlikely(sp < thread_info + sizeof(struct thread_info) + STACK_WARN)) {
		printk(KERN_ERR "stack overflow: 0x%lx\n", sp);
		print_modules();
		show_regs(regs);
		dump_stack();
        }
    }
#endif
#endif

        cause = read_c0_cause();
        status = read_c0_status();

	//get RIPL from cause register.
	irq = cause>>10 & 0x3f;

	//printk("%s:%d	cause is %08x, status is %08x,irq is %08x\n",
	//		__FILE__,__LINE__,cause,status,irq);
	
	if( (irq >= TRIHIDTV_EIC_IRQ_BASE) && (irq <= TRIHIDTV_PHY_MAX_IRQ) ){

#ifdef CONFIG_TRIHIDTV_CPU_350M
		if(irq != TRIHIDTV_MI2C_VIRT_IRQ 
		&& irq != TRIHIDTV_UART_VIRT_IRQ
		&& irq != TRIHIDTV_LYPSYNC_VIRT_IRQ
		&& irq != TRIHIDTV_AVMIPS_VIRT_IRQ
		&& irq != TRIHIDTV_3RDMIPS_VIRT_IRQ
		){
#endif
#ifdef CONFIG_HIGH_RES_TIMERS
			/*
			 * handle timer interrupt specially here.
			 * this is to support HIGH_RES_TIMERS.
			 * Alan Liu
			 */
			if( irq == TRIHIDTV_TIMER_IRQ ){
				event_timer_handler(regs);
				return;
			}
#endif
			//handle non i2c/uart interrupts here.
			do_IRQ(irq,regs);

			// for secureboot
			if(irq == TRIHIDTV_MI2C0_IRQ)	
				add_interrupt_randomness(TRIHIDTV_MI2C0_IRQ);
			else if(irq == TRIHIDTV_MI2C1_IRQ)	
				add_interrupt_randomness(TRIHIDTV_MI2C1_IRQ);
			else if(irq == TRIHIDTV_MI2C2_IRQ)	
				add_interrupt_randomness(TRIHIDTV_MI2C2_IRQ);
#ifdef CONFIG_TRIHIDTV_CPU_350M
		} else {
			//handle i2c/uart interrupts here.
			switch(irq){

			case TRIHIDTV_MI2C_VIRT_IRQ:
				irq_n = ReadRegWord(&eic_regs->i2cisr);
				if(irq_n & 0x1)	
				{
					do_IRQ(TRIHIDTV_MI2C0_IRQ,regs);
					// reference from handle_IRQ_event()
					add_interrupt_randomness(TRIHIDTV_MI2C0_IRQ);
				}
				if(irq_n & 0x2)
				{
					do_IRQ(TRIHIDTV_MI2C1_IRQ,regs);
					// reference from handle_IRQ_event()
					add_interrupt_randomness(TRIHIDTV_MI2C1_IRQ);
				}
				if(irq_n & 0x4)
				{
					do_IRQ(TRIHIDTV_MI2C2_IRQ,regs);
					// reference from handle_IRQ_event()
					add_interrupt_randomness(TRIHIDTV_MI2C2_IRQ);
				}
				break;

			case TRIHIDTV_UART_VIRT_IRQ:
				irq_n = ReadRegWord(&eic_regs->urtisr);
				if(irq_n & 0x1)
					do_IRQ(TRIHIDTV_UART0_IRQ,regs);
				if(irq_n & 0x2)
					do_IRQ(TRIHIDTV_UART1_IRQ,regs);
				if(irq_n & 0x4)
					do_IRQ(TRIHIDTV_UART2_IRQ,regs);
				break;

			case TRIHIDTV_AVMIPS_VIRT_IRQ:
				irq_n = ReadRegWord(&eic_regs->avmipsisr);
				if(!(irq_n & 0x1))
					do_IRQ(TRIHIDTV_AVMIPS_RQST_IRQ ,regs);
				if(!(irq_n & 0x2))
					do_IRQ(TRIHIDTV_AVMIPS_RSPNS_IRQ,regs);
				break;

			case TRIHIDTV_LYPSYNC_VIRT_IRQ:
				irq_n = ReadRegWord(&eic_regs->lypsisr);
				if(irq_n & 0x1)
					do_IRQ(TRIHIDTV_LYPSYNC0_RD_IRQ,regs);
				if(irq_n & 0x2)
					do_IRQ(TRIHIDTV_LYPSYNC0_WR_IRQ,regs);
				if(irq_n & 0x4)
					do_IRQ(TRIHIDTV_LYPSYNC1_RD_IRQ,regs);
				if(irq_n & 0x8)
					do_IRQ(TRIHIDTV_LYPSYNC1_WR_IRQ,regs);
				if(irq_n & 0x10)
					do_IRQ(TRIHIDTV_LYPSYNC2_RD_IRQ,regs);
				if(irq_n & 0x20)
					do_IRQ(TRIHIDTV_LYPSYNC2_WR_IRQ,regs);
				if(irq_n & 0x40)
					do_IRQ(TRIHIDTV_LYPSYNC3_RD_IRQ,regs);
				if(irq_n & 0x80)
					do_IRQ(TRIHIDTV_LYPSYNC3_WR_IRQ,regs);
				break;	

			case TRIHIDTV_3RDMIPS_VIRT_IRQ:
				irq_n = ReadRegWord(&eic_regs->mips3rdisr);
				if(!(irq_n & 0x1))
					do_IRQ(TRIHIDTV_3RDMIPS_RQST_IRQ ,regs);
				if(!(irq_n & 0x2))
					do_IRQ(TRIHIDTV_3RDMIPS_RSPNS_IRQ,regs);
				break;

			default:
				break;
			}
		}
#endif
	}else{
		spurious_interrupt(regs);
		/* SELP TridentSX debugging, to check invalid IRQ num, 2009-12-16 */
		printk(KERN_ALERT " ##### Bad IRQ, num : %d, func : %s\n", irq, __func__);
	}
}
#endif

static void
sx_eic_reset(void)
{

	int i;

	//set iscr registers in eic
	for(i=0;i<=TRIHIDTV_PHY_MAX_IRQ;i++){
		//set shadow register set number to 0 now.
		//Adjust this later!!!!!!!!!!!!!!!!!
		//Alan Liu
		WriteRegWord(&eic_regs->iscr[i],i+1);	//IPL should be bigger than 0
	}
	
	/* mask all eic interrupts,lmr register */
#ifdef CONFIG_TRIHIDTV_CPU_350M
	WriteRegWord(&eic_regs->imr,0xffffffff);
#elif CONFIG_TRIHIDTV_CPU_425M
	WriteRegWord(&eic_regs->imr_h,0xffffffff);
	WriteRegWord(&eic_regs->imr_l,0xffffffff);
#endif

	/* set interrupt active level,ialr register */
	//??????????????????????
	//WriteRegWord(&eic_regs->ialr,0xffffffff);
	//WriteRegWord(&eic_regs->ialr,0xfffeff9f);
	//WriteRegWord(&eic_regs->ialr,0xfff6ff9f);
#ifdef CONFIG_TRIHIDTV_CPU_350M
	WriteRegWord(&eic_regs->ialr,0xfff6ffff);
#elif CONFIG_TRIHIDTV_CPU_425M
	WriteRegWord(&eic_regs->ialr_h,0x6ffff3ff);
	WriteRegWord(&eic_regs->ialr_l,0xfffbfe1f);
#endif

	/* set interrupt trigger mode,itmr register */
	//??????????????????????
	//WriteRegWord(&eic_regs->itmr,0x00000000);
#ifdef CONFIG_TRIHIDTV_CPU_350M
	WriteRegWord(&eic_regs->itmr,0x00380000);
#elif CONFIG_TRIHIDTV_CPU_425M
	WriteRegWord(&eic_regs->itmr_h,~0xffffffff);
	WriteRegWord(&eic_regs->itmr_l,~0xff3fffff);
#endif

	/* set interrupt status register ,isr register */
	//This register is read-only.
	//WriteRegWord(&eic_regs->isr,0x00000000);
	
	/* set interrupt latch clear and i2c interrupt mask register */
	//write 1,then 0,to clear it.
#ifdef CONFIG_TRIHIDTV_CPU_350M
	WriteRegWord(&eic_regs->ilcr,0xffffffff);
#endif
	//make sure i2c is masked.
#ifdef CONFIG_TRIHIDTV_CPU_350M
	WriteRegWord(&eic_regs->ilcr,0x00000007);
#elif CONFIG_TRIHIDTV_CPU_425M
    WriteRegWord(&eic_regs->ilcr_h,0xffffffff);
    WriteRegWord(&eic_regs->ilcr_l,0xffffffff);
    WriteRegWord(&eic_regs->ilcr_h,0x0);
    WriteRegWord(&eic_regs->ilcr_l,0x0);
#endif

	/* set i2c interrupt status register ,i2cisr register */
	//This register is read-only.
	//WriteRegWord(&eic_regs->i2cisr,0x00000000);

#ifdef CONFIG_TRIHIDTV_CPU_350M
	/* set uart interrupt mask register ,urtimr register */
	WriteRegWord(&eic_regs->urtimr,0x7);

	/* set lypsync interrupt mask register ,lypsimr register */
	WriteRegWord(&eic_regs->lypsimr,0xff);

	/* set avmips interrupt mask register ,avmipsimr register */
	WriteRegWord(&eic_regs->avmipsimr,0x3);

	/* set 3rdmips interrupt mask register ,3rdmips register */
	WriteRegWord(&eic_regs->mips3rdimr,0x3);
#endif

	/* set uart interrupt mask register ,urtimr register */
	//WriteRegWord(&eic_regs->urtisr,0x7);

	/* set sw interrupt register ,swir register */
	//make hw to clear edge interrupts.
#ifdef CONFIG_TRIHIDTV_CPU_350M
	WriteRegWord(&eic_regs->swir,0x10000);
#elif CONFIG_TRIHIDTV_CPU_425M
	WriteRegWord(&eic_regs->swir,0x0);
#endif
}

extern void *set_vi_handler(int n, void *addr);
/*
 * Set sx related interrupt handlers here.
 * we are using EIC, and it is vector based.
 * 
 */
void __init
sx_irq_setup(void)
{

        int i;
	int nvec;

	/*
	 * Set interrupt handler here.
	 * Alan Liu 2008-08-20
	 */	 
	nvec = cpu_has_veic ? 64 : 8;
	for (i = 0; i < nvec; i++)
		set_vi_handler(i,plat_irq_dispatch);
	
	/* disable all interrupts at first */
        for (i = 0; i < NR_IRQS; i++) {
                disable_irq(i);
        }
}

/**
 * Initial setup of interrupts. Called by start_kernel(). Only
 * function is to call the function irq_setup().
 *
 * Alan Liu
 * 09/11/2003
 */
/*
 * Re-port it for linux 2.6.
 * 2007/7/17
 */
/*
 * Re-port it for SX/linux 2.6.
 * 2008/8/19
 */
void __init arch_init_irq(void)
{

        int i;

printk("%s:%s:%d, set interrupt handler\n",
	__FILE__,__FUNCTION__,__LINE__);

	//eic init
	sx_eic_reset();
	//sx related irq setup
	sx_irq_setup();

	//make sure these 5 kinds of interrupts are not masked in imr
	//since they have separate mask bits...
#ifdef CONFIG_TRIHIDTV_CPU_350M
	startup_irq(TRIHIDTV_AVMIPS_VIRT_IRQ);
	startup_irq(TRIHIDTV_3RDMIPS_VIRT_IRQ);
	startup_irq(TRIHIDTV_LYPSYNC_VIRT_IRQ);
	startup_irq(TRIHIDTV_MI2C_VIRT_IRQ); 
	startup_irq(TRIHIDTV_UART_VIRT_IRQ);        
#endif

	for(i=0; i < NR_IRQS; i++) { 
		set_irq_chip(i,&trihidtv_irq_chip);
	}
}
