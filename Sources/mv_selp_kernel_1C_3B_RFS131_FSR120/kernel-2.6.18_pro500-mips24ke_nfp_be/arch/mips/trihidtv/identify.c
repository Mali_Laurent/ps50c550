/*
 * identify.c: machine identification code.
 *
 * Copyright (C) 2007 Alan Liu
 * Copyright (C) 2002, 2003, 2004, 2005  Maciej W. Rozycki
 */
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/mc146818rtc.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/types.h>

#include <asm/bootinfo.h>

static const char *trihidtv_system_strings[] = {
	[MACH_TRIHIDTV]		"TRIHIDTV SYSTEM",
};

const char *get_system_type(void)
{
#define STR_BUF_LEN	64
	static char system[STR_BUF_LEN];
	static int called = 0;

	if (called == 0) {
		called = 1;
		snprintf(system, STR_BUF_LEN, "Digital %s",
			 trihidtv_system_strings[mips_machtype]);
	}

	return system;
}
