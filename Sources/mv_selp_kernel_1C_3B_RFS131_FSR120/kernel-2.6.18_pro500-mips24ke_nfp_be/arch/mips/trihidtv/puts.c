
/*
 * Alan Liu
 * Just use this file to print sth before printk could work.
 */

#include <stdarg.h>
#include <linux/types.h>
#include <linux/string.h>
#include <linux/ctype.h>
#include <linux/kernel.h>

#include <linux/types.h>
#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_addrspace.h>

/*
 * Memory segments (32bit kernel mode addresses)
 */

//#define SERIAL_BASE   ((TRIHIDTV_PCI_IO_BASE + TRIHIDTV_UART_BASE)|KSEG1)
#define SERIAL_BASE   0xBB005100	
#define SER_CMD       5
#define SER_DATA      0x00
#define RX_READY      0x01
#define TX_BUSY       0x20
#define SERIAL_ADDR(offset)     ((volatile void *)(SERIAL_BASE + offset))
#define SERIAL_CMD              SERIAL_ADDR(SER_CMD)
#define SERIAL_DATA             SERIAL_ADDR(SER_DATA)


#define TIMEOUT       0xfffff	

const char digits[16] = "0123456789abcdef";
static volatile unsigned char *const com1 = (unsigned char *) SERIAL_BASE;

/*********************************************************************************
		local functions
*********************************************************************************/
/*delay*/
static inline void slow_down(void)
{
	int k;
	for (k = 0; k < 10000; k++);
}

/**********************************************************************************
**********************************************************************************/
/*
put a char, send to serial port in order to display on the host terminal
parameter:	c: the char
return:		void 
*/
void putch(const unsigned char c)
{
	unsigned char ch;
	int i = 0;

	do {
		/*read UART line status register*/
		ch = ReadRegByte(SERIAL_CMD); 
		slow_down();
		i++;
		if (i > TIMEOUT) {
			break;
		}
	} while (0 == (ch & TX_BUSY));
	WriteRegByte(SERIAL_DATA, c);
}
void prom_putchar(const unsigned char c)
{
	putch(c);
}
/*********************************************************************************/

/**********************************************************************************
**********************************************************************************/
/*
put a string, send to serial port in ordet to display on the host terminal
parameter:	cp: pointer points to the target string
return:		void
*/
void puts(unsigned char *cp)
{
	unsigned char ch;
	int i = 0;

	while (*cp) {
		do {
			/*read UART line status register*/
			ch=ReadRegByte(SERIAL_CMD);  //MAG
			i++;
			if (i > TIMEOUT) {
				break;
			}
		} while (0 == (ch & TX_BUSY));
		if(*cp=='\n')
		{
			WriteRegByte(SERIAL_DATA, '\r');	//MAG
			WriteRegByte(SERIAL_DATA, '\n');	//MAG
		}
		else{
			WriteRegByte(SERIAL_DATA, *cp);	//MAG
		}
		cp++;
	}
}
/*********************************************************************************/

/**********************************************************************************
**********************************************************************************/
/*
print a 64 bit unsigned number in a hex format on host terminal
parameter:	ul: the 64bit number
return:		void
*/
void put64(unsigned long ul)
{
	int cnt;
	unsigned ch;

	cnt = 16;		/* 16 nibbles in a 64 bit long */
	putch('0');
	putch('x');
	do {
		cnt--;
		ch = (unsigned char) (ul >> cnt * 4) & 0x0F;
		putch(digits[ch]);
	} while (cnt > 0);
}
/**********************************************************************************/

/***********************************************************************************
***********************************************************************************/
/*
print a 32 bit unsigned number in a hex format on host terminal
parameter:	u: the 32bit number
return:		void
*/
void put32(unsigned u)
{
	int cnt;
	unsigned ch;

	cnt = 8;		/* 8 nibbles in a 32 bit long */

	putch('0');
	putch('x');
	do {
		cnt--;
		ch = (unsigned char) (u >> cnt * 4) & 0x0F;
		putch(digits[ch]);
	} while (cnt > 0);

}
/***********************************************************************************/

/***********************************************************************************
***********************************************************************************/
/*
print a 16 bit unsigned number in a hex format on host terminal
parameter:	u: the 16bit number
return:		void
*/
void put16(unsigned short u)
{
	int cnt;
	unsigned ch;

	cnt = 4;		/* 4 nibbles in a 16 bit long */

	putch('0');
	putch('x');
	do {
		cnt--;
		ch = (unsigned char) (u >> cnt * 4) & 0x0F;
		putch(digits[ch]);
	} while (cnt > 0);

}
/***********************************************************************************/

/************************************************************************************
************************************************************************************/
/*
print a 8 bit--BYTE unsigned number in a hex format on host terminal
parameter:	u: the 8bit number
return:		void
*/
void put8(unsigned char u)
{
	int cnt;
	unsigned ch;

	cnt=2;

/*	putch('0');
	putch('x');	*/
	do {
		cnt--;
		ch = (unsigned char)(u>>cnt*4)&0x0F;
		putch(digits[ch]);
	} while(cnt>0);
}


//#define LINESZ 100
//typedef char *va_list;
/*
 * replace printk with this one.
 * the original printk is placed in kernel/printk.c
 * for debugging ...
 */
/*
int
printk(const char *fmt, ...)
{
	char buf[LINESZ * 2];
	va_list args;
	int i;

	va_start(args, fmt);
	i = vsprintf(buf, fmt, args);   // hopefully i < sizeof(buf)-4
	va_end(args);
	puts(buf);
	return i;
}
*/

/*
int
printf(const char *fmt, ...)
{
	char buf[LINESZ * 2];
	va_list args;
	int i;

	va_start(args, fmt);
	i = vsprintf(buf, fmt, args);   // hopefully i < sizeof(buf)-4
	va_end(args);
//	fputs(buf);
	puts(buf);
	return i;
}
*/
/*
get a char, return value is the target char.
parameter:	void
return:		char got
*/
unsigned char getch(void)
{
	unsigned char ch;
	int i=0;

	do {
		/*read UART line status register*/
//		ch = com1[SER_CMD];
		//add MAG's macro, endian issues
		ch=ReadRegByte(SERIAL_CMD);  //MAG
		slow_down();
		i++;
		if (i > TIMEOUT) {
			return -1;
		}
	} while (0 == (ch & RX_READY));
	/*UART Receiver buffer register*/
//	ch=com1[SER_DATA];
	ch=ReadRegByte(SERIAL_DATA);	//MAG
	return ch;
}
/*
get a string via serial port until get <CR> which equals '\r'
when user hit a key, meanwhile, put the key on the screen
parametre:	p: pointer points to the target string in the end
return 		the length of the string got
*/
int gets(unsigned char * p)
{
	unsigned char * ch;
	int ret=0;

	ch=p;
	
	while(1)
	{
		*ch=getch();
		if(*ch=='\r')
		{
			*ch='\0';
			break;
		}else{
			putch(*ch);
			ret++;
			ch++;
		}
		
	}

	return ret;
}

/* uart data register */
#define UART_UTBR       0       /*DLAB = 0*/

/* uart control register */
#define UART_UIER       1       /*DLAB = 0*/
#define UART_UFCR       2
#define UART_ULCR       3
#define UART_UMCR       4

#define UART_UDLL       0       /*DLAB = 1*/
#define UART_UDLM       1       /*DLAB = 2*/

/*uart status register */
#define UART_ULSR       5
#define UART_UMSR       6
#define UART_USCR       7

#define UART_REG(offset)        ((volatile void *)((SERIAL_BASE+offset) | KSEG1))

void Init_Serial(void)
{
	BYTE tmp;

	//use MAG's macor
	WriteRegByte(UART_REG(UART_ULCR),0x03);	//each character, 8 bits, stop bit 1 bit
	WriteRegByte(UART_REG(UART_UFCR),0xc7);	//FIFO trigger level: 14 bytes, enable FIFO and clear FIFO
	tmp=ReadRegByte(UART_REG(UART_ULCR));	
	WriteRegByte(UART_REG(UART_ULCR),tmp|0x80);	//set DLAB=1
	WriteRegByte(UART_REG(UART_UDLL),1);		//set baudrate=115200
	WriteRegByte(UART_REG(UART_UDLM),0);	
	WriteRegByte(UART_REG(UART_UMCR),0x06);

	tmp=ReadRegByte(UART_REG(UART_ULCR));	
	WriteRegByte(UART_REG(UART_ULCR),tmp&~0x80);	//set DLAB=0
}
