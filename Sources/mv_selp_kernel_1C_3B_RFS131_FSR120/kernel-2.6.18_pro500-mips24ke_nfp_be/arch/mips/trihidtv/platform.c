/*
 * Platform device support for Au1x00 SoCs.
 *
 * Copyright 2004, Matt Porter <mporter@kernel.crashing.org>
 *
 * This file is licensed under the terms of the GNU General Public
 * License version 2.  This program is licensed "as is" without any
 * warranty of any kind, whether express or implied.
 */
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/resource.h>

#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_addrspace.h>
#include <asm/trihidtv/trihidtv_int.h>

/* EHCI (USB high speed host controller) */
static struct resource trihidtv_usb_ehci_resources[] = {
	[0] = {
		.start		= TRIHIDTV_EHCI_BASE,
		.end		= TRIHIDTV_EHCI_BASE + TRIHIDTV_EHCI_LEN - 1,
		.flags		= IORESOURCE_MEM,
	},
	[1] = {
		.start		= TRIHIDTV_USB_IRQ,
		.end		= TRIHIDTV_USB_IRQ,
		.flags		= IORESOURCE_IRQ,
	},
};

/* SELP.4.2.1.x Add patch No.37, TridentSX3 chip porting, 2009-08-07 */
#ifdef CONFIG_TRIHIDTV_CPU_425M
static struct resource trihidtv_usb_ehci2_resources[] = {
       [0] = { 
               .start          = TRIHIDTV_EHCI2_BASE,
               .end            = TRIHIDTV_EHCI2_BASE + TRIHIDTV_EHCI2_LEN - 1,
               .flags          = IORESOURCE_MEM,
       },
       [1] = {
               .start          = TRIHIDTV_USB2_IRQ,
               .end            = TRIHIDTV_USB2_IRQ,
               .flags          = IORESOURCE_IRQ,
       },
};
#endif

/*
 * This is the patch for highmemory problem at using usb.
 */
#ifdef CONFIG_HIGHMEM

static u64 ehci_dmamask = HIGHMEM_START;

#else

static u64 ehci_dmamask = ~(u32)0;

#endif

static struct platform_device trihidtv_usb_ehci_device = {
	.name		= "trihidtv-ehci",
	.id		= 0,
	.dev = {
		.dma_mask		= &ehci_dmamask,
		.coherent_dma_mask	= 0xffffffff,
	},
	.num_resources	= ARRAY_SIZE(trihidtv_usb_ehci_resources),
	.resource	= trihidtv_usb_ehci_resources,
};

/* SELP.4.2.1.x Add patch No.37, TridentSX3 chip porting, 2009-08-07 */
#ifdef CONFIG_TRIHIDTV_CPU_425M
static struct platform_device trihidtv_usb_ehci2_device = {
       .name           = "trihidtv-ehci",
       .id             = 1,
       .dev = {
               .dma_mask               = &ehci_dmamask,
               .coherent_dma_mask      = 0xffffffff,
       },
       .num_resources  = ARRAY_SIZE(trihidtv_usb_ehci2_resources),
       .resource       = trihidtv_usb_ehci2_resources,
};
#endif

static struct platform_device *trihidtv_platform_devices[] __initdata = {
	&trihidtv_usb_ehci_device,
#ifdef CONFIG_TRIHIDTV_CPU_425M
	&trihidtv_usb_ehci2_device,
#endif
};

int trihidtv_platform_init(void)
{
	return platform_add_devices(trihidtv_platform_devices, ARRAY_SIZE(trihidtv_platform_devices));
}

arch_initcall(trihidtv_platform_init);
