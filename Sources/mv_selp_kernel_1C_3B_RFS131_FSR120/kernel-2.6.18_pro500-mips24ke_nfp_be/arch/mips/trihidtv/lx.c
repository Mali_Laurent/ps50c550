/*
 * TTI Shanghai
 * 
 * 09/10/2003
 * Alan Liu alanliu@trident.com.cn
 * 08/19/2008 
 * Alan Liu: Change this to SX chip
 */

#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/types.h>
#include <linux/mm.h>
#include <linux/swap.h>
#include <linux/ioport.h>
#include <linux/interrupt.h>
#include <linux/timex.h>
#include <linux/kernel_stat.h>
#include <asm/bootinfo.h>
#include <asm/page.h>
#include <asm/io.h>
#include <asm/irq.h>
#include <asm/processor.h>
#include <asm/ptrace.h>
#include <asm/reboot.h>
#include <asm/mc146818rtc.h>
#include <linux/version.h>
#include <linux/bootmem.h>
#include <asm/irq.h>

#include <linux/mc146818rtc.h>
#include <asm/trihidtv/trihidtv.h>
#include <asm/trihidtv/trihidtv_irq.h>
#include <asm/trihidtv/trihidtv_defs.h>
#include <asm/trihidtv/trihidtv_addrspace.h>

/**
 * Variable to store the size of the RAM.
 */
unsigned long mem_size;

/*
 * Alan Liu
 * add the following declaration of the extern functions.
 */
extern void (*board_time_init) (void);
extern void trihidtv_time_init(void);

/*
 *
 * Defined in kernel/resource.c
 * We will reset them here
 */
extern struct resource ioport_resource;
extern struct resource iomem_resource;

extern unsigned long mips_machtype;
extern unsigned long mips_machgroup;
extern void Init_Serial(void);

extern int hidtvpro_hw_misc(void);
extern void (*flush_icache_range)(unsigned long start, unsigned long end);

void __init plat_mem_setup(void);

/**
 * Function stub.
 */
void
prom_free_prom_memory(void)
{
};

/**
 * This funtion initializes kernel variables with data from the boot
 * loader.
 */
void __init prom_init(void)
{

	/*
	 * Init serial port for early output...
	 */
	char *hidtvpro_def_cmd = CONFIG_CMDLINE;

	Init_Serial();

#if 0
#ifdef CONFIG_BLK_DEV_INITRD
        //ROOT_DEV = MKDEV(RAMDISK_MAJOR, 0);
        initrd_start = (unsigned long) &__initramfs_start;
        initrd_end = (unsigned long) &__initramfs_end;
#endif
#endif

#ifdef CONFIG_TRIHIDTV_CPU_425M
    if((ReadRegHWord(0x1b00001e)>>8) != 0x91){
        while(1)
            puts("This is not PRO-SX3\n");
    }else{
        printk("Trident SX3 SOC detected\n");
    }
#endif

//Alan Liu
#if 1
	if (fw_arg3 == 0) {
		strlcpy(arcs_cmdline, hidtvpro_def_cmd, COMMAND_LINE_SIZE);
	} else {
		strlcpy(arcs_cmdline, ((struct bootParams *) fw_arg3)->cmd_line, 
			COMMAND_LINE_SIZE);
	}
#else
	strlcpy(arcs_cmdline, hidtvpro_def_cmd, COMMAND_LINE_SIZE);
	printk("%s:%d	change this back after linux is ready\n",__FILE__,__LINE__);	
	printk("%s:%d	arcs_cmdline is %s\n",__FILE__,__LINE__,arcs_cmdline);	
#endif

	/*
	 * We need process the command line parameter here??
	 * Alan Liu
	 */
	mips_machgroup = MACH_GROUP_TRIHIDTV;
	mips_machtype = MACH_TRIHIDTV;
#if 1
	/*
	 * We should get memsize from bootloader.
	 * Bootloader will get it through CONFIG table in flash...
	 * When mem=XXX[KkmM]@YYY[KkmM] is set in cmdline,then
	 * The default setting will be blown away in setup.c
	 * Alan Liu
	 */
	mem_size = TRIHIDTV_RAM_DEFAULT_SIZE;
	printk("Default %d MB SDRAM,parse mem=xxx[MmKkGg] later...\n",
	       (int) (mem_size >> 20));
	add_memory_region(TRIHIDTV_RAM_BASE, mem_size, BOOT_MEM_RAM);
#endif
}

/*
 * This line is the patch for highmemory problem at using usb.
 */
extern unsigned int PCI_DMA_BUS_IS_PHYS;
void __init 
plat_mem_setup(void)
{
	/*
	 * Misc settings about USE,IDE,and so on...
	 */
	hidtvpro_hw_misc();

	/*
	 * Alan Liu
	 * Add the new board_time_init and board_timer_setup assignment
	 * Commented the original assignment of board_timer_setup 
	 */
	board_time_init = trihidtv_time_init;
	
	/*
	 * IO/MEM resources.
	 * revisit this area.
	 */
	//mips_io_port_base = KSEG1;
	set_io_port_base(KSEG1);

#if 1
	/*
	 * It seems we dont separate IO/MEM clearly...
	 * have to do this ...
	 * Alan Liu
	 */
	ioport_resource.start = 0x10000000;
	ioport_resource.end = 0x1fffffff;
	iomem_resource.start = 0x10000000;
	iomem_resource.end = 0x1fffffff;
#endif
	/*
	 * This like is the patch for highmemory problem at using usb.
	 * Set PCI variables here.
	*/
	PCI_DMA_BUS_IS_PHYS = 1;
}
