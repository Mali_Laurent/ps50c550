/*
 * TTI Shanghai
 * 09/17/2003
 * Author : Alan Liu alanliu@trident.com.cn
 * Trident HiDTV SOC  system controller specific pci support.
 *
 *  This program is free software; you can redistribute  it and/or modify it
 *  under  the terms of  the GNU General  Public License as published by the
 *  Free Software Foundation;  either version 2 of the  License, or (at your
 *  option) any later version.
 *
 *  THIS  SOFTWARE  IS PROVIDED   ``AS  IS'' AND   ANY  EXPRESS OR IMPLIED
 *  WARRANTIES,   INCLUDING, BUT NOT  LIMITED  TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
 *  NO  EVENT  SHALL   THE AUTHOR  BE    LIABLE FOR ANY   DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED   TO, PROCUREMENT OF  SUBSTITUTE GOODS  OR SERVICES; LOSS OF
 *  USE, DATA,  OR PROFITS; OR  BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 *  ANY THEORY OF LIABILITY, WHETHER IN  CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  You should have received a copy of the  GNU General Public License along
 *  with this program; if not, write  to the Free Software Foundation, Inc.,
 *  675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <linux/types.h>
#include <linux/pci.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include <asm/addrspace.h>
#include <asm/trihidtv/trihidtv_addrspace.h>
#include <asm/trihidtv/trihidtv_pci.h>
#include <asm/debug.h>

#define PCI_ACCESS_READ  0
#define PCI_ACCESS_WRITE 1

static inline int check_abort(void)
{
	 /*
         * Revisit: check for master or target abort.
         */
	return PCIBIOS_SUCCESSFUL;
}

/*
 * shoud undef it when not debugging...
 * Alan Liu
 */
#ifdef DEBUG
#define DBG(x...) printk(x)
#else
#define DBG(x...)
#endif

static struct resource pci_io_resource = {
	.name = "Trident HiDTV SOC pci IO space", 
	.start = TRIHIDTV_PCI_IO_BASE + TRIHIDTV_PCI_IO_BASE_SKIP,
	.end = TRIHIDTV_PCI_IO_BASE + TRIHIDTV_PCI_IO_SIZE-1,
	.flags = IORESOURCE_IO
};

static struct resource pci_mem_resource = {
	.name = "Trident HiDTV SOC pci memory space ", 
	.start = TRIHIDTV_PCI_MEM_BASE,
	.end = TRIHIDTV_PCI_MEM_BASE + TRIHIDTV_PCI_MEM_SIZE-1,
	.flags = IORESOURCE_MEM
};

struct pci_ops trihidtv_pci_ops;
/*
 * The mips_pci_channels array has all descriptors for all
 * pci bus controllers. Usually on most boards there's only
 * one pci controller to worry about.
 *
 * Note that the '0' and '0xff' below indicate the first
 * and last "devfn" to scan.  You can use these variables
 * to limit the scan.
 *
 * we should maybe set 'devfn' from 0 to 160,but it will not
 * incur anything wrong,so it need not...
 *
 * should start from 0x9(H2P,P2U) should not be so!
 *
 */

#if 0
struct pci_channel mips_pci_channels[] = {
	{ &trihidtv_pci_ops, &pci_io_resource, &pci_mem_resource, 0x9, 0xff },
	{ NULL, NULL, NULL, NULL, NULL}
};
#endif

/*
 *
 * The config routine usually does dword accesses only
 * The functions calling the routine then have to mask
 * the returned value.
 *
 * -----IMPORTANT------
 * If a pci config cycle fails, it's *very* important
 * that if the cycle requested is READ, you set *data
 * to 0xffffffff. The pci_auto code does not check the
 * return value of the pci_ops functions. It expects that
 * if a pci config cycle read fails, the data returned
 * will be 0xffffffff.
 *
 */

static int
trihidtv_pcibios_config_access(unsigned char access_type, unsigned char bus,
                           unsigned char dev_fn,unsigned char where, u32 *data)
{
	/* 
	 * config cycles are on 4 byte boundary only
	 */
#if 0
	unsigned char bus = dev->bus->number;
	unsigned char dev_fn = dev->devfn;
#endif

	DBG("it config: type %d dev %x bus %d dev_fn %x data %x\n",
			access_type, dev, bus, dev_fn, *data);

	/* Setup address */
	TRIHIDTV_WRITE(TRIHIDTV_CONFADDR, (bus << TRIHIDTV_BUSNUM_SHF) | 
			(dev_fn << TRIHIDTV_FUNCNUM_SHF) | (where & ~0x3));

	if (access_type == PCI_ACCESS_WRITE) {
		TRIHIDTV_WRITE(TRIHIDTV_CONFDATA, *data);
	} 
	else {
		TRIHIDTV_READ(TRIHIDTV_CONFDATA, *data);
	}

	/*
	 * Revisit: check for master or target abort.
	 */
	return 0;
}

/*
 * We can't address 8 and 16 bit words directly.  Instead we have to
 * read/write a 32bit word and mask/modify the data we actually want.
 */
static int
read_config_byte (unsigned char bus,unsigned char devfn, unsigned char where, u8 *val)
{
	u32 data = 0;

	if (trihidtv_pcibios_config_access(PCI_ACCESS_READ,
					bus,devfn,where,&data))
		return -1;

	*val = (data >> ((where & 3) << 3)) & 0xff;
        DBG("cfg read byte: bus %d dev_fn %x where %x: val %x\n", 
                dev->bus->number, dev->devfn, where, *val);

	return PCIBIOS_SUCCESSFUL;
}


static int
read_config_word (unsigned char bus,unsigned char devfn, unsigned char where, u16 *val)
{
	u32 data = 0;

	if (where & 1)
		return PCIBIOS_BAD_REGISTER_NUMBER;

	if (trihidtv_pcibios_config_access(PCI_ACCESS_READ,
						bus,devfn, where, &data))
	       return -1;

	*val = (data >> ((where & 3) << 3)) & 0xffff;
        DBG("cfg read word: bus %d dev_fn %x where %x: val %x\n", 
                dev->bus->number, dev->devfn, where, *val);

	return PCIBIOS_SUCCESSFUL;
}

static int
read_config_dword (unsigned char bus,unsigned char devfn, unsigned char where, u32 *val)
{
	u32 data = 0;

	if (where & 3)
		return PCIBIOS_BAD_REGISTER_NUMBER;
	
	if (trihidtv_pcibios_config_access(PCI_ACCESS_READ, 
					bus,devfn, where, &data))
		return -1;

	*val = data;
        DBG("cfg read dword: bus %d dev_fn %x where %x: val %x\n", 
                dev->bus->number, dev->devfn, where, *val);

	return PCIBIOS_SUCCESSFUL;
}

static int
write_config_byte (unsigned char bus,unsigned char devfn, unsigned char where, u8 val)
{
	u32 data = 0;
       
	if (trihidtv_pcibios_config_access(PCI_ACCESS_READ,
					bus,devfn, where, &data))
		return -1;

	data = (data & ~(0xff << ((where & 3) << 3))) |
	       (val << ((where & 3) << 3));

	if (trihidtv_pcibios_config_access(PCI_ACCESS_WRITE, 
					bus,devfn, where, &data))
		return -1;

	return PCIBIOS_SUCCESSFUL;
}

static int
write_config_word (unsigned char bus,unsigned char devfn, unsigned char where, u16 val)
{
        u32 data = 0;

	if (where & 1)
		return PCIBIOS_BAD_REGISTER_NUMBER;
       
        if (trihidtv_pcibios_config_access(PCI_ACCESS_READ, 
					bus,devfn, where, &data))
	       return -1;

	data = (data & ~(0xffff << ((where & 3) << 3))) | 
	       (val << ((where & 3) << 3));

	if (trihidtv_pcibios_config_access(PCI_ACCESS_WRITE, 
					bus,devfn, where, &data))
	       return -1;


	return PCIBIOS_SUCCESSFUL;
}

static int
write_config_dword (unsigned char bus,unsigned char devfn, unsigned char where, u32 val)
{
	if (where & 3)
		return PCIBIOS_BAD_REGISTER_NUMBER;

	if (trihidtv_pcibios_config_access(PCI_ACCESS_WRITE, 
					bus,devfn, where, &val))
	       return -1;

	return PCIBIOS_SUCCESSFUL;
}

#if 0
unsigned __init int pcibios_assign_all_busses(void)
{
       return 1;
}
#endif

static int trihidtv_pci_read_config(struct pci_bus *bus, unsigned int devfn,
	int where, int size, u32 * val)
{
	switch (size) {
	case 1:
		read_config_byte(bus->number,devfn,where,val);
		break;

	case 2:
		read_config_word(bus->number,devfn,where,val);
		break;

	case 4:
		read_config_dword(bus->number,devfn,where,val);
		break;
	}

	return check_abort();
}

static int trihidtv_pci_write_config(struct pci_bus *bus, unsigned int devfn,
	int where, int size, u32 val)
{

	switch (size) {
	case 1:
		write_config_byte(bus->number,devfn,where,val);
		break;

	case 2:
		write_config_word(bus->number,devfn,where,val);
		break;

	case 4:
		write_config_dword(bus->number,devfn,where,val);
	}

	return check_abort();
}

struct pci_ops trihidtv_pci_ops = {
	trihidtv_pci_read_config,
	trihidtv_pci_write_config,
};

struct pci_controller trihidtv_pci_controller = {
	.pci_ops        = &trihidtv_pci_ops,
	.io_resource    = &pci_io_resource,
	.mem_resource   = &pci_mem_resource,
};
