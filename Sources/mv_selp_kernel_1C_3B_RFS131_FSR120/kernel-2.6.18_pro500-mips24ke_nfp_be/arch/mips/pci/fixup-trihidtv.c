/*
 * fixup-trihidtv.c
 *
 * Copyright (C) 2007 Trident Multimedia Technologies (Shanghai) Co.Ltd.
 * Copyright 2000 MontaVista Software Inc.
 * Author: MontaVista Software, Inc.
 *         	ppopov@mvista.com or source@mvista.com
 *
 * Alan Liu
 * TTI(Shanghai),2003
 * Alan Liu 2007
 */
#include <linux/types.h>
#include <linux/pci.h>
#include <linux/kernel.h>
#include <linux/init.h>

#include <asm/trihidtv/trihidtv_addrspace.h>
#include <asm/trihidtv/trihidtv_pci.h>
#include <asm/trihidtv/trihidtv_int.h>

extern struct pci_controller trihidtv_pci_controller[];

int pci_get_irq(struct pci_dev *dev, int pin)
{
	int irq = pin;
	u8 slot = PCI_SLOT(dev->devfn);
	u8 func = PCI_FUNC(dev->devfn);
        const int internal_func_irqs[7] = {
            0xff,	/* P2U */
            0xff,	/* reserved */
            0xff,	/* reserved */
            0xff,	/* reserved */
            0xff,	/* P2I */
            TRIHIDTV_IDE_IRQ,	/* IDE */
            TRIHIDTV_MC68K_IRQ	/* MC68K */
        };

	switch (slot) {
		case 0:
			/*
			 * CPU/PCI Bridge(H2P)
			 * has no INTERRUPT_PIN and INTERRUPT_LINE reg
			 */
			irq = 0xff;
			break;
		case 1:
		    /*
		     * slot 1 now has P2U,P2I,IDE,MC68K linked,
		     */
		    if (func < 7)
				irq = internal_func_irqs[func];
		    else
				irq = 0xff;
		    break;
		case 2:
		case 3:
		case 4:
		case 5:
		case 6:
		case 7:
		case 8:
		case 9:
		case 10:
		case 11:
		case 12:
		case 13:
		case 14:
		case 15:
		case 16:
		case 17:
		case 18:
		case 19:
		case 20:
		    	/*
			 * 2~~~~~~20 is used to plugin External Devices.
			 * Use any one freely
			 */
			switch (pin) {
				case 1: /* pin A */
					irq = TRIHIDTV_PCI_INTA_IRQ;
					break;
				case 2: /* pin B */
					irq = TRIHIDTV_PCI_INTB_IRQ;
					break;
				case 3: /* pin C */
					irq = TRIHIDTV_PCI_INTC_IRQ;
					break;
				case 4: /* pin D */
					irq = TRIHIDTV_PCI_INTD_IRQ;
					break;
				case 0: /* no IRQ needed for this device */
				default:
					irq = 0xff; 
					break;
			}
			break;
		default:
			printk(" pcibios_fixup_irqs,why has this device that slot is %d?\n",slot);
			irq = 0xff; 
			break;
	}
	return irq;
}

int __init pcibios_map_irq(struct pci_dev *dev, u8 slot, u8 pin)
{
	unsigned char irq = 0;

	irq = pci_get_irq(dev, pin);

	printk(KERN_INFO "PCI: 0x%02x:0x%02x(0x%02x,0x%02x) IRQ=%d\n",
	       dev->bus->number, dev->devfn, PCI_SLOT(dev->devfn),
	       PCI_FUNC(dev->devfn), irq);

	return irq;
}

/*
 * Do platform specific device initialization at pci_enable_device() time
 */
int pcibios_plat_dev_init(struct pci_dev *dev)
{
	return 0;
}

/*
 * Fixup your resources here, if necessary. *Usually* you
 * don't have to do anything here.
 * Called after pcibios_fixup() and pcibios_fixup_irqs().
 *
 * The calling sequence of this function:
 *
 * pci_init()					drivers/pci/pci.c,if CONFIG_PCI is defined
 * 	pcibios_init()				arch/mips/kernel/pci.c,if CONFIG_NEW_PCI is defined
 * 		pciauto_assign_resources(mips_pci_channels); 	arch/mips/kernel/pci_auto.c, if CONFIG_PCI_AUTO is defined
 * 			pciauto_bus_scan();			arch/mips/kernel/pci_auto.c
 * 				this is a recursive function,when it found a bridge on the bus.
 * 				
 *
 * 		pci_scan_bus();			drivers/pci/pci.c
 * 			pci_do_scan_bus();
 * 				pci_scan_slot();
 * 					pci_fixup_device(PCI_FIXUP_HEADER, dev);	
 *						
 *						all PCI_FIXUP_HEADER is fixed here...
 *
 * 		pcibios_fixup();		this file.
 * 		pcibios_fixup_irqs();		this file.
 *
 * 	pci_fixup_device(PCI_FIXUP_FINAL,dev);			drivers/pci/quirks.c
 *		pci_do_fixups(dev, pass, pcibios_fixups);	drivers/pci/quirks.c
 *			pcibios_fixups is defined in arch/mips/kernel/pci.c,it is a 'struct pci_fixup'.
 *			in this struct,pcibios_fixup_resourcs() is set as the hookfunction...
 *
 *	        pci_do_fixups(dev, pass, pci_fixups); 		drivers/pci/quirks.c
 *			pcibios_fixups is defined in drivers/pci/quirks.c,it is a 'struct pci_fixup'.
 *			All the quirky borads/devices will has its fixup hook here,only PCI_FIXUP_FINAL will
 *			be processed here.
 *
 * We should modify pcibios_fixup_irqs here.
 * IN future, if any problem occurs in our HiDTV chip, We could modify specific fixup functions or
 * struct pci_fixup pcibios_fixups={},PCI_FIXUP_HEADER or PCI_FIXUP_FINAL
 * Alan Liu
 * 09/18/2003
 *
 */
void __init pcibios_fixup_resources(struct pci_dev *dev)
{
}

/*
 * Any board or system controller fixups go here.
 * Now, this is called after the pci_auto code (if enabled) and
 * after the linux pci scan.
 *
 * See arch/mips/kernel/pci.c, pcibios_init() will can this function.
 */

void __init pcibios_fixup(void)
{
}

